<?php
 defined('BASEPATH') OR exit('No direct script access allowed'); require_once('./application/libraries/Dex_Rest_Controller.php'); require (APPPATH . 'controllers/PassHash.php'); require (APPPATH . 'controllers/AuthManager.php'); require (APPPATH . 'controllers/CommonMethods.php'); class User extends Dex_Rest_Controller { private $commonMethods = null; public function __construct() { parent::__construct(); $this->load->library(array('form_validation', 'session')); $this->load->helper(array('url', 'form')); $this->load->model('usermodel'); $this->load->model('creditmodel'); $this->load->model('visitormodel'); $this->load->model('organizationmodel'); $this->load->model('emailtemplatesmodel'); $this->load->model('shareusermodel'); $this->load->model('emailqueuemodel'); $this->load->database(); $this->lang->load('messages', 'english'); $this->_salt = "123451711201454321"; $this->load->library('session'); $this->commonMethods = new CommonMethods(); } public function user_get() { AuthManager::authenticate(); log_message("debug", "inside the user** get"); $key = $this->get('key'); $val = $this->get('value'); log_message("debug", "got key as " . $key . " and value as " . $val); $users = array(); if ($this->get('key') && $this->get('value')) { log_message("debug", "getting user data for key as [" . $key . "] and value as [" . $val . "]"); $users = $this->usermodel->getBy($this->get('key'), $this->get('value')); } $this->response($users, 200); } public function getUserCount_get() { AuthManager::authenticate(); $user_count = 0; if ($this->get('organizationId')) { $user_count = $this->usermodel->getUsersCount($this->get('organizationId')); } $resp_message = array("status" => "success", "userCount" => $user_count); $this->response($resp_message, 200); } public function getMaxUserCount_get() { AuthManager::authenticate(); $user_count = 0; if ($this->get('organizationId')) { $user_count = $this->usermodel->getUsersCount($this->get('organizationId')); } $resp_message = array("status" => "success", "userCount" => $user_count); $this->response($resp_message, 200); } public function getMaxAccountUsers_get() { AuthManager::authenticate(); $user_count = 0; if ($this->get('organizationId')) { $user_count = $this->usermodel->getMaxUsersCount($this->get('organizationId')); if (!empty($user_count)) { $resp_message = array("status" => "success", "maxUserCount" => $user_count[0]->multi_user); } else { $resp_message = array("status" => "error", "maxUserCount" => 0); } } $this->response($resp_message, 200); } public function getUserById_get() { AuthManager::authenticate(); $users = array(); if ($this->get('inviteId')) { $usersDet = $this->shareusermodel->getShareUserOrgById($this->get('inviteId')); if (!empty($usersDet)) { $users = $usersDet[0]; } } else { $users = array(); } $this->response($users, 200); } public function emailUpdatePreference_put() { AuthManager::authenticate(); if ($this->put('id')) { $emailPref = array( "id" => $this->put('id'), "assignNotify" => !empty($this->put('assignNotify')) ? $this->put('assignNotify') : '0', "commentNotify" => !empty($this->put('commentNotify')) ? $this->put('commentNotify') : '0', "followUpDaily" => !empty($this->put('followUpDaily')) ? $this->put('followUpDaily') : '0', "followUpOntime" => !empty($this->put('followUpOntime')) ? $this->put('followUpOntime') : '0', "weeklyReport" => !empty($this->put('weeklyReport')) ? $this->put('weeklyReport') : '0', "productNotify" => !empty($this->put('productNotify')) ? $this->put('productNotify') : '0', "promotionNotify" => !empty($this->put('promotionNotify')) ? $this->put('promotionNotify') : '0', "modifiedAt" => gmdate('Y-m-d H:i:s') ); if ($this->usermodel->updatePreference($emailPref)) { $respMsg = array("status" => "success", "message" => "User email preference updated successfully"); $this->response($respMsg, 200); } else { $respMsg = array("status" => "error", "message" => "User email preference updation failed"); $this->response($respMsg, 200); } } else { $respMsg = array("status" => "error", "message" => "User Id is reuqired to update the email preference"); $this->response($respMsg, 200); } } public function userByOrganization_get() { AuthManager::authenticate(); if ($this->get('organizationId')) { $users = $this->usermodel->getUsersByOrganizationwithRole($this->get('organizationId')); } if ($users) { $this->response($users, 200); } else { $this->response([], 404); } } public function userCustomColumn_put() { AuthManager::authenticate(); if ($this->put('userId')) { $userId = $this->put('userId'); $custColumn = $this->nullIfEmpty($this->put('custColumn')); if ($this->usermodel->updateColumnPref($userId, $custColumn)) { $this->response(array("status" => "success", "message" => ""), 200); } } } public function getCustomColumn_get() { AuthManager::authenticate(); if ($this->get('userId')) { $userId = $this->get('userId'); $userPref = $this->usermodel->getColumnPref($userId); $this->response($userPref, 200); } } public function userByAdminOrganization_get() { if ($this->get('organizationId')) { $users = $this->usermodel->getUsersByOrganizationwithRole($this->get('organizationId')); } if ($users) { $this->response($users, 200); } else { $this->response([], 404); } } public function user_post() { AuthManager::authenticate(); if (!$this->post('email') || !$this->post('password')) { $this->response(array('error' => 'Missing post data: email'), 400); } $password = $this->post('password'); $password_hash = PassHash::hash($password); $name = $this->post('fullname'); $companyName = $this->post('companyName'); $organizationId = $this->post('organizationId'); $orgName = ""; $countryName = ""; $addr1 = ""; $addr2 = ""; $cityName = ""; $stateName = ""; $zipcode = ""; $orgCountryCode = ""; $orgDetails = $this->organizationmodel->getById($organizationId); if (!empty($orgDetails)) { $orgName = $orgDetails[0]->displayName; $countryName = $orgDetails[0]->country; $addr1 = $orgDetails[0]->address1; $addr2 = $orgDetails[0]->address2; $cityName = $orgDetails[0]->city; $stateName = $orgDetails[0]->state; $zipcode = $orgDetails[0]->zip; $orgCountryCode = $orgDetails[0]->contactPhoneCode; } $data = array( 'firstName' => $this->nullIfEmpty($this->post('firstName')), 'lastName' => $this->nullIfEmpty($this->post('lastName')), 'designation' => $this->nullIfEmpty($this->post('designation')), 'email' => $this->nullIfEmpty($this->post('email')), 'password_hash' => $password_hash, 'phone' => $this->nullIfEmpty($this->post('phone')), 'phoneCode' => $this->nullIfEmpty($this->post('phoneCode')), 'address1' => $addr1, 'address2' => $addr2, 'city' => $cityName, 'state' => $stateName, 'zip' => $zipcode, 'country' => $countryName, 'organizationId' => $this->post('organizationId'), 'access_level' => $this->post('access_level'), 'roleId' => $this->post('role'), 'organizationName' => $orgName, ); $roleDet = ""; $roleId = $this->post('role'); if ($roleId == '1') { $roleDet = "Account Owner"; } if ($roleId == '2') { $roleDet = "Manager"; } if ($roleId == '3') { $roleDet = "L1 User"; } if ($roleId == '4') { $roleDet = "L2 User"; } if ($roleId == '5') { $roleDet = "LeadsWallet User"; } $userId = 0; $userName = $this->post('email'); if ($userName) { $usersCheck = $this->usermodel->getByWithoutStatus("email", $userName); } $this->db->where('id', $organizationId); $orgDet = $this->db->get('organization')->result(); if (!empty($orgDet)) { if ($orgDet[0]->accountType === 'FR') { $message = array( 'status' => 'error', 'message' => "We appreciate your extensive use of HelloLeads for your business. Please subscribe to paid plan to continue to enjoy the full power of HelloLeads."); $this->response($message, 200); } } if (empty($usersCheck)) { $credits = array(); $creditstemp = $this->creditmodel->getBy("organizationId", $organizationId); if ($creditstemp) { $credits = $creditstemp[0]; } if ($this->post('role') == '5') { $usersAcCheck = $this->usermodel->getByACWithoutStatus($organizationId, $this->post('role')); if (!empty($usersAcCheck)) { $message = array( 'status' => 'error', 'message' => "Organization can have onle one LeadsWallet user", ); $this->response($message, 200); } } $usercount = $this->usermodel->getUserCount($organizationId); if ($usercount >= (int) $credits->multi_user) { $message = array( 'status' => 'error', 'message' => "The maximum number of users allowed for your account is " . $credits->multi_user . " you already have " . $credits->multi_user . ". Please subscribe / pay for additional users", ); $this->response($message, 200); } else { $userId = $this->usermodel->create($data); } } if ($userId > 0) { if ($companyName) { $this->organizationmodel->updateCompanyName($companyName, $organizationId); } $cName = ""; $message = ''; if (!empty($orgDetails)) { $cName = $orgDetails[0]->name; $userSubscribeDet = $this->usermodel->getBy("email", $orgDetails[0]->contactEmail); if (!empty($userSubscribeDet)) { if ($userSubscribeDet[0]->unsubscribe_status === '0') { if ($usercount <= 2) { $tempDet = $this->emailtemplatesmodel->getByName('Roles and Permissions'); if (!empty($tempDet)) { if (!empty($tempDet[0]->body) && !empty($tempDet[0]->subject)) { $comemailmessage = $tempDet[0]->body; $comemailmessage = str_replace("#VisitorName#", $orgDetails[0]->contactFirstName, $comemailmessage); $comemailsubject = $tempDet[0]->subject; $comemailmessage = $comemailmessage . $this->commonMethods->commonEmailFooter($comemailsubject, "commu", $orgDetails[0]->contactEmail); } } $comNewmaildata = array( "fromId" => "linda@helloleads.io", "fromName" => "Linda - HelloLeads", "toId" => $orgDetails[0]->contactEmail, "bcc" => "autocomm@helloleads.io", "subject" => $comemailsubject, "message" => $comemailmessage, "date" => gmdate('Y-m-d H:i:s') ); $comNewRegistrationmailqueueId = $this->emailqueuemodel->create($comNewmaildata); } } $this->commonMethods->sendPushToMsgSep($orgDetails[0]->contactEmail, "New User added Sucessfully. To change user details, in manage users , click option across the user to edit / delete it.", "HelloLeads", "https://www.helloleads.io"); } } if ($this->post('role') == '5') { $message = $this->getwelcomeEmailLW($userName, $password, $name, $cName); } else if ($this->post('access_level') == 'REGI') { $message = $this->getwelcomeEmailREGI($userName, $password, $name, $cName); } else { $message = $this->getwelcomeEmail($userName, $password, $name, $cName, $roleDet); } $subject = 'You have been added to HelloLeads account by ' . $name; log_message('debug', '******* Sending welcome email to to email [' . $userName . ']'); log_message('debug', 'sending subject [' . $subject . '] message [' . $message . ']'); if ($userName) { $maildata = array( "fromid" => "no-reply@helloleads.io", "fromName" => $name, "toid" => $userName, "subject" => $subject, "message" => $message, "status" => 'pending', "date" => gmdate('Y-m-d H:i:s') ); $mailqueueId = $this->emailqueuemodel->create($maildata); log_message("debug", "Welcome Email " . $userName . " added to queue id" . $mailqueueId); } $message = array('id' => $userId, 'email' => $userName, 'status' => 'success', 'message' => sprintf($this->lang->line('user.add'), $userName)); $this->response($message, 200); } else { if (!empty($usersCheck)) { $usertemp = $usersCheck[0]; if ($usertemp->organizationId == $organizationId && $usertemp->status == '1') { $data['status'] = '0'; $data['modifiedAt'] = gmdate('Y-m-d H:i:s'); $this->db->where('id', $usertemp->id); $this->db->update('users', $data); $userExtID = (int) $usertemp->id; $message = array('id' => $userExtID, 'email' => $userName, 'status' => 'success', 'message' => sprintf($this->lang->line('user.add'), $userName)); $this->response($message, 200); } else if ($usertemp->organizationId == $organizationId and $userName == $usertemp->email) { $message = array( 'status' => 'error', 'message' => "Email Id (" . $userName . ") already exists"); $this->response($message, 200); } else { $message = array( 'status' => 'error', 'message' => "Email Id (" . $userName . ") already exists"); $this->response($message, 200); } } $message = array( 'status' => 'error', 'message' => "Email Id (" . $userName . ") already exists"); $this->response($message, 200); } } public function userChkEmail_get() { $email = $this->get('email'); $user = array(); if (!empty(email)) { $userDet = $this->usermodel->getBy('email', $email); } } public function user_put() { AuthManager::authenticate(); if (!$this->put('id')) { $this->response(array('error' => 'id is required for update'), 400); } $data = array(); $userId = $this->put('id'); $companyName = $this->put('companyName'); $userName = $this->put('email'); $organizationId = $this->put('organizationId'); if ($this->put('password')) { $password = $this->put('password'); $password_hash = PassHash::hash($password); $data = array( 'id' => $this->put('id'), 'firstName' => $this->nullIfEmpty($this->put('firstName')), 'lastName' => $this->nullIfEmpty($this->put('lastName')), 'designation' => $this->nullIfEmpty($this->put('designation')), 'email' => $this->nullIfEmpty($this->put('email')), 'password_hash' => $password_hash, 'phone' => $this->nullIfEmpty($this->put('phone')), 'phoneCode' => $this->nullIfEmpty($this->put('phoneCode')), 'address1' => $this->nullIfEmpty($this->put('address1')), 'address2' => $this->nullIfEmpty($this->put('address2')), 'city' => $this->nullIfEmpty($this->put('city')), 'state' => $this->nullIfEmpty($this->put('state')), 'zip' => $this->nullIfEmpty($this->put('zip')), 'country' => $this->nullIfEmpty($this->put('country')), 'organizationId' => $this->put('organizationId'), 'access_level' => $this->put('access_level'), 'roleId' => $this->put('role'), 'organizationName' => $this->nullIfEmpty($this->put('organizationName')), 'userWebsite' => $this->nullIfEmpty($this->put('userWebsite')), 'telephoneDir' => $this->nullIfEmpty($this->put('telephoneDir')), 'telephoneOffice' => $this->nullIfEmpty($this->put('telephoneOffice')), 'facebookURL' => $this->nullIfEmpty($this->put('facebookURL')), 'twitterURL' => $this->nullIfEmpty($this->put('twitterURL')), 'linkedinURL' => $this->nullIfEmpty($this->put('linkedinURL')), 'instagramURL' => $this->nullIfEmpty($this->put('instagramURL')), 'otherURL' => $this->nullIfEmpty($this->put('otherURL')) ); } else { $data = array( 'id' => $this->put('id'), 'firstName' => $this->nullIfEmpty($this->put('firstName')), 'lastName' => $this->nullIfEmpty($this->put('lastName')), 'designation' => $this->nullIfEmpty($this->put('designation')), 'email' => $this->nullIfEmpty($this->put('email')), 'phone' => $this->nullIfEmpty($this->put('phone')), 'phoneCode' => $this->nullIfEmpty($this->put('phoneCode')), 'address1' => $this->nullIfEmpty($this->put('address1')), 'address2' => $this->nullIfEmpty($this->put('address2')), 'city' => $this->nullIfEmpty($this->put('city')), 'state' => $this->nullIfEmpty($this->put('state')), 'zip' => $this->nullIfEmpty($this->put('zip')), 'country' => $this->nullIfEmpty($this->put('country')), 'organizationId' => $this->put('organizationId'), 'access_level' => $this->put('access_level'), 'roleId' => $this->put('role'), 'organizationName' => $this->nullIfEmpty($this->put('organizationName')), 'userWebsite' => $this->nullIfEmpty($this->put('userWebsite')), 'telephoneDir' => $this->nullIfEmpty($this->put('telephoneDir')), 'telephoneOffice' => $this->nullIfEmpty($this->put('telephoneOffice')), 'facebookURL' => $this->nullIfEmpty($this->put('facebookURL')), 'twitterURL' => $this->nullIfEmpty($this->put('twitterURL')), 'linkedinURL' => $this->nullIfEmpty($this->put('linkedinURL')), 'instagramURL' => $this->nullIfEmpty($this->put('instagramURL')), 'otherURL' => $this->nullIfEmpty($this->put('otherURL')) ); } $roleDet = $this->put('role'); $this->db->where('organizationId', $organizationId); $this->db->where('roleId', '1'); $this->db->where('status', '0'); $userAccountDet = $this->db->get('users')->num_rows(); if ($userAccountDet === 1 and $roleDet !== 1) { $message = array( 'status' => 'error', 'message' => "You can't modify your current role. There must be atleast one accout owner."); $this->response($message, 200); } if ($companyName) { $users = $this->usermodel->getBy("id", $userId); if ($users) { $user = $users[0]; if ($user->firstName == $user->email) { $orgdata = array('contactFirstName' => $this->nullIfEmpty($this->put('firstName')), 'contactLastName' => $this->nullIfEmpty($this->put('lastName')), 'contactDesignation' => $this->nullIfEmpty($this->put('designation')), 'contactPhone' => $this->nullIfEmpty($this->put('phone')), 'city' => $this->nullIfEmpty($this->put('city')), 'state' => $this->nullIfEmpty($this->put('state')), 'country' => $this->nullIfEmpty($this->put('country')), 'zip' => $this->nullIfEmpty($this->put('zip')), 'modifiedAt' => gmdate('Y-m-d H:i:s')); $this->db->where('id', $organizationId); $this->db->update('organization', $orgdata); } } } if ($this->usermodel->update($data)) { if ($companyName) { $this->organizationmodel->updateCompanyName($companyName, $organizationId); } $message = array('email' => $this->put('email'), 'status' => 'success', 'message' => sprintf($this->lang->line('user.edit'))); } else { $message = array('email' => $this->put('email'), 'status' => 'error', 'message' => $this->lang->line('user.edit.error')); } $this->response($message, 200); } public function listAcitivites_get() { if (!$this->get('userId')) { $this->response(array('error' => 'UserId is required for update'), 400); } $userId = $this->get('userId'); $resultActivities = $this->usermodel->getActivitiesByUser($userId); $this->response($resultActivities, 200); } public function switchAccountOwner($userId, $username, $userEmail, $orgId) { $toUserId = $userId; $toUsername = $username; $toEmail = $userEmail; $fromUserId = $this->session->userdata('userId'); $fromUsername = $this->session->userdata('username'); $fromEmail = $this->session->userdata('userEmail'); $this->usermodel->changeAccountOwner($fromUserId, $toUserId, $toEmail, $orgId); $orgDet = $this->organizationmodel->getById($orgId); if ($orgDet) { $orgFname = $orgDet[0]->contactFirstName; $orgLname = $orgDet[0]->contactLastName; } $alertMsg = "The account owner for " . $orgDet[0]->displayName . " has been changed from " . $fromUsername . " to " . $toUsername . ".
                       " . $toUsername . " is now authorized to create and manage users. 
                        The Role for " . $fromUsername . " has been automatically set to Manager. " . $fromUsername . " will not be able to add users.

                        To modify roles (and hence permission) please visit user management in HelloLeads.

                         <a href='https://app.helloleads.io/'>Login Now</a>
                        "; $alertMaildata = array( "fromId" => "alert@helloleads.io", "toId" => $toEmail, "cc" => $fromEmail, "subject" => "HelloLeads – Change of Account Owner", "message" => $alertMsg, "date" => gmdate('Y-m-d H:i:s') ); $alertMailId = $this->emailqueuemodel->create($alertMaildata); } public function changepassword_put() { AuthManager::authenticate(); $userId = $this->put('userId'); $oldpassword = $this->put('oldpassword'); $newpassword = $this->put('newpassword'); log_message("debug", "received userid " . $userId . " old password " . $oldpassword . " new password " . $newpassword); $result = $this->usermodel->getUser($userId); if ($result) { $user = $result[0]; log_message("debug", "We got the user details...."); $userpass = $user->password_hash; if (PassHash::check_password($userpass, $oldpassword)) { log_message("debug", "Going to updatge new password [" . $newpassword . "]"); $newPasswordhash = PassHash::hash($newpassword); log_message("debug", "new password hash [" . $newPasswordhash . "]"); $this->usermodel->updatePassword($userId, $newPasswordhash); log_message("debug", "Updated the password..."); $message = array('email' => $this->put('email'), 'status' => 'success', 'message' => "The password has been changed successfully"); } else { $message = array('email' => $this->put('email'), 'status' => 'error', 'message' => $this->lang->line('user.edit.errorPass')); } $this->response($message, 200); } else { log_message("debug", "Could not find user for Id " . $userId); } } public function user_delete($id = NULL) { AuthManager::authenticate(); if ($id == NULL) { $message = array('error' => 'Missing delete data: id'); $this->response($message, 400); } else { $this->db->where('id', $id); $userOrg = $this->get('visitor')->result(); if (!empty($userOrg)) { $this->db->where('id', $userOrg[0]->organizationId); $orgDet = $this->db->get('organization')->result(); if (!empty($orgDet)) { if ($orgDet[0]->accountType === 'FR') { $message = array( 'status' => 'error', 'message' => "We appreciate your extensive use of HelloLeads for your business. Please subscribe to paid plan to continue to enjoy the full power of HelloLeads."); $this->response($message, 200); } } } $this->usermodel->delete($id); $message = array('userId' => $id, 'status' => 'success', 'message' => sprintf($this->lang->line('user.delete'))); $this->response($message, 200); } } public function register_post() { if (!$this->post('email') || !$this->post('firstName') || !$this->post('password') || !$this->post('organizationName')) { $this->response(array('error' => 'Missing data: cannot proceed further'), 400); } $orgName = $this->post('organizationName'); $firstName = $this->post('firstName'); $email = $this->post('email'); $password = $this->post('password'); $password_hash = PassHash::hash($password); $existingUser = $this->usermodel->getUserByEmail($email); if ($existingUser) { $message = array( 'status' => 'error', 'message' => 'This email id already exists.. please try again'); $this->response($message, 404); return; } log_message("debug", "not an existing user........... lets add her"); $organizationName = $orgName; $org = $this->organizationmodel->getOrganizationByName($orgName); if ($org) { $orgName = $orgName . "-" . $email; log_message("debug", "yippe - neworg name is [" . $orgName . "] lets create the organization and also the user...."); } $orgData = array( 'name' => $orgName, 'displayName' => $organizationName, 'contactFirstName' => $this->nullIfEmpty($firstName), 'contactEmail' => $this->nullIfEmpty($email) ); $orgId = $this->organizationmodel->create($orgData); $this->organizationmodel->createInitialCredit($orgId, '50'); $userData = array( 'firstName' => $this->nullIfEmpty($firstName), 'lastName' => $this->nullIfEmpty($firstName), 'email' => $this->nullIfEmpty($email), 'password_hash' => $this->nullIfEmpty($password_hash), 'organizationId' => $this->nullIfEmpty($orgId) ); $userId = $this->usermodel->create($userData); if ($userId > 0) { $message = array('id' => $userId, 'organizationId' => $orgId, 'status' => 'success', 'message' => sprintf($this->lang->line('user.add'), $userId, $email)); $this->response($message, 200); } $this->response([], 404); } public function getwelcomeEmail($username, $password, $name, $cName, $role) { $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"/>
    <style type="text/css">
      body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0; padding: 0; }
      img { outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
      body { color: #65646a; font-family: "Helvetica Neue", HelveticaNeue, helvetica, arial, sans-serif; }
      h1 a:active { color: #4a8db8 !important; }
      h2 a:active { color: #4a8db8 !important; }
      h3 a:active { color: #4a8db8 !important; }
      h4 a:active { color: #4a8db8 !important; }
      h5 a:active { color: #4a8db8 !important; }
      h6 a:active { color: #4a8db8 !important; }
      h1 a:visited { color: #4a8db8 !important; }
      h2 a:visited { color: #4a8db8 !important; }
      h3 a:visited { color: #4a8db8 !important; }
      h4 a:visited { color: #4a8db8 !important; }
      h5 a:visited { color: #4a8db8 !important; }
      h6 a:visited { color: #4a8db8 !important; }
      a:visited { color: #4a8db8; }
      a:hover { color: #2dbe60; text-decoration: underline; }
      .u-headingMobileAnchor a {text-decoration: none !important; color: #2dbe60 !important; pointer-events: none !important; cursor: default !important;}
      .u-bodyMobileAnchor a {text-decoration: none !important; color: #65646a !important; pointer-events: none !important; cursor: default !important;}
      .footer a:hover { text-decoration: none; }
      .footer-small a:hover { text-decoration: underline; }
      body { background-color: #f3f3f3; }
      .ExternalClass { width: 100%; }
      .ExternalClass { line-height: 100%; }
      table[id="inception"] .Btn-anchor:hover { color: #fff !important; }
      table[id="inception"] .Btn-anchor:visited { color: #fff !important; }
      @media screen and (min-width: 579px) {
        table[id="inception"] .l-inception-point { width: 578px !important; }
      }
      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        table[id="inception"] a[href^="tel"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        table[id="inception"] a[href^="sms"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        .mobile-link table[id="inception"] a[href^="tel"] { text-decoration: default; color: #65646a !important; pointer-events: auto; cursor: default; }
        .mobile-link table[id="inception"] a[href^="sms"] { text-decoration: default; color: #65646a !important; pointer-events: auto; cursor: default; }
        table[id="inception"] .u-headingMobileAnchor a {text-decoration: none !important; color: #2dbe60 !important; pointer-events: none !important; cursor: default !important;}
        table[id="inception"] .u-bodyMobileAnchor a {text-decoration: none !important; color: #65646a !important; pointer-events: none !important; cursor: default !important;}
      }
      @media only screen and (max-device-width: 578px), only screen and (max-width: 578px) {
        table[id="inception"] .Branding_positive-container { padding: 14px 0 0 16px !important; }
        table[id="inception"] .Btn-anchor { display: block !important; }
        table[id="inception"] .Row_small { padding: 0 !important; }
        table[id="inception"] .Row_medium-top { padding-top: 16px !important; }
        table[id="inception"] .Avatar-img { width: 48px !important; height: 48px !important; }
        table[id="inception"] { width: 100% !important; }
        table[id="inception"] .l-outie { padding: 0px !important; }
        table[id="inception"] .l-inception-point { width: 100% !important; }
        table[id="inception"] .l-footer { width: 100% !important; }
        table[id="inception"] .l-wrap { width: 100% !important; }
        table[id="inception"] .l-full-bleed { width: 100% !important; }
        table[id="inception"] .media-obj { width: 100% !important; }
        table[id="inception"] .media-content { width: 100% !important; }
        table[id="inception"] .btn { width: 100% !important; }
        table[id="inception"] .btn td { width: 100% !important; }
        table[id="inception"] .l-inception-point { border-right: none !important; border-left: none !important; }
        table[id="inception"] .l-full-bleed img { width: 100% !important; height: auto !important; }
        table[id="inception"] .l-pad > tbody > tr > td { padding: 16px !important; }
        table[id="inception"] .l-split { width: 100% !important; }
        table[id="inception"] .l-split + .l-split { margin-top: 32px !important; }
        table[id="inception"] .ill-notebook { width: 100% !important; height: 96px !important; }
        table[id="inception"] .footer { padding-right: 16px !important; padding-left: 16px !important; }
        table[id="inception"] .btn span { display: block !important; }
        table[id="inception"] a[href^="tel"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        table[id="inception"] a[href^="sms"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        .mobile-link table[id="inception"] a[href^="tel"] { text-decoration: default; color: #4a8db8 !important; pointer-events: auto; cursor: default; }
        .mobile-link table[id="inception"] a[href^="sms"] { text-decoration: default; color: #4a8db8 !important; pointer-events: auto; cursor: default; }
        table[id="inception"] .u-headingMobileAnchor a {text-decoration: none !important; color: #2dbe60 !important; pointer-events: none !important; cursor: default !important;}
        table[id="inception"] .u-bodyMobileAnchor a {text-decoration: none !important; color: #65646a !important; pointer-events: none !important; cursor: default !important;}
        table[id="inception"] .Rule { display: none !important; }
        table[id="inception"] .l-pad { text-align: left !important; }
      }
	  <!--
        /* Font Definitions */
        
        @font-face {
            font-family: Helvetica;
            panose-1: 2 11 6 4 2 2 2 2 2 4;
        }
        
        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }
        
        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }
        /* Style Definitions */
        
        p.MsoNormal,
        li.MsoNormal,
        div.MsoNormal {
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Times New Roman", serif;
        }
        
        a:link,
        span.MsoHyperlink {
            mso-style-priority: 99;
            color: blue;
            text-decoration: underline;
        }
        
        a:visited,
        span.MsoHyperlinkFollowed {
            mso-style-priority: 99;
            color: purple;
            text-decoration: underline;
        }
        
        p {
            mso-style-priority: 99;
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
        }
        
        p.externalclass,
        li.externalclass,
        div.externalclass {
            mso-style-name: externalclass;
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
        }
        
        span.header {
            mso-style-name: header;
        }
        
        span.sidecomp {
            mso-style-name: sidecomp;
        }
        
        span.facebook {
            mso-style-name: facebook;
        }
        
        span.pintrest {
            mso-style-name: pintrest;
        }
        
        span.twitter {
            mso-style-name: twitter;
        }
        
        span.EmailStyle24 {
            mso-style-type: personal-reply;
            font-family: "Calibri", sans-serif;
            color: #1F497D;
        }
        
        .MsoChpDefault {
            mso-style-type: export-only;
            font-size: 10.0pt;
        }
        
        @page WordSection1 {
            size: 8.5in 11.0in;
            margin: 1.0in 1.0in 1.0in 1.0in;
        }
        
        div.WordSection1 {
            page: WordSection1;
        }-->
    </style>
  </head>
  <body bgcolor="#f3f3f3" style="width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #65646a; font-family: "Helvetica Neue", HelveticaNeue, helvetica, arial, sans-serif; background: #f3f3f3; margin: 0; padding: 0;">
    <table id="inception" align="center" bgcolor="#f3f3f3" cellpadding="0" cellspacing="0" style="width: 100% !important; line-height: 100% !important; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #f3f3f3; margin: 0; padding: 0;">
      <tr>
        <td id="outie" width="100%" class="l-outie" style="width: 100%; border-collapse: collapse; padding: 32px 0;"><!--[if (gte mso 9)|(IE)]><table width="576" align="center" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; "><tbody><tr><td><![endif]-->
          <table align="center" bgcolor="white" width="100%" cellpadding="0" cellspacing="0" class="l-inception-point" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; max-width: 576px; margin: 0 auto;">
            <tbody>
              <tr>
                <td class="l-wrap BrandStripe" style="border-collapse: collapse; background: #fff; border-color: #D8701D #e7e4e3 #e7e4e3; border-style: solid; border-width: 3px 1px 1px;">
                  <table width="100%" bgcolor="white" cellpadding="0px" cellspacing="0" class="l-banner" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; max-width: 100%; width: 100%;">
                    <tbody>
                      <tr>
                        <td style="border-collapse: collapse;">
                          <!-- begin #branding-->
                          <table id="branding" align="left" cellpadding="0" cellspacing="0" class="Branding_positive" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; ">
                            <tbody>
                              <tr>
                                <td id="logo" class="Branding_positive-container" style="border-collapse: collapse; padding: 19px 0 0 22px;">
                                  <img alt="HelloLeads" border="0" height="40" src="https://app.helloleads.io/images/emaillogo.png" width="164" class="image-fix Branding_positive-logo" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; color: #2dbe60; border: none;" />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- .l-banner-->
                  <table id="main" width="100%" cellpadding="0" cellspacing="0" class="ll-inception-point" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; max-width: 100%; width: 100%;">
                    <tbody>
                      <tr>
                        <td style="border-collapse: collapse;">
                          <!-- messagingInvitationFollowUp -->
						  <hr>
<table width="100%" cellpadding="32" cellspacing="0" class="l-pad" style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
  <tbody>
    <tr>
      <td style="border-collapse: collapse;">
        <table style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
          <tbody>
		  		 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Hello,<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">You have been added by ' . $name . ' to HelloLeads<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Your HelloLeads account has been verified successfully and activated.<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Please find below Login Credentials for your HelloLeads Account <o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif"><b>Username: ' . $username . '</b><o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif"><b>Password: ' . $password . '</b><o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif"><b>Role: ' . $role . '</b><o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">To know about your permissions in the HelloLeads account <a href="https://www.helloleads.io/blog/recent_updates/roles&permission.php">click here.</a> <o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">We are confident HelloLeads will transform the way you track and manage your leads.For any questions please feel free to contact our <a href="https://www.helloleads.io/support.php" target="_blank" >support team.</a><o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
            	
			<tr>
			<p></p>
			</tr>
           
            
            <!-- begin button-->
            <tr>
              <td class="Row Row_medium-top Row_small" style="border-collapse: collapse; padding-bottom: 8px; padding-top: 24px;">
                <div class="Btn"><!--[if mso]> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://app.helloleads.io/index.php/app/account/login" style="height:32px;v-text-anchor:middle;width:200px;" arcsize="7%" strokecolor="#D8701D" fillcolor="#D8701D"> <w:anchorlock/> <center style="color:#ffffff;font-family:sans-serif;font-size:13px;font-weight:bold;">Login Now</center> </v:roundrect> <![endif]--><a href="https://app.helloleads.io/index.php/app/account/login" class="Btn-anchor" style="-webkit-text-size-adjust: none; background-color: #D8701D; border: 1px solid #D8701D; border-radius: 2px; color: #fff; display: inline-block; font-family: sans-serif; font-size: 15px; font-weight: bold; line-height: 32px; mso-hide: all; padding: 0 30px 0 30px; text-align: center; text-decoration: none;"><span class="Btn-span" style="color: #fff;">Login Now</span></a></div>
                
              </td>
               
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<!-- .l-pad-->
<table cellpadding="32" width="100%" cellspacing="0" class="l-pad" style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
  <tbody>
    <tr>
      <td align="center" bgcolor="#fbfbfb" class="Box" style="background-color: #fbfbfb; border-collapse: collapse; border-top: 1px solid #e7e4e3;">
        <p style="color: #65646a; font-family: "Helvetica Neue", HelveticaNeue, helvetica, arial, sans-serif; font-size: 14px; line-height: 0px; margin: 0 0 1px 0;">Powered by <a href="http://helloleads.io/" target="_blank"> HelloLeads.io </a></p>
      </td>
    </tr>
  </tbody>
</table>

                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- .ll-inception-point-->
                </td>
                <!-- .l-wrap-->
              </tr>
            </tbody>
          </table>
          <!-- .l-footer-->
           <table class="l-inception-point l-footer footer" width="100%" cellpadding="20" align="center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; max-width: 578px; width: 100%; background: url((http://www.helloleads.io/email/mail-drop-shadow.png) 50% -2px transparent scroll no-repeat;margin: 0 auto 20px auto;text-align: center;">
            <tr>
              <td>
                <p style="color: #747778;font-size: 11px;line-height: 15px;text-align: center;margin-top: 0;margin-bottom: 0;margin-right: 0;margin-left: 0;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;">For any questions, contact us at <a href="mailto:support@helloleads.io" target="_top" style="color: #4A8DB8; text-decoration: none;">support@helloleads.io</a> or chat with us.</p>

                <p style="color: #747778;font-size: 11px;line-height: 15px;text-align: center;margin-top: 0;margin-bottom: 0;margin-right: 0;margin-left: 0;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;">HelloLeads, 113 Barksdale Professional Center, Newark DE 19711, USA, Tel / Fax: 302-747-5836</p>

              </td>
            </tr>
          </table><!--end .footer-->

          <!-- .l-inception-point--><!--[if (gte mso 9)|(IE)]></td></tr></tbody></table> <![endif]-->
        </td>
      </tr>
    </table>
      

  </body>
</html>
'; return $message; } public function getwelcomeEmailREGI($username, $password, $name, $cName) { $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"/>
    <style type="text/css">
      body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0; padding: 0; }
      img { outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
      body { color: #65646a; font-family: "Helvetica Neue", HelveticaNeue, helvetica, arial, sans-serif; }
      h1 a:active { color: #4a8db8 !important; }
      h2 a:active { color: #4a8db8 !important; }
      h3 a:active { color: #4a8db8 !important; }
      h4 a:active { color: #4a8db8 !important; }
      h5 a:active { color: #4a8db8 !important; }
      h6 a:active { color: #4a8db8 !important; }
      h1 a:visited { color: #4a8db8 !important; }
      h2 a:visited { color: #4a8db8 !important; }
      h3 a:visited { color: #4a8db8 !important; }
      h4 a:visited { color: #4a8db8 !important; }
      h5 a:visited { color: #4a8db8 !important; }
      h6 a:visited { color: #4a8db8 !important; }
      a:visited { color: #4a8db8; }
      a:hover { color: #2dbe60; text-decoration: underline; }
      .u-headingMobileAnchor a {text-decoration: none !important; color: #2dbe60 !important; pointer-events: none !important; cursor: default !important;}
      .u-bodyMobileAnchor a {text-decoration: none !important; color: #65646a !important; pointer-events: none !important; cursor: default !important;}
      .footer a:hover { text-decoration: none; }
      .footer-small a:hover { text-decoration: underline; }
      body { background-color: #f3f3f3; }
      .ExternalClass { width: 100%; }
      .ExternalClass { line-height: 100%; }
      table[id="inception"] .Btn-anchor:hover { color: #fff !important; }
      table[id="inception"] .Btn-anchor:visited { color: #fff !important; }
      @media screen and (min-width: 579px) {
        table[id="inception"] .l-inception-point { width: 578px !important; }
      }
      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        table[id="inception"] a[href^="tel"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        table[id="inception"] a[href^="sms"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        .mobile-link table[id="inception"] a[href^="tel"] { text-decoration: default; color: #65646a !important; pointer-events: auto; cursor: default; }
        .mobile-link table[id="inception"] a[href^="sms"] { text-decoration: default; color: #65646a !important; pointer-events: auto; cursor: default; }
        table[id="inception"] .u-headingMobileAnchor a {text-decoration: none !important; color: #2dbe60 !important; pointer-events: none !important; cursor: default !important;}
        table[id="inception"] .u-bodyMobileAnchor a {text-decoration: none !important; color: #65646a !important; pointer-events: none !important; cursor: default !important;}
      }
      @media only screen and (max-device-width: 578px), only screen and (max-width: 578px) {
        table[id="inception"] .Branding_positive-container { padding: 14px 0 0 16px !important; }
        table[id="inception"] .Btn-anchor { display: block !important; }
        table[id="inception"] .Row_small { padding: 0 !important; }
        table[id="inception"] .Row_medium-top { padding-top: 16px !important; }
        table[id="inception"] .Avatar-img { width: 48px !important; height: 48px !important; }
        table[id="inception"] { width: 100% !important; }
        table[id="inception"] .l-outie { padding: 0px !important; }
        table[id="inception"] .l-inception-point { width: 100% !important; }
        table[id="inception"] .l-footer { width: 100% !important; }
        table[id="inception"] .l-wrap { width: 100% !important; }
        table[id="inception"] .l-full-bleed { width: 100% !important; }
        table[id="inception"] .media-obj { width: 100% !important; }
        table[id="inception"] .media-content { width: 100% !important; }
        table[id="inception"] .btn { width: 100% !important; }
        table[id="inception"] .btn td { width: 100% !important; }
        table[id="inception"] .l-inception-point { border-right: none !important; border-left: none !important; }
        table[id="inception"] .l-full-bleed img { width: 100% !important; height: auto !important; }
        table[id="inception"] .l-pad > tbody > tr > td { padding: 16px !important; }
        table[id="inception"] .l-split { width: 100% !important; }
        table[id="inception"] .l-split + .l-split { margin-top: 32px !important; }
        table[id="inception"] .ill-notebook { width: 100% !important; height: 96px !important; }
        table[id="inception"] .footer { padding-right: 16px !important; padding-left: 16px !important; }
        table[id="inception"] .btn span { display: block !important; }
        table[id="inception"] a[href^="tel"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        table[id="inception"] a[href^="sms"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        .mobile-link table[id="inception"] a[href^="tel"] { text-decoration: default; color: #4a8db8 !important; pointer-events: auto; cursor: default; }
        .mobile-link table[id="inception"] a[href^="sms"] { text-decoration: default; color: #4a8db8 !important; pointer-events: auto; cursor: default; }
        table[id="inception"] .u-headingMobileAnchor a {text-decoration: none !important; color: #2dbe60 !important; pointer-events: none !important; cursor: default !important;}
        table[id="inception"] .u-bodyMobileAnchor a {text-decoration: none !important; color: #65646a !important; pointer-events: none !important; cursor: default !important;}
        table[id="inception"] .Rule { display: none !important; }
        table[id="inception"] .l-pad { text-align: left !important; }
      }
	  <!--
        /* Font Definitions */
        
        @font-face {
            font-family: Helvetica;
            panose-1: 2 11 6 4 2 2 2 2 2 4;
        }
        
        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }
        
        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }
        /* Style Definitions */
        
        p.MsoNormal,
        li.MsoNormal,
        div.MsoNormal {
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Times New Roman", serif;
        }
        
        a:link,
        span.MsoHyperlink {
            mso-style-priority: 99;
            color: blue;
            text-decoration: underline;
        }
        
        a:visited,
        span.MsoHyperlinkFollowed {
            mso-style-priority: 99;
            color: purple;
            text-decoration: underline;
        }
        
        p {
            mso-style-priority: 99;
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
        }
        
        p.externalclass,
        li.externalclass,
        div.externalclass {
            mso-style-name: externalclass;
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
        }
        
        span.header {
            mso-style-name: header;
        }
        
        span.sidecomp {
            mso-style-name: sidecomp;
        }
        
        span.facebook {
            mso-style-name: facebook;
        }
        
        span.pintrest {
            mso-style-name: pintrest;
        }
        
        span.twitter {
            mso-style-name: twitter;
        }
        
        span.EmailStyle24 {
            mso-style-type: personal-reply;
            font-family: "Calibri", sans-serif;
            color: #1F497D;
        }
        
        .MsoChpDefault {
            mso-style-type: export-only;
            font-size: 10.0pt;
        }
        
        @page WordSection1 {
            size: 8.5in 11.0in;
            margin: 1.0in 1.0in 1.0in 1.0in;
        }
        
        div.WordSection1 {
            page: WordSection1;
        }-->
    </style>
  </head>
  <body bgcolor="#f3f3f3" style="width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #65646a; font-family: "Helvetica Neue", HelveticaNeue, helvetica, arial, sans-serif; background: #f3f3f3; margin: 0; padding: 0;">
    <table id="inception" align="center" bgcolor="#f3f3f3" cellpadding="0" cellspacing="0" style="width: 100% !important; line-height: 100% !important; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #f3f3f3; margin: 0; padding: 0;">
      <tr>
        <td id="outie" width="100%" class="l-outie" style="width: 100%; border-collapse: collapse; padding: 32px 0;"><!--[if (gte mso 9)|(IE)]><table width="576" align="center" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; "><tbody><tr><td><![endif]-->
          <table align="center" bgcolor="white" width="100%" cellpadding="0" cellspacing="0" class="l-inception-point" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; max-width: 576px; margin: 0 auto;">
            <tbody>
              <tr>
                <td class="l-wrap BrandStripe" style="border-collapse: collapse; background: #fff; border-color: #D8701D #e7e4e3 #e7e4e3; border-style: solid; border-width: 3px 1px 1px;">
                  <table width="100%" bgcolor="white" cellpadding="0px" cellspacing="0" class="l-banner" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; max-width: 100%; width: 100%;">
                    <tbody>
                      <tr>
                        <td style="border-collapse: collapse;">
                          <!-- begin #branding-->
                          <table id="branding" align="left" cellpadding="0" cellspacing="0" class="Branding_positive" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; ">
                            <tbody>
                              <tr>
                                <td id="logo" class="Branding_positive-container" style="border-collapse: collapse; padding: 19px 0 0 22px;">
                                  <img alt="HelloLeads" border="0" height="40" src="https://app.helloleads.io/images/emaillogo.png" width="164" class="image-fix Branding_positive-logo" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; color: #2dbe60; border: none;" />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- .l-banner-->
                  <table id="main" width="100%" cellpadding="0" cellspacing="0" class="ll-inception-point" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; max-width: 100%; width: 100%;">
                    <tbody>
                      <tr>
                        <td style="border-collapse: collapse;">
                          <!-- messagingInvitationFollowUp -->
						  <hr>
<table width="100%" cellpadding="32" cellspacing="0" class="l-pad" style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
  <tbody>
    <tr>
      <td style="border-collapse: collapse;">
        <table style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
          <tbody>
		  		 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Hello,<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">You have been added to HelloLeads as a Regi user by ' . $name . ' (Account manager), ' . $cName . '.<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        
                                                                                        <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">HelloLeads Regi - Application helps you to simplify the event registration process by capturing the lead details through Form/ ID/ QR.<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					
                                                                                        <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Your HelloLeads account has been verified successfully and activated.<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                         <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Please find below Login Credentials for your HelloLeads Account <o:p></o:p></span></p>
                                                                                            </td>
                                                                                         </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Username: ' . $username . '<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Password: ' . $password . '<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">We are confident HelloLeads will transform the way you track and manage your leads.For any questions please feel free to contact our support team.<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
            	
			<tr>
			<p></p>
			</tr>
           
            
            <!-- begin button-->
            <tr>
              <td class="Row Row_medium-top Row_small" style="border-collapse: collapse; padding-bottom: 8px; padding-top: 24px;">
                <div class="Btn"><!--[if mso]> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://app.helloleads.io/index.php/app/account/login" style="height:32px;v-text-anchor:middle;width:200px;" arcsize="7%" strokecolor="#D8701D" fillcolor="#D8701D"> <w:anchorlock/> <center style="color:#ffffff;font-family:sans-serif;font-size:13px;font-weight:bold;">Login Now</center> </v:roundrect> <![endif]--><a href="https://app.helloleads.io/index.php/app/account/login" class="Btn-anchor" style="-webkit-text-size-adjust: none; background-color: #D8701D; border: 1px solid #D8701D; border-radius: 2px; color: #fff; display: inline-block; font-family: sans-serif; font-size: 15px; font-weight: bold; line-height: 32px; mso-hide: all; padding: 0 30px 0 30px; text-align: center; text-decoration: none;"><span class="Btn-span" style="color: #fff;">Login Now</span></a></div>
                
              </td>
              
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<!-- .l-pad-->
<table cellpadding="32" width="100%" cellspacing="0" class="l-pad" style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
  <tbody>
    <tr>
      <td align="center" bgcolor="#fbfbfb" class="Box" style="background-color: #fbfbfb; border-collapse: collapse; border-top: 1px solid #e7e4e3;">
        <p style="color: #65646a; font-family: "Helvetica Neue", HelveticaNeue, helvetica, arial, sans-serif; font-size: 14px; line-height: 0px; margin: 0 0 1px 0;">Powered by <a href="http://helloleads.io/" target="_blank"> HelloLeads.io </a></p>
      </td>
    </tr>
  </tbody>
</table>

                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- .ll-inception-point-->
                </td>
                <!-- .l-wrap-->
              </tr>
            </tbody>
          </table>
          <!-- .l-footer-->
           <table class="l-inception-point l-footer footer" width="100%" cellpadding="20" align="center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; max-width: 578px; width: 100%; background: url((http://www.helloleads.io/email/mail-drop-shadow.png) 50% -2px transparent scroll no-repeat;margin: 0 auto 20px auto;text-align: center;">
            <tr>
              <td>
                <p style="color: #747778;font-size: 11px;line-height: 15px;text-align: center;margin-top: 0;margin-bottom: 0;margin-right: 0;margin-left: 0;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;">For any questions, contact us at <a href="mailto:support@helloleads.io" target="_top" style="color: #4A8DB8; text-decoration: none;">support@helloleads.io</a> or chat with us.</p>

                <p style="color: #747778;font-size: 11px;line-height: 15px;text-align: center;margin-top: 0;margin-bottom: 0;margin-right: 0;margin-left: 0;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;">HelloLeads, 113 Barksdale Professional Center, Newark DE 19711, USA, Tel / Fax: 302-747-5836</p>

              </td>
            </tr>
          </table><!--end .footer-->

          <!-- .l-inception-point--><!--[if (gte mso 9)|(IE)]></td></tr></tbody></table> <![endif]-->
        </td>
      </tr>
    </table>
      

  </body>
</html>
'; return $message; } public function getwelcomeEmailLW($username, $password, $name, $cName) { $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"/>
    <style type="text/css">
      body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0; padding: 0; }
      img { outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
      body { color: #65646a; font-family: "Helvetica Neue", HelveticaNeue, helvetica, arial, sans-serif; }
      h1 a:active { color: #4a8db8 !important; }
      h2 a:active { color: #4a8db8 !important; }
      h3 a:active { color: #4a8db8 !important; }
      h4 a:active { color: #4a8db8 !important; }
      h5 a:active { color: #4a8db8 !important; }
      h6 a:active { color: #4a8db8 !important; }
      h1 a:visited { color: #4a8db8 !important; }
      h2 a:visited { color: #4a8db8 !important; }
      h3 a:visited { color: #4a8db8 !important; }
      h4 a:visited { color: #4a8db8 !important; }
      h5 a:visited { color: #4a8db8 !important; }
      h6 a:visited { color: #4a8db8 !important; }
      a:visited { color: #4a8db8; }
      a:hover { color: #2dbe60; text-decoration: underline; }
      .u-headingMobileAnchor a {text-decoration: none !important; color: #2dbe60 !important; pointer-events: none !important; cursor: default !important;}
      .u-bodyMobileAnchor a {text-decoration: none !important; color: #65646a !important; pointer-events: none !important; cursor: default !important;}
      .footer a:hover { text-decoration: none; }
      .footer-small a:hover { text-decoration: underline; }
      body { background-color: #f3f3f3; }
      .ExternalClass { width: 100%; }
      .ExternalClass { line-height: 100%; }
      table[id="inception"] .Btn-anchor:hover { color: #fff !important; }
      table[id="inception"] .Btn-anchor:visited { color: #fff !important; }
      @media screen and (min-width: 579px) {
        table[id="inception"] .l-inception-point { width: 578px !important; }
      }
      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
        table[id="inception"] a[href^="tel"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        table[id="inception"] a[href^="sms"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        .mobile-link table[id="inception"] a[href^="tel"] { text-decoration: default; color: #65646a !important; pointer-events: auto; cursor: default; }
        .mobile-link table[id="inception"] a[href^="sms"] { text-decoration: default; color: #65646a !important; pointer-events: auto; cursor: default; }
        table[id="inception"] .u-headingMobileAnchor a {text-decoration: none !important; color: #2dbe60 !important; pointer-events: none !important; cursor: default !important;}
        table[id="inception"] .u-bodyMobileAnchor a {text-decoration: none !important; color: #65646a !important; pointer-events: none !important; cursor: default !important;}
      }
      @media only screen and (max-device-width: 578px), only screen and (max-width: 578px) {
        table[id="inception"] .Branding_positive-container { padding: 14px 0 0 16px !important; }
        table[id="inception"] .Btn-anchor { display: block !important; }
        table[id="inception"] .Row_small { padding: 0 !important; }
        table[id="inception"] .Row_medium-top { padding-top: 16px !important; }
        table[id="inception"] .Avatar-img { width: 48px !important; height: 48px !important; }
        table[id="inception"] { width: 100% !important; }
        table[id="inception"] .l-outie { padding: 0px !important; }
        table[id="inception"] .l-inception-point { width: 100% !important; }
        table[id="inception"] .l-footer { width: 100% !important; }
        table[id="inception"] .l-wrap { width: 100% !important; }
        table[id="inception"] .l-full-bleed { width: 100% !important; }
        table[id="inception"] .media-obj { width: 100% !important; }
        table[id="inception"] .media-content { width: 100% !important; }
        table[id="inception"] .btn { width: 100% !important; }
        table[id="inception"] .btn td { width: 100% !important; }
        table[id="inception"] .l-inception-point { border-right: none !important; border-left: none !important; }
        table[id="inception"] .l-full-bleed img { width: 100% !important; height: auto !important; }
        table[id="inception"] .l-pad > tbody > tr > td { padding: 16px !important; }
        table[id="inception"] .l-split { width: 100% !important; }
        table[id="inception"] .l-split + .l-split { margin-top: 32px !important; }
        table[id="inception"] .ill-notebook { width: 100% !important; height: 96px !important; }
        table[id="inception"] .footer { padding-right: 16px !important; padding-left: 16px !important; }
        table[id="inception"] .btn span { display: block !important; }
        table[id="inception"] a[href^="tel"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        table[id="inception"] a[href^="sms"] { text-decoration: none; color: #65646a; pointer-events: none; cursor: default; }
        .mobile-link table[id="inception"] a[href^="tel"] { text-decoration: default; color: #4a8db8 !important; pointer-events: auto; cursor: default; }
        .mobile-link table[id="inception"] a[href^="sms"] { text-decoration: default; color: #4a8db8 !important; pointer-events: auto; cursor: default; }
        table[id="inception"] .u-headingMobileAnchor a {text-decoration: none !important; color: #2dbe60 !important; pointer-events: none !important; cursor: default !important;}
        table[id="inception"] .u-bodyMobileAnchor a {text-decoration: none !important; color: #65646a !important; pointer-events: none !important; cursor: default !important;}
        table[id="inception"] .Rule { display: none !important; }
        table[id="inception"] .l-pad { text-align: left !important; }
      }
	  <!--
        /* Font Definitions */
        
        @font-face {
            font-family: Helvetica;
            panose-1: 2 11 6 4 2 2 2 2 2 4;
        }
        
        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }
        
        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }
        /* Style Definitions */
        
        p.MsoNormal,
        li.MsoNormal,
        div.MsoNormal {
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Times New Roman", serif;
        }
        
        a:link,
        span.MsoHyperlink {
            mso-style-priority: 99;
            color: blue;
            text-decoration: underline;
        }
        
        a:visited,
        span.MsoHyperlinkFollowed {
            mso-style-priority: 99;
            color: purple;
            text-decoration: underline;
        }
        
        p {
            mso-style-priority: 99;
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
        }
        
        p.externalclass,
        li.externalclass,
        div.externalclass {
            mso-style-name: externalclass;
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            font-family: "Times New Roman", serif;
        }
        
        span.header {
            mso-style-name: header;
        }
        
        span.sidecomp {
            mso-style-name: sidecomp;
        }
        
        span.facebook {
            mso-style-name: facebook;
        }
        
        span.pintrest {
            mso-style-name: pintrest;
        }
        
        span.twitter {
            mso-style-name: twitter;
        }
        
        span.EmailStyle24 {
            mso-style-type: personal-reply;
            font-family: "Calibri", sans-serif;
            color: #1F497D;
        }
        
        .MsoChpDefault {
            mso-style-type: export-only;
            font-size: 10.0pt;
        }
        
        @page WordSection1 {
            size: 8.5in 11.0in;
            margin: 1.0in 1.0in 1.0in 1.0in;
        }
        
        div.WordSection1 {
            page: WordSection1;
        }-->
    </style>
  </head>
  <body bgcolor="#f3f3f3" style="width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #65646a; font-family: "Helvetica Neue", HelveticaNeue, helvetica, arial, sans-serif; background: #f3f3f3; margin: 0; padding: 0;">
    <table id="inception" align="center" bgcolor="#f3f3f3" cellpadding="0" cellspacing="0" style="width: 100% !important; line-height: 100% !important; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #f3f3f3; margin: 0; padding: 0;">
      <tr>
        <td id="outie" width="100%" class="l-outie" style="width: 100%; border-collapse: collapse; padding: 32px 0;"><!--[if (gte mso 9)|(IE)]><table width="576" align="center" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; "><tbody><tr><td><![endif]-->
          <table align="center" bgcolor="white" width="100%" cellpadding="0" cellspacing="0" class="l-inception-point" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; max-width: 576px; margin: 0 auto;">
            <tbody>
              <tr>
                <td class="l-wrap BrandStripe" style="border-collapse: collapse; background: #fff; border-color: #D8701D #e7e4e3 #e7e4e3; border-style: solid; border-width: 3px 1px 1px;">
                  <table width="100%" bgcolor="white" cellpadding="0px" cellspacing="0" class="l-banner" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; max-width: 100%; width: 100%;">
                    <tbody>
                      <tr>
                        <td style="border-collapse: collapse;">
                          <!-- begin #branding-->
                          <table id="branding" align="left" cellpadding="0" cellspacing="0" class="Branding_positive" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; ">
                            <tbody>
                              <tr>
                                <td id="logo" class="Branding_positive-container" style="border-collapse: collapse; padding: 19px 0 0 22px;">
                                  <img alt="HelloLeads" border="0" height="40" src="https://app.helloleads.io/images/emaillogo.png" width="164" class="image-fix Branding_positive-logo" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block; color: #2dbe60; border: none;" />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- .l-banner-->
                  <table id="main" width="100%" cellpadding="0" cellspacing="0" class="ll-inception-point" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; max-width: 100%; width: 100%;">
                    <tbody>
                      <tr>
                        <td style="border-collapse: collapse;">
                          <!-- messagingInvitationFollowUp -->
						  <hr>
<table width="100%" cellpadding="32" cellspacing="0" class="l-pad" style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
  <tbody>
    <tr>
      <td style="border-collapse: collapse;">
        <table style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
          <tbody>
		  		 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Hello,<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">You have been added to HelloLeads as a Leads Wallet user by ' . $name . ' (Account manager), ' . $cName . '.<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					
                                                                                        <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">HelloLeads Leads Wallet - Application helps you to transcribe the Business cards that is being captured by ' . $name . ' & team.<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>    

                                                                                        <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Your HelloLeads account has been verified successfully and activated.<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Please find below Login Credentials for your HelloLeads Account <o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Username: ' . $username . '<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">Password: ' . $password . '<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
					 <tr>
                                                                                            <td style="padding:0in 37.5pt 15.0pt 0.5pt">
                                                                                                <p class=MsoNormal style="line-height:18.0pt;color:#000000"><span style="font-family:"Helvetica",sans-serif">We are confident HelloLeads will transform the way you track and manage your leads.For any questions please feel free to contact our support team.<o:p></o:p></span></p>
                                                                                            </td>
                                                                                        </tr>
            	
			<tr>
			<p></p>
			</tr>
           
            
            <!-- begin button-->
            <tr>
              <td class="Row Row_medium-top Row_small" style="border-collapse: collapse; padding-bottom: 8px; padding-top: 24px;">
                <div class="Btn"><!--[if mso]> <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://app.helloleads.io/index.php/app/account/leadswalletlogin" style="height:32px;v-text-anchor:middle;width:200px;" arcsize="7%" strokecolor="#D8701D" fillcolor="#D8701D"> <w:anchorlock/> <center style="color:#ffffff;font-family:sans-serif;font-size:13px;font-weight:bold;">Login Now</center> </v:roundrect> <![endif]--><a href="https://app.helloleads.io/index.php/app/account/login" class="Btn-anchor" style="-webkit-text-size-adjust: none; background-color: #D8701D; border: 1px solid #D8701D; border-radius: 2px; color: #fff; display: inline-block; font-family: sans-serif; font-size: 15px; font-weight: bold; line-height: 32px; mso-hide: all; padding: 0 30px 0 30px; text-align: center; text-decoration: none;"><span class="Btn-span" style="color: #fff;">Login Now</span></a></div>
                
             
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
<!-- .l-pad-->
<table cellpadding="32" width="100%" cellspacing="0" class="l-pad" style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-collapse: collapse; max-width: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
  <tbody>
    <tr>
      <td align="center" bgcolor="#fbfbfb" class="Box" style="background-color: #fbfbfb; border-collapse: collapse; border-top: 1px solid #e7e4e3;">
        <p style="color: #65646a; font-family: "Helvetica Neue", HelveticaNeue, helvetica, arial, sans-serif; font-size: 14px; line-height: 0px; margin: 0 0 1px 0;">Powered by <a href="http://helloleads.io/" target="_blank"> HelloLeads.io </a></p>
      </td>
    </tr>
  </tbody>
</table>

                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <!-- .ll-inception-point-->
                </td>
                <!-- .l-wrap-->
              </tr>
            </tbody>
          </table>
          <!-- .l-footer-->
           <table class="l-inception-point l-footer footer" width="100%" cellpadding="20" align="center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; max-width: 578px; width: 100%; background: url((http://www.helloleads.io/email/mail-drop-shadow.png) 50% -2px transparent scroll no-repeat;margin: 0 auto 20px auto;text-align: center;">
            <tr>
              <td>
                <p style="color: #747778;font-size: 11px;line-height: 15px;text-align: center;margin-top: 0;margin-bottom: 0;margin-right: 0;margin-left: 0;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;">For any questions, contact us at <a href="mailto:support@helloleads.io" target="_top" style="color: #4A8DB8; text-decoration: none;">support@helloleads.io</a> or chat with us.</p>

                <p style="color: #747778;font-size: 11px;line-height: 15px;text-align: center;margin-top: 0;margin-bottom: 0;margin-right: 0;margin-left: 0;padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;">HelloLeads, 113 Barksdale Professional Center, Newark DE 19711, USA, Tel / Fax: 302-747-5836</p>

              </td>
            </tr>
          </table><!--end .footer-->

          <!-- .l-inception-point--><!--[if (gte mso 9)|(IE)]></td></tr></tbody></table> <![endif]-->
        </td>
      </tr>
    </table>
      

  </body>
</html>
'; return $message; } public function getNewRegAlertMsg($orgDetail, $companyName, $cdate) { $message = "<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid #273038;
    border-collapse: collapse;
}
th, td {
    padding: 10px;
    text-align: left;font-family:Sans-serif;
}
th {width:200px; background-color:#EF5E4A;color:white;}
p {font-size:120%;}
</style>
</head>
<!---Sub:HelloLeads - New Lead Registration Alert-->
<body style='margin:10px;'>
<h2 style='color:#273038'>HelloLeads - New Registration Alert</h2>
<table style='width:600px;'>
  <tr>
    <th>Account Number</th>
    <td>HL00" . $orgDetail['organizationId'] . "</td>
  </tr>
  <tr>
    <th>Organization Name</th>
    <td>" . $companyName . "</td>
  </tr>
<tr>
    <th>Name</th>
    <td>" . $orgDetail['firstName'] . " " . $orgDetail['lastName'] . "</td>
  </tr>
  <tr>
    <th>Designation</th>
    <td>" . $orgDetail['lastName'] . "</td>
  </tr>

<tr>
    <th>Email Address</th>
    <td>" . $orgDetail['email'] . "</td>
  </tr>
<tr>
    <th>Phone</th>
    <td>" . $orgDetail['phone'] . "</td>
  </tr><tr>
    <th>City</th>
    <td>" . $orgDetail['city'] . "</td>
  </tr>
<tr>
    <th>State</th>
    <td>" . $orgDetail['state'] . "</td>
  </tr>
<tr>
    <th>Country</th>
    <td>" . $orgDetail['country'] . "</td>
  </tr>
<tr>
    <th>ZIP</th>
    <td>" . $orgDetail['zip'] . "</td>
  </tr>
  <tr>
    <th>Source of Registration</th>
    <td>Mobile</td>
  </tr>
<tr>
    <th>Date of Registration</th>
    <td>" . date("Y-M-d", strtotime($cdate)) . "</td>
  </tr>
</table>
<p>Please take it forward.</p>
<p>
Best Regards,</p>
<p>Team-HelloLeads</p>

</body>
</html>


"; return $message; } } 