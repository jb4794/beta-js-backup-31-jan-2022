var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
const {phpMinify} = require('@cedx/gulp-php-minify');
var ngAnnotate = require('gulp-ng-annotate');

gulp.task('scripts', function() {
  return gulp.src('D://CompressJS/Beta/compressjs/*.js')
    // Minify the file
	.pipe(concat('HLS.min.js'))
	.pipe(ngAnnotate())
	.pipe(uglify())
    // Output
    .pipe(gulp.dest('D://CompressJS/Beta/compressMin'))
});

gulp.task('compress:php', function() {
  return gulp.src('D://CompressJS/Beta/compressPHP/*.php')
  .pipe(phpMinify())
  .pipe(gulp.dest('D://CompressJS/Beta/compressPHPMin'))
});
