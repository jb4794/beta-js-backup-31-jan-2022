var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
const {phpMinify} = require('@cedx/gulp-php-minify');
const {dest, src, task} = require('gulp');


task('compress:php', () => src('D://CompressJS/Beta/compressPHP/*.php', {read: false})
  .pipe(concat('HLS.php'))
  .pipe(phpMinify())
  .pipe(uglify())
  .pipe(dest('D://CompressJS/Beta1/compressPHPMin'))
);