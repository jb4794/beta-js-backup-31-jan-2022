evtApp.controller('dispeventCtrl', function ($scope, $rootScope, alertUser,checkPremium,trialPopUp, $sce, Data, $uibModal, $http, toaster) {
    $rootScope.emptyevent = {};
    $rootScope.emptyevent.sendGreet = "1";
    $rootScope.emptyevent.listlead_share = "1";
    $scope.grid = true;
    $scope.eventServices = {};
    $scope.eventFields = {};
    $scope.eventCategories = {};
    $scope.tableColor = "";
    $scope.gridColor = "#e7eaec";
    if (sessionStorage.getItem("xtime") === "1") {
        $rootScope.firstProfile = true;
    } else {
        $rootScope.firstProfile = false;
    }

    $sce.trustAsHtml();
    $rootScope.addButtonhtml = '&nbsp;';
    //$rootScope.addButtonhtml += '<button ng-show="eventBut" tooltip-placement="bottom" tooltip="Clear demo event before create an event" class="btn btn-danger btn-sm" ng-click="delevent(eventsDet);"><span class="fa fa-trash"></span> Clear Demo Event</button>&nbsp;';
    //$rootScope.addButtonhtml += '<button class="btn btn-primary btn-sm" ng-click="addevent(emptyevent)" tooltip="Create List" tooltip-placement="bottom"><i class="fa fa-plus"></i></button>&nbsp;&nbsp;<button class="btn btn-default btn-sm btn-circle" popover="List can be a source of lead like trade show, expo or enquiries from website and so on. Create a list with automatic greeting email. For example Healthcon2016, Nashville,TN,  Call enquiries and Web enquiries." popover-title="Manage List" popover-trigger="focus" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    $rootScope.addButtonhtml += '<button class="btn btn-primary btn-sm" ng-click="addevent(emptyevent)" tooltip="Create List" tooltip-placement="bottom"><i class="fa fa-plus-circle"></i> Create List</button>&nbsp;&nbsp;<button class="btn btn-default btn-sm btn-circle" popover="Your Lists (also called Lead Lists) are logical grouping of your leads and customers so that it makes sense for your business and makes managing them easy. You can group your leads in a meaningful way using lists in HelloLeads. Lists can be used to group leads by source (for example Website leads vs Walk-ins leads) or say by region (New York leads vs Michigan leads) or even by time lines (2020 leads vs 2021 leads)." popover-title="Manage Lists" popover-trigger="focus" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    $scope.loader = true;
    $rootScope.eventBut = false;
    $rootScope.title = "Manage Lists";
    $rootScope.services = {};
    $rootScope.categories = {};
    $rootScope.fields = {};
    $rootScope.eventsDet = {};
    $rootScope.cFirstName = sessionStorage.getItem('cFirstName');
    $rootScope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }
    console.log("Organization Id is " + $rootScope.organizationId);

    // var resprom = $http.get(Data.serviceBase() + '/event/eventDetails', {params: {"orgId": $scope.organizationId}});
    sessionStorage.setItem('menuact', '2');
    $rootScope.menuact = '2';
    $scope.events = {};
    /* resprom.then(function (response) {
     $scope.events = response.data;
     sessionStorage.setItem('menuact', '2');
     $rootScope.menuact = '2';
     // console.log("Clear Event1");
     //     console.log(response.data[0]);
     /*if ($scope.events.length > 0) {
     if (angular.equals($scope.events[0]['name'], 'Demo Event')) {
     console.log("Clear Event");
     console.log(response.data[0]);
     $rootScope.eventBut = true;
     $rootScope.eventsDet = response.data[0];
     }
     }
     $scope.loader = false;
     //$scope.listData();
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });*/
    $scope.organizationId = sessionStorage.getItem('orgId'); 
    $scope.countUsers = {};
    $rootScope.getAllServCategField = function () {
        //For checking services and categories empty or not
        var serviceresp = $http.get(Data.serviceBase() + '/service/service', {params: {"key": "organizationId", "value": $rootScope.organizationId}});
        serviceresp.then(function (response) {
            console.log(response.data);
            $rootScope.services = response.data;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        var categoryresp = $http.get(Data.serviceBase() + '/category/category', {params: {"key": "organizationId", "value": $rootScope.organizationId}});
        categoryresp.then(function (response) {
            console.log(response.data);
            $rootScope.categories = response.data;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

        //Custome fields for the organization
        var fresprom = $http.get(Data.serviceBase() + '/field/field', {params: {key: "organizationId", "value": $rootScope.organizationId}});


        fresprom.then(function (response) {
            $rootScope.fields = response.data;
            // $scope.loader = false;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        /*var userresp = $http.get(Data.serviceBase() + '/user/user', {params: {key: "organizationId", "value": $scope.organizationId}});
         
         
         userresp.then(function (response) {
         $scope.countUsers = response.data;
         if (response.data.length > 1) {
         sessionStorage.setItem("cUsers", "1");
         } else {
         sessionStorage.setItem("cUsers", "");
         }
         // $scope.loader = false;
         }, function (response) {
         console.log('Error happened -- ');
         console.log(response);
         });*/
    }
    $scope.switchLayout = function (preview) {
        $scope.grid = preview;
        $scope.loader = true;
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
        $scope.organizationId = sessionStorage.getItem('orgId');
        if (preview === true) {
            $scope.gridColor = "#e7eaec";
            $scope.tableColor = "";
             Data.post('event/setCardViewCount', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else {
            $scope.tableColor = "#e7eaec";
            $scope.gridColor = "";
            Data.post('event/setTableViewCount', $scope.email).then(function (result) {
              console.log('Email '+$scope.cEmail);
            });
        }
        var resprom = $http.get(Data.serviceBase() + '/event/eventDetails', {params: {"orgId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.events = response.data.events;
            $scope.converstionSt = response.data.cStage;
            console.log("Clear Event1");
            /*if ($scope.events.length > 0) {
             console.log(response.data[0]);
             if (angular.equals($scope.events[0]['name'], 'Demo Event')) {
             console.log("Clear Event");
             $rootScope.eventBut = true;
             console.log(response.data[0]);
             $rootScope.eventsDet = response.data[0];
             }
             }*/
            $scope.loader = false;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    /*
     Data.get('event/event').then(function (data) {
     $scope.events = data;
     console.log($scope.events);
     });
     */
    //----------------------For Generating Grid View-----------------------
    var start = 0;
    var ending = start + 8;
    var lastdata = 100;
    var reachLast = false;

    $scope.loadmore = "Loading More data..";
    $scope.eventData = [];


    $scope.listData = function () {
        if (reachLast) {
            return false;
        }
        for (i = start; i < ending; i++) {
            if ($scope.events[i]) {
                $scope.eventData = $scope.eventData.concat($scope.events[i]);
            }
        }
        lastdata = $scope.events.length;
        start = ending;
        ending = start + 8;

        //$scope.testData.concat(jsondt);

        console.log(ending);
        console.log(lastdata);
        if ($scope.eventData.length >= lastdata) {
            reachLast = true;
            $scope.loadmore = "Reached at bottom";
        }
    };



    $scope.formatDate2 = function (date) {
        var dateO = date.split("-").join("/");
        var dateOut = new Date(dateO);
        return dateOut;
    };

    $scope.formatDate = function (date) {
        var dateOut = new Date(date);
        return dateOut;
    };

    $scope.showvisitors = function (event) {

        console.log("About to fetch visitors for event id " + event.id);
        localStorage.setItem('curEventId', event.id);
        localStorage.setItem('curEventName', event.name);
        localStorage.setItem('curEventsend', event.sendGreet);
        localStorage.setItem('curEventshare', event.listlead_share);
        localStorage.setItem('filters', "");
        window.location.href = Data.webAppBase() + 'account/manageVisitor';

        //$scope.eventVisitors = {};
        //$scope.eventVisitors = $http.get(Data.serviceBase() + '/visitor/visitor', {params: {key: "eventId", "value": event.id}});

        /*
         var modalInstance = $uibModal.open({
         templateUrl: 'eventVisitors.html',
         controller: 'dispvisitorCtrl',
         size: size,
         resolve: {
         visitors: function () {
         sessionStorage.setItem("curEventId", event.id);
         return $http.get(Data.serviceBase() + '/visitor/visitor', {params: {key: "eventId", "value": event.id}});;
         }
         }
         });
         
         
         modalInstance.result.then(function (selectedItem) {
         console.log(selectedItem);
         
         }, function () {
         console.info('Modal dismissed at: ' + new Date());
         });
         */
    };
    
    //----------------------------------------------- Show List Activity Log -------------------------------------------------- 

    $rootScope.listActivityLog = function (p, size) {

        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "changeHistory"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                //$scope.getAllServCategField();
                var modalInstance = $uibModal.open({
                    templateUrl: './listActivityMessage',
                    controller: 'listActivityCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        laevent: function () {
                            return p;
                        },
                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {

                    }
                });
            } else {
                 $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to view the change history of lists. Users with account owner or manger roles can view the change history of lists. Please contact HelloLeads account owner or mangers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });


    };

    //----------------------------------------------- End Show List Activity Log --------------------------------------------------   

    $rootScope.editevent = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "editList"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                //$scope.getAllServCategField();
                var modalInstance = $uibModal.open({
                    templateUrl: './modifyEvent',
                    controller: 'editeventCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        eevent: function () {
                            p.startDate = $scope.formatDate(p.startDate);
                            p.endDate = $scope.formatDate(p.endDate);
                            return p;
                        },
                        fields: function () {
                            return $scope.eventFields;
                        },
                        services: function () {
                            return $scope.eventServices;
                        },
                        categories: function () {
                            return $scope.eventCategories;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/event/eventDetails', {params: {"orgId": $scope.organizationId}});
                        resprom.then(function (response) {
                             $scope.events = response.data.events;
                             $scope.converstionSt = response.data.cStage;

                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };


    //Email Confirmation
    
    //End Email Confirmation

    //Clone or Copy List

    $rootScope.cloneevent = function (p, size) {
        $scope.loader = true;
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "editList"}});
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                $scope.loader = false;
                var modalInstance = $uibModal.open({
                    templateUrl: './cloneEvent',
                    controller: 'editeventCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        eevent: function () {
                            p.startDate = $scope.formatDate(p.startDate);
                            p.endDate = $scope.formatDate(p.endDate);
                            return p;
                        },
                        fields: function () {
                            return $scope.eventFields;
                        },
                        services: function () {
                            return $scope.eventServices;
                        },
                        categories: function () {
                            return $scope.eventCategories;
                        }
                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/event/eventDetails', {params: {"orgId": $scope.organizationId}});
                        resprom.then(function (response) {
                             $scope.events = response.data.events;
                             $scope.converstionSt = response.data.cStage;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                alertUser.alertpopmodel($scope.message);
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    //End Clone or Copy List


    $rootScope.addevent = function (p, size) {
         var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        // $rootScope.getAllServCategField();
        console.log($rootScope.services);
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "addList"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                
                // Free Premium Restriction Checking
               /* var respuserchk = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "list"}});

                // $scope.users = {};
                respuserchk.then(function (response) {
                    console.log(response);
                    $scope.loader = false;
                    if (response.data.status === 'success') {*/

                var modalInstance = $uibModal.open({
                    templateUrl: './modifyEvent',
                    controller: 'editeventCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        eevent: function () {
                            return p;
                        },
                        fields: function () {
                            return $rootScope.fields;
                        },
                        services: function () {
                            return $rootScope.services;
                        },
                        categories: function () {
                            return $rootScope.categories;
                        },
                        smsTemplates: function () {
                            return $rootScope.smsTemps;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/event/eventDetails', {params: {"orgId": $scope.organizationId}});
                        resprom.then(function (response) {
                             $scope.events = response.data.events;
                             $scope.converstionSt = response.data.cStage;
                            var vcheck = sessionStorage.getItem('vcheck'); 
                            sessionStorage.setItem('vcheck',vcheck+'e'); 
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            
                /*} else {
                        $scope.message = {"title": "Lists", "message":"The maximum number of lists allowed for free account is 2. To create additional lists, upgrade to premium plan. Premium plan allows unlimited lists in HelloLeads" };
                        checkPremium.premiumpopmodel($scope.message);
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });*/

            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

    $rootScope.generateKey = function (p, size) {
          var respuserchk = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        respuserchk.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "editList"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance = $uibModal.open({
                    templateUrl: './generateEventKey',
                    controller: 'genEventKeyCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        eevent: function () {
                            return p;
                        },
                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/event/eventDetails', {params: {"orgId": $scope.organizationId}});
                        resprom.then(function (response) {
                             $scope.events = response.data.events;
                             $scope.converstionSt = response.data.cStage;

                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to access this info. Only account owner or Managers (within HelloLeads) can access this. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });


    };
    $rootScope.delevent = function (p, size) {
         var respuserchk = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        respuserchk.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteList"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance1 = $uibModal.open({
                    templateUrl: './deleteevent',
                    controller: 'deleventCtrl',
                    size: size,
                    resolve: {
                        devent: function () {
                            return p;
                        }
                    }
                });
                modalInstance1.result.then(function (i) {
                    if (i) {
                        $rootScope.eventBut = false;
                        var resprom = $http.get(Data.serviceBase() + '/event/eventDetails', {params: {"orgId": $scope.organizationId}});
                        resprom.then(function (response) {
                             $scope.events = response.data.events;
                             $scope.converstionSt = response.data.cStage;

                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                        //For checking services and categories empty or not
                        var serviceresp = $http.get(Data.serviceBase() + '/service/service', {params: {"key": "organizationId", "value": $scope.organizationId}});
                        serviceresp.then(function (response) {
                            console.log(response.data);
                            $rootScope.services = response.data;

                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                        var categoryresp = $http.get(Data.serviceBase() + '/category/category', {params: {"key": "organizationId", "value": $scope.organizationId}});
                        categoryresp.then(function (response) {
                            console.log(response.data);
                            $rootScope.categories = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

    $scope.ok = function () {
        $uibModalInstance.close(project);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

evtApp.controller('genEventKeyCtrl', function ($scope, Data, $filter, $uibModalInstance, eevent, toaster, $http, $rootScope) {
    $scope.loader = true;
    var original = angular.copy($scope.eevent);
    $scope.eevent = angular.copy(eevent);
    $scope.purl = "";
    $scope.key = "";
    $scope.surl = "";
    $scope.simg = "";
    $scope.eventName = "";
    var keyresp = $http.get(Data.serviceBase() + '/event/generateKey', {params: {"eventId": $scope.eevent.id}});
    keyresp.then(function (response) {
        console.log(response.data);
        $scope.purl = response.data.url;
        $scope.key = response.data.key;
        if (response.data.shortURL !== undefined) {
            $scope.surl = response.data.shortURL;
            // console.log(response.data.qr_code_image_url);
            $scope.simg = response.data.qr_code_image_url;
            $scope.loader = false;
        }
        $scope.eventName = response.data.eventName;
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
    $scope.clickMsg = function () {
        toaster.pop("info", "", "Text copied to clipboard", 4000, 'trustedHtml');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});





evtApp.controller('editeventCtrl', function ($scope, Data, services, $uibModal, $filter, fields, categories, $uibModalInstance, eevent, toaster, $http, $rootScope) {
    var original = angular.copy($scope.eevent);
    $scope.eevent = angular.copy(eevent);
    $scope.services = {};
    $scope.fields = {};
    $scope.categories = {};
    $scope.selectedServices = [];
    $scope.selectedFields = [];
    $scope.selectedCategories = [];
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.eventsList = {};
    if($scope.eevent.sendGreetSMS === undefined || $scope.eevent.sendGreetSMS === null){
        $scope.eevent.sendGreetSMS = '1';
    }
    //For checking services and categories empty or not

    /*var userresp = $http.get(Data.serviceBase() + '/user/user', {params: {key: "organizationId", "value": $scope.organizationId}});
     
     
     userresp.then(function (response) {
     $scope.countUsers = response.data;
     if (response.data.length > 1) {
     sessionStorage.setItem("cUsers", "1");
     } else {
     sessionStorage.setItem("cUsers", "");
     }
     // $scope.loader = false;
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });*/
     
     //Tiny MCE begin
   /* 
    
     $scope.tinymceOptions = {
        selector: "textarea",
        entity_encoding:"UTF-8",
    height: 280,
  resize: true,
  autosave_ask_before_unload: false,
  paste_data_images: true,
  plugins: [
    " code paste anchor autolink codesample colorpicker fullscreen help image imagetools",
    " lists link media noneditable preview lineheight advlist",
    " searchreplace table textcolor wordcount"
  ], /* removed:  charmap insertdatetime print 
  external_plugins: {
    moxiemanager: "https://www.tiny.cloud/pro-demo/moxiemanager/plugin.min.js"
    
  },
  
  templates: [],
  toolbar:
    "a11ycheck undo redo | bold italic fontselect fontsizeselect| forecolor backcolor | lineheightselect indent outdent | alignleft aligncenter alignright alignjustify | bullist numlist fullpage | link image mybutton mybutton1 signbutton ",
  
  content_css: [
    "https://fonts.googleapis.com/css?family=Lato|Open+Sans|PT+Serif",
     "https://beta.helloleads.io/css/tinymcecontent.css"
  ],
  spellchecker_dialog: true,
  setup: function (editor) {
            editor.addButton('mybutton', {
                text: '#VisitorName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorName#');
                }
            });
            editor.addButton('mybutton1', {
                text: '#VisitorCompanyName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorCompanyName#');
                }
            });
            
            /* editor.addButton('mybutton2', {
             text: '#EventName',
             icon: false,
             onclick: function () {
             editor.insertContent('#EventName#');
             }
             });
             editor.addButton('mybutton2', {
                text: '#firstname',
                icon: false,
                onclick: function () {
                    editor.insertContent('#firstname#');
                }
            });
            editor.addButton('mybutton3', {
                text: '#lastname',
                icon: false,
                onclick: function () {
                    editor.insertContent('#lastname#');
                }
            });
            editor.addButton('mybutton4', {
                text: '#email',
                icon: false,
                onclick: function () {
                    editor.insertContent('#email#');
                }
            });
            editor.addButton('mybutton5', {
                text: '#mobile',
                icon: false,
                onclick: function () {
                    editor.insertContent('#mobile#');
                }
            });
            editor.addButton('mybutton6', {
                text: '#designation',
                icon: false,
                onclick: function () {
                    editor.insertContent('#designation#');
                }
            });

        }
  
};*/$scope.tinymceOptions = {
        selector: 'textarea',
        statusbar: false,
        entity_encoding: "UTF-8",
        elementpath: false,
        height: 375,
        theme: 'modern',
        menubar: true,      
        autosave_ask_before_unload: false,
        paste_data_images: true,
        forced_root_block_attrs: {
            "class": "myclass",
            "data-something": "my data",
            "style": "margin: 5px 0;"
        },
        // images_upload_handler: function (blobInfo, success, failure) {
  //           success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64());
  //       },
        plugins: [
            'advlist autolink lists link image noneditable preview hr pagebreak table textcolor imagetools ',
            'searchreplace wordcount visualblocks visualchars code paste lineheight emoticons ',
            'insertdatetime media nonbreaking save table colorpicker fullscreen contextmenu directionality'
        ],
        lineheight_formats: "LineHeight 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
        toolbar1: 'undo redo | styleselect | fontselect fontsizeselect | bold italic',
        toolbar2: 'forecolor backcolor | lineheightselect | bullist numlist | link uploadimage emoticons | alignleft aligncenter alignright alignjustify indent outdent',
        toolbar3: 'mybutton,mybutton1 signbutton ',
       content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            "https://beta.helloleads.io/css/tinymcecontent.css"],
        content_style: ".tinymce-content p {  padding: 0; margin: 1px 0; }",
        setup: function (editor) {
            editor.addButton('mybutton', {
                text: '#VisitorName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorName#');
                }
            });
            editor.addButton('mybutton1', {
                text: '#VisitorCompanyName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorCompanyName#');
                }
            });
           editor.addButton('signbutton', {
                text: 'Signature',
                title: 'Signature',
                type: 'menubutton',
                icon: false,
                menu: [
                    {
                        text: '#firstname',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#firstname#');
                        }
                    },
                    {
                        text: '#lastname',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#lastname#');
                        }

                    },
                    {
                        text: '#designation',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#designation#');
                        }
                    },
                    {
                        text: '#company',
                        icon: false,
                        onclick: function () {
                            var orgname = sessionStorage.getItem('cOrgName');
                            editor.insertContent(orgname);
                        }
                    },
                    {
                        text: '#email',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#email#');
                        }
                    },
                    {
                        text: '#mobile',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#mobile#');
                        }
                    },
                ]
            });
        }
    };
    //TinyMCE End
        $scope.fields = {};
    $scope.getFieldDet = function () {
        var fresprom = $http.get(Data.serviceBase() + '/field/field', {params: {"key": "organizationId", "value": $scope.organizationId}});


        fresprom.then(function (response) {
            $scope.fields = response.data;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.selectFinal = [];
    $scope.getAllFields = function (field) {
        if (field !== 0) {


            //Custome fields for the organization
            var fresprom = $http.get(Data.serviceBase() + '/event/allFieldsVals', {params: {"organizationId": $scope.organizationId, "fieldId": field}});


            fresprom.then(function (response) {
                var orderindex = 1;
                console.log(response.data);
                var dupCheck = 0;
                angular.forEach($scope.selectFinal, function (d) {

                    if (d.fieldId === response.data[0].fieldId) {
                        dupCheck = 1;

                    }

                });

                if ($scope.selectFinal.length > 0 && $scope.selectFinal[$scope.selectFinal.length - 1]['name'] !== '') {
                    if (dupCheck === 0) {
                        $scope.selectFinal.push(response.data[0]);
                    }


                } else {
                    $scope.selectFinal.length = 0;
                    $scope.selectFinal = response.data;
                }
                $scope.selFname = 'Select and add custom fields';
                $scope.selFieldId = 0;
                $scope.editindex = $scope.selectFinal.length > 0 ? $scope.selectFinal.length : 0;
                angular.forEach($scope.selectFinal, function (d) {

                    if (d['fieldOrder'] === undefined || d['fieldOrder'] === null) {

                        d['fieldOrder'] = orderindex.toString();
                        console.log(d);
                    }
                    orderindex = orderindex + 1;

                });

                // $scope.loader = false;
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
        }
    };

    $scope.getAllSmsTemplate = function () {
    // if($scope.eevent.smsTemplateId !== null || $scope.eevent.smsTemplateId !== '') {
        // var resprom = $http.get(Data.serviceBase() + 'organization/getSmsTemp', {params: {"id": $scope.eevent.smsTemplateId,"organizationId": $scope.organizationId}});
        var resprom = $http.get(Data.serviceBase() + 'organization/smsTemplate', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            //$scope.eevent.smsContent = response.data[0].smsContent;
            // $scope.smsTemplateDet = response.data[0]; 
            $scope.smsTemps = response.data;
            //$scope.eevent.smsTempId = response.data[0].id;
            angular.forEach(response.data, function (d) {
                if (($scope.eevent.smsTemplateId !== null || $scope.eevent.smsTemplateId !== '') && d.id === $scope.eevent.smsTemplateId) {
                    $scope.smsTemplateDet = d;
                 var resprom = $http.get(Data.serviceBase() + 'organization/getSmsTemp', {params: {"id": $scope.eevent.smsTemplateId,"organizationId": $scope.organizationId}});
                 resprom.then(function (response) {
                        $scope.eevent.smsContent = response.data[0].smsContent;
                        $scope.eevent.smsTempId = response.data[0].id;
                     }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                    });
                } else if (($scope.eevent.smsTemplateId == null || $scope.eevent.smsTemplateId == '') && d.id !== $scope.eevent.smsTemplateId) {
                    $scope.smsTemplateDet = d;
                }
            });
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    // }
    };

    $scope.setFDet = function (field, findex) {
        $scope.selFname = field.name;
        $scope.selFieldId = field.id;
        $scope.fields.splice(findex, 1);
    };
    //For checking services and categories empty or not

    // $scope.getAllSmsTemplate = function () {
    //     var resprom = $http.get(Data.serviceBase() + 'organization/smsTemplate', {params: {"organizationId": $scope.organizationId}});
    //     resprom.then(function (response) {
    //         $scope.smsTemps = response.data;
    //     }, function (response) {
    //         console.log('Error happened -- ');
    //         console.log(response);
    //     });
    // };


    $scope.getOrgSmsConfig = function () {
        var resprom = $http.get(Data.serviceBase() + '/organization/getSmsConfig', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            console.log("Edit Event "+response.data[0]);
            $scope.eevent.smsProvider = response.data[0].smsProvider;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    if ($scope.eevent.id !== undefined) {
        $scope.title = "Update List";
        $scope.btntxt = "Update";

    } else {

        // $scope.getAllServCategField();
       /* var serviceresp = $http.get(Data.serviceBase() + '/service/service', {params: {"key": "organizationId", "value": $scope.organizationId}});
        serviceresp.then(function (response) {
            //console.log(response.data);
            $scope.services = response.data;
            if ($scope.services.length > 1) {
                for (var i = 0; i < $scope.services.length; i++) {
                    $scope.selectedServices.push($scope.services[i]['name']);
                    //console.log($scope.services[i]['name']);
                }
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        var categoryresp = $http.get(Data.serviceBase() + '/category/category', {params: {"key": "organizationId", "value": $scope.organizationId}});
        categoryresp.then(function (response) {
            //console.log(response.data);
            $scope.categories = response.data;
            if ($scope.categories.length > 1) {
                for (var i = 0; i < $scope.categories.length; i++) {
                    $scope.selectedCategories.push($scope.categories[i]['name']);
                    // console.log($scope.categories[i]['name']);
                }
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

        //Custome fields for the organization
        var fresprom = $http.get(Data.serviceBase() + '/field/field', {params: {key: "organizationId", "value": $scope.organizationId}});


        fresprom.then(function (response) {
            $scope.fields = response.data;
            if ($scope.fields.length > 1) {
                for (var i = 0; i < $scope.fields.length; i++) {
                    $scope.selectedFields.push($scope.fields[i]['name']);
                    //console.log($scope.fields[i]['name']);
                }
            }
            // $scope.loader = false;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        
        // $scope.services = services;

        //console.log($scope.selectedServices);
        //$scope.categories = categories;

        //$scope.fields = fields;*/

        $scope.title = "Create a New List";
        $scope.btntxt = "Save";
    }
    
    $scope.eventOrgDetails = {};
    $scope.getOrgDetails = function(){
        var orgresp = $http.get(Data.serviceBase() + '/organization/organization', {params: {"key": "id", "value": $scope.organizationId}});
        orgresp.then(function (response) {
            console.log("Org details : " + response.data);
            $scope.eventOrgDetails = response.data[0];
       }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    
    
    $scope.usersDet = [];
    $scope.userlist = [];
    $scope.setUserDet = function (usr) {
        angular.forEach($scope.usersDet, function (d, k) {
            if (d.id === usr.id) {
                $scope.usersDet.splice(k, 1);
            }
        });
        $scope.userlist.push(usr);
    };

    $scope.getAllAccessUser = function () {

        var orgresp = $http.get(Data.serviceBase() + '/user/userByOrganization', {params: {"organizationId": $scope.organizationId}});
        orgresp.then(function (response) {
            console.log($scope.eevent.eventManagerId);
            $scope.usersDet = response.data;
            angular.forEach($scope.userlist, function (d, k) {
                if (d.id === $scope.eventManager) {

                    $scope.usersDet.splice(k, 1);
                }
            });

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    };

    $scope.changecontentSMSTemp = function () {
        $scope.smsTemplateId = $scope.smsTemplateDet.id;
        var resprom = $http.get(Data.serviceBase() + 'organization/getSmsTemp', {params: {"id": $scope.smsTemplateId,"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.eevent.smsContent = response.data[0].smsContent;
            $scope.eevent.flowId = response.data[0].flowId;
            $scope.eevent.smsTempId = $scope.smsTemplateId;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.deleteUserDet = function (usr) {
        angular.forEach($scope.userlist, function (d, k) {
            if (d.id === usr.id) {
                $scope.userlist.splice(k, 1);
                $scope.usersDet.push(usr);
            }
        });
    };
    $scope.selTxt = 'Select All';
    $scope.selectAllUser = function () {
        if ($scope.userlist.length === 0) {
            angular.forEach($scope.usersDet, function (d, k) {

                $scope.userlist.push(d);


            });
            $scope.selTxt = 'Deselect All';
        } else {
            $scope.userlist = [];
        }
    };
    
     $scope.checkField = function(){

        if($scope.eventOrgDetails.id === '35978' || $scope.eventOrgDetails.id === '26053'){
            $scope.eevent.sendGreetSMS == '0';
            $scope.eevent.smsContent = "Dear #VisitorName# Ji,\n\nGreetings from Kothari Group!!!\nThank you for contacting us.\nWe have received your enquiry. Our executive will call you soon.\nFor urgent enquiries please call on 9307309583 or 18001204343 (Toll free). KAPLMH";
            return true;
            }

       if($scope.eventOrgDetails.smsInteg === '1' || $scope.eevent.sendGreetSMS === '1'){
           $scope.smsTemplateDet = "";
           $scope.eevent.smsContent = "";
           return true;
       }else{
           return false;
       }

       
    };
    

    $scope.canSaveTask = function () {
        return $scope.editeventform.$valid && !angular.equals($scope.eevent, original);
    };
    $scope.redirectToS = function () {
        sessionStorage.setItem("menuact", 4);
        window.location.href = Data.webAppBase() + 'account/service';

    };
    $scope.redirectToC = function () {
        sessionStorage.setItem("menuact", 4);
        window.location.href = Data.webAppBase() + 'account/category';

    };
    $scope.redirectToF = function () {
        sessionStorage.setItem("menuact", 4);
        window.location.href = Data.webAppBase() + 'account/fields';

    };

   $scope.tab = 1;
    angular.element(document.getElementById("eventdet-1")).addClass("active");
    angular.element(document.getElementById("emaildet")).removeClass("active");
    //angular.element(document.getElementById("int-cg")).removeClass("active");
    angular.element(document.getElementById("accessright")).removeClass("active");
    //Event Icon Disable
    angular.element(document.getElementById("listInfo")).addClass("listSelected");
    angular.element(document.getElementById("customInfo")).removeClass("listSelected");
    angular.element(document.getElementById("accessInfo")).removeClass("listSelected");
    $scope.switcheventTab = function (tabId) {
        console.log(tabId);
        if (tabId === 1) {
            $scope.tab = 1;
            angular.element(document.getElementById("eventdet-1")).addClass("active");
            angular.element(document.getElementById("emaildet")).removeClass("active");
            //angular.element(document.getElementById("int-cg")).removeClass("active");
            angular.element(document.getElementById("accessright")).removeClass("active");
            //Event Icon Disable
            angular.element(document.getElementById("listInfo")).addClass("listSelected");
            angular.element(document.getElementById("customInfo")).removeClass("listSelected");
            angular.element(document.getElementById("accessInfo")).removeClass("listSelected");
        } else if (tabId === 3) {
            $scope.tab = 3;
            angular.element(document.getElementById("emaildet")).addClass("active");
            angular.element(document.getElementById("eventdet-1")).removeClass("active");
            //angular.element(document.getElementById("int-cg")).removeClass("active");
            angular.element(document.getElementById("accessright")).removeClass("active");
            //Event Icon Disable
            angular.element(document.getElementById("customInfo")).addClass("listSelected");
            angular.element(document.getElementById("listInfo")).removeClass("listSelected");
            angular.element(document.getElementById("accessInfo")).removeClass("listSelected");
        } else if (tabId === 4) {
            console.log(tabId);
            $scope.tab = 4;
            angular.element(document.getElementById("accessright")).addClass("active");
            angular.element(document.getElementById("emaildet")).removeClass("active");
            angular.element(document.getElementById("eventdet-1")).removeClass("active");
            //angular.element(document.getElementById("int-cg")).removeClass("active");
            //Event Icon Disable
            angular.element(document.getElementById("accessInfo")).addClass("listSelected");
            angular.element(document.getElementById("listInfo")).removeClass("listSelected");
            angular.element(document.getElementById("customInfo")).removeClass("listSelected");
        }

    };


    $scope.Continueeventtonext = function () {
        $scope.tab = 3;
        angular.element(document.getElementById("eventdet-1")).removeClass("active");
        angular.element(document.getElementById("emaildet")).addClass("active");
        //angular.element(document.getElementById("int-cg")).removeClass("active");
        angular.element(document.getElementById("accessright")).removeClass("active");
        console.log($scope.tab);

    };
    $scope.Continueeventtonext1 = function () {
        $scope.tab = 4;
        angular.element(document.getElementById("emaildet")).removeClass("active");
        angular.element(document.getElementById("eventdet-1")).removeClass("active");
        //angular.element(document.getElementById("int-cg")).removeClass("active");
        angular.element(document.getElementById("accessright")).addClass("active");
        console.log($scope.tab);

    };


    //---VV----Getting Service list----VV---
    /*$scope.services = {};
     $scope.eventservices = [];
     $scope.selectedService = [];
     // console.log("Organization Id is " + $scope.organizationId);
     
     var serviceresp = $http.get(Data.serviceBase() + '/service/service', {params: {"key": "organizationId", "value": $scope.organizationId}});
     serviceresp.then(function (response) {
     $scope.services = response.data;
     //---VV---Chaecking event id exists or not if exists do or otherwies default it will set eventservices and selectedService to empty array ---VV---
     if ($scope.eevent.id !== undefined) {
     
     var eventserviceresp = $http.get(Data.serviceBase() + '/eventservice/eventservice', {params: {"key": "eventId", "value": $scope.eevent.id}});
     eventserviceresp.then(function (response) {
     //console.log(response.data);
     angular.forEach(response.data, function (d) {
     $scope.eventservices.push(d.serviceId);
     });
     
     //-- next  we loop thru all services - if eventservice exist, then
     //-- we add them to the temp list (selectedService)
     //-- if not, selectedService will have null
     //-- selectedService is equivalent to eventservices, with null for unselected services
     //-- we use selectedService as the model when building the UI for selected services
     
     for (var i = 0; i < $scope.services.length; i++) {
     if ($scope.eventservices.indexOf($scope.services[i]['id']) > -1) {
     $scope.selectedService.push($scope.services[i]['id']);
     //console.log($scope.services[i]['id']);
     } else {
     $scope.selectedService.push(null);
     }
     
     }
     
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
     }
     
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
     
     //---VV----Getting Categories list----VV---
     $scope.categories = {};
     $scope.eventcategories = [];
     $scope.selectedCategories = [];
     //$scope.organizationId = sessionStorage.getItem('orgId');
     //console.log("Organization Id is " + $scope.organizationId);
     
     var categoryresp = $http.get(Data.serviceBase() + '/category/category', {params: {"key": "organizationId", "value": $scope.organizationId}});
     categoryresp.then(function (response) {
     $scope.categories = response.data;
     //---VV---Chaecking event id exists or not if exists do or otherwies  default it will set eventcategories and selectedCategories to empty array ---VV---
     if ($scope.eevent.id !== undefined) {
     
     var eventcategoryresp = $http.get(Data.serviceBase() + '/eventcategory/eventcategory', {params: {"key": "eventId", "value": $scope.eevent.id}});
     eventcategoryresp.then(function (response) {
     //console.log(response.data);
     angular.forEach(response.data, function (d) {
     $scope.eventcategories.push(d.categoryId);
     });
     
     //-- next  we loop thru all categories - if eventcategories exist, then
     //-- we add them to the temp list (selectedCategories)
     //-- if not, selectedCategories will have null
     //-- selectedCategories is equivalent to eventcategories, with null for unselected categories
     //-- we use selectedCategories as the model when building the UI for selected categories
     
     for (var i = 0; i < $scope.categories.length; i++) {
     if ($scope.eventcategories.indexOf($scope.categories[i]['id']) > -1) {
     $scope.selectedCategories.push($scope.categories[i]['id']);
     //console.log($scope.categories[i]['id']);
     } else {
     $scope.selectedCategories.push(null);
     }
     
     }
     
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
     }
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });*/
    // Get Already exisiting list from Db
    /*  var fresprom = $http.get(Data.serviceBase() + '/event/orgEventList', {params: {"organizationId": $scope.organizationId}});
     
     
     fresprom.then(function (response) {
     $scope.eventsList = response.data;
     
     // $scope.loader = false;
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
     $scope.alreadyFlag = false;
     $scope.checkEvent = function (name)
     {
     $scope.alreadyFlag = false;
     angular.forEach($scope.eventsList, function (value, key) {
     if (name === $scope.eventsList[key]['name']) {
     $scope.alreadyFlag = true;
     }
     
     });
     
     };*/
     
     // -------------------------------------------------- Show business card control ------------------------------------------------------------------
    $scope.showBusinessCard = function (p, size) {

        /* var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "changeHistory"}});
         
         // $scope.users = {};
         resprole.then(function (response) {
         console.log(response);
         $scope.loader = false;
         if (response.data.status === 'success') {*/
        //$scope.getAllServCategField();
        var modalInstance = $uibModal.open({
            templateUrl: './showBusinessCardProfile',
            controller: 'showBizCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                laevent: function () {
                    return p;
                },
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

            }
        });
        /*  } else {
         $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to view the change history of lists. Users with account owner or manger roles can view the change history of lists. Please contact HelloLeads account owner or mangers in your company for assistance with this task"};
         alertUser.alertpopmodel($scope.message);
         //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
         }
         }, function (response) {
         
         console.log('Error happened -- ');
         console.log(response);
         });*/


    };
 // ------------------------------------------------------------------ End business card ctrl ----------------------------------------------------------   


    
    //End Email Confiuration

    //--VV-- Getting Email template --VV----
    //var user=
    var ufirstName = sessionStorage.getItem('firstName');
    var ulastName = sessionStorage.getItem('lastName');
    var orgName = sessionStorage.getItem('cOrgName');
    var uemail = sessionStorage.getItem('userEmail');
    //var umobile = sessionStorage.getItem('userPhone');
    var umobile;
    if(sessionStorage.getItem('userPhone') == '' || sessionStorage.getItem('userPhone') == 'null' || sessionStorage.getItem('userPhone') == 'undefined')
    {
        umobile = '';
    
    }
    else
    {
        umobile = sessionStorage.getItem('userPhone');
    }
     $scope.eventemailtemplate = {"subject": "Thank you for your interest in " + orgName, "userBodyText": "Dear #VisitorName#,<br><br>Thank you for your interest in " + orgName + ", We are glad to support #VisitorCompanyName# with our Products & Services.<br><br>We will be glad to stay in touch and we look forward to understanding your business needs.<br><br>Best Regards,<br> #firstname# #lastname# <br> #designation#<br><br>" + orgName + "<br>E: #email#<br>M: #mobile#"};
    $scope.selectedEditFVal = {};
    $scope.selectFinal = [];
    if ($scope.eevent.id !== undefined) {
        var eventemailresp = $http.get(Data.serviceBase() + '/eventemailtemplates/eventemailtemplates', {params: {eventId: $scope.eevent.id}});


        eventemailresp.then(function (response) {

            $scope.eventemailtemplate = response.data[0];
            console.log(response);

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

        //For checking services and categories empty or not
       /* var serviceresp = $http.get(Data.serviceBase() + '/event/eventServices', {params: {"eventId": $scope.eevent.id}});
        serviceresp.then(function (response) {
            //console.log(response.data);
            $scope.services = response.data;

            for (var i = 0; i < $scope.services.length; i++) {
                $scope.selectedServices.push($scope.services[i]['name']);
                //console.log($scope.services[i]['name']);
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        var categoryresp = $http.get(Data.serviceBase() + '/event/eventCategories', {params: {"eventId": $scope.eevent.id}});
        categoryresp.then(function (response) {
            // console.log(response.data);
            $scope.categories = response.data;
            if ($scope.categories.length >= 1) {
                for (var i = 0; i < $scope.categories.length; i++) {
                    $scope.selectedCategories.push($scope.categories[i]['name']);
                    //console.log($scope.categories[i]['name']);
                }
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });*/

        var fieldresp = $http.get(Data.serviceBase() + '/event/eventFieldsVals', {params: {"eventId": $scope.eevent.id}});
        fieldresp.then(function (response) {
            //console.log("Response Filed Values for Event");
            //console.log(response.data);
            var orderindex = 1;
            $scope.selectFinal = response.data;
            $scope.editindex = $scope.selectFinal.length > 0 ? $scope.selectFinal.length : 0;
            angular.forEach(response.data, function (d) {

                if (d['fieldOrder'] === undefined || d['fieldOrder'] === null) {

                    d['fieldOrder'] = orderindex;
                }
                orderindex = orderindex + 1;

            });
            /* angular.forEach(response.data, function (d) {
             if ($scope.selectedFieldVal[d.name] === undefined) {
             $scope.selectedFieldVal[d.name] = d.values;
             } else {
             $scope.selectedFieldVal[d.name] = $scope.selectedFieldVal[d.name] + "," + d.values;
             }
             });*/
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        var accessresp = $http.get(Data.serviceBase() + '/event/eventAccessUsers', {params: {"eventId": $scope.eevent.id}});
        accessresp.then(function (response) {
            console.log("Response Filed Values for Event");
            console.log(response.data);
            $scope.userlist = response.data;
            /* angular.forEach(response.data, function (d) {
             if ($scope.selectedFieldVal[d.name] === undefined) {
             $scope.selectedFieldVal[d.name] = d.values;
             } else {
             $scope.selectedFieldVal[d.name] = $scope.selectedFieldVal[d.name] + "," + d.values;
             }
             });*/


        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    }


    //----VV---Adding Tags in Email content like #Lead Name# etc... it will call myText directive set the text in the textarea---VV---
    $scope.addTag = function (tag) {
        //$scope.items.push($scope.someInput);
        //console.log(tag);
         $rootScope.$broadcast('addTags', tag);
        //toaster.pop("info", "", "<strong>" + tag + "</strong>" + " placeholder copied to clipboard. Paste the placeholder text wherever required in the email content", 7000, 'trustedHtml');

    };
    $scope.servicestrList = "";
    $scope.fieldstrList = "";
    $scope.categorystrList = "";
    //Add services to selected services array
    // console.log($scope.services);


    //Add category to selected services array
    // console.log($scope.categories);

    $scope.addService = function (service) {
        if (service) {
            console.log($scope.selectedServices.indexOf(service));
            if ($scope.selectedServices.indexOf(service) <= -1) {
                $scope.selectedServices.push(service);

                $scope.serviceline = "";
            }
        }
    };
    $scope.clearService = function (service) {
        var index = $scope.selectedServices.indexOf(service);
        $scope.selectedServices.splice(index, 1);

        //$scope.tags.push(tag);
        //$scope.tagline="";
    };

    $scope.addCategorylist = function (category) {
        if (category) {
            console.log($scope.selectedCategories.indexOf(category));
            if ($scope.selectedCategories.indexOf(category) <= -1) {
                $scope.selectedCategories.push(category);
                $scope.cgroupline = "";
            }
        }
    };
    $scope.clearCategory = function (category) {
        var index = $scope.selectedCategories.indexOf(category);
        $scope.selectedCategories.splice(index, 1);

        //$scope.tags.push(tag);
        //$scope.tagline="";
    };
    $scope.fieldline = "";
    $scope.fieldval = "";
    $scope.fieldReq = "";
    $scope.fieldtype = "Text field";
    $scope.filedWValues = {};
    $scope.editindex = $scope.selectFinal.length > 0 ? $scope.selectFinal.length : 0;
    $scope.selecetdValues = [];
    $scope.selectedFieldVal = {};
    $scope.selectedFieldConfig = {};
    //Chenck For custom fields duplicate
    $scope.checkDuplicate = function (selectFinal, fieldtxt) {
        var dupflag = 0;
        $scope.custError = '0';
        for (var i = 0; i < selectFinal.length; i++) {
            if (selectFinal[i]['name'] === fieldtxt) {
                dupflag = 1;
                
            }
        }
        if(dupflag == 1){
            $scope.custError = '1';
        }else{
            $scope.custError = '0';
        }
        return dupflag;
    };
    $scope.addValueToField = function (val, fieldtxt, fieldtype, fieldreq, index) {
        console.log("Field requirement " + fieldreq);
        var checkFlag = 0;
        var fieldr = '';
        if (fieldreq === '1') {
            fieldr = '1';
        } else {
            fieldr = '0';
        }
        if ($scope.selectFinal.length > index) {
            for (var i = 0; i < $scope.selectFinal.length; i++) {
                if (i === index) {
                    //$scope.editindex = 0;
                    //$scope.selectFinal.push({"name": fieldtxt, "Fvalues": val, "fieldType": fieldtype, "fieldReq": fieldr});
                    //checkFlag = 1;
                    if (!$scope.checkDuplicate($scope.selectFinal, fieldtxt)) {
                        $scope.selectFinal[i]['name'] = fieldtxt;
                        $scope.selectFinal[i]['Fvalues'] = val;
                        $scope.selectFinal[i]['fieldType'] = fieldtype;
                        $scope.selectFinal[i]['fieldReq'] = fieldreq;
                    }

                }
                //console.log($scope.categories[i]['name']);
            }
        } else if (index >= $scope.selectFinal.length) {
            if (!$scope.checkDuplicate($scope.selectFinal, fieldtxt)) {
                $scope.selectFinal.push({"name": fieldtxt, "Fvalues": val, "fieldType": fieldtype, "fieldReq": fieldr});
            }
        }
        $scope.shak = false;
        $scope.editindex = $scope.selectFinal.length;
        /* if (checkFlag === 0) {
         
         $scope.selectFinal.push({"name": fieldtxt, "Fvalues": val, "fieldType": fieldtype, "fieldReq": fieldreq});
         }*/
        $scope.fieldval = "";
        $scope.fieldline = "";
        $scope.fieldReq = "1";
        $scope.fieldtype = "Text field";


    };
    
    $scope.addValuesField = function (valarr, efield) {
        $scope.selectedFieldVal[efield] = valarr.toString();
    };
    /*  $scope.addFieldslist = function (efield) {
     if (efield) {
     console.log($scope.selectedFields.indexOf(efield));
     if ($scope.selectedFields.indexOf(efield) <= -1) {
     $scope.selectedFields.push(efield);
     $scope.fieldline = "";
     }
     }
     };*/
    $scope.shak = false;
    $scope.editFieldVal = function (key, val, type, req, index) {
        $scope.shak = true;
        $scope.fieldline = key;
        $scope.fieldval = val;
        $scope.fieldReq = req;
        $scope.fieldtype = type;
        $scope.editindex = index;
        //console.log(key+val+type+req);
    };
    $scope.clearField = function (efield) {

        for (var i = 0; i < $scope.selectFinal.length; i++) {
            if ($scope.selectFinal[i]['name'] === efield) {
                console.log(efield);
                $scope.selectFinal.splice(i, 1);

            }
            //console.log($scope.categories[i]['name']);
        }
        //$scope.tags.push(tag);
        //$scope.tagline="";
    };
    $scope.clearFieldVal = function (efield, index) {
        //var index = $scope.selectedFieldVal.indexOf(efield);
        // console.log("Clear val" + index);
        //  if (index) {

        var selectcopyFinal = angular.copy($scope.selectFinal);
        //console.log(index);
        //console.log(selectcopyFinal);
        selectcopyFinal.splice(index, 1);
        //delete selectcopyFinal[index];
        $scope.selectFinal.length = 0;
        if (selectcopyFinal.length >= 0) {
            $scope.selectFinal = selectcopyFinal;
            //console.log(selectcopyFinal);
            //console.log($scope.selectFinal);
        }

        //} else if (index === 0) {
        //     $scope.selectFinal = {};
        //}
    };
    
     // New UI design for Custom fields
    $scope.addFields = function () {
        var temp = {"name": "", "Fvalues": "", "fieldType": "Text field", "fieldReq": "1", "fieldOrder": '' + (parseInt($scope.selectFinal.length) + 1)};
        console.log(temp);
        $scope.selectFinal.push(temp);
    };

    $scope.removeField = function (index) {
        $scope.resetOrder(index);
        $scope.selectFinal.splice(index, 1);
        
    };
    $scope.resetOrder = function (index) {
        
        var ordertochk = $scope.selectFinal[index]['fieldOrder'];
        for (var t = 0; t < $scope.selectFinal.length; t++) {
            if (parseInt($scope.selectFinal[t]['fieldOrder']) > parseInt(ordertochk)) {
                $scope.selectFinal[t]['fieldOrder'] = parseInt($scope.selectFinal[t]['fieldOrder']) - 1;
            }
        }

    };
    $scope.custerror1 = '0';
    $scope.checkOrder = function () {
        var orderCheck = 0;

        angular.forEach($scope.selectFinal, function (d, k) {
            for (var t = 0; t < $scope.selectFinal.length; t++) {
                if (d['fieldOrder'] === $scope.selectFinal[t]['fieldOrder'] && k != t) {
                    orderCheck = orderCheck + 1;
                }
            }

        });
        if (orderCheck > 0) {
            $scope.custerror1 = '1';
        } else {
            $scope.custerror1 = '0';
        }
    };
    $scope.custError = '0';
    $scope.checkName = function () {
        var orderCheck = 0;
        angular.forEach($scope.selectFinal, function (d, k) {
            for (var t = 0; t < $scope.selectFinal.length; t++) {
                if (d['name'] !== '' && $scope.selectFinal[t]['name'] !== '' && d['name'] === $scope.selectFinal[t]['name'] && k != t) {
                    orderCheck = orderCheck + 1;
                }
            }

        });
        if (orderCheck > 0) {
            $scope.custError = '1';
        } else {
            $scope.custError = '0';
        }
    };
    $scope.custerror2 = '0';
    $scope.checkDropDown = function () {
        var orderCheck = 0;
        for (var t = 0; t < $scope.selectFinal.length; t++) {
            if ($scope.selectFinal[t]['fieldType'] === 'Dropdown' && $scope.selectFinal[t]['Fvalues'] == '') {
                orderCheck = orderCheck + 1;
            }
        }
        if (orderCheck > 0) {
            $scope.custerror2 = '1';
        } else {
            $scope.custerror2 = '0';
        }
    };
    

    //--VV-- Getting Organaization users --VV----
    $scope.organizationUsers = {};
    $scope.eventmanager = {};
    $scope.eventsDet = {};
    //$scope.organizationId = sessionStorage.getItem('orgId');
    var orgusersresp = $http.get(Data.serviceBase() + 'user/userByOrganization', {params: {"organizationId": $scope.organizationId}});


    orgusersresp.then(function (response) {
        $scope.organizationUsers = response.data;
        angular.forEach(response.data, function (d) {
            if (d.id === $scope.eevent.eventManagerId) {
                console.log(d);
                $scope.eventmanager = d;
            } else if (d.id === sessionStorage.getItem("userId") && $scope.eevent.id === undefined) {
                console.log(d);
                $scope.eventmanager = d;
            }
        });
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });

    $scope.clearCategories = function () {
        $scope.selectedCategories = [];
    };
    $scope.clearServices = function () {
        $scope.selectedServices = [];
    };
    
     // Open custom fields page 
    $scope.openCustomField = function () {
        window.open(Data.webAppBase() + "account/qualifier", '_blank');

    };

    //Email Confiuration
    $scope.greetingEmailConfig = function (eevent) {
    var modalInstance = $uibModal.open({
        templateUrl: './greetingEmailConfigAlert',
        controller: 'editeventCtrl',
        size: 'md',
        backdrop: 'static',
        resolve: {
            eevent: function () {
                return eevent;
            },
            fields: function () {
                return event.fieldVallist;
            },
            services: function () {
                return event.servlist;
            },
            categories: function () {
                return event.categlist;
            }
        }
    });
        modalInstance.result.then(function (i) {
            if (i) {

            }
        });
    };

   $scope.getAttachFileDetails = function (e) {
        $scope.fileError = false;
        $scope.errorMsg = "";
        $scope.files = [];
     //console.log("Getting Files");
        //$scope.files = [];
        var fsize = 0;
        var fileExtension = ['wav', 'mp3', 'amr', 'ogg', 'jpeg', 'jpg', 'txt', 'pdf', 'doc', 'msg', 'xls', 'csv', 'xlsx', 'docx', 'ppt', 'pptx', 'eml', 'tif', 'gif', 'png', 'html', 'rtf', 'mbox', 'odt', 'bmp', 'tiff', 'pps', 'xlr', 'ods', 'wps', 'vcf'];
        $scope.$apply(function () {

            // STORE THE FILE OBJECT IN AN ARRAY.
            for (var i = 0; i < e.files.length; i++) {
                var ftype = e.files[i]['name'].substr(e.files[i]['name'].lastIndexOf('.')+1);
                if ((fileExtension.indexOf(ftype.toLowerCase())) !== -1) {
                    $scope.files.push(e.files[i]);
                    fsize = fsize + parseInt(e.files[i]['size']);
                    $scope.fileError = false;
                } else {

                    $scope.errorFile = e.files[i]['name'];
                    $scope.fileError = true;

                }
            }
            if ($scope.fileError) {
                $scope.files = [];
                document.getElementById("attachFile").value = "";
                angular.element("input[type='file']").val(null);
                $scope.errorMsg = "Unsupported file format&nbsp;<i class='fa fa-info-circle' data-toggle='tooltip' title='Please check the supported formats  " + fileExtension.join() + "'></i>";
            }
            console.log((fsize / 1000) / 1000);
            if ((fsize / 1000) / 1000 > 5) {
                $scope.files = [];
                document.getElementById("attachFile").value = "";
                angular.element("input[type='file']").val(null);
                $scope.errorMsg = "The attachement limit should not exceed 5MB";
            }
            console.log($scope.errorFile);
            console.log($scope.fileError);
        });
    };

  $scope.removeAttachment = function () {
    $scope.files = null;
    $scope.eventemailtemplate.filename = "";
  };      

    $scope.clickedFlag = false;
    $scope.updateevent = function (event, eventemailtemplate, eventmanager,userlist) {
         $scope.clickedFlag = true;
     $scope.organizationId = sessionStorage.getItem('orgId');
        $scope.userId = sessionStorage.getItem('userId');
        $scope.btntxt = 'Updating';
        console.log($scope.selectedServices);
        //console.log("EventDetails");
        //console.log(event);
        console.log($scope.selectedCategories);
        //console.log(eventmanager['id']);
        //console.log($scope.eventemailtemplate);
        /*for (var i = 0; i < $scope.selectedServices.length; i++) {

            if (i < $scope.selectedServices.length - 1) {
                $scope.servicestrList = $scope.servicestrList + $scope.selectedServices[i].toString() + ", ";
            } else {
                $scope.servicestrList = $scope.servicestrList + $scope.selectedServices[i].toString();
            }
            //console.log($scope.services[i]['id']);

        }

        for (var i = 0; i < $scope.selectedCategories.length; i++) {

            if (i < $scope.selectedCategories.length - 1) {
                $scope.categorystrList = $scope.categorystrList + $scope.selectedCategories[i].toString() + ", ";
            } else {
                $scope.categorystrList = $scope.categorystrList + $scope.selectedCategories[i].toString();
            }

        }*/

        if ($scope.files) {
        var data = new FormData();
        for (var i in $scope.files) {
          console.log('File Name '+$scope.files[0]['name']);
               $scope.fileName = $scope.files[0]['name'];
               data.append("filelist[]", $scope.files[i]);
            }

            data.append("orgId", $scope.organizationId);
            data.append("userId", $scope.userId);

            var request = {
                method: 'POST',
                url: Data.serviceBase() + "/visitor/greetingEmailFileUpload/",
                data: data,
                headers: {
                    'Content-Type': undefined
                }
            };
             $http(request)
                    .success(function (result) {
                        console.log("File Sent");
                        $scope.files = [];
                        angular.element("input[type='file']").val(null);
                        document.getElementById("attachFile").value = "";
                        });
          }
        
        for (var i = 0; i < $scope.selectFinal.length; i++) {
            if ($scope.selectFinal[i]['name'] === '') {
                var ordertochk = $scope.selectFinal[i]['fieldOrder'];
                for (var t = 0; t < $scope.selectFinal.length; t++) {
                    if (parseInt($scope.selectFinal[t]['fieldOrder']) > parseInt(ordertochk)) {
                        $scope.selectFinal[t]['fieldOrder'] = parseInt($scope.selectFinal[t]['fieldOrder']) - 1;
                    }
                }

                $scope.selectFinal.splice(i, 1);

            }

        }
        if ($scope.eventOrgDetails.accessRights === '0') {
            if ($scope.eevent.id === undefined) {
                userlist.push(eventmanager);
            }
            if (userlist.length > 0) {
                event.userlist = userlist;
            }

        }

        if ((event.name !== '' && event.name !== null && event.name !== undefined) && ((event.sendGreet == '0' && eventemailtemplate.subject !== '' && eventemailtemplate.subject !== null && eventemailtemplate.userBodyText !== '' && eventemailtemplate.userBodyText !== null) || (event.sendGreet !== '0'))) {

        if (event.id !== undefined) {

            //console.log(event.sendGreet);
            event.organizationId = sessionStorage.getItem('orgId');
            //event.eventManagerId = sessionStorage.getItem('userId');
            console.log($scope.categorystrList);
            event.servlist = $scope.servicestrList;
            event.categlist = $scope.categorystrList;
            event.fieldVallist = $scope.selectFinal;
            event.emailtemplate = eventemailtemplate;
            event.eventManagerId = eventmanager.id;
            event.eventManagerName = eventmanager.firstName + " " + eventmanager.lastName;
            event.eventManagerEmail = eventmanager.email;

            if ($scope.fileName !== undefined) {
              event.emailtemplate.filename = $scope.fileName;
            } else if($scope.fileName == undefined && event.emailtemplate.filename !== null && event.emailtemplate.filename !== ''){
              event.emailtemplate.filename = event.emailtemplate.filename.substring(event.emailtemplate.filename.lastIndexOf('/') + 1);
            } else {
              event.emailtemplate.filename = null;
            }

            Data.put('event/event', event).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    var modalInstance = $uibModal.open({
                    templateUrl: './modifyEvent',
                    controller: 'editeventCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        eevent: function () {
                            return event;
                        },
                        fields: function () {
                            return event.fieldVallist;
                        },
                        services: function () {
                            return event.servlist;
                        },
                        categories: function () {
                            return event.categlist;
                        }
                    }
                });
                    console.log('Setting the event ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    console.log(result);
                }
            });

        } else {

            if (event.name !== undefined) {

                event.organizationId = sessionStorage.getItem('orgId');
                event.eventManagerId = eventmanager.id;
                event.servlist = $scope.servicestrList;
                event.categlist = $scope.categorystrList;
                event.fieldVallist = $scope.selectFinal;
                event.emailtemplate = eventemailtemplate;
                event.eventManagerName = eventmanager.firstName + " " + eventmanager.lastName;
                event.eventManagerEmail = eventmanager.email;
                $scope.sdate = event.startDate;
                event.startDate = $filter('date')($scope.sdate, "yyyy-MM-dd HH:mm:ss");
                $scope.edate = event.endDate;
                event.endDate = $filter('date')($scope.edate, "yyyy-MM-dd HH:mm:ss");
                console.log(event);
                console.log("About to submit");
                if ($scope.fileName !== undefined) {
                  event.emailtemplate.filename = $scope.fileName;
                } else {
                  event.emailtemplate.filename = null;
                }
                Data.post('event/event', event).then(function (result) {
                    if (result.status !== 'error') {
                        console.log(result);
                        $uibModalInstance.close(1);
                        event.id = result.eventId;

                        //console.log('Setting the Event ')
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                        // var modalInstance = $uibModal.open({
                        //     templateUrl: './generateEventKey',
                        //     controller: 'genEventKeyCtrl',
                        //     size: 'lg',
                        //     backdrop: 'static',
                        //     resolve: {
                        //         eevent: function () {
                        //             return event;
                        //         },
                        //     }
                        // });
                        // modalInstance.result.then(function (i) {
                        //     if (i) {

                        //     }
                        // });
                    } else {
                        $uibModalInstance.close(1);
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                        console.log(result);
                    }
                });

            } 

            // else {
            //  if (event.name == null || event.name == '') {
            //       $scope.clickedFlag = false;
                     // $scope.btntxt = 'Save';
            //       toaster.pop("error", "", "Please enter the list name", 10000, 'trustedHtml');
            //  } else if (event.sendGreet == '0') {
            //      if (eventemailtemplate.subject == null || eventemailtemplate.subject == '') {
            //       $scope.clickedFlag = false;
                     // $scope.btntxt = 'Save';
            //       toaster.pop("error", "", "Please enter the greeting email subject", 10000, 'trustedHtml');
            //      }
            //      if (eventemailtemplate.userBodyText == null || eventemailtemplate.userBodyText == '') {
            //       $scope.clickedFlag = false;
                     // $scope.btntxt = 'Save';
            //       toaster.pop("error", "", "Please enter the greeting email content message", 10000, 'trustedHtml');
            //      }
            //  }
            // }

        }

    } else {
                if (event.name == null || event.name == '') {
                     $scope.clickedFlag = false;
                     if (event.id !== undefined) {
                         $scope.btntxt = 'Update';
                     } else {
                        $scope.btntxt = 'Save';
                     }
                     toaster.pop("error", "", "Please enter the list name", 10000, 'trustedHtml');
                } else if (event.sendGreet == '0') {
                    if (eventemailtemplate.userBodyText == null || eventemailtemplate.userBodyText == '') {
                     $scope.clickedFlag = false;
                     if (event.id !== undefined) {
                         $scope.btntxt = 'Update';
                     } else {
                        $scope.btntxt = 'Save';
                     }
                     toaster.pop("error", "", "Please enter the greeting email content message", 10000, 'trustedHtml');
                    }
                    if (eventemailtemplate.subject == null || eventemailtemplate.subject == '') {
                     $scope.clickedFlag = false;
                     if (event.id !== undefined) {
                         $scope.btntxt = 'Update';
                     } else {
                        $scope.btntxt = 'Save';
                     }
                     toaster.pop("error", "", "Please enter the greeting email subject", 10000, 'trustedHtml');
                    }   
                }
            }
    };

    //Clone or Copy
    $scope.addcloneevent = function (event, eventemailtemplate, eventmanager,userlist) {
        $scope.clickedFlag = true;
        $scope.btntxt = 'Cloning';        
        for (var i = 0; i < $scope.selectFinal.length; i++) {
            if ($scope.selectFinal[i]['name'] === '') {
                var ordertochk = $scope.selectFinal[i]['fieldOrder'];
                for (var t = 0; t < $scope.selectFinal.length; t++) {
                    if (parseInt($scope.selectFinal[t]['fieldOrder']) > parseInt(ordertochk)) {
                        $scope.selectFinal[t]['fieldOrder'] = parseInt($scope.selectFinal[t]['fieldOrder']) - 1;
                    }
                }
                $scope.selectFinal.splice(i, 1);
            }
        }
        if ($scope.eventOrgDetails.accessRights === '0') {
            if ($scope.eevent.id === undefined) {
                userlist.push(eventmanager);
            }
            if (userlist.length > 0) {
                event.userlist = userlist;
            }
        }
        if ((event.name !== '' && event.name !== null && event.name !== undefined) && ((event.sendGreet == '0' && eventemailtemplate.subject !== '' && eventemailtemplate.subject !== null && eventemailtemplate.userBodyText !== '' && eventemailtemplate.userBodyText !== null) || (event.sendGreet !== '0'))) {
            if (event.name !== undefined) {
                event.organizationId = sessionStorage.getItem('orgId');
                event.eventManagerId = eventmanager.id;
                event.servlist = $scope.servicestrList;
                event.categlist = $scope.categorystrList;
                event.fieldVallist = $scope.selectFinal;
                event.emailtemplate = eventemailtemplate;
                event.eventManagerName = eventmanager.firstName + " " + eventmanager.lastName;
                event.eventManagerEmail = eventmanager.email;
                $scope.sdate = event.startDate;
                event.startDate = $filter('date')($scope.sdate, "yyyy-MM-dd HH:mm:ss");
                $scope.edate = event.endDate;
                event.endDate = $filter('date')($scope.edate, "yyyy-MM-dd HH:mm:ss");
                console.log(event);
                console.log("About to submit");
                Data.post('event/eventClone', event).then(function (result) {
                    if (result.status !== 'error') {
                        console.log(result);
                        $uibModalInstance.close(1);
                        event.id = result.eventId;
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                        // var modalInstance = $uibModal.open({
                        //     templateUrl: './generateEventKey',
                        //     controller: 'genEventKeyCtrl',
                        //     size: 'lg',
                        //     backdrop: 'static',
                        //     resolve: {
                        //         eevent: function () {
                        //             return event;
                        //         },
                        //     }
                        // });
                        // modalInstance.result.then(function (i) {
                        //     if (i) {

                        //     }
                        // });
                    } else {
                        $uibModalInstance.close(1);
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                        console.log(result);
                    }
                });
            } else {
                     $scope.clickedFlag = false;
                     $scope.btntxt = 'Clone';
                     toaster.pop("error", "", "Please enter the list name", 10000, 'trustedHtml');
            }
            } else {
                if (event.name == null || event.name == '') {
                     $scope.clickedFlag = false;
                     $scope.btntxt = 'Clone';
                     toaster.pop("error", "", "Please enter the list name", 10000, 'trustedHtml');
                } else if (event.sendGreet == '0') {
                    if (eventemailtemplate.userBodyText == null || eventemailtemplate.userBodyText == '') {
                     $scope.clickedFlag = false;
                     $scope.btntxt = 'Clone';
                     toaster.pop("error", "", "Please enter the greeting email content message", 10000, 'trustedHtml');
                    }
                    if (eventemailtemplate.subject == null || eventemailtemplate.subject == '') {
                     $scope.clickedFlag = false;
                     $scope.btntxt = 'Clone';
                     toaster.pop("error", "", "Please enter the greeting email subject", 10000, 'trustedHtml');
                    }   
                }
            }
    };
    //End Clone or Copy


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});

evtApp.controller('deleventCtrl', function ($scope, Data, $uibModalInstance, devent, toaster) {

    $scope.devent = devent;
    $scope.title = "Delete List";


    $scope.deleteevent = function (event) {
        if (event.name !== undefined && event.name !== "Default Event") {
            Data.delete('event/event/' + event.id).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    // $uibModalInstance.dismiss('Close');
                    console.log('Setting the event ')
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    console.log(result);
                }
            });
        } else if (event.name !== undefined && event.name === "Default Event")
        {
            console.log("Default Event");
            Data.delete('event/eventdemo/' + event.id).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    // $uibModalInstance.dismiss('Close');
                    console.log('Setting the event ')
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });
        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
//----------------------------------------------- Show List Activity Log -------------------------------------------------- 


evtApp.controller('listActivityCtrl', function ($scope, Data, $http, $uibModalInstance, laevent, toaster) {

    $scope.levent = laevent;
    $scope.title = "Change History for List " + $scope.levent.name;
    $scope.listActivities = {};
    $scope.eventActivity = function (event) {
        var activityResp = $http.get(Data.serviceBase() + 'event/listAcitivites', {params: {"eventId": $scope.levent.id}});


        activityResp.then(function (response) {
            console.log(response.data);
            $scope.listActivities = response.data;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

//-----------------------------------------------End Show List Activity Log -------------------------------------------------- 


//--------------------------------------------------- Show business Card from profile ----------------------------------------

evtApp.controller('showBizCtrl', function ($scope, Data,$rootScope, $http, $uibModalInstance, laevent, toaster) {

    $scope.levent = laevent;
    $scope.title = "Your Business Card ";
    var userId = sessionStorage.getItem("userId");
    $scope.userDet = {};
    
    $scope.editUserProfile = function(){
        $uibModalInstance.dismiss('Close');
    };
   // $rootScope.progressVal
    $scope.getBusinessCard = function () {
        var userResp = $http.get(Data.serviceBase() + 'user/user', {params: {"key":"id","value":userId}});


        userResp.then(function (response) {
            console.log(response.data);
            $scope.userDet = response.data[0];
            $rootScope.checkProgress($scope.userDet);

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

//------------------------------------------------------ end -------------------------------------------------------------------



evtApp.controller('eventVistorCtrl', function ($scope, Data, $uibModalInstance, devent, toaster) {

    $scope.devent = devent;
    $scope.title = "Delete Event";
    $scope.deleteevent = function (event) {

        if (event.name !== undefined) {

            Data.delete('event/event/' + event.id).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    // $uibModalInstance.dismiss('Close');
                    console.log('Setting the event ')
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

//-------------------------------- Client Bulk Email-------------------------------------------------------------------

evtApp.controller('emailBulkCtrl', function ($scope, $rootScope, checkPremium, trialPopUp, alertUser, $uibModal, Data, toaster, $http) {

    $scope.email = {};
    $scope.eventSelected = {};
    $scope.showbox = false;
    $scope.plan = "";
    $scope.visitorName = "";
    $scope.listName = "";
    $rootScope.addButtonhtml = '&nbsp;';
    sessionStorage.setItem('menuact', '5');
    $rootScope.menuact = '5';
    //$rootScope.addButtonhtml += '<button ng-show="eventBut" tooltip-placement="bottom" tooltip="Clear demo event before create an event" class="btn btn-danger btn-sm" ng-click="delevent(eventsDet);"><span class="fa fa-trash"></span> Clear Demo Event</button>&nbsp;';
    $rootScope.addButtonhtml += '<button class="btn btn-default btn-sm btn-circle" popover="Send Personalized smart Follow up email using Hello Email" popover-title="Hello Mail" popover-trigger="focus" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    $rootScope.title = "Hello Email";
    $scope.visitorCName = "";
    $scope.toId = "";
    $scope.emailTemps = [];
    $scope.eventLists = {};
    $scope.leadList = [];
    $scope.emailTotal = 0;
    $scope.btntxt = "Send";
    $scope.btnptxt = "Send";
    $scope.emailFilter = {};
    $scope.emailMetrics = [];
    $scope.visitorList = {};
    $scope.loader = false;
    $scope.mailCount = 0;
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.userEmail = sessionStorage.getItem('userEmail');
    $scope.userName = sessionStorage.getItem('firstName') + " " + sessionStorage.getItem('lastName');
    $scope.emailFilter.replyTo = $scope.userEmail;
    $scope.emailFilter.fromName = $scope.userName;
    $scope.emailFilter.fromEmail = $scope.userEmail;
    $scope.emailFilter.body = "";
    $scope.qtab = 1;
    angular.element(document.getElementById("emailCamp")).addClass("active");
    angular.element(document.getElementById("metric")).removeClass("active");
     $scope.tinymceOptions = {
        selector: 'textarea',
        statusbar: false,
        entity_encoding: "UTF-8",
        elementpath: false,
        height: 375,
        theme: 'modern',
        menubar: true,
        autosave_ask_before_unload: false,
        paste_data_images: true,
        forced_root_block_attrs: {
            "class": "myclass",
            "data-something": "my data",
            "style": "margin: 5px 0;"
        },
        // images_upload_handler: function (blobInfo, success, failure) {
  //           success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64());
  //       },
        plugins: [
            'advlist autolink lists link image noneditable preview hr pagebreak table textcolor imagetools ',
            'searchreplace wordcount visualblocks visualchars code paste lineheight emoticons ',
            'insertdatetime media nonbreaking save table colorpicker fullscreen contextmenu directionality'
        ],
        lineheight_formats: "LineHeight 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
        toolbar1: 'undo redo | styleselect | fontselect fontsizeselect | bold italic',
        toolbar2: 'forecolor backcolor | lineheightselect | bullist numlist | link uploadimage emoticons | alignleft aligncenter alignright alignjustify indent outdent',
        toolbar3: 'mybutton,mybutton1 signbutton',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            "https://beta.helloleads.io/css/tinymcecontent.css"],
        content_style: ".tinymce-content p {  padding: 0; margin: 1px 0; }",
        
        setup: function (editor) {
            editor.addButton('mybutton', {
                text: '#VisitorName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorName#');
                }
            });
            editor.addButton('mybutton1', {
                text: '#VisitorCompanyName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorCompanyName#');
                }
            });
            /* editor.addButton('signbutton', {
             text: 'Signature',
             title: 'Signature',
             type: 'menubutton',
             icon: false,
             menu: [
             {
             text: '#firstname',
             icon: false,
             onclick: function () {
             editor.insertContent('#firstname#');
             }
             },
             {
             text: '#lastname',
             icon: false,
             onclick: function () {
             editor.insertContent('#lastname#');
             }
             
             },
             {
             text: '#designation',
             icon: false,
             onclick: function () {
             editor.insertContent('#designation#');
             }
             },
             {
             text: '#company',
             icon: false,
             onclick: function () {
             var orgname = sessionStorage.getItem('cOrgName');
             editor.insertContent(orgname);
             }
             },
             {
             text: '#email',
             icon: false,
             onclick: function () {
             editor.insertContent('#email#');
             }
             },
             {
             text: '#mobile',
             icon: false,
             onclick: function () {
             editor.insertContent('#mobile#');
             }
             },
             ]
             });*/
        }
    };
    // $scope.emailFilter.replyTo = $scope.emailFilter.emailId;
    $scope.clearFilter = function () {
        $scope.emailFilter.length = 0;

    };
    $scope.showBox = function () {
        if ($scope.showbox) {
            $scope.showbox = false;
        } else {
            $scope.showbox = true;
        }

    };
    $scope.setReplyTo = function () {
        $scope.emailFilter.replyTo = $scope.emailFilter.emailId;//just to show that it is calculated field
    };
    $scope.orgPayemnt = function () {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "bulkEmail", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    console.log(response.data);
                    if (response.data.status != 'success') {
                        $scope.orgPlanCheck(response.data);

                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.addVisitorName = function (el) {
        console.log(el);
        //$rootScope.$broadcast('add', "VisitorName"); 
        $scope.messages = $scope.messages + "VisitorName"
    };

    $scope.addVisitor = function (el) {
        console.log(el);
        //$rootScope.$broadcast('add', "VisitorName"); 
        $scope.emailFilter.body = $scope.emailFilter.body + "VisitorName"
    };
    $scope.orgPlanCheck = function (p, size) {
        var modalInstance1 = $uibModal.open({
            templateUrl: './checkPremiumPopup',
            controller: 'premiumCheckCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                msg: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };


    $scope.switchcampTab = function (tabId) {
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
        if (tabId === 1) {

            $scope.qtab = 1;
            angular.element(document.getElementById("emailCamp")).addClass("active");
            angular.element(document.getElementById("metric")).removeClass("active");
             Data.post('event/setEmailTabOne', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else if (tabId === 2) {
            $scope.qtab = 2;
            angular.element(document.getElementById("emailCamp")).removeClass("active");
            angular.element(document.getElementById("metric")).addClass("active");
             Data.post('event/setEmailTabTwo', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        }
    };

    $scope.getAllOrgEvents = function () {
        var eventresp = $http.get(Data.serviceBase() + '/event/event', {params: {"key": "organizationId", "value": $scope.organizationId}});
        eventresp.then(function (response) {
            //console.log(response.data);
            $scope.eventLists = response.data;
            //console.log("Current event1" + $rootScope.currentEventId);
            angular.forEach($scope.eventLists, function (d) {
                if (d.id === localStorage.getItem('curEventId')) {
                    //localStorage.setItem("filters","");
                    $scope.filterEventSelected = d;
                    $scope.adfilter = true;
                    $scope.filterby = true;
                    $scope.filtersadded.event = $rootScope.currentEventName;
                } else if ($scope.filtersadded !== undefined) {
                    if (d.id === $scope.filtersadded.eventId) {
                        $scope.filterEventSelected = d;
                        $scope.adfilter = true;
                        $scope.filterby = true;
                        $scope.filtersadded.event = d.name;
                    }
                }

            });

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.resetEvent = function () {
        $scope.eventSelected = {};
        $scope.emailFilter.maillist = {};
        $scope.emailTemps.length = 0;
        $scope.emailTotal = 0;
    };
    $scope.getAllOrgTemplate = function () {
        $scope.checkAccountMailCount();
        var resprom = $http.get(Data.serviceBase() + 'organization/emailTemplate', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.emailTemplates = response.data;
            $scope.emailTemplateDet = "";
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.getEmailMetrics = function () {
        var resprom = $http.get(Data.serviceBase() + 'organization/emailMetrics', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            console.log(response.data);
            $scope.emailMetrics = response.data;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };  

    $scope.getAttachFileDetails = function (e) {
        $scope.fileError = false;
        $scope.errorMsg = "";
        $scope.files = [];
     //console.log("Getting Files");
        //$scope.files = [];
        var fsize = 0;
        var fileExtension = ['wav', 'mp3', 'amr', 'ogg', 'jpeg', 'jpg', 'txt', 'pdf', 'doc', 'msg', 'xls', 'csv', 'xlsx', 'docx', 'ppt', 'pptx', 'eml', 'tif', 'gif', 'png', 'html', 'rtf', 'mbox', 'odt', 'bmp', 'tiff', 'pps', 'xlr', 'ods', 'wps', 'vcf'];
        $scope.$apply(function () {

            // STORE THE FILE OBJECT IN AN ARRAY.
            for (var i = 0; i < e.files.length; i++) {
                var ftype = e.files[i]['name'].substr(e.files[i]['name'].lastIndexOf('.')+1);
                console.log(e.files[i]['size']);
                if ((fileExtension.indexOf(ftype.toLowerCase())) !== -1) {
                    $scope.files.push(e.files[i]);
                    fsize = fsize + parseInt(e.files[i]['size']);
                    $scope.fileError = false;
                } else {

                    $scope.errorFile = e.files[i]['name'];
                    $scope.fileError = true;

                }
            }
            if ($scope.fileError) {
                $scope.files = [];
                document.getElementById("attachFile").value = "";
              angular.element("input[type='file']").val(null);
                $scope.errorMsg = "Unsupported file format&nbsp;<i class='fa fa-info-circle' data-toggle='tooltip' title='Please check the supported formats  " + fileExtension.join() + "'></i>";
            }
            console.log((fsize / 1000) / 1000);
            if ((fsize / 1000) / 1000 > 5) {
                $scope.files = [];
                document.getElementById("attachFile").value = "";
                angular.element("input[type='file']").val(null);
                $scope.errorMsg = "The attachement limit should not exceed 5MB";
            }
            console.log($scope.errorFile);
            console.log($scope.fileError);
        });
    };

    $scope.removeAttachment = function () {
      $scope.files = null;
      document.getElementById("attachFile").value = "";
      angular.element("input[type='file']").val(null);
    };

    
    $scope.changecontentEmail = function () {
        //console.log($scope.emailTemplateDet);
          $scope.userDet = {};
        var resprom = $http.get(Data.serviceBase() + 'user/user', {params: {"key":"id","value": $scope.userId}});
        resprom.then(function (response) {
            //console.log(response.data);
            $scope.userDet = response.data[0];
            //$scope.emailTemplateDet = "";
            //console.log($scope.emailTemplateDet);
            var etemp = JSON.parse($scope.emailTemplateDet);
            $scope.emailFilter.subject = etemp.subject;
           // console.log(etemp.body);
            var message = etemp.body;
            var regf = /#firstname#/gi;
            var regl = /#lastname#/gi;
            var regdes = /#designation#/gi;
            var regem = /#email#/gi;
            var regmob = /#mobile#/gi;
            message = message.replace(regf,$scope.userDet['firstName']);
            message = message.replace(regl,$scope.userDet['lastName']);
            message = message.replace(regdes,$scope.userDet['designation']);
            message = message.replace(regem,$scope.userDet['email']);
            message = message.replace(regmob,$scope.userDet['phoneCode']+$scope.userDet['phone']);
            //console.log(message);
            $scope.emailFilter.body = message;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.redirectEtToTemp = function () {
        window.open(Data.webAppBase() + "account/emailTemplate", '_blank');
    };
    $scope.mailCount = 0;
    $scope.emailfiltersadded = {};
    $scope.visitorList = {};
    $scope.filteremailvisitor = function (size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './filterVisitor',
            controller: 'filterEmailVisitorCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                eventId: function () {
                    return 0;
                },
                eventName: function () {
                    return "";
                }
            }
        });
        modalInstance1.result.then(function (results) {
            // //console.log(results.searchFilter);
            // //console.log(results.visitors);
            /* if (results.visitors) {
             $scope.loader = true;
             $scope.adfilter = true;
             $scope.searchvisitors(results.visitors);
             }*/

            if (results.searchFilter) {
                $scope.filterby = true;
                ////console.log("Search Filter");
                // //console.log(results.searchFilter);
                // $scope.clearFilter();
                $scope.emailfiltersadded = {};
                $scope.emailfiltersadded = results.searchFilter;
                console.log($scope.emailfiltersadded.emailStatus);
                if($scope.emailfiltersadded.emailStatus === undefined){
                    $scope.emailfiltersadded.emailStatus = 'active';
                }
                var respc = $http.get(Data.serviceBase() + '/visitor/filterEmailVisitor', {params: {"organizationId": $scope.organizationId, "cgroups": $scope.emailfiltersadded.cgroup, "interests": $scope.emailfiltersadded.service, "potential": $scope.emailfiltersadded.potential, "eventId": $scope.emailfiltersadded.eventId, "assignto": $scope.emailfiltersadded.assignToId, "assigne": $scope.emailfiltersadded.assigneeId, "capture": $scope.emailfiltersadded.capturedId, "type": $scope.emailfiltersadded.stageIds, "designation": $scope.emailfiltersadded.designation, "company": $scope.emailfiltersadded.company, "country": $scope.emailfiltersadded.country, "city": $scope.emailfiltersadded.city, "state": $scope.emailfiltersadded.state, "fromDate": $scope.emailfiltersadded.fromDate, "toDate": $scope.emailfiltersadded.toDate, "drfromDate": $scope.emailfiltersadded.drfromDate, "drtoDate": $scope.emailfiltersadded.drtoDate, "dontFollow": $scope.emailfiltersadded.dontFollow, "notes": $scope.emailfiltersadded.notes, "tags": $scope.emailfiltersadded.tags, "notAssigned": $scope.emailfiltersadded.notAssigned, "searchquery": "", "deal": $scope.emailfiltersadded.deal, "dealComp": $scope.emailfiltersadded.dealComp, "customList": JSON.stringify($scope.emailfiltersadded.custList),"emailStatus":$scope.emailfiltersadded.emailStatus}});
                respc.then(function (response) {
                    //console.log(response.data);
                    $scope.visitorList = response.data.visitors;


                }, function (response) {
                    //console.log('Error happened -- ');
                    //console.log(response);
                });
            } else {
                $scope.emailfiltersadded = {};
                $scope.visitorList = {};
            }

        });
    };

    $scope.eventEmailChanged = function (eventDet) {
        $scope.loader = true;
        var eventresp = $http.get(Data.serviceBase() + '/visitor/bulkEmailFilter', {params: {"eventId": eventDet.id, "orgId": $scope.organizationId}});
        eventresp.then(function (response) {
            $scope.loader = false;
            $scope.emailFilter.maillist = response.data.visitorDet;
            $scope.emailTemps = response.data.visitorDet;
            $scope.emailTotal = response.data.total;
            console.log(1000 - parseInt($scope.mailCount));
            if ($scope.emailTotal > (1000 - parseInt($scope.mailCount))) {
                console.log("Checking");
                $scope.message = {"title": "Warning", "message": "You are running out of daily Email credits. To send bulk email, kindly get back tomorrow."};
                alertUser.alertpopmodel($scope.message);
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.checkAccountMailCount = function () {
        var mailcountresp = $http.get(Data.serviceBase() + '/visitor/checkMailCount', {params: {"orgId": $scope.organizationId}});
        mailcountresp.then(function (response) {
            console.log(response);
            $scope.mailCount = response.data.count;
            //$scope.mailCount = '950';
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    };
    /*$scope.SendMailcancel = function () {
     $scope.emailFilter['fromName'] ="";
     $scope.emailFilter['subject'] ="";
     $scope.emailFilter['body'] ="";
     Data.post('admin/bulkEmail', $scope.emailFilter).then(function (result) {
     if (result.status !== 'error' && result.status === 'success1') {
     
     $scope.message = {"title":"Authorization failure","message": "Campaign Cancelled"};
     alertUser.alertpopmodel($scope.message);
     $scope.btntxt = 'Send';
     
     } else {
     console.log(result);
     }
     });
     };*/
    $scope.files = [];
    $scope.SendMailConfirm = function (p, size) {
        var visitorCount = 0;
        $scope.organizationId = sessionStorage.getItem('orgId');
        $scope.userId = sessionStorage.getItem('userId');
        if ($scope.files) {
        var data = new FormData();
        for (var i in $scope.files) {
          console.log('File Name '+$scope.files[0]['name']);
               $scope.fileName = $scope.files[0]['name'];
               data.append("filelist[]", $scope.files[i]);
            }

            data.append("orgId", $scope.organizationId);
            data.append("userId", $scope.userId);

            var request = {
                method: 'POST',
                url: Data.serviceBase() + "/visitor/emailFileUpload/",
                data: data,
                headers: {
                    'Content-Type': undefined
                }
            };
             $http(request)
                    .success(function (result) {
                        console.log("File Sent");
                        $scope.files = [];
                        angular.element("input[type='file']").val(null);
                        document.getElementById("attachFile").value = "";
                        });
          }
        if ($scope.visitorName) {
            $scope.listName = '';
            p.maillist = [{"name": $scope.visitorName, "email": $scope.toId, "company": $scope.visitorCName, "eventName": $scope.listName}];
            $scope.emailTemps = [{"name": $scope.visitorName, "email": $scope.toId, "company": $scope.visitorCName, "eventName": $scope.listName}]
            visitorCount = 1;
        } else {
            visitorCount = $scope.visitorList.length;
        }
        //if ((p.maillist !== undefined && p.maillist.length <= (500 - parseInt($scope.mailCount)))) {
        var modalInstance1 = $uibModal.open({
            templateUrl: './confirmMessage',
            controller: 'sendClientEmailCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                maillist: function () {
                    p.leadsCount = visitorCount;
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i === 1) {
                if (!$scope.visitorCName && !$scope.visitorName) {
                    $scope.btntxt = 'Sending';
                    $scope.emailFilter.organizationId = $scope.organizationId;
                    $scope.emailFilter.userId = $scope.userId;
                    $scope.emailFilter.visitorlist = $scope.visitorList.join();
                    if ($scope.visitorList.length > (1000 - parseInt($scope.mailCount))) {
                        console.log("Checking");
                        $scope.message = {"title": "Warning", "message": "You are running out of daily Email credits. To send bulk email, kindly get back tomorrow."};
                        alertUser.alertpopmodel($scope.message);
                    } else {
                        if ($scope.fileName !== undefined) {
                          $scope.emailFilter.filename = $scope.fileName;
                        } else {
                          $scope.emailFilter.filename = null;
                        }
                        Data.post('visitor/sepVisitorEmail', $scope.emailFilter).then(function (result) {
                            if (result.status !== 'error' && result.status !== 'success1') {
                                $scope.visitorName = "";
                                $scope.toId = "";
                                $scope.visitorCName = "";
                                $scope.listName = "";
                                $scope.message = {"title": "Success", "message": $scope.visitorList.length + " email(s) with subject <b>''" + $scope.emailFilter.subject + "''</b> has been sent successfully"};
                                alertUser.alertpopmodel($scope.message);
                                $scope.btntxt = 'Send';

                            } else {
                                console.log(result);
                            }
                        });
                    }
                }
             else {
                $scope.btnptxt = 'Sending';
                $scope.emailFilter.organizationId = $scope.organizationId;
                $scope.emailFilter.userId = $scope.userId;
                $scope.emailFilter.maillist = p.maillist;
                        $scope.emailFilter.fromEmail = p.fromEmail;
                if ($scope.fileName !== undefined) {
                  $scope.emailFilter.filename = $scope.fileName;
                } else {
                  $scope.emailFilter.filename = null;
                }
                console.log($scope.emailFilter);
                Data.post('visitor/bulkVisitorEmail', $scope.emailFilter).then(function (result) {
                    if (result.status !== 'error' && result.status !== 'success1') {
                        $scope.visitorName = "";
                        $scope.toId = "";
                        $scope.visitorCName = "";
                        $scope.listName = "";
                        $scope.message = {"title": "Success", "message":  "1 emails with subject " + $scope.emailFilter.subject + " has been sent successfully"};
                        alertUser.alertpopmodel($scope.message);
                        $scope.btnptxt = 'Send';

                    } else {
                        console.log(result);
                    }
                });
            }
        }
        });
        /*} else {
         $scope.visitorName = "";
         $scope.toId = "";
         $scope.visitorCName = "";
         $scope.listName = "";
         $scope.message = {"title": "Warning", "message": "You are running out of daily Email credits. To send bulk email, kindly get back tomorrow."};
         alertUser.alertpopmodel($scope.message);
         }*/

    };
    //$scope.csmtp = {};
    $scope.configSMTP = function (mclick) {
         $scope.csmtp = {
        "incomingServerPort" : 25,
        "outgoingServerPort" : 143,
    };
    var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var smtpConfigresp = $http.get(Data.serviceBase() + '/organization/orgSMTP', {params: {"organizationId": $scope.organizationId,"userId": $scope.userId}});
        smtpConfigresp.then(function (response) {
            console.log(response.data);
            $scope.csmtp = response.data[0];
            $scope.csmtp1 = response.data[0];
            if($scope.csmtp){
                if (!$scope.csmtp.incomingServerPort) {
                    $scope.csmtp.incomingServerPort = 25;
                }
                if (!$scope.csmtp.outgoingServerPort) {
                    $scope.csmtp.outgoingServerPort = 143;
                }
            }else{
                $scope.csmtp={"incomingServerPort":25,"outgoingServerPort":143};
            }
            if (mclick === '1') {
                var smtpmodalInstance = $uibModal.open({
                    templateUrl: './mailConfigPopUp',
                    controller: 'smtpConfigCtrl',
                    size: "md",
                    backdrop: 'static',
                    resolve: {
                        mailConfig: function () {
                            return $scope.csmtp;
                        }
                    }
                });
                smtpmodalInstance.result.then(function (i) {
                    if (i === 1) {

                    }
                });
            }

            if ($scope.csmtp1 !== undefined) {
                $scope.emailFilter.fromEmail = sessionStorage.getItem('userEmail');
            } else {
                $scope.emailFilter.fromEmail = "hello@helloleads.io";
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };


});
evtApp.controller('smtpConfigCtrl', function ($scope, $http, Data, $uibModalInstance, mailConfig, toaster) {
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.csmtp = mailConfig;
    $scope.dverify = true;
    $scope.tstbtn = "";


    $scope.checkDomain = function (csmtp) {
        console.log(csmtp);
        //csmtp.organizationId = $scope.organizationId;
        $scope.tstbtn = '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>';
        var smtpConfigresp = $http.get(Data.webAppBase() + '/account/checkHost', {params: {"incomingServer": csmtp.incomingServer,"outgoingServer": csmtp.outgoingServer,"incomingServerPort": csmtp.incomingServerPort,"outgoingServerPort": csmtp.outgoingServerPort, "emailId": csmtp.emailId, "emailPassword": csmtp.emailPassword, "outgoingEncryptMethod": '0'}});
        smtpConfigresp.then(function (result) {
            //console.log(result);
            var resp = result.data;
            if (resp.status === 'success') {
                $scope.dverify = false;
                $scope.tstbtn = "<i class='fa fa-check-circle-o'></i> &nbsp;SMTP configuration verified";

            } else if (resp.status === 'error') {
                $scope.dverify = true;
                $scope.tstbtn = "Please provide valid SMTP configuration settings";
            }
        });

    };

    $scope.updateSMTP = function (csmtp) {
        $scope.dverify = true;
        csmtp.organizationId = $scope.organizationId;
        csmtp.userId = $scope.userId;
        Data.post('organization/updateSMTPConfig', csmtp).then(function (result) {
            if (result.status !== 'error' && result.status !== 'success1') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

            } else {
                console.log(result);
            }
        });

        $uibModalInstance.close(1);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
     $scope.etypetxt = 'password';
    $scope.viewemailpass = function () {
        if ($scope.etypetxt === 'password') {
            $scope.etypetxt = 'text';
        } else {
            $scope.etypetxt = 'password';
        }
    };

});
evtApp.controller('filterEmailVisitorCtrl', function ($location, $interval, $rootScope, eventId, eventName, $uibModal, $uibModalInstance, toaster, $scope, $http, Data) {

    $scope.currenteventId = eventId;
    $scope.currentEventName = eventName;
    //console.log("EventID");
    //console.log(eventId);
    $scope.categorySelected = [];
    $scope.categoryCount = 0;
    $scope.interestCount = 0;
    $scope.assignToCount = 0;
    $scope.assigneCount = 0;
    $scope.captureCount = 0;
    $scope.eventCount = 0;
    $scope.potentialCount = 0;
    $scope.LeadstageCount = 0;
    $scope.serviceSelected = [];
    $scope.potentialSelected = [];
    $scope.LeadstageSelected = [];
    $scope.eventSelected = [];
    $scope.userassignToSelected = [];
    $scope.userassigneSelected = [];
    $scope.usercaptureSelected = [];
    $scope.searchf = {};
    $scope.cgroups = "";
    $scope.interests = "";
    $scope.potential = "";
    $scope.stage = "";
    $scope.eventslist = "";
    $scope.usersassignTo = "";
    $scope.compVal = "";
    $scope.dealVal = "";
    $scope.usersassigne = "";
    $scope.userscapture = "";
    $scope.cfFilters = [];
    $scope.organizationId = sessionStorage.getItem("orgId");
    this.setInputFocus = function () {
        $scope.$broadcast('UiSelectDemo1');
    };
    $scope.selectedCityList = [];
    var serviceresp = $http.get(Data.serviceBase() + '/service/service', {params: {"key": "organizationId", "value": $scope.organizationId}});
    serviceresp.then(function (response) {
        //console.log(response.data);
        $scope.serviceLists = response.data;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    var categoryresp = $http.get(Data.serviceBase() + '/category/category', {params: {"key": "organizationId", "value": $scope.organizationId}});
    categoryresp.then(function (response) {
        //console.log(response.data);
        $scope.categoryLists = response.data;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    if ($scope.currenteventId === 0) {
        var eventresp = $http.get(Data.serviceBase() + '/event/event', {params: {"key": "organizationId", "value": $scope.organizationId}});
        eventresp.then(function (response) {
            //console.log(response.data);
            $scope.eventLists = response.data;
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    }
    var userresp = $http.get(Data.serviceBase() + '/user/user', {params: {key: "organizationId", "value": $scope.organizationId}});
    $scope.userList = {};
    userresp.then(function (response) {
        $scope.userList = response.data;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });

    // Get the custom lead stages -----------------
    var stageresp = $http.get(Data.serviceBase() + '/organization/getCurrentStages', {params: {"organizationId": $scope.organizationId}});
    stageresp.then(function (response) {
        console.log(response.data);
        $scope.Lstages = response.data;


    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });

    // ----------------------------------------------------- Populating the auto suggesting leads -------------------------
    $scope.changeCity = function (city) {
        $scope.cityList = [];
        var cityresp = $http.get(Data.serviceBase() + '/visitor/cityDetails', {params: {"organizationId": $scope.organizationId, "city": city}});

        cityresp.then(function (response) {

            // $scope.cityList = response.data;
            // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
            angular.forEach(response.data, function (value, key) {

                if (value.city !== null && $scope.cityList.indexOf(value.city) == -1) {
                    //categoryId.push(value.id);
                    $scope.cityList.push(value.city);
                }


            });
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    }
    $scope.stateList = [];
    var stateresp = $http.get(Data.serviceBase() + '/visitor/stateDetails', {params: {"organizationId": $scope.organizationId}});

    stateresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.state !== null && $scope.stateList.indexOf(value.state) == -1) {
                //categoryId.push(value.id);
                $scope.stateList.push(value.state);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    $scope.countryList = [];
    var countryresp = $http.get(Data.serviceBase() + '/visitor/countryDetails', {params: {"organizationId": $scope.organizationId}});

    countryresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.country !== null && $scope.countryList.indexOf(value.country) == -1) {
                //categoryId.push(value.id);
                $scope.countryList.push(value.country);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    $scope.desigList = [];
    var desigresp = $http.get(Data.serviceBase() + '/visitor/desigDetails', {params: {"organizationId": $scope.organizationId}});

    desigresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.designation !== null && $scope.desigList.indexOf(value.designation) == -1) {
                //categoryId.push(value.id);
                $scope.desigList.push(value.designation);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    $scope.companyList = [];
    var companyresp = $http.get(Data.serviceBase() + '/visitor/companyDetails', {params: {"organizationId": $scope.organizationId}});

    companyresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.visitorOrganizationName !== null && $scope.companyList.indexOf(value.visitorOrganizationName) == -1) {
                //categoryId.push(value.id);
                $scope.companyList.push(value.visitorOrganizationName);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    $scope.tagsList = [];
    var tagsresp = $http.get(Data.serviceBase() + '/visitor/tagsDetails', {params: {"organizationId": $scope.organizationId}});

    tagsresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.tag !== null && $scope.tagsList.indexOf(value.tag) == -1) {
                //categoryId.push(value.id);
                $scope.tagsList.push(value.tag);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });


    /// -- Custome Fileds filters ---------------------------------------------
    $scope.addCust = {"field": "", "condition": 'Contains', "value1": '', "value2": ''};
    $scope.custFiltersAdd = function (custField) {
        if (custField.field !== "" && custField.value1 !== "") {
            $scope.cfFilters.push(custField);
            $scope.addCust = {"field": "", "condition": 'Contains', "value1": '', "value2": ''};
        }
        if (custField.field !== "" && (custField.condition === "Is Empty" || custField.condition === "Is Not Empty")) {
            $scope.cfFilters.push(custField);
            $scope.addCust = {"field": "", "condition": 'Contains', "value1": ''};
        }
        if (custField.field !== "" && custField.field === "Between" && custField.value1 !== "" && custField.value2 !== "") {
            $scope.cfFilters.push(custField);
            $scope.addCust = {"field": "", "condition": 'Contains', "value1": '', "value2": ''};
        }

        $scope.custfieldsList.splice($scope.custfieldsList.indexOf(custField.field), 1);


    };
    $scope.custFiltersDelete = function (custField) {
        // if(custField.fieldName === $scope.cfFilters.filedName){
        $scope.cfFilters.splice($scope.cfFilters.indexOf(custField.field), 1);
        if ($scope.custfieldsList.indexOf(custField.field) === -1) {
            $scope.custfieldsList.push(custField.field);
        }
        // }
        //$scope.cfFilters.push(custField);
    };

    var eventListId = [];



    $scope.getCustomFields = function () {
        $scope.custfieldsList = [];
        var cFieldsresp = $http.get(Data.serviceBase() + '/visitor/cutomFields', {params: {"organizationId": $scope.organizationId, "eventId": eventListId.join()}});

        cFieldsresp.then(function (response) {

            // $scope.cityList = response.data;
            // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
            angular.forEach(response.data, function (value, key) {

                if (value.name !== null && $scope.custfieldsList.indexOf(value.name) == -1) {
                    //categoryId.push(value.id);
                    $scope.custfieldsList.push(value.name);
                }


            });
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    };

    //// ------------------------------- end of custom filed filters ---------------


    ///------------------------------------------------------------------- Auto suggestion ------------------------------------------

    this.appendToBodyDemo = {
        remainingToggleTime: 0,
        present: true,
        startToggleTimer: function () {
            var scope = this.appendToBodyDemo;
            var promise = $interval(function () {
                if (scope.remainingTime < 1000) {
                    $interval.cancel(promise);
                    scope.present = !scope.present;
                    scope.remainingTime = 0;
                } else {
                    scope.remainingTime -= 1000;
                }
            }, 1000);
            scope.remainingTime = 3000;
        }
    };
    $scope.categoryCheck = function (index) {
        if ($scope.categorySelected[index] !== null) {
            $scope.categoryCount = $scope.categoryCount + 1;
        } else {
            $scope.categoryCount = $scope.categoryCount - 1;
        }
    };
    $scope.interestCheck = function (index) {
        if ($scope.serviceSelected[index] !== null) {
            $scope.interestCount = $scope.interestCount + 1;
        } else {
            $scope.interestCount = $scope.interestCount - 1;
        }
    };
    $scope.potentialCheck = function (index) {
        if ($scope.potentialSelected[index] !== null) {
            $scope.potentialCount = $scope.potentialCount + 1;
        } else {
            $scope.potentialCount = $scope.potentialCount - 1;
        }
    };
    $scope.LeadstageCheck = function (index) {
        if ($scope.LeadstageSelected[index] !== null) {
            $scope.LeadstageCount = $scope.LeadstageCount + 1;
        } else {
            $scope.LeadstageCount = $scope.LeadstageCount - 1;
        }
    };
    $scope.eventCheck = function (index) {
        if ($scope.eventSelected[index] !== null) {
            $scope.eventCount = $scope.eventCount + 1;
        } else {
            $scope.eventCount = $scope.eventCount - 1;
        }
        //Getting Event Details if exist
        if ($scope.eventSelected.length > 0) {
            angular.forEach($scope.eventSelected, function (value, key) {
                if (value !== null) {


                    if (value.name !== undefined && eventListId.indexOf(value.id) === -1) {
                        eventListId.push(value.id);

                    }

                }

            });
            $scope.getCustomFields();
        } else {
            eventListId = [];
            $scope.getCustomFields();
        }
    };
    $scope.assignToCheck = function (index) {
        if ($scope.userassignToSelected[index] !== null) {
            $scope.assignToCount = $scope.assignToCount + 1;
        } else {
            $scope.assignToCount = $scope.assignToCount - 1;
        }
    };
    $scope.assigneCheck = function (index) {
        if ($scope.userassigneSelected[index] !== null) {
            $scope.assigneCount = $scope.assigneCount + 1;
        } else {
            $scope.assigneCount = $scope.assigneCount - 1;
        }
    };
    $scope.captureCheck = function (index) {
        if ($scope.usercaptureSelected[index] !== null) {
            $scope.captureCount = $scope.captureCount + 1;
        } else {
            $scope.captureCount = $scope.captureCount - 1;
        }
    };

    $scope.userassintoname = "";
    $scope.userassignename = "";
    $scope.usercapturename = "";
    $scope.eventselname = "";
    //$scope.searchf.dealcomp = '>';
    $scope.selectedFilter = {
        "cgroup": "", "service": "", "potential": "", "event": "", "eventId": "", "assignedTo": "", "assignedBy": "", "capturedBy": "", "designation": "", "company": "", "city": "", "state": "", "country": "",
        "fromDate": "", "toDate": "", "drfromDate": "", "drtoDate": "", "dontFollow": "", "stage": "", "assignToId": "", "assigneeId": "", "capturedId": "", "notes": "", "tags": "", "notAssigned": "", "deal": "", "dealComp": ""
        , "custList": "" , "emailStatus":"", "stageIds": ""
    };
    $scope.searchFilter = function () {
        var categoryId = [];
        var categoryName = [];
        angular.forEach($scope.categorySelected, function (value, key) {

            if (value !== null) {
                //categoryId.push(value.id);
                categoryName.push(value.name);
            }


        });
        $scope.cgroups = categoryId.join();
        $scope.selectedFilter.cgroup = categoryName.join();
        var servicesId = [];
        var servicesName = [];
        angular.forEach($scope.serviceSelected, function (value, key) {

            if (value !== null) {
                servicesId.push(value.id);
                servicesName.push(value.name);
            }

        });
        $scope.interests = servicesId.join();
        $scope.selectedFilter.service = servicesName.join();
        $scope.potentialsele = [];
        angular.forEach($scope.potentialSelected, function (value, key) {
            if (value) {
                $scope.potentialsele.push(value);
            }

        });
       $scope.Leadstagesele = [];
        $scope.LeadstageseleIds = [];
        //console.log($scope.LeadstageSelected);
        angular.forEach($scope.Lstages, function (value) {
            console.log(value.id);

            angular.forEach($scope.LeadstageSelected, function (val) {
                console.log(val);

                if (value.id == val) {
                    // console.log(value.id);
                    $scope.Leadstagesele.push(value.stage);
                    $scope.LeadstageseleIds.push(value.id);
                    //console.log(value.stage);
                }
            });
        });
        //$scope.potential = $scope.potentialSelected.join();
        //console.log($scope.potential);
        $scope.selectedFilter.potential = $scope.potentialsele.filter(function (val) {
            return val !== null;
        }).join();
        $scope.selectedFilter.stage = $scope.Leadstagesele.filter(function (val) {
            return val !== null;
        }).join();
        $scope.selectedFilter.stageIds = $scope.LeadstageseleIds.filter(function (val) {
            return val !== null;
        }).join();
        if ($scope.currenteventId === 0) {
            //console.log($scope.eventSelected);
            var eventId = [];
            var eventName = [];
            angular.forEach($scope.eventSelected, function (value, key) {
                if (value !== null) {


                    if (value.name !== undefined) {
                        eventId.push(value.id);
                        eventName.push(value.name);
                    }

                }

            });
            $scope.eventslist = eventId.join();
            $scope.selectedFilter.event = eventName.filter(function (val) {
                return val !== null;
            }).join();
            $scope.selectedFilter.eventId = eventId.filter(function (val) {
                return val !== null;
            }).join();
        } else {
            $scope.eventslist = $scope.currenteventId;
            $scope.selectedFilter.eventId = $scope.currenteventId;
            $scope.selectedFilter.event = localStorage.getItem('curEventName');
            //$scope.eventselname=sessionStrogae.getItem('curEventName');
        }
        var assignTotempId = [];
        var assignTotempName = [];
        angular.forEach($scope.userassignToSelected, function (value, key) {

            if (value !== null) {

                assignTotempId.push(value.id);
                assignTotempName.push(value.firstName);
            }



        });
        $scope.usersassignTo = assignTotempId.join();
        $scope.selectedFilter.assignedTo = assignTotempName.filter(function (val) {
            return val !== null;
        }).join();
        $scope.selectedFilter.assignedToId = assignTotempName.filter(function (val) {
            return val !== null;
        }).join();
        var userassigneId = [];
        var userassigneName = [];
        angular.forEach($scope.userassigneSelected, function (value, key) {

            if (value !== null) {
                userassigneId.push(value.id);
                userassigneName.push(value.firstName);
            }

        });
        $scope.usersassigne = userassigneId.join();
        $scope.selectedFilter.assignedBy = userassigneName.filter(function (val) {
            return val !== null;
        }).join();
        var usercaptureId = [];
        var usercaptureName = [];
        angular.forEach($scope.usercaptureSelected, function (value, key) {

            if (value !== null) {
                usercaptureId.push(value.id);
                usercaptureName.push(value.firstName);
            }

        });
        $scope.userscapture = usercaptureId.join();
        $scope.selectedFilter.capturedBy = usercaptureName.filter(function (val) {
            return val !== null;
        }).join();
        //console.log($scope.cgroups);
        //console.log($scope.interests);
        //console.log($scope.potential);
        //console.log($scope.eventslist);
        //console.log($scope.usersassignTo);
        //console.log($scope.usersassigne);
        //console.log($scope.userscapture);
        //console.log($scope.searchf);
        $scope.selectedFilter.capturedId = $scope.userscapture;
        $scope.selectedFilter.assigneeId = userassigneId.filter(function (val) {
            return val !== null;
        }).join();
        $scope.selectedFilter.assignToId = assignTotempId.filter(function (val) {
            return val !== null;
        }).join();
        $scope.selectedFilter.potential = $scope.potentialsele.filter(function (val) {
            return val !== null;
        }).join();
         $scope.selectedFilter.stage = $scope.Leadstagesele.filter(function (val) {
            return val !== null;
        }).join();
        $scope.selectedFilter.stageIds = $scope.LeadstageseleIds.filter(function (val) {
            return val !== null;
        }).join();
        $scope.selectedFilter.designation = $scope.searchf['designation'] ? $scope.searchf['designation'].join() : '';
        $scope.selectedFilter.company = $scope.searchf['company'] ? $scope.searchf['company'].join() : '';
        $scope.selectedFilter.notes = $scope.searchf['notes'];
        $scope.selectedFilter.tags = $scope.searchf['tags'] ? $scope.searchf['tags'].join() : '';
        $scope.selectedFilter.country = $scope.searchf['country'] ? $scope.searchf['country'].join() : '';
        $scope.selectedFilter.city = $scope.searchf['city'] ? $scope.searchf['city'].join() : '';
        $scope.selectedFilter.state = $scope.searchf['state'] ? $scope.searchf['state'].join() : '';
        $scope.selectedFilter.fromDate = $scope.searchf['fromDate'];
        $scope.selectedFilter.toDate = $scope.searchf['toDate'];
        $scope.selectedFilter.drfromDate = $scope.searchf['drfromDate'];
        $scope.selectedFilter.drtoDate = $scope.searchf['drtoDate'];
        $scope.selectedFilter.dontFollow = $scope.searchf['dontFollow'];
        $scope.selectedFilter.notAssigned = $scope.searchf['notAssigned'];
        $scope.selectedFilter.emailStatus = $scope.searchf['emailStatus'];
        if ($scope.searchf['dealcomp'] === '>' && $scope.searchf['dealValue']) {
            $scope.selectedFilter.deal = $scope.searchf['dealValue'];
            $scope.selectedFilter.dealComp = $scope.searchf['dealcomp'];
        } else if ($scope.searchf['dealcomp'] !== '>') {
            $scope.selectedFilter.deal = $scope.searchf['dealValue'];
            $scope.selectedFilter.dealComp = $scope.searchf['dealcomp'];
        }
        $scope.selectedFilter.custList = $scope.cfFilters;
        console.log($scope.selectedFilter);
        $scope.result = {"visitors": "", "searchFilter": $scope.selectedFilter};
        $uibModalInstance.close($scope.result);
    };
    $scope.cancelFilter = function () {
        $scope.categorySelected = {};
        $scope.serviceSelected = {};
        $scope.potentialSelected = {};
        $scope.LeadstageSelected = {};
        $scope.eventSelected = {};
        $scope.userassignToSelected = {};
        $scope.userassigneSelected = {};
        $scope.usercaptureSelected = {};
        $scope.search = {};
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('smsBulkCtrl', function ($scope, $rootScope, checkPremium, trialPopUp, warningPopup, alertUser, $uibModal, Data, toaster, $http) {

    $scope.sms = {};
    $scope.eventSelected = {};
    $scope.showbox = false;
    $scope.plan = "";
    $scope.visitorName = "";
    $scope.listName = "";
    $rootScope.addButtonhtml = '&nbsp;';
    sessionStorage.setItem('menuact', '10');
    $rootScope.menuact = '10';
    //$rootScope.addButtonhtml += '<button ng-show="eventBut" tooltip-placement="bottom" tooltip="Clear demo event before create an event" class="btn btn-danger btn-sm" ng-click="delevent(eventsDet);"><span class="fa fa-trash"></span> Clear Demo Event</button>&nbsp;';
    $rootScope.addButtonhtml += '<button class="btn btn-default btn-sm btn-circle" popover="Send Personalized smart Follow up SMS using Hello SMS" popover-title="Hello SMS" popover-trigger="focus" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    $rootScope.title = "Hello SMS";
    $scope.visitorCName = "";
    $scope.toId = "";
    $scope.smsTemps = [];
    $scope.eventLists = {};
    $scope.leadList = [];
    $scope.emailTotal = 0;
    $scope.btntxt = "Send";
    $scope.btnptxt = "Send";
    $scope.smsFilter = {};
    $scope.smsDLT = {};
    $scope.visitorList = {};
    $scope.loader = false;
    $scope.mailCount = 0;
    $scope.btnName = 'Save';
    $scope.sms.id = '';
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.userEmail = sessionStorage.getItem('userEmail');
    $scope.userName = sessionStorage.getItem('firstName') + " " + sessionStorage.getItem('lastName');
    $scope.smsFilter.body = "";
    $scope.messages = '';
    this.setInputFocus = function () {
        $scope.$broadcast('UiSelectDemo1');
    };
    $scope.qtab = 1;
    angular.element(document.getElementById("emailCamp")).addClass("active");
    angular.element(document.getElementById("smsTemplate")).removeClass("active");
    angular.element(document.getElementById("smsTemplate2")).removeClass("active");
    angular.element(document.getElementById("smsSettings")).removeClass("active");
    //angular.element(document.getElementById("metric")).removeClass("active");

    // $scope.emailFilter.replyTo = $scope.emailFilter.emailId;
    $scope.clearFilter = function () {
        $scope.emailFilter.length = 0;

    };
    $scope.showBox = function () {
        if ($scope.showbox) {
            $scope.showbox = false;
        } else {
            $scope.showbox = true;
        }

    };

    $scope.getAllOrgSmsTemplate = function () {
        var resprom = $http.get(Data.serviceBase() + 'organization/smsTemplate', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.smsTemplates = response.data;
            $scope.smsTemplateDet = "";
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.getAllServiceProviders = function () {
        var resprom = $http.get(Data.serviceBase() + 'organization/smsProviders', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.smsProviders = response.data;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    
    $scope.smsProviderStatus = {};
    $scope.getSmsProviderStatus = function () {
        var resprom = $http.get(Data.serviceBase() + 'organization/smsProviderStatus', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.smsProviderStatus = response.data;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.changecontentSMS = function () {
        $scope.smsTemplate = JSON.parse($scope.smsTemplateDet);
        //console.log("SMS Temp Response "+$scope.smsTemplate.id);
        var resprom = $http.get(Data.serviceBase() + 'organization/getSmsTemp', {params: {"id": $scope.smsTemplate.id,"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.smsFilter.body = response.data[0].smsContent;
            $scope.smsFilter.templateId = response.data[0].templateId;
            $scope.smsFilter.senderId = response.data[0].senderId;
            $scope.smsFilter.authKey = response.data[0].apiKey;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.addTagg = function (tag) {
        $rootScope.$broadcast('addTaggs', tag);
        //$scope.smsFilter.body = $scope.smsFilter.body + "#LeadName#";
    };

    $scope.addTaggg = function (tag) {
        $rootScope.$broadcast('addTagggs', tag);
        //$scope.smsFilter.body = $scope.smsFilter.body + "#LeadName#";
    };

    $scope.orgPayemnt = function () {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "bulkSMS", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    console.log(response.data);
                    if (response.data.status != 'success') {
                        $scope.orgPlanCheck(response.data);

                    } else {

                        $scope.getOrgDetSMS();
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.smsOrgDet = {};
    $scope.smsConfig = {};
    $scope.getOrgDetSMS = function () {
        var orgResp = $http.get(Data.serviceBase() + '/organization/organization', {params: {"key": "id", "value": $scope.organizationId}});
        orgResp.then(function (response) {
            console.log(response.data[0]);

            $scope.smsOrgDet = response.data[0];
            // if ($scope.smsOrgDet.smsInteg === '1' || $scope.smsOrgDet.id === '26053' || $scope.smsOrgDet.id === '35978') {
                // $scope.message = {"title": "Warning", "message": 'Hello SMS is not configured for your account. Please contact <a href="https://www.helloleads.io/support.php" target="_blank">HelloLeads support</a> team to configure Hello SMS. Hello SMS helps to send Individual/ Bulk SMS to your leads.', "cancel": false};
                // alertUser.alertpopmodel($scope.message);
                
                // $scope.qtab = 3;
                // angular.element(document.getElementById("emailCamp")).removeClass("active");
                // angular.element(document.getElementById("smsTemplate")).removeClass("active");
                // angular.element(document.getElementById("smsTemplate2")).removeClass("active");
                // angular.element(document.getElementById("smsSettings")).addClass("active");

            // } else {
                var orgsmsResp = $http.get(Data.serviceBase() + '/organization/getSmsConfig', {params: {"organizationId": $scope.organizationId}});
                orgsmsResp.then(function (smsres) {
                    $scope.smsConfig = smsres.data[0];
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            // }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.addVisitorName = function (el) {
        console.log(el);
        //$rootScope.$broadcast('add', "VisitorName"); 
        $scope.messages = $scope.messages + "VisitorName"
    };

    $scope.addVisitor = function (el) {
        console.log(el);
        //$rootScope.$broadcast('add', "VisitorName"); 
        $scope.smsFilter.body = $scope.smsFilter.body + "VisitorName";
    };
    $scope.smscheckField = function () {
        if ($scope.smsOrgDet.smsInteg === '1' || $scope.smsOrgDet.id === '26053' || $scope.smsOrgDet.id === '35978') {
            $scope.smsFilter.body = "";
            return true;
        } else {
            return false;
        }
    };
    $scope.addTag = function (tag) {
        //$scope.items.push($scope.someInput);
        //console.log(tag);
        $rootScope.$broadcast('addcTags', tag);
        //toaster.pop("info", "", "<strong>" + tag + "</strong>" + " placeholder copied to clipboard. Paste the placeholder text wherever required in the email content", 7000, 'trustedHtml');

    };

    $scope.orgPlanCheck = function (p, size) {
        var modalInstance1 = $uibModal.open({
            templateUrl: './checkPremiumPopup',
            controller: 'premiumCheckCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                msg: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };

    $scope.toggle = {};
    $scope.globalSmsConfig = function (sms) {
        $scope.provider = sms;
        
        // var msg = {"message": "Configuring <b>" + sms.name + "</b> will disconnect the existing configuration with <b>"+ $scope.provider +"</b>, do you wish to proceed ?<br>", "title": "Confirmation", "canceltxt": "Cancel", "oktxt": "Proceed"};
        // warningPopup.warningpopmodel(msg);
        // $scope.$on('warnresp', function (event, args) {
        //     if (args.status === 'true') {
                // var resprom = $http.get(Data.serviceBase() + 'organization/deleteDLTTemp', {params: {"id": sms.id}});
                // resprom.then(function (response) {
                //     toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                //     $scope.sms = {};
                //     $scope.getAllOrgSmsTemplate();
                //     dflag = '1';
                // }, function (response) {
                //     console.log('Error happened -- ');
                //     console.log(response);
                // });
                var getSMSConfig = $http.get(Data.serviceBase() + '/event/getOrgSmsConfig', {params: {"provider": $scope.provider,"organizationId": $scope.organizationId}});
                getSMSConfig.then(function (response) {
                    $scope.msgOrgConfig = response.data;
                    if (response.data.status == '0') {
                        $switch = false;
                    } else {
                        $switch = true;
                    }
                    $scope.toggle.switch = $switch;
                var modalInstance1 = $uibModal.open({
                    templateUrl: './globalSmsConfig',
                    controller: 'configSMSCtrl',
                    size: 'md',
                    backdrop: 'static',
                    // keyboard: false,
                    resolve: {
                        smsConfigs: function () {
                            return $scope.msgOrgConfig;
                        },
                        smsProviders: function () {
                            return $scope.provider;
                        },
                        toggle: function () {
                            return $switch;
                        }
                    }
                });
                modalInstance1.result.then(function (i) {
                        $scope.getAllOrgSmsTemplate();
                        $scope.getAllServiceProviders();
                        $scope.getSmsProviderStatus();
                });
                 }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
                //     }
                // });
    };

    // $scope.twilio = function () {
    //     var getSMSConfig = $http.get(Data.serviceBase() + '/event/getOrgSmsConfig', {params: {"provider": "twilio","organizationId": $scope.organizationId}});
    //     getSMSConfig.then(function (response) {
    //         $scope.smsConfig = response.data[0];
    //     }, function (response) {
    //         console.log('Error happened -- ');
    //         console.log(response);
    //     });
    //     var modalInstance1 = $uibModal.open({
    //         templateUrl: './twilioConfig',
    //         controller: 'configSMSCtrl',
    //         size: 'md',
    //         backdrop: 'static',
    //         keyboard: false,
    //         resolve: {
    //             smsConfigs: function () {
    //                 return $scope.smsConfig;
    //             }
    //         }
    //     });
    //     // modalInstance1.result.then(function (i) {

    //     // });
    // };

    // $scope.msg91 = function () {
    //     var getSMSConfigs = $http.get(Data.serviceBase() + '/event/getOrgSmsConfig', {params: {"provider": "MSG91","organizationId": $scope.organizationId}});
    //     getSMSConfigs.then(function (response) {
    //         $scope.smsConfig = response.data[0];
    //     }, function (response) {
    //         console.log('Error happened -- ');
    //         console.log(response);
    //     });

    //     var modalInstance1 = $uibModal.open({
    //         templateUrl: './msg91Config',
    //         controller: 'configSMSCtrl',
    //         size: 'md',
    //         backdrop: 'static',
    //         keyboard: false,
    //         resolve: {
    //              smsConfigs: function () {
    //                 return $scope.smsConfig;
    //             }
    //         }
    //     });
    //     modalInstance1.result.then(function (i) {
    //         if (i === 1) {

    //         }
    //     });
    //     // modalInstance1.result.then(function (i) {

    //     // });
    // };

    // $scope.msgActive = function () {
    //     $scope.smsActive = '0';
    // };    

    $scope.mbccode = '';
    $scope.orgMobPhoneCode = sessionStorage.getItem('countryCode');
    $scope.getSMSCountryCodeData = function () {
        $http.get('../../../css/phone.json').success(function (data) {
            $scope.countrysmsmobDet = data;
            $scope.mbccode = $scope.orgMobPhoneCode;
        });
    };
    $scope.setMobCode = function(code){
        $scope.mbccode = code;
    };

    $scope.switchcampTab = function (tabId) {
        var resproles = $http.get(Data.serviceBase() + '/organization/organization', {params: {"key" : "id", "value" :$scope. organizationId}});
                resproles.then(function (result) {
                if (result.data !== null && result.data !== '') {
                    // if (result.data[0].smsInteg === '0') {
                        if (tabId === 1) {
                            $scope.qtab = 1;
                            angular.element(document.getElementById("emailCamp")).addClass("active");
                            angular.element(document.getElementById("smsTemplate")).removeClass("active");
                            angular.element(document.getElementById("smsTemplate2")).removeClass("active");
                            angular.element(document.getElementById("smsSettings")).removeClass("active");
                        } else if (tabId === 2) {
                            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "smsTemplates"}});
                            $scope.users = {};
                            resprole.then(function (response) {
                                if (response.data.status === 'success') {
                                    $scope.qtab = 2;
                                    angular.element(document.getElementById("emailCamp")).removeClass("active");
                                    angular.element(document.getElementById("smsTemplate")).addClass("active");
                                    angular.element(document.getElementById("smsTemplate2")).addClass("active");
                                    angular.element(document.getElementById("smsSettings")).removeClass("active");
                                } else {
                                    $scope.switchcampTab(1);
                                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to use SMS Templates / Settings options. Only account owner or managers can use / edit SMS Templates. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                                    alertUser.alertpopmodel($scope.message);
                                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                                }
                            }, function (response) {
                                console.log('Error happened -- ');
                                console.log(response);
                            });
                        } else if(tabId === 3) {
                            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "smsTemplates"}});
                            $scope.users = {};
                            resprole.then(function (response) {
                                if (response.data.status === 'success') {
                                    $scope.qtab = 3;
                                    angular.element(document.getElementById("emailCamp")).removeClass("active");
                                    angular.element(document.getElementById("smsTemplate")).removeClass("active");
                                    angular.element(document.getElementById("smsTemplate2")).removeClass("active");
                                    angular.element(document.getElementById("smsSettings")).addClass("active");
                                } else {
                                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) oes not allow you to use SMS Templates / Settings options. Only account owner or managers can use / edit SMS Templates. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                                    alertUser.alertpopmodel($scope.message);
                                    $scope.switchcampTab(1);
                                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                                }
                            }, function (response) {
                                console.log('Error happened -- ');
                                console.log(response);
                            });
                        }
                    // } else {
                    //     $scope.qtab = 3;
                    //         angular.element(document.getElementById("emailCamp")).removeClass("active");
                    //         angular.element(document.getElementById("smsTemplate")).removeClass("active");
                    //         angular.element(document.getElementById("smsTemplate2")).removeClass("active");
                    //         angular.element(document.getElementById("smsSettings")).addClass("active");
                    // }
                }
            }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
    };

    $scope.getAllOrgEvents = function () {
        var eventresp = $http.get(Data.serviceBase() + '/event/event', {params: {"key": "organizationId", "value": $scope.organizationId}});
        eventresp.then(function (response) {
            //console.log(response.data);
            $scope.eventLists = response.data;
            //console.log("Current event1" + $rootScope.currentEventId);
            angular.forEach($scope.eventLists, function (d) {
                if (d.id === localStorage.getItem('curEventId')) {
                    //localStorage.setItem("filters","");
                    $scope.filterEventSelected = d;
                    $scope.adfilter = true;
                    $scope.filterby = true;
                    $scope.filtersadded.event = $rootScope.currentEventName;
                } else if ($scope.filtersadded !== undefined) {
                    if (d.id === $scope.filtersadded.eventId) {
                        $scope.filterEventSelected = d;
                        $scope.adfilter = true;
                        $scope.filterby = true;
                        $scope.filtersadded.event = d.name;
                    }
                }

            });

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.resetEvent = function () {
        $scope.eventSelected = {};
        $scope.emailFilter.maillist = {};
        $scope.emailTemps.length = 0;
        $scope.emailTotal = 0;
    };
    /*$scope.getAllOrgTemplate = function () {
     $scope.checkAccountSMSCount();
     var resprom = $http.get(Data.serviceBase() + 'organization/smsTemplate', {params: {"organizationId": $scope.organizationId}});
     resprom.then(function (response) {
     $scope.smsTemplates = response.data;
     $scope.smsTemplateDet = "";
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
     };*/
    /*$scope.getEmailMetrics = function () {
     var resprom = $http.get(Data.serviceBase() + 'organization/emailMetrics', {params: {"organizationId": $scope.organizationId}});
     resprom.then(function (response) {
     console.log(response.data);
     $scope.emailMetrics = response.data;
     
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
     }*/
    $scope.changecontentEmail = function () {
        //console.log($scope.emailTemplateDet);
        $scope.userDet = {};
        var resprom = $http.get(Data.serviceBase() + 'user/user', {params: {"key": "id", "value": $scope.userId}});
        resprom.then(function (response) {
            console.log(response.data);
            $scope.userDet = response.data[0];
            //$scope.emailTemplateDet = "";
            //console.log($scope.emailTemplateDet);
            var etemp = JSON.parse($scope.emailTemplateDet);
            $scope.emailFilter.subject = etemp.subject;
            console.log(etemp.body);
            var message = etemp.body;
            var regf = /#firstname#/gi;
            var regl = /#lastname#/gi;
            var regdes = /#designation#/gi;
            var regem = /#email#/gi;
            var regmob = /#mobile#/gi;
            message = message.replace(regf, $scope.userDet['firstName']);
            message = message.replace(regl, $scope.userDet['lastName']);
            message = message.replace(regdes, $scope.userDet['designation']);
            message = message.replace(regem, $scope.userDet['email']);
            message = message.replace(regmob, $scope.userDet['phoneCode'] + $scope.userDet['phone']);
            console.log(message);
            $scope.emailFilter.body = message;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.redirectEtToTemp = function () {
        window.open(Data.webAppBase() + "account/smsTemplate", '_blank');
    };
    $scope.mailCount = 0;
    $scope.smsfiltersadded = {};
    $scope.visitorList = {};
    $scope.filterAddedd = {};
    $scope.filtersmsvisitor = function (size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './filterVisitor',
            controller: 'filterEmailVisitorCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                eventId: function () {
                    return 0;
                },
                eventName: function () {
                    return "";
                }
            }
        });
        modalInstance1.result.then(function (results) {
            // //console.log(results.searchFilter);
            // //console.log(results.visitors);
            /* if (results.visitors) {
             $scope.loader = true;
             $scope.adfilter = true;
             $scope.searchvisitors(results.visitors);
             }*/
            $scope.filterAddedd = results.searchFilter;
            if (results.searchFilter) {
                $scope.filterby = true;
                ////console.log("Search Filter");
                // //console.log(results.searchFilter);
                // $scope.clearFilter();
                $scope.smsfiltersadded = {};
                $scope.smsfiltersadded = results.searchFilter;
                //console.log($scope.emailfiltersadded.emailStatus);
                //if ($scope.emailfiltersadded.emailStatus === undefined) {
                //   $scope.emailfiltersadded.emailStatus = 'active';
                //}
                var respc = $http.get(Data.serviceBase() + '/visitor/filterMobileVisitor', {params: {"organizationId": $scope.organizationId, "cgroups": $scope.smsfiltersadded.cgroup, "interests": $scope.smsfiltersadded.service, "potential": $scope.smsfiltersadded.potential, "eventId": $scope.smsfiltersadded.eventId, "assignto": $scope.smsfiltersadded.assignToId, "assigne": $scope.smsfiltersadded.assigneeId, "capture": $scope.smsfiltersadded.capturedId, "type": $scope.smsfiltersadded.stageIds, "designation": $scope.smsfiltersadded.designation, "company": $scope.smsfiltersadded.company, "country": $scope.smsfiltersadded.country, "city": $scope.smsfiltersadded.city, "state": $scope.smsfiltersadded.state, "fromDate": $scope.smsfiltersadded.fromDate, "toDate": $scope.smsfiltersadded.toDate, "drfromDate": $scope.smsfiltersadded.drfromDate, "drtoDate": $scope.smsfiltersadded.drtoDate, "dontFollow": $scope.smsfiltersadded.dontFollow, "notes": $scope.smsfiltersadded.notes, "tags": $scope.smsfiltersadded.tags, "notAssigned": $scope.smsfiltersadded.notAssigned, "searchquery": "", "deal": $scope.smsfiltersadded.deal, "dealComp": $scope.smsfiltersadded.dealComp, "customList": JSON.stringify($scope.smsfiltersadded.custList), "emailStatus": $scope.smsfiltersadded.emailStatus}});
                respc.then(function (response) {
                    //console.log(response.data);
                    $scope.visitorList = response.data.visitors;


                }, function (response) {
                    //console.log('Error happened -- ');
                    //console.log(response);
                });
            } else {
                $scope.smsfiltersadded = {};
                $scope.visitorList = {};
            }

        });
    };

    $scope.tinymceOptionss = {
        selector: 'textarea',
        statusbar: false,
        entity_encoding: "UTF-8",
        elementpath: false,
        height: 250,
        theme: 'modern',
        menubar: false,      
        autosave_ask_before_unload: false,
        paste_data_images: false,
        forced_root_block_attrs: {
            "class": "myclass",
            "data-something": "my data",
            "style": "margin: 5px 0;"
        },
        toolbar3: 'mybutton,mybutton1,signbutton',
       content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            "https://beta.helloleads.io/css/tinymcecontent.css"],
        content_style: ".tinymce-content p {  padding: 0; margin: 1px 0; }",
        setup: function (editor) {
            editor.addButton('mybutton', {
                text: '#VisitorName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorName#');
                }
            });
            editor.addButton('mybutton1', {
                text: '#VisitorCompanyName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorCompanyName#');
                }
            });
           editor.addButton('signbutton', {
                text: 'Signature',
                title: 'Signature',
                type: 'menubutton',
                icon: false,
                menu: [
                    {
                        text: '#firstname',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#firstname#');
                        }
                    },
                    {
                        text: '#lastname',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#lastname#');
                        }

                    },
                    {
                        text: '#designation',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#designation#');
                        }
                    },
                    {
                        text: '#company',
                        icon: false,
                        onclick: function () {
                            var orgname = sessionStorage.getItem('cOrgName');
                            editor.insertContent(orgname);
                        }
                    },
                    {
                        text: '#email',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#email#');
                        }
                    },
                    {
                        text: '#mobile',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#mobile#');
                        }
                    },
                ]
            });
        }
    };

    /*$scope.viewFilterVisitors = function () {
        localStorage.setItem("filters", JSON.stringify($scope.filterAddedd));
        window.open(Data.webAppBase() + 'account/manageVisitor', "_blank");
    };*/

    $scope.checkAccountSMSCount = function () {
        var mailcountresp = $http.get(Data.serviceBase() + '/visitor/checkMobileCount', {params: {"orgId": $scope.organizationId}});
        mailcountresp.then(function (response) {
            console.log(response);
            $scope.mailCount = response.data.count;
            //$scope.mailCount = '950';
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    };
    /*$scope.SendMailcancel = function () {
     $scope.emailFilter['fromName'] ="";
     $scope.emailFilter['subject'] ="";
     $scope.emailFilter['body'] ="";
     Data.post('admin/bulkEmail', $scope.emailFilter).then(function (result) {
     if (result.status !== 'error' && result.status === 'success1') {
     
     $scope.message = {"title":"Authorization failure","message": "Campaign Cancelled"};
     alertUser.alertpopmodel($scope.message);
     $scope.btntxt = 'Send';
     
     } else {
     console.log(result);
     }
     });
     };*/

     $scope.getSmsTemplate = function(flowId,id) {
        var getDLT = $http.get(Data.serviceBase() + '/event/checkDLT', {params: {"flowId": flowId,"organizationId": $scope.organizationId}});
        getDLT.then(function (response) {
            // console.log("DLT "+response.data);
                $scope.template = JSON.parse(response.data);
            if (id == null || id == '') {
                if ($scope.template.msg_type == "Success") {
                    $scope.btnName = 'Save';
                    $scope.getDLTTemp = $scope.template.data;
                    $scope.sms.senderId = $scope.getDLTTemp.Sender;
                    $scope.sms.dltId = $scope.getDLTTemp.DLT_TE_ID;
                    $scope.sms.message = $scope.getDLTTemp.Message;
                    $scope.sms.hlsmessage = $scope.getDLTTemp.Message;
                    $scope.sms.name = $scope.getDLTTemp.FlowName;
                    $scope.sms.flowName = $scope.getDLTTemp.FlowName;
                    $scope.sms.id = "";
                } else {
                    toaster.pop("error", "", "Template Flow Id is not found!", 10000, 'trustedHtml');
                }
            } else {
                    $scope.btnName = 'Update';
                    $scope.getDLTTemp = $scope.template.data;
                    $scope.sms.senderId = $scope.getDLTTemp.Sender;
                    $scope.sms.dltId = $scope.getDLTTemp.DLT_TE_ID;
                    $scope.sms.message = $scope.getDLTTemp.Message;
                    $scope.sms.flowName = $scope.getDLTTemp.FlowName;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
     };

    $scope.SendSMSConfirm = function (p, size) {
        var visitorCount = 0;
        if ($scope.visitorName) {
            $scope.listName = '';
            p.maillist = [{"name": $scope.visitorName, "to": $scope.mbccode+$scope.toId, "company": $scope.visitorCName, "eventName": $scope.listName}];
            $scope.smsTemps = [{"name": $scope.visitorName, "to": $scope.mbccode+$scope.toId, "company": $scope.visitorCName, "eventName": $scope.listName}]
            visitorCount = 1;
        } else {
            visitorCount = $scope.visitorList.length;
        }
        //if ((p.maillist !== undefined && p.maillist.length <= (500 - parseInt($scope.mailCount)))) {
        var modalInstance1 = $uibModal.open({
            templateUrl: './confirmMessage',
            controller: 'sendClientSMSCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                maillist: function () {
                    console.log(visitorCount);
                    p.leadsCount = visitorCount;
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i === 1) {
                if ($scope.visitorCName === '' && $scope.visitorName === '') {
                    $scope.btntxt = 'Sending';
                    $scope.smsFilter.organizationId = $scope.organizationId;
                    $scope.smsFilter.userId = $scope.userId;
                    $scope.smsFilter.visitorlist = $scope.visitorList.join();
                    if ($scope.visitorList.length > (10000 - parseInt($scope.mailCount))) {
                        console.log("Checking");
                        $scope.message = {"title": "Warning", "message": "You are running out of daily SMS credits. To send bulk SMS, kindly get back tomorrow."};
                        alertUser.alertpopmodel($scope.message);
                    } else {
                        Data.post('visitor/sepVisitorMobile', $scope.smsFilter).then(function (result) {
                            if (result.status !== 'error' && result.status !== 'success1') {
                                $scope.cEmail = sessionStorage.getItem('userEmail');
                                $scope.email = {"cEmail":$scope.cEmail};
                                $scope.visitorName = "";
                                $scope.toId = "";
                                $scope.visitorCName = "";
                                $scope.listName = "";
                                $scope.message = {"title": "Success", "message": $scope.visitorList.length + " SMS has been sent successfully"};
                                if ($scope.message.title == "Success") {
                                    Data.post('visitor/setSMSCampCount', $scope.email).then(function (result) {
                                    console.log('Email '+$scope.cEmail);
                                    });
                                }
                                alertUser.alertpopmodel($scope.message);
                                $scope.btntxt = 'Send';

                            } else {
                                console.log(result);
                            }
                        });
                    }
                } else {
                    $scope.btnptxt = 'Sending';
                    $scope.smsFilter.organizationId = $scope.organizationId;
                    $scope.smsFilter.userId = $scope.userId;
                    $scope.smsFilter.maillist = p.maillist;
                    $scope.smsFilter.to = $scope.mbccode+$scope.toId;
                    $scope.smsFilter.fromEmail = p.fromEmail;
                    console.log($scope.toId);
                    console.log($scope.smsFilter);
                    Data.post('visitor/testVisitorSms', $scope.smsFilter).then(function (result) {
                        if (result.status !== 'error' && result.status !== 'success1') {
                            $scope.visitorName = "";
                            $scope.toId = "";
                            $scope.visitorCName = "";
                            $scope.listName = "";
                            $scope.message = {"title": "Success", "message": "1 SMS has been sent successfully"};
                            alertUser.alertpopmodel($scope.message);
                            $scope.btnptxt = 'Send';

                        } else {
                            console.log(result);
                        }
                    });
                }
            }
        });
        /*} else {
         $scope.visitorName = "";
         $scope.toId = "";
         $scope.visitorCName = "";
         $scope.listName = "";
         $scope.message = {"title": "Warning", "message": "You are running out of daily Email credits. To send bulk email, kindly get back tomorrow."};
         alertUser.alertpopmodel($scope.message);
         }*/

    };


    $scope.resetForm = function () {
        $scope.btnName = 'Save';
        $scope.sms = {};
    };


    $scope.selectSMSTemp = function (sms) {
        $scope.sms.name = sms.name;
        $scope.sms.flowId = sms.flowId;
        $scope.sms.hlsmessage = sms.smsContent;
        $scope.sms.id = sms.id;
        $scope.btnName = 'Update';
    };

    $scope.deleteDLTTemp = function (sms) {
        var dflag = '0';
        var msg = {"message": "Are you sure you want to delete this template - <b>" + sms.name + "</b> ?<br>", "title": "Confirmation", "canceltxt": "Cancel", "oktxt": "Delete"};
        warningPopup.warningpopmodel(msg);
        $scope.$on('warnresp', function (event, args) {
            if (args.status === 'true' && dflag === '0') {
                var resprom = $http.get(Data.serviceBase() + 'organization/deleteDLTTemp', {params: {"id": sms.id}});
                resprom.then(function (response) {
                    toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    $scope.sms = {};
                    $scope.getAllOrgSmsTemplate();
                    dflag = '1';
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }
        });
    };

    $scope.saveDTLTemp = function (sms) {
        // $scope.smsDLT.name = name;
        // $scope.smsDLT.hlsmessage = hlsmessage;
        // $scope.smsDLT.flowId = flowId;
        // $scope.smsDLT.senderId = senderId;
        // $scope.smsDLT.dltId = dltId;
        $scope.sms.organizationId = sessionStorage.getItem("orgId");
        $scope.sms.userId = sessionStorage.getItem("userId");
        if (sms.id !== 'null' && sms.id !== '' && sms.id !== undefined) {
                Data.put('visitor/updateDTLTemp', sms).then(function (response) {
                if (response.status !== 'error') {
                    toaster.pop("success", "", sms.name + " sms template modified successfully!", 10000, 'trustedHtml');
                    $scope.sms = {};
                    $scope.getAllOrgSmsTemplate();
                    $scope.getSmsProviderStatus();
                    $scope.btnName = "Save";
                } else {
                    toaster.pop("error", "", sms.name + " sms template not modified!", 10000, 'trustedHtml');
                }
            });
        } else {
            Data.post('visitor/saveDTLTemp', sms).then(function (response) {
            if (response.status !== 'error') {
                    toaster.pop("success", "", sms.name + " sms template added successfully!", 10000, 'trustedHtml');
                    $scope.sms = {};
                    $scope.getAllOrgSmsTemplate();
                    $scope.getSmsProviderStatus();
                    $scope.btnName = "Save";
                } else {
                    toaster.pop("error", "", sms.name + " sms template not added!", 10000, 'trustedHtml');
                }
                });
        }
    };

});
evtApp.controller('premiumCheckEmailCtrl', function ($scope, Data, $uibModalInstance, maillist, toaster) {
    $scope.mailDet = maillist;
    $scope.redirectBack = function () {
        $uibModalInstance.close(1);
        history.back();

    };
    $scope.redirectPay = function () {
        $uibModalInstance.close(1);
        window.location.href = Data.webAppBase() + 'account/pricing';

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});

evtApp.controller('sendClientSMSCtrl', function ($scope, Data, $uibModalInstance, maillist, toaster) {
    $scope.mailDet = maillist;
    $scope.cmessage = 'Are you sure you want to send SMS to  <span class="text-danger">' + $scope.mailDet.leadsCount + '</span> lead(s) ?';
    $scope.sendConfirm = function () {

        $uibModalInstance.close(1);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});


evtApp.controller('configSMSCtrl', function ($scope, Data, $uibModalInstance, toaster, smsConfigs, smsProviders, $http, toggle) {
    $scope.toggle = {};
    $scope.smsConfig = smsConfigs;
    $scope.smsProvider = smsProviders;
    $scope.toggle.switch = toggle;

    if($scope.smsConfig !== undefined && $scope.smsConfig.id !== undefined && $scope.smsConfig.id !== null) {
        $scope.btntxt = "Update";
    } else {
        $scope.btntxt = "Configure";
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


    $scope.toggleActionSms = function(smsConfig) {
        if(smsConfig.status == '1') {
            $scope.toggle.switch = false;
            $scope.smsConfig.status = '0';
        } else {
            $scope.toggle.switch = true;
            $scope.smsConfig.status = '1';
        }
    };

    $scope.updateMsgConfig = function (smsConfig,smsProvider) {
        smsConfig.provider = smsProvider;
        smsConfig.organizationId = sessionStorage.getItem("orgId");
        smsConfig.userId = sessionStorage.getItem("userId");
         if (smsConfig.id !== undefined && smsConfig.id !== null) {
            Data.put('event/updateSmsConfig', smsConfig).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    console.log(result);
                }
            });
        } else {
            Data.post('event/addSmsConfig', smsConfig).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    console.log(result);
                }
            });
        }
    };

    $scope.sendTestSMS = function (smsConfig,smsProvider) {
        smsConfig.provider = smsProvider;
        smsConfig.organizationId = sessionStorage.getItem("orgId");
        smsConfig.userId = sessionStorage.getItem("userId");
         var resprom = $http.get(Data.serviceBase() + 'event/testingTwilio', {params: {"param2":smsConfig.apiKey,"param3":smsConfig.extraKey1,"param4":smsConfig.extraKey2,"param9":smsConfig.refMobile,"testNumber":smsConfig.testNumber}});
                resprom.then(function (response) {
                    $scope.smsStatus = response.data.status;
        if ($scope.smsStatus == 'queued') {
         if (smsConfig.id !== undefined && smsConfig.id !== null) {
            Data.put('event/updateSmsConfig', smsConfig).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    console.log(result);
                }
            });
        } else {
            Data.post('event/addSmsConfig', smsConfig).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    console.log(result);
                }
            });
        }
        } else {
             toaster.pop("error", "", "Invalid Credentials", "Please check your credentials", 10000, 'trustedHtml');
        }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
});

evtApp.controller('sendClientEmailCtrl', function ($scope, Data, $uibModalInstance, maillist, toaster) {
    $scope.mailDet = maillist;
    $scope.cmessage = 'Are you sure you want to send email with subject <span class="text-success"> ' + $scope.mailDet.subject + ' </span>  &nbsp; to  <span class="text-danger">' + $scope.mailDet.leadsCount + '</span> contacts?';
    $scope.sendConfirm = function () {

        $uibModalInstance.close(1);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});
////------------------------------------------------------------ End Bulk Email -------------------------------------------------------------------

///// ------------------------------------------------------------------ Web form Controller -----------------------------------


evtApp.controller('webformCtrl', function ($scope,$rootScope, $http, Data, toaster) {
    sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';
    //$rootScope.title="Website Enquiry Integration";
    $rootScope.addButtonhtml = '&nbsp;';
    $scope.eventLists = {};
    $scope.eventtempSelection = {};
    $scope.organizationId = sessionStorage.getItem("orgId");
    var eventresp = $http.get(Data.serviceBase() + '/event/orgEventList', {params: {"organizationId": $scope.organizationId}});
    eventresp.then(function (response) {
        //console.log(response.data);
        $scope.eventLists = response.data;
        if ($scope.eventLists.length > 0) {
            console.log($scope.eventLists[0]);
            $scope.eventtempSelection = $scope.eventLists[0];
            if ($scope.eventtempSelection.eventKey != null) {
                $scope.codeString = "<script type='text/javascript' src='https://beta.helloleads.io/js/intial.js'></script>"
                        + "<div class='form-bottom' id='form_sample' data-key='" + $scope.eventtempSelection.eventKey + "'></div>";
            } else {
                var keyresp = $http.get(Data.serviceBase() + '/event/generateKey', {params: {"eventId": $scope.eventtempSelection.id}});
                keyresp.then(function (response) {
                    console.log(response.data);
                    $scope.purl = response.data.url;
                    $scope.key = response.data.key;
                    if (response.data.shortURL !== undefined) {
                        $scope.surl = response.data.shortURL;
                        // console.log(response.data.qr_code_image_url);
                        $scope.simg = response.data.qr_code_image_url;
                        $scope.loader = false;
                    }
                     $scope.codeString = "<script type='text/javascript' src='https://beta.helloleads.io/js/intial.js'></script>"
                        + "<div class='form-bottom' id='form_sample' data-key='" +  $scope.key + "'></div>";
                    //$scope.eventName = response.data.eventName;
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }    
        }
        $scope.loader = false;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
  
  $scope.eventKeyChange = function(){
         if ($scope.eventtempSelection.eventKey != null) {
                $scope.codeString = "<script type='text/javascript' src='https://beta.helloleads.io/js/intial.js'></script>"
                        + "<div class='form-bottom' id='form_sample' data-key='" + $scope.eventtempSelection.eventKey + "'></div>";
            } else {
                var keyresp = $http.get(Data.serviceBase() + '/event/generateKey', {params: {"eventId": $scope.eventtempSelection.id}});
                keyresp.then(function (response) {
                    console.log(response.data);
                    $scope.purl = response.data.url;
                    $scope.key = response.data.key;
                    if (response.data.shortURL !== undefined) {
                        $scope.surl = response.data.shortURL;
                        // console.log(response.data.qr_code_image_url);
                        $scope.simg = response.data.qr_code_image_url;
                        $scope.loader = false;
                    }
                     $scope.codeString = "<script type='text/javascript' src='https://beta.helloleads.io/js/intial.js'></script>"
                        + "<div class='form-bottom' id='form_sample' data-key='" +  $scope.key + "'></div>";
                    //$scope.eventName = response.data.eventName;
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }
        }
      



});







////--------------------------------------------------------------------- End web form controller ------------------------------
