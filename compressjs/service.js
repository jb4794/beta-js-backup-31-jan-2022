
evtApp.controller('dispServiceCtrl', function ($scope, $rootScope,alertUser, Data, $uibModal,trialPopUp, $http,toaster) {
    $rootScope.emptyservice = {};

    $scope.loader = true;
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if($scope.roleId === '2'){
        $scope.rolemsg = "Manager";
    }
    else if($scope.roleId === '3'){
         $scope.rolemsg = "L1 User";
    }
    else{
        $scope.rolemsg = "L2 User";
    }
    console.log("Organization Id is " + $scope.organizationId);
    $rootScope.addButtonhtml = '<button class="btn btn-primary btn-sm" ng-click="addService(emptyservice);"><span class="fa fa-plus"></span></button>&nbsp;';
    $rootScope.addButtonhtml += '<button class="btn btn-default btn-sm btn-circle" popover="Interests are list of products or services that your potential customers are interested in. Enter list of products or services that you sell in an event, expo or other marketing situations.  Later for a lead, you can attach, from this list, products or services she/he is interested in. For Apple, the products will be iPAD, iPhone, iMAC, MacBook etc."  popover-trigger="focus" popover-title="Interested In(Products and Services)" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    $rootScope.title = "Interested In";
    if (sessionStorage.getItem("xtime") === "1") {
        $rootScope.firstProfile = true;
    } else {
        $rootScope.firstProfile = false;
    }
    sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';
    $scope.getServices = function () {
        $scope.loader = true;
        var resprom = $http.get(Data.serviceBase() + '/service/service', {params: {key: "organizationId", "value": $scope.organizationId}});

        $scope.services = {};
        resprom.then(function (response) {
            $scope.services = response.data;
            $scope.loader = false;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $rootScope.editService = function (p, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "editInterest"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var modalInstance = $uibModal.open({
            templateUrl: './modifyService',
            controller: 'modifyServiceCtrl',
            size: size,
            backdrop: 'static',
            resolve: {
                service: function () {
                    return p;
                },
                services: function () {
                    return $scope.services;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                var resprom = $http.get(Data.serviceBase() + '/service/service', {params: {key: "organizationId", "value": $scope.organizationId}});

                $scope.services = {};
                resprom.then(function (response) {
                    $scope.services = response.data;
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }
        });
            } else {
                $scope.message = {"title":"Authorization failure","message": "Your current role ( "+$scope.rolemsg+" ) does not allow you to manage Interests. Only account owner or managers (within HelloLeads) can manage Interests. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

    //Adding New Organiztion
    $rootScope.addService = function (p, size) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "addInterest"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var modalInstance = $uibModal.open({
            templateUrl: './modifyService',
            controller: 'modifyServiceCtrl',
            size: size,
            backdrop: 'static',
            resolve: {
                service: function () {
                    return p;
                },
                services: function () {
                    return $scope.services;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                var resprom = $http.get(Data.serviceBase() + '/service/service', {params: {key: "organizationId", "value": $scope.organizationId}});

                $scope.services = {};
                resprom.then(function (response) {
                    $scope.services = response.data;
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }
        });
        } else {
                $scope.message = {"title":"Authorization failure","message": "Your current role ( "+$scope.rolemsg+" ) does not allow you to manage Interests. Only account owner or managers (within HelloLeads) can manage Interests. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

    //Deleting an Organization 
    $scope.delService = function (p, size) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteInterest"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var modalInstance1 = $uibModal.open({
            templateUrl: './deleteService',
            controller: 'delServiceCtrl',
            size: size,
            resolve: {
                service: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i) {
                var resprom = $http.get(Data.serviceBase() + '/service/service', {params: {key: "organizationId", "value": $scope.organizationId}});

                $scope.services = {};
                resprom.then(function (response) {
                    $scope.services = response.data;
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }
        });

        } else {
                $scope.message = {"title":"Authorization failure","message": "Your current role ( "+$scope.rolemsg+" ) does not allow you to manage Interests. Only account owner or managers (within HelloLeads) can manage Interests. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		
			} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
	
	    $scope.fileError = false;
    $scope.getServiceFileDetails = function (e) {

        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "addInterest"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.loader = false;
                    if (response.data.status === 'success') {
                        //console.log("Getting Files");
                        $scope.filesCsv = [];
                        $scope.$apply(function () {

                            // STORE THE FILE OBJECT IN AN ARRAY.
                            for (var i = 0; i < e.files.length; i++) {
                                if ((e.files[i]['name'].indexOf("xls")) !== -1 || (e.files[i]['name'].indexOf("xlsx")) !== -1 && e.files[i].size / 1024 / 1024 < 5) { //(e.files[i]['name'].indexOf("csv")) !== -1 ||
                                    $scope.filesCsv.push(e.files[i]);
                                    $scope.fileError = false;
                                    $scope.uploadServiceFiles();
                                } else {
                                    $scope.fileError = true;
                                }
                            }
                            console.log($scope.fileError);
                        });
                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to manage Interests. Only account owner or managers (within HelloLeads) can manage Interests. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.btnText = "Upload Product groups";
    $scope.btnDisable = false;
    // NOW UPLOAD THE FILES.
    $scope.uploadServiceFiles = function () {

        $scope.btnText = 'Uploading...';
        $scope.btnDisable = true;
        //FILL FormData WITH FILE DETAILS.

        var data = new FormData();
        for (var i in $scope.filesCsv) {
            ////console.log($scope.filesCsv[i]);
            data.append("filelist", $scope.filesCsv[i]);
        }

        data.append("organizationId", sessionStorage.getItem('orgId'));


        var request = {
            method: 'POST',
            url: Data.serviceBase() + "/service/fileuploadExcel/",
            data: data,
            headers: {
                'Content-Type': undefined
            }
        };
        // SEND THE FILES.
        $http(request)
                .success(function (result) {
                    //$uibModalInstance.close(1);
                    $scope.serviceForm.$setPristine();
                    $scope.btnText = "Upload the file";
                    if (result.status === 'success') {
                        $scope.getServices();
                        $scope.message = {"title": "Import Products or Services", "message": result.message};
                        alertUser.alertpopmodel($scope.message);
                    } else {
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    }

                })
                .error(function (result) {
                    //$uibModalInstance.close(1);
                    $scope.btnText = "Upload the file";
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                });
    };
   


});

evtApp.controller('modifyServiceCtrl', function ($scope, Data, $uibModalInstance, service, services, toaster) {
    var original = angular.copy($scope.service);
    $scope.service = angular.copy(service);
    $scope.services = angular.copy(services);

    if ($scope.service.id !== undefined) {
        $scope.title = "Update Product Group";
        $scope.btntxt = "Update";
    } else {
        $scope.title = "Add New Product Group";
        $scope.btntxt = "Add";
    }

    $scope.checkService = function (name) {
        $scope.dupFlag = 0;
        angular.forEach($scope.services, function (d) {

            if (d.name.toLowerCase() === name.toLowerCase()) {
                $scope.dupFlag = 1;

            }


        });
        if ($scope.dupFlag === 1) {
            return true;
        } else {
            return false;
        }

    
};

    $scope.canSaveService = function () {
        return $scope.serviceform.$valid && !angular.equals($scope.service, original);
    };
    $scope.updatservice = function (serv) {

        console.log("Going to add or update service....");
        if (serv.id !== undefined) {

            console.log("its an update to the service....");
            Data.put('service/service', serv).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    //$uibModalInstance.dismiss('Close');
                    console.log('Setting the service ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        } else {
            console.log("its an add to the service....");

            if (serv.name !== undefined) {

                serv.organizationId = sessionStorage.getItem('orgId');

                Data.post('service/service', serv).then(function (result) {
                    if (result.status !== 'error') {

                        $uibModalInstance.close(1);
                        console.log('Setting the service ');
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');


                    } else {
                        console.log(result);
                    }
                });

            }



        }

    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});
evtApp.controller('delServiceCtrl', function ($scope, Data, $uibModalInstance, service, toaster) {

    $scope.service = service;
    $scope.title = "Delete Product Group";
    $scope.deleteService = function (serv) {

        if (serv.name !== undefined) {

            Data.delete('service/service/' + serv.id).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);

                    console.log('Setting the service ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };




});
evtApp.controller('servicecsvuploadctrl', function ($scope, $rootScope, alertUser, Data, toaster, $http) {
    $rootScope.addButtonhtml = "";
    $scope.organizationId = sessionStorage.getItem("orgId");
   


});




