evtApp.controller('disporgCtrl', function ($scope,$rootScope, Data, alertUser, $uibModal, $http,toaster, fileReader) {

    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.currentUserEmailId = sessionStorage.getItem('userEmail');
    $scope.currentUserName = sessionStorage.getItem('firstName');
    $rootScope.title="Company Profile";
    $rootScope.addButtonhtml='<button class="btn btn-default btn-sm btn-circle" popover="Your organization and contact details help us to serve you better. We will use this information for setting up your account and for billing purposes" popover-trigger="focus" popover-title="Organization Profile" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    sessionStorage.setItem("menuact","4");
    //if(sessionStorage.getItem("cFirstName")){
        if(sessionStorage.getItem("xtime")=== "1"){
            $rootScope.firstProfile=true;
        }
        else{
            $rootScope.firstProfile=false;
        }
    //}
	$scope.orgdetOld = {};
	$scope.eorg = {};
    var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

    $scope.emptyorg = {};
    $scope.orgs = {};

    resprom.then(function (response) {
        if(response.data){
            $scope.eorg=response.data[0];
			$scope.orgdetOld = angular.copy($scope.eorg);
        }
        $scope.orgs = response.data;
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
    sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';

	    $scope.changeCurrency = function (p, size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './confirmCommonMessage',
            controller: 'currencyWarningMsgCtrl',
            size: size,
            backdrop: 'static',
            resolve: {
                orgdet: function () {
                    return $scope.orgdetOld;
                }
            }
        });
        modalInstance1.result.then(function (result) {

            if (result === 0) {
                //console.log(result);
				sessionStorage.setItem("orgCurrency",p.orgCurrency);
                 $scope.updateorg(p);
            } else {
               // console.log(result);
                //console.log($scope.orgdetOld.toString());
                $scope.eorg.orgCurrency = $scope.orgdetOld.orgCurrency;
            }

        });




    };
	
	 $scope.countryDet = {};
    $http.get('../../../css/phone.json').success(function (data) {
        $scope.countryDet = data;
    });
	
	 $scope.countryDetail = {};
    $scope.getCountryDet = function () {
        $http.get('../../../css/country.json').success(function (data) {
            $scope.countryDetail = data;
        });
    };


    //Delete my account popup
    $scope.getDeletedAccountDet = function () {
        console.log('Error happened -- ');
    var resproms = $http.get(Data.serviceBase() + '/organization/deleteAccount', {params: {"organizationId": $scope.organizationId}});
    $scope.deleteAccountDet = {};
    resproms.then(function (response) {
        if(response.data){
            $scope.deleteAccountDet=response.data[0];
            console.log("Delete Organization");
            console.log(response.data[0]);
        } else {
            console.log('Error happened in Detele my account-- ');
            console.log(response);    
        }
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
    };


    $scope.deleteMyAccountTemp = function (p, size) {
         var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});
        resprole.then(function (response) {
            $scope.loader = false;
            if (response.data.status === 'success') {
         $scope.relLead = angular.copy(p);
        //console.log(p);
        var modalInstance = $uibModal.open({
            templateUrl: './deleteMyAccountTemp',
            controller: 'deleteorgCtrl',
            size: size,
            backdrop: 'static',
            resolve: {
                orgDetails: function () {
                    return p;
                },
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                
            }
        });
        } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    //End Delete my account popup


    //Clear Image
     $scope.removeImage = function () {
        $scope.eorg.BrndImgMob = null;
    };
    $scope.removeIcon = function () {
        $scope.eorg.BrndImgWeb = null;
    };
    $fileFormat='';
    //Uploda Mobile Skining Brand Image
    $scope.updateMobileCompanyPic = function (e) {
        //$scope.uppflag = 1;
        $scope.$apply(function () {
            //$scope.pfiles = e.files[0];
            $fileFormat = e.files[0].type;
            console.log($fileFormat);
            if ($fileFormat == 'image/jpg' || $fileFormat == 'image/jpeg' || $fileFormat == 'image/png') {
            console.log(parseInt(e.files[0]['size']));
            if (Math.round(parseInt(e.files[0]['size']) / 1024) > 200000) {
                console.log("File size should not exceed 2MB");
                document.getElementById("FileUpload1").value = "";
                toaster.pop("error", "", "File size should not exceed 2MB", 10000, 'trustedHtml');
            } else if (Math.round(parseInt(e.files[0]['size']) / 1024) < 200000) {
                fileReader.readAsDataUrl(e.files[0], $scope)
                        .then(function (result) {
                            //console.log($scope.filecsv);
                            $scope.eorg.BrndImgMob = result;
                            //$scope.MobBrnd = result;
                        });
            }
        } else {
            toaster.pop("error", "", "Please check uploaded image file format", 5000, 'trustedHtml');
        }
        });
    };
     //Uploda Web Skining Brand Image
    $scope.updateWebCompanyPic = function (e) {
        //$scope.uppflag = 1;
        $scope.$apply(function () {
            //$scope.pfiles = e.files[0];
            $fileFormat = e.files[0].type;
            console.log($fileFormat);
            if ($fileFormat == 'image/jpg' || $fileFormat == 'image/jpeg' || $fileFormat == 'image/png') {
            console.log(parseInt(e.files[0]['size']));
            if (Math.round(parseInt(e.files[0]['size']) / 1024) > 200000) {
                console.log("File size should not exceed 2MB");
                document.getElementById("FileUpload2").value = "";
                toaster.pop("error", "", "File size should not exceed 2MB", 10000, 'trustedHtml');
                //$scope.uppflag = 0;
                e.files.length = 0;
            } else if (Math.round(parseInt(e.files[0]['size']) / 1024) < 200000) {
                fileReader.readAsDataUrl(e.files[0], $scope)
                        .then(function (result) {
                            //console.log($scope.filecsv);
                            $scope.eorg.BrndImgWeb = result;
                            //$scope.WebImgBrnd = result;
                        });
            }
        } else {
             toaster.pop("error", "", "Please check uploaded image file format", 5000, 'trustedHtml');
        }
        });
    };
	
    //Editing old Organiztion
    $scope.editorg = function (p, size) {

        var modalInstance = $uibModal.open({
            templateUrl: './editOrg',
            controller: 'editorgCtrl',
            size: 'lg',
            resolve: {
                eorg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

                $scope.emptyorg = {};
                $scope.orgs = {};

                resprom.then(function (response) {
                    $scope.orgs = response.data;
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }
        });


    };

    //Adding New Organiztion
    $scope.addorg = function (p, size) {

        var modalInstance = $uibModal.open({
            templateUrl: './editOrg',
            controller: 'editorgCtrl',
            size: size,
            resolve: {
                eorg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

                $scope.emptyorg = {};
                $scope.orgs = {};

                resprom.then(function (response) {
                    $scope.orgs = response.data;
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }
        });


    };

    //Deleting an Organization 
    $scope.delorg = function (p, size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './deleteOrg',
            controller: 'delorgCtrl',
            size: size,
            resolve: {
                dorg: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i) {
                var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

                $scope.emptyorg = {};
                $scope.orgs = {};

                resprom.then(function (response) {
                    $scope.orgs = response.data;
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }
        });


    };
    $scope.canSaveOrg = function () {
        return $scope.editorgform.$valid && !angular.equals($scope.eorg, original);
    };

    $scope.deleteMyAccountFailure = function () {
        $scope.message = {"title": "Authorization failure", "message": "Dear " + $scope.currentUserName + ", Only account owner (Account registered user) can request for deleting an account. Please contact your account owner for assistance."};
        alertUser.alertpopmodel($scope.message);
    };

    $scope.clickDeletionMyAccount = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('organization/setDeleteMyAccount', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.updateorg = function (org) {
    	var messages = [];
    	var message = [];
        if (org.contactFirstName !== '' && org.contactFirstName !== null && org.contactFirstName !== undefined && org.contactLastName !== '' && org.contactLastName !== null && org.contactLastName !== undefined 
            && org.contactEmail !== '' && org.contactEmail !== null && org.contactEmail !== undefined && org.contactPhone !== '' && org.contactPhone !== null && org.contactPhone !== undefined && org.city !== '' 
            && org.city !== null && org.city !== undefined && org.state !== '' && org.state !== null && org.state !== undefined && org.zip !== '' && org.zip !== null && org.zip !== undefined && org.country !== '' 
            && org.country !== null && org.country !== undefined && org.displayName !== '' && org.displayName !== null && org.displayName !== undefined) {
        if (org.id !== undefined) {


            Data.put('organization/organization', org).then(function (result) {
                if (result.status !== 'error') {
                    //$uibModalInstance.close(1);
                    //$uibModalInstance.dismiss('Close');
                    sessionStorage.setItem("xtime", '0');
                    sessionStorage.setItem("cFirstName" ,org.contactFirstName);
                    sessionStorage.setItem("cOrgName",org.displayName);
                    sessionStorage.setItem("cLastName",org.contactLastName);
                    sessionStorage.setItem("firstName",org.contactFirstName);
					sessionStorage.setItem("orgCurrency",org.orgCurrency);
                    sessionStorage.setItem("lastName",org.contactLastName);
                    $rootScope.firstName = sessionStorage.getItem("firstName");
                    console.log('Setting the organization ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    sessionStorage.setItem("menuact", 1);
                    window.location.href = Data.webAppBase() + 'account/company';
                } else {
                    console.log(result);
                }
            });

        }
    } else {
    	if (org.contactFirstName == '' || org.contactFirstName == null || org.contactFirstName == undefined) {
    		$scope.fname = "First Name";
    	}
    	if (org.contactLastName == '' || org.contactLastName == null || org.contactLastName == undefined) {
    		$scope.lname = "Last Name";	
    	}
        if (org.contactEmail == '' || org.contactEmail == null || org.contactEmail == undefined) {
        	$scope.email = "Email";
        }
        if (org.contactPhone == '' || org.contactPhone == null || org.contactPhone == undefined) {
        	$scope.phone = "Phone";
        }
        if (org.city == '' || org.city == null || org.city == undefined) {
        	$scope.city = "City";
        }
        if (org.state == '' || org.state == null || org.state == undefined) {
        	$scope.state = "State";
        }
        if (org.zip == '' || org.zip == null || org.zip == undefined) {
        	$scope.zip = "Zip";
        }
        if (org.country == '' || org.country == null || org.country == undefined) {
        	$scope.country = "Country";
        }
        if (org.displayName == '' || org.displayName == null || org.displayName == undefined) {
         	$scope.orgname = "Organization";	
 		}
 		var messages = [$scope.fname,$scope.lname,$scope.email,$scope.phone,$scope.city,$scope.state,
 		$scope.zip,$scope.country,$scope.orgname];
 		var filtered = messages.filter(function (el) {
		  return el != null;
		});
 		toaster.pop("error", "", "Please fill the "+filtered+" (*) Required fields", 10000, 'trustedHtml');
    } 
    };
});
evtApp.controller('currencyWarningMsgCtrl', function ($scope, Data, $http,$uibModalInstance, orgdet, toaster) {

    $scope.organizationId = sessionStorage.getItem('orgId');
    //$scope.visitorCount = '0';
    var resprom = $http.get(Data.serviceBase() + '/visitor/visitorDealCount', {params: {"orgId": $scope.organizationId}});
    resprom.then(function (response) {
        console.log(response.data);
        $scope.visitorCount = response.data;
        $scope.custommessage = "Please note you have "+ $scope.visitorCount+" records with deal values in " + orgdet.orgCurrency + ".<br><br> Do you still want to change the currency? <br><br> If you change, you need to change the deal values accordingly.";
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });

    $scope.customtitle = "Warning : Change of Currency for Deal value";
    $scope.canceltxt = "No. I don’t want to change";
    $scope.successtxt = "Yes. I want to change";
    $scope.customConfirm = function () {
        console.log('success');
        $uibModalInstance.close(0);
    };
    $scope.customCancel = function () {
        console.log('cancel');
        $uibModalInstance.close(1);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});


evtApp.controller('deleteorgCtrl', function ($scope, Data, $uibModal, $uibModalInstance, toaster, orgDetails) {
    var original = angular.copy($scope.orgDetails);
    $scope.orgDetails = angular.copy(orgDetails);
    $scope.deleteAccount = function (orgDetails) {
        if (orgDetails.id !== undefined) {
            if (orgDetails.duration == 'later') {
                Data.post('organization/delMyAccount', orgDetails).then(function (result) {
                    if (result.status !== 'error') {
                        $uibModalInstance.close(1);
                        var modalInstance = $uibModal.open({
                            templateUrl: './deleteMyAccountPopup',
                            controller: 'deleteorgCtrl',
                            size: 'md',
                            backdrop: 'static',
                            resolve: {
                                orgDetails: function () {
                                    return $scope.orgDetails;
                                },
                            }
                        });
                        //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    } else {
                        console.log(result);
                    }
                });
            } else {
                Data.post('organization/delMyAccount', orgDetails).then(function (result) {
                    if (result.status !== 'error') {
                        $scope.orgId = orgDetails.id;
                        $scope.email = orgDetails.contactEmail;
                        var finalOutput = window.open("https://beta.helloleads.io/index.php/DeleteMyAccount/accountExportNow?orgId="+$scope.orgId+"&email="+$scope.email, "_self");
                        $uibModalInstance.close(1);
                        var modalInstance = $uibModal.open({
                            templateUrl: './deleteMyAccountLoader',
                            controller: 'deleteorgCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            keyboard: false,
                            resolve: {
                                orgDetails: function () {
                                    return $scope.orgDetails;
                                },
                            }
                        });
                    } else {
                        console.log(result);
                    }
                });
            }
        }
    };

    $scope.deleteAccountConfirm = function (orgDetails) {
        var modalInstance = $uibModal.open({
            templateUrl: './deleteMyAccountConfirmation',
            controller: 'deleteorgCtrl',
            size: 'md',
            backdrop: 'static',
            keyboard: false,
            resolve: {
                orgDetails: function () {
                    return $scope.orgDetails;
                },
            }
        });
    };

    $scope.cancelPopup = function () {
        $uibModalInstance.dismiss('Close');
    };

    $scope.cancel = function () {
        window.location.href = Data.webAppBase() + 'account/company';
        $uibModalInstance.dismiss('Close');
    };
});



evtApp.controller('editorgCtrl', function ($scope, Data, $uibModalInstance, eorg, toaster) {
    var original = angular.copy($scope.eorg);
    $scope.eorg = angular.copy(eorg);
    if ($scope.eorg.id !== undefined) {
        $scope.title = "Update Organization";
        $scope.btntxt = "Update";
    } else {
        $scope.title = "Add New Organization";
        $scope.btntxt = "Add";
    }
   
    $scope.updateorg = function (org) {
    	var messages = [];
    	var message = [];
        if (org.contactFirstName !== '' && org.contactFirstName !== null && org.contactFirstName !== undefined && org.contactLastName !== '' && org.contactLastName !== null && org.contactLastName !== undefined 
            && org.contactEmail !== '' && org.contactEmail !== null && org.contactEmail !== undefined && org.contactPhone !== '' && org.contactPhone !== null && org.contactPhone !== undefined && org.city !== '' 
            && org.city !== null && org.city !== undefined && org.state !== '' && org.state !== null && org.state !== undefined && org.zip !== '' && org.zip !== null && org.zip !== undefined && org.country !== '' 
            && org.country !== null && org.country !== undefined && org.displayName !== '' && org.displayName !== null && org.displayName !== undefined) {
        if (org.id !== undefined) {


            Data.put('organization/organization', org).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    //$uibModalInstance.dismiss('Close');
                    console.log('Setting the organization ')
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        } else {

            if (org.name !== undefined) {

                Data.post('organization/organization', org).then(function (result) {
                    if (result.status !== 'error') {

                        $uibModalInstance.close(1);
                        console.log('Setting the organization ')
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');


                    } else {
                        console.log(result);
                    }
                });

            }
        }
    } else {
    	if (org.contactFirstName == '' || org.contactFirstName == null || org.contactFirstName == undefined) {
    		$scope.fname = "First Name";
    	}
    	if (org.contactLastName == '' || org.contactLastName == null || org.contactLastName == undefined) {
    		$scope.lname = "Last Name";	
    	}
        if (org.contactEmail == '' || org.contactEmail == null || org.contactEmail == undefined) {
        	$scope.email = "Email";
        }
        if (org.contactPhone == '' || org.contactPhone == null || org.contactPhone == undefined) {
        	$scope.phone = "Phone";
        }
        if (org.city == '' || org.city == null || org.city == undefined) {
        	$scope.city = "City";
        }
        if (org.state == '' || org.state == null || org.state == undefined) {
        	$scope.state = "State";
        }
        if (org.zip == '' || org.zip == null || org.zip == undefined) {
        	$scope.zip = "Zip";
        }
        if (org.country == '' || org.country == null || org.country == undefined) {
        	$scope.country = "Country";
        }
        if (org.displayName == '' || org.displayName == null || org.displayName == undefined) {
         	$scope.orgname = "Organization";	
 		}
 		var messages = [$scope.fname,$scope.lname,$scope.email,$scope.phone,$scope.city,$scope.state,
 		$scope.zip,$scope.country,$scope.orgname];
 		var filtered = messages.filter(function (el) {
		  return el != null;
		});
 		toaster.pop("error", "", "Please fill the "+filtered+" (*) Required fields", 10000, 'trustedHtml');
    }
    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});
evtApp.controller('customizeCtrl', function ($scope, $rootScope, Data, $http, toaster) {
    $rootScope.addButtonhtml = '';
    $rootScope.title = "Qualifiers";
    $scope.rcuststage = [];
    // Default Page tab is User Profile
    $scope.qtab = 1;
    angular.element(document.getElementById("interestTab")).addClass("active");
    angular.element(document.getElementById("categoryTab")).removeClass("active");
    angular.element(document.getElementById("spcustTab")).removeClass("active");
     angular.element(document.getElementById("tagTab")).removeClass("active");
    $scope.switchqTab = function (tabId) {
        console.log(tabId);
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
        if (tabId === 1) {

            $scope.qtab = 1;
            angular.element(document.getElementById("interestTab")).addClass("active");
            angular.element(document.getElementById("categoryTab")).removeClass("active");
            angular.element(document.getElementById("spcustTab")).removeClass("active");
            angular.element(document.getElementById("tagTab")).removeClass("active");
             Data.post('organization/setTabOne', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else if (tabId === 2) {
            $scope.qtab = 2;
            angular.element(document.getElementById("interestTab")).removeClass("active");
            angular.element(document.getElementById("categoryTab")).addClass("active");
            angular.element(document.getElementById("spcustTab")).removeClass("active");
            angular.element(document.getElementById("tagTab")).removeClass("active");
             Data.post('organization/setTabTwo', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else if(tabId === 3) {
            $scope.qtab = 3;
            angular.element(document.getElementById("interestTab")).removeClass("active");
            angular.element(document.getElementById("categoryTab")).removeClass("active");
            angular.element(document.getElementById("spcustTab")).addClass("active");
            angular.element(document.getElementById("tagTab")).removeClass("active");
            Data.post('organization/setTabThree', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        }else{
            $scope.qtab = 4;
            angular.element(document.getElementById("interestTab")).removeClass("active");
            angular.element(document.getElementById("categoryTab")).removeClass("active");
            angular.element(document.getElementById("tagTab")).addClass("active");
            angular.element(document.getElementById("spcustTab")).removeClass("active");
            Data.post('organization/setTabFour', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        }

    };
});
evtApp.controller('delorgCtrl', function ($scope, Data, $uibModalInstance, dorg, toaster) {

    $scope.dorg = dorg;
    $scope.title = "Delete Organization";
    $scope.deleteorg = function (org) {

        if (org.name !== undefined) {

            Data.delete('organization/organization/' + org.id).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    // $uibModalInstance.dismiss('Close');
                    console.log('Setting the organization ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };




});
evtApp.controller('facebookCtrl', function ($scope, $rootScope,alertUser,trialPopUp,$uibModal, $location, $window, Data, $http, toaster) {
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.cshow = false;
	$scope.cspashow = 'block';
    $rootScope.title = "Facebook Integration";
    $rootScope.addButtonhtml = "";
	$scope.userId = sessionStorage.getItem('userId');
    $scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }
	sessionStorage.setItem("menuact", "9");
    $rootScope.menuact = '9';
    var resprom = $http.get(Data.webAppBase() + 'facebook/fblogin', {params: {}});
    resprom.then(function (response) {
        console.log(response.data);
        if (response.data.status === 'success') {
            $scope.fbloginlink = response.data.loginURL;
            $scope.checkAuth();
        }


    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });

    $scope.connectFacebook = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('organization/setConnectFacebook', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.subscribeFBPage = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('organization/setSubscribeFBPage', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.openURL = function () {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
		 var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "changeHistory"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
				window.open($scope.fbloginlink);
				
			} else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to Integrate Facebook with HelloLeads. Users with account owner or manger roles can Integrate Facebook with HelloLeads. Please contact HelloLeads account owner or mangers in your company for assistance with this task."};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

    $window.onfocus = function () {
        if ($scope.fbAuthId === undefined) {
            $scope.checkAuth();
        }
    };

    $scope.checkAuth = function () {
        $scope.loaderView = true;
        var resprom = $http.get(Data.webAppBase() + 'facebook/fbcheckAuth', {params: {"orgId": $scope.organizationId}});
        resprom.then(function (response) {
            console.log(response);
            if (response.data.status === 'success') {
                $scope.fbAuthId = response.data.fbId;
                $scope.fbaccountName = response.data.fbname;
				$scope.cspashow = 'none';
                $scope.getPages();
            } else {
				//$scope.fbloginlink = response.data.loginURL;
				//$scope.checkAuth();
                if (response.data !== '') {
					
					//$scope.loaderView = true;
                    toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
					//$scope.cshow = true;
                }
                $scope.cshow = true;
                $scope.loaderView = false;
            }
            
           
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.pages = {};
	$scope.mapadsFlag = true;
    $scope.getPages = function () {

        if ($scope.pages !== undefined) {
            var resprom = $http.get(Data.webAppBase() + 'facebook/manage_pages', {params: {}});
            resprom.then(function (response) {
                console.log(response);
                if (response.data.status === 'success') {
                    $scope.pages = JSON.parse(response.data.pages);
                    angular.forEach($scope.pages,function(d){
                       if(d.page_subs === '0'){
                           //console.log("Page subscribed");
                           $scope.mapadsFlag = false;
                       } 
                    });
					if ($scope.pages.length === 0) {
                        $scope.erroMsg = "No pages associated with this Facebook account";
                    }
                    $scope.cshow = false;
                } else {
					$scope.loaderView = true;
                    toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
					$scope.cshow = true;
                }

                $scope.loaderView = false;
                
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
        }
        ;
    };
	$scope.facebookAds = function (p, size) {
        var modalInstance1 = $uibModal.open({
            templateUrl: './facebookAds',
            controller: 'facebookAdsCtrl',
            size: 'lg',
            resolve: {
                fbdet: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i) {
                console.log("Test");
            }
        });
    };
    $scope.subscribe_page = function (page) {
        $scope.cNotify = true;
        $scope.loaderView = true;
        page['fbId'] = $scope.fbAuthId;
        page['orgId'] = $scope.organizationId;
        if (page.page_subs === '0') {
            var resprom = $http.post(Data.webAppBase() + 'facebook/page_unsubscribe', page);
            resprom.then(function (response) {
                console.log(response);
                if (response.data.status === 'success') {
                     toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                     //$scope.message = {"title": "Facebook Page subscribed", "message": "Your Facebook page "+page.name+" has been linked to your HelloLeads Account.A new list Fb_Leads_"+page.name+" has been created to capture leads from "+page.name+". Review the leads under #Fb_Leads_"+page.name+" to view your Facebook leads."};
                
                    $scope.getPages();
                } else {
                    toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }

                $scope.loaderView = false;
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
        } else {
            var resprom = $http.post(Data.webAppBase() + 'facebook/page_subscribe', page);
            resprom.then(function (response) {
                console.log(response);
                if (response.data.status === 'success') {
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                     /*$scope.message = {"title": "Facebook Page subscribed", "message": "<h4>Your Facebook page <b>"+page.name+"</b> has been linked to your HelloLeads Account. A new list <b>Fb_Leads_"+page.name+" </b>has been created to capture leads from "+page.name+" Lead ads.</h4>"};
					 alertUser.alertpopmodel($scope.message);*/
					 $scope.facebookAds($scope.fbAuthId);
                    $scope.getPages();
                } else {
                    toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }

                $scope.loaderView = false;
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
        }
    };
	
	
	$scope.redirectToLeads = function(fbevent){
        
        localStorage.setItem('curEventId', fbevent.eventId);
        localStorage.setItem('curEventName', fbevent.eventName);
        localStorage.setItem('curEventsend', '1');
        localStorage.setItem('curEventshare', '1');
        localStorage.setItem('filters', "");
        window.location.href = Data.webAppBase() + 'account/manageVisitor';
        
    };

	$scope.deleteFb = function () {
        var resprom = $http.get(Data.webAppBase() + 'facebook/fbDelete', {params: {"orgId": $scope.organizationId}});
        resprom.then(function (response) {
            console.log(response);
            if (response.data.status === 'success') {
                toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                $scope.getPages();
            } else {
                toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }

            $scope.loaderView = false;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
});
// Facebook Ads list and event list display and submitting
evtApp.controller('facebookAdsCtrl', function ($scope, Data, $uibModalInstance, fbdet, $http, toaster) {
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.fbId = fbdet;
    $scope.loaderView = false;
    $scope.adsDet = {};
    $scope.adEvents = {};
    $scope.eventtempSelectionId = [];
    $scope.eventtempSelection = [];
    //$scope.title = "Delete Organization";
    $scope.getListAds = function () {

        if ($scope.fbId !== undefined) {
            $scope.loaderView = true;
            var resprom = $http.get(Data.webAppBase() + 'facebook/facebookAds', {params: {"orgId": $scope.organizationId}});
            resprom.then(function (response) {
                console.log(response.data);
                if (response.data.status === 'success') {
                    $scope.adsDet = response.data.addet;

                } else {
                    toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }

                // $scope.loaderView = false;
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
            var resprom = $http.get(Data.serviceBase() + 'event/getFacebookList', {params: {"orgId": $scope.organizationId}});
            resprom.then(function (response) {
                // console.log(response.data);

                $scope.adEvents = response.data;

                angular.forEach($scope.adsDet, function (value, key) {
                    angular.forEach($scope.adEvents, function (d) {
                        if (value.eventId === d.id) {
                            $scope.eventtempSelection[key] = d;
                            $scope.eventtempSelectionId[key] = d.id;
                        }

                    });


                });


                $scope.loaderView = false;
            }, function (response) {
                toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                console.log('Error happened -- ');
                console.log(response);
            });
        }



    };

    $scope.setFblist = function (index, event) {
        //console.log(event);
        if (event !== null) {
            $scope.adsDet[index]['eventId'] = event.id;
            $scope.eventtempSelectionId[index] = event.id;
            $scope.eventtempSelection[index] = event;
        } else {
            $scope.adsDet[index]['eventId'] = null;
            $scope.eventtempSelectionId[index] = null;
            $scope.eventtempSelection[index] = null;
        }
    };
    $scope.updateFbAd = function () {
        console.log($scope.adsDet);
        var resprom = $http.post(Data.webAppBase() + 'facebook/updateFbAds', $scope.adsDet);
        resprom.then(function (response) {
            console.log(response);
            if (response.data.status === 'success') {
                toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                // $scope.message = {"title": "Facebook Page subscribed", "message": "<h4>Your Facebook page <b>"+page.name+"</b> has been linked to your HelloLeads Account. A new list <b>Fb_Leads_"+page.name+" </b>has been created to capture leads from "+page.name+" Lead ads</h4>"};
                // alertUser.alertpopmodel($scope.message);


            } else {
                toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }

            $scope.loaderView = false;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.redirectToLeads = function (fbevent) {

        localStorage.setItem('curEventId', fbevent.id);
        localStorage.setItem('curEventName', fbevent.name);
        localStorage.setItem('curEventsend', fbevent.sendGreet);
        localStorage.setItem('curEventshare', fbevent.listlead_share);
        localStorage.setItem('filters', "");
        // window.location.href = Data.webAppBase() + 'account/manageVisitor';
        window.open(Data.webAppBase() + 'account/manageVisitor', '_blank');
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
// Api Integration controller
evtApp.controller('apiIntegCtrl', function ($scope, $rootScope,alertUser,trialPopUp, $location, $window, Data, $http, toaster) {

    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.apiDet = {};
    $rootScope.title="API Integration";
	var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
		 var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "changeHistory"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
    var resprom = $http.get(Data.serviceBase() + 'organization/apiRequest', {params: {"orgId": $scope.organizationId}});
    resprom.then(function (response) {
        console.log(response.data[0]);
        if (response.data) {
            $scope.apiDet = response.data[0];
        } else{
            $scope.createApi();
        }

        $scope.loaderView = false;
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
	} else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to access API Intgeration. Users with account owner or manger roles can access API Intgeration. Please contact HelloLeads account owner or mangers in your company for assistance with this task."};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
     $scope.createApi = function () {
        $scope.orgDet = {"orgId": $scope.organizationId, "userId": $scope.userId};
        Data.post('organization/createApiKey', $scope.orgDet).then(function (result) {
             $scope.apiDet = result.data;
            
        });

    };

    $scope.copyAPI = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('organization/setCopyAPI', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.downloadAPIDoc = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('organization/setDownloadAPIDoc', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

});
// ---------------------------------------- Email Templates --------------------------------------------------------------
evtApp.controller('emailTemporgCtrl', function ($scope, $rootScope, alertUser, warningPopup, trialPopUp,$uibModal, $location, $window, Data, $http, toaster) {
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $rootScope.title = "Manage Email Templates";
    $rootScope.addButtonhtml = "";
    sessionStorage.setItem('menuact', '5');
    $rootScope.menuact = '5';
    $scope.emailTemp = {};
    $scope.emailTemplates = {};
    $scope.emailWishesTemplate = {};
    $scope.emailSplWishesTemplate = {};
    $scope.emailloader1 = true;
    $scope.toggle = {};

    $scope.itab = 1;
    angular.element(document.getElementById("generalEmail1")).addClass("active");
    angular.element(document.getElementById("generalEmail2")).addClass("active");
    angular.element(document.getElementById("autoEmail1")).removeClass("active");
    angular.element(document.getElementById("autoEmail2")).removeClass("active");
    angular.element(document.getElementById("autoEmail3")).removeClass("active");
     $scope.switchiTab = function (tabId) {
        if (tabId === 1) {
            $scope.itab = 1;
            angular.element(document.getElementById("generalEmail1")).addClass("active");
            angular.element(document.getElementById("generalEmail2")).addClass("active");
            angular.element(document.getElementById("autoEmail1")).removeClass("active");
            angular.element(document.getElementById("autoEmail2")).removeClass("active");
    		angular.element(document.getElementById("autoEmail3")).removeClass("active");
        } else if (tabId === 2) {
            $scope.itab = 2;
            angular.element(document.getElementById("generalEmail1")).removeClass("active");
            angular.element(document.getElementById("generalEmail2")).removeClass("active");
            angular.element(document.getElementById("autoEmail1")).addClass("active");
            angular.element(document.getElementById("autoEmail2")).addClass("active");
    		angular.element(document.getElementById("autoEmail3")).removeClass("active");
        } else {
            $scope.itab = 2;
            angular.element(document.getElementById("generalEmail1")).removeClass("active");
            angular.element(document.getElementById("generalEmail2")).removeClass("active");
            angular.element(document.getElementById("autoEmail1")).addClass("active");
            angular.element(document.getElementById("autoEmail2")).removeClass("active");
    		angular.element(document.getElementById("autoEmail3")).addClass("active");
        }
    };


    //Tiny MCE begin
   /* 
	   $scope.tinymceOptions = {
        selector: "textarea",
        entity_encoding:"UTF-8",
    height: 500,
  resize: true,
  autosave_ask_before_unload: false,
  paste_data_images: true,
  plugins: [
    " code paste anchor autolink codesample colorpicker fullscreen help image imagetools",
    " lists link media noneditable preview lineheight advlist",
    " searchreplace table textcolor wordcount"
  ], /* removed:  charmap insertdatetime print 
  external_plugins: {
    moxiemanager: "https://www.tiny.cloud/pro-demo/moxiemanager/plugin.min.js"
    
  },
  
  templates: [],
  toolbar:
    "a11ycheck undo redo | bold italic fontselect fontsizeselect| forecolor backcolor | lineheightselect indent outdent | alignleft aligncenter alignright alignjustify | bullist numlist fullpage | link image mybutton mybutton1 signbutton ",
  
  content_css: [
    "https://fonts.googleapis.com/css?family=Lato|Open+Sans|PT+Serif",
     "https://beta.helloleads.io/css/tinymcecontent.css"
  ],
  spellchecker_dialog: true,
  setup: function (editor) {
            editor.addButton('mybutton', {
                text: '#VisitorName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorName#');
                }
            });
            editor.addButton('mybutton1', {
                text: '#VisitorCompanyName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorCompanyName#');
                }
            });
			editor.addButton('signbutton', {
                text: 'Signature',
                title: 'Signature',
                type: 'menubutton',
                icon: false,
                menu: [
                    {
                        text: '#firstname',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#firstname#');
                        }
                    },
                    {
                        text: '#lastname',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#lastname#');
                        }

                    },
                    {
                        text: '#designation',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#designation#');
                        }
                    },
                    {
                        text: '#company',
                        icon: false,
                        onclick: function () {
                            var orgname = sessionStorage.getItem('cOrgName');
                            editor.insertContent(orgname);
                        }
                    },
                    {
                        text: '#email',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#email#');
                        }
                    },
                    {
                        text: '#mobile',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#mobile#');
                        }
                    },
                ]
            });

            
            /* editor.addButton('mybutton2', {
             text: '#EventName',
             icon: false,
             onclick: function () {
             editor.insertContent('#EventName#');
             }
             });
        }
  
};*/$scope.tinymceOptions = {
        selector: 'textarea',
        statusbar: false,
	   entity_encoding: "UTF-8",
        elementpath: false,
        height: 375,
        theme: 'modern',
        menubar: true,		
        autosave_ask_before_unload: false,
		paste_data_images: true,
        forced_root_block_attrs: {
            "class": "myclass",
            "data-something": "my data",
            "style": "margin: 5px 0;"
        },
		images_upload_handler: function (blobInfo, success, failure) {
            success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64());
        },
        plugins: [
            'advlist autolink lists link image noneditable preview hr pagebreak table textcolor imagetools ',
            'searchreplace wordcount visualblocks visualchars code paste lineheight emoticons uploadimage ',
            'insertdatetime media nonbreaking save table colorpicker fullscreen contextmenu directionality'
        ],
        lineheight_formats: "LineHeight 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
        toolbar1: 'undo redo | styleselect | fontselect fontsizeselect | bold italic',
        toolbar2: 'forecolor backcolor | lineheightselect | bullist numlist | link uploadimage emoticons | alignleft aligncenter alignright alignjustify indent outdent',
        toolbar3: 'mybutton,mybutton1 signbutton ',
       content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            "https://beta.helloleads.io/css/tinymcecontent.css"],
        content_style: ".tinymce-content p {  padding: 0; margin: 1px 0; }",
        setup: function (editor) {
            editor.addButton('mybutton', {
                text: '#VisitorName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorName#');
                }
            });
            editor.addButton('mybutton1', {
                text: '#VisitorCompanyName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorCompanyName#');
                }
            });
           editor.addButton('signbutton', {
                text: 'Signature',
                title: 'Signature',
                type: 'menubutton',
                icon: false,
                menu: [
                    {
                        text: '#firstname',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#firstname#');
                        }
                    },
                    {
                        text: '#lastname',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#lastname#');
                        }

                    },
                    {
                        text: '#designation',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#designation#');
                        }
                    },
                    {
                        text: '#company',
                        icon: false,
                        onclick: function () {
                            var orgname = sessionStorage.getItem('cOrgName');
                            editor.insertContent(orgname);
                        }
                    },
                    {
                        text: '#email',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#email#');
                        }
                    },
                    {
                        text: '#mobile',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#mobile#');
                        }
                    },
                ]
            });
        }
    };

    $scope.selectorgTemp = function (empt) {
        $scope.emailTemp = empt;
    };
    $scope.resetForm = function () {
        $scope.emailTemp = {};
    };

    $scope.redirectToM = function (url) {
       window.location.href = Data.webAppBase() + url;
    };


    $scope.saveEmailTemp = function (emailTemp) {
        $scope.emailTemp.organizationId = $scope.organizationId;
        $scope.emailTemp.userId = $scope.userId;
        // Checking for same name----------

        if ($scope.emailTemp.id === undefined) {
            angular.forEach($scope.emailTemplates, function (d) {
                if (d.name === $scope.emailTemp.name) {
                    toaster.pop("error", "", $scope.emailTemp.name + " already exists in email template list", 10000, 'trustedHtml');
                    return;
                }
            });
            Data.post('organization/emailTemplate', $scope.emailTemp).then(function (response) {
                if (response.status === 'success') {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                    $scope.emailTemp = {};
                    $scope.getAllOrgTemplate();
                } else {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                }

            });

        } else {
            Data.put('organization/emailTemplate', $scope.emailTemp).then(function (response) {
                if (response.status === 'success') {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                    $scope.emailTemp = {};
                    $scope.getAllOrgTemplate();
                } else {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                }

            });
        }
    };

    $scope.emailToggleAction = function (emailWishesTemplate) {
        $scope.dob = "";
        if (emailWishesTemplate.dob !== '0') {
         var modalInstance = $uibModal.open({
                templateUrl: './wisheswarningPopup',
                controller: 'wisheswarningPopupCtrl',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    dob: function () {
                    return $scope.emailWishesTemplate.dob;
                },
              }
            });
          modalInstance.result.then(function (i) {
            if (i !== '0') {
                $scope.toggle.switch = true;
                $scope.emailWishesTemplate.dob = '0';
                $scope.saveWishesEmailTemp(emailWishesTemplate);
            } else {
                $scope.toggle.switch = false;
                $scope.emailWishesTemplate.dob = '1';
                $scope.saveWishesEmailTemp(emailWishesTemplate);
            }
          });
      } else {
                $scope.toggle.switch = false;
                $scope.emailWishesTemplate.dob = '1';
                $scope.saveWishesEmailTemp(emailWishesTemplate);
      }
    };

    $scope.mailToggleAction = function (emailSplWishesTemplate) {
        $scope.sdate = "";
        if (emailSplWishesTemplate.sdate !== '0') {
        var modalInstance = $uibModal.open({
                templateUrl: './wisheswarningPopup',
                controller: 'specialwisheswarningPopupCtrl',
                size: 'md',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    sdate: function () {
                    return $scope.emailSplWishesTemplate.sdate;
                },
              }
            });
        modalInstance.result.then(function (i) {
            if (i !== '0') {
                $scope.toggle.switchs = true;
                 $scope.emailSplWishesTemplate.sdate = '0';
                 $scope.saveSplWishesEmailTemp(emailSplWishesTemplate);
            } else {
                $scope.toggle.switchs = false;
                $scope.emailSplWishesTemplate.sdate = '1';
                $scope.saveSplWishesEmailTemp(emailSplWishesTemplate);
            }
          });
        } else {
                $scope.toggle.switchs = false;
                $scope.emailSplWishesTemplate.sdate = '1';
                $scope.saveSplWishesEmailTemp(emailSplWishesTemplate);
        }
    };

    $scope.saveWishesEmailTemp = function (emailWishesTemplate) {
        $scope.emailWishesTemplate.organizationId = $scope.organizationId;
        $scope.emailWishesTemplate.userId = $scope.userId;
        console.log('DOB '+$scope.emailWishesTemplate.dob);
        if ($scope.emailWishesTemplate.id === undefined) {
            angular.forEach($scope.emailWishesTemplate, function (d) {
                if (d.name === $scope.emailWishesTemplate.name) {
                    toaster.pop("error", "", $scope.emailWishesTemplate.name + " already exists in email template list", 10000, 'trustedHtml');
                    return;
                }
            });
            Data.post('organization/emailWishesTemplate', $scope.emailWishesTemplate).then(function (response) {
                if (response.status === 'success') {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                    $scope.emailWishesTemplate = {};
                    $scope.getWishesOrgTemplate();
                } else {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                }
            });
        } else {
            Data.put('organization/emailWishesTemplate', $scope.emailWishesTemplate).then(function (response) {
                if (response.status === 'success') {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                    $scope.emailWishesTemplate = {};
                    $scope.getWishesOrgTemplate();
                } else {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                }
            });
        }
    };

    $scope.saveSplWishesEmailTemp = function (emailSplWishesTemplate) {
        $scope.emailSplWishesTemplate.organizationId = $scope.organizationId;
        $scope.emailSplWishesTemplate.userId = $scope.userId;
        console.log('SDATE '+$scope.emailSplWishesTemplate.sdate);
        if ($scope.emailSplWishesTemplate.id === undefined) {
            angular.forEach($scope.emailSplWishesTemplate, function (d) {
                if (d.name === $scope.emailSplWishesTemplate.name) {
                    toaster.pop("error", "", $scope.emailSplWishesTemplate.name + " already exists in email template list", 10000, 'trustedHtml');
                    return;
                }
            });
            Data.post('organization/emailWishesTemplate', $scope.emailSplWishesTemplate).then(function (response) {
                if (response.status === 'success') {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                    $scope.emailSplWishesTemplate = {};
                    $scope.getSplWishesOrgTemplate();
                } else {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                }
            });
        } else {
            Data.put('organization/emailWishesTemplate', $scope.emailSplWishesTemplate).then(function (response) {
                if (response.status === 'success') {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                    $scope.emailSplWishesTemplate = {};
                    $scope.getSplWishesOrgTemplate();
                } else {
                    toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                }
            });
        }
    };

    $scope.deleteTemplate = function (emtmp) {
        var dflag = '0';
                var msg = {"message": "Are you sure you want to delete this template - <b>" + emtmp.name + "</b> ?<br>", "title": "Confirmation", "canceltxt": "Cancel", "oktxt": "Delete"};
        warningPopup.warningpopmodel(msg);
        $scope.$on('warnresp', function (event, args) {
            
            if (args.status === 'true' && dflag === '0') {
                
                var resprom = $http.get(Data.serviceBase() + 'organization/deleteEmailTemplate', {params: {"id": emtmp.id}});
                resprom.then(function (response) {
                    toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    $scope.emailTemp = {};
                    $scope.getAllOrgTemplate();
                    dflag = '1';
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }else{
                dflag = '1';
            }
        });
    }


        $scope.getAllOrgTemplate = function () {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "bulkEmail", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    if (response.data.status === 'success') {	
					var resprom = $http.get(Data.serviceBase() + 'organization/emailTemplate', {params: {"organizationId": $scope.organizationId}});
					resprom.then(function (response) {
						$scope.emailTemplates = response.data;
						$scope.emailloader1 = false;
					}, function (response) {
						console.log('Error happened -- ');
						console.log(response);
					});
		 }else{
                        $scope.orgemtPlanCheck(response.data);
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.getWishesOrgTemplate = function () {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});
        $scope.orgName = sessionStorage.getItem('cOrgName');
        $scope.firstName = sessionStorage.getItem("firstName");
        $scope.lastName = sessionStorage.getItem("cLastName");
        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "bulkEmail", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    if (response.data.status === 'success') {	
					var resprom = $http.get(Data.serviceBase() + 'organization/emailWishesTemplate', {params: {"organizationId": $scope.organizationId}});
					resprom.then(function (response) {
						if (response.data.length > 0) {
							$scope.emailWishesTemplate.id = response.data[0].id;
							$scope.emailWishesTemplate.name = response.data[0].name;
							$scope.emailWishesTemplate.subject = response.data[0].subject;
							$scope.emailWishesTemplate.body = response.data[0].body;
							$scope.emailWishesTemplate.organizationId =  response.data[0].organizationId;
    						$scope.emailWishesTemplate.userId =  response.data[0].userId;
    						$scope.emailWishesTemplate.dob =  response.data[0].dob;
    						$scope.emailWishesTemplate.sdate =  response.data[0].sdate;
                            if ($scope.emailWishesTemplate.dob === '0') {
                                $scope.toggle.switch = true;
                            } else {
                                 $scope.toggle.switch = false;
                            }
						} else {
							$scope.emailWishesTemplate.name = 'Birthday Wishes';
							$scope.emailWishesTemplate.subject = 'Happy Birthday Wishes from '+$scope.firstName+' '+$scope.lastName+', '+$scope.orgName;
							$scope.emailWishesTemplate.body = '<p>'+'Dear #VisitorName#,'+'</p>'+
							'<p>'+'We at '+$scope.orgName+' wish you a very happy birthday. Many more happy returns of this day and have a wonderful year ahead.' + '</p>' +
							'<p>'+'We hope that you will be graced with the strength to accomplish all that you aspire in your days ahead. Happy birthday!' + '</p>' +
							'<p>'+'Thank you and Best Regards,'+ '</p>' +
                    		'#firstname# #lastname#'+ '<br/>' +
                    		'#designation#'+ '<br/>' +
                    		$scope.orgName+ '<br/>' +
                    		'#email#'+ '<br/>' +'#mobile#'+ '</p>';
							$scope.emailWishesTemplate.organizationId = $scope.organizationId;
    						$scope.emailWishesTemplate.userId = sessionStorage.getItem('userId');
    						$scope.emailWishesTemplate.dob =  '1';
                            $scope.toggle.switch = false;
						}
						$scope.emailloader1 = false;
					}, function (response) {
						console.log('Error happened -- ');
						console.log(response);
					});
		 }else{
                        $scope.orgemtPlanCheck(response.data);
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.getSplWishesOrgTemplate = function () {
         $scope.orgName = sessionStorage.getItem('cOrgName');
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "bulkEmail", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    if (response.data.status === 'success') {	
					var resprom = $http.get(Data.serviceBase() + 'organization/emailSplWishesTemplate', {params: {"organizationId": $scope.organizationId}});
					resprom.then(function (response) {
						if (response.data.length > 0) {
							$scope.emailSplWishesTemplate.id = response.data[0].id;
							$scope.emailSplWishesTemplate.name = response.data[0].name;
							$scope.emailSplWishesTemplate.subject = response.data[0].subject;
							$scope.emailSplWishesTemplate.body = response.data[0].body;
							$scope.emailSplWishesTemplate.organizationId =  response.data[0].organizationId;
    						$scope.emailSplWishesTemplate.userId =  response.data[0].userId;
    						$scope.emailSplWishesTemplate.dob =  response.data[0].dob;
    						$scope.emailSplWishesTemplate.sdate =  response.data[0].sdate;                  
                            if ($scope.emailSplWishesTemplate.sdate === '0') {
                                $scope.toggle.switchs = true;
                            } else {
                                 $scope.toggle.switchs = false;
                            }
						} else {
							$scope.emailSplWishesTemplate.name = 'Special Day Wishes';
							$scope.emailSplWishesTemplate.subject = 'Wish you more happiness and joy!';
							$scope.emailSplWishesTemplate.body = '<p>'+'Dear #VisitorName#,'+'</p>'+
							'<p>'+'On this wonderful day, on behalf of '+$scope.orgName+' We wish you a lot of joy and success.' + '</p>' +
							'<p>'+'Sincerely wishing you the best of celebrations on this wonderful day of yours. May your home be filled with all that you need to be comfortable in life.' + '</p>' +
							'<p>'+'Thank you and Best Regards,'+ '</p>' +
                    		'#firstname# #lastname#'+ '<br/>' +
                    		'#designation#'+ '<br/>' +
                    		 $scope.orgName+ '<br/>' +
                    		'#email#'+ '<br/>' +'#mobile#'+ '</p>';
							$scope.emailSplWishesTemplate.organizationId = $scope.organizationId;
    						$scope.emailSplWishesTemplate.userId = sessionStorage.getItem('userId');
    						$scope.emailSplWishesTemplate.sdate =  '1';
                            $scope.toggle.switchs = false;
						}
						$scope.emailloader1 = false;
					}, function (response) {
						console.log('Error happened -- ');
						console.log(response);
					});
		 }else{
                        $scope.orgemtPlanCheck(response.data);
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
	
	$scope.orgemtPlanCheck = function (p, size) {
        var modalInstance1 = $uibModal.open({
            templateUrl: './checkPremiumPopup',
            controller: 'premiumCheckCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                msg: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };

});

evtApp.controller('smtporgCtrl', function ($scope, warningPopup, $rootScope, Data, $uibModal, $http, toaster) {
    $scope.organizationId = sessionStorage.getItem('orgId');
	$scope.userId = sessionStorage.getItem('userId');
    $scope.csmtp = {
        "mailPort" : 25,
        "mailPort2" : 143,
    };
    $rootScope.title = "SMTP Configuration";
    $rootScope.addButtonhtml = "";
    $scope.dverify = true;
    $scope.tstbtn = "";
    sessionStorage.setItem('menuact', '5');
    $rootScope.menuact = '5';
    $scope.configSMTP = function () {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "bulkEmail", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    console.log(response.data);
                    if (response.data.status == 'success') {
						
						var smtpConfigresp = $http.get(Data.serviceBase() + '/organization/orgSMTP', {params: {"organizationId": $scope.organizationId,"userId":$scope.userId}});
						smtpConfigresp.then(function (response) {
							console.log(response.data);
							$scope.csmtp = response.data[0];
							if($scope.csmtp){
								if (!$scope.csmtp.mailPort) {
									$scope.csmtp.mailPort = 25;
								}
								if (!$scope.csmtp.mailPort2) {
									$scope.csmtp.mailPort2 = 143;
								}
							}else{
				$scope.csmtp={"mailPort":25,"mailPort2":143};
			}

						}, function (response) {
							console.log('Error happened -- ');
							console.log(response);
						});
					} else {
                        console.log(response.data);
                        $scope.orgsmtpPlanCheck(response.data);
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

	
	$scope.orgsmtpPlanCheck = function (p, size) {
        var modalInstance1 = $uibModal.open({
            templateUrl: './checkPremiumPopup',
            controller: 'premiumCheckCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                msg: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };

    $scope.checkDomain = function (csmtp) {
        console.log(csmtp);
        //csmtp.organizationId = $scope.organizationId;
        $scope.tstbtn = '<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>';
        var smtpConfigresp = $http.get(Data.webAppBase() + '/account/checkHost', {params: {"mailHost": csmtp.mailHost,"mailHost2": csmtp.mailHost2,"mailPort": csmtp.mailPort,"mailPort2": csmtp.mailPort2, "mailUser": csmtp.mailUser, "mailPassword": csmtp.mailPassword, "mailSSL": '0'}});
        smtpConfigresp.then(function (result) {
            //console.log(result);
            var resp = result.data;
            if (resp.status === 'success') {
                $scope.dverify = false;
                $scope.tstbtn = "<span style='font-size:18px;'><i class='fa fa-check-circle-o'></i> &nbsp;SMTP configuration verified</span>";

            } else if (resp.status === 'error') {
                $scope.dverify = true;
                $scope.tstbtn = "<span style='font-size:18px;'>Please provide valid SMTP configuration settings</span>";
            }
        });

    };
    $scope.updateSMTP = function (csmtp) {
		$scope.dverify = true;
        csmtp.organizationId = $scope.organizationId;
		csmtp.userId = $scope.userId;
        Data.post('organization/updateSMTPConfig', csmtp).then(function (result) {
            if (result.status !== 'error' && result.status !== 'success1') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
				
            } else {
                console.log(result);
            }
        });


    };
	 $scope.etypetxt = 'password';
    $scope.viewemailpass = function () {
        if ($scope.etypetxt === 'password') {
            $scope.etypetxt = 'text';
        } else {
            $scope.etypetxt = 'password';
        }
    };

});
evtApp.controller('trailTestCtrl', function ($scope, warningPopup, $rootScope, Data,alertUser,trialPopUp, $uibModal, $http, toaster) {
    $rootScope.title = 'Lead Stage Customization';
    $scope.loaderView = false;
    $rootScope.addButtonhtml = "";
    sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';   
    $scope.likeClick = function(){
        toaster.pop("success", "", "Thanks for your request and appreciation, we will keep you posted on the update", 8000, 'trustedHtml');
    };
    $scope.redirectToS = function(){
        window.open("https://www.helloleads.io/support.php","_blank");
    };
});
evtApp.controller('leadStageCtrl', function ($scope, $rootScope, alertUser, warningPopup, $uibModal, trialPopUp, $location, $window, Data, $http, toaster) {
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';
	$scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }
	$scope.updateFlag = '0';
    $scope.leadresStage = '';
    $scope.leadStages = [];
    $scope.leadStore = [];
    $rootScope.title = "Lead Stage Customization";
    //$rootScope.addButtonhtml = '<button class="btn btn-default btn-sm btn-circle" popover="Your organization and contact details help us to serve you better. We will use this information for setting up your account and for billing purposes" popover-trigger="focus" popover-title="My Account" popover-placement="bottom"><i class="fa fa-question"></i></button>';

    $rootScope.addButtonhtml = '';

    $scope.getAllLeadStages = function() {    // changes made by Kishore on 09-Dec-2021    
    var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});
    // $scope.users = {};
    resprole.then(function (response) {
        console.log(response);
        $scope.loader = false;
        if (response.data.status === 'success') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "changeHistory"}});
            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    $scope.loaderView = true;
                    var resprom = $http.get(Data.serviceBase() + 'organization/leadStages', {params: {"organizationId": $scope.organizationId}});
                    resprom.then(function (response) {
                        $scope.leadStages = response.data;
                        $scope.copyLeadStage = angular.copy($scope.leadStages);
                        $scope.loaderView = false;
						$scope.updateFlag = '1';
                        $scope.btnShow = '1';  // changes made by Kishore on 09-Dec-2021
                        $scope.stage = {};  // changes made by Kishore on 09-Dec-2021
                        $scope.stage.newStage = "";  // changes made by Kishore on 09-Dec-2021
                        $scope.stage.newCatagory = "";  // changes made by Kishore on 09-Dec-2021
                        $scope.stage.newStatus = "";  // changes made by Kishore on 09-Dec-2021
                        $scope.planType="";  // changes made by Kishore on 09-Dec-2021
                    }, function (response) {
                        console.log('Error happened -- ');
                        console.log(response);
                    });
                } else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to customize the lead stages. Only account owner or managers can customize the lead stages. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });
        } else {
            trialPopUp.trialPopmodel(response.data);
        }
    }, function (response) {

        console.log('Error happened -- ');
        console.log(response);
    });

};  // changes made by Kishore on 09-Dec-2021

    $scope.checkStage = function () {
        var chkflag = '0';
        console.log("CheckStage count : " + $scope.leadStages.length);  // changes made by Kishore on 09-Dec-2021
        for (var t = 0; t < $scope.leadStages.length; t++) {  // changes made by Kishore on 09-Dec-2021
            if ($scope.leadStages[t]['stage'] === '') {
                console.log("Inside");
                chkflag = '1';
            }
        }
        if (chkflag === '0') {
            return false;
        } else {
            return true;
        }
    };
    $scope.toggleAction = function (index) {
        if ($scope.leadStages[index].status === '0') {
            $scope.leadslist = [];
            $scope.leadCount = 0;
            $scope.leadStages[index].status = '1';
            var resprom = $http.get(Data.serviceBase() + 'visitor/checkStage', {params: {"organizationId": $scope.organizationId, "stage": $scope.leadStages[index].stage, "stageId": $scope.leadStages[index].id}});
            resprom.then(function (response) {
                console.log(response.data);
                $scope.leadslist = response.data.visitorlist;
                $scope.leadCount = response.data.leadCount;
                if ($scope.leadCount > 0) {
                    var modalInstance = $uibModal.open({
                        templateUrl: './stagewarningPopup',
                        controller: 'stagewarningPopupCtrl',
                        size: 'md',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            leadCount: function () {
                                return $scope.leadCount;
                            },
                            stages: function () {
                                return $scope.leadStages;
                            },
                            selst: function () {
                                return $scope.leadStages[index].stage;
                            },
                        }
                    });

                    modalInstance.result.then(function (i) {
                        if (i !== '0') {
                            Data.put('organization/stageUpdate', {'organizationId': $scope.organizationId, 'stageId': $scope.leadStages[index].id}).then(function (respor) {
                                if (respor.status === 'success') {
									$scope.leadStages[index].status = '1';
                                    //toaster.pop(respor.status, "", respor.message, 6000, 'trustedHtml');
                                    // $scope.message = {"title": "Facebook Page subscribed", "message": "<h4>Your Facebook page <b>"+page.name+"</b> has been linked to your HelloLeads Account. A new list <b>Fb_Leads_"+page.name+" </b>has been created to capture leads from "+page.name+" Lead ads</h4>"};
                                    // alertUser.alertpopmodel($scope.message);
                                    console.log(i.id);
                                    var modalInstance = $uibModal.open({
                                        templateUrl: './stageCustomLoad',
                                        controller: 'stageCustomMultiCtrl',
                                        size: 'md',
                                        backdrop: 'static',
                                        keyboard: false,
                                        resolve: {
                                            leadlist: function () {

                                                return $scope.leadslist;
                                            },
                                            value: function () {
                                                return  i.id;
                                            }
                                        }
                                    });
                                    modalInstance.result.then(function (i) {
                                        if (i) {
											//$scope.message = {"title": "Info", "message": "<span style='font-size:18px;color:black;'>Changes made in Lead stage status had been updated successfully.<br><br>Note: In HelloLeads mobile application for these changes to get reflected, Please make sure that you (and your team members) are in its latest versions.<br><br>HelloLeads Android    - V 1.7.33 and above<br><br>HelloLeads iOS        - V 1.10.7 and above</span>"};
											//$scope.message = {"title": "Info", "message": "<span style='font-size:18px;color:black;'>Changes made in Lead stage status had been updated successfully.</span>"};
											//alertUser.alertpopmodel($scope.message);

                                            toaster.pop("success", "", "Changes made in Lead stage status had been updated successfully", 10000, 'trustedHtml');  // changes made by Kishore on 09-Dec-2021
                                            /*$scope.loaderView = true;
                                            var resprom = $http.get(Data.serviceBase() + 'organization/leadStages', {params: {"organizationId": $scope.organizationId}});
                                            resprom.then(function (response) {
                                                $scope.leadStages = response.data;
                                                $scope.copyLeadStage = angular.copy($scope.leadStages);
                                                $scope.loaderView = false;
                                            }, function (response) {
                                                console.log('Error happened -- ');
                                                console.log(response);
                                            });*/
                                        }
                                    });

                                } else {
                                    toaster.pop(respor.status, "", respor.message, 10000, 'trustedHtml');
                                }

                            });

                        } else {
                            $scope.leadStages[index].status = '0';
                        }
                    });
                } else {

                    Data.put('organization/stageUpdate', {'organizationId': $scope.organizationId, 'stageId': $scope.leadStages[index].id}).then(function (respor) {
                        if (respor.status === 'success') {
                            //toaster.pop(respor.status, "", respor.message, 10000, 'trustedHtml');
                            //$scope.loaderView = true;
                            //$scope.message = {"title": "Info", "message": "<span style='font-size:18px;color:black;'>Changes made in Lead stage status had been updated successfully.<br><br>Note: In HelloLeads mobile application for these changes to get reflected, Please make sure that you (and your team members) are in its latest versions.<br><br>HelloLeads Android    - V 1.7.33 and above<br><br>HelloLeads iOS        - V 1.10.7 and above</span>"};
                            //$scope.message = {"title": "Info", "message": "<span style='font-size:18px;color:black;'>Changes made in Lead stage status had been updated successfully.</span>"};
                            //alertUser.alertpopmodel($scope.message);

                            toaster.pop("success", "", "Changes made in lead stage had been updated successfully", 10000, 'trustedHtml');  // changes made by Kishore on 09-Dec-2021

                            /* var resprom = $http.get(Data.serviceBase() + 'organization/leadStages', {params: {"organizationId": $scope.organizationId}});
                             resprom.then(function (response) {
                             $scope.leadStages = response.data;
                             $scope.copyLeadStage = angular.copy($scope.leadStages);
                             $scope.loaderView = false;
                             }, function (response) {
                             console.log('Error happened -- ');
                             console.log(response);
                             });*/
                            $scope.leadStages[index].status = '1';

                        } else {
                            toaster.pop(respor.status, "", respor.message, 10000, 'trustedHtml');
                        }

                    });
                }
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });

        } else {
            $scope.leadStages[index].status = '0';
            Data.put('organization/stageUpdate', {'organizationId': $scope.organizationId, 'stageId': $scope.leadStages[index].id}).then(function (respor) {
                if (respor.status === 'success') {
                    //$scope.message = {"title": "Info", "message": "<span style='font-size:18px;color:black;'>Changes made in Lead stage status had been updated successfully.<br><br>Note: In HelloLeads mobile application for these changes to get reflected, Please make sure that you (and your team members) are in its latest versions.<br><br>HelloLeads Android    - V 1.7.33 and above<br><br>HelloLeads iOS        - V 1.10.7 and above</span>"};
                    //$scope.message = {"title": "Info", "message": "<span style='font-size:18px;color:black;'>Changes made in Lead stage status had been updated successfully.</span>"};
                    //alertUser.alertpopmodel($scope.message);

                    toaster.pop("success", "", "Changes made in lead stage had been updated successfully", 10000, 'trustedHtml');  // changes made by Kishore on 09-Dec-2021

                    // toaster.pop(respor.status, "", respor.message, 10000, 'trustedHtml');
                    /*$scope.loaderView = true;
                     var resprom = $http.get(Data.serviceBase() + 'organization/leadStages', {params: {"organizationId": $scope.organizationId}});
                     resprom.then(function (response) {
                     $scope.leadStages = response.data;
                     $scope.copyLeadStage = angular.copy($scope.leadStages);
                     $scope.loaderView = false;
                     }, function (response) {
                     console.log('Error happened -- ');
                     console.log(response);
                     });*/
                } else {
                    toaster.pop(respor.status, "", respor.message, 10000, 'trustedHtml');
                }

            });
        }
    };

    $scope.planVerify = function(){  // function added by Kishore on 09-Dec-2021

        $scope.planType = sessionStorage.getItem("accountType");
        $scope.trialValidate = sessionStorage.getItem("trialValidate");
        console.log("plan info : "+ $scope.trialValidate);

        //$scope.boxShow = false;
        $scope.btnShow = '0';

        if($scope.planType == 'FR' || $scope.planType == 'P2' || $scope.planType == 'P3' || $scope.trialValidate == '1'){

            $scope.message = {"title": "Upgrade your plan to create additional lead stages", "message": "<span style='font-size:18px;color:black;'>Adding additional / custom lead stages is available in L, XL, and XXL plans. Please <a style='text-decoration:underline;' href='https://beta.helloleads.io/index.php/app/account/pricing'><b>upgrade your plan</b></a> to leverage this feature.</span>"};
            alertUser.alertpopmodel($scope.message);
            $scope.btnShow = '1';
            //$scope.boxShow = false;
            console.log("Org Info1 : "+$scope.planType);
            }

    };
    
    $scope.btnShow = '1';
    $scope.stage = {};
    $scope.stage.newStage = "";
    $scope.stage.newCatagory = "";
    $scope.stage.newStatus = "";
    $scope.addNewLeadStage = function(stage){  // function added by Kishore on 09-Dec-2021
        console.log("Before : addNewLeadStages");
                if (stage.newStage !== undefined) {                    
                    stage.orgId = sessionStorage.getItem('orgId');
                    
                Data.post('organization/addNewLeadStages', stage).then(function (response) {
                    if (response.status !== 'error') {
                        toaster.pop("success", "", "Changes made in lead stage had been updated successfully", 10000, 'trustedHtml');
                        //toaster.pop("info", "Info", "Toaster 1", 10000, 'trustedHtml');
                        console.log("Adding New Lead Stage Works!");
                    } else {
                        console.log(response);
                        //toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                        //toaster.pop("info", "Info", "Toaster 2", 10000, 'trustedHtml');
                    }
                    });
                }
                $scope.getAllLeadStages();
            
            console.log("Org Info2 : ", $scope.planType);
    };

    $scope.username = sessionStorage.getItem('firstName');
    $scope.updateLeadConfig = function (leadsStage, delStage) {
        var upflag = 0;

        $scope.diffArray = [];
        $scope.diffmsg = "";
        var sresult = [];
        var resprom = $http.get(Data.serviceBase() + 'visitor/stageCount', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            console.log("count of lead stage : " + leadsStage.length);  // changes made by Kishore on 09-Dec-2021
            $scope.n = leadsStage.length;  // changes made by Kishore on 09-Dec-2021
            sresult = response.data;  // changes made by Kishore on 09-Dec-2021
            for (var ind = 0; ind < $scope.n; ind++) {  // changes made by Kishore on 09-Dec-2021
                if (leadsStage[ind]['stage'] !== $scope.copyLeadStage[ind]['stage']) {
                    var temp = {};
                    var scount = 0;

                    angular.forEach(sresult, function (d) {
                        if (d.stageId == $scope.copyLeadStage[ind]['id']) {
                            //console.log(d);
                            scount = d.Vcount;
                            temp['count'] = scount;
                        }

                    });
                    console.log(temp['count']);
                    if (temp['count'] === undefined) {
                        temp['count'] = '0';
						$scope.diffmsg = $scope.diffmsg + " Lead stage ''" +$scope.copyLeadStage[ind]['stage'] + "'' will be changed to ''"+leadsStage[ind]['stage']+ "''<br/><br/>";
                    }else{
						$scope.diffmsg = $scope.diffmsg + temp['count'] + " Lead(s) will be changed from ''" + $scope.copyLeadStage[ind]['stage'] + "'' to ''" + leadsStage[ind]['stage'] + "''<br/><br/>";
					}

                    

                    //temp['"' + $scope.copyLeadStage[ind] + '"'] = leadsStage[ind];
                    $scope.diffArray.push(temp);

                }
            }
            if ($scope.diffmsg !== '') {
                console.log($scope.diffArray);
                var msg = {"message": "Hai " + $scope.username + ", <br><br> You are trying to change the Lead Stage label and this will impact Lead Stages, Charts, Filters and Reports. <br/><br/>" + $scope.diffmsg, "title": "Confirmation", "canceltxt": "Cancel", "oktxt": "Proceed"};
                warningPopup.warningpopmodel(msg);
                $scope.$on('warnresp', function (event, args) {
                    if (args.status === 'true' && upflag === 0) {
                        console.log(leadsStage);
                        Data.post('organization/leadStages', {"leadStage": JSON.stringify(leadsStage), "organizationId": $scope.organizationId}).then(function (response) {
                            if (response.status === 'success') {
                                //toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
								//$scope.message = {"title": "Info", "message": "<span style='font-size:18px;color:black;'>Changes made in Lead stage had been updated successfully.<br><br>Note: In HelloLeads mobile application for these changes to get reflected, Please make sure that you (and your team members) are in its latest versions.<br><br>HelloLeads Android    - V 1.7.33 and above<br><br>HelloLeads iOS        - V 1.10.7 and above</span>"};
                                //$scope.message = {"title": "Info", "message": "<span style='font-size:18px;color:black;'>Changes made in Lead stage had been updated successfully.</span>"};
                                //alertUser.alertpopmodel($scope.message);
                                toaster.pop("success", "", "Changes made in lead stage had been updated successfully", 10000, 'trustedHtml');  // changes made by Kishore on 09-Dec-2021
                                window.location.href = Data.webAppBase() + 'account/leadStage';  // changes made by Kishore on 09-Dec-2021
								$scope.loaderView = true;
                                var resprom = $http.get(Data.serviceBase() + 'organization/leadStages', {params: {"organizationId": $scope.organizationId}});
                                resprom.then(function (response) {
                                    $scope.leadStages = response.data;
                                    $scope.copyLeadStage = angular.copy($scope.leadStages);
                                    $scope.loaderView = false;
                                    $scope.updateFlag = '1';
                                }, function (response) {
                                    console.log('Error happened -- ');
                                    console.log(response);
                                });

                                $scope.btnShow = '1';  // changes made by Kishore on 09-Dec-2021
                                $scope.stage = {};
                                $scope.stage.newStage = "";
                                $scope.stage.newCatagory = "";
                                $scope.stage.newStatus = "";
                                console.log('Stage value : ' + $scope.stage);
                                if (stage.newStage !== undefined) {                    
                                    stage.orgId = sessionStorage.getItem('orgId');
                                    
                                Data.post('organization/addNewLeadStages', $scope.stage).then(function (response) {
                                    if (response.status !== 'error') {
                                        toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                                        console.log("Adding New Lead Stage Works!");
                                    } else {
                                        console.log(response);
                                        //toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                                    }
                                    });
                                }  // changes made by Kishore on 09-Dec-2021


                                // $scope.message = {"title": "Facebook Page subscribed", "message": "<h4>Your Facebook page <b>"+page.name+"</b> has been linked to your HelloLeads Account. A new list <b>Fb_Leads_"+page.name+" </b>has been created to capture leads from "+page.name+" Lead ads</h4>"};
                                // alertUser.alertpopmodel($scope.message);
                                upflag = 1;
                            } else {
                                toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
                                $scope.getAllLeadStages();  // changes made by Kishore on 09-Dec-2021
                            }
                        });
                    } else {
                        upflag = 1;
                    }
                });
            } else {
                if($scope.stage.length > 0){
                    toaster.pop("info", "Info", "No changes has been made", 10000, 'trustedHtml');
                }
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.resetDefaultStage = function () {
        $scope.leadStageDet = [];
        $scope.defaultStage = '';
        $scope.wonStage = '';
        $scope.lostStage = '';
        $scope.delStage = [];
        $scope.leadStages = ['Open', 'Contacted', 'Nurturing', 'Qualified', 'Negotiation', 'Customer', 'Inactive Customer', 'Unqualified'];
        $scope.defaultStage = 'Open';
        $scope.wonStage = 'Customer';
        $scope.lostStage = 'Unqualified';
        $scope.delStage = ['Open', 'Contacted', 'Nurturing', 'Qualified', 'Negotiation', 'Customer', 'Inactive Customer', 'Unqualified'];
        $scope.leadStageDet[0] = $scope.defaultStage;
        $scope.leadStageDet[5] = $scope.wonStage;
        $scope.leadStageDet[7] = $scope.lostStage;
        var index = $scope.leadStages.indexOf($scope.defaultStage);
        $scope.leadStages.splice(index, 1);
        //$scope.delStage.splice(index, 1);
        var index1 = $scope.leadStages.indexOf($scope.wonStage);
        $scope.leadStages.splice(index1, 1);
        //$scope.delStage.splice(index1, 1);
        var index2 = $scope.leadStages.indexOf($scope.lostStage);
        $scope.leadStages.splice(index2, 1);
        //$scope.delStage.splice(index2, 1);
        var j = 0;
        for (var i = 0; i < 8; i++) {
            if (i !== 0 && i !== 5 && i !== 7) {
                $scope.leadStageDet[i] = $scope.leadStages[j];
                j++;
            }
        }
        // console.log($scope.leadStageDet);
    };
});

evtApp.controller('wisheswarningPopupCtrl', function ($scope, Data, $http, $uibModalInstance, $uibModal,dob) {
    //console.log($scope.selStage);
    $scope.dob = dob;
    $scope.okbtn = function () {
         $uibModalInstance.close($scope.dob);
    };
    $scope.cancelbtn = function () {
        $uibModalInstance.close('0');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});

evtApp.controller('specialwisheswarningPopupCtrl', function ($scope, Data, $http, $uibModalInstance, $uibModal,sdate) {
    //console.log($scope.selStage);
    $scope.sdate = sdate;
    $scope.okbtn = function () {
         $uibModalInstance.close($scope.sdate);
    };
    $scope.cancelbtn = function () {
        $uibModalInstance.close('0');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});

evtApp.controller('stagewarningPopupCtrl', function ($scope, Data, $http, $uibModalInstance, $uibModal, leadCount, stages, selst) {

    $scope.leadCount = leadCount;
    $scope.stages = [];
    $scope.username = sessionStorage.getItem('firstName');
    angular.forEach(stages, function (d) {
        if (d.status === '0') {
            $scope.stages.push(d);
        }
    })
    // $scope.stages = stages;
    $scope.selst = selst;

    $scope.selStage = '';
    //console.log($scope.selStage);
    $scope.okbtn = function () {
        $uibModalInstance.close($scope.selStage);
    };
    $scope.cancelbtn = function () {
        $uibModalInstance.close('0');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});
evtApp.controller('stageCustomMultiCtrl', function ($scope, Data, $uibModalInstance, $http, leadlist, value, toaster) {
    $scope.organizationId = sessionStorage.getItem('orgId');

    $scope.loader = true;
    Data.put('visitor/multiupdate', {'visitorlist': leadlist, 'key': "type", 'value': value}).then(function (response) {
        if (response.status === 'success') {
            toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');

            $scope.loader = false;
            $uibModalInstance.close('1');

        } else {
            toaster.pop(response.status, "", response.message, 10000, 'trustedHtml');
        }

    });


});