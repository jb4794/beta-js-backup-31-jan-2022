evtApp.controller('mainCtrl', function ($scope, $rootScope, Data, toaster, $http, $location, $window) {

    $scope.showReg = false;
    if ($location.absUrl().indexOf("validate") > 0) {
        console.log("Registered");
        $scope.showReg = true;

    }

    /* 
     var sessionStorage_transfer = function (event) {
     
     if (!event) {
     event = window.event;
     } // ie suq
     if (!event.newValue)
     return;          // do nothing if no value to work with
     if (event.key == 'getSessionStorage') {
     
     // another tab asked for the sessionStorage -> send it
     localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
     // the other tab should now have it, so we're done with it.
     //localStorage.removeItem('sessionStorage'); // <- could do short timeout as well.
     } else if (event.key == 'sessionStorage' && !sessionStorage.length) {
     // another tab sent data <- get it
     var data = JSON.parse(event.newValue);
     for (var key in data) {
     sessionStorage.setItem(key, data[key]);
     }
     }
     };
     
     // listen for changes to localStorage
     if (window.addEventListener) {
     //console.log("Event trigger");
     window.addEventListener("storage", sessionStorage_transfer,false);
     } else {
     window.attachEvent("onstorage", sessionStorage_transfer);
     }
     ;
     */
    console.log("Sharing");
    if (sessionStorage.getItem('token') && localStorage.getItem('sessionStorage')) {
        var data = JSON.parse(localStorage.getItem('sessionStorage'));
        for (var key in data) {
            sessionStorage.setItem(key, data[key]);
        }

        if ($location.absUrl().indexOf('nextURL') > 0) {
            $scope.nextUrl = $location.absUrl().split("?=")[1];
            window.location.href = Data.webAppBase() + 'account/' + $scope.nextUrl;
        }
    } else if (!localStorage.length) {
        localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
    }

    $scope.pricingMenu = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('payment/setPricingMenu', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.reportMenu = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('user/setReportMenu', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.upgradeFloat = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('payment/setUpgradeFloat', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };
    
    $scope.redirectPricePage = function () {   
     $window.location.href = Data.webAppBase() + "account/pricing";
    };

    $scope.orgCompanyProfile = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('user/setCompanyProfile', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.qualifiersCount = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('user/setQualifiersCount', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.leadImExp = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('user/setLeadImExp', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.formInteg = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('user/setFormInteg', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.stageCust = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('user/setStageCust', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.apiInteg = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('user/setApiInteg', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    //Get Support form data
     $scope.getUserInfo = function () {
            var supportinfo = $http.get(Data.serviceBase() + 'account/supportdata');
                supportinfo.then(function (response) {
                    angular.forEach(response.data, function (value) {
                       $scope.userName =  value.firstName;
                       $scope.userEmail = value.email;
                       $scope.userMobile = value.mobile;
                       $scope.userOrganizationName = value.organizationName;
                });
            }, function (response) {
                console.log('Error happened -- ');
                console.log("Get User Info error "+response);
            });
            var oresprom = $http.get(Data.serviceBase() + 'organization/organization', {params: {"key": "id", "value": $scope.organizationId}});
            oresprom.then(function (response) {
                //console.log(response);
                $scope.orgexpireDet = response.data[0].trialAccount;
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });
    };
    //End Support form data

});

evtApp.controller('helpCenterCtrl', function ($scope, Data, $http, $sce) {
    // --------- Help Center ------------

    
    
    // sessionStorage.getItem('token');
    // localStorage.getItem('sessionStorage');
    $scope.newprodupdates = {};
    $scope.newvideoupdates = {};
    $scope.getAllNewProductUpdates = function(){
        var resproups = $http.get(Data.serviceBase() + 'user/allProductUpdates', {params: {}});
        resproups.then(function(response){
            // console.log("Product update "+response.data);
            $scope.newprodupdates = response.data;
            $scope.procount = response.data.length;
            $scope.current_time = new Date();
            // angular.forEach(response.data, function (value) {
            //     $scope.newprodupdates = value.newprodupdates;
            //     console.log(value);
            //     $scope.count = value.count;
            //  });

            //  angular.forEach($scope.count, function(result){
            //     $scope.procount = result.count;
            // });

            
        }, function(response){
            console.log('Error');
            console.log(response);
        });
    };

    //$scope.vidcount = 0;
    $scope.getAllNewVideoUpdates = function(){
        var resproups = $http.get(Data.serviceBase() + 'user/allVideoUpdates', {params: {}});        
        resproups.then(function(response){
            
            $scope.newvideoupdates = response.data;
            $scope.vidcount = response.data.length;
            $scope.trustSrc = function(src) {
                return $sce.trustAsResourceUrl(src);
              }
        }, function(response){
            console.log('Error');
            console.log(response);
            });
    };

    // --------- Help Center ------------

    // --------- UBT : Help Center ------------

    $scope.helpIcon = function () {   
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
            Data.post('user/helpIconViewCount', $scope.email).then(function (result) {
            console.log('Email '+$scope.cEmail);
            });
        };
    
        $scope.kbIcon = function () {   
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
            Data.post('user/kbIconViewCount', $scope.email).then(function (result) {
            console.log('Email '+$scope.cEmail);
            });
        };
    
        $scope.videoIcon = function () {   
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
            Data.post('user/videoIconViewCount', $scope.email).then(function (result) {
            console.log('Email '+$scope.cEmail);
            });
        };
    
        $scope.pUpdateIcon = function () {   
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
            Data.post('user/pUpdateIconViewCount', $scope.email).then(function (result) {
            console.log('Email '+$scope.cEmail);
            });
        };
    
        $scope.tawkToIcon = function () {   
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
            Data.post('user/tawkToIconViewCount', $scope.email).then(function (result) {
            console.log('Email '+$scope.cEmail);
            });
        };
    
        // --------- UBT : Help Center ------------

});

evtApp.controller('verifyuserCtrl', function ($scope, Data, $location) {

    if ($location.absUrl().indexOf('emailverify') > 0) {
        console.log("Inside email verify");
        $scope.nextUrl = "";
        if (sessionStorage.getItem('token') || localStorage.getItem('sessionStorage')) {
            console.log("Inside login");
            var data = JSON.parse(localStorage.getItem('sessionStorage'));
            for (var key in data) {
                sessionStorage.setItem(key, data[key]);
            }

            if ($location.absUrl().indexOf('nextURL') > 0) {
                $scope.nextUrl = $location.absUrl().split("?=")[1];
                window.location.href = Data.webAppBase() + 'account' + $scope.nextUrl;
            }

        } else if (!localStorage.getItem('sessionStorage') && !sessionStorage.getItem('token')) {
            console.log("Outside login");
            //localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
            if ($location.absUrl().indexOf('nextURL') > 0) {
                $scope.nextUrl = $location.absUrl().split("?=")[1];
                console.log($scope.nextUrl);
                //localStorage.setItem("nextUrl", $scope.nextUrl);
                window.location.href = Data.webAppBase() + 'account/login?nextURL?=' + $scope.nextUrl;
            }
        }
        ;
    }


});


evtApp.controller('userCtrl', function ($scope, $location, $rootScope,trialPopUp,$uibModal, alertUser, Data, toaster, $http, $location, $window) {

    $scope.users = {};
    $rootScope.title = "User Profile";
    $rootScope.addButtonhtml = '<button class="btn btn-default btn-sm btn-circle" popover="Update your profile information to make your Digital Business card being shared with your leads as 100% completed." popover-title="User Profile" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    $scope.user = {};

	var myElement = angular.element( '#gintanceId' ) ;

    $rootScope.visitorSearch = {};
    /*if(sessionStorage.getItem("xtime")==="1"){
     $rootScope.firstProfile=true;
     }
     else{
     $rootScope.firstProfile=false;
     }*/
	 $rootScope.typetxt = 'password';

     $scope.renewBtn = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('payment/setRenewBtn', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
     //$window.location.href = Data.webAppBase() + "app/account/pricing";
    };
    
    $scope.buyCredit = function () {   
         var userId = sessionStorage.getItem("userId");
            $scope.roleId = sessionStorage.getItem('roleId');
            $scope.rolemsg = "";
            if ($scope.roleId === '2') {
                $scope.rolemsg = "Manager";
            } else if ($scope.roleId === '3') {
                $scope.rolemsg = "L1 User";
            } else {
                $scope.rolemsg = "L2 User";
            }
            var organizationId = sessionStorage.getItem("orgId");
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "payment"}});
                resprole.then(function (response) {
                    console.log(response);
                    $scope.loader = false;
                    if (response.data.status === 'success') {
                        $window.location.href = Data.webAppBase() + "account/pricing";
                    } else {
                       $scope.message = {"title":"Authorization failure","message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to make payment. Only account owner can make payment. Please contact HelloLeads account owner in your company for assistance with this task"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
    };


     
    $rootScope.viewpass = function(){
        if($scope.typetxt === 'password'){
            $scope.typetxt = 'text';
        }else{
            $scope.typetxt = 'password';
        }
    };
    if ($location.absUrl().indexOf("?") > 0 && $location.absUrl().indexOf("email") > 0 && $location.absUrl().indexOf("login") > 0) {
        var param = $location.absUrl().split("=");
        $scope.user.username=param[1].split("&")[0];
        $scope.user.password=param[2];
    }
	if ($location.absUrl().indexOf("?") > 0 && $location.absUrl().indexOf("authKey") > 0 && $location.absUrl().indexOf("login") > 0) {
        var param = $location.absUrl().split("=");
        console.log(param);
        if (param[1] === 'abd12ezfg562hikl') {
            $scope.user.username ='nrn@helloleads.io';
            $scope.user.password = 'helloinfy';
        }

        //$scope.login($scope.user);
        // console.log($scope.user);
    }
    if (($location.absUrl().indexOf("login") > 0 || $location.absUrl().indexOf("leadswalletlogin") > 0) && $location.absUrl().indexOf("nextURL") <= 0) {
        sessionStorage.removeItem("userId");
        sessionStorage.removeItem("userId");
        sessionStorage.removeItem("firstName");
        sessionStorage.removeItem("lastName");
        sessionStorage.removeItem("orgId");
        sessionStorage.removeItem("token");
        sessionStorage.removeItem("curEventId");
        sessionStorage.removeItem("curEventName");
        sessionStorage.removeItem("curEventsend");
        sessionStorage.removeItem("visitors");
        localStorage.removeItem("curEventsend");
        localStorage.removeItem("curEventId");
        localStorage.removeItem("curEventName");
        localStorage.removeItem("credit");
		localStorage.removeItem("curEventshare");
        sessionStorage.removeItem("userEmail");
        sessionStorage.removeItem("userLevel");
        sessionStorage.removeItem("userId");
        sessionStorage.removeItem("firstName");
        sessionStorage.removeItem("lastName");
        sessionStorage.removeItem("token");
        sessionStorage.removeItem("userEmail");
        sessionStorage.removeItem("cFirstName");
        sessionStorage.removeItem("cLastName");
        sessionStorage.removeItem("userType");
        sessionStorage.removeItem("menuact");
		sessionStorage.removeItem("wcount");
        sessionStorage.removeItem("chkcount");
        sessionStorage.removeItem("lcount");
        sessionStorage.removeItem("selpage");
        sessionStorage.removeItem("cOrgName");
        sessionStorage.removeItem("cUsers");
        sessionStorage.removeItem("category");
        sessionStorage.removeItem("filterlabel");
        sessionStorage.removeItem("service");
        sessionStorage.removeItem("xtime");
        localStorage.removeItem('sessionStorage');
        localStorage.removeItem('visitors');
        localStorage.removeItem("filters");
        localStorage.removeItem("visitorDet");
    }
    $scope.tab = 1;
    $scope.error = false;
    $scope.warning = false;
	angular.element(document.getElementById("home")).addClass("active");
    angular.element(document.getElementById("profile")).removeClass("active");
    angular.element(document.getElementById("businessCardImg")).removeClass("active");
    angular.element(document.getElementById("emailPref")).removeClass("active");
    $scope.switchTab = function (tabId) {
        console.log(tabId);
        if (tabId == 1) {

            $scope.tab = 1;
            angular.element(document.getElementById("home")).addClass("active");
            angular.element(document.getElementById("profile")).removeClass("active");
            angular.element(document.getElementById("businessCardImg")).removeClass("active");
            angular.element(document.getElementById("emailPref")).removeClass("active");
        } else if (tabId == 2) {
            $scope.tab = 2;
            angular.element(document.getElementById("profile")).addClass("active");
            angular.element(document.getElementById("businessCardImg")).removeClass("active");
            angular.element(document.getElementById("home")).removeClass("active");
            angular.element(document.getElementById("emailPref")).removeClass("active");
        } else if (tabId === 3) {
            $scope.tab = 3;
            angular.element(document.getElementById("businessCardImg")).addClass("active");
            angular.element(document.getElementById("profile")).removeClass("active");
            angular.element(document.getElementById("home")).removeClass("active");
            angular.element(document.getElementById("emailPref")).removeClass("active");
        }else{
            $scope.tab = 4;
            angular.element(document.getElementById("businessCardImg")).removeClass("active");
            angular.element(document.getElementById("profile")).removeClass("active");
            angular.element(document.getElementById("home")).removeClass("active");
            angular.element(document.getElementById("emailPref")).addClass("active");
        }


    };

    $scope.lwlogin = function (user) {

        if (user["username"] !== '') {

            Data.post('account/lwlogin', user).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.firstName + "Logged in Scuccesfully", 10000, 'trustedHtml');
                    sessionStorage.setItem("userId", result.id);
                    sessionStorage.setItem("firstName", result.firstName);
                    sessionStorage.setItem("lastName", result.lastName);
                    sessionStorage.setItem("token", result.token);
                    sessionStorage.setItem("userType", result.userType);
                    if (result.userType === 'supervisor') {
                        window.location.href = Data.webAppBase() + 'account/leadwalletsingle';
                    } else {
                        window.location.href = Data.webAppBase() + 'account/leadwalletvisitor';
                    }


                } else {
                    $scope.error = true;
                    console.log('logged in sucessfully');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    console.log(result);
                }
            });
        }
    };

    $rootScope.lwlogout = function () {
        console.log('About to add a logout user......');
        Data.get('account/lwlogout').then(function (results) {
            Data.toast(results);
            sessionStorage.removeItem("userId", '');
            sessionStorage.removeItem("firstName", '');
            sessionStorage.removeItem("lastName", '');
            sessionStorage.removeItem("token", '');
            sessionStorage.removeItem("userEmail", "");
            sessionStorage.removeItem("cFirstName", '');
            sessionStorage.removeItem("cLastName", '');
            sessionStorage.removeItem("userType", '');
            sessionStorage.removeItem("menuact", '');
            if ($location.absUrl().indexOf('leadswalletorg') > 0) {

                window.location.href = Data.webAppBase() + 'account/leadswalletlogin';

            } else {
                window.location.href = Data.webAppBase() + 'account/lwlogin';
            }
        });
    };

	 $scope.changetext = function(){
        $scope.user.fcmtoken = angular.element('#gintanceId' ).val();
    }

    $scope.login = function (user) {
    	
    	
        if (user["username"] !== '') {
            console.log('Username is ' + user["username"]);
			 var timezone_offset_minutes = new Date().getTimezoneOffset();
            timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;
            user["timeZone"] = timezone_offset_minutes;

            Data.post('account/login', user).then(function (result) {
				console.log(result.status);
                if (result.status !== 'error' && result.status !== 'warning' && result.status !== 'basic') {
					$scope.errormsg = "";
                    $scope.error = false;
					console.log(result);
                    console.log('login success.....');
                    sessionStorage.setItem("userId", result.id);
                    sessionStorage.setItem("firstName", result.firstName);
                    sessionStorage.setItem("lastName", result.lastName);
                    sessionStorage.setItem("orgId", result.organizationId);
                    sessionStorage.setItem("token", result.token);
                    sessionStorage.setItem("roleId", result.roleId);
                    sessionStorage.setItem("userType", result.userType);
                    sessionStorage.setItem("accountType", result.accountType);
                    sessionStorage.setItem("userLevel", result.access_level);
					sessionStorage.setItem("PEDate", result.createdAt);
                    sessionStorage.setItem("userEmail", result.email);
					sessionStorage.setItem("userPhone", result.phone);
					sessionStorage.setItem("lcount", result.loginTime);
					sessionStorage.setItem("orgCurrency", result.orgCurrency);
					sessionStorage.setItem("orgCountry", result.orgCountry);
                    sessionStorage.setItem("countryCode", result.contactPhoneCode);
                    localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
                    //console.log("Visitor Data");
                    //console.log(localStorage.getItem("visitorDet"));
					
                    if ($location.absUrl().indexOf('leadswalletlogin') > 0) {

                        window.location.href = Data.webAppBase() + 'account/leadswalletorg';

                    } else {
                         if (result.email === result.contactEmail && result.industryFlag === '0') {
                            //console.log("First Time");
                            window.location.href = Data.webAppBase() + 'account/industrydet';
                        }  else {
                            //console.log("Visitor Details");
                            if ($location.absUrl().indexOf('nextURL') > 0) {
                                $scope.nextUrl = $location.absUrl().split("?=")[1];
                                window.location.href = Data.webAppBase() + 'account' + $scope.nextUrl;

                            } else {
                                sessionStorage.setItem("xtime", '0');
                                sessionStorage.setItem("menuact", '1');
                                window.location.href = Data.webAppBase() + 'account/dashboard';
                            }
                        }
                    }


                } else if (result.status === 'basic') {
                    $scope.basic = true;
                    console.log(result);
                } else if (result.status === 'warning') {
                    $scope.warning = true;
                    console.log(result);
                } else {
                    $scope.error = true;
                    $scope.warning = false;
					$scope.errormsg = result.message;
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    console.log(result);
                }
            });
        }
    };

    $scope.leadswalletlogin = function (user) {

        if (user["username"] !== '') {

            console.log('Username is ' + user["username"]);

            Data.post('account/leadswalletlogin', user).then(function (result) {
                if (result.status !== 'error' && result.status !== 'warning' && result.status !== 'basic') {

                    $scope.error = false;
                    console.log('login success.....');
                    sessionStorage.setItem("userId", result.id);
                    sessionStorage.setItem("firstName", result.firstName);
                    sessionStorage.setItem("lastName", result.lastName);
                    sessionStorage.setItem("orgId", result.organizationId);
                    sessionStorage.setItem("token", result.token);
                    sessionStorage.setItem("userType", result.userType);
                    sessionStorage.setItem("accountType", result.accountType);
                    sessionStorage.setItem("userLevel", result.access_level);
                    sessionStorage.setItem("userEmail", result.email);
                    sessionStorage.setItem("roleId", result.roleId);
                    localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
                    //console.log("Visitor Data");
                    //console.log(localStorage.getItem("visitorDet"));
                    if ($location.absUrl().indexOf('leadswalletlogin') > 0) {

                        window.location.href = Data.webAppBase() + 'account/leadswalletorg';

                    } else {
                        if (result.email === result.firstName) {
                            sessionStorage.setItem("menuact", 4);
                            sessionStorage.setItem("xtime", '1');
                            window.location.href = Data.webAppBase() + 'account/company';
                        } else {
                            console.log("Visitor Details");
                            if ($location.absUrl().indexOf('nextURL') > 0) {
                                $scope.nextUrl = $location.absUrl().split("?=")[1];
                                window.location.href = Data.webAppBase() + 'account' + $scope.nextUrl;

                            } else {
                                sessionStorage.setItem("xtime", '0');
                                sessionStorage.setItem("menuact", '1');
                                window.location.href = Data.webAppBase() + 'account/dashboard';
                            }
                        }
                    }


                } else if (result.status === 'basic') {
                    $scope.basic = true;
                    console.log(result);
                } else if (result.status === 'warning') {
                    $scope.warning = true;
                    console.log(result);
                } else {
                    $scope.error = true;
                    $scope.warning = false;
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    console.log(result);
                }
            });
        }
    };
	
	$scope.emailVerify = 'true';
    $scope.checkEmail = function () {
        var emailC = angular.element(document.getElementsByName("email"));
        var checkEmail = emailC.val();
        if (checkEmail) {
            var emailverify = $http.get(Data.serviceBase() + '/visitor/vemailVerification', {params: {"verifyEmail": checkEmail}});
            emailverify.then(function (response) {
                //console.log(response.data)
                $scope.emailVerify = response.data;
               /* if ($scope.emailVerify === 'false') {
                    var sbt = angular.element(document.getElementsByName("submit"))[0];
                    sbt.disabled = true;
                } else {
                    var sbt = angular.element(document.getElementsByName("submit"))[0];
                    sbt.disabled = false;
                }*/
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
    };

    //$scope.response = null;
    $rootScope.accountType = sessionStorage.getItem("accountType");
    $rootScope.firstName = sessionStorage.getItem("firstName");
    $rootScope.lastName = sessionStorage.getItem("lastName");
    $rootScope.menuact = sessionStorage.getItem("menuact");
    $rootScope.curroleId = sessionStorage.getItem("roleId");
    var orgId = sessionStorage.getItem("orgId");
    if ($rootScope.curroleId === '1') {
        $rootScope.roletxt = 'Account Owner';
    } else if ($rootScope.curroleId === '2') {
        $rootScope.roletxt = 'Manager';
    } else if ($rootScope.curroleId === '3') {
        $rootScope.roletxt = 'L1 User';
    } else {
        $rootScope.roletxt = 'L2 User';
    }
    $scope.initMenu = function () {
        var oresprom = $http.get(Data.serviceBase() + 'organization/organization', {params: {"key": "id", "value": orgId}});
        oresprom.then(function (response) {
            //console.log(response);
            $rootScope.orgexpireDet = response.data[0];
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        var resprom = $http.get(Data.serviceBase() + '/credit/credit', {params: {"key": "organizationId", "value": $scope.organizationId}});
        resprom.then(function (response) {
            $rootScope.initalcredits = response.data[0];
  
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        var resprom = $http.get(Data.serviceBase() + '/organization/planDet', {});
        resprom.then(function (response) {
            if (response.data) {
                angular.forEach(response.data, function (value) {
                    if($rootScope.accountType === value.planNameId){
                        $rootScope.planSelectedName = value.planName;
                    }
                });
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $rootScope.activeness = function (menuact) {

        sessionStorage.setItem("menuact", menuact);
        if (menuact === '1') {
            $rootScope.title = "Dashboard";
        }
        if (menuact === '2') {
            $rootScope.title = "Manage List";
        }
        if (menuact === '3') {
            $rootScope.title = "All Leads";
        }

		if (menuact === '5') {
           // $rootScope.title = "Hello Email";
        }
		if (menuact === '6') {
           // $rootScope.title = "Manage Users";
        }
        /* if(menuact === '4'){
         $rootScope.title="Settings";
         }*/
         if(menuact === '7'){
         $rootScope.title="Import Leads";
         }

    };
    $rootScope.redirectToM = function (url) {
        var userId = sessionStorage.getItem("userId");
        $scope.roleId = sessionStorage.getItem('roleId');
        $scope.rolemsg = "";
        if ($scope.roleId === '2') {
            $scope.rolemsg = "Manager";
        } else if ($scope.roleId === '3') {
            $scope.rolemsg = "L1 User";
        } else {
            $scope.rolemsg = "L2 User";
        }
        var organizationId = sessionStorage.getItem("orgId");
        //localStorage.getItem('sessionStorage')

        if (url === 'account/uploadlead') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "importLead"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    window.location.href = Data.webAppBase() + url;

                } else {
                    $scope.message = {"title":"Authorization failure","message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to Import Leads in to HelloLeads. Users with account owner or manger roles can Import leads. Please contact HelloLeads account owner or managers in your company for Importing leads."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        } else if (url === 'account/exportLead') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "exportLeads"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    window.location.href = Data.webAppBase() + url;

                } else {
                     $scope.message = {"title":"Authorization failure","message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to export leads from HelloLeads. Only account owner can export leads. Please contact HelloLeads account owner in your company for exporting leads."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        } else if (url === 'account/company') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "companyProfile"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    window.location.href = Data.webAppBase() + url;

                } else {
                   $scope.message = {"title":"Authorization failure","message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to  manage company profile information. Only account owner can manage company profile  information. Please contact HelloLeads account owner in your company for assistance with this task."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        } else if (url === 'account/pricing') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "payment"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    window.location.href = Data.webAppBase() + url;

                } else {
                   $scope.message = {"title":"Authorization failure","message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to make payment. Only account owner can make payment. Please contact HelloLeads account owner in your company for assistance with this task."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        }else if (url === 'account/report') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "customAnalytics"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    window.location.href = Data.webAppBase() + url;

                } else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to use Custom Analytics feature. Only account owner or managers can view Custom Analytics. Please contact HelloLeads account owner or managers in your company for assistance with this task."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        }
		
		else if (url === 'account/helloEmail') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "bulkEmail"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    window.location.href = Data.webAppBase() + url;

                } else {
                    $scope.message = {"title":"Authorization failure","message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to use Hello Email feature. Only account owner or managers can send Hello Email. Please contact HelloLeads account owner or managers in your company for assistance with this task."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        }  
		else if (url === 'account/hellosms') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "bulkEmail"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    window.location.href = Data.webAppBase() + url;

                } else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to use Hello SMS feature. Only account owner or managers can send Hello SMS. Please contact HelloLeads account owner or managers in your company for assistance with this task."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        }else if (url === 'account/hellosms') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "bulkEmail"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    window.location.href = Data.webAppBase() + url;

                } else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to use Hello SMS feature. Only account owner or managers can send Hello SMS. Please contact HelloLeads account owner or managers in your company for assistance with this task."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        }else if (url === 'account/leadStage') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "changeHistory"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    window.location.href = Data.webAppBase() + url;

                } else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to customize the lead stages. Only account owner can customize the lead stages. Please contact HelloLeads account owner in your company for assistance with this task."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        }
		else if (url ==='account/apisettings') {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": organizationId, "userId": userId, "permission": "apisettings"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {

                    window.location.href = Data.webAppBase() + url;

                } else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to access API integration page and the authentication details. Only user(s) with role of account owner can access API integration page. Please contact your HelloLeads account owner in your company for assistance with this task."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        }	
		
		else {
            window.location.href = Data.webAppBase() + url;
        }
    };
	
	// Whatsnew --------------------------------------------------------------------------
	
	 // ------------------------ Whats new --------------------------------
     $scope.newupdates = {};
     $scope.getAllNewUpdates = function(){
         var resprom = $http.get(Data.serviceBase() + '/organization/allWhatsNew', {params: {}});


        resprom.then(function (response) {
            console.log(response.data);
            $scope.newupdates = response.data;
            

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
     };

     $scope.getsessionstatus = function ()
     {
        var datas=setInterval(function()
        {
            var categoryresp = $http.get(Data.serviceBase() + '/account/checksession');
            categoryresp.then(function (response)
            {
               if(response.data.data==null)
               {
                    var modalInstanceassignTo = $uibModal.open({
                    templateUrl: './checkuser',
                    controller: 'userCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        selcols: function () {
                            return true;
                        }
                    }
                    });
                    clearInterval(datas);
               }
            });
        },5000);
        
    };

    $scope.logoutuser=function()
    {
        window.location.href=Data.webAppBase() + 'account/login';
    };
     
     $scope.newlinkOpen = function(update){
         if(update.urlLink){
             window.open(update.urlLink,'_blank');
         }else{
             return false;
         }
     }
	
	
	
	
	// ---------------------------------------- End Whats new --------------------------------
	
	// Mail Parser details popup-----------------------
	 $scope.lparseCount = 0;
     $scope.orgId = sessionStorage.getItem('orgId');
     $scope.userId = sessionStorage.getItem('userId');
    $scope.loadEParseCount = function () {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/organization/getlParseCount', {params: {"organizationId": $scope.orgId, "userId": $scope.userId}});

        // $scope.users = {};
        resprole.then(function (response) {
            $scope.lparseCount = parseInt(response.data.mailCount);
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.mailStat = {"organizationId": orgId, "userId": $scope.userId};
    $scope.mailDetails = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './listMparseDet',
            controller: 'listMparseCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                mailDet: function () {
                    return p;
                },
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

            }
        });
    };
// ----------------------------------- End Email parser popup --------------------------

    $scope.adduser = function (user) {
		 var orgId = sessionStorage.getItem("orgId");
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        console.log('About to add a new user......');
        console.log(Data.serviceBase());
        console.log(user);

        Data.post('user/register', user).then(function (result) {
            console.log("done with user registration ");
            console.log(result);
            if (result.status !== 'error') {
                toaster.pop(result.status, "", "Registered new user with username " + result.email, 10000, 'trustedHtml');
                window.location.href = Data.webAppBase() + 'account/login';
            } else {
                toaster.pop(result.status, "", 'hi how are you', 10000, 'trustedHtml');
                console.log(result);
            }
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.logout = function () {
        console.log('About to add a logout user......');
        Data.get('account/logout').then(function (results) {
            Data.toast(results);
            sessionStorage.removeItem("userId");
            sessionStorage.removeItem("roleId");
            sessionStorage.removeItem("firstName");
            sessionStorage.removeItem("lastName");
            sessionStorage.removeItem("orgId");
            sessionStorage.removeItem("token");
            sessionStorage.removeItem("curEventId");
            sessionStorage.removeItem("curEventName");
            sessionStorage.removeItem("curEventsend");
            sessionStorage.removeItem("visitors");
            localStorage.removeItem("curEventsend");
            localStorage.removeItem("filters");
            localStorage.removeItem("visitorDet");
            localStorage.removeItem("curEventId");
            localStorage.removeItem("curEventName");
            localStorage.removeItem("credit");
            sessionStorage.removeItem("userEmail");
            sessionStorage.removeItem("wcount");
            sessionStorage.removeItem("chkcount");
            sessionStorage.removeItem("lcount");
            sessionStorage.removeItem("userLevel");
            sessionStorage.removeItem("userId");
            sessionStorage.removeItem("firstName");
            sessionStorage.removeItem("lastName");
            sessionStorage.removeItem("token");
            sessionStorage.removeItem("userEmail");
            sessionStorage.removeItem("cFirstName");
            sessionStorage.removeItem("cLastName");
            sessionStorage.removeItem("userType");
            sessionStorage.removeItem("menuact");
            sessionStorage.removeItem("selpage");
            sessionStorage.removeItem("cOrgName");
            sessionStorage.removeItem("cUsers");
            sessionStorage.removeItem("pchkcount");
            sessionStorage.removeItem("PEDate");
            sessionStorage.removeItem("accountType");
            sessionStorage.removeItem("cEmail");
            sessionStorage.removeItem("countryCode");
            sessionStorage.removeItem("orgCountry");
            sessionStorage.removeItem("orgCurrency");
            sessionStorage.removeItem("filterindex");
            sessionStorage.removeItem("category");
            sessionStorage.removeItem("filterlabel");
            sessionStorage.removeItem("service");
            sessionStorage.removeItem("xtime");
            localStorage.removeItem('sessionStorage');
            localStorage.removeItem('visitors');
            localStorage.removeItem('visitorDet');
            window.location.href = Data.webAppBase() + 'account/login';
        });
    };
	
	//Progress Values
    $rootScope.progressVal = 0;
    $scope.countryDet = {};
	$scope.countryDetail = {};
    $scope.getCountryCodeDet = function () {
        $http.get('../../../css/country.json').success(function (data) {
            $scope.countryDetail = data;
        });
		$http.get('../../../css/phone.json').success(function (data) {
        $scope.countryDet = data;
    });
    };

    $scope.getUserDet = function () {
        if (sessionStorage.getItem("userId") && sessionStorage.getItem("orgId")) {
            var userId = sessionStorage.getItem("userId");
            var organizationId = sessionStorage.getItem("orgId");
            var oresprom = $http.get(Data.serviceBase() + 'organization/organization', {params: {"key": "id", "value": organizationId}});
            oresprom.then(function (response) {
                //console.log(response);
                var orgDet = response.data[0];


                var resprom = $http.get(Data.serviceBase() + 'user/user', {params: {"key": "id", "value": userId}});
                resprom.then(function (response) {
                    //console.log(response);
                    $scope.user = response.data[0];
                    $scope.user.organizationName = sessionStorage.getItem('cOrgName');
					if($scope.user.country !== null){   } else{ $scope.user.country = sessionStorage.getItem('orgCountry');}
                    if($scope.user.phoneCode !== null) { }else{ $scope.user.phoneCode = sessionStorage.getItem('countryCode');}
                    if($scope.user.address1 === null ){ $scope.user.address1 = orgDet['address1']; }else{ $scope.user.address1; }
                    if($scope.user.address2 !== null){ }else{ $scope.user.address2 =orgDet.address2; }
                    if($scope.user.city !== null) { }else{ $scope.user.city = orgDet.city;}
                    if($scope.user.state !== null) { }else{ $scope.user.state = orgDet.state;}
                    if($scope.user.zip !== null) { }else{ $scope.user.zip = orgDet.zip;}
                    console.log($scope.user);
                    $rootScope.checkProgress($scope.user);
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });

        }

    };
    if (sessionStorage.getItem("userId") && sessionStorage.getItem("orgId") && !localStorage.getItem("credit")) {
        var userId = sessionStorage.getItem("userId");
        var organizationId = sessionStorage.getItem("orgId");
        console.log(userId);

        //Checking Credits
        var resprom = $http.get(Data.serviceBase() + '/credit/credit', {params: {"key": "organizationId", "value": organizationId}});
        resprom.then(function (response) {
            $rootScope.credits = response.data[0];
            localStorage.setItem("credit", JSON.stringify($rootScope.credits));
            //console.log(response.data[0]);
            //$scope.eventSelected = response.data[0];


        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    } else {
        $rootScope.credits = JSON.parse(localStorage.getItem("credit"));
    }
	
	 $rootScope.checkProgress = function (user) {
        var filedschk = ['firstName','lastName','phone','email','telephoneOffice','userWebsite','designation','organizationName','address1','city','state','country','zip'];
        var steps = 0;
        angular.forEach(user, function (value, key) {
            if (value && filedschk.indexOf(key) !== -1) {
                if (value.length > 0) {
                    steps = steps + 1;
                    // console.log(steps);
                }
            }
        });
        if(user.facebookURL || user.twitterURL || user.linkedinURL || user.instagramURL || user.otherURL){
            steps = steps + 1;
        }
        $rootScope.progressVal = Math.round((steps / 14) * 100);

    };
	$scope.submitPref = function(user){
        //console.log(user);
    user.id = sessionStorage.getItem('userId');
    Data.put('user/emailUpdatePreference', user).then(function (result) {
           // console.log("done with user registration ");
            //console.log(result);
            if (result.status !== 'error') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                
            } else {
                toaster.pop(result.status, "", result.message, 6000, 'trustedHtml');
                //console.log(result);
            }
        });
    
    
    };
    /* if (sessionStorage.getItem("userId") && sessionStorage.getItem("orgId")) {
     var vorgId = sessionStorage.getItem("orgId");
     if (localStorage.getItem("visitorDet") === null) {
     var visitorSearchInst = $http.get(Data.serviceBase() + '/visitor/visitorSearchdata', {params: {"organizationId": vorgId}});
     
     visitorSearchInst.then(function (response) {
     $rootScope.visitorSearch = response.data;
     //localStorage.setItem("visitorDet", JSON.stringify($rootScope.visitorSearch));
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
     } else {
     $rootScope.visitorSearch = {};
     $rootScope.visitorSearch = JSON.parse(localStorage.getItem("visitorDet"));
     }
     
     }*/
    $rootScope.vorgId = sessionStorage.getItem("orgId");
    //$rootScope.urlBase = Data.serviceBase()+"/visitor/visitorSearchdata?organizationId="+vorgId+"&query=";
    $rootScope.getSearchData = function (query) {
        if (query.length > 4) {


            var visitorSearchInst = $http.get(Data.serviceBase() + '/visitor/visitorSearchdata', {params: {"organizationId": vorgId, "query": query}});

            visitorSearchInst.then(function (response) {
                $rootScope.visitorSearch = response.data[0];
                //localStorage.setItem("visitorDet", JSON.stringify($rootScope.visitorSearch));
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
        }



    };
    $rootScope.vvalue = {};

    $rootScope.viewVisitor = function (selected) {
        console.log(selected.originalObject.id);
        if (selected.originalObject.id !== undefined) {
            window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + selected.originalObject.id, "_self");
        }
    };

    $rootScope.redirectToP = function () {
        sessionStorage.setItem("menuact", 4);
        window.location.href = Data.webAppBase() + 'account/pricing';
    };
	
	// Dupliacte email checking
    
    var original = angular.copy($scope.user);
    $scope.emailDup = false;
    $scope.emailpChange = function (userId, email, valid, dirty) {
        console.log(valid);
        if (valid && email) {
            if (dirty) {


                var emailResp = $http.get(Data.serviceBase() + 'user/userChkEmail', {params: {"email": email}});
                emailResp.then(function (result) {
                    if (result.data.length > 0) {
                        if (result.data[0].id !== userId && userId !== undefined) {
                            $scope.emailDup = true;
                        } else {
                            $scope.emailDup = false;
                        }
                    } else if (result.data.length === 0) {
                        $scope.emailDup = false;
                    }



                    //$scope.emailMesage = result;
                    //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');



                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });

            } else {
                $scope.emailDup = false;
            }
        }
    };


    $scope.updateUser = function (user) {
		user.role = sessionStorage.getItem('roleId');
        //console.log('About to add a logout user......');
		 user.organizationId = sessionStorage.getItem('orgId');
         user.fullname = sessionStorage.getItem('firstName') + " " + sessionStorage.getItem('lastName');
        Data.put('user/userMod', user).then(function (result) {
            if (result.status !== 'error') {
                //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
				 if(result.rtype === 'UPS04'){
                        $scope.message = {"title": "Success", "message":result.message};
                        alertUser.alertpopmodel($scope.message);
                    }else{
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    }
				 $scope.checkProgress($scope.user);

            } else {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

            }
        });


    };
    $scope.updatePassword = function (user) {


        console.log('About to update password......');
        console.log(user);

        var updatePassword = {
            userId: sessionStorage.getItem("userId"),
            oldpassword: user.oldpassword,
            newpassword: user.newpassword
        };

        console.log(updatePassword);


        Data.put('user/changepassword', updatePassword).then(function (result) {
            if (result.status !== 'error') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                $scope.user = {};

            } else {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

            }
        });



    };


});
evtApp.controller('listMparseCtrl', function ($scope, Data, $http, $uibModalInstance, $uibModal, mailDet, toaster) {


    $scope.actype = sessionStorage.getItem('accountType');
    $scope.orgId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.mailDetails = {};
	$scope.loader = false;
    $scope.mailDetails.length = 0;
    
    if ($scope.actype === 'P2' || $scope.actype === 'P3' || $scope.actype === 'P4'|| $scope.actype === 'P5'|| $scope.actype === 'P6') {
		$scope.loader = true;
        
        var resprom = $http.get(Data.serviceBase() + '/organization/mparseList', {params: mailDet});


        resprom.then(function (response) {
            $scope.mailDetails = response.data;
            $scope.loader = false;
			if ($scope.mailDetails.length == 0) {
                var respromsmtp = $http.get(Data.serviceBase() + '/organization/orgSMTP', {params: {'organizationId': $scope.orgId, "userId": $scope.userId}});
                respromsmtp.then(function (response) {
                       // console.log(response.data);
                        $scope.smtpConf = response.data;
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    }


    $scope.openLead = function (visitorId,mailId) {
        if (visitorId) {
            
            sessionStorage.setItem('emailPId',mailId);
            window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + visitorId, "_blank");
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
	$scope.redirectToPay = function () {
        $uibModalInstance.dismiss('Close');
        window.open(Data.webAppBase() + 'account/pricing', "_self");
    };
    
});

evtApp.controller('dispUserCtrl', function ($scope, $rootScope, alertUser,checkPremium,trialPopUp, Data, toaster, $uibModal, $http) {
    $rootScope.emptyuser = {};
    sessionStorage.setItem("menuact", '6');
    $rootScope.menuact = '6';
    if (sessionStorage.getItem("xtime") === "1") {
        $rootScope.firstProfile = true;
    } else {
        $rootScope.firstProfile = false;
    }
	
	$scope.orgDetails = {};

    $scope.setCategory = function (categ) {
        $scope.selectedCategories = categ;
        $scope.selCategname = categ.name;
    };

    $scope.getCurrenyDetails = function () {
        // --------------------------------------------------------------- Getting Currency details ----------------------------------------------------
        var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

        resprom.then(function (response) {
            if (response.data) {
                $scope.orgDetails = response.data[0];
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    }
	
    $rootScope.title = "Manage Users ";
    $rootScope.addButtonhtml = '<button class="btn btn-primary btn-sm" ng-click="addUser(emptyuser);"><i class="fa fa-plus-circle"></i> Add User</button>&nbsp;';
    //$rootScope.addButtonhtml = '&nbsp;';
    $rootScope.addButtonhtml += '<button class="btn btn-default btn-sm btn-circle" popover="You may add additional users to your HelloLeads account and manage them here. Additional users are typically team members in the marketing or sales department of your organization." popover-title="Manage Users" popover-trigger="focus" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    $scope.loader = true;
    $scope.organizationId = sessionStorage.getItem('orgId');
	$scope.currencyType = sessionStorage.getItem('orgCurrency');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }
    console.log("user Organization Id is " + $scope.organizationId);
    $scope.cFirstName = sessionStorage.getItem("cFirstName");
    $scope.cEmail = sessionStorage.getItem("cEmail");
    $scope.getAllUsers = function () {
        var resprom = $http.get(Data.serviceBase() + '/user/userByOrganization', {params: {"organizationId": $scope.organizationId}});

        $scope.users = {};
        resprom.then(function (response) {
            $scope.users = response.data;
            $scope.loader = false;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    }
    $scope.editUser = function (p, size) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "editUser"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {

                var modalInstance = $uibModal.open({
                    templateUrl: './modifyUser',
                    controller: 'modifyUserCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        user: function () {
                            return p;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/user/userByOrganization', {params: {"organizationId": $scope.organizationId}});

                        $scope.users = {};
                        resprom.then(function (response) {
                            $scope.users = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {

                $scope.message = {"title":"Authorization failure","message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete users. Only user with role of account owner have permission to manage users. Please contact HelloLeads account owner in your company for adding users or modifying them"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

	//----------------------------------------------- Show User Activity Log -------------------------------------------------- 

    $scope.userHistory = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('user/setUserHistory', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $rootScope.showActivityLog = function (p, size) {

        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "changeHistory"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                //$scope.getAllServCategField();
                var modalInstance = $uibModal.open({
                    templateUrl: './listActivityMessage',
                    controller: 'userActivityCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        lauser: function () {
                            return p;
                        },
                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {

                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });


    };
    
   //----------------------------------------------- End Show List Activity Log -------------------------------------------------- 

    $scope.getTag = function () {
        return '<a>sdfsdfsdf</a>';
    }

    $rootScope.addUser = function (p, size) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "editUser"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
				
				 // Free Premium Restriction Checking
                var respuserchk = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId,"checkReq":"user"}});

                // $scope.users = {};
                respuserchk.then(function (response) {
                    console.log(response);
                    $scope.loader = false;
                    if (response.data.status === 'success') {
                var modalInstance = $uibModal.open({
                    templateUrl: './modifyUser',
                    controller: 'modifyUserCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        user: function () {
                            return p;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/user/userByOrganization', {params: {"organizationId": $scope.organizationId}});

                        $scope.users = {};
                        resprom.then(function (response) {
                            $scope.users = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
				} else {
					$scope.message = {"title": "Users", "message": response.data.message,"cancel": true};
					checkPremium.premiumpopmodel($scope.message);
				}
			}, function (response) {

				console.log('Error happened -- ');
				console.log(response);
			});
            } else {
                $scope.message = {"title":"Authorization failure","message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete users. Only user with role of account owner have permission to manage users. Please contact HelloLeads account owner in your company for adding users or modifying them"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });



    };

    $scope.delUser = function (p, size) {
         var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});
        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteUser"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/user/userDet', {params: {"key": "organizationId", "value": $scope.organizationId, "deleteUserId": p.id}});

        // $scope.users = {};
        resprole.then(function (response) {
            $scope.allUsers = response.data.users;
            $scope.leadCount = response.data.leadCount;
            $scope.loader = false;
        if (response.data.status === 'success') {
                var modalInstance1 = $uibModal.open({
                    templateUrl: './deleteUser',
                    controller: 'delUserCtrl',
                    size: size,
                    resolve: {
                        user: function () {
                            return p;
                        },
                        allUsers: function() {
                            return $scope.allUsers;    
                        },
                        leadCount: function() {
                            return $scope.leadCount;
                        }
                    }
                });
                modalInstance1.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/user/userByOrganization', {params: {"organizationId": $scope.organizationId}});

                        $scope.users = {};
                        resprom.then(function (response) {
                            $scope.users = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                console.log("Users not found!");
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
            } else {
                $scope.message = {"title":"Authorization failure","message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete users. Only user with role of account owner have permission to manage users. Please contact HelloLeads account owner in your company for adding users or modifying them"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });


    };

  //   $scope.delUser = function (p, size) {
		//  var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

  //       // $scope.users = {};
  //       resprole.then(function (response) {
  //           console.log(response);
  //           $scope.loader = false;
  //           if (response.data.status === 'success') {
  //       var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteUser"}});

  //       // $scope.users = {};
  //       resprole.then(function (response) {
  //           console.log(response);
  //           $scope.loader = false;
  //           if (response.data.status === 'success') {
  //               var modalInstance1 = $uibModal.open({
  //                   templateUrl: './deleteUser',
  //                   controller: 'delUserCtrl',
  //                   size: size,
  //                   resolve: {
  //                       user: function () {
  //                           return p;
  //                       }
  //                   }
  //               });
  //               modalInstance1.result.then(function (i) {
  //                   if (i) {
  //                       var resprom = $http.get(Data.serviceBase() + '/user/userByOrganization', {params: {"organizationId": $scope.organizationId}});

  //                       $scope.users = {};
  //                       resprom.then(function (response) {
  //                           $scope.users = response.data;
  //                       }, function (response) {
  //                           console.log('Error happened -- ');
  //                           console.log(response);
  //                       });
  //                   }
  //               });
  //           } else {
  //               $scope.message = {"title":"Authorization failure","message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete users. Only user with role of account owner have permission to manage users. Please contact HelloLeads account owner in your company for adding users or modifying them"};
  //               alertUser.alertpopmodel($scope.message);
  //               //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
  //           }
  //       }, function (response) {

  //           console.log('Error happened -- ');
  //           console.log(response);
  //       });
		// } else {
  //               trialPopUp.trialPopmodel(response.data);
  //           }
  //       }, function (response) {

  //           console.log('Error happened -- ');
  //           console.log(response);
  //       });


  //   };


});

//----------------------------------------------- Show List Activity Log -------------------------------------------------- 


evtApp.controller('userActivityCtrl', function ($scope, Data,$http, $uibModalInstance, lauser, toaster) {

    $scope.luser = lauser;
    $scope.title = "Change History for User " + $scope.luser.firstName;
    $scope.listActivities = {};
    $scope.eventActivity = function (user) {
        var activityResp = $http.get(Data.serviceBase() + 'user/listAcitivites', {params: {"userId":$scope.luser.id}});


            activityResp.then(function (response) {
            console.log(response.data);
            $scope.listActivities = response.data;
            
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

 //-----------------------------------------------End Show List Activity Log -------------------------------------------------- 


evtApp.controller('modifyUserCtrl', function ($scope, Data, $http, $uibModalInstance, $uibModal, user,alertUser, toaster) {
	$scope.orgDetails = {};
    $scope.setCategory = function (categ) {
        $scope.selectedCategories = categ;
        $scope.selCategname = categ.name;
    };
    $scope.getCurrencyDet = function () {
        // --------------------------------------------------------------- Getting Currency details ----------------------------------------------------
        var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});
        resprom.then(function (response) {
            if (response.data) {
                $scope.orgDetails = response.data[0];
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    var original = angular.copy(user);
    $scope.user = angular.copy(user);
	if (!$scope.user.phoneCode)
        $scope.user.phoneCode = sessionStorage.getItem("countryCode");
	$scope.cEmail = sessionStorage.getItem("cEmail");
    if ($scope.user.id !== undefined) {
        $scope.title = "Update User";
        $scope.btntxt = "Update";
    } else {
        $scope.title = "Add New User";
        $scope.btntxt = "Add";
    }
	$scope.currencyType = sessionStorage.getItem('orgCurrency');
     $scope.mobileCodeCheck = function () {
        if ($scope.user.phone) {
            if ($scope.user.phoneCode) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
    $scope.canSaveUser = function () {
        return $scope.userform.$valid && !angular.equals($scope.user, original);
    };
    $scope.countryDet = {};
    $http.get('../../../css/phone.json').success(function (data) {
        $scope.countryDet = data;
    });
	
	//Password visibility-----------------------------
    $scope.txtpassword = 'password';
    $scope.seePass = function(){
        if($scope.txtpassword === 'password'){
            $scope.txtpassword = 'text';
        }else{
            $scope.txtpassword = 'password';
        }
    };
    //-------------------------------------------------
    //$scope.levellist = ["All", "REGI", "LW"];
    var resprom = $http.get(Data.serviceBase() + '/role/role', {params: {}});

    $scope.roles = {};
    resprom.then(function (response) {
        console.log(response);
        $scope.roles = response.data;
        angular.forEach($scope.roles, function (d) {
            if (d.id === $scope.user.roleId) {
                console.log(d.id);
                $scope.user.role = d.id;
            }
        });
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });

    $scope.popmodel = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './showRoles',
            controller: 'rolesCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                user: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

            }
        });
    }


	 $scope.emailDup = false;
    $scope.emailChange = function (userId, email, valid, dirty) {
        console.log(valid);
        if (valid && email) {
            if (dirty) {


                var emailResp = $http.get(Data.serviceBase() + 'user/userChkEmail', {params: {"email": email}});
                emailResp.then(function (result) {
                    if (result.data.length > 0) {
                        if (result.data[0].id !== userId && userId !== undefined) {
                            $scope.emailDup = true;
                        }else if (userId === undefined) {
                            $scope.emailDup = true;
                        }

						else {
                            $scope.emailDup = false;
                        }
                    } else if (result.data.length === 0) {
                        $scope.emailDup = false;
                    }



                    //$scope.emailMesage = result;
                    //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');



                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });

            } else {
                $scope.emailDup = false;
            }
        }
    };

    $scope.bloader = '0';


    $scope.updatuser = function (user) {
        console.log(user);
		 $scope.bloader = '1';
        console.log("Going to add or update user....");
        if (user.id !== undefined) {

            console.log("its an update to the user....");
			user.organizationId = sessionStorage.getItem('orgId');
            user.fullname = sessionStorage.getItem('firstName') + " " + sessionStorage.getItem('lastName');
            Data.put('user/userMod', user).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    //$uibModalInstance.dismiss('Close');
                    console.log('Setting the user ');
					 $scope.bloader = '0';
                    if(result.rtype === 'UPS04'){
                        $scope.message = {"title": "Success", "message":result.message};
                        alertUser.alertpopmodel($scope.message);
                    }else{
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    }

                } else {
                    $uibModalInstance.close(1);
                    console.log(result);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                }
            });

        } else {
            console.log("its an add to the user....");
            console.log(user);
            if (user.email !== undefined && user.password !== undefined) {
                console.log(user.email);
                user.organizationId = sessionStorage.getItem('orgId');
                user.fullname = sessionStorage.getItem('firstName') + " " + sessionStorage.getItem('lastName');
                Data.post('user/user', user).then(function (result) {
                    if (result.status !== 'error') {

                        $uibModalInstance.close(1);
                        console.log('Setting the user ');
						$scope.bloader = '0';
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                    } else {
                        $uibModalInstance.close(1);
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                        console.log(result);
                    }
                });
            }
        }
    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});
evtApp.controller('rolesCtrl', function ($scope, Data, $http, $uibModalInstance, $uibModal, user, toaster) {



    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});

evtApp.controller('delUserCtrl', function ($scope, Data, $uibModalInstance, $uibModal, user, allUsers, leadCount, alertUser, toaster) {

    $scope.user = user;
    $scope.allUsers = allUsers;
    $scope.leadCount = leadCount;
    $scope.title = "Delete User";
    $scope.deleteUser = function (user) {
        if (user.email !== undefined) {
            Data.put('visitor/updateDeletedUserAssignTo', user).then(function (result) {
                if (result.status !== 'error') {
                    console.log(result);
                    $scope.assignCount = result.counts.assignCount;
                    $scope.withAceess = result.counts.withAceess;
                    $scope.withoutAceess = result.counts.withoutAceess;
                    $scope.deletedUserName = user.firstName + " " + user.lastName;
                    $scope.assignToWithUserName = result.counts.assignToWithUserName;
                    Data.delete('user/user/' + user.id).then(function (result) {
                        if (result.status !== 'error') {
                            $uibModalInstance.close(1);
                            if ($scope.assignCount !== null) {
                                $scope.message = {"title":"Success","message": "User " + $scope.deletedUserName + " has been deleted. " + " " + $scope.assignCount + " lead(s) successfully assigned to " + $scope.assignToWithUserName + "."};
                            } else if ($scope.withAceess !== null && $scope.withoutAceess !== null) {
                                if ($scope.withAceess !== 0 && $scope.withoutAceess !== 0) {
                                    $scope.message = {"title":"Success","message": "User " + $scope.deletedUserName + " has been deleted. " + " " + $scope.withAceess + " lead(s) successfully assigned to " + $scope.assignToWithUserName + ", " + $scope.withoutAceess + " lead(s) assigned to respective list owner(s) (since " + $scope.assignToWithUserName + " does not have rights to these list(s))"};
                                } else if ($scope.withAceess !== 0 && $scope.withoutAceess == 0){
                                    $scope.message = {"title":"Success","message": "User " + $scope.deletedUserName + " has been deleted. " + " " + $scope.withAceess + " lead(s) successfully assigned to " + $scope.assignToWithUserName + "."};
                                } else if ($scope.withAceess == 0 && $scope.withoutAceess !== 0){
                                    $scope.message = {"title":"Success","message": "User " + $scope.deletedUserName + " has been deleted. " + " " + $scope.withAceess + " lead(s) successfully assigned to " + $scope.assignToWithUserName + ", " + $scope.withoutAceess + " lead(s) assigned to respective list owner(s) (since " + $scope.assignToWithUserName + " does not have rights to these list(s))"};
                                }
                            }
                            alertUser.alertpopmodel($scope.message);
                            //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                        } else {
                            console.log(result);
                        }
                    });
                } else {
                    Data.delete('user/user/' + user.id).then(function (result) {
                        if (result.status !== 'error') {
                            $uibModalInstance.close(1);
                            $scope.deletedUserName = user.firstName + " " + user.lastName;
                            $scope.message = {"title":"Success","message": "User " + $scope.deletedUserName + " has been deleted successfully."};
                            alertUser.alertpopmodel($scope.message);
                        } else {
                            console.log(result);
                        }
                    });
                }
            });
        } else {
            Data.delete('user/user/' + user.id).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    $scope.deletedUserName = user.firstName + " " + user.lastName;
                    $scope.message = {"title":"Success","message": "User " + $scope.deletedUserName + " has been deleted successfully."};
                    alertUser.alertpopmodel($scope.message);
                } else {
                    console.log(result);
                }
            });
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});

// evtApp.controller('delUserCtrl', function ($scope, Data, $uibModalInstance, user, toaster) {

//     $scope.user = user;
//     $scope.title = "Delete User";
//     $scope.deleteUser = function (user) {

//         if (user.email !== undefined) {

//             Data.delete('user/user/' + user.id).then(function (result) {
//                 if (result.status !== 'error') {
//                     $uibModalInstance.close(1);

//                     console.log('Setting the user ');
//                     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

//                 } else {
//                     console.log(result);
//                 }
//             });
//         }
//     };

//     $scope.cancel = function () {
//         $uibModalInstance.dismiss('Close');
//     };

// });
