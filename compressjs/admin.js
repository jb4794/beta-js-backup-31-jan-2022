evtApp.controller('adminUserCtrl', function ($scope, Data, toaster, $http) {

    $scope.users = {};
    $scope.user = {};
    $scope.login = function (user) {

        if (user["username"] !== '') {

            Data.post('account/adminlogin', user).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.firstName + "Logged in Scuccesfully", 10000, 'trustedHtml');
                    sessionStorage.setItem("userId", result.id);
                    //sessionStorage.setItem("firstName", result.firstName);
                    //sessionStorage.setItem("lastName", result.lastName);
                    sessionStorage.setItem("token", result.token);
                    sessionStorage.setItem("userType", result.userType);

                    window.location.href = Data.webAppBase() + 'account/adminhome';

                } else {
                    console.log('error');
                    $scope.error = true;
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    console.log(result);
                }
            });
        }
    };
    $scope.logout = function () {
        console.log('About to add a logout user......');
        Data.get('account/logout').then(function (results) {
            Data.toast(results);
            sessionStorage.setItem("userId", '');
            //sessionStorage.setItem("firstName", '');
            //sessionStorage.setItem("lastName", '');
            sessionStorage.setItem("token", '');


            window.location.href = Data.webAppBase() + 'account/adminlogin';
        });
    }
});
evtApp.controller('emaildisporgCtrl', function ($scope, $uibModal, Data, toaster, $http) {		
    $scope.emailtemp = {};		
    $scope.emailTemps = {};		
    $scope.btntxt = 'Add';		
    $scope.getAllTemp = function () {		
        var respemailrom = $http.get(Data.serviceBase() + '/admin/emailTemplates');		
        respemailrom.then(function (response) {		
            console.log(response.data);		
            $scope.emailTemps = response.data;		
        }, function (response) {		
            console.log('Error happened -- ');		
            console.log(response);		
        });		
    }		
    $scope.selectTemp = function (email) {		
        $scope.emailtemp = email;		
        $scope.btntxt = 'Save';		
    }		
    $scope.updateemail = function (email) {		
        if (email.id !== undefined) {		
            Data.put('admin/emailTemplate', email).then(function (result) {		
                if (result.status !== 'error') {		
                    //$uibModalInstance.dismiss('Close');		
                    console.log('Setting the Email ');		
                    $scope.emailtemp = {};		
                    $scope.btntxt = 'Add';		
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');		
                } else {		
                    console.log(result);		
                }		
            });		
        } else {		
            if (email.name !== undefined) {		
                Data.post('admin/emailTemplate', email).then(function (result) {		
                    if (result.status !== 'error') {		
                         $scope.getAllTemp();		
                        		$scope.emailtemp = {};		
                    $scope.btntxt = 'Add';
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');		
                        		
                    } else {		
                        console.log(result);		
                    }		
                });		
            }		
        }		
       		
    };		
});
evtApp.controller('emailbulkdisporgCtrl', function ($scope,alertUser, $uibModal, Data, toaster, $http) {

    $scope.email = {};
    $scope.visitorName="";
    $scope.visitorCName = "";
    $scope.toId = "";
    $scope.emailTemps = [];
    $scope.emailTotal = 0;
    $scope.btntxt = "Send";
    $scope.btnptxt = "Send";
    $scope.emailFilter = {};
	//Tiny MCE Begin
	$scope.tinymceOptions = {
        selector: 'textarea',
        height: 500,
        theme: 'modern',
        menubar: false,
        forced_root_block_attrs: {
         "class": "myclass",
         "data-something": "my data",
         "style": "margin: 5px 0;"
        },
        plugins: [
            'advlist autolink lists link image preview hr pagebreak ',
            'searchreplace wordcount visualblocks visualchars code paste',
            'insertdatetime media nonbreaking save table contextmenu directionality'
        ],
       toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
       toolbar2: 'mybutton,mybutton1,mybutton2 ',
        paste_data_images: true,
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'],

        setup: function (editor) {
            editor.addButton('mybutton', {
                text: '#VisitorName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorName#');
                }
            });
            editor.addButton('mybutton1', {
                text: '#VisitorCompanyName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorCompanyName#');
                }
            });
            editor.addButton('mybutton2', {
                text: '#EventName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#EventName#');
                }
            });
        }
    };
	//Tiny MCE End
   // $scope.emailFilter.replyTo = $scope.emailFilter.emailId;
    $scope.clearFilter = function () {
        $scope.emailFilter.length = 0;

    };
    $scope.setReplyTo = function(){
        $scope.emailFilter.replyTo = $scope.emailFilter.emailId;//just to show that it is calculated field
    };
    $scope.filterChanged = function (emailFilters) {
        $scope.emailTemps=[];
        var dtfrom = emailFilters.dtFrom ? emailFilters.dtFrom.toLocaleString() : "";
        var dtto = emailFilters.dtTo ? emailFilters.dtTo.toLocaleString() : "";
        var respemailrom = $http.get(Data.serviceBase() + '/admin/getLeadFilter', {params: {"cgroup": emailFilters.tgroup, "activity": emailFilters.tactivity, "enable": emailFilters.tstatus, "users": emailFilters.tusers,"dtFrom":dtfrom,"dtTo":dtto,"unsub": emailFilters.tunsub,"plan":emailFilters.plan}});
        respemailrom.then(function (response) {
            console.log(response.data['userCount']);
            $scope.emailTemps = response.data['emails'];
            $scope.emailFilter['maillist'] = $scope.emailTemps;
            $scope.emailTotal = response.data['userCount'];
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.SendMailcancel = function () {
        $scope.emailFilter['fromName'] ="";
        $scope.emailFilter['subject'] ="";
        $scope.emailFilter['body'] ="";
        Data.post('admin/bulkEmail', $scope.emailFilter).then(function (result) {
                    if (result.status !== 'error' && result.status === 'success1') {
                        
                      $scope.message = {"title":"Authorization failure","message": "Campaign Cancelled"};
                      alertUser.alertpopmodel($scope.message);
                        $scope.btntxt = 'Send';

                    } else {
                        console.log(result);
                    }
                });
    };
    $scope.SendMailConfirm = function (p, size) {
       if($scope.visitorName){
           p.maillist=[{"name":$scope.visitorName,"email":$scope.toId,"displayName":$scope.visitorCName}];
           $scope.emailTemps=[{"name":$scope.visitorName,"email":$scope.toId,"displayName":$scope.visitorCName}]
       }
        var modalInstance1 = $uibModal.open({
            templateUrl: './adminconfirmMessage',
            controller: 'sendEmailCtrl',
            size: size,
            resolve: {
                maillist: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i === 1) {
                
                $scope.btntxt = 'Sending';
                
                 Data.post('admin/bulkEmail', $scope.emailFilter).then(function (result) {
                    if (result.status !== 'error' && result.status !== 'success1') {
                        
                        $scope.message = {"title":"Success","message": $scope.emailTemps.length + " emails with subject "+ $scope.emailFilter.subject  +" has been sent successfully"};
                        alertUser.alertpopmodel($scope.message);
                        $scope.btntxt = 'Send';

                    } else {
                        console.log(result);
                    }
                });
            }

        });

    };
    $scope.sendPush={};
    $scope.sendPushToAll = function(){
        $scope.btnptxt = 'Sending';
                
                 Data.post('admin/bulkPushToAll',  $scope.sendPush).then(function (result) {
                    if (result.status !== 'error' && result.status !== 'success1') {
                        console.log(result);
                        $scope.message = {"title":"Success","message": "Push message sent to "+result.count+" Users (Devices)"};
                        alertUser.alertpopmodel($scope.message);
                        $scope.btnptxt = 'Send';

                    } else {
                        console.log(result);
                    }
                });
    };

});
evtApp.controller('pricePlanGlobal', function ($scope, $uibModal, Data, toaster, $http) {

    $scope.prices = {};
    var resprom = $http.get(Data.serviceBase() + '/admin/priceDet');


    resprom.then(function (response) {

        $scope.prices = response.data;

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
    $scope.changeExport = function(price){
        if(price.exportLead === '0'){
            price.exportLead = '0';
        }else{
            price.exportLead = '1';
        }
    };
    $scope.updatePrice = function (price) {
        if (price.id !== undefined) {
            
            Data.put('admin/priceUpdate', price).then(function (result) {
                if (result.status !== 'error') {
                  
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    
                }
            });

        }
    }

});
evtApp.controller('sendEmailCtrl', function ($scope, Data, $uibModalInstance, maillist, toaster) {
    $scope.mailDet = maillist;
    $scope.sendConfirm = function () {

        $uibModalInstance.close(1);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});

//UBT
evtApp.controller('dispadminUBTCtrl', function ($scope, $uibModal, Data, toaster, $http) {
    $scope.ubts = {};
    $scope.pageNumber = 1;
    $scope.totalVisitors = 0;
    $scope.totalPage = 1;
    $scope.UBTPerPage = 20;
     $scope.searchInput = '';
    $scope.searchCountry = '';
    $scope.orgDet = '';
    $scope.industryType = '';
    $scope.accountType = '';
    $scope.TSource = '';
    $scope.fDate = '';
    $scope.eDate = '';
    $queryOne = '';
    $queryTwo = '';
    $queryThree = '';
    $queryFour = '';
    $queryFive = '';
    $querySix = '';
    $querySeven = '';
    
    $scope.ubtFilter = function () {
        if ($scope.searchCountry !== null && $scope.searchCountry !== '') {
            console.log('Country Change '+$scope.searchCountry);
            $queryOne = $scope.searchCountry;
        }
        if ($scope.industryType !== null && $scope.industryType !== '') {
            console.log('Industry Type '+$scope.industryType);
            $queryTwo = $scope.industryType;
        }
        if ($scope.accountType !== null && $scope.accountType !== '') {
            console.log('Account Type '+$scope.accountType);
            $queryThree = $scope.accountType;
        }
        if ($scope.TSource !== null && $scope.TSource !== '') {
            console.log('Trns Source '+$scope.TSource);
            $queryFour = $scope.TSource;
        }
        if ($scope.fDate !== null && $scope.fDate !== '') {
            console.log('F Date '+$scope.fDate);
            $queryFive = $scope.fDate;
        }
        if ($scope.fDate !== null && $scope.fDate !== '' && $scope.eDate !== null && $scope.eDate !== '') {
            console.log('F Date '+$scope.TSource);
            console.log('E Date '+$scope.eDate);
            $queryFive = $scope.fDate;
            $querySix = $scope.eDate;
        }
        if ($scope.searchInput !== null ) {
            console.log('Search Input '+$scope.searchInput);
            $querySeven = $scope.searchInput; 
        }
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBT', {params: {"page": $scope.pageNumber, "query1": $queryOne, "query2": $queryTwo, 'query3': $queryThree, 'query4': $queryFour, 'query5': $queryFive, 'query6': $querySix, 'query7': $querySeven}});
        resprom.then(function (response) {
            //console.log(response.data.ubts);
            //console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 50) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 50) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    // $scope.ubtFetch = function (stxt) {
    //     var resprom = $http.get(Data.serviceBase() + '/admin/allUBT', {params: {"page": $scope.pageNumber, "query": stxt}});
    //     resprom.then(function (response) {
    //         console.log(response.data.ubts);
    //         console.log(response.data.count[0].count);
    //         $scope.ubts = response.data.ubts;
    //         $scope.totalubts = parseInt(response.data.count[0].count);
    //         if ($scope.totalubts <= 20) {
    //             $scope.totalPage = 1;
    //         } else {
    //             $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
    //         }
    //     }, function (response) {
    //         console.log('Error happened -- ');
    //         console.log(response);
    //     });
    // };

    $scope.ubtFetchDates = function (ftxt,etxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBTDate', {params: {"page": $scope.pageNumber, "query1": ftxt, "query2": etxt}});
        resprom.then(function (response) {
            console.log(response.data.ubts);
            console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.ubtFetchCountry = function (ctxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBTCountry', {params: {"page": $scope.pageNumber, "query": ctxt}});
        resprom.then(function (response) {
            console.log(response.data.ubts);
            console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.ubtFetchIndustryType = function (itxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBTIndustry', {params: {"page": $scope.pageNumber, "query": itxt}});
        resprom.then(function (response) {
            console.log(response.data.ubts);
            console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.ubtFetchAccountype = function (atxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBTAccount', {params: {"page": $scope.pageNumber, "query": atxt}});
        resprom.then(function (response) {
            console.log(response.data.ubts);
            console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.ubtFetchSource = function (stxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBTSource', {params: {"page": $scope.pageNumber, "query": stxt}});
        resprom.then(function (response) {
            console.log(response.data.ubts);
            console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    

    $scope.pageChanged = function (newPage) {
        $scope.loader = true;
        // $scope.cardView = false;
        $scope.pageNumber = newPage;
        //$scope.pagination.current = newPage;
        //paginationService.setCurrentPage('paginationId', newPage);
        if ($scope.totalubts <= 20) {
            //console.log("Visitor Count" + $scope.totalOrgs);
            $scope.totalPage = 1;
        }
        if ($scope.totalubts > 20) {
            //console.log("Visitor Count 2 " + $scope.totalOrgs);

            if (($scope.totalubts / 20) > 1) {
                $scope.totalPage = Math.floor($scope.totalubts / 20);
                $scope.totalPage = $scope.totalPage + 1;
            }

        }
        
        if ($scope.prevpage !== newPage) {
            $scope.ubtFetch();
            $scope.ubtFetchDates();
            $scope.ubtFetchCountry();
            $scope.ubtFetchIndustryType();
            $scope.ubtFetchAccountype();
            $scope.ubtFetchSource();
            $scope.prevpage = newPage;
        }
    };

    $scope.reset = function() {
        $scope.orgDet = "";
        $scope.searchCountry = "";
        $scope.searchIndustryType = "";
        $scope.searchPlan = "";
        $scope.searchSource = "";
        $scope.fDate = "";
        window.location.href = Data.webAppBase() + 'account/ubtReport';
    };

});
// Ent UBT

//North Star Report
evtApp.controller('dispadminNorthStarCtrl', function ($scope, $uibModal, Data, toaster, $http) {
    $scope.stars = {};
    $scope.pageNumber = 1;
    $scope.totalVisitors = 0;
    $scope.totalPage = 1;
    $scope.reportPerPage = 20;
    $scope.searchCountry = '';
    $scope.orgDet = '';
    $scope.industryType = '';
    $scope.accountType = '';
    $scope.TSource = '';
    $queryOne = '';
    $queryTwo = '';
    $queryThree = '';
    $queryFour = '';
    $queryFive = '';
    
    $scope.northStarFilter = function () {
    	if ($scope.TSource !== null && $scope.TSource !== '') {
            $queryOne = $scope.TSource;
        }
        if ($scope.accountType !== null && $scope.accountType !== '') {
            $queryTwo = $scope.accountType;
        }
        if ($scope.searchCountry !== null && $scope.searchCountry !== '') {
            $queryThree = $scope.searchCountry;
        }
        if ($scope.industryType !== null && $scope.industryType !== '') {
            $queryFour = $scope.industryType;
        }
        if ($scope.ignoreRegi !== null && $scope.ignoreRegi !== '') {
        	$queryFive = $scope.ignoreRegi;
        }

        var resprom = $http.get(Data.serviceBase() + '/admin/allNorthStarOrganizations', {params: {"page": $scope.pageNumber, "query1": $queryOne, "query2": $queryTwo, 'query3': $queryThree, 'query4': $queryFour, 'query5': $queryFive}});
        resprom.then(function (response) {
            //console.log(response.data.ubts);
            //console.log(response.data.count[0].count);
            $scope.northStarOrganizations = response.data.starOrganizations;
            $scope.totalPage = 1;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

        var resprom = $http.get(Data.serviceBase() + '/admin/allNorthStarUsers', {params: {"page": $scope.pageNumber, "query1": $queryOne, "query2": $queryTwo, 'query3': $queryThree, 'query4': $queryFour, 'query5': $queryFive}});
        resprom.then(function (response) {
            //console.log(response.data.ubts);
            //console.log(response.data.count[0].count);
            $scope.northStarUsers = response.data.starUsers;
            $scope.totalPage = 1;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

        var resprom = $http.get(Data.serviceBase() + '/admin/allNorthStarTransaction', {params: {"page": $scope.pageNumber, "query1": $queryOne, "query2": $queryTwo, 'query3': $queryThree, 'query4': $queryFour, 'query5': $queryFive}});
        resprom.then(function (response) {
            //console.log(response.data.ubts);
            //console.log(response.data.count[0].count);
            $scope.northStarTransactions = response.data.starTransactions;
            $scope.totalPage = 1;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

        var resprom = $http.get(Data.serviceBase() + '/admin/allKeyTranactionStars', {params: {"page": $scope.pageNumber, "query1": $queryOne, "query2": $queryTwo, 'query3': $queryThree, 'query4': $queryFour, 'query5': $queryFive}});
        resprom.then(function (response) {
            $scope.yesterdayAddLead = response.data.yesterdayAddLead;
            $scope.lastThirtyAddLead = response.data.lastThirtyAddLead;
            $scope.prevThirtyAddLead = response.data.prevThirtyAddLead;
            $scope.lastSixtyAddLead = response.data.lastSixtyAddLead;
            $scope.prevSixtyAddLead = response.data.prevSixtyAddLead;
            $scope.lastOneTweentyAddLead = response.data.lastOneTweentyAddLead;
            $scope.prevOneTweentyAddLead = response.data.prevOneTweentyAddLead;
            $scope.lastThreeSixtyFiveAddLead = response.data.lastThreeSixtyFiveAddLead;
            $scope.prevThreeSixtyFiveAddLead = response.data.prevThreeSixtyFiveAddLead;
            $scope.totalubts = parseInt(response.data.yesterdayAddLead[0].count);
            $scope.totalPage = 1;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });


    };


    $scope.ubtFetchDates = function (ftxt,etxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBTDate', {params: {"page": $scope.pageNumber, "query1": ftxt, "query2": etxt}});
        resprom.then(function (response) {
            console.log(response.data.ubts);
            console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.ubtFetchCountry = function (ctxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBTCountry', {params: {"page": $scope.pageNumber, "query": ctxt}});
        resprom.then(function (response) {
            console.log(response.data.ubts);
            console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.ubtFetchIndustryType = function (itxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBTIndustry', {params: {"page": $scope.pageNumber, "query": itxt}});
        resprom.then(function (response) {
            console.log(response.data.ubts);
            console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.ubtFetchAccountype = function (atxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBTAccount', {params: {"page": $scope.pageNumber, "query": atxt}});
        resprom.then(function (response) {
            console.log(response.data.ubts);
            console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.ubtFetchSource = function (stxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allUBTSource', {params: {"page": $scope.pageNumber, "query": stxt}});
        resprom.then(function (response) {
            console.log(response.data.ubts);
            console.log(response.data.count[0].count);
            $scope.ubts = response.data.ubts;
            $scope.totalubts = parseInt(response.data.count[0].count);
            if ($scope.totalubts <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalubts / 20) + 1;
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    

    $scope.pageChanged = function (newPage) {
        $scope.loader = true;
        // $scope.cardView = false;
        $scope.pageNumber = newPage;
        //$scope.pagination.current = newPage;
        //paginationService.setCurrentPage('paginationId', newPage);
        if ($scope.totalubts <= 20) {
            //console.log("Visitor Count" + $scope.totalOrgs);
            $scope.totalPage = 1;
        }
        if ($scope.totalubts > 20) {
            //console.log("Visitor Count 2 " + $scope.totalOrgs);

            if (($scope.totalubts / 20) > 1) {
                $scope.totalPage = Math.floor($scope.totalubts / 20);
                $scope.totalPage = $scope.totalPage + 1;
            }

        }
        
        if ($scope.prevpage !== newPage) {
            $scope.ubtFetch();
            $scope.ubtFetchDates();
            $scope.ubtFetchCountry();
            $scope.ubtFetchIndustryType();
            $scope.ubtFetchAccountype();
            $scope.ubtFetchSource();
            $scope.prevpage = newPage;
        }
    };

    $scope.reset = function() {
        $scope.orgDet = "";
        $scope.searchCountry = "";
        $scope.searchIndustryType = "";
        $scope.searchPlan = "";
        $scope.searchSource = "";
        $scope.fDate = "";
        window.location.href = Data.webAppBase() + 'account/ubtReport';
    };

});
// End North Star Report

// Trns
evtApp.controller('adminTrnsporgCtrl', function ($scope, $uibModal, Data, toaster, $http) {
    $scope.trns = {};
    $scope.pageNumber = 1;
    $scope.totalTrns = 0;
    $scope.totalPage = 1;
    $scope.trnsPerPage = 20;
    $scope.TrnsFetch = function (stxt) {
        var resprom = $http.get(Data.serviceBase() + '/admin/allTrns', {params: {"page": $scope.pageNumber, "query": stxt}});
        resprom.then(function (response) {
            console.log(response.data.trns);
            console.log(response.data.count[0].count);
            $scope.trns = response.data.trns;
            $scope.totalList = parseInt(response.data.count[0].count);
            if ($scope.totalList <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalList / 20) + 1;
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    
    //Editing Trns
    $scope.edittrns = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './admineditTrns',
            controller: 'admineditTrnsCtrl',
            size: 'lg',
            resolve: {
                trns: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.TrnsFetch();
            }
        });
    };

    //Add new trns
    $scope.addTrns = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './adminAddTrns',
            controller: 'admineditTrnsCtrl',
            size: 'lg',
            resolve: {
                trns: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.TrnsFetch();
            }
        });
    };

    //Delete trns
    $scope.deleteadminTrns = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './deleteTrns',
            controller: 'admineditTrnsCtrl',
            size: 'lg',
            resolve: {
                trns: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.TrnsFetch();
            }
        });
    };

    //Download Trns
    $scope.trnsExport = function () {

        window.open(Data.webAppBase() + 'account/adminTrnsCSV', "_self");

    };

    $scope.pageChanged = function (newPage) {
        $scope.loader = true;
        // $scope.cardView = false;
        $scope.pageNumber = newPage;
        //$scope.pagination.current = newPage;
        //paginationService.setCurrentPage('paginationId', newPage);
        if ($scope.totalOrgs <= 20) {
            //console.log("Visitor Count" + $scope.totalOrgs);
            $scope.totalPage = 1;
        }
        if ($scope.totalOrgs > 20) {
            //console.log("Visitor Count 2 " + $scope.totalOrgs);

            if (($scope.totalOrgs / 20) > 1) {
                $scope.totalPage = Math.floor($scope.totalOrgs / 20);
                $scope.totalPage = $scope.totalPage + 1;
            }

        }
        /* if ($scope.totalOrgs > (Math.round($scope.totalOrgs / 20) * 20) && !$scope.fixlength) {
         $scope.totalPage = $scope.totalPage + 1;
         $scope.fixlength = true;
         }*/

        ////console.log("Current"+newPage);
        if ($scope.prevpage !== newPage) {
            $scope.TrnsFetch();
            $scope.prevpage = newPage;
        }
        //$scope.getResultsPage(newPage);
    };

});

// End Trns

evtApp.controller('admindisporgCtrl', function ($scope, $uibModal, Data, toaster, $http) {

    $scope.orgs = {};
	$scope.pageNumber = 1;
    $scope.totalVisitors = 0;
    $scope.totalPage = 1;
    $scope.usersPerPage = 20;
	$scope.OrgFetch = function (stxt) {
		var resprom = $http.get(Data.serviceBase() + '/admin/allorgs', {params: {"page": $scope.pageNumber,"query":stxt}});


		resprom.then(function (response) {
			console.log(response.data.count[0].count);
			$scope.orgs = response.data.orgs;
			$scope.totalOrgs = parseInt(response.data.count[0].count);
			if ($scope.totalOrgs <= 20) {
				$scope.totalPage = 1;
			} else {
				$scope.totalPage = Math.floor($scope.totalOrgs / 20) + 1;
			}

		}, function (response) {
			console.log('Error happened -- ');
			console.log(response);
		});
	};
    $scope.formatDate = function (date) {
        var dateOut = new Date(date);
        return dateOut;
    };
    //Editing old Organiztion
    $scope.editorg = function (p, size) {

        var modalInstance = $uibModal.open({
            templateUrl: './admineditOrg',
            controller: 'admineditorgCtrl',
            size: 'lg',
            resolve: {
                eorg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
               $scope.OrgFetch();
            }
        });


    };
    $scope.showvisitor_admin = function (org) {

        console.log("About to fetch visitors for event id " + event.id);

        sessionStorage.setItem('curOrgId', org.id);

        window.location.href = Data.webAppBase() + 'account/adminVisitors';
    };
	 //Breakbysource
    
    $scope.breakbysourceorg = function (p, size) {

        var modalInstance2 = $uibModal.open({
            templateUrl: './breakbysourceorg',
            controller: 'breakbysourceCtrl',
            size: 'lg',
            resolve: {
                eorg: function () {
                    return p;
                }

            }
        });
        modalInstance2.result.then(function (i) {
            if (i) {
               
            }
        });


    };
	
	 //Editing old organization using three dot button Begin
     $scope.paymentorg = function (p, size) {

        var modalInstance = $uibModal.open({
            templateUrl: './paymentorg',
            controller: 'admineditorgCtrl',
            size: 'lg',
            resolve: {
                eorg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                //$scope.OrgFetch();
            }
        });


    };
	
	//Edit old Organization using three dot button End
	
	 //Paymenthistory for Organization Begin
    
    $scope.paymenthistoryorg = function (p, size) {

       var modalInstance = $uibModal.open({
            templateUrl: './paymenthistoryorg',
            controller: 'paymenthistoryorgCtrl',
            size: 'lg',
            resolve: {
                eorg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                
            }
        });
    };
	
	$scope.pageChanged = function (newPage) {
        $scope.loader = true;
       // $scope.cardView = false;
        $scope.pageNumber = newPage;
        //$scope.pagination.current = newPage;
        //paginationService.setCurrentPage('paginationId', newPage);
        if ($scope.totalOrgs <= 20) {
            //console.log("Visitor Count" + $scope.totalOrgs);
            $scope.totalPage = 1;
        }
        if ($scope.totalOrgs > 20) {
            //console.log("Visitor Count 2 " + $scope.totalOrgs);

            if (($scope.totalOrgs / 20) > 1) {
                $scope.totalPage = Math.floor($scope.totalOrgs / 20);
                $scope.totalPage = $scope.totalPage + 1;
            }

        }
        /* if ($scope.totalOrgs > (Math.round($scope.totalOrgs / 20) * 20) && !$scope.fixlength) {
         $scope.totalPage = $scope.totalPage + 1;
         $scope.fixlength = true;
         }*/

        ////console.log("Current"+newPage);
        if ($scope.prevpage !== newPage) {
            $scope.OrgFetch();
            $scope.prevpage = newPage;
        }
        //$scope.getResultsPage(newPage);
    };
    
    //Paymenthistory for Organization End
	 $scope.orgUserList = function(p){
         var modalInstance = $uibModal.open({
            templateUrl: './adminUserList',
            controller: 'orgUserListCtrl',
            size: 'lg',
            resolve: {
                eorg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

            }
        });
    };
});

//Detailed Report from Admin

evtApp.controller('dispOrgReport',function($scope,$uibModal,Data,toaster,$http,DTOptionsBuilder,DTColumnBuilder){
    $scope.orgDets = {};
	 $scope.pageNumber = 1;
    $scope.totalVisitors = 0;
    $scope.totalPage = 1;
    $scope.usersPerPage = 20;
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('<"html5buttons"B>fTgtpi')
            .withButtons([
                /* {extend: 'copy', exportOptions: {
                 columns: [0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12]
                 }},*/
                {extend: 'csv', text: '<i class="fa fa-download"></i> Download as CSV', title: 'HLZ- Leads', exportOptions: {
                         columns: [0,1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16,17,18,19,20,21,22,23,24,25,26,27]
                    }},
                {extend: 'excel', text: '<i class="fa fa-download"></i> Download as Excel', customize: function () {
                        
                    }, title: 'HLZ- Leads', exportOptions: {
                        columns: [0,1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16,17,18,19,20,21,21,22,23,24,25,26,27]
                    }},
                /*  {extend: 'pdf', title: 'HL-Visitors-' + $scope.currentEventName, exportOptions: {
                 columns: [0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16,17,18]
                 }},
                 {extend: 'print',
                 customize: function (win) {
                 $(win.document.body).addClass('white-bg');
                 $(win.document.body).css('font-size', '10px');
                 
                 $(win.document.body).find('table')
                 .addClass('compact')
                 .css('font-size', 'inherit');
                 },
                 exportOptions: {
                 columns: [0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16,17,18]
                 }
                 }*/
            ])
            .withPaginationType('full_numbers')
            .withDisplayLength(10);
    $scope.orgFetch = function(stxt){
    var resprom = $http.get(Data.serviceBase() + '/admin/allorgsDetail', {params: {"page": $scope.pageNumber,"query":stxt}});


    resprom.then(function (response) {
      console.log(response.data);
        $scope.orgDets = response.data.orgs;
        $scope.totalOrgs = parseInt(response.data.count[0].count);
            if ($scope.totalOrgs <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalOrgs / 20) + 1;
            }

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
	};
    //Editing old Organiztion
    $scope.editreportorg = function (p, size) {

        var modalInstance = $uibModal.open({
            templateUrl: './editReportOrg',
            controller: 'admineditReportorgCtrl',
            size: 'lg',
            resolve: {
                eorg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.orgFetch();
            }
        });


    };
 
    $scope.pageChanged = function (newPage) {
        $scope.loader = true;
       // $scope.cardView = false;
        $scope.pageNumber = newPage;
        //$scope.pagination.current = newPage;
        //paginationService.setCurrentPage('paginationId', newPage);
        if ($scope.totalOrgs <= 20) {
            //console.log("Visitor Count" + $scope.totalOrgs);
            $scope.totalPage = 1;
        }
        if ($scope.totalOrgs > 20) {
            //console.log("Visitor Count 2 " + $scope.totalOrgs);

            if (($scope.totalOrgs / 20) > 1) {
                $scope.totalPage = Math.floor($scope.totalOrgs / 20);
                $scope.totalPage = $scope.totalPage + 1;
            }

        }
        /* if ($scope.totalOrgs > (Math.round($scope.totalOrgs / 20) * 20) && !$scope.fixlength) {
         $scope.totalPage = $scope.totalPage + 1;
         $scope.fixlength = true;
         }*/

        ////console.log("Current"+newPage);
        if ($scope.prevpage !== newPage) {
            $scope.orgFetch("");
            $scope.prevpage = newPage;
        }
        //$scope.getResultsPage(newPage);
    };

$scope.requestExport = function(){
    
   window.open(Data.webAppBase() + 'account/adminFilterCSVVisitor', "_self");
    
};
});
 
//UserList for Org

evtApp.controller('orgUserListCtrl', function ($scope, Data, $uibModalInstance, eorg, toaster, $http) {
    var original = angular.copy($scope.eorg);
    $scope.eorg = angular.copy(eorg);
    if ($scope.eorg.id !== undefined) {
        $scope.title = "Users";

    }
     $scope.users= {};
    var resprom1 = $http.get(Data.serviceBase() + '/user/userByAdminOrganization', {params: {"organizationId": $scope.eorg.id}});


    resprom1.then(function (response) {
        console.log(response.data);
        $scope.users = response.data;

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });



    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };



});
//Break by source controller Begin

evtApp.controller('breakbysourceCtrl', function ($scope, Data, $uibModalInstance, eorg, toaster,$http) {
    var original = angular.copy($scope.eorg);
    $scope.eorg = angular.copy(eorg);
    if ($scope.eorg.id !== undefined) {
        $scope.title = "Break by source";
       
    }
    var resprom1 = $http.get(Data.serviceBase() + '/organization/breakbysource', {params: {"orgid": $scope.eorg.id }});


    resprom1.then(function (response) {
        console.log(response.data);
        $scope.breackbysource = response.data;

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
   
   

 $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
   


});

//Break by source controller End

//Payment History Org control Begin

evtApp.controller('paymenthistoryorgCtrl', function ($scope, Data, $uibModalInstance, eorg, toaster,$http) {
    var original = angular.copy($scope.eorg);
    $scope.eorg = angular.copy(eorg);
    if ($scope.eorg.id !== undefined) {
        $scope.title = "Payment History";
        $scope.btntxt = "Update";
    } 
	
	
	var resprom2 = $http.get(Data.serviceBase() + '/paymenthistory/paymentadminhistory', {params: {"organizationId": $scope.eorg.id }});


    resprom2.then(function (response) {
        console.log(response.data[0]);
        $scope.paymentHistories = response.data;

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
   
  


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});


//Payment History Org control End

evtApp.controller('dispadminvisitorCtrl', function ($scope, $uibModal, Data, toaster, $http) {

    console.log("Welcome");
    $scope.visitors = {};
    $scope.orgId = sessionStorage.getItem('curOrgId');
    var resprom = $http.get(Data.serviceBase() + '/admin/getVisitorbyOrganization', {params: {"orgId": $scope.orgId}});


    resprom.then(function (response) {
        console.log(response.data);
        $scope.visitors = response.data;

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
    $scope.vBcard = {};
    $scope.valueAdd = {};
    $scope.editvisitor = function (p, size) {
        var visitorBcard = $http.get(Data.serviceBase() + 'admin/getvisitorBcard', {params: {"visitorId": p.id}});
        visitorBcard.then(function (response) {
            $scope.vBcard = response.data[0];
            var valueAdd = $http.get(Data.serviceBase() + 'admin/getvisitorValueAdds', {params: {"visitorId": p.id}});
            valueAdd.then(function (response) {
                $scope.valueAdd = response.data[0];
                var modalInstance = $uibModal.open({
                    templateUrl: './adminmodifyVisitor',
                    controller: 'editvisitoradminCtrl',
                    size: 'lg',
                    resolve: {
                        evisitor: function () {
                            return p;
                        },
                        bcard: function () {
                            return $scope.vBcard;
                        },
                        valueAdd: function () {
                            return $scope.valueAdd;
                        },
                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        $scope.refresh();
                    }
                });
            }, function (response) {
                console.log('Error occured -- ');
                console.log(response);
            });
        }, function (response) {
            console.log('Error occured -- ');
            console.log(response);
        });

    }


});
evtApp.controller('editvisitoradminCtrl', function ($scope, Data, $uibModalInstance, evisitor, bcard, valueAdd, toaster, $http, fileReader, $rootScope, $window) {
    $scope.visitor = evisitor;
    $scope.vBcard = bcard;
    $scope.valueAdd = valueAdd;

    $scope.switchTab = function (tabId) {
        if (tabId === 1) {
            $scope.tab = 1;
            angular.element(document.getElementById("tab-3")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
        } else {
            $scope.tab = 2;
            angular.element(document.getElementById("tab-4")).addClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
        }

    };
    $scope.Continueadminnext = function () {
        $scope.tab = 2;
        angular.element(document.getElementById("tab-4")).addClass("active");
        angular.element(document.getElementById("tab-3")).removeClass("active");
        console.log($scope.tab);

    };

    $scope.adminrotate = function (angle) {
        $scope.angle = $scope.angle + angle;
        console.log($scope.angle);
    };


    $scope.getFile = function () {
        $scope.progress = 0;
        fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    console.log($scope.file);

                    $scope.imageSrc = result;
                });
    };
	 $scope.countrymobDet = {};
    $scope.getCountryCodeData = function () {
        $http.get('../../../css/phone.json').success(function (data) {
            $scope.countrymobDet = data;
            if (($scope.evisitor.mobileCode === null || $scope.evisitor.mobileCode === undefined || $scope.evisitor.mobileCode === '') && ($scope.orgPhoneCode === null || $scope.orgPhoneCode === '' || $scope.orgPhoneCode === undefined))
            {
                console.log($scope.countrymobDet);
                var country = sessionStorage.getItem('orgCountry');
                angular.forEach($scope.countrymobDet, function (value, key) {
                    console.log("First " + value.calling_code);
                    if (value.calling_code === $scope.orgPhoneCode) {
                        console.log(value.calling_code);
                        $scope.orgPhoneCode = value.calling_code;
                        $scope.evisitor.mobileCode = value.calling_code;
                    } else if (value.country === country) {
                        $scope.orgPhoneCode = value.calling_code;
                        $scope.evisitor.mobileCode = value.calling_code;
                    }
                });
            }
        });
    };

    $scope.updatevisitorvalue = function (visitor, visitorvalue) {
        if (visitor.id !== undefined) {
            visitor.userType = $scope.userType;
            Data.put('admin/visitorupdate', visitor).then(function (result) {
                if (result.status !== 'error') {

                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    console.log(result);
                }
            });
        }


        if (visitorvalue.id !== undefined) {
            visitorvalue.visitorId = $scope.visitor.id;
            visitorvalue.photourl = $scope.imageSrc;

            Data.put('admin/visitorvalueadd', visitorvalue).then(function (result) {
                if (result.status !== 'error') {

                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                    $uibModalInstance.close(1);

                } else {
                    console.log(result);
                }
            });
        }
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('admineditorgCtrl', function ($scope, Data, $uibModalInstance,$filter, eorg, toaster) {
    var original = angular.copy($scope.eorg);
    $scope.eorg = angular.copy(eorg);
    if ($scope.eorg.id !== undefined) {
        $scope.title = "Update Organization";
        $scope.btntxt = "Update";
    } else {
        $scope.title = "Add New Organization";
        $scope.btntxt = "Add";
    }
     
    $scope.canadminSaveOrg = function () {
        return $scope.editorgform.$valid && !angular.equals($scope.eorg, original);
    };
    $scope.updateadminorg = function (org) {

        if (org.id !== undefined) {
            console.log(org);
            console.log(org.status);
            
            $scope.pdate=org.paymentDate;
            org.paymentDate = $filter('date')($scope.pdate, "yyyy-MM-dd HH:mm:ss");
            console.log(org.paymentDate);
            Data.put('admin/organization', org).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    //$uibModalInstance.dismiss('Close');
                    console.log('Setting the organization ')
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        } else {

            if (org.name !== undefined) {

                Data.post('organization/organization', org).then(function (result) {
                    if (result.status !== 'error') {

                        $uibModalInstance.close(1);
                        console.log('Setting the organization ')
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');


                    } else {
                        console.log(result);
                    }
                });

            }



        }

    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});
evtApp.controller('admineditReportorgCtrl', function ($scope, Data, $uibModalInstance, $filter, eorg, toaster) {
    var original = angular.copy($scope.eorg);
    $scope.eorg = angular.copy(eorg);
    if ($scope.eorg.id !== undefined) {
        $scope.title = "Update Organization";
        $scope.btntxt = "Update";
    } else {
        $scope.title = "Add New Organization";
        $scope.btntxt = "Add";
    }

    $scope.canadminSaveOrg = function () {
        return $scope.editorgform.$valid && !angular.equals($scope.eorg, original);
    };
    $scope.updateadminorg = function (org) {

        if (org.id !== undefined) {
            console.log(org);
            console.log(org.status);

            $scope.pdate = org.paymentDate;
            org.paymentDate = $filter('date')($scope.pdate, "yyyy-MM-dd HH:mm:ss");
            console.log(org.paymentDate);
            Data.put('admin/organization', org).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    //$uibModalInstance.dismiss('Close');
                    console.log('Setting the organization ')
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        } else {

            if (org.name !== undefined) {

                Data.post('organization/organization', org).then(function (result) {
                    if (result.status !== 'error') {

                        $uibModalInstance.close(1);
                        console.log('Setting the organization ')
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');


                    } else {
                        console.log(result);
                    }
                });

            }



        }

    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});
evtApp.controller('orgAccountTrend', function ($scope,$http, Data, toaster) {

    $scope.frTAccount = {};
    $scope.frBAccount = {};
    $scope.tTAccount = {};
    $scope.tBAccount = {};
    $scope.prTAccount = {};
    $scope.prBAccount = {};
    $scope.tab = 1;
	$scope.noOfDays = '7';
    angular.element(document.getElementById("ttab-1")).addClass("active");
    angular.element(document.getElementById("ttab-2")).removeClass("active");
    angular.element(document.getElementById("ttab-3")).removeClass("active");
    $scope.trendswitchTab = function (tabId) {
        
        if (tabId === 1) {
            $scope.tab = 1;
            angular.element(document.getElementById("ttab-1")).addClass("active");
            angular.element(document.getElementById("ttab-2")).removeClass("active");
            angular.element(document.getElementById("ttab-3")).removeClass("active");
            $scope.orgFRTrend($scope.noOfDays);
        } else if (tabId === 2) {
            $scope.tab = 2;
            angular.element(document.getElementById("ttab-1")).removeClass("active");
            angular.element(document.getElementById("ttab-2")).addClass("active");
            angular.element(document.getElementById("ttab-3")).removeClass("active");
            $scope.orgTrialTrend($scope.noOfDays);
        } else {
            $scope.tab = 3;
            angular.element(document.getElementById("ttab-1")).removeClass("active");
            angular.element(document.getElementById("ttab-2")).removeClass("active");
            angular.element(document.getElementById("ttab-3")).addClass("active");
            $scope.orgPRTrend($scope.noOfDays);
        }

    };

    $scope.orgFRTrend = function (noOfDays) {

        var resprom2 = $http.get(Data.serviceBase() + '/admin/FRTrend',{params:{"days":noOfDays}});


        resprom2.then(function (response) {
            console.log(response.data);
            $scope.orgFRTrends = response.data;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.orgTrialTrend = function (noOfDays) {
        var resprom2 = $http.get(Data.serviceBase() + '/admin/trailTrend', {params:{"days":noOfDays}});


        resprom2.then(function (response) {
            console.log(response.data[0]);
            $scope.orgTrialTrends = response.data;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.orgPRTrend = function (noOfDays) {
        var resprom2 = $http.get(Data.serviceBase() + '/admin/preTrend', {params:{"days":noOfDays}});


        resprom2.then(function (response) {
            console.log(response.data[0]);
            $scope.orgPreTrends = response.data;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };


});
// Web whats new configuration
evtApp.controller('whatsdisporgCtrl', function ($scope, $uibModal, Data, toaster, $http, DTOptionsBuilder, DTColumnBuilder) {
    $scope.updates = {};
    $scope.update = {};
    $scope.getAllUpdates = function () {
        var resprom = $http.get(Data.serviceBase() + '/admin/whatsNew', {params: {}});


        resprom.then(function (response) {
            console.log(response.data);
            $scope.updates = response.data;


        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.addnewUpdate = function (update) {
        if (update.id === undefined) {
            Data.post('admin/whatsNew', update).then(function (result) {
                if (result.status !== 'error') {


                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    $scope.getAllUpdates();
                    $scope.update = {};

                } else {
                    console.log(result);
                }
            });
        } else {
            Data.put('admin/whatsNew', update).then(function (result) {
                if (result.status !== 'error') {


                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    $scope.getAllUpdates();
                    $scope.update = {};

                } else {
                    console.log(result);
                }
            });
        }

    };
    $scope.uUpdate = function (update) {
        $scope.update = update;
    };
    $scope.deleteUpdate = function (update) {
        var resprom = $http.get(Data.serviceBase() + '/admin/whatsNewDelete', {params: {"id": update.id}});


        resprom.then(function (response) {

            toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            $scope.getAllUpdates();

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    };
});

// Device whats new configurations
evtApp.controller('devwhatorgCtrl', function ($scope, $uibModal, Data, toaster, $http, DTOptionsBuilder, DTColumnBuilder) {
    $scope.updates = {};
    $scope.update = {};
    $scope.actionTxt = 'Add';
    $scope.getAllUpdates = function () {
        var resprom = $http.get(Data.serviceBase() + '/admin/devwhatsNew', {params: {}});
        resprom.then(function (response) {
            console.log(response.data);
            $scope.updates = response.data;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.addnewUpdate = function (update) {
        console.log(update.id);
        if (update.id === undefined) {
            Data.post('admin/devwhatsNew', update).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    $scope.getAllUpdates();
                    $scope.update = {};
                } else {
                    console.log(result);
                }
            });
        } else {
            Data.put('admin/devwhatsNew', update).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    $scope.getAllUpdates();
                    $scope.update = {};
                } else {
                    console.log(result);
                }
            });
        }

    };
    $scope.uUpdate = function (update) {
        $scope.update = update;
        $scope.actionTxt = 'Update';
    };
    $scope.deleteUpdate = function (update) {
        var resprom = $http.get(Data.serviceBase() + '/admin/devwhatsNewDelete', {params: {"id": update.id}});


        resprom.then(function (response) {

            toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            $scope.getAllUpdates();

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    };
});


