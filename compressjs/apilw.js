/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
evtApp.controller('lwuserapiCtrl', function ($scope, $timeout, Data, $uibModal, toaster, $http, $rootScope, $window) {
    $scope.user = {};
    $scope.lwlogin = function (user) {

        if (user["username"] !== '') {

            Data.post('account/lwapilogin', user).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.firstName + "Logged in Scuccesfully", 10000, 'trustedHtml');
                    sessionStorage.setItem("userId", result.id);
                    sessionStorage.setItem("firstName", result.firstName);
                    sessionStorage.setItem("lastName", result.lastName);
                    sessionStorage.setItem("token", result.token);
                    sessionStorage.setItem("userType", result.userType);
                    window.location.href = Data.webAppBase() + 'account/lwapivisitor';

                }
            });
        }
    };

    $rootScope.lwlogout = function () {
        console.log('About to add a logout user......');
        Data.get('account/lwapilogout').then(function (results) {
            Data.toast(results);
            sessionStorage.removeItem("userId", '');
            sessionStorage.removeItem("firstName", '');
            sessionStorage.removeItem("lastName", '');
            sessionStorage.removeItem("token", '');
            sessionStorage.removeItem("userEmail", "");
            sessionStorage.removeItem("cFirstName", '');
            sessionStorage.removeItem("cLastName", '');
            sessionStorage.removeItem("userType", '');
            sessionStorage.removeItem("menuact", '');
            window.location.href = Data.webAppBase() + 'account/lwapivisitor';
        });
    };




});
evtApp.controller('vcardapiCtrl', function ($scope, $timeout, Data, $uibModal, toaster, $http, fileReader, $rootScope, $window) {

    $scope.visitorvalue = {};
    $scope.visitor = {};
    $rootScope.totstats = '0';
    $rootScope.totnewstats = '0';
    $scope.valueflag = true;
    $scope.btntxt = "";
    $scope.imageSrc = '';
    $rootScope.firstName = sessionStorage.getItem("firstName");
    $rootScope.lastName = sessionStorage.getItem("lastName");
    $rootScope.type = sessionStorage.getItem('userType');
    $rootScope.level = sessionStorage.getItem('userLevel');
    var chaeckStatus = function () {
       /* if (sessionStorage.getItem('userType') === "regular") {
            // Based on five minutes once request will be sent and find the count
            console.log("Regular");
            Data.get('visitor/vcardqueuestatsdetNew').then(function (result) {
                $rootScope.totnewstats = result.total;
            });
        } else {*/
            console.log("QC");
            Data.get('BusinesscardAPI/vcardapiqueuestatsdetNew').then(function (result) {
                // console.log(result);
                $rootScope.totstats = result.total;
            });
      //  }
        $timeout(chaeckStatus, 100000);
    };


    $rootScope.makeRefresh = function () {
        $window.location.reload();
    };
    chaeckStatus();
    $scope.switchTab = function (tabId) {
        if (tabId === 1) {
            $scope.tab = 1;
            angular.element(document.getElementById("tab-3")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
        } else if (tabId === 2) {
            $scope.tab = 2;
            angular.element(document.getElementById("tab-4")).addClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
        } else {
            $scope.tab = 3;
            angular.element(document.getElementById("tab-5")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
        }

    };

    $scope.verifyEmail = function (visitor) {

        $scope.verifydata = {
            "orgId": visitor.organizationId,
            "emailId": visitor.email,
            "visitorId": visitor.id
        };

        if (visitor.organizationId && visitor.email && visitor.id) {
            Data.post('BusinesscardAPI/verifyAPIEmail', $scope.verifydata).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    //$window.location.reload();
                    //$scope.visitor = {};
                   /* $scope.visitorvalue = {};
                    $scope.visitorvalue.leadnotes = "";
                    $scope.visitorvalue.orgnotes = "";
                    $scope.visitorvalue.linkedin = "";
                    $scope.visitorvalue.url1 = "";
                    $scope.visitorvalue.url2 = "";
                    $scope.visitorvalue.url3 = "";
                    console.log(result.visitorValAdd['leadnotes']);*/
                    $scope.visitor.firstName = result.visitor.firstName;
                    $scope.visitor.lastName = result.visitor.lastName;
                    $scope.visitor.visitorOrganizationName = result.visitor.visitorOrganizationName;
                    $scope.visitor.designation = result.visitor.designation;
                    $scope.visitor.email = result.visitor.email;
                    $scope.visitor.alternateEmail = result.visitor.alternateEmail;
                    $scope.visitor.mobile = result.visitor.mobile;
                    $scope.visitor.website = result.visitor.website;
                    $scope.visitor.phone = result.visitor.phone;
                    $scope.visitor.alternatePhone = result.visitor.alternatePhone;
                    $scope.visitor.fax = result.visitor.fax;
                    $scope.visitor.address1 = result.visitor.address1;
                    $scope.visitor.address2 = result.visitor.address2;
                    //$scope.visitor.photourl = result.visitor.photourl;
                    $scope.visitor.city = result.visitor.city;
                    $scope.visitor.state = result.visitor.state;
                    $scope.visitor.country = result.visitor.country;
                    $scope.visitor.zip = result.visitor.zip;
                   // $scope.visitorvalue.leadnotes = result.visitorValAdd['leadnotes'];
                   // $scope.visitorvalue.orgnotes = result.visitorValAdd['orgnotes'];
                   // $scope.visitorvalue.linkedin = result.visitorValAdd['linkedin'];
                  //  $scope.visitorvalue.url1 = result.visitorValAdd['url1'];
                  //  $scope.visitorvalue.url2 = result.visitorValAdd['url2'];
                  //  $scope.visitorvalue.url3 = result.visitorValAdd['url3'];

                } else {
                    console.log(result);
                }
            });
        }
    };
    $scope.skipemail = function () {

        $scope.skipdata = {
            "userId": $scope.visitor.capturedUserId,
            "visitorId": $scope.visitor.id
        };

        Data.post('BusinesscardAPI/skipEmail', $scope.skipdata).then(function (result) {
            if (result.status !== 'error') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                $window.location.reload();
            } else {
                console.log(result);
            }
        });
    };

    $scope.rejectemail = function () {

        $scope.rejectdata = {
             "visitorId": $scope.visitor.id
        };

        Data.post('BusinesscardAPI/vcardapirejectionmail', $scope.rejectdata).then(function (result) {
            if (result.status !== 'error') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                $window.location.reload();
            } else {
                console.log(result);
            }
        });
    };

    $scope.getFile = function () {
        $scope.progress = 0;
        fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    console.log($scope.file);

                    $scope.imageSrc = result;
                });
    };


    var userId = sessionStorage.getItem('userId');
    $scope.userType = sessionStorage.getItem('userType');
    var businesscardurl = "";


    //  if (sessionStorage.getItem('userType') === "regular") {
    //       businesscardurl = "BusinesscardAPI/bcardvisitordetail";
    //  } else {
    businesscardurl = "BusinesscardAPI/bcardVisitorQcDetail";
    //   }

    var resprom = $http.get(Data.serviceBase() + businesscardurl, {params: {'userId': userId}});
    resprom.then(function (response) {
        /*  if (sessionStorage.getItem('userType') === "regular") {
         
         console.log(response);
         $scope.visitor = response.data;
         console.log('got visitor data to transcribe ');
         console.log($scope.visitor);
         $scope.btntxt = "Save";
         if ($scope.visitor.length <= 0) {
         toaster.pop("error", "", "No more business cards are available", 10000, 'trustedHtml');
         }
         } else {*/
        $scope.btntxt = "Verified";
        $scope.visitor = response.data.visitors;
        $scope.visitorvalue = response.data.visitorsvalue[0];
        if ($scope.visitor.length <= 0) {
            toaster.pop("error", "", "No more business cards are available", 10000, 'trustedHtml');
        }
        // if($rootScope.totstats > 0 && response.data.visitors.length <=0 ){
        //     toaster.pop("error", "", "Business card are still in DC Queue", 10000, 'trustedHtml');
        // }

        //  }
        if ($scope.visitor['valueAddInfo'] === '1') {
            $scope.valueflag = true;
        } else {
            $scope.valueflag = false;
        }

    }, function (response) {
        console.log('Error occured -- ');
        console.log(response);
    });
    $scope.angle = 0;

    $scope.rotate = function (angle) {
        $scope.angle = $scope.angle + angle;
        console.log($scope.angle);
    };


    $scope.tab = 1;
    angular.element(document.getElementById("tab-3")).addClass("active");
    angular.element(document.getElementById("tab-4")).removeClass("active");
    angular.element(document.getElementById("tab-5")).removeClass("active");
    $scope.Continuetonext = function () {
        $scope.tab = 2;
        angular.element(document.getElementById("tab-4")).addClass("active");
        angular.element(document.getElementById("tab-3")).removeClass("active");
        angular.element(document.getElementById("tab-5")).removeClass("active");
        console.log($scope.tab);

    };
    $scope.Continuetonext1 = function () {
        $scope.tab = 3;
        angular.element(document.getElementById("tab-5")).addClass("active");
        angular.element(document.getElementById("tab-3")).removeClass("active");
        angular.element(document.getElementById("tab-4")).removeClass("active");
        console.log($scope.tab);

    };



    $scope.confvisitor = function (p, q, size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './confvisitor',
            controller: 'confapivisitorCtrl',
            size: size,
            resolve: {
                visitor: function () {
                    return p;
                },
                visitorvalue: function () {
                    return q;
                },
                imgsrc: function () {
                    return $scope.imageSrc;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            $window.location.reload();
        });
    };
    $rootScope.vcardqueuestats = function (size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './vcardqueuestats',
            controller: 'vcardapiqueuestatsCtrl',
            size: 'lg',
            resolve: {
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };
});
evtApp.controller('confapivisitorCtrl', function ($scope, Data, $uibModalInstance, visitor, visitorvalue, imgsrc, toaster) {
    $scope.visitor = visitor;
    $scope.imageSrc = imgsrc;
    $scope.visitorvalue = visitorvalue;
    $scope.title = "Confirmation";
    $scope.userType = "";
  /*  if (sessionStorage.getItem('userType') === "regular") {
        $scope.userType = "DC";

    } else {
*/
        $scope.userType = "QC";
 //   }
    $scope.updatevisitorvalue = function (visitor, visitorvalue) {
        //console.log(visitorvalue);
        if (visitor.id !== undefined) {
            visitor.userType = $scope.userType;

            if ($scope.imageSrc) {
                visitor.profilephotourl = $scope.imageSrc;
            }
            //if (visitorvalue !== undefined) {
               /* visitor.leadnotes = visitorvalue.leadnotes;
                visitor.orgnotes = visitorvalue.orgnotes;
                visitor.linkedin = visitorvalue.linkedin;
                visitor.url1 = visitorvalue.url1;
                visitor.url2 = visitorvalue.url2;
                visitor.url3 = visitorvalue.url3;*/
                visitor.bcardstatus = $scope.userType;
           // }
            Data.put('BusinesscardAPI/lwvisitor', visitor).then(function (result) {
                if (result.status !== 'error') {

                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    $uibModalInstance.close(1);
                } else {
                    console.log(result);
                }
            });
        }
    };


  
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

evtApp.controller('vcardapiqueuestatsCtrl', function ($scope, Data, $uibModalInstance, toaster) {

    $scope.title = "Visitor Card Queue stats";
    $scope.stats = {};
    Data.get('BusinesscardAPI/vcardapiqueuestatsdet').then(function (result) {
        $scope.stats = result;
        console.log(result);
    });
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});