
evtApp.controller('dispCustomFieldCtrl', function ($scope, $rootScope, alertUser, Data, trialPopUp, $uibModal, $http, toaster) {
    $rootScope.emptyfield = {};

    $scope.loader = true;
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }
    console.log("Organization Id is " + $scope.organizationId);
    $rootScope.addButtonhtml = '<button class="btn btn-primary btn-sm" ng-click="addField(emptyfield);"><span class="fa fa-plus"></span></button>&nbsp;';
    //$rootScope.addButtonhtml += '<button class="btn btn-default btn-sm btn-circle" popover="Interests are list of products or fields that your potential customers are interested in. Enter list of products or fields that you sell in an event, expo or other marketing situations.  Later for a lead, you can attach, from this list, products or fields she/he is interested in. For Apple, the products will be iPAD, iPhone, iMAC, MacBook etc."  popover-trigger="focus" popover-title="Interests (Products and Services)" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    $rootScope.title = "Custom Fields";
    if (sessionStorage.getItem("xtime") === "1") {
        $rootScope.firstProfile = true;
    } else {
        $rootScope.firstProfile = false;
    }
    var resprom = $http.get(Data.serviceBase() + '/field/field', {params: {key: "organizationId", "value": $scope.organizationId}});

    $scope.fields = {};
    resprom.then(function (response) {
        $scope.fields = response.data;
        $scope.loader = false;
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });

    sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';
    $rootScope.editField = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "editCustom"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance = $uibModal.open({
                    templateUrl: './modifyField',
                    controller: 'modifyFieldCtrl',
                    size: size,
                    backdrop: 'static',
                    resolve: {
                        field: function () {
                            return p;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/field/field', {params: {key: "organizationId", "value": $scope.organizationId}});

                        $scope.fields = {};
                        resprom.then(function (response) {
                            $scope.fields = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });

            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to manage custom fields. Only account owner or managers (within HelloLeads) can manage custom fields. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

    //Adding New Organiztion
    $rootScope.addField = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "addCustom"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance = $uibModal.open({
                    templateUrl: './modifyField',
                    controller: 'modifyFieldCtrl',
                    size: size,
                    backdrop: 'static',
                    resolve: {
                        field: function () {
                            return p;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/field/field', {params: {key: "organizationId", "value": $scope.organizationId}});

                        $scope.fields = {};
                        resprom.then(function (response) {
                            $scope.fields = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to manage custom fields. Only account owner or managers (within HelloLeads) can manage custom fields. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

    //Deleting an Organization 
    $scope.delField = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteCustom"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance1 = $uibModal.open({
                    templateUrl: './deleteField',
                    controller: 'delFieldCtrl',
                    size: size,
                    resolve: {
                        field: function () {
                            return p;
                        }
                    }
                });
                modalInstance1.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/field/field', {params: {key: "organizationId", "value": $scope.organizationId}});

                        $scope.fields = {};
                        resprom.then(function (response) {
                            $scope.fields = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to manage custom fields. Only account owner or managers (within HelloLeads) can manage custom fields. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };


});

evtApp.controller('modifyFieldCtrl', function ($scope, $http, Data, $uibModalInstance, field, toaster) {
    var original = angular.copy($scope.field);
    $scope.field = angular.copy(field);

    if ($scope.field.id !== undefined) {
        $scope.title = "Update Field";
        $scope.btntxt = "Update";
    } else {
        $scope.title = "Add New Field";
        $scope.btntxt = "Add";
    }
    var resprom = $http.get(Data.serviceBase() + '/field/values', {params: {"fieldId": $scope.field.id}});


    resprom.then(function (response) {
        console.log(response.data[0]);

        $scope.field.values = response.data[0]["fvalues"];


    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
    $scope.canSaveField = function () {
        return $scope.fieldform.$valid && !angular.equals($scope.field, original);
    };
    
    $scope.checkDate = function(){
    $scope.organizationId = sessionStorage.getItem('orgId');
    var resprom = $http.get(Data.serviceBase() + '/field/checkdate', {params: {"orgId": $scope.organizationId}});
     resprom.then(function (response) {
            $scope.dcount = response.data;         
            console.log($scope.dcount.length);
            if($scope.dcount.length < 1){
                $scope.field.datecount = '0';
            }else{
                $scope.field.datecount = '1';
            } 
            return $scope.field.datecount;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.updatefield = function (field) {

        console.log("Going to add or update field....");
        if (field.id !== undefined) {
            field.organizationId = sessionStorage.getItem('orgId');
            field.userId = sessionStorage.getItem('userId');
            console.log("its an update to the field....");
            Data.put('field/field', field).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    //$uibModalInstance.dismiss('Close');
                    console.log('Setting the field ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        } else {
            console.log("its an add to the field....");

            if (field.name !== undefined) {

                field.organizationId = sessionStorage.getItem('orgId');
                field.userId = sessionStorage.getItem('userId');
                Data.post('field/field', field).then(function (result) {
                    if (result.status !== 'error') {

                        $uibModalInstance.close(1);
                        console.log('Setting the field ');
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');


                    } else {
                        console.log(result);
                    }
                });

            }



        }

    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});
evtApp.controller('delFieldCtrl', function ($scope, Data, $uibModalInstance, field, toaster) {

    $scope.field = field;
    $scope.title = "Delete Field";
    $scope.deleteField = function (field) {

        if (field.name !== undefined) {

            Data.delete('field/field/' + field.id).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);

                    console.log('Setting the field ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };




});
evtApp.controller('dispTagsCtrl', function ($scope, warningPopup, trialPopUp, alertUser, $uibModal, $location, $rootScope, $http, Data, toaster) {
    $rootScope.emptyTag = {};

    $scope.loader = true;
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }
    console.log("Organization Id is " + $scope.organizationId);
    $rootScope.addButtonhtml = '';
    //$rootScope.addButtonhtml += '<button class="btn btn-default btn-sm btn-circle" popover="Interests are list of products or fields that your potential customers are interested in. Enter list of products or fields that you sell in an event, expo or other marketing situations.  Later for a lead, you can attach, from this list, products or fields she/he is interested in. For Apple, the products will be iPAD, iPhone, iMAC, MacBook etc."  popover-trigger="focus" popover-title="Interests (Products and Services)" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    $rootScope.title = "Qualifiers";
    if (sessionStorage.getItem("xtime") === "1") {
        $rootScope.firstProfile = true;
    } else {
        $rootScope.firstProfile = false;
    }
    $scope.getTags = function () {
        $scope.tags = {};
        var resprom = $http.get(Data.serviceBase() + '/field/tags', {params: {key: "organizationId", "value": $scope.organizationId}});


        resprom.then(function (response) {
            $scope.tags = response.data;
            $scope.loader = false;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';
    $rootScope.editTag = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "editCustom"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance = $uibModal.open({
                    templateUrl: './modifyTag',
                    controller: 'modifyTagCtrl',
                    size: size,
                    backdrop: 'static',
                    resolve: {
                        tag: function () {
                            return p;
                        },
                        tags: function () {
                            return $scope.tags;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/field/tags', {params: {key: "organizationId", "value": $scope.organizationId}});

                        $scope.tags = {};
                        resprom.then(function (response) {
                            $scope.tags = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });

            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to manage custom fields. Only account owner or managers (within HelloLeads) can manage custom fields. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
         } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    //Adding New Organiztion
    $rootScope.addTag = function (p, size) {
         var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "addCustom"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance = $uibModal.open({
                    templateUrl: './modifyTag',
                    controller: 'modifyTagCtrl',
                    size: size,
                    backdrop: 'static',
                    resolve: {
                        tag: function () {
                            return p;
                        },
                        tags: function () {
                            return $scope.tags;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/field/tags', {params: {key: "organizationId", "value": $scope.organizationId}});

                        $scope.tags = {};
                        resprom.then(function (response) {
                            $scope.tags = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to manage custom fields. Only account owner or managers (within HelloLeads) can manage custom fields. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
         } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    //Deleting an Tag 
    $scope.delTag = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteCustom"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance1 = $uibModal.open({
                    templateUrl: './deleteTag',
                    controller: 'delTagCtrl',
                    size: size,
                    resolve: {
                        tag: function () {
                            return p;
                        }
                    }
                });
                modalInstance1.result.then(function (i) {
                    if (i) {
                        var resprom = $http.get(Data.serviceBase() + '/field/tags', {params: {key: "organizationId", "value": $scope.organizationId}});

                        $scope.tags = {};
                        resprom.then(function (response) {
                            $scope.tags = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to manage custom fields. Only account owner or managers (within HelloLeads) can manage custom fields. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
         } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

});

evtApp.controller('modifyTagCtrl', function ($scope, $http, Data, $uibModalInstance, tag, tags, toaster) {
    var original = angular.copy($scope.tag);
    $scope.tag = angular.copy(tag);
    $scope.tags = tags;
    $scope.dupFlag = 0;
    if ($scope.tag.id === undefined) {
        $scope.title = "Add New Tag";
        $scope.btntxt = "Add";

    } else {
        $scope.title = "Update Tag";
        $scope.btntxt = "Update";
    }

    $scope.checkTags = function (name) {
        $scope.dupFlag = 0;
        angular.forEach($scope.tags, function (d) {

            if (d.tag.toLowerCase() === name.toLowerCase()) {
                $scope.dupFlag = 1;

            }


        });
        if ($scope.dupFlag === 1) {
            return true;
        } else {
            return false;
        }

    };
    $scope.canSaveTag = function () {
        if (($scope.tagform.$valid && !angular.equals($scope.tag, original))) {
            return true;

        } else {
            return false;
        }

    };
    $scope.updateTag = function (tag) {

        console.log("Going to add or update field....");
        if (tag.id !== undefined) {
            tag.organizationId = sessionStorage.getItem('orgId');
            tag.userId = sessionStorage.getItem('userId');
            console.log("its an update to the field....");
            Data.put('field/tags', tag).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    //$uibModalInstance.dismiss('Close');
                    console.log('Setting the field ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        } else {
            console.log("its an add to the field....");

            if (tag.tag !== undefined) {

                tag.organizationId = sessionStorage.getItem('orgId');
                tag.userId = sessionStorage.getItem('userId');
                Data.post('field/tags', tag).then(function (result) {
                    if (result.status !== 'error') {

                        $uibModalInstance.close(1);
                        console.log('Setting the tag ');
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');


                    } else {
                        console.log(result);
                    }
                });

            }



        }

    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});
evtApp.controller('delTagCtrl', function ($scope, Data, $uibModalInstance, tag, toaster) {

    $scope.tag = tag;
    $scope.title = "Delete Tag";
    $scope.deleteTag = function (tag) {

        if (tag.tag !== undefined) {

            Data.delete('field/tags/' + tag.id).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);

                    console.log('Setting the field ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };




});


evtApp.controller('configCtrl', function ($scope, warningPopup, $location, $rootScope, $http, Data, toaster) {
    sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';
    //$rootScope.title = "Enquiry Form Configuration";
	$rootScope.addButtonhtml = "";
    $scope.eventLists = {};
    $scope.eventtempSelection = {};
    $scope.formsFields = {};
    $scope.dispField = ['L', 'D', 'C', 'E', 'M', 'Cu', 'W', 'Di', 'Al', 'A1', 'A2', 'Ci', 'St', 'Z', 'P', 'CG', 'In', 'N'];
    $scope.mandField = ['', '', '', 'E', 'M', '', '', '', '', '', '', '', '', '', '', '', '', ''];
    $scope.dfieldSet = "E,F,";
    //$scope.dispField[0]="L";
    $scope.mfieldSet = "F,";
    $scope.enquirySubTitle = ['Basic Info', 'Contact Info', 'Address Info', 'My Business Needs', 'More About My Needs'];
    $scope.dispText = ['First Name', 'Last Name', 'Designation', 'Organization Name', 'Email', 'Mobile', 'Country', 'Website', 'Direct Number', 'Office Number', 'Address Line 1', 'Address Line 2', 'City', 'State', 'Zip', 'Purchase Time Line', 'Customer Group', 'Product Group', 'Notes'];
    $scope.enquiryTitle = "Digital Enquiry Form - " + sessionStorage.getItem('cOrgName');
    $scope.enquiryResponse = "Dear #LeadName#,\nThank you for your time.  We will review your business needs and get back to you at the earliest";
    $scope.organizationId = sessionStorage.getItem("orgId");
    $scope.selDispTxt = [];
	$scope.grouped ="";
	$rootScope.$on("callConfig", function () {
        console.log("Called")
        if (localStorage.getItem("eventXG")) {
            $scope.eventtempSelection = JSON.parse(localStorage.getItem("eventXG"));
            $scope.fieldCheck();
        }
    });
    /*var param = "";
    if ($location.absUrl().indexOf("?") > 0 && $location.absUrl().indexOf("event") > 0) {
        param = $location.absUrl().split("=");
        console.log(decodeURIComponent(param[1]));
    }*/

    $scope.resetForm = function () {
        var msg = {"message": "Are you sure you want to reset the form configuration ? <br>Changes made will be discarded.", "title": "Warning","canceltxt":"Cancel","oktxt":"Reset"};
        warningPopup.warningpopmodel(msg);
        $scope.$on('warnresp', function (event, args) {

            if (args.status === 'true') {
                $scope.dispField = ['L', 'D', 'C', 'E', 'M', 'Cu', 'W', 'Di', 'Al', 'A1', 'A2', 'Ci', 'St', 'Z', 'P', 'CG', 'In', 'N'];
                $scope.mandField = ['', '', '', 'E', 'M', '', '', '', '', '', '', '', '', '', '', '', '', ''];
                $scope.dfieldSet = "M,F,";
                //$scope.dispField[0]="L";
                $scope.mfieldSet = "F,";
                $scope.enquirySubTitle = ['Basic Info', 'Contact Info', 'Address Info', 'My Business Needs', 'More About My Needs'];
                $scope.dispText = ['First Name', 'Last Name', 'Designation', 'Organization Name', 'Email', 'Mobile', 'Country', 'Website', 'Direct Number', 'Office Number', 'Address Line 1', 'Address Line 2', 'City', 'State', 'Zip', 'Purchase Time Line', 'Customer Group', 'Product Group', 'Notes'];
                $scope.enquiryTitle = "Digital Enquiry Form - " + sessionStorage.getItem('cOrgName');
                $scope.enquiryResponse = "Dear #LeadName#,\nThank you for your time.  We will review your business needs and get back to you at the earliest";
                $scope.organizationId = sessionStorage.getItem("orgId");
                $scope.selDispTxt = [];
            }
        });

    };



    $scope.toggleSwitch = function (inputD) {
        if (inputD) {
            return false;
        } else {
            return true;
        }
    }

    //console.log($rootScope.title);
    /*$scope.getEventOrgList = function () {
        var eventresp = $http.get(Data.serviceBase() + '/event/orgEventList', {params: {"organizationId": $scope.organizationId}});
        eventresp.then(function (response) {
            // console.log(response.data);
            $scope.eventLists = response.data;
            angular.forEach(response.data, function (d) {
                //console.log(d.name);
                if (d.name === decodeURIComponent(param[1])) {
                    console.log(d);
                    $scope.eventtempSelection = d;
                }
            });
            if (param[1]) {
                $scope.fieldCheck();
            }
            $scope.loader = false;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };*/

    $scope.previewForm = function () {
        window.open(Data.webAppBase() + 'account/eventform/' + $scope.eventtempSelection.eventKey, "_blank");

    };
    $scope.fieldCheck = function () {
        //console.log($scope.eventtempSelection);
        var eventrespd = $http.get(Data.serviceBase() + '/event/formConfig', {params: {"eventId": $scope.eventtempSelection.id}});
        eventrespd.then(function (response) {
            //console.log(response.data);
            $scope.eventTemp = {};
            $scope.eventTemp = response.data[0];
            console.log($scope.eventTemp.eventId);
            if ($scope.eventTemp.eventId != undefined) {
                $scope.enquiryTitle = $scope.eventTemp.enquiry_title;
                $scope.enquirySubTitle = $scope.eventTemp.enquiry_subtitle.split(',');
                $scope.enquiryResponse = $scope.eventTemp.enquiry_response;
				$scope.grouped = $scope.eventTemp.grouped;
                var displayText = $scope.eventTemp.lableField.split(',');
               // $scope.selDispTxt[0] = displayText[0];
                $scope.dispText[0] = displayText[0];
                //$scope.selDispTxt[4] = displayText[4];
                // $scope.dispText[4] = displayText[4];
                // $scope.selDispTxt[5] = displayText[5];
                // $scope.dispText[5] = displayText[5];
            } else {
                //var displayText = $scope.dispText;
                $scope.selDispTxt = [];
                $scope.enquirySubTitle = ['Basic Info', 'Contact Info', 'Address Info', 'My Business Needs', 'More About My Needs'];
                $scope.dispText = ['First Name', 'Last Name', 'Designation', 'Organization Name', 'Email', 'Mobile', 'Country', 'Website', 'Direct Number', 'Office Number', 'Address Line 1', 'Address Line 2', 'City', 'State', 'Zip', 'Purchase Time Line', 'Customer Group', 'Product Group', 'Notes'];
                $scope.enquiryTitle = "Digital Enquiry Form - " + sessionStorage.getItem('cOrgName');
                $scope.enquiryResponse = "Dear #LeadName#,\nThank you for your time.  We will review your business needs and get back to you at the earliest";
            }
            $scope.loader = false;
            var keyIndex = 1;
            var displayFieldL = $scope.eventTemp.dispField.split(',');
            var mandFieldL = $scope.eventTemp.mandField.split(',');
			console.log(mandFieldL);
            if ($scope.eventTemp.dispField || $scope.eventTemp.mandField) {
                $scope.dispField = [];
                $scope.mandField = [];
               if (displayFieldL.indexOf('L') > -1) {
                    $scope.dispField[0] = 'L';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[1] = displayText[keyIndex];
                        console.log(displayText[keyIndex]);
                        $scope.dispText[1] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('D') > -1) {
                    $scope.dispField[1] = 'D';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[2] = displayText[keyIndex];
                        console.log(displayText[keyIndex]);
                        $scope.dispText[2] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('C') > -1) {
                    $scope.dispField[2] = 'C';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[3] = displayText[keyIndex];
                        console.log(displayText[keyIndex]);
                        $scope.dispText[3] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('E') > -1) {
                    $scope.dispField[3] = 'E';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[4] = displayText[keyIndex];
                        $scope.dispText[4] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('M') > -1) {
                    //$scope.dispField[3] = 'E';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[5] = displayText[keyIndex];
                        $scope.dispText[5] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('Cu') > -1) {
                    $scope.dispField[5] = 'Cu';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[6] = displayText[keyIndex];
                        $scope.dispText[6] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('W') > -1) {
                    $scope.dispField[6] = 'W';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[7] = displayText[keyIndex];
                        $scope.dispText[7] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('Di') > -1) {
                    $scope.dispField[7] = 'Di';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[8] = displayText[keyIndex];
                        $scope.dispText[8] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('Al') > -1) {
                    $scope.dispField[8] = 'Al';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[9] = displayText[keyIndex];
                        $scope.dispText[9] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('A1') > -1) {
                    $scope.dispField[9] = 'A1';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[10] = displayText[keyIndex];
                        $scope.dispText[10] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('A2') > -1) {
                    $scope.dispField[10] = 'A2';
                    if ($scope.eventTemp.eventId != undefined) {
                       // $scope.selDispTxt[11] = displayText[keyIndex];
                        $scope.dispText[11] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('Ci') > -1) {
                    $scope.dispField[11] = 'Ci';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[12] = displayText[keyIndex];
                        $scope.dispText[12] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('St') > -1) {
                    $scope.dispField[12] = 'St';
                    if ($scope.eventTemp.eventId != undefined) {
                       // $scope.selDispTxt[13] = displayText[keyIndex];
                        $scope.dispText[13] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('Z') > -1) {
                    $scope.dispField[13] = 'Z';
                    if ($scope.eventTemp.eventId != undefined) {
                       // $scope.selDispTxt[14] = displayText[keyIndex];
                        $scope.dispText[14] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('P') > -1) {
                    $scope.dispField[14] = 'P';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[15] = displayText[keyIndex];
                        $scope.dispText[15] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('CG') > -1) {
                    $scope.dispField[15] = 'CG';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[16] = displayText[keyIndex];
                        $scope.dispText[16] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('In') > -1) {
                    $scope.dispField[16] = 'In';
                    if ($scope.eventTemp.eventId != undefined) {
                        //$scope.selDispTxt[17] = displayText[keyIndex];
                        $scope.dispText[17] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (displayFieldL.indexOf('N') > -1) {
                    $scope.dispField[17] = 'N';
                    if ($scope.eventTemp.eventId != undefined) {
                       // $scope.selDispTxt[18] = displayText[keyIndex];
                        $scope.dispText[18] = displayText[keyIndex];
                        keyIndex = keyIndex + 1;
                    }
                }
                if (mandFieldL.indexOf('L') > -1) {
                    $scope.mandField[0] = 'L';
                    $scope.dispField[0] = 'L';

                }
                if (mandFieldL.indexOf('D') > -1) {
                    $scope.mandField[1] = 'D';
                    $scope.dispField[1] = 'D';

                }
                if (mandFieldL.indexOf('C') > -1) {
                    $scope.mandField[2] = 'C';
                    $scope.dispField[2] = 'C';

                }
                if (mandFieldL.indexOf('E') > -1) {
                    $scope.mandField[3] = 'E';
                    $scope.dispField[3] = 'E';

                }
                if (mandFieldL.indexOf('M') > -1) {
                    $scope.mandField[4] = 'M';
					 $scope.dispField[4] = 'M';
                }
                if (mandFieldL.indexOf('Cu') > -1) {
                    $scope.mandField[5] = 'Cu';
                    $scope.dispField[5] = 'Cu';
                }
                if (mandFieldL.indexOf('W') > -1) {
                    $scope.mandField[6] = 'W';
                    $scope.dispField[6] = 'W';
                }
                if (mandFieldL.indexOf('Di') > -1) {
                    $scope.mandField[7] = 'Di';
                    $scope.dispField[7] = 'Di';
                }
                if (mandFieldL.indexOf('Al') > -1) {
                    $scope.mandField[8] = 'Al';
                    $scope.dispField[8] = 'Al';
                }
                if (mandFieldL.indexOf('A1') > -1) {
                    $scope.mandField[9] = 'A1';
                    $scope.dispField[9] = 'A1';
                }
                if (mandFieldL.indexOf('A2') > -1) {
                    $scope.mandField[10] = 'A2';
                    $scope.dispField[10] = 'A2';
                }
                if (mandFieldL.indexOf('Ci') > -1) {
                    $scope.mandField[11] = 'Ci';
                    $scope.dispField[11] = 'Ci';
                }
                if (mandFieldL.indexOf('St') > -1) {
                    $scope.mandField[12] = 'St';
                    $scope.dispField[12] = 'St';
                }
                if (mandFieldL.indexOf('Z') > -1) {
                    $scope.mandField[13] = 'Z';
                    $scope.dispField[13] = 'Z';
                }
                if (mandFieldL.indexOf('P') > -1) {
                    $scope.mandField[14] = 'P';
                    $scope.dispField[14] = 'P';
                }
                if (mandFieldL.indexOf('CG') > -1) {
                    $scope.mandField[15] = 'CG';
                    $scope.dispField[15] = 'CG';
                }
                if (mandFieldL.indexOf('In') > -1) {
                    $scope.mandField[16] = 'In';
                    $scope.dispField[16] = 'In';
                }
                if (mandFieldL.indexOf('N') > -1) {
                    $scope.mandField[17] = 'N';
                    $scope.dispField[17] = 'N';
                }
                console.log($scope.selDispTxt);
                console.log($scope.dispField);
            } else {

                $scope.dispField = ['L', 'D', 'C', 'E', 'M', 'Cu', 'W', 'Di', 'Al', 'A1', 'A2', 'Ci', 'St', 'Z', 'P', 'CG', 'In', 'N'];
                $scope.mandField = ['', '', '', 'E', 'M', '', '', '', '', '', '', '', '', '', '', '', '', ''];
            }
            

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });


    };
    $scope.checkDisp = function (label, index) {
        if (label !== "") {
            $scope.dispField[index] = label;
        }
        
    };
    $scope.checkMandt = function (label, index) {
        if ($scope.mandField[index] === '' && index === 3) {
            $scope.mandField[4] = 'M';
            $scope.dispField[4] = 'M';
            toaster.pop("info", "", "Email / Mobile should be mandatory to contact", 10000, 'trustedHtml');
        }
        if ($scope.mandField[index] === '' && index === 4) {
            $scope.mandField[3] = 'E';
            $scope.dispField[3] = 'E';
            toaster.pop("info", "", "Email / Mobile should be mandatory to contact", 10000, 'trustedHtml');
        }
    };
    $scope.checkDispt = function (label, index) {
        if ($scope.dispField[index] === '' && index === 3) {
            $scope.dispField[4] = 'M';
            $scope.mandField[4] = 'M';
            toaster.pop("info", "", "Email / Mobile should be displayed to collect contact information", 10000, 'trustedHtml');
        }
        if ($scope.dispField[index] === '' && index === 4) {
            $scope.dispField[3] = 'E';
            $scope.mandField[3] = 'E';
            toaster.pop("info", "", "Email / Mobile should be displayed to collect contact information", 10000, 'trustedHtml');
        }
    };
    function emptyElement(element) {
        //Removes nulls, zeros (also falses), text version of false, and blank element
        if (element == null || element == 0 || element.toString().toLowerCase() == 'false' || element == '')
            return false;
        else
            return true;
    }
    $scope.updateConfig = function () {
        console.log($scope.dispText);
        console.log($scope.dispText.indexOf(undefined) );
        console.log($scope.dispText.indexOf('') );
		$scope.selDispTxt = [];
        if($scope.dispText.indexOf(undefined) === -1 && $scope.dispText.indexOf('') === -1 && $scope.enquiryTitle !== '' && $scope.enquiryResponse !== ""){
        //$scope.dispField[4] = 'M';
        //$scope.mandField[3] = 'E';
        $scope.selDispTxt[0] = $scope.dispText[0];
        // $scope.selDispTxt[3] = $scope.dispText[4];
        // $scope.selDispTxt[4] = $scope.dispText[5];
        //$scope.selDispTxt[0] = $scope.dispText[0];
        // $scope.selDispTxt[4] = $scope.dispText[4];
        // $scope.selDispTxt[5] = $scope.dispText[5];
        //$scope.dispField[4]='E';
        console.log($scope.dispField);
        // console.log($scope.mandField);
        $scope.dfieldSet = "F," + $scope.dispField.filter(emptyElement).join();
        angular.forEach($scope.dispField, function (value, key) {
            if (value !== "") {
                console.log(key);
                if ($scope.dfieldSet.indexOf(value) > 0) {
                    $scope.selDispTxt[key + 1] = $scope.dispText[key + 1];
                }else{
                    $scope.selDispTxt[key + 1] = "";
                }
            }
        });
        
        console.log($scope.selDispTxt);
        /*angular.forEach($scope.mandField, function (value1, key1) {
         if (value1 && $scope.mfieldSet.indexOf(value1) <= -1) {
         if ($scope.mfieldSet === "") {
         $scope.mfieldSet = value1;
         } else {
         $scope.mfieldSet = $scope.mfieldSet + "," + value1;
         }
         }
         });*/
        var seleFieldTxt = $scope.selDispTxt.filter(emptyElement).join();
        $scope.mfieldSet = "F," + $scope.mandField.filter(emptyElement).join();
        console.log($scope.dfieldSet);


        if ($scope.dfieldSet && $scope.mfieldSet) {
            $scope.requestData = {"id": $scope.eventtempSelection.id, "disp": $scope.dfieldSet, "mand": $scope.mfieldSet, "lableField": seleFieldTxt, "enquiryTitle": $scope.enquiryTitle, "enquirySubTitle": $scope.enquirySubTitle.join(), "enquiryResp": $scope.enquiryResponse,"grouped":$scope.grouped};
            Data.put('event/eventFieldConfig', $scope.requestData).then(function (result) {
                if (result.status !== 'error') {

                    //$uibModalInstance.close(1);
                    console.log('Setting the field ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');


                } else {
                    console.log(result);
                }
            });
            //console.log($scope.requestData);
        }
    }
    
    else if($scope.enquiryTitle === ""){
        toaster.pop("error", "", "Please fill the Title of the form", 10000, 'trustedHtml');
    }
    else if($scope.enquiryResponse === ""){
        toaster.pop("error", "", "Please fill the response of the form", 10000, 'trustedHtml');
    }
   /* else if($scope.enquirySubTitle.length != 5 && $scope.enquirySubTitle.indexOf(undefined) > 0){
        toaster.pop("error", "", "Please fill the subtitles of the form", 10000, 'trustedHtml');
    }*/
    
    else if(parseInt($scope.dispText.indexOf(undefined)) > 0 || $scope.dispText.indexOf('') > 0){
        toaster.pop("error", "", "Please ensure all the display text fields are filled", 10000, 'trustedHtml');
    }
    else{
        toaster.pop("error", "", "Please fill the title , subtitles , response and display text of the form", 10000, 'trustedHtml');
    }
        // console.log($scope.dfieldSet);
        // console.log($scope.mfieldSet);
    };




    /*Data.post('field/field', field).then(function (result) {
     if (result.status !== 'error') {
     
     $uibModalInstance.close(1);
     console.log('Setting the field ');
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     
     
     } else {
     console.log(result);
     }
     });
     */

});
evtApp.controller('integrationCtrl', function ($scope, warningPopup, $location, $rootScope,trialPopUp, $http, Data, toaster) {

    $rootScope.title = "Web Form Integration";
    $rootScope.addButtonhtml = '';

    //Get the list and set  
    $scope.eventLists = {};
    $scope.eventtempSelection = {};
    $scope.organizationId = sessionStorage.getItem("orgId");
    $scope.purl = "";
    $scope.key = "";
    $scope.surl = "";
    var param = "";
    if ($location.absUrl().indexOf("?") > 0 && $location.absUrl().indexOf("event") > 0) {
        param = $location.absUrl().split("=");
        console.log(decodeURIComponent(param[1]));
    }

    $scope.getEventDetails = function () {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        /*var respuserchk = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "integration"}});
         
         // $scope.users = {};
         respuserchk.then(function (response) {
         console.log(response);
         $scope.loader = false;
         if (response.data.status === 'success') {*/
        var eventresp = $http.get(Data.serviceBase() + '/event/orgEventList', {params: {"organizationId": $scope.organizationId}});
        eventresp.then(function (response) {
            //console.log(response.data);
            $scope.eventLists = response.data;
            // Get parameter details from url

            angular.forEach(response.data, function (d) {
                console.log(d.name);
                if (d.name === decodeURIComponent(param[1])) {
                    console.log(d);
                    $scope.eventtempSelection = d;
                    
                }
            });

            if ($scope.eventLists.length > 0 && param == "") {
                //console.log($scope.eventLists[0]);
                $scope.eventtempSelection = $scope.eventLists[0];
            }
            if ($scope.eventtempSelection.eventKey != null) {
                $scope.purl = "https://beta.helloleads.io/index.php/app/account/eventForm/" + $scope.eventtempSelection.eventKey;
                $scope.key = $scope.eventtempSelection.eventKey;
                if ($scope.eventtempSelection.eventURL !== undefined) {
                    $scope.surl = $scope.eventtempSelection.eventURL;
                    // console.log(response.data.qr_code_image_url);
                    $scope.simg = $scope.eventtempSelection.eventQR;
                }
                $scope.codeString = "<script type='text/javascript' src='http://beta.helloleads.io/js/intial.js'></script>"
                        + "<div class='form-bottom' id='form_sample' data-key='" + $scope.eventtempSelection.eventKey + "'></div>";
                localStorage.setItem("eventXG", JSON.stringify({"eventKey": $scope.eventtempSelection.eventKey, "id": $scope.eventtempSelection.id}));
                console.log($scope.purl);

            } else {
                var keyresp = $http.get(Data.serviceBase() + '/event/generateKey', {params: {"eventId": $scope.eventtempSelection.id}});
                keyresp.then(function (response) {
                    console.log(response.data);
                    $scope.purl = response.data.url;
                    $scope.key = response.data.key;
                    if (response.data.shortURL !== undefined) {
                        $scope.surl = response.data.shortURL;
                        // console.log(response.data.qr_code_image_url);
                        $scope.simg = response.data.qr_code_image_url;
                        $scope.loader = false;
                    }
                    $scope.codeString = "<script type='text/javascript' src='http://beta.helloleads.io/js/intial.js'></script>"
                            + "<div class='form-bottom' id='form_sample' data-key='" + $scope.key + "'></div>";
                    localStorage.setItem("eventXG", JSON.stringify({"eventKey": $scope.key, "id": $scope.eventtempSelection.id}));
                    //$scope.eventName = response.data.eventName;
                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }
            $rootScope.$emit("callConfig", {});

            $scope.loader = false;
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });

        /*} else {
         $scope.message = {"title": response.data.title, "message": response.data.message, "cancel": false};
         checkPremium.premiumpopmodel($scope.message);
         }
         }, function (response) {
         
         console.log('Error happened -- ');
         console.log(response);
         });*/
		 
			} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    }

    $scope.eventKeyChange = function () {
        if ($scope.eventtempSelection.eventKey != null) {

            $scope.purl = "https://beta.helloleads.io/index.php/app/account/eventForm/" + $scope.eventtempSelection.eventKey;
            $scope.key = $scope.eventtempSelection.eventKey;
            if ($scope.eventtempSelection.eventURL !== undefined) {
                $scope.surl = $scope.eventtempSelection.eventURL;
                // console.log(response.data.qr_code_image_url);
                $scope.simg = $scope.eventtempSelection.eventQR;

            }
            $scope.codeString = "<script type='text/javascript' src='https://beta.helloleads.io/js/intial.js'></script>"
                    + "<div class='form-bottom' id='form_sample' data-key='" + $scope.eventtempSelection.eventKey + "'></div>";
            localStorage.setItem("eventXG", JSON.stringify({"eventKey": $scope.eventtempSelection.eventKey, "id": $scope.eventtempSelection.id}));

        } else {
            var keyresp = $http.get(Data.serviceBase() + '/event/generateKey', {params: {"eventId": $scope.eventtempSelection.id}});
            keyresp.then(function (response) {
                console.log(response.data);
                $scope.purl = response.data.url;
                $scope.key = response.data.key;
                if (response.data.shortURL !== undefined) {
                    $scope.surl = response.data.shortURL;
                    // console.log(response.data.qr_code_image_url);
                    $scope.simg = response.data.qr_code_image_url;
                    $scope.loader = false;
                }
                $scope.codeString = "<script type='text/javascript' src='https://beta.helloleads.io/js/intial.js'></script>"
                        + "<div class='form-bottom' id='form_sample' data-key='" + $scope.key + "'></div>";
                localStorage.setItem("eventXG", JSON.stringify({"eventKey": $scope.key, "id": $scope.eventtempSelection.id}));
                //$scope.eventName = response.data.eventName;
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
        }
        $rootScope.$emit("callConfig", {});

    };

    // Default Page tab is User Profile
    $scope.itab = 1;
    angular.element(document.getElementById("formConfigTab")).removeClass("active");
    angular.element(document.getElementById("DEFTab")).addClass("active");
    angular.element(document.getElementById("websiteIntegTab")).removeClass("active");

    $scope.switchiTab = function (tabId) {
        console.log(tabId);
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
        if (tabId === 1) {

            $scope.itab = 1;
            angular.element(document.getElementById("formConfigTab")).removeClass("active");
            angular.element(document.getElementById("DEFTab")).addClass("active");
            angular.element(document.getElementById("websiteIntegTab")).removeClass("active");
            Data.post('event/setTabOne', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else if (tabId === 2) {
            $scope.itab = 2;
            angular.element(document.getElementById("formConfigTab")).removeClass("active");
            angular.element(document.getElementById("DEFTab")).removeClass("active");
            angular.element(document.getElementById("websiteIntegTab")).addClass("active");
            Data.post('event/setTabTwo', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else {
            $scope.itab = 3;
            angular.element(document.getElementById("formConfigTab")).addClass("active");
            angular.element(document.getElementById("DEFTab")).removeClass("active");
            angular.element(document.getElementById("websiteIntegTab")).removeClass("active");
            Data.post('event/setTabThree', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        }

    };




});

