evtApp.controller('ruserCtrl', function ($scope, $location, $rootScope, Data, toaster, $http, $location, $window) {
    $scope.users = {};
    $scope.firstName = "";

    $scope.rlogin = function (user) {

        if (user["username"] !== '') {

            Data.post('account/regilogin', user).then(function (result) {
                if (result.status !== 'error' && result.status !== 'warning' && result.status !== 'basic') {

                    $scope.error = false;
                    console.log('login success.....');
                    $scope.firstName = result.firstName;
                    sessionStorage.setItem("userId", result.id);
                    sessionStorage.setItem("firstName", result.firstName);
                    sessionStorage.setItem("lastName", result.lastName);
                    sessionStorage.setItem("orgId", result.organizationId);
                    sessionStorage.setItem("token", result.token);
                    sessionStorage.setItem("userType", result.userType);
                    sessionStorage.setItem("userLevel", result.access_level);
                    sessionStorage.setItem("userEmail", result.email);
                    window.location.href = Data.webAppBase() + 'account/regihome';

                } else {
                    $scope.error = true;
                    console.log('logged in sucessfully');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    console.log(result);
                }
            });
        }
    };

    $rootScope.rlogout = function () {
        console.log('About to add a logout user......');
        Data.get('account/regilogout').then(function (results) {
            Data.toast(results);
            sessionStorage.removeItem("userId", '');
            sessionStorage.removeItem("firstName", '');
            sessionStorage.removeItem("lastName", '');
            sessionStorage.removeItem("token", '');
            sessionStorage.removeItem("orgId", '');
            sessionStorage.removeItem("userEmail", "");
            sessionStorage.removeItem("cFirstName", '');
            sessionStorage.removeItem("orgId");
            sessionStorage.removeItem("cLastName", '');
            sessionStorage.removeItem("userType", '');
            sessionStorage.removeItem("menuact", '');

            window.location.href = Data.webAppBase() + 'account/regilogin';

        });
    };


});
evtApp.controller('regimain', function ($scope, $rootScope, $filter, $window, Data, $uibModal, visitorShareData, $http, DTOptionsBuilder) {

    $scope.editable = true;
    $scope.searchVal = '';
    $scope.atfocus = true;
    $scope.nfocus = false;
    $scope.errorval = false;
    $scope.visitors = {};
    $scope.loader = false;
    $scope.errorlabel = false;
    $scope.orgId = sessionStorage.getItem("orgId");
    $scope.userId = sessionStorage.getItem("userId");
    $scope.editableRef = {};
    $scope.tab = 1;
    angular.element(document.getElementById("refid")).addClass("active");
    angular.element(document.getElementById("name")).removeClass("active");
    angular.element(document.getElementById("company")).removeClass("active");
    angular.element(document.getElementById("mobile")).removeClass("active");
    angular.element(document.getElementById("email")).removeClass("active");
    $scope.switchregiTab = function (tabId) {
        if (tabId === 1) {
            $scope.tab = 1;
            $scope.editable = true;
            angular.element(document.getElementById("refid")).addClass("active");
            angular.element(document.getElementById("name")).removeClass("active");
            angular.element(document.getElementById("company")).removeClass("active");
            angular.element(document.getElementById("mobile")).removeClass("active");
            angular.element(document.getElementById("email")).removeClass("active");
        } else if (tabId === 2) {

            $scope.tab = 2;
            if ($scope.editableRef.visitorId === undefined) {
                $scope.editable = false;
            } else {
                $scope.editable = true;
            }
            angular.element(document.getElementById("refid")).removeClass("active");
            angular.element(document.getElementById("name")).addClass("active");
            angular.element(document.getElementById("company")).removeClass("active");
            angular.element(document.getElementById("mobile")).removeClass("active");
            angular.element(document.getElementById("email")).removeClass("active");
        } else if (tabId === 3) {
            $scope.tab = 3;
            if ($scope.editableRef.visitorId === undefined) {
                $scope.editable = false;
            } else {
                $scope.editable = true;
            }
            angular.element(document.getElementById("refid")).removeClass("active");
            angular.element(document.getElementById("name")).removeClass("active");
            angular.element(document.getElementById("company")).addClass("active");
            angular.element(document.getElementById("mobile")).removeClass("active");
            angular.element(document.getElementById("email")).removeClass("active");
        } else if (tabId === 4) {
            $scope.tab = 4;
            if ($scope.editableRef.visitorId === undefined) {
                $scope.editable = false;
            } else {
                $scope.editable = true;
            }
            angular.element(document.getElementById("refid")).removeClass("active");
            angular.element(document.getElementById("name")).removeClass("active");
            angular.element(document.getElementById("company")).removeClass("active");
            angular.element(document.getElementById("mobile")).addClass("active");
            angular.element(document.getElementById("email")).removeClass("active");
        } else if (tabId === 5) {
            $scope.tab = 5;
            if ($scope.editableRef.visitorId === undefined) {
                $scope.editable = false;
            } else {
                $scope.editable = true;
            }
            angular.element(document.getElementById("refid")).removeClass("active");
            angular.element(document.getElementById("name")).removeClass("active");
            angular.element(document.getElementById("company")).removeClass("active");
            angular.element(document.getElementById("mobile")).removeClass("active");
            angular.element(document.getElementById("email")).addClass("active");
        }


    };


    $scope.searchRefId = function (valId) {
		valId = "HL"+valId;
        $scope.loader = true;
        var vresp = $http.get(Data.serviceBase() + '/visitor/searchVisitorById', {params: {"visitorId": valId, "orgId": $scope.orgId}});
        vresp.then(function (response) {
            console.log(response.data);

            $scope.editableRef = response.data[0];
            //$scope.formatteddt = $filter('date')($scope.todaydt, "dd-MM-yyyy H:mm:ss");
			
			$scope.editable = true;
            $scope.searchVal = '';
            $scope.loader = false;
            //console.log("Current event1" + $rootScope.currentEventId);
            if ($scope.editableRef === undefined) {
                $scope.errorlabel = true;
            } else {
                $scope.date = new Date();
                $scope.formattedDate = $filter('date')($scope.date, "dd-MMM-yyyy HH:mm:ss");
                $scope.errorlabel = false;
                $scope.editableRef['dttime'] = $scope.formattedDate;

                $scope.loader = false;
                window.document.getElementById("id_txt").blur();
                
               /* clickMe.addEventListener('click', function () {
                    console.log('I was clicked');
                }, true);*/

               
                //window.document.getElementById("printDiv").focus();
               

            }
        }, function (response) {
            $scope.loader = false;
            $scope.errorlabel = true;
            console.log('Error happened -- ');
            console.log(response);
        });




    };

    $scope.searchByKey = function (key, value) {
        $scope.key = key;
        $scope.value = value;
        $scope.visitors = {};
        $scope.loader = true;
        var vresp = $http.get(Data.serviceBase() + '/visitor/searchVisitorByKey', {params: {"key": $scope.key, "value": $scope.value, "orgId": $scope.orgId}});
        vresp.then(function (response) {
            console.log(response.data);
            $scope.editableRef = {};
            $scope.editable = false;   
            $scope.visitors = response.data;
            $scope.searchVal = '';
            //console.log("Current event1" + $rootScope.currentEventId);
            if ($scope.editableRef === undefined) {
                $scope.errorval = true;
            }
            $scope.loader = false;

        }, function (response) {
            $scope.errorval = true;
            console.log('Error happened -- ');
            console.log(response);
        });

    };

    $scope.selectVisitor = function (visitor) {

        //$scope.editableRef = visitor;
        $scope.editable = true;
        $scope.loader = true;
        console.log(visitor);
		var rand = Math.floor(Math.random() * 90 + 10);
        var id = visitor.visitorId + rand.toString();
		$scope.searchRefId(id);
    };

    $scope.cancelVisitor = function () {
        if ($scope.tab !== 1) {
            $scope.editableRef = {};
            $scope.editable = false;
        } else {
            $scope.editableRef = {};
        }

    };

    $scope.clearFilter = function () {
        $scope.editableRef = {};
        $scope.editable = false;
        $scope.visitors = {};
    };

    $scope.beforePrint = function () {
        console.log("Befor");
    };
    $scope.afterPrint = function () {
        console.log("After");
    };

    $scope.printDiv = function (visitor) {


        /* var mywindow = window.open('', 'PRINT', 'height=400,width=600');
         mywindow.document.write('<html><head><title></title>');
         mywindow.document.write('<link href="../../../css/print.css" rel="stylesheet" type="text/css"/></head><body >');
         mywindow.document.write(jQuery('#printable').html());
         mywindow.document.write('</body></html>');
         
         mywindow.document.close();
         //mywindow.focus();
         setTimeout(function () {
         mywindow.print();
         // mywindow.close();
         }, 10);
         //mywindow.close();
         console.log(visitor);*/
        var innerContents = document.getElementById('printable').innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link href="../../../css/print.css" rel="stylesheet" type="text/css"/></head><body onload="window.print();window.close();" >' + innerContents + '</html>');
        popupWinindow.document.close();


        $scope.printData = {};
        // $scope.printData['visitorId'] = 0;
        $scope.printData['visitorId'] = visitor.visitorId;
        $scope.printData['orgId'] = $scope.orgId;
        $scope.printData['eventId'] = visitor.eventId;
        $scope.printData['userId'] = $scope.userId;
        $scope.printData['printTime'] = $scope.formattedDate;
        $scope.printData['firstName'] = visitor.firstName;
        Data.post('visitor/printDet', $scope.printData).then(function (result) {
            if (result.status !== 'error') {
                $scope.editableRef = {};
                $scope.editable = false;
                $scope.visitors = {};
                console.log('Success.....' + result);


            } else {
                $scope.error = true;
                console.log('Error');

            }
        });

        return true;
        //window.print();
    };



});


