
evtApp.controller('dispCategoryCtrl', function ($scope, $rootScope, Data, $uibModal,alertUser,trialPopUp, $http, DTOptionsBuilder,toaster) {
    $rootScope.emptycategory = {};
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('<"html5buttons"B>lTfgitp')
            .withButtons([
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'HL-Category'},
                //{extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]);
    $scope.loader = true;
    if (sessionStorage.getItem("xtime") === "1") {
        $rootScope.firstProfile = true;
    } else {
        $rootScope.firstProfile = false;
    }
    sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if($scope.roleId === '2'){
        $scope.rolemsg = "Manager";
    }
    else if($scope.roleId === '3'){
         $scope.rolemsg = "L1 User";
    }
    else{
        $scope.rolemsg = "L2 User";
    }
    console.log("Organization Id is " + $scope.organizationId);
    $scope.loader = true;
    var resprom = $http.get(Data.serviceBase() + '/category/category', {params: {key: "organizationId", "value": $scope.organizationId}});
    $rootScope.addButtonhtml = '<button class="btn btn-primary btn-sm" ng-click="addCategory(emptycategory);"><span class="fa fa-plus"></span></button>&nbsp;';
    $rootScope.addButtonhtml += '<button class="btn btn-default btn-sm btn-circle" popover="Customer Group is a grouping of potential customers based on their nature of organization or organization size. Customer Group helps in segmenting your leads so that you can do targeted marketing. For Apple, the c-groups can be business organization, government, employed individuals, and students." popover-trigger="focus" popover-title="Customer Group (Segments or Customer Groups)" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    //$rootScope.title = "";
    $scope.categories = {};
    resprom.then(function (response) {
        $scope.categories = response.data;
        $scope.loader = false;
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });

    $scope.editCategory = function (p, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {

        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "editCategory"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {

                var modalInstance = $uibModal.open({
                    templateUrl: './modifyCategory',
                    controller: 'modifyCategoryCtrl',
                    size: size,
                    backdrop: 'static',
                    resolve: {
                        category: function () {
                            return p;
                        },
                        categories: function () {
                            return $scope.categories;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {

                        var resprom = $http.get(Data.serviceBase() + '/category/category', {params: {key: "organizationId", "value": $scope.organizationId}});

                        $scope.categories = {};
                        resprom.then(function (response) {
                            $scope.categories = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title":"Authorization failure","message": "Your current role ( "+$scope.rolemsg+" ) does not allow you to manage customer group. Only account owner or managers (within HelloLeads) can manage customer group. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		
			} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });


    };

    $rootScope.addCategory = function (p, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "addCategory"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance = $uibModal.open({
                    templateUrl: './modifyCategory',
                    controller: 'modifyCategoryCtrl',
                    size: size,
                    backdrop: 'static',
                    resolve: {
                        category: function () {
                            return p;
                        },
                        categories: function () {
                            return $scope.categories;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        //console.log('selected operation is ' + selectedObject.save);
                        var resprom = $http.get(Data.serviceBase() + '/category/category', {params: {key: "organizationId", "value": $scope.organizationId}});

                        $scope.categories = {};
                        resprom.then(function (response) {
                            $scope.categories = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title":"Authorization failure","message": "Your current role ( "+$scope.rolemsg+" ) does not allow you to manage customer group. Only account owner or managers (within HelloLeads) can manage customer group. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		
			} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });


    };

    $scope.delCategory = function (p, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteCategory"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance1 = $uibModal.open({
                    templateUrl: './deleteCategory',
                    controller: 'delCategoryCtrl',
                    size: size,
                    resolve: {
                        category: function () {
                            return p;
                        }
                    }
                });
                modalInstance1.result.then(function (i) {
                    if (i) {
                        //console.log('selected operation is ' + selectedObject.save);
                        var resprom = $http.get(Data.serviceBase() + '/category/category', {params: {key: "organizationId", "value": $scope.organizationId}});

                        $scope.categories = {};
                        resprom.then(function (response) {
                            $scope.categories = response.data;
                        }, function (response) {
                            console.log('Error happened -- ');
                            console.log(response);
                        });
                    }
                });
            } else {
                $scope.message = {"title":"Authorization failure","message": "Your current role ( "+$scope.rolemsg+" ) does not allow you to manage customer group. Only account owner or managers (within HelloLeads) can manage customer group. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		
			} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });


    };
	$scope.cfileError = false;
    $scope.getCategoryFileDetails = function (e) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "editCategory"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.loader = false;
                    if (response.data.status === 'success') {
                        //console.log("Getting Files");
                        $scope.filesCsv = [];
                        $scope.$apply(function () {

                            // STORE THE FILE OBJECT IN AN ARRAY.
                            for (var i = 0; i < e.files.length; i++) {
                                if ((e.files[i]['name'].indexOf("xls")) !== -1 || (e.files[i]['name'].indexOf("xlsx")) !== -1 && e.files[i].size / 1024 / 1024 < 5) { //(e.files[i]['name'].indexOf("csv")) !== -1 ||
                                    $scope.filesCsv.push(e.files[i]);
                                    $scope.cfileError = false;
                                    $scope.uploadCatgeoryFiles();
                                } else {
                                    $scope.cfileError = true;
                                }
                            }
                            console.log($scope.cfileError);
                        });
                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to manage customer group. Only account owner or managers (within HelloLeads) can manage customer group. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.btnText = "Upload Customer Groups";
    $scope.btnDisable = false;
    // NOW UPLOAD THE FILES.
    $scope.uploadCatgeoryFiles = function () {
        console.log("Inside Customer group");
        $scope.btnText = 'Uploading...';
        $scope.btnDisable = true;
        //FILL FormData WITH FILE DETAILS.

        var data = new FormData();
        for (var i in $scope.filesCsv) {
            ////console.log($scope.filesCsv[i]);
            data.append("filelist", $scope.filesCsv[i]);
        }

        data.append("organizationId", sessionStorage.getItem('orgId'));


        var request = {
            method: 'POST',
            url: Data.serviceBase() + "/category/fileuploadExcel/",
            data: data,
            headers: {
                'Content-Type': undefined
            }
        };
        // SEND THE FILES.
        $http(request)
                .success(function (result) {
                    //$uibModalInstance.close(1);
                    $scope.categoryForm.$setPristine();
                    $scope.btnText = "Upload the file";
                    if (result.status === 'success') {
                        $scope.getCateories();
                        $scope.message = {"title": "Import Customer group", "message": result.message};
                        alertUser.alertpopmodel($scope.message);
                    } else {
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    }

                })
                .error(function (result) {
                    //$uibModalInstance.close(1);
                    $scope.btnText = "Upload the file";
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                });
    };



});

evtApp.controller('modifyCategoryCtrl', function ($scope, Data, $uibModalInstance, category, categories, toaster) {
    var original = angular.copy($scope.category);
    $scope.category = angular.copy(category);
    $scope.categories = angular.copy(categories);

    if ($scope.category.id !== undefined) {
        $scope.title = "Update Customer Group";
        $scope.btntxt = "Update";
    } else {
        $scope.title = "Add New Customer Group";
        $scope.btntxt = "Add";
    }

    $scope.checkCategory = function (name) {
        $scope.dupFlag = 0;
        angular.forEach($scope.categories, function (d) {

            if (d.name.toLowerCase() === name.toLowerCase()) {
                $scope.dupFlag = 1;

            }


        });
        if ($scope.dupFlag === 1) {
            return true;
        } else {
            return false;
        }

    };

    $scope.canSaveCategory = function () {
        return $scope.categoryform.$valid && !angular.equals($scope.category, original);
    };
    $scope.updatcategory = function (categ) {

        if (categ.id !== undefined) {


            Data.put('category/category', categ).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    //$uibModalInstance.dismiss('Close');
                    console.log('Setting the category ')
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        } else {

            if (categ.name !== undefined) {

                categ.organizationId = sessionStorage.getItem('orgId');

                Data.post('category/category', categ).then(function (result) {
                    if (result.status !== 'error') {

                        $uibModalInstance.close(1);
                        console.log('Setting the category ');
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');


                    } else {
                        console.log(result);
                    }
                });

            }



        }

    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});
evtApp.controller('delCategoryCtrl', function ($scope, $http, Data, $uibModalInstance, category, toaster) {

    $scope.category = category;
    $scope.title = "Delete Category";
    $scope.deleteCategory = function (categ) {

        if (categ.name !== undefined) {

            var resprom = Data.delete('category/category/' + categ.id);
            resprom.then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);

                    console.log('Setting the category ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

                } else {
                    console.log(result);
                }
            });

        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };




});








