evtApp.controller('dispbuycreditCtrl', function ($scope, $timeout,$uibModal, $rootScope, Data, $window, toaster, $http) {
    if (sessionStorage.getItem("xtime") === "1") {
        $rootScope.firstProfile = true;
    } else {
        $rootScope.firstProfile = false;
    }
    $scope.loader = true;
    $scope.buycredits = function (plan, amount, credits) {
        $scope.organizationId = sessionStorage.getItem('orgId');
        $scope.userId = sessionStorage.getItem('userId');

        $window.location.href = Data.webAppBase() + "paypal/makePaymentUsingPayPal?oId=" + $scope.organizationId + "&uId=" + $scope.userId + "&amount=" + amount
                + "&credits=" + credits + "&planId=" + plan;


    };
    $scope.plans = "1";
    $scope.mcurrency = 'USD';
    $scope.planamount = "$300";
    $scope.plancredit = "500";
    $rootScope.title = "Plan & Billing";
    $rootScope.addButtonhtml = "";
    $scope.payments = {};
    $scope.credits = {};
	$scope.promoCodes = {};
	$scope.selectPromo = {};
    $scope.payhist = false;
    $scope.planChoosed = "";
    $scope.planChoosedName = "";
    $scope.firstDiv = false;
    $scope.splan = true;
    $scope.secondDiv = false;
    $scope.thirdDiv = false;
    $scope.accounttype = sessionStorage.getItem('accountType');
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    console.log("Organization Id is " + $scope.organizationId);
    sessionStorage.setItem("menuact", '7');
    $rootScope.menuact = '7';
    $scope.nofUser = 0;
    $scope.minnofUser = 0;
    $scope.bcycle = "1";
    $scope.allPlanDetails = {};
    $scope.getCreditPayDet = function () {
        var resprom = $http.get(Data.serviceBase() + '/credit/credit', {params: {"key": "organizationId", "value": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.credits = response.data[0];
            console.log($scope.credits.multi_user);
            $scope.nofUser = parseInt($scope.credits.multi_user);
            $scope.minnofUser = parseInt($scope.credits.multi_user);
            console.log(response.data[0]);
            //$scope.eventSelected = response.data[0];


        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        $scope.orgName = sessionStorage.getItem('cOrgName');
        var resprom = $http.get(Data.serviceBase() + '/paymenthistory/paymenthistory', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.payments = response.data;
            $scope.loader = false;
            console.log(response.data);
            //$scope.eventSelected = response.data[0];


        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        $scope.orgDet = {};
        var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {"key": "id", "value": $scope.organizationId}});
        resprom.then(function (response) {
            if (response.data) {
                $scope.orgDet = response.data[0];
                console.log("Org Det");
                //$scope.loader = false;
                console.log(response.data);
                //$scope.eventSelected = response.data[0];

            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        $scope.userCount = 0;
        var resprom = $http.get(Data.serviceBase() + '/user/getMaxUserCount', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            if (response.data) {
                $scope.userCount = response.data.userCount;

                console.log("User Count");
                //$scope.loader = false;
                console.log(response.data.userCount);
                //$scope.eventSelected = response.data[0];
				
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
		var resprom = $http.get(Data.serviceBase() + '/payment/promoCodes', {params: {}});
        resprom.then(function (response) {
            if (response.data) {
                $scope.promoCodes = response.data;
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.checkDate = function (datedet) {

        if (!datedet) {
            return false;
        } else {
            var dateDet = new Date(datedet);
            var curDate = new Date();

            if (dateDet < curDate) {
                return true;

            } else {
                $scope.splan = false;
                return false;

            }
        }

    };
    $scope.planChoosedPrice = "";
    $scope.planc = {};
    $scope.planChoosedMonthPrice = "";
    $scope.CurrencyRate = "";
    $scope.getCurrency = function (toCur, type, amount) {
        if ($scope.mcurrency === 'INR') {
            $scope.mcurrency = "USD";
        } else {
            $scope.mcurrency = "INR";
        }

        var resprom = $http.get(Data.webAppBase() + 'account/currencyConverter/' + toCur + '/' + type + '/' + 1, {});
        resprom.then(function (response) {
            if (response.data) {
                $scope.CurrencyRate = response.data;
                calculatePrice($scope.planChoosedPrice, $scope.planChoosedMonthPrice, $scope.CurrencyRate);

            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.planDet = function () {
        var resprom = $http.get(Data.serviceBase() + '/organization/planDet', {});
        resprom.then(function (response) {
            if (response.data) {
                $scope.allPlanDetails = response.data;
                if (parseInt($scope.userCount) <= parseInt($scope.allPlanDetails[1].multi_users)) {
                    $scope.chplan = '2';
                    $scope.planChoose($scope.allPlanDetails[2]);

                }
                if (parseInt($scope.userCount) > parseInt($scope.allPlanDetails[1].multi_users) && parseInt($scope.userCount) <= parseInt($scope.allPlanDetails[2].multi_users)) {
                    $scope.chplan = '2';
                    $scope.planChoose($scope.allPlanDetails[2]);
                }
                if (parseInt($scope.userCount) > parseInt($scope.allPlanDetails[2].multi_users)) {
                    $scope.chplan = '3';
                    $scope.planChoose($scope.allPlanDetails[3]);
                }
                /*console.log($scope.orgDet.customerCardId);
                console.log($scope.orgDet.stripeCustomerId);
                var resprom = $http.get(Data.serviceBase() + 'organization/getOrgCarddetails/', {params: {"cardId":$scope.orgDet.customerCardId,"customerId":$scope.orgDet.stripeCustomerId}});
                resprom.then(function (response) {
                    if (response.data) {
                        $scope.cardDetails = response.data;
                        console.log(response.data);
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });*/



            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
	var paymentGate = angular.element(document.getElementById("paymentG"));
    paymentGate.val("stripe");
    $scope.plansel = {};
    $scope.planChoose = function (plan) {
        if (plan) {
            $scope.planChoosed = plan.planNameId;
            $scope.planChoosedName = plan.planName;
            $scope.planChoosedPrice = plan.planAnAmount;
            $scope.planChoosedMonthPrice = plan.planMonAmount;
            $scope.plansel = plan;
            $scope.planc = plan.toString();
            // $scope.firstDiv = false;
            //$scope.secondDiv = true;
            // $scope.thirdDiv = false;
            //console.log($scope.credits.multi_user);
            if ($scope.credits.multi_user === '0') {

                $scope.nofUser = $scope.userCount;
                var userDiv = angular.element(document.getElementById("numberofuser"));
                userDiv.val($scope.userCount);
                //calculatePrice($scope.planChoosedPrice, $scope.planChoosedMonthPrice);
            }
			if($scope.userCount === 1 ){
					$scope.nofUser = 1;
				}
			$scope.subTotal = (parseFloat($scope.planChoosedPrice) * $scope.nofUser * 12).toFixed(0);
           
           $scope.discountAmt = '0';
           $scope.netAmount = ((parseFloat($scope.planChoosedPrice) * $scope.nofUser * 12)).toFixed(0);
           $scope.nmonth = '12';
            console.log($scope.netAmount);
           if($scope.orgDet.country === 'India'){
                $scope.taxPayment = ((parseFloat($scope.netAmount) * 18)/100).toFixed(0);
                $scope.totalPayment = (parseFloat($scope.netAmount) + parseFloat($scope.taxPayment)).toFixed(0);
                
            }else{
                $scope.taxPayment = '-';
                $scope.totalPayment = parseFloat($scope.netAmount).toFixed(0);
            }
			$scope.getCurrencyConv();
            calculatePrice($scope.planChoosedPrice, $scope.planChoosedMonthPrice);
        }
    };
	
    $scope.userChkError = false;

    $scope.planSelectChoose = function (plan) {
        $scope.planChoosedPrice = "";
        $scope.planChoosedMonthPrice = "";
        if (plan) {
			$scope.userChkError = false;
            var splanc = JSON.parse(plan);
            $scope.planChoosed = splanc.planNameId;
            $scope.planChoosedName = splanc.planName;
            $scope.planChoosedPrice = splanc.planAnAmount;
            console.log(splanc.planAnAmount);
            $scope.planChoosedMonthPrice = splanc.planMonAmount;
            $scope.plansel = JSON.parse(plan);
            $scope.bcycle = '1';
			//console.log($scope.nofUser + "-"+splanc.multi_users);
            if (parseInt($scope.nofUser) > parseInt(splanc.multi_users)) {
				//console.log("true");
                $scope.userChkError = true;

            } else if (parseInt($scope.nofUser) > 100) {
				//console.log("true1");
                $scope.userChkError = true;

            } else {
				//console.log("false");
                $scope.userChkError = false;
            }
			$scope.subTotal = (parseFloat($scope.planChoosedPrice) * $scope.nofUser * 12).toFixed(0);
            if($scope.selectPromo['promoCode'] !== undefined){
                $scope.discountAmt = (((parseFloat($scope.planChoosedPrice) * $scope.nofUser * 12) * $scope.selectPromo.discount)/100).toFixed(0);
                $scope.netAmount = (parseFloat($scope.subTotal) - parseFloat($scope.discountAmt)).toFixed(0);
                $scope.nmonth = $scope.selectPromo.validity;
               
            }else{
                $scope.discountAmt = '0';
                $scope.netAmount = ((parseFloat($scope.planChoosedPrice) * $scope.nofUser * 12)).toFixed(0);
                $scope.nmonth = '12';
            }
            
           if($scope.orgDet.country === 'India'){
                $scope.taxPayment = ((parseFloat($scope.netAmount) * 18)/100).toFixed(0);
                $scope.totalPayment = (parseFloat($scope.netAmount) + parseFloat($scope.taxPayment)).toFixed(0);
                
            }else{
                $scope.taxPayment = '-';
                $scope.totalPayment = parseFloat($scope.netAmount).toFixed(0);
            }
			$scope.getCurrencyConv();
            calculatePrice($scope.planChoosedPrice, $scope.planChoosedMonthPrice);
        }
    };
	$scope.calculateUser = function(userC){
		if($scope.orgDet.country === 'India'){
                $scope.taxPayment = ((parseFloat($scope.netAmount) * 18)/100).toFixed(0);
                $scope.totalPayment = (parseFloat($scope.netAmount) + parseFloat($scope.taxPayment)).toFixed(0);
                
            }else{
                $scope.taxPayment = '-';
                $scope.totalPayment = parseFloat($scope.netAmount).toFixed(0);
            }
			$scope.getCurrencyConv();
            calculatePrice($scope.planChoosedPrice, $scope.planChoosedMonthPrice);
	}
	$scope.calcTaxTotal = function(){
         if($scope.orgDet.country === 'India'){
                $scope.taxPayment = ((parseFloat($scope.netAmount) * 18)/100).toFixed(0);
                $scope.totalPayment = (parseFloat($scope.netAmount) + parseFloat($scope.taxPayment)).toFixed(0);
                
            }else{
                $scope.taxPayment = '-';
                $scope.totalPayment = parseFloat($scope.netAmount).toFixed(0);
            }
    }
	$scope.getCurrencyConv = function () {
        
        var resprom = $http.get(Data.webAppBase() + 'account/getcurrency/', {params: {"amount": $scope.totalPayment}});
        resprom.then(function (response) {
            if (response.data) {
                console.log(response.data);
                $scope.currencyAmount = response.data.amount;
                $scope.currencyType = response.data.currency;
                //calculatePrice($scope.planChoosedPrice, $scope.planChoosedMonthPrice, $scope.CurrencyRate);

            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
	
	$scope.changeUserCount = function(){
        $scope.planSelectChoose(JSON.stringify($scope.plansel));
    };
	
	
    $scope.planBillingChange = function () {

        var userDiv = angular.element(document.getElementById("cycle"));
        userDiv.val($scope.bcycle);
        calculatePrice($scope.planChoosedPrice, $scope.planChoosedMonthPrice);


    };
    $scope.checkUserCount = function (cuser, auser) {
        if (auser) {
			$scope.userChkError = false;
            //console.log(cuser + " " + parseInt(auser));
            if (parseInt(cuser) > parseInt(auser)) {
                $scope.userChkError = true;
                return true;
            } else if (cuser > 100) {
                $scope.userChkError = true;
                return true;
            } else {
                $scope.userChkError = false;
                return false;
            }
        }
    };
    $scope.trailDate = function (expiryDate) {
        var curDate = new Date();
        var edate = new Date(expiryDate);
        edate.setDate(edate.getDate());
        var timeDiff = Math.abs((edate.getTime()) - curDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    };

    $scope.checkout = function () {

        $scope.firstDiv = false;
        $scope.secondDiv = false;
        $timeout(function () {}, 900);
        $scope.thirdDiv = true;

    };
    $scope.cancelUser = function () {

        $scope.firstDiv = false;
        $scope.secondDiv = false;
        $scope.thirdDiv = false;

    };
    $scope.chooseUser = function () {

        $scope.firstDiv = false;
        $scope.secondDiv = false;
        $scope.thirdDiv = true;

    };
    $scope.renewPlan = function (plan) {
        if (plan) {
            $scope.firstDiv = false;
            $scope.secondDiv = false;
            $scope.planChoosed = plan.planId;
            $scope.planChoosedName = plan.paymentNotes;
            if (plan.noOfMonth === '12') {
                // $scope.planChoosedPrice = plan.amount;
                $scope.bcycle = '1';
                var monthDiv = angular.element(document.getElementById("totmonths"));
                monthDiv.val(plan.noOfMonth);
            } else {
                // $scope.planChoosedMonthPrice = plan.amount;
                $scope.bcycle = '2';
                var monthDiv = angular.element(document.getElementById("totmonths"));
                monthDiv.val(plan.noOfMonth);
            }
            $scope.nofUser = parseInt(plan.noOfUser);
            var userDiv = angular.element(document.getElementById("numberofuser"));
            userDiv.val(plan.noOfUser);
            var amountDiv = angular.element(document.getElementById("NetPrice"));
            amountDiv.val(plan.amount);
            var totpriceDiv = angular.element(document.getElementsByClassName("totPrice"));
            totpriceDiv.text("USD " + plan.amount);
            var totamtDiv = angular.element(document.getElementById("totalPramount"));
            totamtDiv.text("USD " + plan.totalAmount);
             var totamtInt = angular.element(document.getElementById("totalPamount"));
            totamtInt.val(plan.totalAmount);
            var taxAmountDiv = angular.element(document.getElementById("taxPramount"));
            taxAmountDiv.text("USD " + plan.taxAmount);
            var taxAmountInt = angular.element(document.getElementById("netTax"));
            taxAmountInt.val(plan.taxAmount);
            $scope.splan = true;
            $scope.thirdDiv = true;
            // $scope.planc = plan;




            // calculatePrice($scope.planChoosedPrice, $scope.planChoosedMonthPrice);
        }

    };
	 $scope.chooseGateWay = function (payG) {
        //alert(payG);
       /* if (payG === 'paypal') {
            $scope.payG = 'paypal';
            $scope.paymentURL = Data.webAppBase()+"paypal/makePaymentUsingPayPal";
            
            var paymentactDiv = angular.element(document.getElementById("payment-form"))[0];
            paymentactDiv.setAttribute("action",Data.webAppBase()+"paypal/makePaymentUsingPayPal");
            var paymentGpDiv = angular.element(document.getElementById("paypalS"))[0];
            paymentGpDiv.setAttribute("checked",true);
            var paymentGsDiv = angular.element(document.getElementById("stripeS"))[0];
            paymentGsDiv.setAttribute("checked",false);
            paymentGpDiv.value = "paypal";
            var paymentGIn = angular.element(document.getElementById("paymentG"));
            paymentGIn.val("paypal");
            //init_stripe();
        }
        else{*/
            $scope.payG = 'stripe';
            
            $scope.paymentURL = Data.webAppBase()+"account/payment_process";
            var paymentactDiv = angular.element(document.getElementById("payment-form"))[0];
            paymentactDiv.setAttribute("action",Data.webAppBase()+"account/payment_process");
            //var paymentGpDiv = angular.element(document.getElementById("paypalS"))[0];
            //paymentGpDiv.setAttribute("checked",false);
            var paymentGsDiv = angular.element(document.getElementById("stripeS"))[0];
            paymentGsDiv.setAttribute("checked",true);
            paymentGsDiv.value = "stripe";
            var paymentGIn = angular.element(document.getElementById("paymentG"));
            paymentGIn.val("stripe");
            //init_stripe();
       // }

    };
    
    $scope.redirectPaypal = function(){
        
       var userId = $scope.userId;
         var amount =$scope.totalPayment;
          
          var planId = $scope.planChoosed;
          var orgId = $scope.organizationId;
          var tax = $scope.taxPayment;
          var stotal = $scope.subTotal;
          var nettotal = $scope.netAmount;
          var discountAmt = $scope.discountAmt;
          var promo = '';
          var promoNotes = '';
          var noOfUsers = $scope.nofUser;
          var planName = $scope.planChoosedName;
          var noOfmonths = '0';
          if($scope.selectPromo['promoCode'] !== undefined){
              noOfmonths = $scope.selectPromo.validity;
              promo = $scope.selectPromo.promoCode;
              promoNotes = $scope.selectPromo.discount+"_"+$scope.discountAmt;
          }else{
              noOfmonths = '12';
          }
          var gstIN = angular.element(document.getElementById("GSTNo"));
          var gstNo = gstIN.val();
        
        var paypalURL = Data.webAppBase()+"paypal/makePaymentUsingPayPal?uId="+userId+"&pId="+planId+"&oId="+orgId+"&sttotal="+stotal+"&amt="+amount+"&tax="+tax+"&nusr="+noOfUsers+"&nmon="+noOfmonths+"&netamt="+nettotal+"&promo="+promo+"&pronotes="+promoNotes+"&pName="+planName+"&TIN="+gstNo;
        
        window.location = paypalURL;
        
    };
    
    $scope.cancelPay = function () {

        $scope.firstDiv = true;
        $scope.secondDiv = false;
        $scope.thirdDiv = false;

    };
    $scope.payhistShow = function () {
        if ($scope.payhist) {
            $scope.payhist = false;
        } else {
            $scope.payhist = true;
        }
    };
    $scope.changePlan = function ()
    {
        if ($scope.plans === "1") {
            $scope.planamount = "$300";
            $scope.plancredit = "500";
            console.log($scope.planamount);
        } else if ($scope.plans === "2") {
            $scope.planamount = "$540";
            $scope.plancredit = "1000";
        } else if ($scope.plans === "3") {
            $scope.planamount = "$960";
            $scope.plancredit = "2000";
        }
    };
	$scope.promoCodePopUp = function(p,size){
      var modalInstance = $uibModal.open({
                    templateUrl: './promoCodepopUp',
                    controller: 'promoCodeCtrl',
                    size: 'sm',
                    backdrop: 'static',
                    resolve: {
                        promoCodes:function(){
                            return p;
                        }
                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        angular.forEach($scope.promoCodes,function(d){
                            //console.log(d);
                            //console.log(i);
                            if(d.promoCode === i){
                                $scope.selectPromo = d;
                                //console.log($scope.selectPromo);
                            }
                        });
                        //console.log(JSON.stringify($scope.plansel));
                        $scope.planSelectChoose(JSON.stringify($scope.plansel));
                    }
                });  
    };
});
evtApp.controller('pricingCtrl', function ($scope, $timeout, $uibModal, $rootScope, Data, $window, toaster, $http) {
    // sessionStorage.setItem('menuact', '7');
    // $rootScope.menuact = '7';
    if (sessionStorage.getItem("xtime") === "1") {
        $rootScope.firstProfile = true;
    } else {
        $rootScope.firstProfile = false;
    }
    $scope.loader = true;
    $scope.buycredits = function (plan, amount, credits) {
        $scope.organizationId = sessionStorage.getItem('orgId');
        $scope.userId = sessionStorage.getItem('userId');

        $window.location.href = Data.webAppBase() + "paypal/makePaymentUsingPayPal?oId=" + $scope.organizationId + "&uId=" + $scope.userId + "&amount=" + amount
                + "&credits=" + credits + "&planId=" + plan;


    };
    $scope.paymentURL = Data.webAppBase() + "/account/payment_process";
    // Intiating the stripe
    //init_stripe();
    $scope.plans = "1";
    $scope.mcurrency = 'USD';
    $scope.planamount = "$300";
    $scope.plancredit = "500";
    $rootScope.title = "Plans and Usage";
    $rootScope.addButtonhtml = "";
    $scope.payments = {};
    $scope.credits = {};
    $scope.payhist = false;
    $scope.planChoosed = "";
    $scope.planChoosedName = "";
    $scope.planChoosedPrice = "";
    $scope.planChoosedTaxPrice = "";
    $scope.planChoosedTotalPrice = "";
    $scope.planChoosedDiscountPrice = "";
    $scope.firstDiv = false;
    $scope.splan = true;
    $scope.secondDiv = false;
    $scope.thirdDiv = false;
    $scope.accounttype = sessionStorage.getItem('accountType');
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    console.log("Organization Id is " + $scope.organizationId);
    sessionStorage.setItem("menuact", '7');
    $rootScope.menuact = '7';
    $scope.nofUser = 0;
    $scope.curtime = new Date().getTime();
    $scope.minnofUser = 0;
    $scope.bcycle = "1";
    $scope.promoCodes = {};
    $scope.selectPromo = {};
    $scope.allPlanDetails = {};
    $scope.getCreditPayDet = function () {
        var resprom = $http.get(Data.serviceBase() + '/credit/credit', {params: {"key": "organizationId", "value": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.credits = response.data[0];
            console.log($scope.credits.multi_user);
            $scope.nofUser = parseInt($scope.credits.multi_user);
            $scope.minnofUser = parseInt($scope.credits.multi_user);
            console.log(response.data[0]);
            //$scope.eventSelected = response.data[0];


        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        $scope.orgName = sessionStorage.getItem('cOrgName');
        var resprom = $http.get(Data.serviceBase() + '/paymenthistory/paymenthistory', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.payments = response.data;
            $scope.loader = false;
            console.log(response.data);
            //$scope.eventSelected = response.data[0];


        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        $scope.orgDet = {};
        $scope.countryplan = {};
        var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {"key": "id", "value": $scope.organizationId}});
        resprom.then(function (response) {
            if (response.data) {
                $scope.orgDet = response.data[0];
                console.log("Org Det");
                $scope.planDet();
                //$scope.loader = false;
                console.log(response.data);
                //$scope.eventSelected = response.data[0];
                var uCountry = angular.element(document.getElementById("userCountry"));
                uCountry.val($scope.orgDet.country);



            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

        $scope.userCount = 0;
        var resprom = $http.get(Data.serviceBase() + '/user/getMaxUserCount', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            if (response.data) {
                $scope.userCount = response.data.userCount;
				$scope.noOfUser = $scope.userCount;
                console.log("User Count");
                //$scope.loader = false;
                console.log(response.data.userCount);
                //$scope.eventSelected = response.data[0];
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        var resprom = $http.get(Data.serviceBase() + '/payment/promoCodes', {params: {}});
        resprom.then(function (response) {
            if (response.data) {
                $scope.promoCodes = response.data;
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    $scope.upgradePlan = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('payment/setPlanUpgrage', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.proccedToPay = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('payment/setProccedToPay', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.payNow = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('payment/setPayNow', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };


    };
    $scope.checkDate = function (datedet) {

        if (!datedet) {
            return false;
        } else {
            var dateDet = new Date(datedet);
            var curDate = new Date();

            if (dateDet < curDate) {
                return true;

            } else {
                $scope.splan = false;
                return false;

            }
        }

    };
    $scope.planChoosedPrice = "";
    $scope.planc = {};
    $scope.planChoosedMonthPrice = "";
    $scope.CurrencyRate = "";
    $scope.getCurrency = function (toCur, type, amount) {
        if ($scope.mcurrency === 'INR') {
            $scope.mcurrency = "USD";
        } else {
            $scope.mcurrency = "INR";
        }


    };
    $scope.allPlanDetails = {};
    $scope.chplan = '';
    $scope.planDet = function () {

        var rescountprom = $http.get(Data.serviceBase() + '/payment/paymentByCountry', {params: {"country": $scope.orgDet.country}});
        rescountprom.then(function (response) {
            if (response.data) {
                $scope.countryplan = response.data;

                var resprom = $http.get(Data.serviceBase() + '/organization/planDet', {});
                resprom.then(function (response) {
                    if (response.data) {
                        $scope.allPlanDetails = response.data;

                        angular.forEach($scope.allPlanDetails, function (d, key) {
                            console.log(d)
                            if (key === 1) {
                                d.planMonAmount = parseInt($scope.countryplan.small);
                                d.planAnAmount = parseInt($scope.countryplan.small) * 12;
                                /*if ($scope.userCount <= d.multi_users) {
                                    $scope.planc = JSON.stringify(d);
                                    $scope.chplan = '1';
                                    $scope.selectPlan(d);
                                }*/
                            }
                            if (key === 2) {
                                d.planMonAmount = parseInt($scope.countryplan.medium);
                                d.planAnAmount = parseInt($scope.countryplan.medium) * 12;
                                /*if ($scope.userCount <= d.multi_users && $scope.chplan === '') {
                                    $scope.planc = JSON.stringify(d);
                                    $scope.chplan = '2';
                                    $scope.selectPlan(d);
                                }*/
                            }
                            if (key === 3) {
                                d.planMonAmount = parseInt($scope.countryplan.large);
                                d.planAnAmount = parseInt($scope.countryplan.large) * 12;
                                /*if ($scope.chplan === '' && $scope.userCount <= parseInt(d.multi_users)) {
                                    $scope.planc = JSON.stringify(d);
                                    $scope.chplan = '3';
                                    $scope.selectPlan(d);
                                }*/
                            }
                            if (key === 4) {

                                d.planMonAmount = parseInt($scope.countryplan.xlarge);
                                d.planAnAmount = parseInt($scope.countryplan.xlarge) * 12;
                                console.log(d.planMonAmount);
                                /*if($scope.userCount <= parseInt(d.multi_users)){
                                 
                                 $scope.planc = JSON.stringify(d);
                                 $scope.chplan = '4';
                                 console.log($scope.chplan);
                                 $scope.selectPlan(d);
                                 }*/
                            }
                            if (key === 5) {
                                d.planMonAmount = parseInt($scope.countryplan.xxlarge);
                                d.planAnAmount = parseInt($scope.countryplan.xxlarge) * 12;
                                console.log(d.planMonAmount);
                                /* if($scope.userCount <= parseInt(d.multi_users) && $scope.chplan === ''){
                                 $scope.planc = JSON.stringify(d);
                                 $scope.chplan = '5';
                                 $scope.selectPlan(d);
                                 
                                 }*/

                            }
                        });
                       
                        $scope.setUsersbasedPlan();
                        

                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });





    };

    $scope.setUsersbasedPlan = function () {
        console.log(parseInt($scope.userCount) > parseInt($scope.allPlanDetails[3].multi_users) && parseInt($scope.userCount) <= parseInt($scope.allPlanDetails[4].multi_users));
        if (parseInt($scope.userCount) <= parseInt($scope.allPlanDetails[1].multi_users)) {
            $scope.chplan = '1';
            $scope.planc = JSON.stringify($scope.allPlanDetails[1]);
            $scope.selectPlan(JSON.stringify($scope.allPlanDetails[1]));
            //$scope.planc = $scope.allPlanDetails[1];

        } else if (parseInt($scope.userCount) > parseInt($scope.allPlanDetails[1].multi_users) && parseInt($scope.userCount) <= parseInt($scope.allPlanDetails[2].multi_users)) {
            $scope.chplan = '2';

            $scope.planc = JSON.stringify($scope.allPlanDetails[2]);
            $scope.selectPlan(JSON.stringify($scope.allPlanDetails[2]));
            // $scope.planc = $scope.allPlanDetails[1];

        } else if (parseInt($scope.userCount) > parseInt($scope.allPlanDetails[2].multi_users) && parseInt($scope.userCount) <= parseInt($scope.allPlanDetails[3].multi_users)) {
            $scope.chplan = '3';
            $scope.planc = JSON.stringify($scope.allPlanDetails[3]);
            $scope.selectPlan(JSON.stringify($scope.allPlanDetails[3]));

            //$scope.planc = $scope.allPlanDetails[3];

        } else if (parseInt($scope.userCount) > parseInt($scope.allPlanDetails[3].multi_users) && parseInt($scope.userCount) <= parseInt($scope.allPlanDetails[4].multi_users)) {
            $scope.chplan = '4';
            console.log($scope.allPlanDetails[1].multi_users + " ------" + $scope.userCount);
            $scope.planc = JSON.stringify($scope.allPlanDetails[4]);
            console.log("Inside 25 leads");
            $scope.selectPlan(JSON.stringify($scope.allPlanDetails[4]));
            //$scope.planc = JSON.parse($scope.allPlanDetails[4]);

        } else if (parseInt($scope.userCount) > parseInt($scope.allPlanDetails[4].multi_users)) {
            $scope.chplan = '5';
            $scope.planc = JSON.stringify($scope.allPlanDetails[5]);
            $scope.selectPlan(JSON.stringify($scope.allPlanDetails[5]));
            // $scope.planc = $scope.allPlanDetails[5];

        }
        console.log($scope.planc);

    };

    var paymentGate = angular.element(document.getElementById("paymentG"));
    paymentGate.val("stripe");
    $scope.plansel = {};
    $scope.nettotal = 0;
    $scope.totalpay = 0;
    $scope.taxPay = 0
    $scope.nmonths = 12;
	$scope.discountAmt= '0';
    $scope.selectPlan = function (plan) {
        console.log(plan);
        $scope.plansel = JSON.parse(plan);
        $scope.planChoosed = $scope.plansel.planNameId;
        $scope.planChoosedName = $scope.plansel.planName;
        $scope.nettotal = $scope.plansel.planMonAmount * $scope.nmonths;
		if ($scope.selectPromo['promoCode'] !== undefined) {
            console.log("Promo");
            $scope.discountAmt = ((parseFloat($scope.nettotal) * $scope.selectPromo.discount) / 100).toFixed(0);
            $scope.netAmount = (parseFloat($scope.nettotal) - parseFloat($scope.discountAmt)).toFixed(0);
            $scope.nmonths = $scope.selectPromo.validity;

        } else{
             console.log("non Promo");
            $scope.netAmount = $scope.nettotal;
        }
		if($scope.orgDet.country === 'India'){
        $scope.taxPay = ((parseFloat($scope.netAmount) * 18) / 100).toFixed(0);
        $scope.totalpay = (parseFloat($scope.netAmount) + parseFloat($scope.taxPay)).toFixed(0);
		}else{
			$scope.taxPay = '-';
			$scope.totalpay =$scope.netAmount;
		}
        if($scope.orgDet.country === 'India'){
            $scope.payG = 'InstaMojo';
            $scope.paymentURL = 'https://beta.helloleads.io/index.php/app/account/pricing_process';
        }else{
			$scope.payG = 'stripe';
            $scope.paymentURL = Data.webAppBase() + "account/payment_process";
            var paymentactDiv = angular.element(document.getElementById("payment-form"))[0];
            paymentactDiv.setAttribute("action", Data.webAppBase() + "account/payment_process");
            var paymentGIn = angular.element(document.getElementById("paymentG"));
            paymentGIn.val("stripe");
            init_stripe();
		}
    };
    
    $scope.checkUserCount = function (cuser, auser) {
        if (auser && cuser) {
            $scope.userChkError = false;
            //console.log(cuser + " " + parseInt(auser));
            if (parseInt(cuser) > parseInt(auser)) {
                $scope.userChkError = true;
                return true;
            } else if (cuser > 100) {
                $scope.userChkError = true;
                return true;
            } else {
                $scope.userChkError = false;
                return false;
            }
        }
        //console.log(cuser);
    };

    $scope.trailDate = function (expiryDate) {
        var curDate = new Date();
        var edate = new Date(expiryDate);
        edate.setDate(edate.getDate() - 1);
        var timeDiff = Math.abs(curDate.getTime() - (edate.getTime()));
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    }

    $scope.checkout = function () {

        $scope.firstDiv = false;
        $scope.secondDiv = false;
        $timeout(function () {}, 900);
        $scope.thirdDiv = true;

    };
    $scope.cancelUser = function () {

        $scope.firstDiv = false;
        $scope.secondDiv = false;
        $scope.thirdDiv = false;

    };
    $scope.chooseUser = function () {

        $scope.firstDiv = false;
        $scope.secondDiv = false;
        $scope.thirdDiv = true;

    };
    $scope.renewPlan = function (plan) {
        if (plan) {
            $scope.firstDiv = false;
            $scope.secondDiv = false;
            $scope.planChoosed = plan.planId;
            $scope.planChoosedName = plan.paymentNotes;
            if (plan.noOfMonth === '12') {
                // $scope.planChoosedPrice = plan.amount;
                $scope.bcycle = '1';
                var monthDiv = angular.element(document.getElementById("totmonths"));
                monthDiv.val(plan.noOfMonth);
            } else {
                // $scope.planChoosedMonthPrice = plan.amount;
                $scope.bcycle = '2';
                var monthDiv = angular.element(document.getElementById("totmonths"));
                monthDiv.val(plan.noOfMonth);
            }
            $scope.nofUser = parseInt(plan.noOfUser);
            var userDiv = angular.element(document.getElementById("numberofuser"));
            userDiv.val(plan.noOfUser);
            var amountDiv = angular.element(document.getElementById("NetPrice"));
            amountDiv.val(plan.amount);
            var totpriceDiv = angular.element(document.getElementsByClassName("totPrice"));
            totpriceDiv.text("USD " + plan.amount);
            var totamtDiv = angular.element(document.getElementById("totalPramount"));
            totamtDiv.text("USD " + plan.totalAmount);
            var totamtInt = angular.element(document.getElementById("totalPamount"));
            totamtInt.val(plan.totalAmount);
            var taxAmountDiv = angular.element(document.getElementById("taxPramount"));
            taxAmountDiv.text("USD " + plan.taxAmount);
            var taxAmountInt = angular.element(document.getElementById("netTax"));
            taxAmountInt.val(plan.taxAmount);
            $scope.splan = true;
            $scope.thirdDiv = true;
            // $scope.planc = plan;




            // calculatePrice($scope.planChoosedPrice, $scope.planChoosedMonthPrice);
        }

    };

    $scope.changeUserCount = function () {
        $scope.planSelectChoose(JSON.stringify($scope.plansel));
    };

    $scope.chooseGateWay = function (payG) {
        
        if($scope.orgDet.country !== 'India'){
        // alert(payG);
        if (payG === 'paypal') {
            $scope.payG = 'paypal';
            $scope.paymentURL = Data.webAppBase() + "paypal/makePaymentUsingPayPal";

            var paymentactDiv = angular.element(document.getElementById("payment-form"))[0];
            paymentactDiv.setAttribute("action", Data.webAppBase() + "paypal/makePaymentUsingPayPal");
            var paymentGpDiv = angular.element(document.getElementById("paypalS"))[0];
            paymentGpDiv.setAttribute("checked", true);
            var paymentGsDiv = angular.element(document.getElementById("stripeS"))[0];
            paymentGsDiv.setAttribute("checked", false);
            paymentGpDiv.value = "paypal";
            var paymentGIn = angular.element(document.getElementById("paymentG"));
            paymentGIn.val("paypal");
            //init_stripe();
        } else {
            $scope.payG = 'stripe';

            $scope.paymentURL = Data.webAppBase() + "account/payment_process";
            var paymentactDiv = angular.element(document.getElementById("payment-form"))[0];
            paymentactDiv.setAttribute("action", Data.webAppBase() + "account/payment_process");
            //var paymentGpDiv = angular.element(document.getElementById("paypalS"))[0];
            //paymentGpDiv.setAttribute("checked",false);
            //var paymentGsDiv = angular.element(document.getElementById("stripeS"))[0];
            //paymentGsDiv.setAttribute("checked", true);
            //paymentGsDiv.value = "stripe";
            var paymentGIn = angular.element(document.getElementById("paymentG"));
            paymentGIn.val("stripe");
            init_stripe();
        }
    }

    };

    $scope.redirectPaypal = function () {

        var userId = $scope.userId;
        var amount = $scope.totalPayment;

        var planId = $scope.planChoosed;
        var orgId = $scope.organizationId;
        var tax = $scope.taxPayment;
        var stotal = $scope.subTotal;
        var nettotal = $scope.netAmount;
        var discountAmt = $scope.discountAmt;
        var promo = '';
        var promoNotes = '';
        var noOfUsers = $scope.nofUser;
        var planName = $scope.planChoosedName;
        var noOfmonths = '0';
        if ($scope.selectPromo['promoCode'] !== undefined) {
            noOfmonths = $scope.selectPromo.validity;
            promo = $scope.selectPromo.promoCode;
            promoNotes = $scope.selectPromo.discount + "_" + $scope.discountAmt;
        } else {
            noOfmonths = '12';
        }
        var gstIN = angular.element(document.getElementById("GSTNo"));
        var gstNo = gstIN.val();

        var paypalURL = Data.webAppBase() + "paypal/makePaymentUsingPayPal?uId=" + userId + "&pId=" + planId + "&oId=" + orgId + "&sttotal=" + stotal + "&amt=" + amount + "&tax=" + tax + "&nusr=" + noOfUsers + "&nmon=" + noOfmonths + "&netamt=" + nettotal + "&promo=" + promo + "&pronotes=" + promoNotes + "&pName=" + planName + "&TIN=" + gstNo;

        window.location = paypalURL;

    };

    $scope.cancelPay = function () {

        $scope.firstDiv = true;
        $scope.secondDiv = false;
        $scope.thirdDiv = false;

    };
    $scope.payhistShow = function () {
        if ($scope.payhist) {
            $scope.payhist = false;
        } else {
            $scope.payhist = true;
        }
    };
    $scope.changePlan = function ()
    {
        if ($scope.plans === "1") {
            $scope.planamount = "$300";
            $scope.plancredit = "500";
            //console.log($scope.planamount);
        } else if ($scope.plans === "2") {
            $scope.planamount = "$540";
            $scope.plancredit = "1000";
        } else if ($scope.plans === "3") {
            $scope.planamount = "$960";
            $scope.plancredit = "2000";
        }
    };
     $scope.promoCodePopUp = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './promoCodepopUp',
            controller: 'promoCodeCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                promoCodes: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                angular.forEach($scope.promoCodes, function (d) {
                    //console.log(d);
                    //console.log(i);
                    if (d.promoCode === i) {
                        $scope.selectPromo = d;
                        //console.log($scope.selectPromo);
                    }
                });
                //console.log(JSON.stringify($scope.plansel));
                $scope.selectPlan(JSON.stringify($scope.plansel));
            }
        });
    };
});








evtApp.controller('promoCodeCtrl', function ($scope, Data, $http, $uibModalInstance,promoCodes,toaster) {
     $scope.promoText = '';
     $scope.promoerror = false;
     $scope.applyCode=function(p){
         $uibModalInstance.close(p);
     };
     $scope.onPromo = function(p){
         var chkflag = 0;
         angular.forEach(promoCodes,function(d){
             if(d.promoCode === p){
                 chkflag = 1;
             }
         });
         if(chkflag !== 1){
              $scope.promoerror = true;
         }else{
             $scope.promoerror = false;
         }
     };
      $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
 });  

   