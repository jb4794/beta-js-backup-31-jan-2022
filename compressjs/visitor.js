evtApp.controller('dispvisitorCtrl', function ($scope, toaster, alertUser,$interval, checkPremium,trialPopUp, $rootScope, $location, $window, Data, $uibModal, visitorShareData, $http, DTOptionsBuilder) {
    $rootScope.emptyvisitor = {};
   // $rootScope.emptyvisitor.sendGreet = "0";
	//$rootScope.emptyvisitor.lead_share = "0";
    $scope.visitors = {};
    $scope.visitor = {};
    $scope.adfilter = false;
    $scope.filterby = false;
    $scope.visitorData = {};
	$scope.sortvisitorBy = "";
    $scope.sortvisitorDetBy = "";
    $scope.filterPotentialSelected = "";
    $scope.filterAssignToSelected = "";
    $scope.filterStageSelected = "";
    $scope.curDate = new Date();
    $scope.curTenDate = new Date();
    $scope.curTenDate.setDate($scope.curDate.getDate() + 10);
    $scope.grid = false;
    $scope.tableColor = "#e7eaec";
    $scope.gridColor = "";
    $scope.loader = true;
    $scope.addvisitorflag = false;
    $scope.searchRt = false;
    $scope.filterlabel = "";
    $scope.filterindex = "";
	$scope.selectAllFlag = true;
    $scope.stageSelected = [];
    $scope.eventSelected = [];
    $scope.userId = sessionStorage.getItem('userId');
    sessionStorage.setItem('menuact', '3');
    $rootScope.menuact = sessionStorage.getItem('menuact');
    $scope.stageCount = 0;
	$scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }

    $scope.eventCount = 0;
	
	$scope.selectedPCols = [];
    $scope.filteredColumn = [];
	$scope.Lstages = [];
    // ng-repeat object to get column value 'visitor'
    var ngobject = 'visitor';
    //Column Selection and arrangements

    $scope.selectedPCols = ['Organization', 'List', 'Potential', 'Stage', 'Deal Size', 'Lead Score', 'Customer Group', 'Assigned To', 'Recent Activity', 'Posted By', 'Posted On', 'Last Modified'];
    $scope.allmappedCols = [{'lable': "Organization", "column": "visitorOrganizationName", "sort": "1"}, {'lable': "List", 'column': "eventName", "sort": "1"}, {'lable': "Potential", 'column': "potential", "sort": "1"}, {'lable': "Stage", 'column': "type", "sort": "1"}, {'lable': "Deal Size", 'column': "dealSize", "sort": "1"}, {'lable': "Lead Score", 'column': "lead_score", "sort": "1"}, {'lable': "Customer Group", 'column': "categories", "sort": "1"}, {'lable': "Assigned To", 'column': "gAssignUser", "sort": "1"}, {'lable': "Designation", 'column': "designation", "sort": "1"}, {'lable': "Email", 'column': "email", "sort": "1"}, {'lable': "Mobile", 'column': "mobile", "sort": "1"},  {'lable': "City", 'column': "city", "sort": "1"}, {'lable': "State", 'column': "state", "sort": "1"}, {'lable': "Country", 'column': "country", "sort": "1"}, {'lable': "Captured By", 'column': "ufname", "sort": "1"}, {'lable': "Captured On", 'column': "createdAt", "sort": "1"}, {'lable': "Recent Activity", 'column': "activity", "sort": "1"}, {'lable': "Posted By", 'column': "postedBy", "sort": "1"}, {'lable': "Posted On", 'column': "postedDate", "sort": "1"}, {'lable': "Last Modified", 'column': "modifiedAt", "sort": "1"},{'lable': "Tags", 'column': "tags", "sort": "1"}];
    $scope.columnArrange = function () {
        $scope.filteredColumn = [];
        angular.forEach($scope.allmappedCols, function (d) {

            if ($scope.selectedPCols.indexOf(d.lable) !== -1) {
                // d.column = ngobject+"."+d.column;
                $scope.filteredColumn[$scope.selectedPCols.indexOf(d.lable)] = d;
            } else {

            }

        });
		
		
		var resprom11 = $http.get(Data.serviceBase() + '/visitor/getCustom', {params: {"organizationid": $scope.organizationId}});
		resprom11.success(function(res)
		{
			angular.forEach(res,function(response)
			{
			  var resprom12 =$http.get(Data.serviceBase() + '/user/getCustomColumn', {params: {"userId": $scope.userId}});
			  resprom12.success(function(res2)
			  {
				 angular.forEach(res2[0].customColumn.split(','),function(response2)
				 { 
				 if(response2==response.name)
					{
						if($scope.selectedPCols.indexOf(response.name) !== -1) 
						{
							$scope.filteredColumn[$scope.selectedPCols.indexOf(response.name)] = {'lable': response.name, 'column': response.name, "sort": "1"};
							
						}
					}
				});
			  });
			});
		});


    };

// Get the custom column for user from server-----------------
$scope.getCustomColumn = function () {
    var columnresp = $http.get(Data.serviceBase() + '/user/getCustomColumn', {params: {"userId": $scope.userId}});
    columnresp.then(function (response) {
        console.log(response.data);
        if (response.data[0].customColumn !== null && response.data[0].customColumn !== "") {
            if (response.data[0].customColumn.indexOf(',') !== -1) {
                $scope.selectedPCols = response.data[0].customColumn.split(',');
            } else {
                $scope.selectedPCols = response.data[0].customColumn;
            }

        }

        $scope.columnArrange();
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
};
  $scope.getleadStage = function () {
        // Get the custom lead stages -----------------
        var stageresp = $http.get(Data.serviceBase() + '/organization/getCurrentStages', {params: {"organizationId": $scope.organizationId}});
        stageresp.then(function (response) {
            console.log(response.data);
            $scope.Lstages = response.data;


        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    };

    $scope.getAssignUsers = function () {
        // Get the assign users infromations -----------------
        var stageresp = $http.get(Data.serviceBase() + '/user/user', {params: {key:"organizationId","value": $scope.organizationId}});
        stageresp.then(function (response) {
            console.log(response.data);
            $scope.assingUsers = response.data;
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    };
	
	$scope.dayDiff = function (firstDate, secondDate) {
        var date2 = new Date(secondDate);
        var date1 = new Date(firstDate);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    };

    if (localStorage.getItem("filters")) {
        ////console.log("Filetr Exisit");
        $scope.filtersadded = JSON.parse(localStorage.getItem("filters"));
    } else {
        ////console.log("Filetr not Exisit");
        $scope.filtersadded = {
            "cgroup": "", "service": "", "potential": "", "event": "", "eventId": "", "assignedTo": "", "assignedBy": "", "capturedBy": "", "designation": "", "company": "", "city": "", "state": "", "country": "",
            "fromDate": "", "toDate": "", "dontFollow": "", "stage": "", "assignToId": "", "assigneeId": "", "capturedId": "", "notes": "", "tags": "", "drfromDate": "", "drtoDate": "","notAssigned":"", "deal": "", "dealComp": "" , "custList": "","emailStatus":"", "stageIds": "",
             "gtime": "", "gsign": ""

        };
        $scope.filterEventSelected = {};
        $scope.filterStageSelected = "";
    }

    sessionStorage.setItem('visitors', '');
    $scope.organizationId = sessionStorage.getItem('orgId');
    $rootScope.currentEventId = localStorage.getItem('curEventId');
    $rootScope.currentEventName = localStorage.getItem('curEventName');
    $scope.filterlabel = sessionStorage.getItem('filterlabel');
    $scope.filterindex = sessionStorage.getItem('filterindex');
    $scope.copyeventName = $rootScope.currentEventName;
    $scope.copyeventId = $rootScope.currentEventId;
    $rootScope.title = "All Leads";
    $rootScope.addButtonhtml = "";
    //Default Page number
    $scope.pageNumber = 1;
    $scope.totalVisitors = 0;
    $scope.totalPage = 1;
    $scope.usersPerPage = 20;
    if (sessionStorage.getItem("xtime") === "1") {
        $rootScope.firstProfile = true;
    } else {
        $rootScope.firstProfile = false;
    }
    if ($rootScope.currentEventName === "Default Event") {
        $rootScope.addButtonhtml = "";
    } else {
        $rootScope.addButtonhtml = '';
    }
////console.log("Filters" + localStorage.getItem("filters"));
    /* if (localStorage.getItem("filters")) {
     $scope.filtersadded = JSON.parse(localStorage.getItem("filters"));
     //console.log($scope.filtersadded);
     }*/
	
    $rootScope.addButtonhtml = '&nbsp;<button class="btn btn-default btn-sm btn-circle" popover="The person who is interested in your product and service for this event." popover-title="Manage Leads" popover-trigger="focus" popover-placement="bottom"><i class="fa fa-question"></i></button>';
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('<"html5buttons"B>Tgtpi')
            .withDataProp('data')
            .withButtons([
                /* {extend: 'copy', exportOptions: {
                 columns: [0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12]
                 }},*/
                {extend: 'csv', text: '<i class="fa fa-download"></i> Download as CSV', title: 'HLS- Leads', exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                    }},
                {extend: 'excel', text: '<i class="fa fa-download"></i> Download as Excel', customize: function () {
                        $(this).addClass('btn-primary');
                    }, title: 'HLS- Leads', exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                    }},
                /*  {extend: 'pdf', title: 'HL-Visitors-' + $scope.currentEventName, exportOptions: {
                 columns: [0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16,17,18]
                 }},
                 {extend: 'print',
                 customize: function (win) {
                 $(win.document.body).addClass('white-bg');
                 $(win.document.body).css('font-size', '10px');
                 
                 $(win.document.body).find('table')
                 .addClass('compact')
                 .css('font-size', 'inherit');
                 },
                 exportOptions: {
                 columns: [0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16,17,18]
                 }
                 }*/
            ])
            .withDisplayLength(20);
    $scope.eventLists = {};
    $scope.getAllEvents = function () {
        var eventresp = $http.get(Data.serviceBase() + '/event/event', {params: {"key": "organizationId", "value": $scope.organizationId}});
        eventresp.then(function (response) {
            ////console.log(response.data);
            $scope.eventLists = response.data;
            ////console.log("Current event1" + $rootScope.currentEventId);
            angular.forEach($scope.eventLists, function (d) {
                if (d.id === localStorage.getItem('curEventId')) {
                    //localStorage.setItem("filters","");
                    $scope.filterEventSelected = d;
                    $scope.adfilter = true;
                    $scope.filterby = true;
                    $scope.filtersadded.event = $rootScope.currentEventName;
                } else if ($scope.filtersadded !== undefined) {
                    if (d.id === $scope.filtersadded.eventId) {
                        $scope.filterEventSelected = d;
                        $scope.adfilter = true;
                        $scope.filterby = true;
                        $scope.filtersadded.event = d.name;
                    }
                }

            });
            ////console.log($scope.filterEventSelected);
            if ($scope.pageNumber === 1) {
                //console.log("Get All events");
                $scope.filterEventChanged();
            }
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    };
	$scope.orgDetails = {};
	
    // --------------------------------------------------------------- Getting Currency details ----------------------------------------------------
	$scope.getCurrenyDetails = function () {
    var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

    resprom.then(function (response) {
        if (response.data) {
            $scope.orgDetails = response.data[0];
        }

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
	};
	// Multi select on leads to assign , delete and move list --------------------------------------------------------------------------------------
    $scope.selectedVisitor = [];
    $scope.toggleSelection = function (svisitor) {
        var falseflag = 0;
        angular.forEach($scope.visitors, function (visitor) {
            if (visitor.select === true) {
                falseflag = falseflag + 1;
            }
        });
        if (falseflag === $scope.visitors.length) {
            $scope.checkstate = true;
        } else {
            $scope.checkstate = false;
        }
        if (svisitor.select === true) {

            if ($scope.selectedVisitor.indexOf(svisitor.id) <= -1) {
                //console.log("Select True" + svisitor.id);
                $scope.selectedVisitor.push(svisitor.id);
            }

        }
        if (svisitor.select === false) {
            if ($scope.selectedVisitor.indexOf(svisitor.id) > -1) {
                //console.log("Select False" + svisitor.id);
                $scope.selectedVisitor.splice($scope.selectedVisitor.indexOf(svisitor.id), 1);
            }

        }


    };
    $scope.checkAll = function () {
        if ($scope.checkstate)
        {
            $scope.checkstate = false;
            $scope.selectedVisitor = [];
            $scope.selectedVisitor.length = 0;
        } else {
            $scope.checkstate = true;
        }
        $scope.selectedVisitor = [];
        $scope.selectedVisitor.length = 0;

        angular.forEach($scope.visitors, function (visitor) {
            visitor.select = $scope.checkstate;
            if ($scope.checkstate) {
                $scope.selectedVisitor.push(visitor.id);
            }
        });

    };

    $scope.clearAllVisitorSelection = function () {
        $scope.selectAllFlag = true;
        $scope.checkstate = false;
        angular.forEach($scope.visitors, function (visitor) {
            visitor.select = $scope.checkstate;

        });
        $scope.selectedVisitor = [];
        $scope.selectedVisitor.length = 0;
    }
    // --------------------------------------------- End of multi selection ------------------------------------------------------------------------



    /*if ($scope.filterlabel && $scope.filterEventSelected) {
     //$scope.searchText = $scope.filterlabel;
     $scope.visitors.length = 0;
     //console.log("Called For Event with Filter");
     $scope.filterEventChanged();
     }*/
    /* //Checking Credits
     var respromc = $http.get(Data.serviceBase() + '/credit/credit', {params: {"key":"organizationId","value":organizationId}});
     respromc.then(function (response) {
     $rootScope.credits = response.data[0];
     //console.log(response.data[0]);
     //$scope.eventSelected = response.data[0];
     
     
     }, function (response) {
     //console.log('Error happened -- ');
     //console.log(response);
     });*/
    ////console.log("Current event Id is " + $rootScope.currentEventId);
    ////console.log("Organization Id is " + $scope.organizationId);

    ////console.log($scope.visitors);

    //Event Change Filter Stage change filter 
	$scope.fieldSelToggle = "";
    $scope.fieldSelSymbolToggle = {};
    $scope.sortSetList = function (field) {
        console.log("Field Name "+field);
        $scope.sortvisitorDetBy = "";
        if ($scope.fieldSelSymbolToggle[field] === undefined) {
            $scope.fieldSelSymbolToggle[field] = undefined;
            var keys = Object.keys($scope.fieldSelSymbolToggle);
            if (keys.length > 1) {
                $scope.fieldSelSymbolToggle = {};
				$scope.fieldSelToggle = "";
				
            }
            //console.log(keys);
            $scope.fieldSelSymbolToggle[field] = undefined;
			$scope.fieldSelToggle = "";
        }
        var filedUp = field + "Up";
        var filedDown = field + "Down";
        if ($scope.fieldSelToggle === "" && $scope.fieldSelSymbolToggle[field] === undefined) {
            $scope.fieldSelToggle = "Up";
            //$scope.fieldSelSymbolToggle[field] = "Up";
        }


        //console.log($scope.fieldSelSymbolToggle);
        if (field + $scope.fieldSelToggle === filedUp) {
            $scope.sortvisitorBy = filedUp;
            $scope.fieldSelToggle = filedDown;
            $scope.filterEventChanged('1');
            $scope.fieldSelToggle = "Down";
            $scope.fieldSelSymbolToggle[field] = "Up";
        } else if (field + $scope.fieldSelToggle === filedDown) {
            $scope.sortvisitorBy = filedDown;
            $scope.fieldSelToggle = filedUp;
            $scope.filterEventChanged('1');
            $scope.fieldSelToggle = "";
            //$scope.fieldSelSymbolToggle = "Down";
            $scope.fieldSelSymbolToggle[field] = "Down";

        } else {
            $scope.sortvisitorBy = "";
            $scope.fieldSelToggle = "";
            //$scope.fieldSelSymbolToggle = "";
            $scope.fieldSelSymbolToggle[field] = undefined;
            $scope.filterEventChanged('1');
        }
    };
	
	$scope.mailCount = '0';
    $scope.sendSepEmail = function (p, size) {
		
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "bulkEmail", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    console.log(response.data);
                    if (response.data.status === 'success') {
							$scope.vemailCount = 0;
							// $scope.checkAccountMailCount();
							var mailcountresp = $http.get(Data.serviceBase() + '/visitor/checkMailCount', {params: {"orgId": $scope.organizationId}});
							mailcountresp.then(function (response) {
								console.log(response);
								$scope.mailCount = response.data.count;
								var respromc = $http.get(Data.serviceBase() + '/visitor/checkEmailByIds', {params: {"visitorlist": $scope.selectedVisitor.join()}});
								respromc.then(function (response) {
									$scope.vemails = response.data;
									var emailCount = 0;
									if ($scope.vemails !== '') {
										emailCount = $scope.vemails.split(',').length;
									}
									console.log(emailCount);
									console.log($scope.mailCount);
									if (emailCount > (1000 - parseInt($scope.mailCount))) {

										$scope.message = {"title": "Warning", "message": "Selected leads with email: " + emailCount + "<br>Current daily email credits : " + $scope.mailCount + "<br><br> You are running out of daily Email credits(1000). Kindly re-target your recipients."};
										alertUser.alertpopmodel($scope.message);
									} else if (emailCount === 0) {
										$scope.message = {"title": "Warning", "message": "EmailID does not exists for the selected lead(s). Kindly re-target your recipients."};
										alertUser.alertpopmodel($scope.message);
									} else {
										//console.log(response.data[0]);
										//$scope.eventSelected = response.data[0];
										var modalInstanceassignTo = $uibModal.open({
											templateUrl: './emailSepTemplate',
											controller: 'emailCtrlSep',
											size: 'lg',
											backdrop: 'static',
											resolve: {
												vvisitorlist: function () {
													return $scope.vemails;
												},
												orgCount: function () {
													return $scope.selectedVisitor.length;
												},
												mailCount: function () {
													return $scope.mailCount;
												},
												subject: function () {
													return p && p.subject != undefined ? p.subject : '';
												},
												message: function () {
													return p && p.message != undefined ? p.message : '';
												},
												visitorEmailId: function () {
													return p && p.email != undefined ? p.email : '';
												}
											}
										});
										modalInstanceassignTo.result.then(function (i) {
											if (i) {
												$scope.checkstate = false;
												$scope.selectedVisitor = [];
												$scope.selectedVisitor.length = 0;
												$scope.filterEventChanged();
											}
										});
									}

								}, function (response) {
									//console.log('Error happened -- ');
									//console.log(response);
								});
							}, function (response) {
								//console.log('Error happened -- ');
								//console.log(response);
							});
					} else {
                        response.data['cancel'] = 'true';
                        $scope.orgsendEmailPlanCheck(response.data);
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });		

    };

    $scope.setBulkEmailCount = function () {
         $scope.cEmail = sessionStorage.getItem('userEmail');
         $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setBulkEmailCount', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    }

    $scope.setBulkSmsCount = function () {
         $scope.cEmail = sessionStorage.getItem('userEmail');
         $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setBulkSmsCount', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    }
	
	$scope.orgsendEmailPlanCheck = function (p, size) {
        var modalInstance1 = $uibModal.open({
            templateUrl: './checkPremiumPopup',
            controller: 'premiumCheckCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                msg: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };
    $scope.checkAccountMailCount = function () {

        var mailcountresp = $http.get(Data.serviceBase() + '/visitor/checkMailCount', {params: {"orgId": $scope.organizationId}});
        mailcountresp.then(function (response) {
            console.log(response);
            $scope.mailCount = response.data.count;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
	  // SMS Sending template
    $scope.mailCount = '0';
    $scope.sendSepSms = function (p, size) {

        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "bulkSMS", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    console.log(response.data);
                    if (response.data.status === 'success') {
                        $scope.vemailCount = 0;
                        console.log($scope.selectedVisitor.join());
                        // $scope.checkAccountMailCount();
                        var mailcountresp = $http.get(Data.serviceBase() + '/visitor/checkMobileCount', {params: {"orgId": $scope.organizationId}});
                        mailcountresp.then(function (response) {
                            console.log(response);
                            $scope.mailCount = response.data.count;
                            var respromc = $http.get(Data.serviceBase() + '/visitor/checkMobileByIds', {params: {"visitorlist": $scope.selectedVisitor.join()}});
                            respromc.then(function (response) {
                                $scope.vmobiles = response.data;
                                var smsCount = 0;
                                if ($scope.vmobiles !== '') {
                                    smsCount = $scope.vmobiles.split(',').length;
                                }
                                console.log(smsCount);
                                console.log($scope.mailCount);
                                if (smsCount > (10000 - parseInt($scope.mailCount))) {

                                    $scope.message = {"title": "Warning", "message": "Selected leads with mobile number: " + smsCount + "<br>Current daily sms credits : " + $scope.mailCount + "<br><br> You are running out of daily Email credits(10000). Kindly re-target your recipients."};
                                    alertUser.alertpopmodel($scope.message);
                                } else if (smsCount === 0) {
                                    $scope.message = {"title": "Warning", "message": "Mobile number does not exists for the selected lead(s). Kindly re-target your recipients."};
                                    alertUser.alertpopmodel($scope.message);
                                } else {
                                    //console.log(response.data[0]);
                                    //$scope.eventSelected = response.data[0];
                                    var modalInstanceassignTo = $uibModal.open({
                                        templateUrl: './smsSepTemplate',
                                        controller: 'smsCtrlSep',
                                        size: 'lg',
                                        backdrop: 'static',
                                        resolve: {
                                            vvisitorlist: function () {
                                                return $scope.vmobiles;
                                            },
                                            orgCount: function () {
                                                return $scope.selectedVisitor.length;
                                            },
                                            mailCount: function () {
                                                return $scope.mailCount;
                                            },
                                            message: function () {
                                                return p && p.message != undefined ? p.message : '';
                                            }
                                        }
                                    });
                                    modalInstanceassignTo.result.then(function (i) {
                                        if (i) {
                                            $scope.checkstate = false;
                                            $scope.selectedVisitor = [];
                                            $scope.selectedVisitor.length = 0;
                                            $scope.filterEventChanged();
                                        }
                                    });
                                }

                            }, function (response) {
                                //console.log('Error happened -- ');
                                //console.log(response);
                            });
                        }, function (response) {
                            //console.log('Error happened -- ');
                            //console.log(response);
                        });
                    } else {
                        response.data['cancel'] = 'true';
                        $scope.orgsendEmailPlanCheck(response.data);
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.checkAccountMobileCount = function () {

        var mailcountresp = $http.get(Data.serviceBase() + '/visitor/checkMobileCount', {params: {"orgId": $scope.organizationId}});
        mailcountresp.then(function (response) {
            console.log(response);
            $scope.mailCount = response.data.count;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
	$scope.autoSearch = function(){
       
        if ($scope.searchText !== "" && $scope.searchText !== undefined) {
            if ($scope.searchText.length >= 3) {
                $interval($scope.filterEventChanged(), 2500);
            }
        }
    }

    $scope.searchTextBox = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setSearchLeadTextBox', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.fixlength = false;
    // this should match however many results your API puts on one page
    $scope.filterEventChanged = function (change) {
        $scope.loader = true;
        // $scope.filtersadded.event
        ////console.log($scope.filterEventSelected);
        $scope.adfilter = false;
        $scope.filterby = false;
        var eventselectId = 0;
        var lstage = "";
        var potential = "";
        var category = "";
        var interest = "";
        var designation = "";
        var assignto = "";
        // //console.log($scope.filterEventSelected);

        if ($scope.filterEventSelected && $scope.filterEventSelected.id !== undefined) {
            eventselectId = $scope.filterEventSelected.id;
            console.log($scope.filterEventSelected);
            $scope.cEmail = sessionStorage.getItem('userEmail');
            $scope.email = {"cEmail":$scope.cEmail};
            Data.post('visitor/setListNameFilter', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
            //localStorage.setItem('curEventId', $scope.filterEventSelected.id);
            //localStorage.setItem('curEventName', $scope.filterEventSelected.name);
            //$rootScope.currentEventId = localStorage.getItem('curEventId');
            //$rootScope.currentEventName = localStorage.getItem('curEventName');
            // $rootScope.title = "Leads (" + $rootScope.currentEventName + ")";
            $scope.filtersadded.event = $scope.filterEventSelected.name;
            $scope.filtersadded.eventId = $scope.filterEventSelected.id;
            $rootScope.currentEventId = $scope.filterEventSelected.id;
			localStorage.setItem('curEventsend',$scope.filterEventSelected.sendGreet);
            localStorage.setItem('curEventshare',$scope.filterEventSelected.listlead_share);
            // $scope.pageNumber = 0;

        } else if ($scope.filterEventSelected === null) {

            eventselectId = 0;
            $rootScope.currentEventId = "";
            $scope.filtersadded.event = "";
            $scope.filtersadded.eventId = 0;
        } else {
            //eventselectId = 0;
            //$rootScope.currentEventId = "";
            // $scope.filtersadded.event = "";
            // $scope.filtersadded.eventId = 0;
			localStorage.setItem('curEventsend','');
            localStorage.setItem('curEventshare','');
            eventselectId = $scope.filtersadded.eventId;
            $rootScope.currentEventId = 0;
        }
        ////console.log("Filter stage1" + $scope.filterStageSelected);
        /*if ($scope.filterStageSelected) {
         ////console.log("Filter Stage2" + $scope.filterStageSelected);
         lstage = $scope.filterStageSelected;
         $scope.filtersadded.stage = $scope.filterStageSelected;
         // $scope.pageNumber = 0;
         
         } else if ($scope.filterindex === 'type') {
         ////console.log("Filter Index " + $scope.filterindex);
         lstage = $scope.filterlabel;
         $scope.filterStageSelected = $scope.filterlabel;
         $scope.filtersadded.stage = $scope.filterlabel;
         } else if ($scope.filtersadded.stage !== '' && $scope.filterStageSelected !== '') {
         //console.log("Insiade1");
         $scope.filterStageSelected = $scope.filtersadded.stage;
         lstage = $scope.filtersadded.stage;
         } else {
         lstage = "";
         $scope.filtersadded.stage = "";
         $scope.filterStageSelected = "";
         }*/

        if ($scope.filterStageSelected !== '') {
             $scope.cEmail = sessionStorage.getItem('userEmail');
            $scope.email = {"cEmail":$scope.cEmail};
            Data.post('visitor/setStageFilter', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
            console.log("Console" + $scope.filterStageSelected);
            var stageVal = JSON.parse($scope.filterStageSelected);
            lstage = stageVal.id;
            $scope.filtersadded.stage = stageVal.stage;
			$scope.filtersadded.stageIds = stageVal.id;
        } else if ($scope.filterindex === 'type') {

            ////console.log("Potential");
            if ($scope.filterlabel !== '') {
                console.log($scope.filterlabel);
                var stageVal1 = JSON.parse($scope.filterlabel);
                lstage = stageVal1.id;
                $scope.filterStageSelected = "";
                $scope.filtersadded.stage = stageVal1.stage;
                $scope.filtersadded.stageIds = stageVal1.id;
            }
        } else if ($scope.filterindex === 'dtype') {

            ////console.log("Potential");
            if ($scope.filterlabel !== '') {
                var stageVal2 = JSON.parse($scope.filterlabel);
                //console.log(stageVal2)
                lstage = stageVal2.id;
                $scope.filterStageSelected = "";
                $scope.filtersadded.stage = stageVal2.stage;
                $scope.filtersadded.deal = '1';
                $scope.filtersadded.dealComp = '>';
                $scope.filtersadded.stageIds = stageVal2.id;
            }
        } else if ($scope.filtersadded.stageIds !== '' && change !== '1') {

            //$scope.filterStageSelected = $scope.filtersadded.stageIds;
            //$scope.filterPotentialSelected = $scope.filtersadded.potential;

            lstage = $scope.filtersadded.stageIds;
        } else if ($scope.filterStageSelected === '') {
            lstage = "";
            $scope.filtersadded.stage = null;
            $scope.filtersadded.stageIds = null;
        } else {
            lstage = "";
            $scope.filtersadded.stage = null;
            $scope.filtersadded.stageIds = null;
        }

        console.log("Potential Selected Id" + $scope.filterPotentialSelected);

        if ($scope.filterPotentialSelected !== '') {
            ////console.log("Console" + $scope.filterPotentialSelected);
             $scope.cEmail = sessionStorage.getItem('userEmail');
            $scope.email = {"cEmail":$scope.cEmail};
            Data.post('visitor/setPotentialFilter', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
            potential =  $scope.filterPotentialSelected ;
            $scope.filtersadded.potential =  $scope.filterPotentialSelected ;
        } else if ($scope.filterindex === 'potential') {
            ////console.log("Potential");
            potential = $scope.filterlabel ;
            $scope.filterPotentialSelected = $scope.filterlabel;
            $scope.filtersadded.potential =  $scope.filterlabel ;
        } else if ($scope.filtersadded.potential !== '' && change !== '1') {
            //$scope.filterPotentialSelected = $scope.filtersadded.potential;
            potential = $scope.filtersadded.potential;
        } else if ($scope.filterPotentialSelected === '') {

            potential = "";
            $scope.filtersadded.potential = null;

        } else {
            potential = "";
            $scope.filtersadded.potential = "";
        }

        if ($scope.filterindex === 'category') {
            category = $scope.filterlabel;
            $scope.filtersadded.cgroup = $scope.filterlabel;

        } else if ($scope.filtersadded.cgroup !== '' && $scope.filterindex !== 'category') {
            category = $scope.filtersadded.cgroup;
        } else {
            category = "";
            $scope.filtersadded.cgroup = "";
        }
        if ($scope.filterindex === 'interest') {
            interest = $scope.filterlabel;
            $scope.filtersadded.service = $scope.filterlabel;
        } else if ($scope.filtersadded.service !== '' && $scope.filterindex !== 'interest') {
            interest = $scope.filtersadded.service;
        } else {
            interest = "";
            $scope.filtersadded.service = "";
        }


        //AssignTo users
        if ($scope.filterAssignToSelected !== '') {
            $scope.filtersadded.assignToId =  $scope.filterAssignToSelected;
        } else if ($scope.filtersadded.assignedTo == '' || $scope.filtersadded.assignedTo == null) {
            $scope.filtersadded.assignToId =  '';
        }
        //End AssignTo users


		if ($scope.sortvisitorDetBy != "") {
            $scope.cEmail = sessionStorage.getItem('userEmail');
            $scope.email = {"cEmail":$scope.cEmail};
            Data.post('visitor/setSortFilter', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
            $scope.sortvisitorBy = $scope.sortvisitorDetBy;
            $scope.fieldSelToggle = "";
            $scope.fieldSelSymbolToggle = {};
        }
		var query = '';
        if($scope.searchText !== "") {
           query = $scope.searchText; 
        }
        //console.log("Filter Content");
        //console.log("Potential" + potential);
        //console.log("Category" + category);
        //console.log("Interest" + interest);
        //console.log("Stage" + lstage);
        //console.log("Event" + eventselectId);
        
        $scope.visitors.length = 0;
        //$scope.pageNumber = pageNumber;
        $scope.loader = true;
        console.log("Page Number" + $scope.pageNumber);
        //console.log("Event selected" + eventselectId);
        if ($scope.filtersadded.fromDate) {
            $scope.filtersadded.fromDate = new Date($scope.filtersadded.fromDate);
            $month = ("0" + ($scope.filtersadded.fromDate.getMonth() + 1)).slice(-2);
            $day = ("0" + $scope.filtersadded.fromDate.getDate()).slice(-2);
            $scope.filtersadded.fromDate = [$scope.filtersadded.fromDate.getFullYear(), $month, $day].join("-");
            //$scope.filtersadded.fromDate = $scope.filtersadded.fromDate.toLocaleString();
        }
        if ($scope.filtersadded.toDate) {
            $scope.filtersadded.toDate = new Date($scope.filtersadded.toDate);
            $month = ("0" + ($scope.filtersadded.toDate.getMonth() + 1)).slice(-2);
            $day = ("0" + $scope.filtersadded.toDate.getDate()).slice(-2);
            $scope.filtersadded.toDate = [$scope.filtersadded.toDate.getFullYear(), $month, $day].join("-");
            //$scope.filtersadded.toDate = $scope.filtersadded.toDate.toLocaleString();
        }
        if ($scope.filtersadded.drfromDate) {
            $scope.filtersadded.drfromDate = new Date($scope.filtersadded.drfromDate);
            //$scope.filtersadded.drfromDate = $scope.filtersadded.drfromDate.toLocaleString();
            $month = ("0" + ($scope.filtersadded.drfromDate.getMonth() + 1)).slice(-2);
            $day = ("0" + $scope.filtersadded.drfromDate.getDate()).slice(-2);
            $scope.filtersadded.drfromDate = [$scope.filtersadded.drfromDate.getFullYear(), $month, $day].join("-");
        }
        if ($scope.filtersadded.drtoDate) {
            $scope.filtersadded.drtoDate = new Date($scope.filtersadded.drtoDate);
            //$scope.filtersadded.drtoDate = $scope.filtersadded.drtoDate.toLocaleString();
            $month = ("0" + ($scope.filtersadded.drtoDate.getMonth() + 1)).slice(-2);
            $day = ("0" + $scope.filtersadded.drtoDate.getDate()).slice(-2);
            $scope.filtersadded.drtoDate = [$scope.filtersadded.drtoDate.getFullYear(), $month, $day].join("-");
        }
		localStorage.setItem("filters", JSON.stringify($scope.filtersadded));
		// For global access
        $scope.selectedCategory = category;
        $scope.selectedInterest = interest;
        $scope.selectedEventID = eventselectId;
        $scope.selectedPotential = potential;
        $scope.selectedStage = lstage;
        //$scope.selectedAssignTo = assignto;
		$scope.urlParam = {"organizationId": $scope.organizationId, "cgroups": category, "interests": interest, "potential": potential, "eventId": eventselectId, "assignto": $scope.filtersadded.assignToId, "assigne": $scope.filtersadded.assigneeId, "capture": $scope.filtersadded.capturedId, "type": lstage, "designation": $scope.filtersadded.designation, "company": $scope.filtersadded.company, "country": $scope.filtersadded.country, "city": $scope.filtersadded.city, "state": $scope.filtersadded.state, "fromDate": $scope.filtersadded.fromDate, "toDate": $scope.filtersadded.toDate, "drfromDate": $scope.filtersadded.drfromDate, "drtoDate": $scope.filtersadded.drtoDate, "dontFollow": $scope.filtersadded.dontFollow, "notes": $scope.filtersadded.notes, "tags": $scope.filtersadded.tags, "notAssigned": $scope.filtersadded.notAssigned,"searchquery": query, "deal": $scope.filtersadded.deal, "dealComp": $scope.filtersadded.dealComp, "customList": JSON.stringify($scope.filtersadded.custList),"emailStatus":$scope.filtersadded.emailStatus};
        $scope.downloadURL = Data.webAppBase() + 'account/filteredCSVVisitor?' + $.param($scope.urlParam);
        $scope.downloadExcelURL = Data.webAppBase() + 'account/filteredExcelVisitor?' + $.param($scope.urlParam);
        
        var resprom = $http.get(Data.serviceBase() + '/visitor/allvisitorsFilter', {params: {"orgId": $scope.organizationId, "cgroups": category, "interests": interest, "potential": potential, "eventId": eventselectId, "assignto": $scope.filtersadded.assignToId, "assigne": $scope.filtersadded.assigneeId, "capture": $scope.filtersadded.capturedId, "type": lstage, "designation": $scope.filtersadded.designation, "company": $scope.filtersadded.company, "country": $scope.filtersadded.country, "city": $scope.filtersadded.city, "state": $scope.filtersadded.state, "fromDate": $scope.filtersadded.fromDate, "toDate": $scope.filtersadded.toDate, "drfromDate": $scope.filtersadded.drfromDate, "drtoDate": $scope.filtersadded.drtoDate, "dontFollow": $scope.filtersadded.dontFollow, "notes": $scope.filtersadded.notes,"notAssigned": $scope.filtersadded.notAssigned,"noAssignedDate": $scope.filtersadded.noAssignedDate,"tags": $scope.filtersadded.tags,"searchquery": query,"deal": $scope.filtersadded.deal, "dealComp": $scope.filtersadded.dealComp, "customList": JSON.stringify($scope.filtersadded.custList),"emailStatus":$scope.filtersadded.emailStatus ,"page": $scope.pageNumber,"sortBy": $scope.sortvisitorBy,"gtime": $scope.filtersadded.gtime, "gsign": $scope.filtersadded.gsign}});
        resprom.then(function (response) {
            $scope.visitors = response.data.visitors;
            sessionStorage.setItem("filterlabel", "");
            sessionStorage.setItem("filterindex", "");
            $scope.filterlabel = "";
            $scope.filterindex = "";
            $scope.loader = false;
            $scope.adfilter = true;
            $scope.filterby = true;
            $scope.cardView = true;
            $scope.totalVisitors = parseInt(response.data.count);
            $scope.viMailCount = parseInt(response.data.viMailCount);
			$scope.dealSum = parseInt(response.data.dealSum);
            //console.log("Page count " + Math.round($scope.totalVisitors / 20));
			angular.forEach($scope.visitors, function (visitor) {
                if ($scope.selectedVisitor.indexOf(visitor.id) > -1) {
                    visitor.select = true;
                } else {
                    visitor.select = false;
                }
            });
            if ($scope.totalVisitors <= 20) {
                $scope.totalPage = 1;
            } else {
                $scope.totalPage = Math.floor($scope.totalVisitors / 20) + 1;
            }

            //console.log("Total Page count " + $scope.totalPage);
            /*  $scope.totPage = $scope.visitors.length;
             $scope.startP = 1;
             if ($scope.visitors.length < 15) {
             $scope.endP = $scope.visitors.length;
             } else {
             $scope.endP = 15;
             }
             $scope.pageChangeHandler(1);*/
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });



    };
	
	 // GEt CSV filtered
    // 

    $scope.exportLead = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setExportLead', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.submitCSVForm = function(){
        //$('#CSVForm').submit();
        //window.history.pushState("Download", "Download", $scope.downloadURL);
		
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        
        var respuserchk = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "exportLead"}});

    // $scope.users = {};
    respuserchk.then(function (response) {
        console.log(response);
        $scope.loader = false;
        if (response.data.status === 'success') {
             var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "exportLeads"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {
       window.location.href = $scope.downloadURL;
        
        
        } else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to export leads from HelloLeads. Only account owner can export leads. Please contact HelloLeads account owner in your company for exporting leads."};
                    alertUser.alertpopmodel($scope.message);
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });
        
        
        
        }else {
            $scope.message = {"title": response.data.title, "message": response.data.message, "cancel": false};
                checkPremium.premiumpopmodel($scope.message);
        }
    }, function (response) {

        console.log('Error happened -- ');
        console.log(response);
    });
	 } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
	
	$scope.submitExcelForm = function () {
        //$('#CSVForm').submit();
        //window.history.pushState("Download", "Download", $scope.downloadURL);
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {

                var respuserchk = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "exportLead"}});

                // $scope.users = {};
                respuserchk.then(function (response) {
                    console.log(response);
                    $scope.loader = false;
                    if (response.data.status === 'success') {
                        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "exportLeads"}});

                        // $scope.users = {};
                        resprole.then(function (response) {
                            console.log(response);
                            $scope.loader = false;
                            if (response.data.status === 'success') {
                                window.location.href = $scope.downloadExcelURL;


                            } else {
                                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to export leads from HelloLeads. Only account owner can export leads. Please contact HelloLeads account owner in your company for exporting leads."};
                                alertUser.alertpopmodel($scope.message);
                                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                            }
                        }, function (response) {

                            console.log('Error happened -- ');
                            console.log(response);
                        });



                    } else {
                        trialPopUp.trialPopmodel(response.data);
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
	
	
	

	 //---------------------------------------------------------- Multi visitor Assignmtnt / delete / move ---------------------------------------
    $scope.visitorsIDs = {};
    $scope.selectAllVisitor = function () {
    	var query = '';
        if($scope.searchText !== ""){
           query = $scope.searchText; 
        }
        $scope.selectedVisitor = [];
        $scope.selectedVisitor.length = 0;
        $scope.checkstate = true;
        var resprom = $http.get(Data.serviceBase() + '/visitor/selectAllvisitorsFilter', {params: {"orgId": $scope.organizationId, "cgroups": $scope.selectedCategory, "interests": $scope.selectedInterest, "potential": $scope.selectedPotential, "eventId": $scope.selectedEventID, "assignto": $scope.filtersadded.assignToId, "assigne": $scope.filtersadded.assigneeId, "capture": $scope.filtersadded.capturedId, "type": $scope.selectedStage, "designation": $scope.filtersadded.designation, "company": $scope.filtersadded.company, "country": $scope.filtersadded.country, "city": $scope.filtersadded.city, "state": $scope.filtersadded.state, "fromDate": $scope.filtersadded.fromDate, "toDate": $scope.filtersadded.toDate, "drfromDate": $scope.filtersadded.drfromDate, "drtoDate": $scope.filtersadded.drtoDate, "dontFollow": $scope.filtersadded.dontFollow, "notes": $scope.filtersadded.notes, "tags": $scope.filtersadded.tags, "notAssigned": $scope.filtersadded.notAssigned, "deal": $scope.filtersadded.deal, "dealComp": $scope.filtersadded.dealComp,"emailStatus":$scope.filtersadded.emailStatus, "page": $scope.pageNumber, "searchquery": query}});
        resprom.then(function (response) {
            $scope.visitorsIDs = response.data.visitors;
            $scope.selectAllFlag = false;
            angular.forEach($scope.visitorsIDs, function (svisitor) {
                $scope.selectedVisitor.push(svisitor.id);
            });
            angular.forEach($scope.visitors, function (visitor) {
                if ($scope.selectedVisitor.indexOf(visitor.id) > -1) {
                    visitor.select = true;
                } else {
                    visitor.select = false;
                }
            });

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.assignToModel = function (p, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "openAssignments"}});
        // $scope.users = {};
        resprole.then(function (response) {
            //console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {

                var modalInstanceassignTo = $uibModal.open({
                    templateUrl: './assignVisitor',
                    controller: 'assignVisitorCtrl',
                    size: 'md',
                    backdrop: 'static',
                    resolve: {
                        vvisitorlist: function () {
                            return $scope.selectedVisitor.join();
                        }
                    }
                });
                modalInstanceassignTo.result.then(function (i) {
                    if (i) {
                        $scope.checkstate = false;
                        $scope.selectedVisitor = [];
                        $scope.selectedVisitor.length = 0;
                        $scope.filterEventChanged();
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to share lead information. Only account owner or Managers (within HelloLeads) can share lead information. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            //console.log('Error happened -- ');
            //console.log(response);
        });
			} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.moveListModel = function (p, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
			
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "openAssignments"}});
        // $scope.users = {};
        resprole.then(function (response) {
            //console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {

                var modalInstanceassignTo = $uibModal.open({
                    templateUrl: './moveVisitor',
                    controller: 'moveVisitorCtrl',
                    size: 'md',
                    backdrop: 'static',
                    resolve: {
                        vvisitorlist: function () {
                            return $scope.selectedVisitor.join();
                        }
                    }
                });
                modalInstanceassignTo.result.then(function (i) {
                    if (i) {
                        $scope.checkstate = false;
                        $scope.selectedVisitor = [];
                        $scope.selectedVisitor.length = 0;
                        $scope.filterEventChanged();
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to share lead information. Only account owner or Managers (within HelloLeads) can share lead information. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            //console.log('Error happened -- ');
            //console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

    $scope.multiDeleteModel = function (p, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteLead"}});
        // $scope.users = {};
        resprole.then(function (response) {
            //console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {

                var modalInstanceassignTo = $uibModal.open({
                    templateUrl: './deleteMultiVisitor',
                    controller: 'deleteMultiVisitorCtrl',
                    size: 'md',
                    backdrop: 'static',
                    resolve: {
                        vvisitorlist: function () {
                            return $scope.selectedVisitor.join();
                        },
                        visitorCount: function () {
                            return $scope.selectedVisitor.length;
                        }
                    }
                });
                modalInstanceassignTo.result.then(function (i) {
                    if (i) {
                        $scope.checkstate = false;
                        $scope.selectedVisitor = [];
                        $scope.selectedVisitor.length = 0;
                        $scope.filterEventChanged();
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to share lead information. Only account owner or Managers (within HelloLeads) can share lead information. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            //console.log('Error happened -- ');
            //console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.multiUpdateModel = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
		/* var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteLead"}});
         // $scope.users = {};
         resprole.then(function (response) {
         //console.log(response);
         $scope.loader = false;
         if (response.data.status === 'success') {
         */
        var modalInstanceassignTo = $uibModal.open({
            templateUrl: './updateMultiVisitor',
            controller: 'updateMultiVisitorCtrl',
            size: 'md',
            backdrop: 'static',
            resolve: {
                vvisitorlist: function () {
                    return $scope.selectedVisitor.join();
                },
                visitorCount: function () {
                    return $scope.selectedVisitor.length;
                }
            }
        });
        modalInstanceassignTo.result.then(function (i) {
            if (i) {
                $scope.checkstate = false;
                $scope.selectedVisitor = [];
                $scope.selectedVisitor.length = 0;
                $scope.filterEventChanged();
            }
        });
        /* } else {
         $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to share lead information. Only account owner or Managers (within HelloLeads) can share lead information. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
         alertUser.alertpopmodel($scope.message);
         //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
         }
         }, function (response) {
         
         //console.log('Error happened -- ');
         //console.log(response);
         });*/
		 } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
//---------------------------------------------------------------------------- End multi assignment /delete / move ----------------------------------------

 // Edit Coulmn Pop up 
    $scope.editColumn = function (p, size) {


        var modalInstanceassignTo = $uibModal.open({
            templateUrl: './editColumnTable',
            controller: 'editColumnCtrl',
            size: 'md',
            backdrop: 'static',
            resolve: {
                selcols: function () {
                    return  $scope.selectedPCols;
                }
            }
        });
        modalInstanceassignTo.result.then(function (i) {
            if (i) {
                $scope.selectedPCols = [];
                $scope.selectedPCols = i;
                $scope.columnArrange();
                $scope.filterEventChanged();
            }
        });



    };

    // ------------------------------------- End Edit Column -----------------------------------------------

    /*$scope.getResultsPage=function(pageNumber) {
     $scope.visitors.length = 0;
     $scope.pageNumber = pageNumber;
     $scope.loader = true;
     // this is just an example, in reality this stuff should be in a service
     $http.get(Data.serviceBase() + '/visitor/allvisitorgriddetail?orgId='+$scope.organizationId+'&page=' + $scope.pageNumber)
     .then(function(result) {
     ////console.log(result.data.visitors);
     $scope.visitors = result.data.visitors;
     sessionStorage.setItem("filterlabel", "");
     sessionStorage.setItem("filterindex", "");
     $scope.filterlabel = "";
     $scope.filterindex = "";
     $scope.loader = false;
     $scope.adfilter = true;
     $scope.filterby = true;
     //$scope.users = result.data.Items;
     $scope.totalVisitors = result.data.count;
     });
     };*/
    //$scope.cardView = true;

    //$scope.prevPageNumber = 0;
    $scope.pageChanged = function (newPage) {
        $scope.loader = true;
        $scope.cardView = false;
        $scope.pageNumber = newPage;

        //$scope.pagination.current = newPage;
        //paginationService.setCurrentPage('paginationId', newPage);
        if ($scope.totalVisitors <= 20) {
            //console.log("Visitor Count" + $scope.totalVisitors);
            $scope.totalPage = 1;
        }
        if ($scope.totalVisitors > 20) {
            //console.log("Visitor Count 2 " + $scope.totalVisitors);

            if (($scope.totalVisitors / 20) > 1) {
                $scope.totalPage = Math.floor($scope.totalVisitors / 20);
                $scope.totalPage = $scope.totalPage + 1;
            }

        }
        /* if ($scope.totalVisitors > (Math.round($scope.totalVisitors / 20) * 20) && !$scope.fixlength) {
         $scope.totalPage = $scope.totalPage + 1;
         $scope.fixlength = true;
         }*/

        ////console.log("Current"+newPage);
        if ($scope.prevpage != newPage) {
            $scope.filterEventChanged();
            $scope.prevpage = newPage;
        }
        //$scope.getResultsPage(newPage);
    };
    /*if (!$scope.filterlabel) {
     $scope.visitors.length = 0;
     //console.log("Called For Event");
     
     $scope.filterEventChanged();
     }*/



    $scope.makeUrl = function (vvisitor) {
        var orgnaiz = vvisitor.visitorOrganizationName;
        var city = vvisitor.city;
        var state = vvisitor.state;
        var country = vvisitor.country;
        if (city && state) {
            return vvisitor.visitorOrganizationName + "+" + vvisitor.city + "+" + vvisitor.state + "+" + vvisitor.country;
        } else {
            return vvisitor.visitorOrganizationName + "+" + vvisitor.country;
        }
    };
    //  $scope.curPage = 1;
    //  $scope.pageChangeHandler = function (num) {
    //       ////console.log("Total Page :" + Math.round($scope.visitors.length / 15)); 
    //      $scope.curPage = num;
    //   };
    $scope.shareCard = function (p, size) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "shareinfo"}});

        // $scope.users = {};
        resprole.then(function (response) {
            //console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstanceshare = $uibModal.open({
                    templateUrl: './sharevisitor',
                    controller: 'sharevisitorCtrl',
                    size: 'md',
                    backdrop: 'static',
                    resolve: {
                        vvisitor: function () {
                            return p;
                        }
                    }
                });
                modalInstanceshare.result.then(function (i) {
                    if (i) {
                        //$scope.refresh();
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to share lead information. Only account owner or Managers (within HelloLeads) can share lead information. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            //console.log('Error happened -- ');
            //console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

    $scope.resetLeadFilter = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/resetLeadFilterBtn', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.clearFilter = function () {
        $scope.adfilter = false;
        $scope.filterby = false;
        $scope.filtersadded = {};
        localStorage.setItem("curEventId", "");
        localStorage.setItem("curEventName", "");
        localStorage.setItem("filters", "");
        $scope.filtersadded = {
            "cgroup": "", "service": "", "potential": "", "event": "", "eventId": "", "assignedTo": "", "assignedBy": "", "capturedBy": "", "designation": "", "company": "", "country": "",
            "fromDate": "", "toDate": "", "drfromDate": "", "drtoDate": "", "dontFollow": "", "stage": "", "city": "", "state": "",
             "gtime": "", "gsign": ""

        };
        localStorage.setItem("filters", JSON.stringify($scope.filtersadded));
        $scope.filterStageSelected = "";
        $rootScope.currentEventId = "";
        $rootScope.currentEventName = "";
        $window.location.reload();
    };
    $scope.advancedSearch = function (query) {
        //console.log(query);
        if (query && query.length > 2) {
            $scope.searcherrortxt = false;
            $scope.loader = true;
            if ($scope.filterlabel) {
                var respromc = $http.get(Data.serviceBase() + '/visitor/advancedvisitorsSearch', {params: {"query": query, "orgId": $scope.organizationId, "eventId": $rootScope.currentEventId, "filterindex": $scope.filterindex, "filterlabel": $scope.filterlabel}});
            } else if ($scope.filterby) {

                var respromc = $http.get(Data.serviceBase() + '/visitor/allvisitorsFilter', {params: {"orgId": $scope.organizationId, "eventslist": $scope.filtersadded.eventId ? "'" + $scope.filtersadded.eventId + "'" : null, "cgroups": $scope.filtersadded.cgroup ? "'" + $scope.filtersadded.cgroup + "'" : null, "interests": $scope.filtersadded.service, "potential": $scope.filtersadded.potential ? "'" + $scope.filtersadded.potential + "'" : null, "assignto": $scope.filtersadded.assignedTo ? "'" + $scope.filtersadded.assignedTo + "'" : null, "assigne": $scope.filtersadded.assignedBy ? "'" + $scope.filtersadded.assignedBy + "'" : null, "capture": $scope.filtersadded.capturedBy ? "'" + $scope.filtersadded.capturedBy + "'" : null, "designation": $scope.filtersadded.designation,
                        "comapny": $scope.filtersadded.company, "country": $scope.filtersadded.country, "fromDate": $scope.filtersadded.fromDate, "toDate": $scope.filtersadded.toDate, "drfromDate": $scope.filtersadded.drfromDate, "drtoDate": $scope.filtersadded.drtoDate, "city": $scope.filtersadded.city, "state": $scope.filtersadded.state, "dontFollow": $scope.filtersadded.dontFollow, "searchquery": query}});
            } else {
                var respromc = $http.get(Data.serviceBase() + '/visitor/advancedvisitorsSearch', {params: {"query": query, "orgId": $scope.organizationId, "eventId": $rootScope.currentEventId}});
            }
            respromc.then(function (response) {
                $scope.visitors.length = 0;
                $scope.adfilter = true;
                $scope.searchRt = true;
                //$scope.showsearchtxt=true;
                ////console.log(response.data[0]);
                $scope.visitors = response.data;
                $scope.loader = false;
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        } else {
            // $scope.adfilter = false;
            // $scope.refresh();
            $scope.searchRt = false;
            $scope.searcherrortxt = true;
        }
    };
    $scope.searchvisitors = function (visitors) {

        //$scope.refresh();
        $scope.visitors.length = 0;
        $scope.visitors = visitors;
        $scope.loader = false;
        $scope.totPage = $scope.visitors.length;
        $scope.totalPages = Math.round($scope.visitors.length / 21);
        if ($scope.totalPages <= 0) {
            $scope.totalPages = 1;
        }
    };
    $scope.switchLayout = function (preview) {
        $scope.searcherrortxt = false;
        $scope.grid = preview;
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
        $scope.loader = true;
        if (preview === true) {
            $scope.gridColor = "#e7eaec";
            $scope.tableColor = "";
             Data.post('visitor/setTableViewCount', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else {
            $scope.tableColor = "#e7eaec";
            $scope.gridColor = "";
            Data.post('visitor/setCardViewCount', $scope.email).then(function (result) {
              console.log('Email '+$scope.cEmail);
            });
        }
        if ($scope.filterlabel && !$scope.adfilter) {
            //$scope.searchText = $scope.filterlabel;
            $scope.visitors.length = 0;
            //$scope.filterEventChanged();
        }
        if (!$scope.filterlabel && !$scope.adfilter) {
            $scope.visitors.length = 0;
            //$scope.filterEventChanged();
        } else {
            $scope.loader = true;
            $scope.adfilter = true;
            var tempvisitors = angular.copy($scope.visitors);
            $scope.searchvisitors(tempvisitors);
        }

    };
	$scope.addBcard = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './uploadbizcard',
            controller: 'uploadbizcardCtrl',
            size: 'md',
            backdrop: 'static',
            resolve: {
                evisitor: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $window.location.reload();

            }
        });
    };
    $scope.vBcard = {};
//Showing Business Card
    $scope.showBcard = function (p, size) {
        var visitorBcard = $http.get(Data.serviceBase() + 'visitor/getvisitorBcard', {params: {"visitorId": p.id}});
        visitorBcard.then(function (response) {
            $scope.vBcard = response.data[0];
            //console.log(response.data[0]);
            var modalInstance = $uibModal.open({
                templateUrl: './viewBcard',
                controller: 'viewBcardCtrl',
                size: "lg",
                resolve: {
                    vvisitor: function () {
                        return p;
                    },
                    vbcard: function () {
                        return $scope.vBcard;
                    }
                }
            });
        }, function (response) {
            //console.log('Error occured -- ');
            //console.log(response);
        });
    };
    $scope.allvisitors = {};
    /* var resprom2 = $http.get(Data.serviceBase() + '/visitor/visitor', {params: {key: "organizationId", "value": $scope.organizationId}});
     resprom2.then(function (response2) {
     $scope.allvisitors = response2.data;
     }, function (response2) {
     //console.log('Error happened -- ');
     //console.log(response2);
     });*/
    $scope.refresh = function () {
        $scope.loader = true;
        $scope.filterby = false;
        if ($scope.filterlabel) {
            //$scope.searchText = $scope.filterlabel;
            $scope.visitors.length = 0;
            $scope.filterVisitorDetail();
        }
        if (!$scope.filterlabel && !$scope.adfilter) {
            $scope.visitors.length = 0;
            $scope.getVisitorDetailEvent();
        } else if ($scope.adfilter) {
            $scope.loader = true;
            $scope.adfilter = true;
            var tempvisitors = angular.copy($scope.visitors);
            $scope.searchvisitors(tempvisitors);
        }
    };
    //Filter Search

    /*-----------------For Generating Grid View-------------------------------------------
     var start = 0;
     var ending = start + 6;
     var lastdata = 50;
     var reachLast = false;*/


    // $scope.visitorData = [];
    $scope.formatDate = function (date) {
        var dateOut = new Date(date);
        return dateOut;
    };
    //$scope.filtersadded = {};
    $scope.editvisitor = function (p, selecttab, size) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
			
        //console.log(p);
        var modalInstance = $uibModal.open({
            templateUrl: './modifyVisitor',
            controller: 'editvisitorCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                evisitor: function () {
                    return p;
                },
                tabsel: function () {
                    return selecttab;
                },
                currententId: function () {
                    return $rootScope.currentEventId;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {


                $scope.filterEventChanged();
            }
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $rootScope.addvisitor = function (p, size) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
			

        // Free Premium Restriction Checking
        var respuserchk = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "visitor"}});

        // $scope.users = {};
        respuserchk.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var modalInstance = $uibModal.open({
                    templateUrl: './modifyVisitor',
                    controller: 'editvisitorCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        evisitor: function () {
                            return $scope.visitor;
                        },
                        tabsel: function () {
                            return "NA";
                        },
                        currententId: function () {
                            if ($location.absUrl().indexOf('manageVisitor') > 0 && $rootScope.currentEventId !== 0) {
                                return $rootScope.currentEventId;
                            } else {
                                return 0;
                            }
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        //console.log(i);
                        if (sessionStorage.getItem("userId") && sessionStorage.getItem("orgId")) {
                            var vorgId = sessionStorage.getItem("orgId");
							var vcheck = sessionStorage.getItem('vcheck');
                            sessionStorage.setItem('vcheck', vcheck + 'v');
                            var visitorSearchInst = $http.get(Data.serviceBase() + '/visitor/visitorSearchdata', {params: {"organizationId": vorgId}});

                            visitorSearchInst.then(function (response) {
                                $rootScope.visitorSearch = response.data;
                                localStorage.setItem("visitorDet", JSON.stringify($rootScope.visitorSearch));
                            }, function (response) {
                                //console.log('Error happened -- ');
                                //console.log(response);
                            });

                        }
                        if (i.visitorId !== undefined) {
                            window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + i.visitorId, "_self");
                        }

                    }
                });
            } else {
                //$scope.message = {"title": "Upgrade & enjoy full capacity of HelloLeads", "message": "<p>Good to see you are making full use of HelloLeads Free plan.</p> <p>You have successfully added 250 leads so far, which is the maximum number of leads your free plan can accommodate.<p> <p>Sure you want to go beyond. Upgrade to a premium plan so that you can add, manage and track more leads using HelloLeads.</p>"};
                checkPremium.premiumpopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
	 $scope.clonevisitor = function (p, selecttab, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
         $scope.relLead = angular.copy(p);
        //console.log(p);
        var modalInstance = $uibModal.open({
            templateUrl: './modifyVisitor',
            controller: 'editvisitorCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                evisitor: function () {
                    $scope.relLead.id = undefined;
                    $scope.relLead.services = "";
                    $scope.relLead.categories = "";
                    $scope.relLead.tags = "";
                    $scope.relLead.notes = "";
                    $scope.relLead.notes = "";
                    return $scope.relLead;
                },
                tabsel: function () {
                    return selecttab;
                },
                currententId: function () {
                    return $rootScope.currentEventId;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {


                if (i.visitorId !== undefined) {
                    window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + i.visitorId, "_self");
                }
            }
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.delvisitor = function (p, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
		  var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteLead"}});
        // $scope.users = {};
        resprole.then(function (response) {
            //console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {

        var modalInstance1 = $uibModal.open({
            templateUrl: './deletevisitor',
            controller: 'delvisitorCtrl',
            size: size,
            resolve: {
                dvisitor: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i) {
                $scope.filterEventChanged();
            }
        });
		 } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to delete lead information. Only account owner or Managers (within HelloLeads) can delete lead information. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            //console.log('Error happened -- ');
            //console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.filtervisitor = function (size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './filterVisitor',
            controller: 'filterVisitorCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                eventId: function () {
                    return 0;
                },
                eventName: function () {
                    return "";
                }
            }
        });
        modalInstance1.result.then(function (results) {
            // //console.log(results.searchFilter);
            // //console.log(results.visitors);
            /* if (results.visitors) {
             $scope.loader = true;
             $scope.adfilter = true;
             $scope.searchvisitors(results.visitors);
             }*/

            if (results.searchFilter) {
                $scope.filterby = true;
                ////console.log("Search Filter");
                // //console.log(results.searchFilter);
               // $scope.clearFilter();
                $scope.filtersadded = {};
                $scope.filtersadded = results.searchFilter;
                console.log(results.searchFilter);
                localStorage.setItem("filters", JSON.stringify($scope.filtersadded));
                $scope.filterEventSelected = "";
                $scope.filterStageSelected = "";
                $scope.filterAssignToSelected = "";
				$scope.sortvisitorBy = "";
                $scope.sortvisitorDetBy = "";
                $scope.fieldSelSymbolToggle = {};
                $scope.filterEventChanged();
            }

        });
    };
    $scope.viewvisitor = function (p, size) {
        //console.log(p.id);
        if (!p.firstName) {
            return false;
        }
        if (p.firstName) {
            //localStorage.setItem("filters", JSON.stringify($scope.filtersadded));
            if ($scope.adfilter === true) {
                ////console.log($scope.visitors.length + "Visitor Count");
                visitorShareData.addVisitorData(angular.copy($scope.visitors));
                window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + p.id, "_blank");
            } else {
                window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + p.id, "_blank");
            }
        } else {
            return false;
        }
        /* var valueAdd = $http.get(Data.serviceBase() + 'visitor/getvisitorValueAdds', {params: {"visitorId": p.id}});
         valueAdd.then(function (response) {
         $scope.valueAdd = response.data[0];
         //console.log(response.data[0]);
         var visitorComment = $http.get(Data.serviceBase() + 'visitor/getvisitorComments', {params: {"visitorId": p.id}});
         visitorComment.then(function (responseC) {
         $scope.vComments = responseC.data;
         
         
         var modalInstance = $uibModal.open({
         templateUrl: './viewVisitor',
         controller: 'viewvisitorCtrl',
         size: "lg",
         resolve: {
         vvisitor: function () {
         if (p.nextFollow) {
         p.nextFollow = $scope.formatDate(p.nextFollow);
         }
         return p;
         },
         vvisitors: function () {
         return $scope.visitors;
         },
         valueAdd: function () {
         return $scope.valueAdd;
         },
         vComments: function () {
         return $scope.vComments;
         }
         }
         });
         
         }, function (response) {
         //console.log('Error occured -- ');
         //console.log(response);
         });
         }, function (response) {
         //console.log('Error occured -- ');
         //console.log(response);
         });*/

    };
});
evtApp.controller('filterVisitorCtrl', function ($location, $rootScope, eventId, eventName, $uibModal, $uibModalInstance, toaster, $scope, $http, Data) {

     $scope.currenteventId = eventId;
    $scope.currentEventName = eventName;
    //console.log("EventID");
    //console.log(eventId);
    $scope.categorySelected = [];
    $scope.categoryCount = 0;
    $scope.interestCount = 0;
    $scope.assignToCount = 0;
    $scope.assigneCount = 0;
    $scope.captureCount = 0;
    $scope.eventCount = 0;
    $scope.potentialCount = 0;
    $scope.LeadstageCount = 0;
    $scope.serviceSelected = [];
    $scope.potentialSelected = [];
    $scope.LeadstageSelected = [];
    $scope.eventSelected = [];
    $scope.userassignToSelected = [];
    $scope.userassigneSelected = [];
    $scope.usercaptureSelected = [];
    $scope.searchf = {};
    $scope.cgroups = "";
    $scope.interests = "";
    $scope.potential = "";
    $scope.stage = "";
    $scope.eventslist = "";
    $scope.usersassignTo = "";
    $scope.compVal = "";
    $scope.dealVal = "";
    $scope.usersassigne = "";
    $scope.userscapture = "";
	$scope.cfFilters = [];
    $scope.organizationId = sessionStorage.getItem("orgId");
    this.setInputFocus = function () {
        $scope.$broadcast('UiSelectDemo1');
    };
	$scope.selectedCityList = [];
    var serviceresp = $http.get(Data.serviceBase() + '/service/service', {params: {"key": "organizationId", "value": $scope.organizationId}});
    serviceresp.then(function (response) {
        //console.log(response.data);
        $scope.serviceLists = response.data;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });

    //Get GMT
     var gmttime = $http.get(Data.serviceBase() + '/organization/getGMT', {params: {"organizationId": $scope.organizationId}});
     gmttime.then(function (response) {
        console.log(response.data);
        $scope.timezone = response.data;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    //End GMT


    var categoryresp = $http.get(Data.serviceBase() + '/category/category', {params: {"key": "organizationId", "value": $scope.organizationId}});
    categoryresp.then(function (response) {
        //console.log(response.data);
        $scope.categoryLists = response.data;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    if ($scope.currenteventId === 0) {
        var eventresp = $http.get(Data.serviceBase() + '/event/event', {params: {"key": "organizationId", "value": $scope.organizationId}});
        eventresp.then(function (response) {
            //console.log(response.data);
            $scope.eventLists = response.data;
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    }
    var userresp = $http.get(Data.serviceBase() + '/user/user', {params: {key: "organizationId", "value": $scope.organizationId}});
    $scope.userList = {};
    userresp.then(function (response) {
        $scope.userList = response.data;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
	
	 // Get the custom lead stages -----------------
    var stageresp = $http.get(Data.serviceBase() + '/organization/getCurrentStages', {params: {"organizationId": $scope.organizationId}});
    stageresp.then(function (response) {
        console.log(response.data);
        $scope.Lstages = response.data;


    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
	
	// -------------- Populating the auto suggesting leads --------------
    $scope.cityList = [];
	$scope.changeCity = function (city) {
        $scope.cityList = [];
        var cityresp = $http.get(Data.serviceBase() + '/visitor/cityDetails', {params: {"organizationId": $scope.organizationId, "city": city}});

        cityresp.then(function (response) {

            // $scope.cityList = response.data;
            // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
            angular.forEach(response.data, function (value, key) {

                if (value.city !== null && $scope.cityList.indexOf(value.city) == -1) {
                    //categoryId.push(value.id);
                    $scope.cityList.push(value.city);
                }


            });
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    };
    $scope.stateList = [];
    $scope.changeState = function (state) {
    var stateresp = $http.get(Data.serviceBase() + '/visitor/stateDetails', {params: {"organizationId": $scope.organizationId,"state":state}});

    stateresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.state !== null && $scope.stateList.indexOf(value.state) == -1) {
                //categoryId.push(value.id);
                $scope.stateList.push(value.state);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
};
    $scope.countryList = [];
    $scope.changeCountry = function (country) {
    var countryresp = $http.get(Data.serviceBase() + '/visitor/countryDetails', {params: {"organizationId": $scope.organizationId,'country':country}});

    countryresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.country !== null && $scope.countryList.indexOf(value.country) == -1) {
                //categoryId.push(value.id);
                $scope.countryList.push(value.country);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
};
    $scope.desigList = [];
    $scope.changeDesig = function (desig) {
    var desigresp = $http.get(Data.serviceBase() + '/visitor/desigDetails', {params: {"organizationId": $scope.organizationId,"designation":desig}});

    desigresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.designation !== null && $scope.desigList.indexOf(value.designation) == -1) {
                //categoryId.push(value.id);
                $scope.desigList.push(value.designation);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
};
    $scope.companyList = [];
    $scope.changeCompany = function(company){
    var companyresp = $http.get(Data.serviceBase() + '/visitor/companyDetails', {params: {"organizationId": $scope.organizationId,'company':company}});

    companyresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.visitorOrganizationName !== null && $scope.companyList.indexOf(value.visitorOrganizationName) == -1) {
                //categoryId.push(value.id);
                $scope.companyList.push(value.visitorOrganizationName);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
};
    $scope.tagsList = [];
  $scope.changeTag = function(tags){  
    var tagsresp = $http.get(Data.serviceBase() + '/visitor/tagsDetails', {params: {"organizationId": $scope.organizationId,"tags":tags}});

    tagsresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.tag !== null && $scope.tagsList.indexOf(value.tag) == -1) {
                //categoryId.push(value.id);
                $scope.tagsList.push(value.tag);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });

  };
    /*var cityresp = $http.get(Data.serviceBase() + '/visitor/cityDetails', {params: {"organizationId": $scope.organizationId}});

    cityresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.city !== null && $scope.cityList.indexOf(value.city) == -1) {
                //categoryId.push(value.id);
                $scope.cityList.push(value.city);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    $scope.stateList = [];
    var stateresp = $http.get(Data.serviceBase() + '/visitor/stateDetails', {params: {"organizationId": $scope.organizationId}});

    stateresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.state !== null && $scope.stateList.indexOf(value.state) == -1) {
                //categoryId.push(value.id);
                $scope.stateList.push(value.state);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    $scope.countryList = [];
    var countryresp = $http.get(Data.serviceBase() + '/visitor/countryDetails', {params: {"organizationId": $scope.organizationId}});

    countryresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.country !== null && $scope.countryList.indexOf(value.country) == -1) {
                //categoryId.push(value.id);
                $scope.countryList.push(value.country);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    $scope.desigList = [];
    var desigresp = $http.get(Data.serviceBase() + '/visitor/desigDetails', {params: {"organizationId": $scope.organizationId}});

    desigresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.designation !== null && $scope.desigList.indexOf(value.designation) == -1) {
                //categoryId.push(value.id);
                $scope.desigList.push(value.designation);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    $scope.companyList = [];
    var companyresp = $http.get(Data.serviceBase() + '/visitor/companyDetails', {params: {"organizationId": $scope.organizationId}});

    companyresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.visitorOrganizationName !== null && $scope.companyList.indexOf(value.visitorOrganizationName) == -1) {
                //categoryId.push(value.id);
                $scope.companyList.push(value.visitorOrganizationName);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
    $scope.tagsList = [];
    var tagsresp = $http.get(Data.serviceBase() + '/visitor/tagsDetails', {params: {"organizationId": $scope.organizationId}});

    tagsresp.then(function (response) {

        // $scope.cityList = response.data;
        // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
        angular.forEach(response.data, function (value, key) {

            if (value.tag !== null && $scope.tagsList.indexOf(value.tag) == -1) {
                //categoryId.push(value.id);
                $scope.tagsList.push(value.tag);
            }


        });
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });*/
	
	 /// -- Custome Fileds filters ---------------------------------------------
    $scope.addCust = {"field": "", "condition": 'Contains', "value1": '', "value2": ''};
    $scope.custFiltersAdd = function (custField) {
    	if (custField.field !== "") {
        if (custField.field !== "" && custField.value1 !== "") {
            $scope.cfFilters.push(custField);
            $scope.addCust = {"field": "", "condition": 'Contains', "value1": '', "value2": ''};
        } else if(custField.condition !== "Is Empty" && custField.condition !== "Is Not Empty") {
        	toaster.pop("error", "", "Please enter custom field value", 10000, 'trustedHtml');
        }
        if (custField.field !== "" && (custField.condition === "Is Empty" || custField.condition === "Is Not Empty")) {
            $scope.cfFilters.push(custField);
            $scope.addCust = {"field": "", "condition": 'Contains', "value1": ''};
        }
        if (custField.field !== "" && custField.field === "Between" && custField.value1 !== "" && custField.value2 !== "") {
            $scope.cfFilters.push(custField);
            $scope.addCust = {"field": "", "condition": 'Contains', "value1": '', "value2": ''};
        } else if(custField.field === "Between" && (custField.value1 == "" || custField.value2 == "") && (custField.condition !== "Is Empty" && custField.condition !== "Is Not Empty")) {
        	toaster.pop("error", "", "Please enter custom field values", 10000, 'trustedHtml');
        }
        $scope.custfieldsList.splice($scope.custfieldsList.indexOf(custField.field), 1);
    } else {
    	toaster.pop("error", "", "Please select a custom field name", 10000, 'trustedHtml');
    }

    };
    $scope.custFiltersDelete = function (custField) {
        // if(custField.fieldName === $scope.cfFilters.filedName){
        $scope.cfFilters.splice($scope.cfFilters.indexOf(custField.field), 1);
        if ($scope.custfieldsList.indexOf(custField.field) === -1) {
            $scope.custfieldsList.push(custField.field);
        }
        // }
        //$scope.cfFilters.push(custField);
    };

    var eventListId = [];



    $scope.getCustomFields = function () {
        $scope.custfieldsList = [];
        var cFieldsresp = $http.get(Data.serviceBase() + '/visitor/cutomFields', {params: {"organizationId": $scope.organizationId, "eventId": eventListId}});

        cFieldsresp.then(function (response) {

            // $scope.cityList = response.data;
            // $scope.cityList.filter(function(value, index){ return $scope.cityList.indexOf(value) == index });
            angular.forEach(response.data, function (value, key) {

                if (value.name !== null && $scope.custfieldsList.indexOf(value.name) == -1) {
                    //categoryId.push(value.id);
                    $scope.custfieldsList.push(value.name);
                }


            });
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    };

    //// ------------------------------- end of custom filed filters ---------------


    ///------------------------------------------------------------------- Auto suggestion ------------------------------------------

    this.appendToBodyDemo = {
        remainingToggleTime: 0,
        present: true,
        startToggleTimer: function () {
            var scope = this.appendToBodyDemo;
            var promise = $interval(function () {
                if (scope.remainingTime < 1000) {
                    $interval.cancel(promise);
                    scope.present = !scope.present;
                    scope.remainingTime = 0;
                } else {
                    scope.remainingTime -= 1000;
                }
            }, 1000);
            scope.remainingTime = 3000;
        }
    };
    $scope.categoryCheck = function (index) {
        if ($scope.categorySelected[index] !== null) {
            $scope.categoryCount = $scope.categoryCount + 1;
        } else {
            $scope.categoryCount = $scope.categoryCount - 1;
        }
    };
    $scope.interestCheck = function (index) {
        if ($scope.serviceSelected[index] !== null) {
            $scope.interestCount = $scope.interestCount + 1;
        } else {
            $scope.interestCount = $scope.interestCount - 1;
        }
    };
    $scope.potentialCheck = function (index) {
        if ($scope.potentialSelected[index] !== null) {
            $scope.potentialCount = $scope.potentialCount + 1;
        } else {
            $scope.potentialCount = $scope.potentialCount - 1;
        }
    };
    $scope.LeadstageCheck = function (index) {
        if ($scope.LeadstageSelected[index] !== null) {
            $scope.LeadstageCount = $scope.LeadstageCount + 1;
        } else {
            $scope.LeadstageCount = $scope.LeadstageCount - 1;
        }
    };
    $scope.eventCheck = function (index) {
        if ($scope.eventSelected[index] !== null) {
            $scope.eventCount = $scope.eventCount + 1;
        } else {
            $scope.eventCount = $scope.eventCount - 1;
        }
		 //Getting Event Details if exist
        if ($scope.eventSelected.length > 0) {
            var checkedId;
            var checkedValueId;
            var customCount = [];
            $scope.customLength = 0;
            angular.forEach($scope.eventSelected, function (value, key) {
            	if (value !== null) {
            		customCount.push(value.id);
            		$scope.lengthCount = customCount.join();
            		$scope.customLength = $scope.lengthCount.length;
            	}
                if (value !== null) {
                    if (value.name !== undefined && eventListId.indexOf(value.id) >= -1) {
                    	if (checkedId !== '' && checkedId !== undefined) {
		                    checkedId += ",";
                    	}
                    	if (value.id !== undefined && value.id !== '' && eventListId.indexOf(value.id) >= -1) {
	                    	checkedId += value.id;
	                    	checkedValueId = checkedId.replace('undefined','');
		                    eventListId = checkedValueId;
		                }
                    }
                } else if ($scope.customLength === 0) {
	            	eventListId = [];
	            }
            });
            $scope.getCustomFields();
        } else {
            eventListId = [];
            $scope.getCustomFields();
        }
    };
    $scope.assignToCheck = function (index) {
        if ($scope.userassignToSelected[index] !== null) {
            $scope.assignToCount = $scope.assignToCount + 1;
        } else {
            $scope.assignToCount = $scope.assignToCount - 1;
        }
    };
    $scope.assigneCheck = function (index) {
        if ($scope.userassigneSelected[index] !== null) {
            $scope.assigneCount = $scope.assigneCount + 1;
        } else {
            $scope.assigneCount = $scope.assigneCount - 1;
        }
    };
    $scope.captureCheck = function (index) {
        if ($scope.usercaptureSelected[index] !== null) {
            $scope.captureCount = $scope.captureCount + 1;
        } else {
            $scope.captureCount = $scope.captureCount - 1;
        }
    };
    $scope.userassintoname = "";
    $scope.userassignename = "";
    $scope.usercapturename = "";
    $scope.eventselname = "";
    $scope.selectedFilter = {
        "cgroup": "", "service": "", "potential": "", "event": "", "eventId": "", "assignedTo": "", "assignedBy": "", "capturedBy": "", "designation": "", "company": "", "city": "", "state": "", "country": "",
        "fromDate": "", "toDate": "", "drfromDate": "", "drtoDate": "", "dontFollow": "", "stage": "", "assignToId": "", "assigneeId": "", "capturedId": "", "notes": "", "tags": "","notAssigned":"", "deal": "", "dealComp": "" , "custList": "","emailStatus":"", "stageIds": "",
        "gtime": "", "gsign": ""

    };


    $scope.advanceFilterBtn = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setAdvanceFilterBtn', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };


    $scope.searchFilter = function () {
        var categoryId = [];
        var categoryName = [];
        angular.forEach($scope.categorySelected, function (value, key) {

            if (value !== null) {
                //categoryId.push(value.id);
                categoryName.push(value.name);
            }


        });
        $scope.cgroups = categoryId.join();
        $scope.selectedFilter.cgroup = categoryName.join();

        var servicesId = [];
        var servicesName = [];
        angular.forEach($scope.serviceSelected, function (value, key) {

            if (value !== null) {
                servicesId.push(value.id);
                servicesName.push(value.name);
            }

        });
        $scope.interests = servicesId.join();
        $scope.selectedFilter.service = servicesName.join();
        $scope.potentialsele = [];
        angular.forEach($scope.potentialSelected, function (value, key) {
            if (value) {
                $scope.potentialsele.push( value );
            }

        });

        $scope.Leadstagesele = [];
        $scope.LeadstageseleIds = [];
        //console.log($scope.LeadstageSelected);
        angular.forEach($scope.Lstages, function (value) {
            // console.log(value.id);

            angular.forEach($scope.LeadstageSelected, function (val) {
                //console.log(val);

                if (value.id == val) {
                    // console.log(value.id);
                    $scope.Leadstagesele.push(value.stage);
                    $scope.LeadstageseleIds.push(value.id);
                    //console.log(value.stage);
                }
            });
        });
        //$scope.potential = $scope.potentialSelected.join();
        //console.log($scope.potential);
        $scope.selectedFilter.potential = $scope.potentialsele.filter(function(val) { return val !== null; }).join();
         $scope.selectedFilter.stage = $scope.Leadstagesele.filter(function (val) {
            return val !== null;
        }).join();
        $scope.selectedFilter.stageIds = $scope.LeadstageseleIds.filter(function (val) {
            return val !== null;
        }).join();
        if ($scope.currenteventId === 0) {
            //console.log($scope.eventSelected);
            var eventId = [];
            var eventName = [];
            angular.forEach($scope.eventSelected, function (value, key) {
                if (value !== null) {


                    if (value.name !== undefined) {
                        eventId.push(value.id);
                        eventName.push(value.name);
                    }

                }

            });
            $scope.eventslist = eventId.join();
            $scope.selectedFilter.event = eventName.filter(function(val) { return val !== null; }).join();
            $scope.selectedFilter.eventId = eventId.filter(function(val) { return val !== null; }).join();
        } else {
            $scope.eventslist = $scope.currenteventId;
            $scope.selectedFilter.eventId = $scope.currenteventId;
            $scope.selectedFilter.event = localStorage.getItem('curEventName');
            //$scope.eventselname=sessionStrogae.getItem('curEventName');
        }
        var assignTotempId = [];
        var assignTotempName = [];
        angular.forEach($scope.userassignToSelected, function (value, key) {

            if (value !== null) {

                assignTotempId.push(value.id);
                assignTotempName.push(value.firstName);

            }



        });
        $scope.usersassignTo = assignTotempId.join();
        $scope.selectedFilter.assignedTo = assignTotempName.filter(function(val) { return val !== null; }).join();
        $scope.selectedFilter.assignedToId = assignTotempName.filter(function(val) { return val !== null; }).join();

        var userassigneId = [];
        var userassigneName = [];
        angular.forEach($scope.userassigneSelected, function (value, key) {

            if (value !== null) {
                userassigneId.push(value.id);
                userassigneName.push(value.firstName);

            }

        });
        $scope.usersassigne = userassigneId.join();
        $scope.selectedFilter.assignedBy = userassigneName.filter(function(val) { return val !== null; }).join();
        var usercaptureId = [];
        var usercaptureName = [];
        angular.forEach($scope.usercaptureSelected, function (value, key) {

            if (value !== null) {
                usercaptureId.push(value.id);
                usercaptureName.push(value.firstName);

            }

        });
        $scope.userscapture = usercaptureId.join();
        $scope.selectedFilter.capturedBy = usercaptureName.filter(function(val) { return val !== null; }).join();

        //console.log($scope.cgroups);
        //console.log($scope.interests);
        //console.log($scope.potential);
        //console.log($scope.eventslist);
        //console.log($scope.usersassignTo);
        //console.log($scope.usersassigne);
        //console.log($scope.userscapture);
        //console.log($scope.searchf);
        $scope.selectedFilter.capturedId = $scope.userscapture;
        $scope.selectedFilter.assigneeId = userassigneId.filter(function(val) { return val !== null; }).join();
        $scope.selectedFilter.assignToId = assignTotempId.filter(function(val) { return val !== null; }).join();
        $scope.selectedFilter.potential = $scope.potentialsele.filter(function(val) { return val !== null; }).join();
         $scope.selectedFilter.stage = $scope.Leadstagesele.filter(function (val) {
            return val !== null;
        }).join();
        $scope.selectedFilter.stageIds = $scope.LeadstageseleIds.filter(function (val) {
            return val !== null;
        }).join();
        $scope.selectedFilter.designation = $scope.searchf['designation'] ? $scope.searchf['designation'].join() : '';
        $scope.selectedFilter.company = $scope.searchf['company'] ? $scope.searchf['company'].join() : '';
        $scope.selectedFilter.notes = $scope.searchf['notes'];
        $scope.selectedFilter.tags = $scope.searchf['tags'] ? $scope.searchf['tags'].join() : '';
        $scope.selectedFilter.country = $scope.searchf['country'] ? $scope.searchf['country'].join() : '';
        $scope.selectedFilter.city = $scope.searchf['city'] ? $scope.searchf['city'].join() : '';
        $scope.selectedFilter.state = $scope.searchf['state'] ? $scope.searchf['state'].join() : '';
        $scope.selectedFilter.fromDate = $scope.searchf['fromDate'];
        $scope.selectedFilter.toDate = $scope.searchf['toDate'];
        $scope.selectedFilter.drfromDate = $scope.searchf['drfromDate'];
        $scope.selectedFilter.drtoDate = $scope.searchf['drtoDate'];
        $scope.selectedFilter.noAssignedDate = $scope.searchf['noAssignedDate'];
        $scope.selectedFilter.dontFollow = $scope.searchf['dontFollow'];
		$scope.selectedFilter.notAssigned = $scope.searchf['notAssigned'];
        $scope.selectedFilter.gtime = $scope.searchf['gtime'];
        $scope.selectedFilter.gsign = $scope.searchf['gsign'];
        $scope.selectedFilter.emailStatus = $scope.searchf['emailStatus'];
		if ($scope.searchf['dealcomp'] === '>' && $scope.searchf['dealValue']) {
            $scope.selectedFilter.deal = $scope.searchf['dealValue'];
            $scope.selectedFilter.dealComp = $scope.searchf['dealcomp'];
        } else if ($scope.searchf['dealcomp'] !== '>') {
            $scope.selectedFilter.deal = $scope.searchf['dealValue'];
            $scope.selectedFilter.dealComp = $scope.searchf['dealcomp'];
        }
		$scope.selectedFilter.custList = $scope.cfFilters;
        console.log($scope.selectedFilter);
        $scope.result = {"visitors": "", "searchFilter": $scope.selectedFilter};
        $uibModalInstance.close($scope.result);

    };
    $scope.cancelFilter = function () {
        $scope.categorySelected = {};
        $scope.serviceSelected = {};
        $scope.potentialSelected = {};
        $scope.LeadstageSelected = {};
        $scope.eventSelected = {};
        $scope.userassignToSelected = {};
        $scope.userassigneSelected = {};
        $scope.usercaptureSelected = {};
        $scope.search = {};
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('VisitorDetailCtrl', function ($location, $timeout, $filter, $rootScope, visitorShareData, alertUser,trialPopUp,warningPopup, fileReader,$uibModal, toaster, $scope, $http, Data) {
    $scope.visitorId = 0;
    $scope.edit = false;
    $scope.loaderView = true;
    $scope.currentDate = new Date();
    $scope.currentTenDate = new Date();
    $scope.currentTenDate.setDate($scope.currentTenDate.getDate() + 10);
    $scope.viewvisitors = {};
    $scope.viewVisitorList = [];
    $scope.vRelatesdet = {};
    $scope.viewvisitor = {};
    $rootScope.title = "Lead Details";
    $rootScope.addButtonhtml = "";
    $scope.filterdetlabel = "";
    $scope.filterdetindex = "";
	$scope.nextFollowDate = "";
    var page = 0;
    $scope.curpage = 0;
    $scope.eventId = 0;
    sessionStorage.setItem("menuact", "3");
    $rootScope.menuact = "3";
    var orgId = sessionStorage.getItem("orgId");
	$scope.organizationId = sessionStorage.getItem("orgId");
	$scope.userId = sessionStorage.getItem("userId");
    $scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }

    $scope.dayDiff = function (firstDate, secondDate) {
        var date2 = new Date(secondDate);
        var date1 = new Date(firstDate);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    };
    $scope.viewsepvisitor = function (p, size) {
        //console.log(p.id);
        if (!p.firstName && !p.email) {
            return false;
        }
        //sessionStorage.setItem("filterdetlabel", $scope.filterlabel);
        //sessionStorage.setItem("filterdetindex", $scope.filterindex);
        window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + p.id, "_self");
    };
    $scope.editClick = function (p, selecttab, size) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        //console.log(p);
        var modalInstance = $uibModal.open({
            templateUrl: './modifyVisitor',
            controller: 'editvisitorCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                evisitor: function () {
                    return p;
                },
                tabsel: function () {
                    return selecttab;
                },
                currententId: function () {
                    var tempevent = $scope.eventId;
                    if ($scope.eventId !== 0) {
                        tempevent = $scope.eventId;
                    }

                    return tempevent;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.loaderView = true;
                var tempindex = 0;
                var lcont = true;
                //console.log("Visitor  Sep1 Detail ");
                //console.log(p);
                var visitorsdet = visitorShareData.getVisitorData();
                angular.forEach($scope.viewvisitors, function (v) {
                    if (v && lcont) {
                        tempindex = tempindex + 1;
                        if (v.id === p.id && v.eventId === p.eventId) {

                            //$scope.viewvisitors[tempindex] = {};
                            $scope.viewvisitors[tempindex - 1] = {};
                            v = i;
                            v['services'] = i['serviceslist'];
                            v['categories'] = i['categorylist'];
                            $scope.viewvisitor = v;
                            $scope.viewvisitors[tempindex - 1] = v;
                            // //console.log("Visitor  Sep2 Detail ");
                            // //console.log(visitorsdet[tempindex]);
                            //$scope.viewvisitors = visitorsdet;
                            $scope.loaderView = false;
                            $scope.visitorFieldsValues();
                            lcont = false;
                        } else {

                            v = {};
                        }
                    }
                });
				$scope.visitorComments();
                /* var visitorsdet = visitorShareData.getData();
                 if (visitorsdet.length > 0) {
                 //console.log("Search View5");
                 $scope.loaderView = true;
                 $scope.visitorGetStorage();
                 } else {
                 
                 //console.log("URL Inside");
                 var url = "";
                 var id = "";
                 var str = $location.absUrl();
                 var st = str.split("visitorId=");
                 if (st.length > 0) {
                 if (st[1].lastIndexOf("&") > -1) {
                 url = st[0] + "visitorId=";
                 var sttemp = st[1].split("&");
                 id = sttemp[0];
                 url = url + p.id + "&" + sttemp[1];
                 } else {
                 url = st[0] + "visitorId=";
                 id = st[1];
                 url = url + p.id;
                 }
                 }
                 if (p.id !== id) {
                 //console.log(url);
                 window.open(url, "_self");
                 } else {
                 if ($scope.filterdetlabel) {
                 $scope.filterVisitorDetails();
                 } else {
                 $scope.visitorGetDetail();
                 }
                 }
                 }*/
            }
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
	
	// File upload functionality ----------------
	 $scope.files = [];
    $scope.attachFiles = function (commentId, vid, commentResult) {
        $scope.postflag = true;
        if ($scope.files.length > 0) {
            //$scope.btnText = 'Uploading';
            //$scope.btnDisable = true;
            //FILL FormData WITH FILE DETAILS.

            var data = new FormData();
            for (var i in $scope.files) {
                console.log($scope.files[i]);
                data.append("filelist[]", $scope.files[i]);
            }

            data.append("orgId", $scope.organizationId);
            data.append("commentId", commentId);
            data.append("visitorId", vid);

            /* ADD LISTENERS.
             var objXhr = new XMLHttpRequest();
             objXhr.addEventListener("progress", updateProgress, false);
             objXhr.addEventListener("load", transferComplete, false);
             
             // SEND FILE DETAILS TO THE API.
             objXhr.open("POST", Data.serviceBase() + "/visitor/fileupload/");
             objXhr.send(data);
             // //console.log(data.get('filelist'));*/
            var request = {
                method: 'POST',
                url: Data.serviceBase() + "/visitor/fileCommentUpload/",
                data: data,
                headers: {
                    'Content-Type': undefined
                }
            };
            // SEND THE FILES.
            $http(request)
                    .success(function (result) {
                        console.log("File Sent");
                        $scope.files = [];
                        angular.element("input[type='file']").val(null);
						document.getElementById("attachFile").value = "";
                        $scope.postflag = false;
                        $scope.commentdet = {};
                        $scope.errorMsg ="";
                        toaster.pop(commentResult.status, "", commentResult.message, 10000, 'trustedHtml');
                        var visitorComment = $http.get(Data.serviceBase() + 'visitor/leadAcitivites', {params: {"visitorId": $scope.viewvisitor.id}});
                        visitorComment.then(function (responseC) {
                            $scope.vCommentsdet = responseC.data;
                            $scope.commentdet = {};

                        }, function (response) {
                            ////console.log('Error happened -- ');
                            ////console.log(response);
                        });
                        //$uibModalInstance.close(1);
                        //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    })
                    .error(function () {
                        //$uibModalInstance.close(1);
                        $scope.btnText = "Upload";
                        //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    });
        } else {
            $scope.postflag = false;
            $scope.commentdet = {};
            $scope.errorMsg ="";
            $scope.files = [];
            angular.element("input[type='file']").val(null);
			document.getElementById("attachFile").value = "";
            toaster.pop(commentResult.status, "", commentResult.message, 10000, 'trustedHtml');
            var visitorComment = $http.get(Data.serviceBase() + 'visitor/leadAcitivites', {params: {"visitorId": $scope.viewvisitor.id}});
            visitorComment.then(function (responseC) {
                $scope.vCommentsdet = responseC.data;
                $scope.commentdet = {};
            }, function (response) {
                ////console.log('Error happened -- ');
                ////console.log(response);
            });
        }
    };
	 $scope.openwhatsApp = function(visitor){
         if (!visitor.mobile) {
            toaster.pop("error", "", "There is no mobile / whatsapp number. Pls update", 10000, 'trustedHtml');
        }
        if (!visitor.mobileCode && visitor.mobile) {
            //var mobcode = str.indexOf(+);
            if(visitor.mobile.charAt(0) !== '+'){
            toaster.pop("error", "", "Please add mobile country code", 10000, 'trustedHtml');
            }
            else{
            var whatsappnumber = visitor.mobile;
            whatsappnumber = whatsappnumber.replace('+', '');
            $scope.WhatsApp();
            window.open('https://wa.me/' + whatsappnumber);
            }
        }
        if (visitor.mobile && visitor.mobileCode) {
            var whatsappnumber = visitor.mobileCode + visitor.mobile;
            whatsappnumber = whatsappnumber.replace('+', '');
            $scope.WhatsApp();
            window.open('https://wa.me/' + whatsappnumber);
        }
      
    };

    $scope.WhatsApp = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setWhatsApp', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
     };


     //IVR 
     $scope.ivrButton = 1;
     $scope.ivrcall = function (p) {
     $scope.ivrButton = 0;
     var resprole = $http.get(Data.serviceBase() + '/Clicktocall/clicktocall', {params: {"organizationId": $scope.organizationId,"userId": $scope.userId,"mobileCode":$scope.viewvisitor.mobileCode,"mobile":$scope.viewvisitor.mobile,"visitorId":$scope.viewvisitor.id}});
             resprole.then(function (response) {
                 if (response.data.status === 'success') {
                    console.log(response.status);
                    console.log(response.data.message);
                    toaster.pop(response.status, "", response.data.message, 10000, 'trustedHtml');
                    $scope.ivrButton = 1;
                 }
            });
     };
	 
	   $scope.sendSepVLCall = function () 
     {                                
         Data.post('clickExotelToCall/exotelCall', {"organizationId":$scope.organizationId,"userId":$scope.userId,"mobile":$scope.viewvisitor.mobile,"mobileCode":$scope.viewvisitor.mobileCode,"visitorId":$scope.viewvisitor.id}).then(function (result) {
                if(result.status == 'success'){
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                }
                if(result.status == 'failed'){
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                }
                if(result.status == 'detail_failed'){
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                }                               
         });
    };


     $scope.callBtn = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setCallBtn', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
     };

    $scope.fileError = false;
    $scope.errorFile = "";
    $scope.getAttachFileDetails = function (e) {
        $scope.fileError = false;
        $scope.errorMsg = "";
		 //console.log("Getting Files");
        //$scope.files = [];
        var fsize = 0;
        var fileExtension = ['wav', 'mp3', 'amr', 'ogg', 'jpeg', 'jpg', 'txt', 'pdf', 'doc', 'msg', 'xls', 'csv', 'xlsx', 'docx', 'ppt', 'pptx', 'eml', 'tif', 'gif', 'png', 'html', 'rtf', 'mbox', 'odt', 'bmp', 'tiff', 'pps', 'xlr', 'ods', 'wps', 'vcf'];
        $scope.$apply(function () {

            // STORE THE FILE OBJECT IN AN ARRAY.
            for (var i = 0; i < e.files.length; i++) {
                var ftype = e.files[i]['name'].substr(e.files[i]['name'].lastIndexOf('.')+1);
                console.log(e.files[i]['size']);
                if ((fileExtension.indexOf(ftype.toLowerCase())) !== -1) {
                    $scope.files.push(e.files[i]);
                    fsize = fsize + parseInt(e.files[i]['size']);
                    $scope.fileError = false;
                } else {

                    $scope.errorFile = e.files[i]['name'];
                    $scope.fileError = true;

                }
            }
            if ($scope.fileError) {
                $scope.files = [];
                document.getElementById("attachFile").value = "";
				angular.element("input[type='file']").val(null);
                $scope.errorMsg = "Unsupported file format&nbsp;<i class='fa fa-info-circle' data-toggle='tooltip' title='Please check the supported formats  " + fileExtension.join() + "'></i>";
            }
            console.log((fsize / 1000) / 1000);
            if ((fsize / 1000) / 1000 > 5) {
                $scope.files = [];
                document.getElementById("attachFile").value = "";
				angular.element("input[type='file']").val(null);
                $scope.errorMsg = "The attachement limit should not exceed 5MB";
            }
            console.log($scope.errorFile);
            console.log($scope.fileError);
        });
    };
	$scope.clearFiles = function (file) {
        var index = $scope.files.indexOf(file);
        $scope.files.splice(index, 1);

        //$scope.tags.push(tag);
        //$scope.tagline="";
    };
	// Add business cards 
	   $scope.addBizCard = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var modalInstance = $uibModal.open({
            templateUrl: './uploadbizcard',
            controller: 'uploadbizcardCtrl',
            size: 'md',
            backdrop: 'static',
            resolve: {
                evisitor: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.viewvisitor['cardId'] = i;

            }
        });
        } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.lphoto = '';
    $scope.uppflag = 0;
    $scope.pfiles = {};
	// Add photo profile
    $scope.updateProfilePic = function (e) {
        $scope.uppflag = 1;
        $scope.leadPhoto = {};
        $scope.$apply(function () {


            //$scope.pfiles = e.files[0];
            if (parseInt(e.files[0]['size']) > 200000) {

                console.log("File size should not exceed 2MB");
                document.getElementById("photoFile").value = "";
                toaster.pop("error", "", "File size should not exceed 2MB", 10000, 'trustedHtml');
                $scope.uppflag = 0;
                e.files.length = 0;

            } else if (parseInt(e.files[0]['size']) < 200000) {
                fileReader.readAsDataUrl(e.files[0], $scope)
                        .then(function (result) {
                            //console.log($scope.filecsv);
                            $scope.lphoto = result;
                            //$scope.fmsg = '';
                            $scope.leadPhoto.visitorId = $scope.viewvisitor.id;
                            $scope.leadPhoto.organizationId = $scope.viewvisitor.organizationId;
                            $scope.leadPhoto.profilephotourl = $scope.lphoto;
                            Data.put('visitor/updateProfilePhoto', $scope.leadPhoto).then(function (result) {
                                if (result.status !== 'error') {
                                    $scope.uppflag = 0;
                                    $scope.viewvisitor['photourl'] = result.location;
                                    document.getElementById("photoFile").value = "";
                                } else {
                                    document.getElementById("photoFile").value = "";
                                }
                            });

                        });


            }
        });
    };
	$scope.clearPhoto = function () {
		var msg = {"message": "Are you sure you want to delete the profile picture of this lead ?", "title": "Delete profile picture", "canceltxt": "Cancel", "oktxt": "Delete"};
        warningPopup.warningpopmodel(msg);
        $scope.$on('warnresp', function (event, args) {
            if (args.status === 'true') {
                $scope.leadPhoto = {};
                $scope.leadPhoto.visitorId = $scope.viewvisitor.id;
                $scope.leadPhoto.organizationId = $scope.viewvisitor.organizationId;
                $scope.leadPhoto.profilephotourl = '';
                Data.put('visitor/updateProfilePhoto', $scope.leadPhoto).then(function (result) {
                    if (result.status !== 'error') {
                        $scope.uppflag = 0;
                        $scope.viewvisitor['photourl'] = result.location;
                        document.getElementById("photoFile").value = "";
                    } else {
                        document.getElementById("photoFile").value = "";
                    }
                });
            }
        });
    };
    $scope.s3fileOpen = function (url) {
        window.open(Data.webAppBase() + "account/s3fileviewer?fileURL=" + encodeURIComponent(url));
    };
    $scope.openFileDialog = function (e) {
        var file = document.getElementById('attachFile');
        file.click();
    };
	
	// ------------------- End of file upload functionality --------------------
	
	// ----------------------------Email SMTP integration ---------------------------
	 $scope.mailCount = '0';
    $scope.sendSepVLEmail = function (p, size) {
		
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "bulkEmail", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    console.log(response.data);
                    if (response.data.status === 'success') {
							$scope.vemailCount = 0;
							//console.log($scope.selectedVisitor.join());
							// $scope.checkAccountMailCount();
							var mailcountresp = $http.get(Data.serviceBase() + '/visitor/checkMailCount', {params: {"orgId": $scope.organizationId}});
							mailcountresp.then(function (response) {
								console.log(response);
								$scope.mailCount = response.data.count;
								var respromc = $http.get(Data.serviceBase() + '/visitor/checkEmailByIds', {params: {"visitorlist": p.id}});
								respromc.then(function (response) {
									$scope.vemails = response.data;
									var emailCount = 0;
									if ($scope.vemails !== '') {
										emailCount = $scope.vemails.split(',').length;
									}
									console.log(emailCount);
									console.log($scope.mailCount);
									if (emailCount > (1000 - parseInt($scope.mailCount))) {

										$scope.message = {"title": "Warning", "message": "Selected leads with email: " + emailCount + "<br>Current daily email credits : " + $scope.mailCount + "<br><br> You are running out of daily Email credits(1000). Kindly re-target your recipients."};
										alertUser.alertpopmodel($scope.message);
									} else if (emailCount === 0) {
										$scope.message = {"title": "Warning", "message": "EmailID does not exists for the selected lead(s). Kindly re-target your recipients."};
										alertUser.alertpopmodel($scope.message);
									} else {
										//console.log(response.data[0]);
										//$scope.eventSelected = response.data[0];
										var modalInstanceassignTo = $uibModal.open({
											templateUrl: './emailSepTemplate',
											controller: 'emailCtrlSep',
											size: 'lg',
											backdrop: 'static',
											resolve: {
												vvisitorlist: function () {
													return $scope.vemails;
												},
												orgCount: function () {
													return 1;
												},
												mailCount: function () {
													return $scope.mailCount;
												},
												subject: function () {
													return p && p.subject != undefined ? p.subject : '';
												},
												message: function () {
													return p && p.message != undefined ? p.message : '';
												},
												visitorEmailId: function () {
													return p && p.email != undefined ? p.email : '';
												}
											}
										});
										modalInstanceassignTo.result.then(function (i) {
											if (i) {
											   // $scope.checkstate = false;
												//$scope.selectedVisitor = [];
											   // $scope.selectedVisitor.length = 0;
											   // $scope.filterEventChanged();
											}
										});
									}

								}, function (response) {
									//console.log('Error happened -- ');
									//console.log(response);
								});
							}, function (response) {
								//console.log('Error happened -- ');
								//console.log(response);
							});
					} else {
                        response.data['cancel'] = 'true';
                        $scope.orgsendSepEmailPlanCheck(response.data);
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
	$scope.mailCount = '0';
	    $scope.sendSepVLSms = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "bulkSMS", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    console.log(response.data);
                    if (response.data.status === 'success') {
                        $scope.vemailCount = 0;
                        //console.log($scope.selectedVisitor.join());
                        // $scope.checkAccountMailCount();
                        var mailcountresp = $http.get(Data.serviceBase() + '/visitor/checkMobileCount', {params: {"orgId": $scope.organizationId}});
                        mailcountresp.then(function (response) {
                            console.log(response);
                            $scope.mailCount = response.data.count;
                            var respromc = $http.get(Data.serviceBase() + '/visitor/checkMobileByIds', {params: {"visitorlist": p.id}});
                            respromc.then(function (response) {
                                $scope.vmobiles = response.data;
                                console.log($scope.vmobiles);
                                var smsCount = 0;
                                if ($scope.vmobiles !== '') {
                                    smsCount = $scope.vmobiles.split(',').length;
                                }
                                console.log(smsCount);
                                console.log($scope.mailCount);
                                if (smsCount > (10000 - parseInt($scope.mailCount))) {

                                    $scope.message = {"title": "Warning", "message": "Selected leads with mobile number: " + smsCount + "<br>Current daily sms credits : " + $scope.mailCount + "<br><br> You are running out of daily sms credits(10000). Kindly re-target your recipients."};
                                    alertUser.alertpopmodel($scope.message);
                                } else if (smsCount === 0) {
                                    $scope.message = {"title": "Warning", "message": "Mobile number does not exists for the selected lead(s). Kindly re-target your recipients."};
                                    alertUser.alertpopmodel($scope.message);
                                } else {
                                    //console.log(response.data[0]);
                                    //$scope.eventSelected = response.data[0];
                                    var modalInstanceassignTo = $uibModal.open({
                                        templateUrl: './smsSepTemplate',
                                        controller: 'smsCtrlSep',
                                        size: 'lg',
                                        backdrop: 'static',
                                        resolve: {
                                            vvisitorlist: function () {
                                                return $scope.vmobiles;
                                            },
                                            orgCount: function () {
                                                return 1;
                                            },
                                            mailCount: function () {
                                                return $scope.mailCount;
                                            },
                                            message: function () {
                                                return p && p.message != undefined ? p.message : '';
                                            }
                                        }
                                    });
                                    modalInstanceassignTo.result.then(function (i) {
                                        if (i) {
                                            // $scope.checkstate = false;
                                            //$scope.selectedVisitor = [];
                                            // $scope.selectedVisitor.length = 0;
                                            // $scope.filterEventChanged();
                                        }
                                    });
                                }

                            }, function (response) {
                                //console.log('Error happened -- ');
                                //console.log(response);
                            });
                        }, function (response) {
                            //console.log('Error happened -- ');
                            //console.log(response);
                        });
                    } else {
                        response.data['cancel'] = 'true';
                        $scope.orgsendSepEmailPlanCheck(response.data);
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

	$scope.orgsendSepEmailPlanCheck = function (p, size) {
        var modalInstance1 = $uibModal.open({
            templateUrl: './checkPremiumPopup',
            controller: 'premiumCheckCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                msg: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };
    $scope.viewMailSent = function (mailId) {
        var resmail = $http.get(Data.serviceBase() + '/organization/getEmailContent', {params: {"emailId": mailId}});

        // $scope.users = {};
        resmail.then(function (response) {
            $scope.vemail = response.data;
            var modalInstance = $uibModal.open({
                templateUrl: './viewEmail',
                controller: 'viewEmailCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    vemail: function () {

                        return $scope.vemail;
                    },
                }
            });
            modalInstance.result.then(function (i) {
                if (i) {

                    /* if (i.visitorId !== undefined) {
                     window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + i.visitorId, "_self");
                     }*/
                    //$scope.filterEventChanged();
                }
            });
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        console.log(mailId);
    };
	
	$scope.viewMailReceive = function (mailId) {
        var resmail = $http.get(Data.serviceBase() + '/organization/getEmailRevContent', {params: {"emailId": mailId}});

        // $scope.users = {};
        resmail.then(function (response) {
            $scope.vemail = response.data;
            var modalInstance = $uibModal.open({
                templateUrl: './viewRevEmail',
                controller: 'viewEmailRevCtrl',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    vemail: function () {

                        return $scope.vemail;
                    },
                }
            });
            modalInstance.result.then(function (mailDet) {
                if (mailDet) {
                    console.log(mailDet);

                    var resprole = $http.get(Data.serviceBase() + '/organization/getOrgEmailRevContent', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "emailId": mailDet.id}});
                    // $scope.users = {};
                    resprole.then(function (response) {
                        //console.log(response);
                        $scope.loader = false;
                        if (response.data) {
                            $scope.emessage = response.data;
                            $scope.epmessage = {};
                     $scope.epmessage.id = mailDet.visitorId;
                     $scope.epmessage.to = mailDet.fromEmail;
                     $scope.epmessage.subject = mailDet.subject;
					 var dataorg = mailDet.date; 
					 var datorg = dataorg.replace(' ', 'T');
					datorg = datorg + "Z";
					var valdate = new Date(datorg);
					 var rdate = $filter('date')(valdate,'dd-MMM-yy H:mm a');
                     $scope.epmessage.message = "<html><body><br><br><br><hr>Received on:&nbsp;"+rdate+"<br>From:&nbsp;"+mailDet.fromEmail+"<br>To:&nbsp;"+mailDet.to+"<br>Subject:&nbsp;"+mailDet.subject+"<br><br>"+$scope.vemail.body+"</html></body>";
                            $scope.sendSepVLEmail($scope.epmessage);

                        }
                    }, function (response) {

                        console.log('Error happened -- ');
                        console.log(response);
                    });
                    /* if (i.visitorId !== undefined) {
                     window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + i.visitorId, "_self");
                     }*/
                    //$scope.filterEventChanged();
                    /*$scope.emessage = {};
                     $scope.emessage.id = mailDet.visitorId;
                     $scope.emessage.to = mailDet.fromEmail;
                     $scope.emessage.subject = mailDet.subject;
                     $scope.emessage.message = "<html><body><br><br><br><hr>From:"+mailDet.fromEmail+"<br>To:"+mailDet.to+"<br>Subject:"+mailDet.subject+"<br><br>"+$scope.vemail.body+"</html></body>";
                     */

                }
            });
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
        console.log(mailId);
    };


    if (sessionStorage.getItem('emailPId') && sessionStorage.getItem('emailPId') !== null && sessionStorage.getItem('emailPId') !== 'null') {
        console.log("Inside email parse");
        var epid = sessionStorage.getItem('emailPId');
        $scope.viewMailReceive(epid);
        sessionStorage.setItem('emailPId', null);
    }
	
	$scope.cloneClick = function (p, selecttab, size) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        $scope.relLead = angular.copy(p);
        //console.log(p);
        var modalInstance = $uibModal.open({
            templateUrl: './modifyVisitor',
            controller: 'editvisitorCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                evisitor: function () {
                    $scope.relLead.id = undefined;
                    $scope.relLead.services = "";
                    $scope.relLead.categories = "";
                    $scope.relLead.tags = "";
                    $scope.relLead.notes = "";
                    $scope.relLead.notes = "";
                    return $scope.relLead;
                },
                tabsel: function () {
                    return selecttab;
                },
                currententId: function () {
                    var tempevent = $scope.eventId;
                    if ($scope.eventId !== 0) {
                        tempevent = $scope.eventId;
                    }

                    return tempevent;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

                if (i.visitorId !== undefined) {
                    window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + i.visitorId, "_self");
                }
                //$scope.filterEventChanged();
            }
        });
		 } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.cloneLead = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setCloneLead', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.addRelatedLead = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setAddRelatedLead', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

//Date time picker config
// global config picker
    $scope.picker6 = {
        date: new Date(),
        open: true
    };
	$scope.minDate = new Date();
    $scope.openCalendar = function (e, picker) {
        $scope.picker6.open = true;
    };

    $scope.files = [];
    $scope.upload = function () {
        //alert($scope.files.length+" files selected ... Write your Upload Code"); 

    };
    /* $scope.getFile = function () {
     $scope.progress = 0;
     fileReader.readAsDataUrl($scope.file, $scope)
     .then(function (result) {
     //console.log($scope.file);
     
     $scope.files.push(result);
     });
     };*/
    $scope.opnURL = function (site) {
        var s = site;
		if (s !== undefined && s !== "" && s !== null) {
			if(s.indexOf("//") > 0){
				 return s;
			}else{
				return "http://" + s;
			}
		}
    };
    $scope.shareleadCard = function (p, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
		 var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "shareinfo"}});
        // $scope.users = {};
        resprole.then(function (response) {
            //console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var modalInstanceshare = $uibModal.open({
            templateUrl: './sharevisitor',
            controller: 'sharevisitorCtrl',
            size: 'md',
            resolve: {
                vvisitor: function () {
                    return p;
                }
            }
        });
        modalInstanceshare.result.then(function (i) {
            if (i) {
                //$scope.refresh();
            }
        });
		 } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to share lead information. Only account owner or Managers (within HelloLeads) can share lead information. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            //console.log('Error happened -- ');
            //console.log(response);
        });
		 } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.orgUsers = function () {
        var orgusersresp = $http.get(Data.serviceBase() + 'user/userByOrganization', {params: {"organizationId": orgId}});
        orgusersresp.then(function (response) {
            $scope.organizationUsers = response.data;
            angular.forEach(response.data, function (d) {
                if (d.id === $scope.viewvisitor.gAssignTo) {
                    //console.log("Matching User ID");
                    //console.log(d);
                    $scope.gAssigner = d;
                }
                if (d.id === $scope.viewvisitor.gAssigne) {
                    //console.log("Matching User name");
                    //console.log(d);
                    $scope.gAssignee = d.firstName + " " + d.lastName;
                }
            });
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    };
	 $scope.orgDetails = {};
    // --------------------------------------------------------------- Getting Currency details ----------------------------------------------------
	$scope.getCurrencyDets = function () {
    var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

    resprom.then(function (response) {
        if (response.data) {
            $scope.orgDetails = response.data[0];
        }

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
	};
    $scope.sumTab = true;
    $scope.gtouchTab = false;
    $scope.commentTab = false;
    $scope.sameOrgTab = false;
    $scope.loaderView = true;
    $scope.activestyle1 = "#2F4050";
    $scope.activestyle2 = "";
    $scope.activestyle3 = "";
    $scope.activestyle4 = "";
    //$scope.reletedTab = false;
    $scope.switchMenu = function (tabid) {
        if (tabid === 1) {
            $scope.sumTab = true;
            $scope.activestyle1 = "#2F4050";
            $scope.activestyle2 = "";
            $scope.activestyle3 = "";
            $scope.activestyle4 = "";
            $scope.gtouchTab = false;
            $scope.commentTab = false;
            $scope.sameOrgTab = false;
            // $scope.reletedTab = false;
        } else if (tabid === 2) {
            $scope.sumTab = false;
            $scope.gtouchTab = true;
            $scope.commentTab = false;
            $scope.sameOrgTab = false;
            $scope.activestyle1 = "";
            $scope.activestyle3 = "";
            $scope.activestyle4 = "";
            $scope.activestyle2 = "#2F4050";
			//$scope.viewvisitor.nextFollow = $scope.nxtFollow;
						
            //$scope.reletedTab = false;
        } else if (tabid === 3) {
            $scope.sumTab = false;
            $scope.gtouchTab = false;
            $scope.commentTab = true;
            $scope.sameOrgTab = false;
            $scope.activestyle1 = "";
            $scope.activestyle2 = "";
            $scope.activestyle4 = "";
            $scope.activestyle3 = "#2F4050";
             $scope.activityTab();
            //$scope.reletedTab = false;
        } else if (tabid === 4) {
            $scope.sumTab = false;
            $scope.gtouchTab = false;
            $scope.commentTab = false;
            $scope.sameOrgTab = true;
            //$scope.visitorRelated();
            $rootScope.$broadcast('masonry.reload');
            $scope.activestyle1 = "";
            $scope.activestyle2 = "";
            $scope.activestyle3 = "";
            $scope.activestyle4 = "#2F4050";
             $scope.relatedTab();
        }
        /*   else if (tabid === 4) {
         $scope.sumTab = false;
         $scope.gtouchTab = false;
         $scope.commentTab = true;
         $scope.reletedTab = false;
         }*/
    };

    $scope.activityTab = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setActivityTab', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.relatedTab = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setRelatedTab', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
     };

    $scope.visitorComments = function () {
        $scope.commentLoader = true;
       if ($scope.viewvisitor.id !== undefined) {
            var visitorComment = $http.get(Data.serviceBase() + 'visitor/leadAcitivites', {params: {"visitorId": $scope.viewvisitor.id}});
            visitorComment.then(function (responseC) {
                $scope.vCommentsdet = responseC.data;
                //console.log($scope.vCommentsdet);
                //$scope.loaderView = false;
                $scope.commentLoader = false;
                $scope.commentdet = {};
				$scope.files = [];
                $scope.errorMsg ="";
                document.getElementById("attachFile").value = "";
                $scope.filterComments();
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
    };
	    $scope.selActType = "";
    // Filter within activities -------------
    $scope.filterComments = function (selType) {

        if ($scope.selActType !== '0') {
            var filterComments = [];
            angular.forEach($scope.vCommentsdet, function (value) {

                if ($scope.selActType === 'slog' && value.transType !== 'LCOM' && value.transType !== 'LFNT') {
                    filterComments.push(value);
                }
                if ((value.transType === 'LCOM' || value.transType === 'LFNT') && $scope.selActType === 'LCOM') {

                    filterComments.push(value);


                }
                if (value.transType === 'LCOM' && $scope.selActType === 'LCOMA' && value.transType !== 'LFNT') {
                    if (value.commentFiles.length > 0) {
                        filterComments.push(value);
                    }

                }
            });

            return filterComments;
        } else {
            return $scope.vCommentsdet;
        }

    };

     $scope.filterCommentsDD = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setfilterComments', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };  

    $scope.clearFilterComment = function () {
        $scope.selActType = '0';
        $scope.commentSearch = "";
        $scope.leadRest();
    };

    $scope.leadRest = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setLeadRest', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.commentSearchBox = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setCommentSearchBox', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    // -----------------------------------
    $scope.vFieldValues = {};
    $scope.visitorFieldsValues = function () {
        $scope.commentLoader = true;
        if ($scope.viewvisitor.id !== undefined) {
            var visitorField = $http.get(Data.serviceBase() + 'visitor/visitorValues', {params: {"visitorId": $scope.viewvisitor.id}});
            visitorField.then(function (responseF) {
                $scope.vFieldValues = responseF.data;
                //$scope.loaderView = false;
                //$scope.commentLoader = false;
                //$scope.commentdet = {};
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
    };
    
    $scope.visitorRelated = function () {
        $scope.vRelatesdet.length = 0;
        $scope.nxtFollow = "";
        if ($scope.viewvisitor.email) {
            if ($scope.viewvisitor.id !== undefined && $scope.viewvisitor.email.length > 2 && $scope.viewvisitor.email.indexOf("@") !== -1) {
                var visitorRelate = $http.get(Data.serviceBase() + 'visitor/getVisitorRelate', {params: {"organizationId": orgId, "orgEmail": $scope.viewvisitor.email.split('@')[1], "orgName": $scope.viewvisitor.visitorOrganizationName, "visitorId": $scope.viewvisitor.id}});
                visitorRelate.then(function (responseR) {
                    $scope.vRelatesdet = responseR.data;
                    $scope.loaderView1 = false;
                    $scope.loaderView = false;
                    $timeout.cancel($scope.timeout);
                }, function (response) {
                    //console.log('Error happened -- ');
                    //console.log(response);
                });
            }
        }
        if ($scope.viewvisitor.createdAt || $scope.viewvisitor.modifiedAt) {
            //console.log($scope.viewvisitor.nextFollow);
            //$scope.voldnxtFollow = $scope.viewvisitor.nextFollow;
            var visitorresp = $http.get(Data.serviceBase() + '/visitor/getVisitorDetail', {params: {"visitorId": $scope.viewvisitor.id}});
             visitorresp.then(function (response) {
             var data = response.data;
             angular.forEach(data,function(value,key)
             {  
                 if(value.createdAt){
                    var datorg=value.createdAt;
                    datorg = datorg.replace(' ', 'T');
                    datorg = datorg + "Z";
                    var valdate = new Date(datorg);
                    console.log("Created At" + $filter('date')(valdate, 'dd-MMM-yyyy HH:mm'));                 
                    $scope.viewvisitor.createdAt = $filter('date')(valdate, 'dd-MMM-yyyy HH:mm');
                 }
                 if(value.modifiedAt){
                    var mdate=value.modifiedAt;
                    mdate = mdate.replace(' ', 'T');
                    mdate = mdate + "Z";
                    var modDate = new Date(mdate);
                    console.log("Modified At" + $filter('date')(modDate, 'dd-MMM-yyyy HH:mm'));                 
                    $scope.viewvisitor.modifiedAt = $filter('date')(modDate, 'dd-MMM-yyyy HH:mm');
                 }
             });
             });
            }
             if ($scope.viewvisitor.nextFollow) {
                //console.log($scope.viewvisitor.nextFollow);
                $scope.voldnxtFollow = $scope.viewvisitor.nextFollow;
                var visitorresp = $http.get(Data.serviceBase() + '/visitor/getVisitorDetail', {params: {"visitorId": $scope.viewvisitor.id}});
                 visitorresp.then(function (response) {
                 var data = response.data;
                 angular.forEach(data,function(value,key)
                 {
                     var datorg=value.nextFollow;
                     datorg = datorg.replace(' ', 'T');
                     datorg = datorg + "Z";
                     var valdate = new Date(datorg);
                     console.log("Next Followup" + $filter('date')(valdate, 'dd-MMM-yyyy HH:mm:ss'));
                     $scope.nextFollowDate = $filter('date')(valdate, 'dd-MMM-yyyy HH:mm');
                 });
                 });
                // var datorg = $scope.viewvisitor.nextFollow;
                //          datorg = datorg.replace(' ', 'T');
                //          datorg = datorg + "Z";
                //          var valdate = new Date(datorg);
                //console.log($filter('date')(valdate, 'dd-MMM-yyyy HH:mm:ss'));
                //$scope.nextFollowDate = $filter('date')(valdate, 'dd-MMM-yyyy HH:mm');
                /*if($scope.viewvisitor.followFreq === '' || $scope.viewvisitor.followFreq === undefined || $scope.viewvisitor.followFreq === null){
                $scope.viewvisitor.followFreq = '30';
                }
                if($scope.viewvisitor.followCallRemind === '' || $scope.viewvisitor.followCallRemind === undefined || $scope.viewvisitor.followCallRemind === null){
                $scope.viewvisitor.followCallRemind = '0';
                }*/
                }else if($scope.viewvisitor.dontFollow === '1'){
                $scope.viewvisitor.followFreq = null;
                $scope.viewvisitor.followCallRemind = '1';
                }
                else{
                    $scope.viewvisitor.followFreq = null;
                    $scope.viewvisitor.followCallRemind = null;
                    //$scope.notifyFlag = true;
            }
    };
    
    $scope.deleteComment = function (comment) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var visitorComment = $http.get(Data.serviceBase() + 'visitor/deleteComment', {params: {"commentId": comment.id}});
        visitorComment.then(function (responseC) {
            $scope.visitorComments();
            ////console.log(responseC);
            toaster.pop(responseC.data.status, "", responseC.data.message, 10000, 'trustedHtml');
            // $scope.vCommentsdet = responseC.data;
            //$scope.commentdet = {};
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
		 } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.filterVisitorDetails = function () {
        $scope.viewvisitors = {};
        $scope.viewVisitorList = [];
        page = 0;
        if ($scope.filterdetindex === 'potential') {
            var resprom = $http.get(Data.serviceBase() + '/visitor/filterVisitor', {params: {"eventId": $scope.eventId, "orgId": orgId, "potential": $scope.filterdetlabel, "category": "", "interest": ""}});
            resprom.then(function (response) {
                $scope.viewvisitors = response.data;
                $scope.loaderView = false;
                angular.forEach($scope.viewvisitors, function (v) {
                    if (v) {
                        page = page + 1;
                        $scope.viewVisitorList[page] = v;
                        if (v.id === $scope.visitorId) {
                            $scope.curpage = page;
                            $scope.viewvisitor = v;
                        }
                    }
                });
                $rootScope.title = "Lead Details";
                $scope.orgUsers();
                $scope.visitorComments();
                $scope.visitorRelated();
                $scope.visitorFieldsValues();
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
        if ($scope.filterdetindex === 'category') {
            var resprom = $http.get(Data.serviceBase() + '/visitor/filterVisitor', {params: {"eventId": $scope.eventId, "orgId": orgId, "potential": "", "category": $scope.filterdetlabel, "interest": "", "type": ""}});
            resprom.then(function (response) {
                $scope.viewvisitors = response.data;
                $scope.loaderView = false;
                angular.forEach($scope.viewvisitors, function (v) {
                    if (v) {
                        page = page + 1;
                        $scope.viewVisitorList[page] = v;
                        if (v.id === $scope.visitorId) {
                            $scope.curpage = page;
                            $scope.viewvisitor = v;
                        }
                    }
                });
                $rootScope.title = "Lead Details";
                $scope.orgUsers();
                $scope.visitorComments();
                $scope.visitorRelated();
                $scope.visitorFieldsValues();
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
        if ($scope.filterdetindex === 'interest') {
            var resprom = $http.get(Data.serviceBase() + '/visitor/filterVisitor', {params: {"eventId": $scope.eventId, "orgId": orgId, "potential": "", "category": "", "interest": $scope.filterdetlabel, "type": ""}});
            resprom.then(function (response) {
                $scope.viewvisitors = response.data;
                $scope.loaderView = false;
                angular.forEach($scope.viewvisitors, function (v) {
                    if (v) {
                        page = page + 1;
                        $scope.viewVisitorList[page] = v;
                        if (v.id === $scope.visitorId) {
                            $scope.curpage = page;
                            $scope.viewvisitor = v;
                        }
                    }
                });
                $rootScope.title = "Lead Details";
                $scope.orgUsers();
                $scope.visitorComments();
                $scope.visitorRelated();
                $scope.visitorFieldsValues();
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
        if ($scope.filterdetindex === 'assignTo') {
            var resprom = $http.get(Data.serviceBase() + '/visitor/filterVisitor', {params: {"orgId": orgId, "potential": "", "category": "", "assignTo": $scope.filterdetlabel}});
            resprom.then(function (response) {
                $scope.viewvisitors = response.data;
                $scope.loaderView = false;
                angular.forEach($scope.viewvisitors, function (v) {
                    if (v) {

                        page = page + 1;
                        $scope.viewVisitorList[page] = v;
                        if (v.id === $scope.visitorId) {
                            $scope.curpage = page;
                            $scope.viewvisitor = v;
                        }
                        //console.log(page);
                    }
                });
                $rootScope.title = "Lead Details";
                $scope.orgUsers();
                $scope.visitorComments();
                $scope.visitorRelated();
                $scope.visitorFieldsValues();
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
        if ($scope.filterdetindex === 'assignee') {
            var resprom = $http.get(Data.serviceBase() + '/visitor/filterVisitor', {params: {"orgId": orgId, "potential": "", "category": "", "assignee": $scope.filterdetlabel}});
            resprom.then(function (response) {
                $scope.viewvisitors = response.data;
                $scope.loaderView = false;
                angular.forEach($scope.viewvisitors, function (v) {
                    if (v) {
                        page = page + 1;
                        $scope.viewVisitorList[page] = v;
                        if (v.id === $scope.visitorId) {
                            $scope.curpage = page;
                            $scope.viewvisitor = v;
                        }
                    }
                });
                $rootScope.title = "Lead Details";
                $scope.orgUsers();
                $scope.visitorComments();
                $scope.visitorRelated();
                $scope.visitorFieldsValues();
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
        //$scope.filterlabel="";
        //$scope.filterindex="";
        //sessionStorage.setItem("filterdetlabel", "");
        //sessionStorage.setItem("filterdetindex", "");



    };
    $scope.showLeadBcard = function (p, size) {
        var visitorBcard = $http.get(Data.serviceBase() + 'visitor/getvisitorBcard', {params: {"visitorId": p.id}});
        visitorBcard.then(function (response) {
            $scope.vBcard = response.data[0];
            //console.log(response.data[0]);
            var modalInstance = $uibModal.open({
                templateUrl: './viewBcard',
                controller: 'viewBcardCtrl',
                size: "lg",
                resolve: {
                    vvisitor: function () {
                        return p;
                    },
                    vbcard: function () {
                        return $scope.vBcard;
                    }
                }
            });
        }, function (response) {
            //console.log('Error occured -- ');
            //console.log(response);
        });
    };
    /*$scope.updateVisitor = function (editflag, visitor) {
     
     if ($scope.visitorEdit.$dirty) {
     Data.put('visitor/visitorSep', visitor).then(function (result) {
     if (result.status !== 'error') {
     //$uibModalInstance.close(1);
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     } else {
     //console.log(result);
     }
     });
     //console.log("True");
     }
     if ($scope.visitorEdit.$pristine) {
     //console.log("False");
     return false;
     
     }
     };*/

    $scope.visitorGetStorage = function () {
        page = 0;
        $scope.viewVisitorList = [];
        ////console.log("Visitor data");
        ////console.log(visitorShareData.getData()[0]);
        $scope.viewvisitors = visitorShareData.getVisitorData()[0];
        localStorage.removeItem("visitors");
        $scope.loaderView = false;
        angular.forEach($scope.viewvisitors, function (v) {
            if (v) {
                page = page + 1;
                $scope.viewVisitorList[page] = v;
                if (v.id === $scope.visitorId) {
                    $scope.curpage = page;
                    $scope.viewvisitor = v;
                }
            }
        });
        //console.log("Page Count " + page);
        $rootScope.title = "Lead Details";
        $scope.orgUsers();
        $scope.visitorComments();
        $scope.visitorRelated();
        $scope.visitorFieldsValues();
    };
    $scope.visitorGetDetail = function () {
        ////console.log("Get Details");

        // if ($scope.eventId === 0) {
        var visitorresp = $http.get(Data.serviceBase() + '/visitor/getVisitorDetail', {params: {"visitorId": $scope.visitorId}});
        visitorresp.then(function (response) {
            $scope.viewvisitors = response.data;
            $scope.loaderView = false;
            angular.forEach($scope.viewvisitors, function (v) {
                if (v) {
                    page = page + 1;
                    $scope.viewVisitorList[page] = v;
                    if (v.id === $scope.visitorId) {
                        $scope.curpage = page;
                        $scope.viewvisitor = v;
                    }
                }
            });
            $rootScope.title = "Lead Details";
            $scope.orgUsers();
            $scope.visitorComments();
            $scope.visitorRelated();
            $scope.visitorFieldsValues();
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
        //   } else {
        /*     var visitorresp = $http.get(Data.serviceBase() + '/visitor/allvisitorbyevent', {params: {"eventId": $scope.eventId, "orgId": orgId}});
         visitorresp.then(function (response) {
         $scope.viewvisitors = response.data;
         $scope.loaderView = false;
         angular.forEach($scope.viewvisitors, function (v) {
         if (v) {
         page = page + 1;
         $scope.viewVisitorList[page] = v;
         if (v.id === $scope.visitorId) {
         $scope.curpage = page;
         $scope.viewvisitor = v;
         }
         }
         });
         $rootScope.title = "Lead Details";
         $scope.orgUsers();
         $scope.visitorComments();
         $scope.visitorRelated();
         $scope.visitorFieldsValues();
         }, function (response) {
         //console.log('Error happened -- ');
         //console.log(response);
         });
         }*/
    };
    //console.log("Debug " + ($location.absUrl().match(/&/g) || []).length);
    ////console.log("Debug "+($location.absUrl().lastIndexOf("&")));
    ////console.log("Debug "+($location.absUrl().split("&")[1]));
    if ($location.absUrl().indexOf('visitorId') > 0 && $location.absUrl().indexOf('?') > 0 && $location.absUrl().indexOf('&') <= 0)
    {

        $scope.visitorId = $location.absUrl().split("=")[1];
        //console.log("Visitor Details");
        $scope.loaderView = true;
        localStorage.setItem('curEventId', "0");
        localStorage.setItem('curEventName', "");
        var visitorsdet1 = visitorShareData.getVisitorData()[0];
        if (visitorsdet1 !== undefined && visitorsdet1.length > 0) {
            ////console.log("Search View1");
            $scope.loaderView = true;
            $scope.visitorGetStorage();
        } else {
            $scope.visitorGetDetail();
        }
        // $scope.visitorGetDetail();

    } else if ($location.absUrl().indexOf('visitorId') > 0 && $location.absUrl().indexOf('eventId') > 0 && $location.absUrl().indexOf('?') > 0 && $location.absUrl().indexOf('&') > 0) {
        var visitorDet = $location.absUrl().split("&")[0];
        var eventDet = $location.absUrl().split("&")[1];
        if (($location.absUrl().match(/&/g) || []).length >= 2) {
            var filterDet = $location.absUrl().split("&")[2];
            $scope.filterdetlabel = filterDet.split("=")[1];
            $scope.filterdetindex = filterDet.split("=")[0];
        }
        $scope.visitorId = visitorDet.split("=")[1];
        $scope.eventId = eventDet.split("=")[1];
        ////console.log("Event Details" + $scope.eventId);
        $scope.loaderView = true;
        var visitorsdet2 = visitorShareData.getVisitorData()[0];
        //////console.log("Visitor Details");
        // ////console.log(visitorsdet2.length);
        if (visitorsdet2 !== undefined && visitorsdet2.length > 0) {
            ////console.log("Search View2");
            $scope.loaderView = true;
            $scope.visitorGetStorage();
        } else {
            if ($scope.filterdetlabel) {
                $scope.filterVisitorDetails();
            } else {
                $scope.visitorGetDetail();
            }
        }

    } else if ($location.absUrl().indexOf('visitorId') > 0 && $location.absUrl().indexOf('?') > 0 && $location.absUrl().indexOf('&') >= 0 && ($location.absUrl().indexOf('assignTo') > 0 || $location.absUrl().indexOf('assignee')))
    {

        var visitorDet = $location.absUrl().split("&")[0];
        $scope.visitorId = visitorDet.split("=")[1];
        ////console.log("Visitor Details");
        $scope.loaderView = true;
        if ($location.absUrl().indexOf('&') > 0) {
            //////console.log($location.absUrl().indexOf('&'));
            var filterDet = $location.absUrl().split("&")[1];
            $scope.filterdetlabel = filterDet.split("=")[1];
            $scope.filterdetindex = filterDet.split("=")[0];
        }
        var visitorsdet = visitorShareData.getVisitorData()[0];
        if (visitorsdet !== undefined && visitorsdet.length > 0) {
            ////console.log("Search View3");
            $scope.loaderView = true;
            $scope.visitorGetStorage();
        } else {
            ////console.log($scope.filterdetlabel);
            if ($scope.filterdetlabel) {
                $scope.filterVisitorDetails();
            } else {
                $scope.visitorGetDetail();
            }
        }
        // $scope.visitorGetDetail();

    }

    $scope.visitorChange = function (vvisitor) {
        ////console.log(page);
        $scope.loaderView1 = true;
        $scope.sumTab = true;
        $scope.gtouchTab = false;
        $scope.commentTab = false;
        $scope.sameOrgTab = false;
        $scope.activestyle1 = "#2F4050";
        $scope.activestyle2 = "";
        $scope.activestyle3 = "";
        $scope.activestyle4 = "";
		$scope.nextFollowDate = "";
        $scope.vCommentsdet = {};
        $scope.vRelatesdet = {};
		$scope.viewvisitor = {};
		$scope.voldnxtFollow = "";
        angular.forEach($scope.viewvisitors, function (v) {
            if (v.id === vvisitor.id) {
                $scope.viewvisitor = v;
            }
        });
        var count = 0;
        $rootScope.title = "Lead Details";
        if ($scope.timeout) {

            $timeout.cancel($scope.timeout);
        }
        $scope.timeout = $timeout(function () {
            count = count + 1;
            ////console.log(count);
            if (count === 1) {
                $scope.orgUsers();
                $scope.visitorComments();
                $scope.visitorRelated();
                $scope.visitorFieldsValues();
                $scope.loaderView1 = false;
            }
        }, 2000);
    };
    $scope.visitorNext = function () {

        if ($scope.curpage < $scope.viewVisitorList.length - 1) {

            $scope.curpage = $scope.curpage + 1;
            ////console.log("Next");
            ////console.log($scope.curpage);
            ////console.log($scope.viewvisitors.length);
            $scope.visitorChange($scope.viewVisitorList[$scope.curpage]);
        }


    };
    $scope.visitorPrevious = function () {

        if ($scope.curpage > 1 && $scope.curpage !== 0) {

            $scope.curpage = $scope.curpage - 1;
            ////console.log("Previous");
            ////console.log($scope.curpage);
            $scope.visitorChange($scope.viewVisitorList[$scope.curpage]);
        }

    };
    $scope.makeUrl = function (vvisitor) {
        var orgnaiz = vvisitor.visitorOrganizationName;
        var city = vvisitor.city;
        var state = vvisitor.state;
        var country = vvisitor.country;
        if (city && state) {
            return vvisitor.visitorOrganizationName + "+" + vvisitor.country;
        } else {
            return vvisitor.visitorOrganizationName + "+" + vvisitor.country;
        }
    };
	
	$scope.makeaddressUrl = function (vvisitor) {
		var address1 = vvisitor.address1;
		var address2 = vvisitor.address2;
        var city = vvisitor.city;
        var state = vvisitor.state;
        var country = vvisitor.country;
		var resultStr = '';
		
		if(address1){
			resultStr = resultStr + address1;
			
		}
		if(address2){
			if(resultStr != ''){
				resultStr = resultStr + "+" + address2;
			}else{
				resultStr = resultStr + address2;
			}
			
		}
        
        if (city && state) {
			if(resultStr != ''){
				resultStr =resultStr + "+" + city + "+" + state;
			}else{
				resultStr =resultStr + city + "+" + state;
			}
            
        } 
		if(vvisitor.country) {
			if(resultStr != ''){
				resultStr =resultStr + "+" +  vvisitor.country;
			}else{
				resultStr =resultStr +  vvisitor.country;
			}
            
        }
		return resultStr;
    };
    /*$scope.dnfChange = function (visitor) {

        if (visitor.dontFollow === "0") {
            visitor.dontFollowRes = "";
        }


    };*/
    $scope.organizationUsers = {};
    $scope.gAssigner = [];
    $scope.gAssignee = "";
	$scope.commentStatus = false;
    //$scope.organizationId = sessionStorage.getItem('orgId');

    $scope.postComment = function (comment) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var currentUserId = sessionStorage.getItem('userId');
        var currentUsername = sessionStorage.getItem('firstName') + " " + sessionStorage.getItem('lastName');
        comment.visitorId = $scope.viewvisitor.id;
        comment.commentUserId = currentUserId;
        comment.commentUsername = currentUsername;
		$scope.commentStatus = true;
        //$scope.commentdet={};
        //////console.log($scope.files[0]);
        //fileUpload.uploadFileToUrl($scope.files[0]);
        //////console.log(comment);
        Data.post('visitor/visitorComments', comment).then(function (result) {
            if (result.status !== 'error') {
				$scope.commentStatus = false;

                $scope.attachFiles(result.commentId, $scope.viewvisitor.id, result);
				//toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                //$uibModalInstance.close(1);
                ////console.log('Storing comments for visitor ');
                ////console.log(result.message);
                /*var visitorComment = $http.get(Data.serviceBase() + 'visitor/leadAcitivites', {params: {"visitorId": $scope.viewvisitor.id}});
                visitorComment.then(function (responseC) {
                    $scope.vCommentsdet = responseC.data;
                    $scope.commentdet = {};
                }, function (response) {
                    ////console.log('Error happened -- ');
                    ////console.log(response);
                });*/
                //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
            } else {
                ////console.log(result);
            }
        });
		 } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.resetGtouch = function (cvisitor, assigner) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        cvisitor = {"dontFollow": "0"};
        $scope.gAssigner = {};
		$scope.nextFollowDate = "";
        assigner = {};
        $scope.viewvisitor.nextFollow = "";
        $scope.Gtouch(cvisitor, assigner,$scope.nextFollowDate);
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.delsepvisitor = function (p, size) {
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
		 var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "deleteLead"}});

        // $scope.users = {};
        resprole.then(function (response) {
            //console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var modalInstance1 = $uibModal.open({
            templateUrl: './deletevisitor',
            controller: 'delvisitorCtrl',
            size: size,
            resolve: {
                dvisitor: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i) {

                window.location.href = Data.webAppBase() + 'account/manageVisitor';

            }
        });
		 } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to delete lead information. Only account owner or Managers (within HelloLeads) can delete lead information. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            //console.log('Error happened -- ');
            //console.log(response);
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.saveCheck = function (viewvisitor) {
        if ($scope.viewvisitor.dontFollow === "1" && !$scope.viewvisitor.dontFollowRes) {
            return true;
        }
        if ($scope.gAssigner.length === 0 && $scope.nextFollowDate) {
            return true;
        }
		if(($scope.nextFollowDate === "" || $scope.nextFollowDate === undefined) && $scope.viewvisitor.notify === '1'){
            return true;
        }

    };
    $scope.checkNotify = function () {
        //console.log($scope.viewvisitor.nextFollow);
        if (($scope.viewvisitor.nextFollow === "" || $scope.viewvisitor.nextFollow === undefined) && $scope.viewvisitor.notify === '1') {
           //console.log($scope.viewvisitor.notify);
            //$scope.viewvisitor.notify = "'0'";
            //console.log($scope.viewvisitor.notify);
            toaster.pop("error", "", "Please specify the follow up date and time to notify", 8000, 'trustedHtml');
            
        }
    };
	
    $scope.checkFollowDate = function () {

        if ($scope.nextFollowDate === '' || $scope.nextFollowDate === undefined || $scope.viewvisitor.dontFollow === '1') {
            $scope.notifyFlag = true;
        } else if($scope.viewvisitor.followFreq === null || $scope.viewvisitor.followFreq === undefined || $scope.viewvisitor.followFreq === '') {
            $scope.viewvisitor.followFreq = '30';
            //$scope.viewvisitor.followCallRemind = '0';
            $scope.notifyFlag = false;
        }else if(($scope.viewvisitor.followFreq !== null && $scope.viewvisitor.followFreq !== '')){
            //$scope.viewvisitor.followFreq = '30';
           // $scope.viewvisitor.followCallRemind = '0';
            $scope.notifyFlag = false;
        }
		
		if($scope.viewvisitor.followCallRemind === null){
			$scope.viewvisitor.followCallRemind = '0';
		}
		
		
		
		
		/*else {
            $scope.viewvisitor.followFreq = '30';
            $scope.viewvisitor.followCallRemind = '0';
            $scope.notifyFlag = false;
        }*/

    };
    $scope.dnfChange = function (visitor) {

        if (visitor.dontFollow === "0") {
            visitor.dontFollowRes = "";
        }
         
            $scope.viewvisitor.followFreq = '-1';
            $scope.viewvisitor.followCallRemind = '1';
            
        

    };
	
	$scope.callRemaindCheck = function(state){
        console.log(state);
        if($scope.nextFollowDate === '' || $scope.nextFollowDate === null || $scope.nextFollowDate === undefined){
            toaster.pop("info", "", "Set follow-up date to get on time follow-up reminder", 10000, 'trustedHtml');
            
            if(state === 1){
                $scope.viewvisitor.followCallRemind = '0';
            }
            else{
                $scope.viewvisitor.followCallRemind = '1';
            }
        }
    };
    $scope.repeatFollowCheck = function(){
        if($scope.nextFollowDate === '' || $scope.nextFollowDate === null || $scope.nextFollowDate === undefined){
            toaster.pop("info", "", "Repeat follow-up works only if follow-up date is set", 10000, 'trustedHtml');
            
            $scope.viewvisitor.followFreq = null;
            //console.log($scope.viewvisitor.followFreq);
        }
    };
	
	
    $scope.Gtouch = function (cvisitor, assigner,nxtFollow) {
        //////console.log(assigner.id);
		 var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        $scope.gData = {};
        var curId = sessionStorage.getItem('userId');
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": curId, "permission": "openAssignments"}});

        // $scope.users = {};
        resprole.then(function (response) {
            ////console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                $scope.gData.visitorId = $scope.viewvisitor.id;
                $scope.gData.gAssignTo = assigner ? assigner.id : "";
                $scope.gData.dontFollow = cvisitor.dontFollow;
                $scope.gData.followDesc = cvisitor.followDesc;
                $scope.gData.dontFollowRes = cvisitor.dontFollowRes;
				$scope.gData.followCallRemind = cvisitor.followCallRemind;
                $scope.gData.followupFreq = cvisitor.followFreq;
                if (nxtFollow && nxtFollow !== '0000-00-00 00:00:00') {
                    $scope.nextD = new Date(nxtFollow);
                } else {
                    $scope.nextD = "";
                }
                //$scope.gData.nextfollow = $filter('date')($scope.nextD, "yyyy-MM-dd HH:mm:ss");
                $scope.gData.nextFollow = $scope.nextD;
                //if (cvisitor.gAssigne) {
                //////console.log("data");
                //  $scope.gData.gAssigne = cvisitor.gAssignee;
                // } else {
                //////console.log("changed");
                $scope.gData.gAssigne = curId;
                if ($scope.gData.dontFollow === '1') {
                    $scope.gData.nextFollow = "";
                }
				if (cvisitor.notify === '1' && $scope.gData.nextFollow) {
                    $scope.gData.notify = "1";
                }
                // }
                ////console.log($scope.gData);
                Data.put('visitor/visitorgTouch', $scope.gData).then(function (result) {
                    if (result.status !== 'error') {
                        //$scope.ugalert = true;
                        //   $timeout(function () {
                        //     $scope.ugalert = false;
                        // }, 4500);
                        //$uibModalInstance.close(1);
                        ////console.log('Storing comments for visitor ');
                        if ($scope.gData.gAssignTo === undefined) {
                            $scope.gAssigner = '';
                        }
                        $scope.viewvisitor.gAssignTo = $scope.gData.gAssignTo;
                        //$scope.viewvisitor.gAssigne = $scope.gData.gAssigne;
                        $scope.viewvisitor.dontFollow = $scope.gData.dontFollow;
                        $scope.viewvisitor.followDesc = $scope.gData.followDesc;
                        // $scope.viewvisitor.dontFollowRes = $scope.gData.dontFollowRes;
                        //console.log(result.message);
						if ($scope.gData.nextFollow !== "") {
                            $scope.viewvisitor.nextFollow = $filter('date')($scope.gData.nextFollow, 'yyyy-MM-dd HH:mm', 'UTC');
                        }
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
						$scope.visitorComments();
                    }else {
						var msg = "<br/><br/><h3><a href ='"+Data.webAppBase()+"account/viewLead?visitorId="+result.visitor+"' target='_blank'><b>View the Lead</b> <i class='fa fa-external-link'></i></a></h3>";
                        $scope.message = {"title": "Warning", "message":result.message + msg };
                        alertUser.alertpopmodel($scope.message);
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to Assign or Change assignments of leads. Only account owner or users with Manager role can modify assignments. Please contact HelloLeads account owner or managers in your company for modifying lead assignments."};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            //console.log('Error happened -- ');
            //console.log(response);
        });
		
			} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
	$scope.adminvisitor = function (p, size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './viewAdminLead',
            controller: 'adminvisitorCtrl',
            size: 'lg',
            resolve: {
                adminV: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i) {
                //$scope.gData.dfollow = '1';
            }
        });
    };
    $scope.confvisitor = function (p, size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './confirmvisitor',
            controller: 'confirmvisitorCtrl',
            size: size,
            resolve: {
                dvisitor: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i) {
                $scope.gData.dfollow = '1';
            }
        });
    };
    $scope.confvisitorcomment = function (p, size) {
		var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var modalInstance1 = $uibModal.open({
            templateUrl: './confirmvisitorcomment',
            controller: 'confirmvisitorcommentCtrl',
            size: size,
            resolve: {
                comment: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            if (i) {
                $scope.visitorComments();
            }
        });
		} else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
});
evtApp.controller('confirmvisitorcommentCtrl', function ($scope, $http, Data, $uibModalInstance, comment, toaster) {
    $scope.ecomment = comment;
    $scope.title = "Confirmation";
    $scope.confirmok = function (dcomment) {

        if (comment.commentId !== undefined) {
            $uibModalInstance.close(1);
            var visitorComment = $http.get(Data.serviceBase() + 'visitor/deleteComment', {params: {"commentId": dcomment.commentId}});
            visitorComment.then(function (responseC) {
				$uibModalInstance.close(1);
                //$scope.visitorComments();
                ////console.log(responseC);
                toaster.pop(responseC.data.status, "", responseC.data.message, 10000, 'trustedHtml');
                // $scope.vCommentsdet = responseC.data;
                //$scope.commentdet = {};
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('adminvisitorCtrl', function ($scope, $http, Data, $uibModalInstance, adminV, toaster) {
    $scope.adminV = adminV;
    $scope.orgs = {};
    
    var adminResp = $http.get(Data.serviceBase() + 'visitor/getAdminStatus', {params: {"emailId": $scope.adminV.email}});
    adminResp.then(function (responseC) {
        //$scope.visitorComments();
        console.log(responseC.data.orgs[0].vccount);
        $scope.org = responseC.data.orgs[0];
        //$scope.adminDet = responseC.data[0];
        // $scope.vCommentsdet = responseC.data;
        //$scope.commentdet = {};
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
        

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('confirmvisitorCtrl', function ($scope, Data, $uibModalInstance, dvisitor, toaster) {
    $scope.dvisitor = dvisitor;
    $scope.title = "Confirmation";
    $scope.confirmok = function (comment) {

        if (visitor.firstName !== undefined) {
            $uibModalInstance.close(1);
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('editvisitorCtrl', function ($scope, $rootScope, Data, $timeout, $uibModal, alertUser, $parse, $uibModalInstance, tabsel, currententId, evisitor, toaster, $http, fileReader, $window) {
    $scope.email = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
    $scope.imagephotoSrc = '';
    var original = angular.copy($scope.evisitor);
    $scope.sendG = localStorage.getItem("curEventsend");
    $scope.lshare = localStorage.getItem("curEventshare");
    $scope.colorbg = "";
    $scope.eventLists = {};
    $scope.eventSelection = 0;
    $scope.addcgrouptxt = false;
    $scope.eventtempSelection = {};
    if (currententId === 0) {
        $scope.currenteventId = '0';
    } else {
        $scope.currenteventId = currententId;
    }
    $scope.organizationId = sessionStorage.getItem("orgId");

    $scope.evisitor = angular.copy(evisitor);
    if ($scope.evisitor.id !== undefined) {
        $scope.title = "Update Lead";
        $scope.btntxt = "Update";
        $scope.eventSelection = $scope.evisitor.eventId;
    } else {
        $scope.title = "Add New Lead";
        $scope.btntxt = "Add";
        $scope.eventSelection = $scope.currenteventId;
       if ($scope.sendG === '0' && $scope.evisitor.sendGreet === undefined) {
            $scope.evisitor.sendGreet = "0";
        } else if($scope.sendG === '1' && $scope.evisitor.sendGreet === undefined) {

            $scope.evisitor.sendGreet = "1";
            $scope.colorbg = "#f8f8f8";
        }
        if ($scope.lshare === '0' ) { //&& $scope.evisitor.lead_share === undefined
            $scope.evisitor.lead_share = "0";
        } else if($scope.lshare === '1' ) { //&& $scope.evisitor.lead_share === undefined

            $scope.evisitor.lead_share = "1";
            $scope.colorbg = "#f8f8f8";
        }
    }
    $scope.userId = sessionStorage.getItem('userId');
    $scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }
    $scope.addvisitorevent = function () {
        ////console.log("Hi");
        $uibModalInstance.close();
        $scope.emptyvisitorevent = {}
        $rootScope.addevent($scope.emptyvisitorevent);
    };
    $scope.loader = true;
    $scope.getFile = function () {
        $scope.progress = 0;
        fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    //console.log($scope.file);
                    $scope.imagephotoSrc = result;
                });
    };
    $scope.moveLeadFlag = '0';
    // Move lead Popup
    $scope.movePopUp = function () {


        $scope.message = {"title": "Warning!", "message": "Please note that qualifiers (Customer group, Interest & Custom information) associated with this lead will be cleared. Only the contact information and activity history of the lead will be moved"};
        alertUser.alertpopmodel($scope.message);
    };
    $scope.moveSwitch = function () {

        if ($scope.moveLeadFlag === '0') {
            //$scope.movePopUp();
            $scope.moveLeadFlag = '1';
        }

    };

    $scope.redirectToOrg = function () {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "changeCurrency"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                window.open(Data.webAppBase() + 'account/company', '_blank'); // in new tab
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to change the currency type. Users with account owner can the change the currency type. Please contact HelloLeads account owner in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };
    $scope.getcsvFile = function () {
        $scope.progress = 0;
        fileReader.readAsDataUrl($scope.filecsv, $scope)
                .then(function (result) {
                    //console.log($scope.filecsv);
                    $scope.fileCsv = result;
                });
    };
    $scope.canSaveTask = function () {
        return $scope.editvisitorform.$valid && !angular.equals($scope.evisitor, original);
    };
    $scope.setRequired = function (index, value, req)
    {
        console.log(req + "----" + value);
        if (req === '0' && value === '') {
            $scope.requiredField[index] = '0';
        } else {
            $scope.requiredField[index] = '1';
        }
    };
    $scope.validateReq1 = function () {
        if ($scope.evisitor.firstName && $scope.eventtempSelection.name) {

            return false;
        } else {
            return true;
        }
    };
    $scope.fullvalidateReq = function () {
        if ($scope.evisitor.firstName && $scope.evisitor.potential && $scope.eventtempSelection.name && $scope.requiredField.join('').indexOf('0') === -1) {

            return false;
        } else {
            return true;
        }
    };
	  $scope.tagsList = {};
    $scope.suggestTag = function (tags) {
        var resprom = $http.get(Data.serviceBase() + '/field/tags', {params: {key: "organizationId", "value": $scope.organizationId}});

        $scope.tagsList = {};
        resprom.then(function (response) {
            $scope.tagsList = response.data;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    };
	
    $scope.setCategory = function (categ) {
        $scope.selectedCategories = categ;
        $scope.selCategname = categ.name;
    };
    /*$scope.checkField = function(req,index,value){
     if(req === '1'){
     $scope.requiredField[index]="";
     }else if(value === "" && req === '0'){
     $scope.requiredField[index]="0";
     }
     };*/
    // -------------------------------------------------- Show business card control ------------------------------------------------------------------
    $scope.showBusinessCard = function (p, size) {

        /* var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "changeHistory"}});
         
         // $scope.users = {};
         resprole.then(function (response) {
         console.log(response);
         $scope.loader = false;
         if (response.data.status === 'success') {*/
        //$scope.getAllServCategField();
        var modalInstance = $uibModal.open({
            templateUrl: './showBusinessCardProfile',
            controller: 'showBizCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                laevent: function () {
                    return p;
                },
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

            }
        });
        /*  } else {
         $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to view the change history of lists. Users with account owner or manger roles can view the change history of lists. Please contact HelloLeads account owner or mangers in your company for assistance with this task"};
         alertUser.alertpopmodel($scope.message);
         //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
         }
         }, function (response) {
         
         console.log('Error happened -- ');
         console.log(response);
         });*/


    };
    $scope.orgDetails = {};
	$scope.getCountryCodeDataOrg = function () {
    // --------------------------------------------------------------- Getting Currency details ----------------------------------------------------
    var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

    resprom.then(function (response) {
        if (response.data) {
            $scope.orgDetails = response.data[0];
        }

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
	};


	//Getting OrgEmailTemp
	$scope.getOrgEmailTempFlag = function () {
	var resprom = $http.get(Data.serviceBase() + '/organization/organizationEmailTempFlag', {params: {"organizationId": $scope.organizationId}});
	    resprom.then(function (response) {
	        if (response.data) {
	            $scope.OrgEmailTemp = response.data;
	        }
	    }, function (response) {
	        console.log('Error happened -- ');
	        console.log(response);
	    });
	};
	 //End Getting OrgEmailTemp

 // For mobile country code
	 $scope.orgPhoneCode = sessionStorage.getItem('countryCode');
       /* if ($scope.evisitor.mobileCode === null || $scope.evisitor.mobileCode === undefined || $scope.evisitor.mobileCode === '') {
     $scope.evisitor.mobileCode = $scope.orgPhoneCode;
     }*/
    // Country code for mobile 
    $scope.countrymobDet = {};
	$scope.getCountryCodeData = function () {
    $http.get('../../../css/phone.json').success(function (data) {
        $scope.countrymobDet = data;
        if (($scope.evisitor.mobileCode === null || $scope.evisitor.mobileCode === undefined || $scope.evisitor.mobileCode === '') && ($scope.orgPhoneCode === null || $scope.orgPhoneCode === '' || $scope.orgPhoneCode === undefined))
        {
            //console.log($scope.countrymobDet);
            var country = sessionStorage.getItem('orgCountry');
            angular.forEach($scope.countrymobDet, function (value, key) {
                //console.log("First " +value.calling_code);
                if (value.calling_code === $scope.orgPhoneCode) {
                    //console.log(value.calling_code);
                    $scope.orgPhoneCode = value.calling_code;
                    $scope.evisitor.mobileCode = value.calling_code;
                }else if(value.country === country){
                    $scope.orgPhoneCode = value.calling_code;
                    $scope.evisitor.mobileCode = value.calling_code;
                }
            });
        }
		else if(($scope.orgPhoneCode !== '' || $scope.orgPhoneCode !== null || $scope.orgPhoneCode !== undefined) &&($scope.evisitor.mobileCode === null || $scope.evisitor.mobileCode === undefined || $scope.evisitor.mobileCode === '') ) {
                $scope.evisitor.mobileCode = $scope.orgPhoneCode;
                console.log($scope.evisitor.mobileCode);
        }
	
	
    });
	};

	
	
    //-----------------------------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------ End business card ctrl ----------------------------------------------------------   


    var eventresp = $http.get(Data.serviceBase() + '/event/orgEventList', {params: {"organizationId": $scope.organizationId}});
    eventresp.then(function (response) {
        //console.log(response.data);
        $scope.eventLists = response.data;
        angular.forEach($scope.eventLists, function (d) {
            if (d.id === $scope.currenteventId || d.id === $scope.eventSelection) {
                $scope.eventtempSelection = d;
                $scope.sendG = $scope.eventtempSelection.sendGreet;
                $scope.lshare = $scope.eventtempSelection.listlead_share;
                localStorage.setItem('curEventsend','');
                localStorage.setItem('curEventshare','');
               
                if($scope.evisitor.sendGreet === undefined && $scope.sendG === '0'){
                    $scope.evisitor.sendGreet = '0';
                }else if($scope.evisitor.sendGreet === undefined && $scope.sendG === '1'){
                    $scope.evisitor.sendGreet = '1';
                }
                if($scope.evisitor.lead_share === undefined && $scope.lshare === '0'){
                    $scope.evisitor.lead_share = '0';
                }else if($scope.evisitor.lead_share === undefined && $scope.lshare === '1'){
                    $scope.evisitor.lead_share = '1';
                }
				localStorage.setItem('curEventsend','');
                localStorage.setItem('curEventshare','');
				localStorage.setItem('curEventsend',$scope.evisitor.sendGreet);
                localStorage.setItem('curEventshare',$scope.evisitor.lead_share);
                console.log("Event Set");
                //$scope.eventChanged();
            }

        });
        $scope.loader = false;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
// ----------------------------- Get Lead duplicates based on email -----------------------------------------------------

    $scope.emailVerify = true;
    $scope.emailDupes = {};
    $scope.checkEmailDupe = function (email) {
        if ($scope.evisitor.firstName) {
            if (!editvisitorform.email.$error && email) {
                var emaildupe = $http.get(Data.serviceBase() + '/visitor/checkEmailDup', {params: {"email": email, "organizationId": $scope.organizationId}});
                emaildupe.then(function (response) {
                    console.log(response.data);
                    $scope.emailDupes = response.data;
                    if ($scope.emailDupes.length > 0) {
                        var modalInstanceshare = $uibModal.open({
                            templateUrl: './leadDuplicate',
                            controller: 'leadDupctrl',
                            size: 'md',
                            backdrop: 'static',
                            resolve: {
                                vvisitors: function () {
                                    return $scope.emailDupes;
                                },
                                vvisitor: function () {
                                    return $scope.evisitor;
                                },
                                chkType: function () {
                                    return 'email';
                                }
                            }
                        });
                        modalInstanceshare.result.then(function (i) {
                            if (i) {
                                //$scope.refresh();
                            }
                        });
                        $scope.loader = false;
                        $scope.emailVerify = true;

                    } else {
                        $scope.verifyLoader = true;
                        $scope.emailVerify = '';
                        var emailverify = $http.get(Data.serviceBase() + '/visitor/vemailVerification', {params: {"verifyEmail": email}});
                        emailverify.then(function (response) {
                            $scope.emailVerify = response.data;
                            $scope.verifyLoader = false;
                        }, function (response) {
                            //console.log('Error happened -- ');
                            //console.log(response);
                        });
                    }
                }, function (response) {
                    //console.log('Error happened -- ');
                    //console.log(response);
                });
            }
        } else {
            toaster.pop("error", "", "Please provide lead's first name", 10000, 'trustedHtml');
        }

    };
    $scope.mobileDupes = {};
    $scope.checkMobileDupe = function (mobile) {
        if ($scope.evisitor.firstName) {
			if(mobile){ 
				if (mobile.length > 4) {
					var emaildupe = $http.get(Data.serviceBase() + '/visitor/checkMobileDup', {params: {"mobile": mobile, "organizationId": $scope.organizationId}});
					emaildupe.then(function (response) {
						console.log(response.data);
						$scope.mobileDupes = response.data;
						if ($scope.mobileDupes.length > 0) {
							var modalInstanceshare = $uibModal.open({
								templateUrl: './leadDuplicate',
								controller: 'leadDupctrl',
								size: 'md',
								backdrop: 'static',
								resolve: {
									vvisitors: function () {
										return $scope.mobileDupes;
									},
									vvisitor: function () {
										return $scope.evisitor;
									},
									chkType: function () {
										return 'mobile';
									}
								}
							});
							modalInstanceshare.result.then(function (i) {
								if (i) {
									//$scope.refresh();
								}
							});
							$scope.loader = false;
						}
					}, function (response) {
						//console.log('Error happened -- ');
						//console.log(response);
					});
				}
			}
        } else {
            toaster.pop("error", "", "Please provide lead's first name", 10000, 'trustedHtml');
        }

    };
    //-----------------------------------------------------------------------------------------------------------------------------------  

    $scope.tagsadded = [];
    $scope.AddTags = function (tag) {
        if (tag) {
            //console.log($scope.tagsadded.indexOf(tag));
            if ($scope.tagsadded.indexOf(tag) <= -1) {
                $scope.tagsadded.push(tag);
                $scope.tagline = "";
            }
        }
    };
    $scope.clearTags = function (tag) {
        var index = $scope.tagsadded.indexOf(tag);
        $scope.tagsadded.splice(index, 1);
        //$scope.tags.push(tag);
        //$scope.tagline="";
    };
    if ($scope.evisitor['tags']) {
        $scope.tagsadded = $scope.evisitor['tags'].split(',');
    }
    $scope.canSaveTask = function () {
        return $scope.editvisitorform.$valid && !angular.equals($scope.evisitor, original);
    };
	
    //---VV Creating Potetial List and add it into drop down
    $scope.potentiallist = ["High", "Medium", "Low", "Not Relevant"];
    if ($scope.evisitor.potential) {
        angular.forEach($scope.potentiallist, function (d) {
            if (d === $scope.evisitor.potential) {
                //console.log(d);
                $scope.evisitor.potential = d;
            }
        });
    } else {
        $scope.evisitor.potential = "Low";
    }
    //---VV Creating Lead Stage List and add it into drop down
    //---VV Creating Lead Stage List and add it into drop down
    $scope.Lstages = [];
    // Get the custom lead stages -----------------
    var stageresp = $http.get(Data.serviceBase() + '/organization/getCurrentStages', {params: {"organizationId": $scope.organizationId}});
    stageresp.then(function (response) {
        console.log(response.data);
		$scope.stagelist = response.data;

        if ($scope.evisitor.stageId) {
            angular.forEach($scope.stagelist, function (d) {
                if (d.id === $scope.evisitor.stageId) {
					$scope.evisitor.type = d.stage;
                    $scope.evisitor.stageId = d.id;
                }
            });

        } else {
			$scope.evisitor.type = $scope.stagelist[0]['stage'];
            $scope.evisitor.stageId = $scope.stagelist[0]['id'];
        }
		console.log($scope.evisitor.stageId);

    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });

    //-----------------------------------------------------------------------------------------------------------------------------------------------

    $scope.maintab = 1;
    $scope.mainFirstTab = false;
    $scope.mainSecodTab = true;
    $scope.switcheventTab = function (tabId) {
        if (tabId === 1) {
            //console.log("Tab Active" + tabId);
            $scope.tab = 1;
            //angular.element(document.getElementById("visitordet-1")).addClass("active");
            //angular.element(document.getElementById("visitordet-2")).removeClass("active");
            //angular.element(document.getElementById("leadinfo")).removeClass("active");
            $scope.firstTab = false;
            $scope.secondTab = true;
            $scope.thirdTab = true;
        } else if (tabId === 2) {
            //console.log("Tab Active" + tabId);
            $scope.tab = 2;
            //angular.element(document.getElementById("visitordet-1")).removeClass("active");
            //angular.element(document.getElementById("visitordet-2")).removeClass("active");
            //angular.element(document.getElementById("leadinfo")).addClass("active");
            $scope.firstTab = true;
            $scope.secondTab = true;
            $scope.thirdTab = false;
        } else {
            //console.log("Tab Active" + tabId);
            $scope.tab = 3;
            // angular.element(document.getElementById("visitordet-2")).addClass("active");
            // angular.element(document.getElementById("visitordet-1")).removeClass("active");
            // angular.element(document.getElementById("leadinfo")).removeClass("active");
            $scope.firstTab = true;
            $scope.secondTab = false;
            $scope.thirdTab = true;
        }

    };
    $scope.switchmainTab = function (tabId) {

        if (tabId === 1) {
            $scope.maintab = 1;
            $scope.mainFirstTab = false;
            $scope.mainSecodTab = true;
        } else {
            $scope.maintab = 2;
            $scope.mainFirstTab = true;
            $scope.mainSecodTab = false;
        }


    };
    $scope.tab = 1;
    //angular.element(document.getElementById("visitordet-1")).addClass("active");
    //angular.element(document.getElementById("visitordet-2")).removeClass("active");
    //angular.element(document.getElementById("leadinfo")).removeClass("active");
    $scope.firstTab = false;
    $scope.secondTab = true;
    $scope.thirdTab = true;
    $scope.septab = false;
    if (tabsel === 'NA') {
        $scope.septab = false;
    } else {
        $scope.septab = true;
        //console.log("Else");
        if (tabsel === 'first') {
            //console.log("First");
            $scope.tab = 1;
            $scope.switcheventTab(1);
            $scope.firstTab = false;
            $scope.secondTab = true;
            $scope.thirdTab = true;
        }
        if (tabsel === 'second') {
            $scope.tab = 3;
            //angular.element(document.getElementById("visitordet-2")).addClass("active");
            //angular.element(document.getElementById("visitordet-1")).removeClass("active");
            //angular.element(document.getElementById("leadinfo")).removeClass("active");
            //console.log("Second");
            $scope.switcheventTab(3);
            $scope.firstTab = true;
            $scope.secondTab = false;
            $scope.thirdTab = true;
        }
        if (tabsel === 'third') {
            //console.log("Third");
            $scope.tab = 2;
            $scope.switcheventTab(2);
            $scope.firstTab = true;
            $scope.secondTab = true;
            $scope.thirdTab = false;
        }
    }

    $scope.continuevisit2 = function () {
        $scope.tab = 2;
        //angular.element(document.getElementById("visitordet-1")).removeClass("active");
        //angular.element(document.getElementById("visitordet-2")).removeClass("active");
        //angular.element(document.getElementById("leadinfo")).addClass("active");
        $scope.firstTab = true;
        $scope.secondTab = true;
        $scope.thirdTab = false;
    };
    $scope.continuevisit1 = function () {
        if ($scope.evisitor.email || $scope.evisitor.mobile) {

            $scope.tab = 3;
            // angular.element(document.getElementById("visitordet-2")).addClass("active");
            // angular.element(document.getElementById("visitordet-1")).removeClass("active");
            // angular.element(document.getElementById("leadinfo")).removeClass("active");
            $scope.firstTab = true;
            $scope.secondTab = false;
            $scope.thirdTab = true;
        } else {
            toaster.pop("info", "", "Email ID / Mobile number is mandatory", 10000, 'trustedHtml');
        }
    }
    $scope.eventcategories = {};
    $scope.selectedCategories = {};
    $scope.allefieldsDate = {};
    $scope.allefieldsDateType = {};
	$scope.allefieldsDate = {};
    $scope.valueAdds = {};
    var eventId = '';
    //console.log("Event ID");
    //console.log(eventId);
    //console.log($scope.evisitor.eventId);
    if ($scope.evisitor.eventId !== undefined) {
        // console.log("Get from visitor");
        eventId = $scope.evisitor.eventId;

    } else if (currententId) {
        eventId = currententId;
    } else {
        //console.log("Get from local");

        eventId = localStorage.getItem("curEventId");

    }

    ////console.log($scope.evisitor.eventId);
    $scope.cureventId = eventId;
    $scope.filedValuelist = {};
    $scope.allefields = {};
    $scope.requiredField = [];
    $scope.extraFields = {};
    $scope.fieldVal = "";
    $scope.eventservices = {};
    $scope.visitorservices = [];
    $scope.fieldsSelectedIds = [];

    if (eventId) {
        console.log("Event Selected");
        $scope.loader = true;
        $scope.timeout = $timeout(function () {
             var eventcategoryresp = $http.get(Data.serviceBase() + '/category/category',
                    {params: {"key": "organizationId", "value": $scope.organizationId}});
            eventcategoryresp.then(function (response) {
                $scope.eventcategories = response.data;
                if ($scope.evisitor.id !== undefined) {
                    var visitorCategoryresp = $http.get(Data.serviceBase() + '/visitorcategory/visitorcategory',
                            {params: {"key": "visitorId", "value": $scope.evisitor.id}});
                    visitorCategoryresp.then(function (response) {
                        if (response.data) {
                            angular.forEach($scope.eventcategories, function (d) {
                                if (response.data[0]) {
                                    if (response.data[0].categoryId !== undefined) {
                                        if (d.id === response.data[0].categoryId) {
                                            console.log("got VisitorCategory as");
                                            console.log(d);
                                            $scope.selectedCategories = d;
                                            $scope.selCategname = d.name;
                                        }
                                    }
                                }
                            });
                        }
                        //-- next  we loop thru all categories - if eventcategories exist, then
                        //-- we add them to the temp list (selectedCategories)
                        //-- if not, selectedCategories will have null
                        //-- selectedCategories is equivalent to eventcategories, with null for unselected categories
                        //-- we use selectedCategories as the model when building the UI for selected categories

                        /* for (var i = 0; i < $scope.eventcategories.length; i++) {
                         if ($scope.visitorcategories.indexOf($scope.eventcategories[i]['id']) > -1) {
                         $scope.selectedCategories.push($scope.eventcategories[i]['id']);
                         ////console.log($scope.categories[i]['id']);
                         } else {
                         $scope.selectedCategories.push(null);
                         }
                         
                         }*/

                    }, function (response) {
                        //console.log('Error happened -- ');
                        //console.log(response);
                    });
                }

            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
            $scope.selectedService = [];
            $scope.visitorservices =[];
            var eventserviceresp = $http.get(Data.serviceBase() + '/service/service', {params: {"key": "organizationId", "value": $scope.organizationId}});
            eventserviceresp.then(function (response) {
                //console.log("Services lists");
                //console.log(response.data);
                $scope.eventservices = response.data;
                if ($scope.evisitor.eventId !== undefined) {
                    var visitorserviceresp = $http.get(Data.serviceBase() + '/visitorservice/visitorservice',
                            {params: {"key": "visitorId", "value": $scope.evisitor.id}});
                    visitorserviceresp.then(function (response) {
                        //console.log("Visitor Services lists");
                        //console.log(response.data);
                        angular.forEach(response.data, function (d) {
                            $scope.visitorservices.push(d.serviceId);
                        });
                        console.log($scope.visitorservices);
                        //-- next  we loop thru all eventservices - if visitorservice exist, then
                        //-- we add them to the temp list (selectedService)
                        //-- if not, selectedService will have null
                        //-- selectedService is equivalent to visitorservice, with null for unselected services
                        //-- we use selectedService as the model when building the UI for selected services
                        //console.log("Total length services" + $scope.eventservices[0]['id']);
                        for (var i = 0; i < $scope.eventservices.length; i++) {
                            console.log($scope.visitorservices.indexOf($scope.eventservices[i]['id']));
                            if ($scope.visitorservices.indexOf($scope.eventservices[i]['id']) > -1) {
                                $scope.eventservices[i]['checked'] = '0';
                                $scope.selectedService.push($scope.eventservices[i]);
                                console.log("Final list of services");
                                console.log($scope.selectedService);
                            } else {
                                $scope.eventservices[i]['checked'] = '1';
                            }
                        }
                        $scope.loader = false;
                        //console.log('Finally selected Services');
                        //console.log($scope.selectedService);
                    }, function (response) {
                        //console.log('Error happened -- ');
                        //console.log(response);
                    });
                }

            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
            var fieldresp = $http.get(Data.serviceBase() + '/event/eventFields', {params: {"eventId": eventId}});
            fieldresp.then(function (response) {
                console.log("Field lists");
                console.log(response.data);
                $scope.extraFields = response.data;
                var ind = 0;
                angular.forEach(response.data, function (d) {
                    $scope.fieldsSelectedIds.push(d.id);

                    if ($scope.evisitor.id === undefined) {
                        if (d.fieldReq === '0') {
                            $scope.requiredField[ind] = '0';
                        } else {
                            $scope.requiredField[$scope.fieldsSelectedIds.indexOf(d.id)] = '1';
                        }
                        ind = ind + 1;
                    } else {

                        if (d.fieldReq === '1') {
                            $scope.requiredField[$scope.fieldsSelectedIds.indexOf(d.id)] = '1';
                        } else if (d.fieldReq === '0') {
                            $scope.requiredField[ind] = '0';
                        }
                        ind = ind + 1;
                    }

                });
                //console.log($scope.fieldsSelectedIds);
                $scope.loader = false;
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
                $scope.visitor2custom();
            });
            ////console.log("Field Selected Ids");
            ////console.log($scope.fieldsSelectedIds);
            $scope.visitor2custom = function() {
            // if ($scope.evisitor.id !== undefined) {
                var fieldvalresp = $http.get(Data.serviceBase() + '/visitor/visitorValues', {params: {"visitorId": $scope.evisitor.id,"orgid": eventId}});
                fieldvalresp.then(function (response) {
                    console.log(response.data);
                    $scope.visitorFieldValues = response.data;
                    var ind = 0;
                    angular.forEach(response.data, function (d) {
                        ////console.log(d);
                        if ($scope.fieldsSelectedIds.indexOf(d.fieldId) !== -1) {
                            //console.log("Within loop assigning Id");
                            console.log(d.fieldId);
                            $scope.allefields[d.fieldId] = d.values;
                            $scope.allefieldsDate[d.fieldId] = d.values;
                            $scope.allefieldsDateType[d.fieldId] = d.fieldType;
                            $scope.requiredField[$scope.fieldsSelectedIds.indexOf(d.fieldId)] = '1';
                            // $scope.fieldVal = 
                            ind = ind + 1;
                        }

                    });
                    //$scope.fieldsSelectedIds=[];
                    ////console.log("Field Values");
                    ////console.log(response.data);
                    $scope.loader = false;
                }, function (response) {
                    //console.log('Error happened -- ');
                    //console.log(response);
                });
            //}
        }
            $scope.fieldValuesOrg = {};
            var fieldorgvalresp = $http.get(Data.serviceBase() + '/field/fieldValue', {params: {"organizationId": $scope.organizationId, "eventId": eventId}});
            fieldorgvalresp.then(function (response) {
                //console.log("Visitor Field Values");
                //console.log(response.data);
                $scope.fieldValuesOrg = response.data;
                //$scope.fieldsSelectedIds=[];
                ////console.log("Field Values");
                ////console.log(response.data);
                $scope.loader = false;
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }, 700);
    }
    ;
    $scope.visitorFieldValues = {};
    $scope.eventChanged = function () {
        /* if ($scope.evisitor.id !== undefined) {
         $scope.message = {"title": "Move Lead between Lists", "message": "Once moved, the qualifier details assigned to the lead will be reset. So do re-assign the qualifiers (Customer group, Interest  & Custom information)."};
         alertUser.alertpopmodel($scope.message);
         }*/
        console.log("Event Changed");
        $scope.loader = true;
        $scope.selectedService = [];
        $scope.selectedCategories = [];
        $scope.allefields = {};
        $scope.allefieldsDateType = {};
		$scope.allefieldsDate = {};
        //console.log("Event Selection");
        //console.log($scope.eventtempSelection.sendGreet);
        /*$scope.sendG = $scope.eventtempSelection.sendGreet;
        if ($scope.sendG === '0') {
            $scope.evisitor.sendGreet = "0";
        } else {
            $scope.evisitor.sendGreet = "1";
            $scope.colorbg = "#f8f8f8";
        }
        $scope.lshare = $scope.eventtempSelection.listlead_share;
        if ($scope.lshare === '0') {
            $scope.evisitor.lead_share = "0";
        } else {
            $scope.evisitor.lead_share = "1";
            $scope.colorbg = "#f8f8f8";
        }*/
		$scope.sendG = $scope.eventtempSelection.sendGreet;
        if ($scope.sendG === '0' ) { //&& $scope.evisitor.sendGreet === undefined
            $scope.evisitor.sendGreet = "0";
        } else if ( $scope.sendG === '1'){ //$scope.evisitor.sendGreet === undefined &&
            $scope.evisitor.sendGreet = "1";
            $scope.colorbg = "#f8f8f8";
        }
        $scope.lshare = $scope.eventtempSelection.listlead_share;
        if ($scope.lshare === '0' ) { //&& $scope.evisitor.lead_share === undefined
            $scope.evisitor.lead_share = "0";
        } else if( $scope.lshare === '1'){ //$scope.evisitor.lead_share === undefined &&
            $scope.evisitor.lead_share = "1";
            $scope.colorbg = "#f8f8f8";
        }
		localStorage.setItem('curEventshare',$scope.evisitor.lead_share);
		localStorage.setItem('curEventSend',$scope.evisitor.lead_share);
         var eventcategoryresp = $http.get(Data.serviceBase() + '/category/category',
                {params: {"key": "organizationId", "value": $scope.organizationId}});
        eventcategoryresp.then(function (response) {
            $scope.eventcategories = response.data;
            if ($scope.evisitor.id !== undefined) {


                if ($scope.eventcategories.indexOf($scope.selectedCategories) == -1) {
                    var temp = $scope.selectedCategories;
                    $scope.eventcategories.push(temp);

                }
                console.log($scope.eventcategories);
            }


        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
        console.log($scope.selectedService);
        var eventserviceresp = $http.get(Data.serviceBase() + '/service/service', {params: {"key": "organizationId", "value": $scope.organizationId}});
        eventserviceresp.then(function (response) {
            //console.log("Services lists");
            console.log(response.data);

            $scope.eventservices = response.data;
            var visitorServIds = [];
            if ($scope.evisitor.id !== undefined) {
                angular.forEach($scope.selectedService, function (k) {
                    visitorServIds.push(k.id);
                });
                console.log(visitorServIds);
                var selservice = [];
                angular.forEach($scope.eventservices, function (d, i) {
                    console.log(visitorServIds.indexOf(d.id));
                    selservice.push(d);
                    if (visitorServIds.indexOf(d.id) > -1) {
                        console.log('Inside 1');
                        $scope.eventservices[i]['checked'] = '0';
                        //$scope.eventservices.push(d);

                    } else{
                        $scope.eventservices[i]['checked'] = '1';
                    }
                });
               



                console.log($scope.eventservices);
            }
            $scope.loader = false;
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
        var fieldresp = $http.get(Data.serviceBase() + '/event/eventFields', {params: {"eventId": $scope.eventtempSelection.id}});
        fieldresp.then(function (response) {
            //console.log("Field lists");
            //console.log(response.data);
            $scope.extraFields = response.data;
            var ind = 0;
            angular.forEach(response.data, function (d) {
                $scope.fieldsSelectedIds.push(d.id);
                if ($scope.evisitor.id === undefined) {
                    if (d.fieldReq === '0') {
                        $scope.requiredField[ind] = '0';
                    } else {
                        $scope.requiredField[ind] = '1';
                    }
                    ind = ind + 1;
                }

            });
            /* angular.forEach(response.data, function (d) {
             $scope.fieldsSelectedIds.push(d.id);
             });*/
            $scope.loader = false;
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
            $scope.visitor2custom();
        });
        ////console.log("Field Selected Ids");
        ////console.log($scope.fieldsSelectedIds);
        /*if ($scope.evisitor.id !== undefined) {
         var fieldvalresp = $http.get(Data.serviceBase() + '/visitor/visitorValues', {params: {"visitorId": $scope.evisitor.id}});
         fieldvalresp.then(function (response) {
         //console.log("Field values");
         //console.log(response.data);
         $scope.visitorFieldValues = response.data;
         var ind = 0;
         angular.forEach(response.data, function (d) {
         ////console.log(d);
         if ($scope.fieldsSelectedIds.indexOf(d.fieldId) !== -1) {
         //console.log("Within loop assigning Id");
         //console.log(d.fieldId);
         $scope.allefields[d.fieldId] = d.values;
         $scope.requiredField[ind] = '1';
         // $scope.fieldVal = 
         ind = ind + 1;
         }
         
         });
         /* angular.forEach(response.data, function (d) {
         //console.log("Loop Count");
         //console.log(d);
         if ($scope.fieldsSelectedIds.indexOf(d.fieldId) !== -1) {
         //console.log("Within loop assigning Id");
         //console.log(d);
         $scope.allefields[d.fieldId] = d.values;
         }
         
         });
         //$scope.fieldsSelectedIds=[];
         ////console.log("Field Values");
         ////console.log(response.data);
         $scope.loader = false;
         }, function (response) {
         //console.log('Error happened -- ');
         //console.log(response);
         });
         }*/
         $scope.visitor2custom = function() {
         // if ($scope.evisitor.id !== undefined) {
                var fieldvalresp = $http.get(Data.serviceBase() + '/visitor/visitorValues', {params: {"visitorId": $scope.evisitor.id}});
                fieldvalresp.then(function (response) {
                    //console.log(response.data);
                    $scope.visitorFieldValues = response.data;
                    var ind = 0;
                    angular.forEach(response.data, function (d) {
                        ////console.log(d);
                        if ($scope.fieldsSelectedIds.indexOf(d.fieldId) !== -1) {
                            //console.log("Within loop assigning Id");
                            console.log(d.fieldId);
                            $scope.allefields[d.fieldId] = d.values;
                            $scope.allefieldsDate[d.fieldId] = d.values;
                            $scope.allefieldsDateType[d.fieldId] = d.fieldType;
                            $scope.requiredField[$scope.fieldsSelectedIds.indexOf(d.fieldId)] = '1';
                            // $scope.fieldVal = 
                            ind = ind + 1;
                        }

                    });
                    //$scope.fieldsSelectedIds=[];
                    ////console.log("Field Values");
                    ////console.log(response.data);
                    $scope.loader = false;
                }, function (response) {
                    //console.log('Error happened -- ');
                    //console.log(response);
                });
            // }
        }
        $scope.fieldValuesOrg = {};
        var fieldorgvalresp = $http.get(Data.serviceBase() + '/field/fieldValue', {params: {"organizationId": $scope.organizationId, "eventId": $scope.eventtempSelection.id}});
        fieldorgvalresp.then(function (response) {
            //console.log("Visitor Field Values");
            //console.log(response.data);
            $scope.fieldValuesOrg = response.data;
            //$scope.fieldsSelectedIds=[];
            ////console.log("Field Values");
            ////console.log(response.data);
            $scope.loader = false;
        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    };
    $scope.showmore = '0';
    $scope.ebtntxt = "";
    if ($scope.btntxt === 'Add') {
        $scope.ebtntxt = "Show more";
    } else {
        $scope.ebtntxt = "Show more";
    }
    $scope.expand = function () {
        if ($scope.showmore === '0') {
            $scope.showmore = '1';
            if ($scope.btntxt === 'Add') {
                $scope.ebtntxt = "Show less";
            } else {
                $scope.ebtntxt = "Show less";
            }
        } else {
            $scope.ebtntxt = "Show more";
            $scope.showmore = '0';
        }
    };
    /* if ($scope.btntxt === 'Add') {
     //Get Organization Tags
     var tagsresp = $http.get(Data.serviceBase() + '/visitor/orgTags', {params: {"organizationId": $scope.organizationId}});
     tagsresp.then(function (response) {
     //console.log("Tags lists");
     //console.log(response.data);
     //$scope.tagsadded = response.data;
     angular.forEach(response.data, function (d) {
     if ($scope.tagsadded.indexOf(d.tag) <= -1) {
     $scope.tagsadded.push(d.tag);
     $scope.tagline = "";
     }
     });
     $scope.loader = false;
     }, function (response) {
     //console.log('Error happened -- ');
     //console.log(response);
     });
     }*/

    //// ---------------------END --------------------------------------------------///
    $scope.addFieldValue = function (val, fid) {
        $scope.allefields[fid] = val;
        /* if ($scope.allefields[fid] === undefined) {
         
         } else {
         //$scope.allefields[fid] = "";
         $scope.allefields[fid] = val;
         }*/


    };
    $scope.selectedExtraServices = [];
    $scope.serviceline = "";
    $scope.addVisitorService = function (service) {
        if (service) {
            //console.log($scope.selectedExtraServices.indexOf(service));
            if ($scope.selectedExtraServices.indexOf(service) <= -1) {
                $scope.selectedExtraServices.push(service);
                $scope.serviceline = "";
            }
        }
    };
    $scope.clearService = function (service) {
        var index = $scope.selectedExtraServices.indexOf(service);
        $scope.selectedExtraServices.splice(index, 1);
        //$scope.tags.push(tag);
        //$scope.tagline="";
    };
    $scope.extrabtn = true;
    $scope.addextraCgroup = function () {

        if (!$scope.addcgrouptxt) {

            $scope.addcgrouptxt = true;
        } else {

            $scope.addcgrouptxt = false;
        }
    };
    $scope.hideextrabtn = function (btn) {
        if (btn === 1) {
            $scope.extrabtn = false;
        } else {
            $scope.extrabtn = true;
        }
    };
	 $scope.dupcvFlag = 0;
    $scope.checkvCategory = function (name) {
        $scope.dupcvFlag = 0;
        angular.forEach($scope.eventcategories, function (d) {

            if (d.name.toLowerCase() === name.toLowerCase()) {
                $scope.dupcvFlag = 1;

            }


        });
        if ($scope.dupcvFlag === 1) {
            return true;
        } else {
            return false;
        }

    };
    $scope.dupsvFlag = 0;
    $scope.checkvService = function (name) {
        $scope.dupsvFlag = 0;
        angular.forEach($scope.eventservices, function (d) {

            if (d.name.toLowerCase() === name.toLowerCase()) {
                $scope.dupsvFlag = 1;

            }


        });
        if ($scope.dupsvFlag === 1) {
            return true;
        } else {
            return false;
        }

    };
	$scope.setLeadStage = function (stage) {
        // console.log(stage);
        angular.forEach($scope.stagelist, function (d) {
            if (d.id === stage) {
                $scope.evisitor.type = d.stage;
                //console.log($scope.evisitor);
            }
        });


    };
    $scope.inds = 0;
    if ($scope.evisitor.id === undefined) {
        $scope.selectedService = [];
    }
    $scope.setIValue = function (v) {
        console.log(v);
        if ($scope.evisitor.id !== undefined) {
        var visitorserviceresp = $http.get(Data.serviceBase() + '/visitorservice/visitorservice',
                            {params: {"key": "visitorId", "value": $scope.evisitor.id}});
        visitorserviceresp.then(function (response) {
        $scope.inds = response.data.length;
        angular.forEach($scope.eventservices, function (d) {
            if (d.id === v.id && v.checked === '0') {
                $scope.selectedService[$scope.inds] = d;
            }
            if (d.id === v.id && v.checked === '1') {
                $scope.selectedService[$scope.inds] = null;
            }
            $scope.inds = $scope.inds + 1;
            });
        });
    } else {
        $scope.inds = 0;
        angular.forEach($scope.eventservices, function (d) {

            if (d.id === v.id && v.checked === '0') {
                $scope.selectedService[$scope.inds] = d;
            }
            if (d.id === v.id && v.checked === '1') {
                $scope.selectedService[$scope.inds] = null;
            }
            $scope.inds = $scope.inds + 1;

            });
    }
    };

    $scope.redirectToM = function (url) {
       window.location.href = Data.webAppBase() + url;
    };
    
    $scope.extraCgroup = "";
    $scope.extraInterest = "";
    $scope.selectedServiceName = [];
    $scope.selectedServiceId = [];
    $scope.selectedCatgeoryId = [];
	$scope.subclicked = false;
    $scope.updatevisitor = function (visitor, selectedServices, selectedCategories, tagslist, allefields, allefieldsDate, allefieldsDateType) {
		$scope.subclicked = true;
        if (visitor.email || visitor.mobile) {
            if ($scope.cureventId !== '1' && $scope.cureventId !== '2' && $scope.cureventId !== '3') {
                if (visitor.id !== undefined) {
                    ////console.log(selectedServices);
                    ////console.log("Selected categoryId is ");
                    ////console.log(selectedCategories.id);
                    ////console.log(visitor.potential);
                    //-- Important note -- no need to set this
                    //visitor.eventId = sessionStorage.getItem('curEventId');
                    //visitor.organizationId = sessionStorage.getItem('orgId');
                    if ($scope.addcgrouptxt) {
                        visitor.categorylist = $scope.extraCgroup;
                    }
                    if (selectedCategories && !$scope.addcgrouptxt) {

                        //console.log(selectedCategories.id);
                        $scope.selectedCatgeoryId.push(selectedCategories.id);
                        visitor.categories = $scope.selectedCatgeoryId;
                        visitor.categorylist = selectedCategories.name;
                    }
                    if ($scope.eventtempSelection !== 0) {
                        visitor.eventId = $scope.eventtempSelection.id;
                    }

                    //console.log("Lead stage" + visitor.type);
                    visitor.leadinfo = visitor.leadnotes;
                    visitor.orginfo = visitor.orgnotes;
                    visitor.linkedin = visitor.linkedin;
                    visitor.url1 = visitor.url1;
                    visitor.url2 = visitor.url2;
                    visitor.url3 = visitor.url3;
                    visitor.dobFlag = visitor.dobFlag;
                    visitor.sdateFlag = visitor.sdateFlag;
                    if (visitor.dob) {
                    	$scope.dob = new Date(visitor.dob); 
                        $mnthF = ("0" + ($scope.dob.getMonth() + 1)).slice(-2);
                        $dayF = ("0" + $scope.dob.getDate()).slice(-2);
                        $scope.finalDateOfBirth = [$scope.dob.getFullYear(), $mnthF, $dayF].join("-");
                        visitor.dob = $scope.finalDateOfBirth.toString();
                    }
                    if (visitor.sdate) {
                    	$scope.sdate = new Date(visitor.sdate); 
                        $mnthF = ("0" + ($scope.sdate.getMonth() + 1)).slice(-2);
                        $dayF = ("0" + $scope.sdate.getDate()).slice(-2);
                        $scope.finalSpecialDate = [$scope.sdate.getFullYear(), $mnthF, $dayF].join("-");
                        visitor.sdate = $scope.finalSpecialDate.toString(); 
                    }
                    if ($scope.fileCsv) {
                        visitor.filecsv = $scope.fileCsv;
                    }

                    if(allefieldsDate) { 
                       $scope.edayoF = allefieldsDate;
                       for(y in allefieldsDate){ 
                        $scope.idedateF = y;
                        $scope.dateType = JSON.stringify(allefieldsDateType[y]); 
        
                        if($scope.dateType === '"Date"'){ 
                        $scope.edateF = new Date($scope.edayoF[y]); 
                        $mnthF = ("0" + ($scope.edateF.getMonth() + 1)).slice(-2);
                        $dayF = ("0" + $scope.edateF.getDate()).slice(-2);
                        $timeF = "T00:00:00";
                        $scope.finalDateF = [$scope.edateF.getFullYear(), $mnthF, $dayF, $timeF].join("-");
                        $scope.finalDateStrF = $scope.finalDateF.toString(); 
                         console.log("Final Date String" + $scope.finalDateStrF);
                         $scope.idedateStrF = $scope.idedateF.toString(); 
                         var objDate = {}; 
                      objDate[$scope.idedateStrF] = $scope.finalDateStrF; 
                      visitor.customlist = objDate;
                      }
                        if (allefields && objDate && Object.keys(allefieldsDate).length > 0) {  
                            console.log("allefields loop Obj" + JSON.stringify(objDate));
                            console.log("allefields loop Obj 2" + JSON.stringify(allefields));

                        if($scope.dateType === '"Date"'){
                           
                            //console.log("allefields before Date remove" + $scope.dateType);

                            delete allefields[y];

                        }

                            //console.log("allefields loop after delete" + JSON.stringify(allefields));

                            visitor.customlist = Object.assign(objDate,allefields); 

                            //console.log(visitor.customlist); 
                        }
                    
                        else{
                            visitor.customlist = allefields; 
                        }

                        }   
                    }
                   // if (allefields) {
                        //console.log(allefields);
                       //visitor.customlist = allefields;
                    //}

                        
                    if ($scope.imagephotoSrc) {
                        //console.log("Image Changed");
                        visitor.profilephotourl = $scope.imagephotoSrc;
                    }
                    if (selectedServices) {
                        for (var i = 0; i < selectedServices.length; i++) {

                            if (selectedServices[i] !== null && selectedServices[i] !== false && selectedServices[i] !== undefined) {
                                if ($scope.selectedServiceName.indexOf(selectedServices[i]['name']) === -1) {
                                if (selectedServices[i]['checked'] === '0') {
                                $scope.selectedServiceName.push(selectedServices[i]['name']);
                                $scope.selectedServiceId.push(selectedServices[i]['id']);
                                }
                            }
                          }
                        }
                        if ($scope.selectedExtraServices.length > 0) {
                            $scope.selectedServiceName = $scope.selectedServiceName.concat($scope.selectedExtraServices);
                        }
                        //console.log($scope.selectedServiceName.toString());
                        visitor.services = $scope.selectedServiceId;
                        visitor.serviceslist = $scope.selectedServiceName.toString().replace(/,/g, ", ");
                    }
					if(visitor.mobile){
						if(visitor.mobile.indexOf('+') != -1){
                        //console.log("Inside mobile Code");
                        visitor.mobileCode = null;
						}
					}
					

                    visitor.organizationId = sessionStorage.getItem('orgId');
                    visitor.tags = tagslist.join(", ");
                    //console.log('Saving visitor with category as ' + visitor.categorylist);
                    //console.log('Saving visitor with sevices as ' + visitor.serviceslist);
                    //console.log('Finally saving visitor as ' + visitor);
                    Data.put('visitor/visitor', visitor).then(function (result) {
                        if (result.status !== 'error') {
                            visitor.eventName = $scope.eventtempSelection.name;
                            if (visitor.profilephotourl) {
                                visitor.photourl = visitor.profilephotourl;
                            }
							
                            $uibModalInstance.close(visitor);
                            toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
							$scope.subclicked = false;
                        } else {
                            //console.log(result);
                        }
                    });
                } else {
                    //console.log('gonna add new visitor.....');
                    // //console.log(visitor);
                    ////console.log(selectedServices);

                    if (visitor.firstName !== undefined) {
                        //console.log(selectedServices);
                        //visitor.eventId = sessionStorage.getItem('curEventId');
                        visitor.organizationId = sessionStorage.getItem('orgId');
                        visitor.capturedUserId = sessionStorage.getItem('userId');
                        visitor.capturedUserEmailId = sessionStorage.getItem('userEmail');
                        visitor.tags = tagslist.join(", ");
                        if ($scope.addcgrouptxt) {
                            visitor.categorylist = $scope.extraCgroup;
                        }
                        if (selectedCategories && !$scope.addcgrouptxt) {

                            ////console.log(selectedCategories.id);
                            $scope.selectedCatgeoryId.push(selectedCategories.id);
                            visitor.categories = $scope.selectedCatgeoryId;
                            visitor.categorylist = selectedCategories.name;
                        }
                        if (selectedServices) {
                            for (var i = 0; i < selectedServices.length; i++) {

                                if (selectedServices[i] !== null && selectedServices[i] !== false && selectedServices[i] !== undefined) {
                                    $scope.selectedServiceName.push(selectedServices[i]['name']);
                                    $scope.selectedServiceId.push(selectedServices[i]['id']);
                                }
                            }
                            if ($scope.selectedExtraServices.length > 0) {
                                $scope.selectedServiceName = $scope.selectedServiceName.concat($scope.selectedExtraServices);
                            }
                            //console.log($scope.selectedServiceName.toString());
                            visitor.services = $scope.selectedServiceId;
                            visitor.serviceslist = $scope.selectedServiceName.toString().replace(/,/g, ", ");
                        }
                        if ($scope.fileCsv) {
                            visitor.filecsv = $scope.fileCsv;
                        }
                        if ($scope.eventtempSelection !== 0) {
                            visitor.eventId = $scope.eventtempSelection.id;
                        }
                        /* if (selectedServices) {
                         for (var i = 0; i < selectedServices.length; i++) {
                         
                         if (selectedServices[i] !== null && selectedServices[i] !== false && selectedServices[i] !== undefined) {
                         //console.log(selectedServices[i]);
                         $scope.selectedServiceName.push(selectedServices[i]['name']);
                         $scope.selectedServiceId.push(selectedServices[i]['id']);
                         }
                         ////console.log($scope.services[i]['id']);
                         
                         }
                         ////console.log($scope.selectedServiceName.toString());
                         visitor.services = $scope.selectedServiceId;
                         visitor.categories = $scope.selectedCatgeoryId;
                         visitor.serviceslist = $scope.selectedServiceName.toString();
                         }*/
                        //16-Nov-202
                        //if (allefields) {
                            //console.log(allefields);
                            //visitor.customlist = allefields;
                        //}

                        if(allefieldsDate){
                       $scope.edayo = allefieldsDate;
                       for(x in allefieldsDate){
                        console.log($scope.edayo[x]); 
                        $scope.idedate = x;
                        $scope.edate = new Date($scope.edayo[x]);
                        $mnth = ("0" + ($scope.edate.getMonth() + 1)).slice(-2);
                        $day = ("0" + $scope.edate.getDate()).slice(-2);
                        $time = "T00:00:00";
                        $scope.finalDate = [$scope.edate.getFullYear(), $mnth, $day, $time].join("-");
                        $scope.finalDateStr = $scope.finalDate.toString();
                         console.log($scope.edateFinal);
                         $scope.idedateStr = $scope.idedate.toString();
                         var obj = {}; 
                        //for (var i = 0; i < x.length; i++) {
                             obj[$scope.idedateStr] = $scope.finalDateStr; 
                             // alert(obj[$scope.idedateStr]);
                        //} 
                       }
                       //alert(JSON.stringify(obj)); 
                        visitor.customlist = obj; 
                        } 
                       if (allefields && Object.keys(allefieldsDate).length > 0) { 
                            visitor.customlist = Object.assign(obj,allefields); 
                        } else{
                            visitor.customlist = allefields; 
                        }
						/*if(visitor.mobile){
							if(visitor.mobile.indexOf('+') != -1){
							//console.log("Inside mobile Code");
							visitor.mobileCode = null;
							}
						}*/
                        //visitor.categorylist = selectedCategories.name;
                        // //console.log('Saving visitor with category as ' + visitor.categorylist);
                        //  //console.log('Saving visitor with sevices as ' + visitor.serviceslist);
                        //console.log('Finally saving visitor as ' + visitor);
					
                        Data.post('visitor/visitor', visitor).then(function (result) {

                            if (result.status !== 'error') {

                                $uibModalInstance.close(result);
                                //console.log('Setting the Event ');
                                //console.log(result.message);
                                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
								$scope.subclicked = false;
                            } else {
                                if (result.title) {

                                    toaster.pop(result.status, result.title, result.message, 10000, 'trustedHtml');
                                } else {
                                    $uibModalInstance.close(1);
                                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                                    //console.log(result);
                                }
                            }
                        });
                    }
                }
            } else {
                toaster.pop("error", "", "You can't do add/modify on demo event", 10000, 'trustedHtml');
            }
        } else {
            toaster.pop("info", "", "Email ID / Mobile number is mandatory", 10000, 'trustedHtml');
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
    //sessionStorage.setItem('curEventId','');
});
evtApp.controller('delvisitorCtrl', function ($scope, Data, $uibModalInstance, dvisitor, toaster) {
    $scope.dvisitor = dvisitor;
    $scope.title = "Delete Lead";
    $scope.deletevisitor = function (visitor) {

        if (visitor.firstName !== undefined) {

            Data.delete('visitor/visitor/' + visitor.id).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    //console.log(result);
                }
            });
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

// Column edit popup
evtApp.controller('editColumnCtrl', function ($scope, Data, $uibModalInstance, selcols, toaster,$http) {
    //$scope.dvisitor = selcols;
    $scope.selectedCols = [];
    $scope.selectedCols = selcols;
    $scope.allCols = [{'lable': "Organization", "column": "visitorOrganizationName", "sort": "1"}, {'lable': "List", 'column': "eventName", "sort": "1"}, {'lable': "Potential", 'column': "potential", "sort": "0"}, {'lable': "Stage", 'column': "type", "sort": "0"}, {'lable': "Deal Size", 'column': "dealSize", "sort": "1"}, {'lable': "Lead Score", 'column': "lead_score", "sort": "1"}, {'lable': "Customer Group", 'column': "categories", "sort": "1"}, {'lable': "Assigned To", 'column': "gAssignUser", "sort": "1"}, {'lable': "Designation", 'column': "designation", "sort": "1"}, {'lable': "Email", 'column': "email", "sort": "1"}, {'lable': "Mobile", 'column': "mobile", "sort": "1"}, {'lable': "City", 'column': "city", "sort": "0"}, {'lable': "State", 'column': "state", "sort": "0"}, {'lable': "Country", 'column': "country", "sort": "1"}, {'lable': "Captured By", 'column': "ufname", "sort": "1"}, {'lable': "Captured On", 'column': "createdAt", "sort": "0"}, {'lable': "Recent Activity", 'column': "activity", "sort": "0"}, {'lable': "Posted By", 'column': "postedBy", "sort": "0"}, {'lable': "Posted On", 'column': "postedDate", "sort": "0"}, {'lable': "Last Modified", 'column': "modifiedAt", "sort": "0"},{'lable': "Tags", 'column': "tags", "sort": "0"}];
    $scope.title = "Edit Column";
    $scope.filteredCol = [];
    var emptyStat = [];
    angular.forEach($scope.allCols, function (d) {
        if ($scope.selectedCols.indexOf(d.lable) !== -1) {
            d.status = '1';
            $scope.filteredCol[$scope.selectedCols.indexOf(d.lable)] = d;
        }

    });
    angular.forEach($scope.allCols, function (d) {
        if ($scope.selectedCols.indexOf(d.lable) === -1) {
            d.status = '0';
            $scope.filteredCol.push(d);
        }

    });
	
	var resprom11 = $http.get(Data.serviceBase() + '/visitor/getCustom', {params: {"organizationid": $scope.organizationId}});
    resprom11.success(function(res)
	{
		angular.forEach(res,function(response)
		{
			  if ($scope.selectedCols.indexOf(response.name) != -1) 
			  {
				  $scope.filteredCol.push({'lable': response.name, 'column': response.name, "sort": "0","status":"1","st":true});
			  }
			  else
			  {
				   $scope.filteredCol.push({'lable': response.name, 'column': response.name, "sort": "0","status":"0","st":true});
			  }
		});
	});

    $scope.customizeTable = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setCustomizeTable', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    console.log($scope.filteredCol);
    $scope.updateColumn = function () {
        var userId = sessionStorage.getItem('userId');
        var updateColumn = [];
        angular.forEach($scope.filteredCol, function (d) {
            if (d.status === '1') {
                updateColumn.push(d.lable);
            }
        });
        $scope.userObj = {"userId": userId, "custColumn": updateColumn.join()}
        // Update the column to server
        Data.put('user/userCustomColumn', $scope.userObj).then(function (result) {
            if (result.status !== 'error') {
				toaster.pop("success", "", "Successfully updated the column changes", 10000, 'trustedHtml');
                $uibModalInstance.close(updateColumn);
                //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
            } else {
                //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                //console.log(result);
            }
        });

        console.log(updateColumn.toString());


    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

evtApp.controller('leadDupctrl', function ($scope, $routeParams, Data, $window, toaster, $filter, $timeout, $http, chkType, $uibModalInstance, vvisitor, vvisitors) {
    $scope.leads = vvisitors;
    $scope.chkType = chkType;
    //console.log($scope.visitorId);
    $scope.dlead = vvisitor;
   
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
/*evtApp.controller('viewvisitorCtrl', function ($scope, $routeParams, Data, $window, toaster, $filter, $timeout, $http, $uibModalInstance, vvisitor, vvisitors, valueAdd, vComments) {
 $scope.vvisitor = vvisitor;
 $scope.visitorId = $routeParams.id;
 //console.log($scope.visitorId);
 $scope.vvisitors = vvisitors;
 $scope.ugalert = false;
 //$scope.gAssigner={};
 $scope.commentdet = {};
 $scope.organizationId = sessionStorage.getItem('orgId');
 $scope.vCommentsdet = vComments;
 $scope.valueAdd = valueAdd;
 $scope.title = "View Visitor Details";
 $scope.opnURL = function (site) {
 var s = site;
 $window.open("http://" + s, '_blank');
 };
 $scope.makeUrl = function (vvisitor) {
 var orgnaiz = vvisitor.visitorOrganizationName;
 var city = vvisitor.city;
 var state = vvisitor.state;
 var country = vvisitor.country;
 if (city && state) {
 return vvisitor.visitorOrganizationName + "+" + vvisitor.city + "+" + vvisitor.state + "+" + vvisitor.country;
 } else {
 return vvisitor.visitorOrganizationName + "+" + vvisitor.country;
 }
 };
 $scope.dnfChange = function (visitor) {
 
 if (visitor.dontFollow === "0") {
 visitor.dontFollowRes = "";
 }
 
 
 
 };
 $scope.organizationUsers = {};
 $scope.gAssigner = [];
 $scope.gAssignee = "";
 //$scope.organizationId = sessionStorage.getItem('orgId');
 var orgusersresp = $http.get(Data.serviceBase() + 'user/userByOrganization', {params: {"organizationId": $scope.organizationId}});
 orgusersresp.then(function (response) {
 $scope.organizationUsers = response.data;
 angular.forEach(response.data, function (d) {
 if (d.id === $scope.vvisitor.gAssignTo) {
 //console.log("Matching User ID");
 //console.log(d);
 $scope.gAssigner = d;
 }
 if (d.id === $scope.vvisitor.gAssigne) {
 //console.log("Matching User name");
 //console.log(d);
 $scope.gAssignee = d.firstName + " " + d.lastName;
 }
 });
 }, function (response) {
 //console.log('Error happened -- ');
 //console.log(response);
 });
 $scope.postComment = function (comment) {
 var currentUserId = sessionStorage.getItem('userId');
 var currentUsername = sessionStorage.getItem('firstName') + " " + sessionStorage.getItem('lastName');
 comment.visitorId = $scope.vvisitor.id;
 comment.commentUserId = currentUserId;
 comment.commentUsername = currentUsername,
 Data.post('visitor/visitorComments', comment).then(function (result) {
 if (result.status !== 'error') {
 
 //$uibModalInstance.close(1);
 //console.log('Storing comments for visitor ');
 //console.log(result.message);
 var visitorComment = $http.get(Data.serviceBase() + 'visitor/getvisitorComments', {params: {"visitorId": vvisitor.id}});
 visitorComment.then(function (responseC) {
 $scope.vCommentsdet = responseC.data;
 $scope.commentdet = {};
 }, function (response) {
 //console.log('Error happened -- ');
 //console.log(response);
 });
 //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
 } else {
 //console.log(result);
 }
 });
 };
 $scope.Gtouch = function (cvisitor, assigner) {
 ////console.log(assigner.id);
 $scope.gData = {};
 var curId = sessionStorage.getItem('userId');
 $scope.gData.visitorId = $scope.vvisitor.id;
 $scope.gData.gAssignTo = assigner.id;
 $scope.gData.dontFollow = cvisitor.dontFollow;
 $scope.gData.dontFollowRes = cvisitor.dontFollowRes;
 // $scope.nextD = cvisitor.nextFollow;
 // $scope.gData.nextfollow = $filter('date')($scope.nextD, "yyyy-MM-dd HH:mm:ss");
 $scope.gData.nextFollow = cvisitor.nextFollow;
 if (cvisitor.gAssigne) {
 ////console.log("data");
 $scope.gData.gAssigne = cvisitor.gAssignee;
 } else {
 ////console.log("changed");
 $scope.gData.gAssigne = curId;
 }
 //console.log($scope.gData);
 Data.put('visitor/visitorgTouch', $scope.gData).then(function (result) {
 if (result.status !== 'error') {
 $scope.ugalert = true;
 $timeout(function () {
 $scope.ugalert = false;
 }, 4500);
 //$uibModalInstance.close(1);
 ////console.log('Storing comments for visitor ');
 //console.log(result.message);
 //toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
 }
 });
 };
 $scope.cancel = function () {
 $uibModalInstance.dismiss('Close');
 };
 });*/
/*evtApp.controller('allvisitorsCtrl', function ($scope, $rootScope, Data, $uibModal, $window, visitorShareData, $routeParams, $http, DTOptionsBuilder) {
 $scope.emptyvisitor = {};
 $scope.visitors = {};
 $scope.visitor = {};
 $scope.visitorData = [];
 $scope.gridvisitors = {};
 $scope.grid = false;
 $scope.filtererr = false;
 $scope.filterby = false;
 $scope.adfilter = false;
 $scope.tableColor = "#e7eaec";
 $scope.gridColor = "";
 $scope.curDate = new Date();
 $scope.curDate.setDate($scope.curDate.getDate() + 10);
 $scope.loader = true;
 $rootScope.title = "All Leads";
 $rootScope.addButtonhtml = '&nbsp;';
 $scope.filtersadded = {
 "cgroup": "", "service": "", "potential": "", "event": "", "eventId": "", "assignedTo": "", "assignedBy": "", "capturedBy": "", "designation": "", "company": "", "country": "",
 "fromDate": "", "toDate": "", "dontFollow": ""
 
 };
 $rootScope.addButtonhtml = $rootScope.addButtonhtml + '<button class="btn btn-default btn-sm btn-circle" popover="The person who is interested in your product and service." popover-trigger="focus" popover-title="All Leads" popover-placement="bottom"><i class="fa fa-question"></i></button>';
 $scope.organizationId = sessionStorage.getItem('orgId');
 //localStorage.setItem('curEventId', "0");
 //localStorage.setItem('curEventName', "");
 //sessionStorage.setItem("menuact", '3');
 //$rootScope.menuact = '3';   
 // $scope.currentEventId = sessionStorage.getItem('curEventId');
 ////console.log("Current all event Id is " + $scope.currentEventId);
 // //console.log("all visitors organization Id is " + $scope.organizationId);
 $scope.dtOptions = DTOptionsBuilder.newOptions()
 .withDOM('<"html5buttons"B>Tfgtpi')
 .withButtons([
 /*{extend: 'copy', exportOptions: {
 columns: [0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12]
 }},
 {extend: 'csv', text: '<i class="fa fa-download"></i> Download as CSV', title: 'HLZ - All events', exportOptions: {
 columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
 }},
 {extend: 'excel', text: '<i class="fa fa-download"></i> Download as Excel', title: 'HLZ - All events', exportOptions: {
 columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
 }},
 /*  {extend: 'pdf', title: 'HL-Leads', exportOptions: {
 columns: [0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16,17,18]
 }},
 {extend: 'print',
 customize: function (win) {
 $(win.document.body).addClass('white-bg');
 $(win.document.body).css('font-size', '10px');
 $(win.document.body).find('table')
 .addClass('compact')
 .css('font-size', 'inherit')
 .css('width','255px');
 },
 exportOptions: {
 columns: [0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14,15,16,17,18]
 }
 }
 ])
 
 .withDisplayLength(20);
 $scope.advancedSearch = function (query) {
 //console.log(query);
 $scope.filtererr = false;
 if (query.length >= 3) {
 $scope.loader = true;
 $scope.filtererr = false;
 if ($scope.filterby) {
 
 var respromc = $http.get(Data.serviceBase() + '/visitor/allvisitorsFilter', {params: {"orgId": $scope.organizationId, "eventslist": $scope.filtersadded.eventId ? "'" + $scope.filtersadded.eventId + "'" : null, "cgroups": $scope.filtersadded.cgroup ? "'" + $scope.filtersadded.cgroup + "'" : null, "interests": $scope.filtersadded.service, "potential": $scope.filtersadded.potential ? "'" + $scope.filtersadded.potential + "'" : null, "assignto": $scope.filtersadded.assignedTo ? "'" + $scope.filtersadded.assignedTo + "'" : null, "assigne": $scope.filtersadded.assignedBy ? "'" + $scope.filtersadded.assignedBy + "'" : null, "capture": $scope.filtersadded.capturedBy ? "'" + $scope.filtersadded.capturedBy + "'" : null, "designation": $scope.filtersadded.designation,
 "comapny": $scope.filtersadded.company, "country": $scope.filtersadded.country, "fromDate": $scope.filtersadded.fromDate, "toDate": $scope.filtersadded.toDate, "dontFollow": $scope.filtersadded.dontFollow, "searchquery": query}});
 } else {
 var respromc = $http.get(Data.serviceBase() + '/visitor/advancedvisitorsSearch', {params: {"query": query, "orgId": $scope.organizationId}});
 }
 
 respromc.then(function (response) {
 $scope.visitors.length = 0;
 $scope.adfilter = true;
 ////console.log(response.data[0]);
 $scope.visitors = response.data;
 $scope.loader = false;
 }, function (response) {
 //console.log('Error happened -- ');
 //console.log(response);
 });
 } else {
 $scope.filtererr = true;
 $scope.adfilter = false;
 //$scope.refresh();
 }
 };
 $scope.searchvisitors = function (visitors) {
 
 //$scope.refresh();
 $scope.visitors.length = 0;
 $scope.visitors = visitors;
 $scope.loader = false;
 $scope.totPage = $scope.visitors.length;
 $scope.totalPages = Math.round($scope.visitors.length / 21);
 if ($scope.totalPages <= 0) {
 $scope.totalPages = 1;
 }
 };
 $scope.allVisitorDetail = function () {
 var resprom = $http.get(Data.serviceBase() + 'visitor/allvisitordetail', {params: {key: "organizationId", "value": $scope.organizationId}});
 resprom.then(function (response) {
 $scope.visitors = response.data;
 //$scope.visitorData= response.data;
 $scope.totPage = $scope.visitors.length;
 $scope.totalPages = Math.round($scope.visitors.length / 21);
 if ($scope.totalPages <= 0 && $scope.visitors.length <= 21) {
 $scope.totalPages = 1;
 }
 if ($scope.totalPages <= 0 && $scope.visitors.length >= 21) {
 $scope.totalPages = $scope.totalPages + 1;
 }
 $scope.loader = false;
 //$scope.listData();
 ////console.log(data);
 //$scope.gridvisitors = chunk(response.data, 3);
 }, function (response) {
 //console.log('Error occured -- ');
 //console.log(response);
 });
 };
 if ($scope.adfilter) {
 var tempvisitors = angular.copy($scope.visitors);
 $scope.searchvisitors(tempvisitors);
 }
 
 $scope.filtervisitor = function (size) {
 
 var modalInstance1 = $uibModal.open({
 templateUrl: './filterVisitor',
 controller: 'filterVisitorCtrl',
 size: 'lg',
 backdrop: 'static',
 resolve: {
 eventId: function () {
 return 0;
 },
 eventName: function () {
 return "";
 }
 }
 });
 modalInstance1.result.then(function (results) {
 if (results.visitors) {
 $scope.loader = true;
 $scope.adfilter = true;
 $scope.searchvisitors(results.visitors);
 }
 
 if (results.searchFilter) {
 $scope.filterby = true;
 //console.log(results.searchFilter);
 $scope.filtersadded = results.searchFilter;
 }
 
 });
 };
 $scope.formatDate = function (date) {
 var dateOut = new Date(date);
 return dateOut;
 };
 $scope.makeUrl = function (vvisitor) {
 var orgnaiz = vvisitor.visitorOrganizationName;
 var city = vvisitor.city;
 var state = vvisitor.state;
 var country = vvisitor.country;
 if (city && state) {
 return vvisitor.visitorOrganizationName + "+" + vvisitor.city + "+" + vvisitor.state + "+" + vvisitor.country;
 } else {
 return vvisitor.visitorOrganizationName + "+" + vvisitor.country;
 }
 };
 $scope.switchLayout = function (preview) {
 $scope.grid = preview;
 $scope.refresh();
 if (preview === true) {
 $scope.gridColor = "#e7eaec";
 $scope.tableColor = "";
 } else {
 $scope.tableColor = "#e7eaec";
 $scope.gridColor = "";
 }
 
 };
 $scope.curPage = 1;
 $scope.shareCard = function (p, size) {
 var modalInstanceshare = $uibModal.open({
 templateUrl: './sharevisitor',
 controller: 'sharevisitorCtrl',
 size: 'sm',
 resolve: {
 vvisitor: function () {
 return p;
 }
 }
 });
 modalInstanceshare.result.then(function (i) {
 if (i) {
 // $scope.refresh();
 }
 });
 }
 $scope.pageChangeHandler = function (num) {
 ////console.log("Total Page :" + Math.round($scope.visitors.length / 15)); 
 //
 
 //$scope.currentPage=num;
 $scope.curPage = num;
 };
 $scope.delvisitor = function (p, size) {
 
 var modalInstance1 = $uibModal.open({
 templateUrl: './deletevisitor',
 controller: 'delvisitorCtrl',
 size: size,
 resolve: {
 dvisitor: function () {
 return p;
 }
 }
 });
 modalInstance1.result.then(function (i) {
 if (i) {
 $scope.refresh();
 }
 });
 };
 $scope.editvisitor = function (p, selecttab, size) {
 
 //console.log(p);
 var modalInstance = $uibModal.open({
 templateUrl: './modifyVisitor',
 controller: 'editvisitorCtrl',
 size: 'lg',
 backdrop: 'static',
 resolve: {
 evisitor: function () {
 return p;
 },
 tabsel: function () {
 return selecttab;
 },
 currententId: function () {
 return 0;
 }
 }
 });
 modalInstance.result.then(function (i) {
 if (i) {
 
 $scope.refresh();
 }
 });
 };
 /*  var start = 0;
 var ending = start + 6;
 var lastdata = 50;
 var reachLast = false;
 $scope.loadmore = "Loading More data..";
 
 $rootScope.addvisitor = function (p, size) {
 
 //console.log('going to add new visitor');
 var modalInstance = $uibModal.open({
 templateUrl: './modifyVisitor',
 controller: 'editvisitorCtrl',
 size: 'lg',
 backdrop: 'static',
 resolve: {
 evisitor: function () {
 return $scope.visitor;
 },
 tabsel: function () {
 return "NA";
 },
 currententId: function () {
 return 0;
 }
 
 }
 });
 };
 $scope.refresh = function () {
 $scope.loader = true;
 if (!$scope.adfilter) {
 $scope.visitors.length = 0;
 $scope.allVisitorDetail();
 } else if ($scope.adfilter) {
 $scope.loader = true;
 $scope.adfilter = true;
 var tempvisitors = angular.copy($scope.visitors);
 $scope.searchvisitors(tempvisitors);
 }
 /*start = 0;
 ending = start + 6;
 lastdata = 50;
 reachLast = false;
 $scope.visitorData.length=0;
 $scope.listData();
 };
 function chunk(arr, size) {
 var newArr = [];
 for (var i = 0; i < arr.length; i += size) {
 newArr.push(arr.slice(i, i + size));
 }
 return newArr;
 }
 $scope.clearFilter = function () {
 $scope.adfilter = false;
 $window.location.reload();
 };
 $scope.vBcard = {};
 //Showing Business Card
 $scope.showBcard = function (p, size) {
 var visitorBcard = $http.get(Data.serviceBase() + 'visitor/getvisitorBcard', {params: {"visitorId": p.id}});
 visitorBcard.then(function (response) {
 $scope.vBcard = response.data[0];
 //console.log(response.data[0]);
 var modalInstance = $uibModal.open({
 templateUrl: './viewBcard',
 controller: 'viewBcardCtrl',
 size: "lg",
 resolve: {
 vvisitor: function () {
 return p;
 },
 vbcard: function () {
 return $scope.vBcard;
 }
 }
 })
 }, function (response) {
 //console.log('Error occured -- ');
 //console.log(response);
 });
 };
 $scope.valueAdd = {};
 $scope.viewvisitor = function (p, size) {
 //console.log(p.id);
 if (!p.firstName && !p.email) {
 return false;
 }
 if (p.firstName && p.email) {
 if ($scope.adfilter === true) {
 
 visitorShareData.addVisitorData(angular.copy($scope.visitors));
 window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + p.id, "_self");
 } else {
 window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + p.id, "_self");
 }
 } else {
 return false;
 }
 /* var valueAdd = $http.get(Data.serviceBase() + 'visitor/getvisitorValueAdds', {params: {"visitorId": p.id}});
 valueAdd.then(function (response) {
 $scope.valueAdd = response.data[0];
 //console.log(response.data[0]);
 var visitorComment = $http.get(Data.serviceBase() + 'visitor/getvisitorComments', {params: {"visitorId": p.id}});
 visitorComment.then(function (responseC) {
 $scope.vComments = responseC.data;
 var modalInstance = $uibModal.open({
 templateUrl: './viewVisitor',
 controller: 'viewvisitorCtrl',
 size: "lg",
 resolve: {
 vvisitor: function () {
 if (p.nextFollow) {
 p.nextFollow = $scope.formatDate(p.nextFollow);
 }
 return p;
 },
 vvisitors: function () {
 return $scope.visitors;
 },
 valueAdd: function () {
 return $scope.valueAdd;
 },
 vComments: function () {
 return $scope.vComments;
 }
 }
 });
 }, function (response) {
 //console.log('Error occured -- ');
 //console.log(response);
 });
 }, function (response) {
 //console.log('Error occured -- ');
 //console.log(response);
 });
 
 };
 });*/
//View Business Card
evtApp.controller('viewBcardCtrl', function ($scope, Data, fileReader,$uibModalInstance, vvisitor, vbcard) {
    $scope.vvisitor = vvisitor;
    $scope.vbcard = vbcard;
    $scope.upflag = 0;
    $scope.title = "View Business Card";
    $scope.side = "1";
    $scope.bmsg = '';
    $scope.bflag = '0';

    $scope.bfile = {};
    $scope.cardside = "Front";
    $scope.selectCard = function (side) {
        if (side === 1) {
            $scope.side = "1";
            $scope.cardside = "Front";
        } else {
            $scope.side = "2";
            $scope.cardside = "Back";
            angular.element('#bmsg').css('display', 'none');

        }
    };

    $scope.addBackBusinessCard = function (e) {
        if (parseInt(e.files[0]['size']) <= 200000) {
            fileReader.readAsDataUrl(e.files[0], $scope)
                    .then(function (result) {
                        //console.log($scope.filecsv);
                        $scope.bbiz = result;
                        $scope.bizcard = {};
                        $scope.bizcard.visitorId = $scope.vvisitor.id;
                        $scope.bizcard.organizationId = $scope.vvisitor.organizationId;
                        $scope.bizcard.img = $scope.bbiz;
                        Data.put('visitor/updateBackBusinessCard', $scope.bizcard).then(function (result) {
                            if (result.status !== 'error') {
                                $scope.upflag = 0;
                                $scope.bflag = '0';
                                angular.element('#bmsg').css('display', 'none');
                                $scope.bmsg = '';
                                $scope.vbcard.cardFileName2 = $scope.bbiz;

                            } else {

                            }
                        });
                    });
        } else if (parseInt(e.files[0]['size']) > 200000) {
            console.log(e.files[0].size);
            angular.element('#bmsg').css('display', 'block');
            //$scope.setflag();
        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
//Share Visitor Details
evtApp.controller('sharevisitorCtrl', function ($scope, Data, toaster, $uibModalInstance, vvisitor) {

    $scope.shareemail = {"to": "", "cc": ""};
    $scope.title = "Share";
    $scope.sendEmail = function (shvisitor) {
        shvisitor.username = sessionStorage.getItem("firstName") + " " + sessionStorage.getItem('lastName');
        shvisitor.useremail = sessionStorage.getItem("userEmail");
        shvisitor.visitorId = vvisitor.id;
        Data.post('visitor/sendEmail', shvisitor).then(function (result) {
            if (result.status !== 'error') {

                $uibModalInstance.close(1);
                //console.log('Sharing the Leads ');
                //console.log(result.message);
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
            } else {
                //console.log(result);
            }
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

//Multi Assign Visitor Details
evtApp.controller('assignVisitorCtrl', function ($scope, alertUser, $http, Data, toaster, $uibModalInstance, vvisitorlist) {
    $scope.multiassignvisitor = {
    };
    //$scope.saveloader = false;
    $scope.visitorgAssigner = null;
    var orgId = sessionStorage.getItem('orgId');
    $scope.organizationUsers = {};
    $scope.title = "Assign To";

    $scope.orgUsers = function () {
        var orgusersresp = $http.get(Data.serviceBase() + 'user/userByOrganization', {params: {"organizationId": orgId}});
        orgusersresp.then(function (response) {
            $scope.organizationUsers = response.data;

        }, function (response) {
            //console.log('Error happened -- ');
            //console.log(response);
        });
    };
    $scope.savemultiCheck = function (multivisitor) {
        //console.log($scope.multiassignvisitor);
        //console.log(multivisitor);
        //console.log($scope.visitorgAssigner);

        if (multivisitor.dontFollow === "1" && !multivisitor.dontFollowRes && !$scope.saveloader) {
            return true;
        } else if (!$scope.visitorgAssigner && multivisitor.dontFollow !== "1" && !$scope.saveloader) {
            return true;
        }
        if ($scope.saveloader) {
            return true;
        }
    };
    $scope.dnfpopChange = function (visitor) {

        if (visitor.dontFollow === "0") {
            visitor.dontFollowRes = "";
        }


    };
    $scope.multiGtouch = function (cvisitor, assigner) {
        if(assigner != null && assigner != '') {
            if (cvisitor.dontFollow === "1" && (cvisitor.dontFollowRes == null || cvisitor.dontFollowRes == '')) {                
                toaster.pop('error', "", "Please enter Do not follow up reason *", 10000, 'trustedHtml');
                $scope.saveloader = false;
                $scope.dontFollowFlag = '0';
            } else {
                $scope.dontFollowFlag = '1';
            }
        if($scope.dontFollowFlag == '1') {
        $scope.saveloader = true;
        //console.log(assigner.id);
        $scope.gData = {};
        var curId = sessionStorage.getItem('userId');
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": curId, "permission": "openAssignments"}});
        // $scope.users = {};

        resprole.then(function (response) {
            ////console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                $scope.gData.visitorIds = vvisitorlist;
                $scope.gData.organizationId = sessionStorage.getItem('orgId');
                $scope.gData.gAssignTo = assigner ? assigner.id : "";
                $scope.gData.assignBy = curId;
                $scope.gData.dontFollow = cvisitor.dontFollow;
                $scope.gData.dontFollowRes = cvisitor.dontFollowRes;
                $scope.gData.assignedAt = new Date();

                //if (cvisitor.gAssigne) {
                //////console.log("data");
                //  $scope.gData.gAssigne = cvisitor.gAssignee;
                // } else {
                //////console.log("changed");
                $scope.gData.gAssigne = curId;
                if ($scope.gData.dontFollow === '1') {
                    $scope.gData.nextFollow = "";
                }
                // }
                ////console.log($scope.gData);
                Data.put('visitor/multiassignment', $scope.gData).then(function (result) {

                    if (result.status !== 'error') {
                        //$scope.ugalert = true;
                        //   $timeout(function () {
                        //     $scope.ugalert = false;
                        // }, 4500);
                        //$uibModalInstance.close(1);
                        ////console.log('Storing comments for visitor ');
                        $scope.saveloader = false;
                        // $scope.viewvisitor.dontFollowRes = $scope.gData.dontFollowRes;
                        //console.log(result.message);
                        $uibModalInstance.close(1);

                        $scope.cEmail = sessionStorage.getItem('userEmail');
                        $scope.email = {"cEmail":$scope.cEmail};
                        Data.post('visitor/setBulkAssignmentCount', $scope.email).then(function (result) {
                            console.log('Email '+$scope.cEmail);
                        });
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    }
                });
            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to Assign or Change assignments of leads. Only account owner or users with Manager role can modify assignments. Please contact HelloLeads account owner or managers in your company for modifying lead assignments."};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            //console.log('Error happened -- ');
            //console.log(response);
        });
            }
        } else {
            toaster.pop('error', "", "Please select Assigned To *", 10000, 'trustedHtml');
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

evtApp.controller('moveVisitorCtrl', function ($scope, $http, Data, toaster, $uibModalInstance, vvisitorlist) {

    $scope.eventLists = {};
    $scope.selectedEvent = {};
    var eventresp = $http.get(Data.serviceBase() + '/event/orgEventList', {params: {"organizationId": $scope.organizationId}});
    eventresp.then(function (response) {
        //console.log(response.data);
        $scope.eventLists = response.data;

        $scope.loader = false;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });

    $scope.eventmultiChanged = function (event)
    {
        $scope.selectedEvent = event;

    };
    $scope.checkEventStatus = function (sevent) {
        if (sevent.id === undefined) {
            return true;
        }
    };
    $scope.moveUpdate = function (selectedevent) {
        if (selectedevent) {
            $scope.saveloader = true;
            Data.put('visitor/multimove', {'eventName': selectedevent.name, "eventId": selectedevent.id, "visitorIds": vvisitorlist}).then(function (result) {
                if (result.status !== 'error') {
                    //  if ($scope.message.title == "Success") {
                    //     Data.post('visitor/setSMSCount', $scope.email).then(function (result) {
                    //     console.log('Email '+$scope.cEmail);
                    //     });
                    // }
                    //$scope.ugalert = true;
                    //   $timeout(function () {
                    //     $scope.ugalert = false;
                    // }, 4500);
                    //$uibModalInstance.close(1);
                    ////console.log('Storing comments for visitor ');
                    $scope.saveloader = false;
                    // $scope.viewvisitor.dontFollowRes = $scope.gData.dontFollowRes;
                    //console.log(result.message);
                    $uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                }
            });
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

evtApp.controller('deleteMultiVisitorCtrl', function ($scope, $http, Data, toaster, $uibModalInstance, vvisitorlist, visitorCount) {

    $scope.title = "Delete Lead";
    $scope.visitorCount = visitorCount;
    $scope.deletemultivisitor = function () {

        if (vvisitorlist) {
            $scope.saveloader = true;
            Data.put('visitor/multiDelete', {"visitorIds": vvisitorlist}).then(function (result) {
                if (result.status !== 'error') {
                    $scope.saveloader = false;
                    $uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    //console.log(result);
                }
            });
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});

evtApp.controller('updateMultiVisitorCtrl', function ($scope, $interval, $http, Data, toaster, $uibModalInstance, vvisitorlist) {
    $scope.qualifiers = "";
    $scope.selectedValues = "";
    $scope.services = {};
    $scope.title = "Update Qualifier Fields";
    $scope.organizationId = sessionStorage.getItem('orgId');
    this.setInputFocus = function () {
        $scope.$broadcast('UiSelectDemo1');
    };
    $scope.potentiallist = ["High", "Medium", "Low", "Not Relevant"];
	 // Get the custom lead stages -----------------
    var stageresp = $http.get(Data.serviceBase() + '/organization/getCurrentStages', {params: {"organizationId": $scope.organizationId}});
    stageresp.then(function (response) {
        console.log(response.data);

        
		$scope.stagelist = response.data;

        /*if ($scope.evisitor.stageId) {
            angular.forEach($scope.stagelist, function (d) {
                if (d.id === $scope.evisitor.stageId) {
					$scope.evisitor.type = d.stage;
                    $scope.evisitor.stageId = d.id;
                }
            });

        } else {
			$scope.evisitor.type = $scope.stagelist[0]['stage'];
            $scope.evisitor.stageId = $scope.stagelist[0]['id'];
        }*/

    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });

    //-----------------------------------------------------------------------------------------------------------------------------------------------

    //$scope.stagelist = ['Open', 'Contacted', 'Qualified', 'Customer', 'Unqualified', 'Inactive Customer'];
    this.appendToBodyDemo = {
        remainingToggleTime: 0,
        present: true,
        startToggleTimer: function () {
            var scope = this.appendToBodyDemo;
            var promise = $interval(function () {
                if (scope.remainingTime < 1000) {
                    $interval.cancel(promise);
                    scope.present = !scope.present;
                    scope.remainingTime = 0;
                } else {
                    scope.remainingTime -= 1000;
                }
            }, 1000);
            scope.remainingTime = 3000;
        }
    };
    $scope.serviceLists = [];
    $scope.categoryLists = {};
    $scope.qualifierChange = function (qualifier) {
        //$scope.qualifiers = qualifier;
        if (qualifier === 'categories') {

            var categoryresp = $http.get(Data.serviceBase() + '/category/category', {params: {"key": "organizationId", "value": $scope.organizationId}});
            categoryresp.then(function (response) {
                //console.log(response.data);
                $scope.categoryLists = response.data;
            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
        if (qualifier === 'services') {

            var serviceresp = $http.get(Data.serviceBase() + '/service/service', {params: {"key": "organizationId", "value": $scope.organizationId}});
            serviceresp.then(function (response) {
                //console.log(response.data);
                angular.forEach(response.data, function (value, key) {

                    if (value.name !== null && $scope.serviceLists.indexOf(value.name) == -1) {
                        //categoryId.push(value.id);
                        $scope.serviceLists.push(value.name);
                    }


                });
                // $scope.serviceLists = response.data;

            }, function (response) {
                //console.log('Error happened -- ');
                //console.log(response);
            });
        }
    };

    $scope.$watch('services.values', function () {
        //console.log($scope.services.values);
        if ($scope.services.values) {
            $scope.selectedValues = $scope.services.values.join();
        }
    });

    $scope.updatemultivisitor = function () {

        if (vvisitorlist) {
            $scope.saveloader = true;
            Data.put('visitor/multiUpdate/', {'key': $scope.qualifiers, 'value': $scope.selectedValues, 'visitorlist': vvisitorlist}).then(function (result) {
                if (result.status !== 'error') {
                    $scope.saveloader = false;
                    $uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    //console.log(result);
                }
            });
        }
    };


    $scope.updatemultistageandpotentialvisitor = function (qualifiers, selectedValues) {
        if(qualifiers != null && qualifiers != '') {
        //console.log("Selected Values", $scope.selectedValues);
        if (vvisitorlist && qualifiers == 'type') {
            if(selectedValues != null && selectedValues != '') {
            $scope.saveloader = true;
            Data.put('visitor/multiUpdateLeadStage/', {'stageId': $scope.selectedValues.id, 'type': $scope.selectedValues.stage, 'visitorIds': vvisitorlist}).then(function (result) {
                if (result.status !== 'error') {
                    $scope.saveloader = false;
                    $uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    //console.log(result);
                }
            });
            
            } else {
                toaster.pop('error', "", "Please select lead stage", 10000, 'trustedHtml');
            }
            
        } else if (vvisitorlist && qualifiers == 'potential') {
            if(selectedValues != null && selectedValues != '') {
            $scope.saveloader = true;
            Data.put('visitor/multiUpdatePotential/', {'potential': $scope.selectedValues, 'visitorIds': vvisitorlist}).then(function (result) {
                if (result.status !== 'error') {
                    $scope.saveloader = false;
                    $uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    //console.log(result);
                }
            });

            } else {
                toaster.pop('error', "", "Please select potential", 10000, 'trustedHtml');
            }

        } else {
            //toaster.pop('error', "", "Invalid option", 10000, 'trustedHtml'); 
            console.log("Error : Invalid Option");
        }

    } else {
        toaster.pop('error', "", "Please select qualifier", 10000, 'trustedHtml');
    }

    };


    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };


});




//---VV--- Bcard Visitor detail update module ----- VV -----
evtApp.controller('bcardvisitorsCtrl', function ($scope, Data, $uibModal, $http) {

    $scope.visitors = {};
    $scope.visitor = {};
    $scope.organizationId = sessionStorage.getItem('orgId');
    //console.log("all visitors organization Id is " + $scope.organizationId);
    var resprom = $http.get(Data.serviceBase() + '/visitor/bcardvisitordetail', {params: {key: "organizationId", "value": $scope.organizationId}});
    resprom.then(function (response) {
        $scope.visitors = response.data;
    }, function (response) {
        //console.log('Error occured -- ');
        //console.log(response);
    });
    $scope.refresh = function () {
        var resprom = $http.get(Data.serviceBase() + '/visitor/bcardvisitordetail', {params: {key: "organizationId", "value": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.visitors = response.data;
        }, function (response) {
            //console.log('Error occured -- ');
            //console.log(response);
        });
    };
    $scope.editvisitor = function (p, size) {

        var modalInstance = $uibModal.open({
            templateUrl: './modifyBcardVisitor',
            controller: 'editbcardvisitorCtrl',
            size: 'lg',
            resolve: {
                evisitor: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.refresh();
            }
        });
    };
});
evtApp.controller('editbcardvisitorCtrl', function ($scope, Data, $uibModalInstance, evisitor, toaster, $http) {

    var original = angular.copy($scope.evisitor);
    $scope.evisitor = angular.copy(evisitor);
    $scope.updatebcardvisitor = function (visitor) {

        if (visitor.id !== undefined) {

            Data.put('visitor/visitor', visitor).then(function (result) {
                if (result.status !== 'error') {
                    $uibModalInstance.close(1);
                    //console.log('Setting the visitor ');
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                } else {
                    //console.log(result);
                }
            });
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('csvuploadctrl', function ($scope, $rootScope,alertUser, Data, toaster, $http,$uibModal) {
	sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';
    $scope.eventtempSelection = {};
    $scope.eventLists = {};
	 $scope.medata1={};
    //$rootScope.title = "Import Leads";
    $rootScope.addButtonhtml = "";
    $scope.organizationId = sessionStorage.getItem("orgId");
    $scope.getcsvFile = function () {
        $scope.progress = 0;
        fileReader.readAsDataUrl($scope.filecsv, $scope)
                .then(function (result) {
                    //console.log($scope.filecsv);
                    $scope.fileCsv = result;
                });
    };
	$scope.dupFlag = '1';
	
	 $scope.esellist = function (p, size) {

        modalInstanceassignTo = $uibModal.open({
            templateUrl: './datamanages',
            controller: 'editColumnCtrl2',
            size: 'md',
            backdrop: 'static',
        });
        modalInstanceassignTo.result.then(function (results) {
           $scope.medata1=results;
        });

        
     };
	
	$scope.listActivities = {};
    $scope.dataActivity = function () {
        var activityResp = $http.get(Data.serviceBase() + 'organization/dataAcitivites', {params: {"organizationId": $scope.organizationId, "request": "IML"}});


        activityResp.then(function (response) {
            console.log(response.data);
            $scope.listActivities = response.data;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    var eventresp = $http.get(Data.serviceBase() + '/event/orgEventList', {params: {"organizationId": $scope.organizationId}});
    eventresp.then(function (response) {
        //console.log(response.data);
        $scope.eventLists = response.data;
        angular.forEach($scope.eventLists, function (d) {
            if (d.id === $scope.currenteventId || d.id === $scope.eventSelection) {
                $scope.eventtempSelection = d;
                $scope.sendG = $scope.eventtempSelection.sendGreet;
                //$scope.eventChanged();
            }

        });
        $scope.loader = false;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });



    $scope.fileError = true;
    $scope.getFileDetails = function (e) {
        //console.log("Getting Files");
        $scope.filesCsv = [];
        $scope.$apply(function () {

            // STORE THE FILE OBJECT IN AN ARRAY.
            for (var i = 0; i < e.files.length; i++) {
                if ((e.files[i]['name'].indexOf("xls")) !== -1 || (e.files[i]['name'].indexOf("xlsx")) !== -1) {
                    $scope.filesCsv.push(e.files[i]);
                    $scope.fileError = false;
                } else {
                    $scope.fileError = true;
                }
            }

        });
    };
    $scope.btnText = "Upload";
    $scope.btnDisable = false;
    // NOW UPLOAD THE FILES.
    $scope.uploadFiles = function (eventId, eventName,dupFlag) {

        $scope.btnText = 'Uploading';
        $scope.btnDisable = true;
        //FILL FormData WITH FILE DETAILS.

        var data = new FormData();
        for (var i in $scope.filesCsv) {
            ////console.log($scope.filesCsv[i]);
            data.append("filelist", $scope.filesCsv[i]);
        }
        data.append("eventId", eventId);
        data.append("eventName", eventName);
		data.append("dupFlag", dupFlag);
        data.append("organizationId", sessionStorage.getItem('orgId'));
        data.append("userId", sessionStorage.getItem('userId'));
        data.append("userEmail", sessionStorage.getItem('userEmail'));
        /* ADD LISTENERS.
         var objXhr = new XMLHttpRequest();
         objXhr.addEventListener("progress", updateProgress, false);
         objXhr.addEventListener("load", transferComplete, false);
         
         // SEND FILE DETAILS TO THE API.
         objXhr.open("POST", Data.serviceBase() + "/visitor/fileupload/");
         objXhr.send(data);
         // //console.log(data.get('filelist'));*/
        var request = {
            method: 'POST',
            url: Data.serviceBase() + "/visitor/fileuploadExcel/",
            data: data,
            headers: {
                'Content-Type': undefined
            }
        };
        // SEND THE FILES.
        $http(request)
                .success(function (result) {
                    //$uibModalInstance.close(1);
                    $scope.editvisitoruploadform.$setPristine();
                    $scope.btnText = "Upload";
                    if (result.status === 'success') {
                            modalInstanceassignTo = $uibModal.open({
                            templateUrl: './dataSummarys',
                            controller: 'editColumnCtrl4',
                            size: 'md',
                            backdrop: 'static',
                            resolve:{
                            evisitor: function () {
                                return result;
                            }
                           }                    
                        });
                    }
                    else if(result.status === 'error')
                    {
        
                        modalInstanceassignTo = $uibModal.open({
                            templateUrl: './dataSummary',
                            controller: 'editColumnCtrl3',
                            size: 'md',
                            backdrop: 'static',
                            resolve:{
                            evisitor: function () {
                                return result;
                            }
                           }                    
                        });
                    }     else {
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    }
                    localStorage.setItem('curEventId', $scope.eventtempSelection.id);
                    localStorage.setItem('curEventName', $scope.eventtempSelection.name);
                    localStorage.setItem('curEventsend', $scope.eventtempSelection.sendGreet);
                    //window.location.href = Data.webAppBase() + 'account/manageVisitor';
                })
                .error(function () {
					$scope.btnText = "Upload";
                    //$uibModalInstance.close(1);
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                });

    };
    // UPDATE PROGRESS BAR.
    /*function uploadProgress(evt) {
     scope.$apply(function(){
     if (evt.lengthComputable) {
     scope.progress = Math.round(evt.loaded * 100 / evt.total)
     } else {
     scope.progress = 'unable to compute'
     }
     });
     }*/
    function updateProgress(e) {
        if (e.lengthComputable) {
            document.getElementById('pro').setAttribute('value', e.loaded);
            document.getElementById('pro').setAttribute('max', e.total);
        }
    }

    // CONFIRMATION.
    function transferComplete(e) {
        alert("Files uploaded successfully.");
    }



});

evtApp.controller('editColumnCtrl2', function ($scope, Data, $uibModalInstance,$http) 
{

    var eventresp = $http.get(Data.serviceBase() + '/event/eventDetails', {params: {"orgId": $scope.organizationId}});
    eventresp.then(function (response) {
        $scope.eventlists = response.data.events;
    });
    $scope.lists={};
    $scope.updatels=function()
    {
         eventresp.then(function (response) {
         angular.forEach(response.data.events,function(d)
         {
             if(d.id==$scope.lists.name)
             {
                  $scope.lists.id=d.name;
             }
          });
          $scope.result={'listname':$scope.lists.name,'listid':$scope.lists.id,'orgid':$scope.organizationId};
          $uibModalInstance.close($scope.result);
    });
    };
    $scope.cancel = function () 
    {
        $uibModalInstance.close($scope.result);
    };
});

evtApp.controller('editColumnCtrl3', function ($scope, Data, $uibModalInstance,$http,evisitor) 
{
    $scope.edata=evisitor;
    $scope.cancel = function () 
    {
        $uibModalInstance.close();
    };
});

evtApp.controller('editColumnCtrl4', function ($scope, Data, $uibModalInstance,$http,$window,evisitor) 
{
    $scope.edata=evisitor;
    $scope.cancel = function () 
    {
        $uibModalInstance.close();
        $window.location.reload();
    };
});

evtApp.controller('csvdownloadctrl', function ($scope,alertUser, $rootScope, Data, toaster, $http) {
	sessionStorage.setItem("menuact", '4');
    $rootScope.menuact = '4';
    $scope.eventtempSelection = {};
    $scope.eventLists = {};
    $scope.eventId = 0;
	$scope.formurl = "https://beta.helloleads.io/index.php/app/account/filterCSVVisitor";

    $scope.csvSubmit = function () {
        $scope.formurl = "https://beta.helloleads.io/index.php/app/account/filterCSVVisitor";
    };

    $scope.exportLeadCSV = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setExportLeadCSV', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };


    $scope.excelSubmit = function () {
        $scope.formurl = "https://beta.helloleads.io/index.php/app/account/filterExcelVisitor";
    };

    $scope.exportLeadExcel = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/setExportLeadExcel', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };


    
    //$rootScope.title = "Export Leads";
    $rootScope.addButtonhtml = "";
    $scope.organizationId = sessionStorage.getItem("orgId");
    $scope.userId = sessionStorage.getItem("userId");
	$scope.listActivities = {};
    $scope.dataExportActivity = function () {
        var activityResp = $http.get(Data.serviceBase() + 'organization/dataAcitivites', {params: {"organizationId": $scope.organizationId, "request": "EXL"}});


        activityResp.then(function (response) {
            console.log(response.data);
            $scope.listActivities = response.data;

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
	 var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "exportLeads"}});

    // $scope.users = {};
    resprole.then(function (response) {
        console.log(response);
        $scope.loader = false;
        if (response.data.status === 'success') {
    var eventresp = $http.get(Data.serviceBase() + '/event/orgEventList', {params: {"organizationId": $scope.organizationId}});
    eventresp.then(function (response) {
        //console.log(response.data);
        $scope.eventLists = response.data;
        angular.forEach($scope.eventLists, function (d) {
            if (d.id === $scope.currenteventId || d.id === $scope.eventSelection) {
                $scope.eventtempSelection = d;
                $scope.sendG = $scope.eventtempSelection.sendGreet;
                //$scope.eventChanged();
            }

        });
        $scope.loader = false;
    }, function (response) {
        //console.log('Error happened -- ');
        //console.log(response);
    });
	 } else {
            // $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to export leads from HelloLeads. Only account owner can export leads. Please contact HelloLeads account owner in your company for exporting leads."};
            //alertUser.alertpopmodel($scope.message);
            //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
        }
    }, function (response) {

        console.log('Error happened -- ');
        console.log(response);
    });

});
evtApp.controller('datamanageCtrl', function ($scope,alertUser, $rootScope,trialPopUp, Data, toaster, $http) {
    $rootScope.title = "Import Leads";
    $rootScope.addButtonhtml = '';
	$scope.organizationId = sessionStorage.getItem("orgId");
    $scope.userId = sessionStorage.getItem("userId");
    $scope.roleId = sessionStorage.getItem('roleId');
        $scope.rolemsg = "";
        if ($scope.roleId === '2') {
            $scope.rolemsg = "Manager";
        } else if ($scope.roleId === '3') {
            $scope.rolemsg = "L1 User";
        } else {
            $scope.rolemsg = "L2 User";
        }
    // Default Page tab is User Profile
    $scope.dtab = 1;
    angular.element(document.getElementById("importTab")).addClass("active");
    angular.element(document.getElementById("exportTab")).removeClass("active");
	$scope.preCheck = function () {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    }

    $scope.switchdTab = function (tabId) {
        console.log(tabId);
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
        if (tabId === 1) {

            $scope.dtab = 1;
            angular.element(document.getElementById("importTab")).addClass("active");
            angular.element(document.getElementById("exportTab")).removeClass("active");
            Data.post('visitor/setTabOne', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "exportLeads"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {
                    $scope.dtab = 2;
                    angular.element(document.getElementById("importTab")).removeClass("active");
                    angular.element(document.getElementById("exportTab")).addClass("active");
                    Data.post('visitor/setTabTwo', $scope.email).then(function (result) {
                        console.log('Email '+$scope.cEmail);
                    });
                } else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to export leads from HelloLeads. Only account owner can export leads. Please contact HelloLeads account owner in your company for exporting leads."};
                    alertUser.alertpopmodel($scope.message);
                    $scope.dtab = 1;
                    angular.element(document.getElementById("importTab")).addClass("active");
                    angular.element(document.getElementById("exportTab")).removeClass("active");
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        }

    };

});
evtApp.controller('datamanageExptCtrl', function ($scope,alertUser, $rootScope,trialPopUp, Data, toaster, $http) {
    $rootScope.title = "Export Leads";
    $rootScope.addButtonhtml = '';
    $scope.organizationId = sessionStorage.getItem("orgId");
    $scope.userId = sessionStorage.getItem("userId");
    $scope.roleId = sessionStorage.getItem('roleId');
        $scope.rolemsg = "";
        if ($scope.roleId === '2') {
            $scope.rolemsg = "Manager";
        } else if ($scope.roleId === '3') {
            $scope.rolemsg = "L1 User";
        } else {
            $scope.rolemsg = "L2 User";
        }
    // Default Page tab is User Profile
    $scope.dtab = 1;
    angular.element(document.getElementById("importTab")).addClass("active");
    angular.element(document.getElementById("exportTab")).removeClass("active");
    $scope.preCheck = function () {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    }

    $scope.switchdTab = function (tabId) {
        console.log(tabId);
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
        if (tabId === 1) {

            $scope.dtab = 1;
            angular.element(document.getElementById("importTab")).addClass("active");
            angular.element(document.getElementById("exportTab")).removeClass("active");
            Data.post('visitor/setTabOne', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "exportLeads"}});

            // $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {
                    $scope.dtab = 2;
                    angular.element(document.getElementById("importTab")).removeClass("active");
                    angular.element(document.getElementById("exportTab")).addClass("active");
                    Data.post('visitor/setTabTwo', $scope.email).then(function (result) {
                        console.log('Email '+$scope.cEmail);
                    });
                } else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to export leads from HelloLeads. Only account owner can export leads. Please contact HelloLeads account owner in your company for exporting leads."};
                    alertUser.alertpopmodel($scope.message);
                    $scope.dtab = 1;
                    angular.element(document.getElementById("importTab")).addClass("active");
                    angular.element(document.getElementById("exportTab")).removeClass("active");
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });

        }

    };

});
evtApp.controller('viewEmailCtrl', function ($scope, checkPremium, $uibModal, alertUser, $uibModalInstance, $rootScope, Data, vemail, toaster, $http) {
    $scope.vemail = vemail;
    angular.element(document.getElementById('usublink'))
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('viewEmailRevCtrl', function ($scope, checkPremium, $uibModal, alertUser, $uibModalInstance, $rootScope, Data, vemail, toaster, $http) {
    $scope.vemail = vemail;
    angular.element(document.getElementById('usublink'))
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
    $scope.mailreply = function (mailDet) {
        $uibModalInstance.close(mailDet);
    };
});
evtApp.controller("uploadbizcardCtrl", function ($scope, $uibModal, alertUser, $uibModalInstance, $rootScope, Data, evisitor, fileReader, toaster, $http) {
    $scope.visitorDet = evisitor;
    $scope.upflag = 0;

    $scope.addFBusinessCard = function (e) {
        console.log(e.files[0].size < 200000);
        if (parseInt(e.files[0]['size']) < 200000) {
            fileReader.readAsDataUrl(e.files[0], $scope)
                    .then(function (result) {
                        //console.log($scope.filecsv);
                        $scope.fbiz = result;
                        //$scope.fmsg = '';
                        angular.element('#fmsg').css('display', 'none');
                    });


        } else {
            // console.log(e.files);
            angular.element('#fmsg').css('display', 'block');
        }
    };

    $scope.addBBusinessCard = function (e) {
        if (parseInt(e.files[0]['size']) < 200000) {
            fileReader.readAsDataUrl(e.files[0], $scope)
                    .then(function (result) {
                        //console.log($scope.filecsv);
                        $scope.bbiz = result;
                        // $scope.bmsg = '';
                        angular.element('#bmsg').css('display', 'none');
                    });

        } else {
            angular.element('#bmsg').css('display', 'block');
        }
        //console.log(e.files[0].size);
    };

    $scope.uploadImages = function (img1, img2) {
        $scope.upflag = 1;
        $scope.bizcard = {};
        $scope.bizcard.visitorId = $scope.visitorDet.id;
        $scope.bizcard.organizationId = $scope.visitorDet.organizationId;
        $scope.bizcard.img1 = img1;
        $scope.bizcard.img2 = img2;
        Data.put('visitor/updateBusinessCard', $scope.bizcard).then(function (result) {
            if (result.status !== 'error') {
                $scope.upflag = 0;
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                $uibModalInstance.close(result.cardId);
            } else {
                console.log(result);
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});
evtApp.controller('emailCtrlSep', function ($scope, checkPremium, $uibModal, alertUser, $uibModalInstance, $rootScope, Data, vvisitorlist,subject, message, orgCount, mailCount, visitorEmailId, toaster, $http) {

    $scope.visiorlist = vvisitorlist;
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.userEmail = sessionStorage.getItem('userEmail');
    $scope.username = sessionStorage.getItem('firstName');
    $scope.plan = sessionStorage.getItem('accountType');
    $scope.emailFilter = {};
    $scope.mailCount = mailCount;
    $scope.btntxt = 'Send';
    $scope.emailFilter.fromName = $scope.username;
    $scope.emailFilter.fromEmail = $scope.userEmail;
    $scope.emailFilter.replyTo = $scope.userEmail;
    if (vvisitorlist.length > 0) {
    	$scope.emailFilter.visitorEmailId = visitorEmailId;
    } else {
    	$scope.emailFilter.visitorEmailId = "";
    }
    if (vvisitorlist.indexOf(',') > 0) {
        $scope.rcount = vvisitorlist.split(",").length;
    } else {
        if (vvisitorlist.length > 0)
            $scope.rcount = 1;
        else
            $scope.rcount = 0;
    }
	$scope.emailFilter.chkbcc = '1';
    $scope.orgCount = orgCount;
    $scope.emailTemplateDet = {};
    /*
	 $scope.tinymceOptions = {
        selector: "textarea",
        entity_encoding:"UTF-8",
  width: 755,
  height: 500,
  resize: true,
  autosave_ask_before_unload: false,
  paste_data_images: true,
  plugins: [
    " code paste anchor autolink codesample colorpicker fullscreen help image imagetools",
    " lists link media noneditable preview lineheight advlist",
    " searchreplace table textcolor wordcount"
  ], /* removed:  charmap insertdatetime print 
  external_plugins: {
    moxiemanager: "https://www.tiny.cloud/pro-demo/moxiemanager/plugin.min.js"
    
  },
  
  templates: [],
  toolbar:
    "a11ycheck undo redo | bold italic fontselect fontsizeselect| forecolor backcolor | lineheightselect indent outdent | alignleft aligncenter alignright alignjustify | bullist numlist fullpage | link image mybutton mybutton1 signbutton ",
  
  content_css: [
    "https://fonts.googleapis.com/css?family=Lato|Open+Sans|PT+Serif",
     "https://beta.helloleads.io/css/tinymcecontent.css"
  ],
  spellchecker_dialog: true,*/
  /*setup: function (editor) {
            editor.addButton('mybutton', {
                text: '#VisitorName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorName#');
                }
            });
            editor.addButton('mybutton1', {
                text: '#VisitorCompanyName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorCompanyName#');
                }
            });
            
            /* editor.addButton('mybutton2', {
             text: '#EventName',
             icon: false,
             onclick: function () {
             editor.insertContent('#EventName#');
             }
             });
        }
  
};*/
$scope.tinymceOptions = {
        selector: 'textarea',
        statusbar: false,
	    entity_encoding: "UTF-8",
        height: 375,
        theme: 'modern',
        menubar: true,		
        autosave_ask_before_unload: false,
		paste_data_images: true,
        forced_root_block_attrs: {
            "class": "myclass",
            "data-something": "my data",
            "style": "margin: 5px 0;"
        },
		images_upload_handler: function (blobInfo, success, failure) {
            success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64());
        },
        plugins: [
            'advlist autolink lists link image noneditable preview hr pagebreak table textcolor imagetools ',
            'searchreplace wordcount visualblocks visualchars code paste lineheight emoticons uploadimage ',
            'insertdatetime media nonbreaking save table colorpicker fullscreen contextmenu directionality'
        ],
        lineheight_formats: "LineHeight 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 36pt",
        toolbar1: 'undo redo | styleselect | fontselect fontsizeselect | bold italic',
        toolbar2: 'forecolor backcolor | lineheightselect | bullist numlist | link uploadimage emoticons | alignleft aligncenter alignright alignjustify indent outdent',
        toolbar3: 'mybutton,mybutton1 signbutton ',
       content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            "http://localhost/eventHello/css/tinymcecontent.css"],
        content_style: ".tinymce-content p {  padding: 0; margin: 1px 0; }",
        setup: function (editor) {
            editor.addButton('mybutton', {
                text: '#VisitorName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorName#');
                }
            });
            editor.addButton('mybutton1', {
                text: '#VisitorCompanyName',
                icon: false,
                onclick: function () {
                    editor.insertContent('#VisitorCompanyName#');
                }
            });
           /*editor.addButton('signbutton', {
                text: 'Signature',
                title: 'Signature',
                type: 'menubutton',
                icon: false,
                menu: [
                    {
                        text: '#firstname',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#firstname#');
                        }
                    },
                    {
                        text: '#lastname',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#lastname#');
                        }

                    },
                    {
                        text: '#designation',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#designation#');
                        }
                    },
                    {
                        text: '#company',
                        icon: false,
                        onclick: function () {
                            var orgname = sessionStorage.getItem('cOrgName');
                            editor.insertContent(orgname);
                        }
                    },
                    {
                        text: '#email',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#email#');
                        }
                    },
                    {
                        text: '#mobile',
                        icon: false,
                        onclick: function () {
                            editor.insertContent('#mobile#');
                        }
                    },
                ]
            });*/
        }
    };
    $scope.getAllOrgTemplate = function () {
        var resprom = $http.get(Data.serviceBase() + 'organization/emailTemplate', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.emailTemplates = response.data;
            $scope.emailTemplateDet = "";
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
	$scope.setBcc = function(emailFilter){
        if($scope.emailFilter.chkbcc === '0'){
            console.log("Inside");
            $scope.emailFilter.bcc = $scope.userEmail;
        }else{
            $scope.emailFilter.bcc = "";
        }
    };

    $scope.emailContent = {"subject": "", "message": ""};
    $scope.emailFilter.organizationId = $scope.organizationId;
    $scope.emailFilter.userId = $scope.userId;
    $scope.emailFilter.visitorlist = vvisitorlist;
	if (subject !== '') {
        $scope.emailFilter.subject = subject;
    }
    if (message !== '') {
        $scope.emailFilter.body = message;
    }
    $scope.changeEmail = function () {
        //console.log($scope.emailTemplateDet);
       $scope.userDet = {};
       var resprom = $http.get(Data.serviceBase() + 'user/user', {params: {"key":"id","value": $scope.userId}});
        resprom.then(function (response) {
            //console.log(response.data);
            $scope.userDet = response.data[0];
            //$scope.emailTemplateDet = "";
            //console.log($scope.emailTemplateDet);
            var etemp = JSON.parse($scope.emailTemplateDet);
            $scope.emailFilter.subject = etemp.subject;
            //console.log(etemp.body);
            var message = etemp.body;
            var regf = /#firstname#/gi;
            var regl = /#lastname#/gi;
            var regdes = /#designation#/gi;
            var regem = /#email#/gi;
            var regmob = /#mobile#/gi;
            message = message.replace(regf,$scope.userDet['firstName']);
            message = message.replace(regl,$scope.userDet['lastName']);
            message = message.replace(regdes,$scope.userDet['designation']);
            message = message.replace(regem,$scope.userDet['email']);
            message = message.replace(regmob,$scope.userDet['phoneCode']+$scope.userDet['phone']);
            //console.log(message);
            $scope.emailFilter.body = message;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    };

    $scope.getAttachFileDetails = function (e) {
        $scope.fileError = false;
        $scope.errorMsg = "";
        $scope.files = [];
     //console.log("Getting Files");
        //$scope.files = [];
        var fsize = 0;
        var fileExtension = ['wav', 'mp3', 'amr', 'ogg', 'jpeg', 'jpg', 'txt', 'pdf', 'doc', 'msg', 'xls', 'csv', 'xlsx', 'docx', 'ppt', 'pptx', 'eml', 'tif', 'gif', 'png', 'html', 'rtf', 'mbox', 'odt', 'bmp', 'tiff', 'pps', 'xlr', 'ods', 'wps', 'vcf'];
        $scope.$apply(function () {

            // STORE THE FILE OBJECT IN AN ARRAY.
            for (var i = 0; i < e.files.length; i++) {
                var ftype = e.files[i]['name'].substr(e.files[i]['name'].lastIndexOf('.')+1);
                console.log(e.files[i]['size']);
                if ((fileExtension.indexOf(ftype.toLowerCase())) !== -1) {
                    $scope.files.push(e.files[i]);
                    fsize = fsize + parseInt(e.files[i]['size']);
                    $scope.fileError = false;
                } else {

                    $scope.errorFile = e.files[i]['name'];
                    $scope.fileError = true;

                }
            }
            if ($scope.fileError) {
                $scope.files = [];
                document.getElementById("attachFile").value = "";
        angular.element("input[type='file']").val(null);
                $scope.errorMsg = "Unsupported file format&nbsp;<i class='fa fa-info-circle' data-toggle='tooltip' title='Please check the supported formats  " + fileExtension.join() + "'></i>";
            }
            console.log((fsize / 1000) / 1000);
            if ((fsize / 1000) / 1000 > 5) {
                $scope.files = [];
                document.getElementById("attachFile").value = "";
        angular.element("input[type='file']").val(null);
                $scope.errorMsg = "The attachement limit should not exceed 5MB";
            }
            console.log($scope.errorFile);
            console.log($scope.fileError);
        });
    };

     $scope.removeAttachment = function () {
      $scope.files = null;
      document.getElementById("attachFile").value = "";
      angular.element("input[type='file']").val(null);
    };

    $scope.sendingEmail = function () {
    	$scope.organizationId = sessionStorage.getItem('orgId');
        $scope.userId = sessionStorage.getItem('userId');
    	if ($scope.files) {
        var data = new FormData();
        for (var i in $scope.files) {
          console.log('File Name '+$scope.files[0]['name']);
               $scope.fileName = $scope.files[0]['name'];
               data.append("filelist[]", $scope.files[i]);
            }

            data.append("orgId", $scope.organizationId);
            data.append("userId", $scope.userId);

            var request = {
                method: 'POST',
                url: Data.serviceBase() + "/visitor/emailFileUpload/",
                data: data,
                headers: {
                    'Content-Type': undefined
                }
            };
             $http(request)
                    .success(function (result) {
                        console.log("File Sent");
                        $scope.files = [];
                        angular.element("input[type='file']").val(null);
                        document.getElementById("attachFile").value = "";
                        });
          }
        $scope.btntxt = 'Sending';
        if ($scope.fileName !== undefined) {
          $scope.emailFilter.filename = $scope.fileName;
        } else {
          $scope.emailFilter.filename = null;
        }
        Data.post('visitor/sepVisitorEmail', $scope.emailFilter).then(function (result) {
            if (result.status !== 'error' && result.status !== 'success1') {
                $scope.visitorName = "";
                $scope.toId = "";
                $scope.visitorCName = "";
                $scope.listName = "";
                $scope.message = {"title": "Success", "message": $scope.rcount + " email(s) with subject <b>''" + $scope.emailFilter.subject + " ''</b> has been sent successfully"};
                alertUser.alertpopmodel($scope.message);
                $scope.btntxt = 'Send';
                $uibModalInstance.close(1);
            } else {
                console.log(result);
            }
        });
    };
    $scope.redirectToTemp = function(){
        window.open(Data.webAppBase()+"account/emailTemplate",'_blank');
    };
    $scope.configSMTP = function (mclick) {
        var smtpConfigresp = $http.get(Data.serviceBase() + '/organization/orgSMTP', {params: {"organizationId": $scope.organizationId,"userId": $scope.userId}});
        smtpConfigresp.then(function (response) {
            console.log(response.data);
            $scope.csmtp = response.data[0];
            //$scope.csmtp = undefined;
            if (mclick === '1') {
                var smtpmodalInstance = $uibModal.open({
                    templateUrl: './mailConfigPopUp',
                    controller: 'smtpConfigCtrl',
                    size: "md",
                    backdrop: 'static',
                    resolve: {
                        mailConfig: function () {
                            return $scope.csmtp;
                        }
                    }
                });
                smtpmodalInstance.result.then(function (i) {
                    if (i === 1) {

                    }
                });
            }

            if ($scope.csmtp !== undefined) {
                $scope.emailFilter.fromEmail = sessionStorage.getItem('userEmail');
            } else {
                $scope.emailFilter.fromEmail = "hello@helloleads.io";
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });


    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('smsCtrlSep', function ($scope, checkPremium, $uibModal, alertUser, $uibModalInstance, $rootScope, Data, vvisitorlist, message, orgCount, mailCount, toaster, $http) {

    $scope.visiorlist = vvisitorlist;
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.userId = sessionStorage.getItem('userId');
    $scope.userEmail = sessionStorage.getItem('userEmail');
    $scope.username = sessionStorage.getItem('firstName');
    $scope.plan = sessionStorage.getItem('accountType');
    $scope.smsFilter = {};
    $scope.smsFilter.body = '';
    $scope.mailCount = mailCount;
    $scope.btntxt = 'Send';
    //$scope.emailFilter.fromName = $scope.username;
    //$scope.emailFilter.fromEmail = $scope.userEmail;
    //$scope.emailFilter.replyTo = $scope.userEmail;
    if (vvisitorlist.indexOf(',') > 0) {
        $scope.rcount = vvisitorlist.split(",").length;
    } else {
        if (vvisitorlist.length > 0)
            $scope.rcount = 1;
        else
            $scope.rcount = 0;
    }

    $scope.orgCount = orgCount;
    // $scope.emailTemplateDet = {};

    $scope.userDet = {};
    /*$scope.getAllOrgTemplate = function () {
     var resprom = $http.get(Data.serviceBase() + 'organization/emailTemplate', {params: {"organizationId": $scope.organizationId}});
     resprom.then(function (response) {
     $scope.emailTemplates = response.data;
     $scope.emailTemplateDet = "";
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
     };*/
    $scope.addeTag = function (tag) {
        //$scope.items.push($scope.someInput);
        //console.log(tag);
        $rootScope.$broadcast('addcTags', tag);
        //toaster.pop("info", "", "<strong>" + tag + "</strong>" + " placeholder copied to clipboard. Paste the placeholder text wherever required in the email content", 7000, 'trustedHtml');

    };

    $scope.smsContent = {"message": ""};
    $scope.smsFilter.organizationId = $scope.organizationId;
    $scope.smsFilter.userId = $scope.userId;
    $scope.smsFilter.visitorlist = vvisitorlist;
    if (message !== '') {
        $scope.emailFilter.body = message;
    }
    $scope.smsOrgDet = {};
    $scope.getOrgDetSMS = function () {
        var orgResp = $http.get(Data.serviceBase() + '/organization/organization', {params: {"key": "id", "value": $scope.organizationId}});
        orgResp.then(function (response) {
            console.log(response.data[0]);

            $scope.smsOrgDet = response.data[0];



        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.getOrgSmsConfig = function () {
        var resprom = $http.get(Data.serviceBase() + '/organization/getSmsConfig', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            console.log("Edit Event "+response.data[0]);
            $scope.smsFilter.smsProvider = response.data[0].smsProvider;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.getAllSmsTemplates = function () {
        var resprom = $http.get(Data.serviceBase() + 'organization/smsTemplate', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.smsTemps = response.data;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.smsvcheckField = function () {
        if ($scope.smsOrgDet.smsInteg === '1' || $scope.smsOrgDet.id === '33260') {
            $scope.smsFilter.body = "";
            return true;
        } else {
            return false;
        }
    };
    $scope.changeSms = function () {

        $scope.userDet = {};
        var resprom = $http.get(Data.serviceBase() + 'user/user', {params: {"key": "id", "value": $scope.userId}});
        resprom.then(function (response) {
            console.log(response.data);
            $scope.userDet = response.data[0];
            //$scope.emailTemplateDet = "";
            //console.log($scope.emailTemplateDet);
            var etemp = JSON.parse($scope.emailTemplateDet);
            $scope.emailFilter.subject = etemp.subject;
            //console.log(etemp.body);
            var message = etemp.body;
            var regf = /#firstname#/gi;
            var regl = /#lastname#/gi;
            var regdes = /#designation#/gi;
            var regem = /#email#/gi;
            var regmob = /#mobile#/gi;
            message = message.replace(regf, $scope.userDet['firstName']);
            message = message.replace(regl, $scope.userDet['lastName']);
            message = message.replace(regdes, $scope.userDet['designation']);
            message = message.replace(regem, $scope.userDet['email']);
            message = message.replace(regmob, $scope.userDet['phoneCode'] + $scope.userDet['phone']);
            console.log(message);
            $scope.emailFilter.body = message;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });


    };

    $scope.changecontentSMSTemp = function () {
        $scope.smsTemplate = JSON.parse($scope.smsTemplateDet);
        var resprom = $http.get(Data.serviceBase() + 'organization/getSmsTemp', {params: {"id": $scope.smsTemplate.id,"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            $scope.smsFilter.body = response.data[0].smsContent;
            $scope.smsFilter.templateId = response.data[0].templateId;
            $scope.smsFilter.senderId = response.data[0].senderId;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.sendingSms = function () {
        $scope.btntxt = 'Sending';
        Data.post('visitor/sepVisitorMobile', $scope.smsFilter).then(function (result) {
            if (result.status !== 'error' && result.status !== 'success1') {
                $scope.cEmail = sessionStorage.getItem('userEmail');
                $scope.email = {"cEmail":$scope.cEmail};
                
                $scope.visitorName = "";
                $scope.toId = "";
                $scope.visitorCName = "";
                $scope.listName = "";
                $scope.message = {"title": "Success", "message": $scope.rcount + " SMS has been sent successfully"};
                if ($scope.message.title == "Success") {
                    Data.post('visitor/setSMSCount', $scope.email).then(function (result) {
                    console.log('Email '+$scope.cEmail);
                    });
                }
                alertUser.alertpopmodel($scope.message);
                $scope.btntxt = 'Send';
                $uibModalInstance.close(1);
            } else {
                console.log(result);
            }
        });
    };
    /*$scope.redirectToTemp = function () {
     window.open(Data.webAppBase() + "account/emailTemplate", '_blank');
     };*/

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});