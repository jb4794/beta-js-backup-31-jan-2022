/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
evtApp.controller('quoteinvoiceCtrl', function ($scope, $rootScope, alertUser, checkPremium, $location, fileReader, trialPopUp, $sce, Data, $uibModal, $http, toaster) {

    $scope.organizationId = sessionStorage.getItem('orgId');
    $rootScope.addButtonhtml = '';
    $rootScope.title = "Quotes and Invoices";
    sessionStorage.setItem('menuact', '11');
    $rootScope.menuact = '11';
    $scope.invoiceloader = false;
    $scope.quoteloader = false;
    $scope.settloader = false;
    $scope.productloader = false;
    $scope.selectAllFlag = true;
    $scope.currencyCode = '';
    $scope.qiCurrency = '';
    $scope.quoteSort = '';
	$scope.invoiceSort = '';
    $scope.emptyQuote = {};
    $scope.emptyInvoice = {};
    $scope.quotes = {};
    $scope.qiConfig = {};
    $scope.invoices = {};
    $scope.products = {};
    $scope.product = {};
    $scope.clogo = '';
    $scope.signpic = '';
    $scope.userId = sessionStorage.getItem('userId');
    $scope.roleId = sessionStorage.getItem('roleId');
    $scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }
    // Default Page tab is User Profile
    $scope.itab = 1;
    angular.element(document.getElementById("quoteTab")).addClass("active");
    angular.element(document.getElementById("invoiceTab")).removeClass("active");
    angular.element(document.getElementById("productTab")).removeClass("active");
    angular.element(document.getElementById("settingTab")).removeClass("active");
    $scope.switchiTab = function (tabId) {
        console.log(tabId);
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
        if (tabId === 1) {
            $scope.itab = 1;
            angular.element(document.getElementById("quoteTab")).addClass("active");
            angular.element(document.getElementById("invoiceTab")).removeClass("active");
            angular.element(document.getElementById("productTab")).removeClass("active");
            angular.element(document.getElementById("settingTab")).removeClass("active");
            Data.post('quote/setTabOne', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else if (tabId === 2) {
            $scope.itab = 2;
            angular.element(document.getElementById("quoteTab")).removeClass("active");
            angular.element(document.getElementById("invoiceTab")).addClass("active");
            angular.element(document.getElementById("productTab")).removeClass("active");
            angular.element(document.getElementById("settingTab")).removeClass("active");
            Data.post('quote/setTabTwo', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else if (tabId === 3) {
             var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "productCatalogue"}});

            $scope.users = {};
            resprole.then(function (response) {
                console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {
            $scope.itab = 3;
            angular.element(document.getElementById("quoteTab")).removeClass("active");
            angular.element(document.getElementById("invoiceTab")).removeClass("active");
            angular.element(document.getElementById("productTab")).addClass("active");
            angular.element(document.getElementById("settingTab")).removeClass("active");
             Data.post('quote/setTabThree', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } 
        else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to manage product catalog and quotes/ invoice settings. Users with account owner or manager roles can manage product catalog and quotes/ invoice settings. Please contact HelloLeads account owner or mangers in your company for assistance."};
                    alertUser.alertpopmodel($scope.message);
                    $scope.itab = 1;
                    angular.element(document.getElementById("quoteTab")).addClass("active");
                    angular.element(document.getElementById("invoiceTab")).removeClass("active");
                    angular.element(document.getElementById("productTab")).removeClass("active");
                    angular.element(document.getElementById("settingTab")).removeClass("active");
                    Data.post('quote/setTabThree', $scope.email).then(function (result) {
                        console.log('Email '+$scope.cEmail);
                    });
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });
        }
         else {
            var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoiceSettings"}});

            $scope.users = {};
            resprole.then(function (response) {
               console.log(response);
                $scope.loader = false;
                if (response.data.status === 'success') {
            $scope.itab = 4;
            angular.element(document.getElementById("quoteTab")).removeClass("active");
            angular.element(document.getElementById("invoiceTab")).removeClass("active");
            angular.element(document.getElementById("settingTab")).addClass("active");
            angular.element(document.getElementById("productTab")).removeClass("active");
            Data.post('quote/setTabFour', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } 
        else {
                    $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to manage product catalog and quotes/ invoice settings. Users with account owner or manager roles can manage product catalog and quotes/ invoice settings. Please contact HelloLeads account owner or mangers in your company for assistance."};
                    alertUser.alertpopmodel($scope.message);
                    $scope.itab = 1;
                    angular.element(document.getElementById("quoteTab")).addClass("active");
                    angular.element(document.getElementById("invoiceTab")).removeClass("active");
                    angular.element(document.getElementById("productTab")).removeClass("active");
                    angular.element(document.getElementById("settingTab")).removeClass("active");
                    Data.post('quote/setTabFour', $scope.email).then(function (result) {
                        console.log('Email '+$scope.cEmail);
                    });
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                }
            }, function (response) {

                console.log('Error happened -- ');
                console.log(response);
            });
        }

    };

    var hashVal = $location.url();
    console.log(hashVal.indexOf('/settings'));
    if (hashVal.indexOf('/Quote') === 0) {
        $scope.switchiTab(1);
    } else if (hashVal.indexOf('/Invoice') === 0) {
        $scope.switchiTab(2);
    } else if (hashVal.indexOf('/product') === 0) {
        $scope.switchiTab(3);
    } else if (hashVal.indexOf('/settings') === 0) {

        $scope.switchiTab(4);
    }

    //Get QI Config
    // $scope.getConfig = function () {

    //     var resprom = $http.get(Data.serviceBase() + '/organization/qiConfig', {params: {"organizationId": $scope.organizationId}});
    //     resprom.then(function (response) {
    //         if (response.data.length > 0) {
    //             $scope.qiConfig = response.data[0];

    //             if (response.data[0].companyLogo) {
    //                 $scope.clogo = response.data[0].companyLogo;
    //             }
    //             if (response.data[0].signatureImg) {
    //                 $scope.signpic = response.data[0].signatureImg;
    //             }
    //         }
    //         // if ($scope.qiConfig.qCurrency === undefined) {
    //         //     $scope.qiConfig.qCurrency = sessionStorage.getItem('orgCurrency');
    //         //     $scope.qiConfig.iCurrency = sessionStorage.getItem('orgCurrency');
    //         // }
    //         // if ($scope.qiConfig.companyName === undefined) {
    //         //     $scope.qiConfig.companyName = sessionStorage.getItem('cOrgName');
    //         // }
    //         // if ($scope.qiConfig.qValidity === undefined) {
    //         //     $scope.qiConfig.qValidity = 30;
    //         // }
    //         // if ($scope.qiConfig.qTitle === undefined) {
    //         //     $scope.qiConfig.qTitle = 'Quotation';
    //         // }
    //         // if ($scope.qiConfig.qFooter === undefined) {
    //         //     $scope.qiConfig.qFooter = 'Contact us if you have any questions - ' + '\n\
    //         //         ' + $scope.userEmail + '\n\
    //         //         ' + $scope.userPhone;
    //         // }
    //         // if ($scope.qiConfig.iFooter === undefined) {
    //         //     $scope.qiConfig.iFooter = 'Contact us if you have any questions - ' + '\n\
    //         //         ' + $scope.userEmail + '\n\
    //         //         ' + $scope.userPhone;
    //         // }
    //         // if ($scope.qiConfig.qStarting === undefined) {
    //         //     $scope.qiConfig.qStarting = '410000001';
    //         // }
    //         // if ($scope.qiConfig.iStarting === undefined) {
    //         //     $scope.qiConfig.iStarting = '510000001';
    //         // }
    //         // if ($scope.qiConfig.paymentNotes === undefined) {
    //         //     $scope.qiConfig.paymentNotes = 'Payment is due in 30 days from the date of receipt of invoice. Payment can be made online or by cheque';
    //         // }

    //     }, function (response) {
    //         console.log('Error happened -- ');
    //         console.log(response);
    //     });


    // };
    //End QI Config

    //Get QI Config With Locale
    $scope.getConfigLocale = function () {

        var resprom = $http.get(Data.serviceBase() + '/organization/qiConfiglocale', {params: {"organizationId": $scope.organizationId}});
        resprom.then(function (response) {
            if (response.data.length > 0) {
                $scope.qiConfig = response.data[0];

                if (response.data[0].companyLogo) {
                    $scope.clogo = response.data[0].companyLogo;
                }
                if (response.data[0].signatureImg) {
                    $scope.signpic = response.data[0].signatureImg;
                }
            }
            if ($scope.qiConfig.qCurrency === undefined) {
                $scope.qiConfig.qCurrency = sessionStorage.getItem('orgCurrency');
                $scope.qiConfig.iCurrency = sessionStorage.getItem('orgCurrency');
            }
            if ($scope.qiConfig.companyName === undefined) {
                $scope.qiConfig.companyName = sessionStorage.getItem('cOrgName');
            }
            if ($scope.qiConfig.qValidity === undefined) {
                $scope.qiConfig.qValidity = 30;
            }
            if ($scope.qiConfig.qTitle === undefined) {
                $scope.qiConfig.qTitle = 'Quotation';
            }
            if ($scope.qiConfig.qFooter === undefined) {
                $scope.qiConfig.qFooter = 'Contact us if you have any questions - ' + '\n\
                    ' + $scope.userEmail + '\n\
                    ' + $scope.userPhone;
            }
            if ($scope.qiConfig.iFooter === undefined) {
                $scope.qiConfig.iFooter = 'Contact us if you have any questions - ' + '\n\
                    ' + $scope.userEmail + '\n\
                    ' + $scope.userPhone;
            }
            if ($scope.qiConfig.qStarting === undefined) {
                $scope.qiConfig.qStarting = '410000001';
            }
            if ($scope.qiConfig.iStarting === undefined) {
                $scope.qiConfig.iStarting = '510000001';
            }
            if ($scope.qiConfig.paymentNotes === undefined) {
                $scope.qiConfig.paymentNotes = 'Payment is due in 30 days from the date of receipt of invoice. Payment can be made online or by cheque';
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });


    };
    //End QI Config with Locale

    $scope.getQuotes = function (query, sort) {
        $scope.quoteloader = true;
        
        if (query === '' && sort === '') {
            var resprom = $http.get(Data.serviceBase() + '/quote/orgQuotes', {params: {"organizationId": $scope.organizationId,"userId": $scope.userId}});
            resprom.then(function (response) {
                if (response.data) {
                    $scope.quotes = response.data;
					//var filtered = $filter('orderBy')(response, '-quoteDate');
					//$scope.quotes = filtered;
                    $scope.quoteloader = false;
                }

            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });

            //Locale
            var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});
	        resprom.then(function (response) {
	            if (response.data) {
	                $scope.orgDet = response.data[0];
	            }
	        });
        } else if (query !== '' || sort !== '') {
            var reqflag = 0;
            if (query && query.length > 2) {
                reqflag = 1;
            } else if (sort && sort.length > 2) {
                reqflag = 1;
            }
            if (reqflag) {
                var resprom = $http.get(Data.serviceBase() + '/quote/orgQueryQuotes', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "query": query, "sort": sort}});
                resprom.then(function (response) {
                    if (response.data) {
                        $scope.quotes = response.data;

                        $scope.quoteloader = false;
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });

            }
        }
        
    };
    $scope.getProductCatalogue = function () {
        
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
           $scope.productloader = false;
            if (response.data.status === 'success') {
        var resprom = $http.get(Data.serviceBase() + '/quote/productCatalogueLocale', {params: {key: "organizationId", "value": $scope.organizationId}});
        resprom.then(function (response) {
            if (response.data) {
                $scope.productloader = false;
                $scope.products = response.data;
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
        } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.selectedProduct = [];
    $scope.toggleSelection = function (sproduct) {
        var falseflag = 0;
        angular.forEach($scope.products, function (product) {
            if (product.select === true) {
                falseflag = falseflag + 1;
            }
        });
        if (falseflag === $scope.products.length) {
            $scope.checkstate = true;
        } else {
            $scope.checkstate = false;
        }
        if (sproduct.select === true) {
            if ($scope.selectedProduct.indexOf(sproduct.id) <= -1) {
                $scope.selectedProduct.push(sproduct.id);
            }
        }
        if (sproduct.select === false) {
            if ($scope.selectedProduct.indexOf(sproduct.id) > -1) {
                $scope.selectedProduct.splice($scope.selectedProduct.indexOf(sproduct.id), 1);
            }
        }
    };

    $scope.checkAll = function () {
        if ($scope.checkstate)
        {
            $scope.checkstate = false;
            $scope.selectedProduct = [];
            $scope.selectedProduct.length = 0;
        } else {
            $scope.checkstate = true;
        }
        $scope.selectedProduct = [];
        $scope.selectedProduct.length = 0;

        angular.forEach($scope.products, function (product) {
            product.select = $scope.checkstate;
            if ($scope.checkstate) {
                $scope.selectedProduct.push(product.id);
            }
        });
    };

    $scope.clearAllProductSelection = function () {
        $scope.selectAllFlag = true;
        $scope.checkstate = false;
        angular.forEach($scope.products, function (product) {
            product.select = $scope.checkstate;

        });
        $scope.selectedProduct = [];
        $scope.selectedProduct.length = 0;
    };

    $scope.getInvoices = function (query, sort) {
       
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
             $scope.invoiceloader = true;
            if (response.data.status === 'success') {
        if (query === '' && sort === '') {
            var resprom = $http.get(Data.serviceBase() + '/invoice/orgInvoices', {params: {"organizationId": $scope.organizationId,"userId": $scope.userId}});
            resprom.then(function (response) {
                if (response.data) {
                    $scope.invoices = response.data;
                    $scope.invoiceloader = false;
                }
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
            //Locale
            var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});
	        resprom.then(function (response) {
	            if (response.data) {
	                $scope.orgDet = response.data[0];
	            }
	        });
        } else if (query !== '' || sort !== '') {
            var reqflag = 0;
            if (query && query.length > 2) {
                reqflag = 1;
            } else if (sort && sort.length > 2) {
                reqflag = 1;
            }
            if (reqflag) {
                var resprom = $http.get(Data.serviceBase() + '/invoice/orgQueryInvoices', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "query": query, "sort": sort}});
                resprom.then(function (response) {
                    if (response.data) {
                        $scope.invoices = response.data;

                        $scope.invoiceloader = false;
                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });

            }
        }
        } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.userName = sessionStorage.getItem('firstName') + ' ' + sessionStorage.getItem('lastName');
    $scope.userEmail = sessionStorage.getItem('userEmail');
    $scope.userPhone = sessionStorage.getItem('countryCode') + sessionStorage.getItem('userPhone');

    


    $scope.updateQuoteConfig = function (qiConfig) {
    	console.log(qiConfig.id);
        qiConfig.organizationId = $scope.organizationId;
        if (qiConfig.id === undefined) {
            Data.post('organization/qiConfig', qiConfig).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
					window.location.href = Data.webAppBase() + 'account/quotesinvoice';
                } else {
                    console.log(result);
                }
            });
        } else {
            Data.put('organization/qiConfig', qiConfig).then(function (result) {
                console.log(qiConfig);
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
					window.location.href = Data.webAppBase() + 'account/quotesinvoice';
                } else {
                    console.log(result);
                }
            });
        }






    };

    $scope.updateProduct = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoice"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.loader = false;
                    if (response.data.status === 'success') {

                        //$scope.getAllServCategField();
                        var modalInstance = $uibModal.open({
                            templateUrl: './prodcutModifiy',
                            controller: 'modifyProductCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                product: function () {

                                    return p;
                                }


                            }
                        });
                        modalInstance.result.then(function (i) {
                            if (i) {
                                $scope.productloader = true;
                                $scope.getProductCatalogue();
                            }
                        });
                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                //console.log(response.data);
                //$scope.message = {"title": "Upgrade & enjoy full power of HelloLeads", "message": "We appreciate your extensive use of HelloLeads for your business. Please subscribe to paid plan to continue to enjoy the full power of HelloLeads. <a href='" + Data.webAppBase() + "account/pricing'>Click here  to pay now.</a>","cancel":"true"};
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };

    $rootScope.addProduct = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                // $rootScope.getAllServCategField();

                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoice"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.loader = false;
                    if (response.data.status === 'success') {


                        var modalInstance = $uibModal.open({
                            templateUrl: './prodcutModifiy',
                            controller: 'modifyProductCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                product: function () {

                                    return p;
                                },
                            }
                        });
                        modalInstance.result.then(function (i) {
                            if (i) {
                                $scope.productloader = true;
                                $scope.getProductCatalogue();
                            }
                        });


                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.deleteMultiProduct = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                // $rootScope.getAllServCategField();

                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoice"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.loader = false;
                    if (response.data.status === 'success') {
                        var modalInstance = $uibModal.open({
                            templateUrl: './deleteProduct',
                            controller: 'delProductCtrl',
                            size: 'md',
                            backdrop: 'static',
                            resolve: {
                                productList: function () {
                                    return $scope.selectedProduct.join();
                                },
                                productCount: function () {
                                    return $scope.selectedProduct.length;
                                },
                                product: function () {
                                    return 0;
                                },
                            }
                        });
                        modalInstance.result.then(function (i) {
                            if (i) {
                                $scope.productloader = true;
                                $scope.checkstate = false;
                                $scope.selectedProduct = [];
                                $scope.selectedProduct.length = 0;
                                $scope.getProductCatalogue();
                            }
                        });
                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.deleteProduct = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
                // $rootScope.getAllServCategField();

                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoice"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.loader = false;
                    if (response.data.status === 'success') {


                        var modalInstance = $uibModal.open({
                            templateUrl: './deleteProduct',
                            controller: 'delProductCtrl',
                            size: 'md',
                            backdrop: 'static',
                            resolve: {
                                product: function () {
                                    return p;
                                },
                                productList: function () {
                                    return 0;
                                },
                                productCount: function () {
                                    return 0;
                                }
                            }
                        });
                        modalInstance.result.then(function (i) {
                            if (i) {
                                $scope.productloader = true;
                                $scope.getProductCatalogue();
                            }
                        });


                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };


    $scope.fileError = false;
    $scope.getProductCatalogFile = function (e) {
        //console.log("Getting Files");
        $scope.filesCsv = [];
        $scope.$apply(function () {

            // STORE THE FILE OBJECT IN AN ARRAY.
            for (var i = 0; i < e.files.length; i++) {
                if ((e.files[i]['name'].indexOf("xls")) !== -1 || (e.files[i]['name'].indexOf("xlsx")) !== -1 && e.files[i].size / 1024 / 1024 < 5) { //(e.files[i]['name'].indexOf("csv")) !== -1 ||
                    $scope.filesCsv.push(e.files[i]);
                    $scope.fileError = false;
                    $scope.uploadProductFiles();
                } else {
                    $scope.fileError = true;
                }
            }
            console.log($scope.fileError);
        });
    };
    $scope.btnText = "Upload product catalog";
    $scope.btnDisable = false;
    // NOW UPLOAD THE FILES.
    $scope.uploadProductFiles = function () {

        $scope.btnText = 'Uploading...';
        $scope.btnDisable = true;
        //FILL FormData WITH FILE DETAILS.

        var data = new FormData();
        for (var i in $scope.filesCsv) {
            ////console.log($scope.filesCsv[i]);
            data.append("filelist", $scope.filesCsv[i]);
        }

        data.append("organizationId", sessionStorage.getItem('orgId'));


        var request = {
            method: 'POST',
            url: Data.serviceBase() + "/quote/fileuploadExcel/",
            data: data,
            headers: {
                'Content-Type': undefined
            }
        };
        // SEND THE FILES.
        $http(request)
                .success(function (result) {
                    //$uibModalInstance.close(1);
                    //$scope.serviceForm.$setPristine();
                    //$scope.productForm.$setPristine();
                    $scope.btnText = "Upload product";
                    if (result.status === 'success') {
                        $scope.getProductCatalogue();
                        $scope.message = {"title": "Import Product Catalog", "message": result.message};
                        alertUser.alertpopmodel($scope.message);
                    } else {
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    }

                })
                .error(function (result) {
                    //$uibModalInstance.close(1);
                    $scope.btnText = "Upload the file";
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                });
    };

    $scope.clickCheck = function () {
        if ($scope.btnText === 'Uploading...') {
            return false;
        } else {
            return true;
        }
    };

    $scope.updateCompanyPic = function (e) {
        $scope.uppflag = 1;
        $scope.$apply(function () {


            //$scope.pfiles = e.files[0];
            console.log(parseInt(e.files[0]['size']));
            if (Math.round(parseInt(e.files[0]['size']) / 1024) > 200000) {

                console.log("File size should not exceed 2MB");
                document.getElementById("logo").value = "";
                toaster.pop("error", "", "File size should not exceed 2MB", 10000, 'trustedHtml');
                $scope.uppflag = 0;
                e.files.length = 0;

            } else if (Math.round(parseInt(e.files[0]['size']) / 1024) < 200000) {
                fileReader.readAsDataUrl(e.files[0], $scope)
                        .then(function (result) {
                            //console.log($scope.filecsv);
                            $scope.qiConfig.companyLogopic = result;
                            $scope.clogo = result;
                            /*Data.put('visitor/updateProfilePhoto', $scope.leadPhoto).then(function (result) {
                             if (result.status !== 'error') {
                             $scope.uppflag = 0;
                             $scope.viewvisitor['photourl'] = result.location;
                             document.getElementById("photoFile").value = "";
                             } else {
                             document.getElementById("photoFile").value = "";
                             }
                             });*/

                        });


            }
        });
    };

    $scope.updateSignaturePic = function (e) {
        $scope.uppflag = 1;
        $scope.$apply(function () {


            //$scope.pfiles = e.files[0];
            console.log(e.files[0]);
            if (Math.round((parseInt(e.files[0]['size']) / 1024)) > 200000) {

                console.log("File size should not exceed 2MB");
                document.getElementById("signature").value = "";
                toaster.pop("error", "", "File size should not exceed 2MB", 10000, 'trustedHtml');
                $scope.uppflag = 0;
                e.files.length = 0;

            } else if (Math.round((parseInt(e.files[0]['size']) / 1024)) < 200000) {
                fileReader.readAsDataUrl(e.files[0], $scope)
                        .then(function (result) {
                            //console.log($scope.filecsv);
                            $scope.qiConfig.signatureImgpic = result;
                            $scope.signpic = result;
                            /*Data.put('visitor/updateProfilePhoto', $scope.leadPhoto).then(function (result) {
                             if (result.status !== 'error') {
                             $scope.uppflag = 0;
                             $scope.viewvisitor['photourl'] = result.location;
                             document.getElementById("photoFile").value = "";
                             } else {
                             document.getElementById("photoFile").value = "";
                             }
                             });*/

                        });


            }
        });
    };

    $scope.removeLogo = function () {
        $scope.qiConfig.companyLogopic = '';
        $scope.qiConfig.companyLogo = '';
        $scope.clogo = '';
    };
    $scope.removeSignature = function () {
        $scope.qiConfig.signatureImgpic = '';
        $scope.qiConfig.signatureImg = '';
        $scope.signpic = '';
    };


    // Quotes creattion popup

    $scope.createQuote = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.quoteloader = false;
            if (response.data.status === 'success') {
                // $rootScope.getAllServCategField();

                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoice"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.quoteloader = false;
                    if (response.data.status === 'success') {
                        // $scope.icopyStarting = 0;
                        // if ($scope.quotes.length > 0) {

                        //     $scope.icopyStarting = parseInt($scope.qiConfig.qStarting) + 1;
                        //     console.log($scope.icopyStarting);
                        // } else {
                        //     $scope.icopyStarting = $scope.qiConfig.qStarting;
                        // }
                        var modalInstance = $uibModal.open({
                            templateUrl: './modifyQuote',
                            controller: 'modifyQuoteCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                quote: function () {
                                    return p;
                                },
                                qiConfig: function () {

                                    return $scope.qiConfig;
                                }
                                // qStarting: function () {
                                //     return $scope.icopyStarting;
                                // }
                            }
                        });
                        modalInstance.result.then(function (i) {
                            if (i) {
                                $scope.quoteloader = true;

                                //$scope.qiConfig.qStarting = $scope.icopyStarting;
                                // Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                                //     if (result.status !== 'error') {
                                //         $scope.uppflag = 0;
                                //         //toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');

                                //     } else {

                                //     }
                                // });
                                $scope.getQuotes('', '');
                            }
                        });


                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.editQuote = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.quoteloader = false;
            if (response.data.status === 'success') {
                // $rootScope.getAllServCategField();

                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoice"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.quoteloader = false;
                    if (response.data.status === 'success') {


                        var modalInstance = $uibModal.open({
                            templateUrl: './modifyQuote',
                            controller: 'modifyQuoteCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                quote: function () {
                                    return p;
                                },
                                qiConfig: function () {

                                    return $scope.qiConfig;
                                }
                                // qStarting: function () {
                                //     return 0;
                                // }
                            }
                        });
                        modalInstance.result.then(function (i) {
                            if (i) {
                                $scope.quoteloader = true;
                                $scope.getQuotes('', '');

                            }
                        });


                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.deleteQuote = function (p, size) {

        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.quoteloader = false;
            if (response.data.status === 'success') {
                // $rootScope.getAllServCategField();

                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoice"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.quoteloader = false;
                    if (response.data.status === 'success') {


                        var modalInstance = $uibModal.open({
                            templateUrl: './deleteQuote',
                            controller: 'deleteQuoteCtrl',
                            size: 'md',
                            backdrop: 'static',
                            resolve: {
                                quote: function () {
                                    return p;
                                }

                            }
                        });
                        modalInstance.result.then(function (i) {
                            if (i) {
                                $scope.quoteloader = true;
                                /*$scope.qiConfig.qStarting = parseInt($scope.qiConfig.qStarting) - 1;
                                 Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                                 if (result.status !== 'error') {
                                 $scope.uppflag = 0;
                                 //toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');
                                 
                                 } else {
                                 
                                 }
                                 });*/
                                $scope.getQuotes('', '');
                            }
                        });


                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });


    };


    $scope.previewQuote = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('quote/setPreviewQuote', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.previewInvoice = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('invoice/setPreviewInvoice', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };


    $scope.viewQuote = function (quote) {
        /*  Data.post('quote/createActivity', quote).then(function (result) {
         if (result.status !== 'error') {
         $scope.uppflag = 0;
         toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
         
         } else {
         toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
         }
         });*/
		 $scope.userName = sessionStorage.getItem('firstName') + ' ' + sessionStorage.getItem('lastName');
        window.open(Data.webAppBase() + "/account/showQuotes/" + quote.quoteRef,"_blank");
    };


    $scope.convertInvoice = function (quote) {
        $scope.icopyStarting = 0;
        $scope.icopyStarting = $scope.qiConfig.iStarting;
        $scope.cinvoice = {};
        $scope.cinvoice['invoiceNo'] = $scope.icopyStarting;
        $scope.cinvoice['leadName'] = quote.leadName;
        $scope.cinvoice['leadOrgName'] = quote.leadOrgName;
        $scope.cinvoice['visitorId'] = quote.visitorId;
        $scope.cinvoice['organizationId'] = $scope.organizationId;
        $scope.cinvoice['invoiceCurrency'] = quote.quoteCurrency;
        $scope.cinvoice['invoiceSubTotal'] = quote.quoteSubTotal;
        $scope.cinvoice['invoiceNetTotal'] = quote.quoteNetTotal;
        $scope.cinvoice['invoiceTax'] = quote.quoteTax;
        $scope.cinvoice['invoiceOffer'] = quote.quoteOffer;
        $scope.cinvoice['invoiceTotal'] = quote.quoteTotal;
        $scope.cinvoice['invoiceUserId'] = quote.quoteUserId;
        $scope.cinvoice['invoiceCreatedBy'] = quote.quoteCreatedBy;
		$scope.cinvoice['discountLabel'] = quote.discountLabel;
		$scope.cinvoice['discountPercent'] = quote.discountPercent;
		$scope.cinvoice['taxLabel'] = quote.taxLabel;
		$scope.cinvoice['taxPercent'] = quote.taxPercent;
        $scope.cinvoice['leadTaxCode'] = quote.leadTaxCode;
		$scope.cinvoice['additionalTaxLabel'] = quote.additionalTaxLabel;
		$scope.cinvoice['invoiceAdditional'] = quote.quoteAdditional;
		$scope.cinvoice['leadAddress1'] = quote.leadAddress1;
		$scope.cinvoice['leadAddress2'] = quote.leadAddress2;
		$scope.cinvoice['leadCity'] = quote.leadCity;
		$scope.cinvoice['leadState'] = quote.leadState;
		$scope.cinvoice['leadCountry'] = quote.leadCountry;
		$scope.cinvoice['leadZip'] = quote.leadZip;
		$scope.cinvoice['leadEmail'] = quote.leadEmail;
		$scope.cinvoice['leadMobile'] = quote.leadMobile;

        var resprom = $http.get(Data.serviceBase() + '/quote/quoteProductsWithLocale', {params: {"organizationId": $scope.organizationId, "quoteId": quote.id}});

        resprom.then(function (response) {
            if (response.data) {

                $scope.items = response.data;
                $scope.cinvoice['items'] = JSON.stringify($scope.items);



                Data.post('invoice/convertInvoice', $scope.cinvoice).then(function (result) {
                    if (result.status !== 'error') {
                        $scope.uppflag = 0;
                        $scope.qiConfig.iStarting = $scope.icopyStarting;
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                        window.location.href = Data.webAppBase() + 'account/quotesinvoice';
                        $scope.qiConfig.iStarting = $scope.icopyStarting;
                        Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                            if (result.status !== 'error') {
                                $scope.uppflag = 0;
                                //toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');

                            } else {

                            }
                        });
                        $scope.getInvoices('', '');
                    } else {
                        toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    }
                });
                //$scope.rearrangeItem();
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    // Invoices creattion popup

    $scope.createInvoice = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.invoiceloader = false;
            if (response.data.status === 'success') {
                // $rootScope.getAllServCategField();

                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoice"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.invoiceloader = false;
                    if (response.data.status === 'success') {
                        // $scope.icopyStarting = 0;
                        // if ($scope.invoices.length > 0) {

                        //     $scope.icopyStarting = parseInt($scope.qiConfig.iStarting) + 1;
                        //     console.log($scope.icopyStarting);
                        // } else {
                        //     $scope.icopyStarting = $scope.qiConfig.iStarting;
                        // }
                        var modalInstance = $uibModal.open({
                            templateUrl: './modifyInvoice',
                            controller: 'modifyInvoiceCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                invoice: function () {
                                    p.invoiceStatus = 'Unpaid';
                                    return p;
                                },
                                qiConfig: function () {

                                    return $scope.qiConfig;
                                }
                                // iStarting: function () {
                                //     return $scope.icopyStarting;
                                // }
                            }
                        });
                        modalInstance.result.then(function (i) {
                            if (i) {
                                $scope.invoiceloader = true;

                                // $scope.qiConfig.iStarting = $scope.icopyStarting;
                                // Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                                //     if (result.status !== 'error') {
                                //         $scope.uppflag = 0;
                                //         //toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');

                                //     } else {

                                //     }
                                // });
                                $scope.getInvoices('', '');
                            }
                        });


                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.editInvoice = function (p, size) {
        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.invoiceloader = false;
            if (response.data.status === 'success') {
                // $rootScope.getAllServCategField();

                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoice"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.invoiceloader = false;
                    if (response.data.status === 'success') {


                        var modalInstance = $uibModal.open({
                            templateUrl: './modifyInvoice',
                            controller: 'modifyInvoiceCtrl',
                            size: 'lg',
                            backdrop: 'static',
                            resolve: {
                                invoice: function () {
                                    return p;
                                },
                                qiConfig: function () {

                                    return $scope.qiConfig;
                                }
                                // iStarting: function () {
                                //     return 0;
                                // }
                            }
                        });
                        modalInstance.result.then(function (i) {
                            if (i) {
                                $scope.invoiceloader = true;
                                $scope.getInvoices('', '');

                            }
                        });


                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.deleteInvoice = function (p, size) {

        var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": $scope.organizationId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.invoiceloader = false;
            if (response.data.status === 'success') {
                // $rootScope.getAllServCategField();

                var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": $scope.organizationId, "userId": $scope.userId, "permission": "quotesInvoice"}});

                // $scope.users = {};
                resprole.then(function (response) {
                    console.log(response);
                    $scope.invoiceloader = false;
                    if (response.data.status === 'success') {


                        var modalInstance = $uibModal.open({
                            templateUrl: './deleteInvoice',
                            controller: 'deleteInvoiceCtrl',
                            size: 'md',
                            backdrop: 'static',
                            resolve: {
                                invoice: function () {
                                    return p;
                                }

                            }
                        });
                        modalInstance.result.then(function (i) {
                            if (i) {
                                $scope.invoiceloader = true;
                                /*$scope.qiConfig.qStarting = parseInt($scope.qiConfig.qStarting) - 1;
                                 Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                                 if (result.status !== 'error') {
                                 $scope.uppflag = 0;
                                 //toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');
                                 
                                 } else {
                                 
                                 }
                                 });*/
                                $scope.getInvoices('', '');
                            }
                        });


                    } else {
                        $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to create, modify or delete lists. Users with account owner or manger roles can create or manage lists. Please contact HelloLeads account owner or mangers in your company for adding lists or modifying them"};
                        alertUser.alertpopmodel($scope.message);
                        //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    }
                }, function (response) {

                    console.log('Error happened -- ');
                    console.log(response);
                });
            } else {
                trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });


    };
    $scope.viewInvoice = function (invoice) {
        /*  Data.post('invoice/createActivity', invoice).then(function (result) {
         if (result.status !== 'error') {
         $scope.uppflag = 0;
         toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
         
         } else {
         toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
         }
         });*/
        window.open(Data.webAppBase() + "/account/showInvoices/" + invoice.invoiceRef, "_blank");
    };


});

evtApp.controller('modifyProductCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, Data, $uibModal, $uibModalInstance, product, $http, toaster)
{

    $scope.product = angular.copy(product);
    $scope.orgCurrency = sessionStorage.getItem('orgCurrency');
    $scope.organizationId = sessionStorage.getItem('orgId');
    if ($scope.product.id === undefined) {
        $scope.title = 'Add new product';
        $scope.btntxt = 'Add';
        console.log($scope.orgCurrency);
        $scope.product.currency = $scope.orgCurrency;
    } else {
        $scope.title = 'Update product';
        $scope.btntxt = 'Update';
    }

    $scope.updateProduct = function (product) {
        product.organizationId = $scope.organizationId;
        if (product.id === undefined) {
            Data.post('quote/productCatalogue', product).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    $uibModalInstance.close(1);

                } else {
                    console.log(result);
                }
            });
        } else {
            Data.put('quote/productCatalogue', product).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    $uibModalInstance.close(1);

                } else {
                    console.log(result);
                }
            });
        }



    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('delProductCtrl', function ($scope, Data, $uibModalInstance, product, toaster, productList, productCount) {

    $scope.product = angular.copy(product);
    $scope.title = "Delete Product";
    $scope.productCount = productCount;
    $scope.deleteProduct = function (product) {
        Data.delete('quote/productCatalogue/' + product.id).then(function (result) {
            if (result.status !== 'error') {
                $uibModalInstance.close(1);
                // $uibModalInstance.dismiss('Close');
                console.log('Setting the event ')
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');

            } else {
                console.log(result);
            }
        });
    };

    $scope.deleteMultipleProducts = function () {
        Data.delete('quote/productCatalogueMany/' + productList).then(function (result) {
            if (result.status !== 'error') {
                $uibModalInstance.close(1);
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
            } else {
                console.log(result);
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});


evtApp.controller('modifyQuoteCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, $uibModalInstance, qiConfig, quote, $http, toaster)
{
    $scope.quote = angular.copy(quote);
    $scope.qiConfig = angular.copy(qiConfig);
    //$scope.qstart = qStarting;
    $scope.orgCurrency = sessionStorage.getItem('orgCurrency');
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.comlogo = '';
    $scope.pitem = {};
    $scope.items = [];
    $scope.orgDet = {};
    $scope.leadDet = {};

    if ($scope.qiConfig) {
        $scope.comlogo = $scope.qiConfig.companyLogo;
        $scope.signpict = $scope.qiConfig.signatureImg;
        if ($scope.quote.id === undefined) {
            //$scope.quote.quoteNo = $scope.qstart !== 0 ? $scope.qstart : $scope.qiConfig.qStarting;
            $scope.quote.quoteDate = new Date();
            $scope.quote.quoteTitle = $scope.qiConfig.qTitle;
			$scope.quote.discountLabel = $scope.qiConfig.discountLabel;
			$scope.quote.discountPercent = $scope.qiConfig.discountPercent;
			$scope.quote.taxLabel = $scope.qiConfig.taxlabel;
			$scope.quote.taxPercent = $scope.qiConfig.taxPercent;
			$scope.quote.additionalTaxLabel = $scope.qiConfig.additionalTaxLabel;
			$scope.quote.quoteAdditional = $scope.qiConfig.additionalTaxPercent;
            $scope.quote.quoteCreatedBy = sessionStorage.getItem('firstName') + " " + sessionStorage.getItem('lastName');
        }
    }

    $scope.rearrangeItem = function () {
        var index = 1;
        var tempitems = $scope.items;
        $scope.items = [];
        angular.forEach(tempitems, function (d) {
            if (d.totalCost) {
                d.slno = index;
                index = index + 1;
                $scope.items.push(d);
            }
        });
    };

    $scope.getQuoteProduct = function () {
        var resprom = $http.get(Data.serviceBase() + '/quote/quoteProductsWithLocaleModify', {params: {"organizationId": $scope.organizationId, "quoteId": $scope.quote.id}});

        resprom.then(function (response) {
            if (response.data) {

                $scope.items = response.data;
                $scope.rearrangeItem();
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    if ($scope.quote.id === undefined) {
        $scope.title = 'Create ' + $scope.quote.quoteTitle;
        $scope.btntxt = 'Save';
        $scope.quote.quoteCurrency = $scope.qiConfig.qCurrency;
    } else {
        $scope.title = 'Update ' + $scope.quote.quoteTitle;
        $scope.btntxt = 'Save';
        $scope.leadDet.name = $scope.quote.leadName;
        $scope.leadDet.visitorOrganizationName = $scope.quote.leadOrgName;
        $scope.leadDet.address1 = $scope.quote.leadAddress1;
        $scope.leadDet.address2 = $scope.quote.leadAddress2;
        $scope.leadDet.email = $scope.quote.leadEmail;
        $scope.leadDet.mobile = $scope.quote.leadMobile;
        $scope.leadDet.others = $scope.quote.leadTaxCode;
        $scope.leadDet.city = $scope.quote.leadCity;
        $scope.leadDet.state = $scope.quote.leadState;
        $scope.leadDet.country = $scope.quote.leadCountry;
        $scope.leadDet.zip = $scope.quote.leadZip;
        $scope.getQuoteProduct();
    }
    $scope.getOrgDetail = function () {
        var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

        resprom.then(function (response) {
            if (response.data) {
                $scope.orgDet = response.data[0];
                $scope.locale = response.data[0].countryCode;
            }

            //Additional Amount
		    if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
				$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
				$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
				$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
				$scope.locale != 'fr-FR') {
				if($scope.quote.quoteAdditional == null || $scope.quote.quoteAdditional == '') {
					$scope.quote.quoteAdditional = '0';
				}
				$scope.addI = $scope.quote.quoteAdditional;
				$scope.addComma = $scope.addI.replace(/,/g,'');
				$scope.addFI = parseFloat($scope.addComma).toFixed(2);
		     } else {
		     	if($scope.quote.quoteAdditional == null || $scope.quote.quoteAdditional == '') {
					$scope.quote.quoteAdditional = '0';
				}
		      	$scope.addI = $scope.quote.quoteAdditional;
		      	$scope.addDot = $scope.addI.replace(/(\.|,)/g, function(x) {
				    return x == ',' ? '.' : ',';
				});
				$scope.addComma = $scope.addDot.replace(/,/g,'');
				$scope.addFI = parseFloat($scope.addComma).toFixed(2);
		     }
		    $scope.addPer = $scope.addFI;
		    $scope.quote.qA = $scope.addPer;
		    //End Additional Amount

		    //Tax Percentage
		    if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
			    	if($scope.quote.taxPercent == null || $scope.quote.taxPercent == '') {
						$scope.quote.taxPercent = '0';
					}
            		$scope.taxI = $scope.quote.taxPercent;
            		$scope.taxComma = $scope.taxI.replace(/,/g,'');
            		$scope.taxFI = parseFloat($scope.taxComma).toFixed(2);
	             } else {
	             	if($scope.quote.taxPercent == null || $scope.quote.taxPercent == '') {
						$scope.quote.taxPercent = '0';
					}
	              	$scope.taxI = $scope.quote.taxPercent;
	              	$scope.taxDot = $scope.taxI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.taxComma = $scope.taxDot.replace(/,/g,'');
					$scope.taxFI = parseFloat($scope.taxComma).toFixed(2);
	             }
	            $scope.taxPer = $scope.taxFI;
                $scope.quote.tP = $scope.taxPer;
                //End Tax Percentage

                //Discount Percentage
                if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
                	if($scope.quote.discountPercent == null || $scope.quote.discountPercent == '') {
						$scope.quote.discountPercent = '0';
					}
            		$scope.disI = $scope.quote.discountPercent;
            		$scope.disComma = $scope.disI.replace(/,/g,'');
            		$scope.disFI = parseFloat($scope.disComma).toFixed(2);
	             } else {
	             	if($scope.quote.discountPercent == null || $scope.quote.discountPercent == '') {
						$scope.quote.discountPercent = '0';
					}
	              	$scope.disI = $scope.quote.discountPercent;
	              	$scope.disDot = $scope.disI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.disComma = $scope.disDot.replace(/,/g,'');
					$scope.disFI = parseFloat($scope.disComma).toFixed(2);
	             }
	            $scope.disPer = $scope.disFI;
                $scope.quote.dP = $scope.disPer;
                //End Discount Percentage

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    
    $scope.openOrgDetail = function () {
        window.open(Data.webAppBase() + "account/company", '_blank');
    };

    $scope.setVisitor = function (selected) {
        $scope.leadDet = selected.originalObject;
        $scope.quote.leadName = $scope.leadDet.name;
        $scope.quote.leadOrgName = $scope.leadDet.visitorOrganizationName;
        $scope.quote.visitorId = $scope.leadDet.id;
        $scope.quote.leadTaxCode = $scope.leadDet.others;
        $scope.quote.leadAddress1 = $scope.leadDet.address1;
        $scope.quote.leadAddress2 = $scope.leadDet.address2;
        $scope.quote.leadCity = $scope.leadDet.city;
        $scope.quote.leadState = $scope.leadDet.state;
        $scope.quote.leadCountry = $scope.leadDet.country;
        $scope.quote.leadZip = $scope.leadDet.zip;
        $scope.quote.leadEmail = $scope.leadDet.email;
        $scope.quote.leadMobile = $scope.leadDet.mobile;
    };



    $scope.getItem = function (p, size) {

        var modalInstance = $uibModal.open({
            templateUrl: './addItem',
            controller: 'addItemCtrl',
            size: 'md',
            backdrop: 'static',
            resolve: {
                item: function () {
                    return p;
                },
                quote: function () {
                    return $scope.quote;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

                if (i.slno !== undefined) {
                    angular.forEach($scope.items, function (d) {
                        if (d.slno === i.slno) {
                            $scope.items[i.slno - 1] = i;
                        }
                    });

                } else {
                    var index = 1;
                    angular.forEach($scope.items, function (d) {
                        if (d.totalCost) {
                            index = index + 1;
                        }
                    });
                    i.slno = index;
                    $scope.items.push(i);
                }

                $scope.calculateTotal();
            }
        });


    };

    $scope.deleteItem = function (item) {

        $scope.items.splice(parseInt(item.slno) - 1, 1);

        $scope.rearrangeItem();
        $scope.calculateTotal();

    };

    $scope.updateCompanylogoPic = function (e) {
        $scope.uppflag = 1;
        $scope.$apply(function () {


            //$scope.pfiles = e.files[0];
            if (parseInt(e.files[0]['size']) > 200000) {

                console.log("File size should not exceed 2MB");
                document.getElementById("cmlogo").value = "";
                toaster.pop("error", "", "File size should not exceed 2MB", 10000, 'trustedHtml');
                $scope.uppflag = 0;
                e.files.length = 0;

            } else if (parseInt(e.files[0]['size']) < 200000) {
                fileReader.readAsDataUrl(e.files[0], $scope)
                        .then(function (result) {
                            //console.log($scope.filecsv);
                            $scope.qiConfig.companyLogopic = result;
                            $scope.comlogo = result;
                            Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                                if (result.status !== 'error') {
                                    $scope.uppflag = 0;
                                    toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');
                                    document.getElementById("cmlogo").value = "";
                                } else {
                                    document.getElementById("cmlogo").value = "";
                                }
                            });

                        });


            }
        });
    };

    $scope.updateSignaturePict = function (e) {
        $scope.uppflag = 1;
        $scope.$apply(function () {


            //$scope.pfiles = e.files[0];
            if (Math.round(parseInt(e.files[0]['size'] / 1024)) > 200000) {

                console.log("File size should not exceed 2MB");
                document.getElementById("signaturePic").value = "";
                toaster.pop("error", "", "File size should not exceed 2MB", 10000, 'trustedHtml');
                $scope.uppflag = 0;
                e.files.length = 0;

            } else if (Math.round(parseInt(e.files[0]['size'] / 1024)) < 200000) {
                fileReader.readAsDataUrl(e.files[0], $scope)
                        .then(function (result) {
                            //console.log($scope.filecsv);
                            $scope.qiConfig.signatureImgpic = result;
                            $scope.signpict = result;
                            Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                                if (result.status !== 'error') {
                                    $scope.uppflag = 0;
                                    toaster.pop("success", "", "Signature updated successfully", 10000, 'trustedHtml');
                                    document.getElementById("signaturePic").value = "";
                                } else {
                                    document.getElementById("signaturePic").value = "";
                                }
                            });

                        });


            }
        });
    };
    //Change Currency Value
    $scope.changeQiCurrency = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './changeCurrency',
            controller: 'changeCurrencyCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                currency: function () {
                    return p;
                },
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.quote.quoteCurrency = i;
                $scope.qiConfig.qCurrency = i
                Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                    if (result.status !== 'error') {
                        $scope.uppflag = 0;
                        // toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');

                    } else {

                    }
                });
            }
        });


    };

     //Change Discount Percentage Value
    $scope.changeQiDiscount = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './changeDiscount',
            controller: 'changeDiscountCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                discount: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                //$scope.quote.quoteOffer = i;
                //$scope.qiConfig.discountPercent = i;
                if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
            		$scope.disI = i;
            		$scope.disComma = $scope.disI.replace(/,/g,'');
            		$scope.disFI = parseFloat($scope.disComma).toFixed(2);
	             } else {
	              	$scope.disxI = i;
	              	$scope.disDot = $scope.disxI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.disComma = $scope.disDot.replace(/,/g,'');
					$scope.disFI = parseFloat($scope.disComma).toFixed(2);
	             }
	            $scope.disPer = $scope.disFI;
                $scope.quote.dP = $scope.disPer;
                $scope.uppflag = 0;
               	$scope.calculateTotal();
            }
        });
    };


     //Change Tax Percentage Value
    $scope.changeQiTax = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './changeTax',
            controller: 'changeTaxCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                tax: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
            	if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
            		$scope.taxI = i;
            		$scope.taxComma = $scope.taxI.replace(/,/g,'');
            		$scope.taxFI = parseFloat($scope.taxComma).toFixed(2);
	             } else {
	              	$scope.taxI = i;
	              	$scope.taxDot = $scope.taxI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.taxComma = $scope.taxDot.replace(/,/g,'');
					$scope.taxFI = parseFloat($scope.taxComma).toFixed(2);
	             }
	            $scope.taxPer = $scope.taxFI;
                $scope.quote.tP = $scope.taxPer;
                $scope.uppflag = 0;
               	$scope.calculateTotal(); 
            }
        });
    };


     //Change Discount Amount Value
    $scope.changeQiDiscountAmount = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './changeTaxAmount',
            controller: 'changeTaxAmountCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                taxAmount: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
            	$scope.percent = 0;
            	$scope.conI = 0;
            	$scope.convertSym = '';
            	$scope.setI = 0;
            	$scope.getI = 0;
            	$scope.Ival = 0;
            	if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
            		$scope.Ival = i;
            		$scope.convI = $scope.Ival.replace(/,/g,'');
            		$scope.conI = parseFloat($scope.convI).toFixed(2);
            		$scope.setST = $scope.quote.quoteSubTotal;
					$scope.getST = $scope.setST.replace(/,/g,'');
					$scope.getSuTotal = parseFloat($scope.getST).toFixed(2);
            	if ($scope.items.length > 0) {
	            angular.forEach($scope.items, function (d) {
	                if($scope.qiConfig.discountLabel || $scope.quote.discountLabel) {
	                	$scope.per = (($scope.conI * 100) / $scope.getSuTotal);
	                }
	            });
               }
              } else {
              	$scope.conI = i;
              	$scope.convertSym = $scope.conI.replace(/(\.|,)/g, function(x) {
				    return x == ',' ? '.' : ',';
				});
				$scope.setI = $scope.convertSym.replace(/,/g,'');
				$scope.getI = parseFloat($scope.setI).toFixed(2);
				$scope.SubT = $scope.quote.quoteSubTotal;
				$scope.convertST = $scope.SubT.replace(/(\.|,)/g, function(x) {
				    return x == ',' ? '.' : ',';
				});
				$scope.SubTo = $scope.convertST.replace(/,/g,'');
				$scope.SubTot = parseFloat($scope.SubTo).toFixed(2);
              	if ($scope.items.length > 0) {
	            angular.forEach($scope.items, function (d) {
	                if($scope.qiConfig.discountLabel || $scope.quote.discountLabel) {
	                	$scope.per = (($scope.getI * 100) / $scope.SubTot);
	                }
	            });
               }
             }
                $scope.discountPer = $scope.per;
                $scope.quote.dP = $scope.discountPer;
                //Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                  //  if (result.status !== 'error') {
                        $scope.uppflag = 0;
                        $scope.calculateTotal();
                    //} else {

                    //}
                //});
            }
        });
    };

    //Change Additional Amount Value
    $scope.changeQiAdditionalAmount = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './changeAdditionalAmount',
            controller: 'changeAdditionalAmountCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                additionalAmount: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                //$scope.quote.quoteTax = i;
                //$scope.qiConfig.additionalTaxPercent = i;
                if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
            		$scope.addI = i;
            		$scope.addComma = $scope.addI.replace(/,/g,'');
            		$scope.addFI = parseFloat($scope.addComma).toFixed(2);
	             } else {
	              	$scope.addI = i;
	              	$scope.addDot = $scope.addI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.addComma = $scope.addDot.replace(/,/g,'');
					$scope.addFI = parseFloat($scope.addComma).toFixed(2);
	             }
	            $scope.addPer = $scope.addFI;
                $scope.quote.qA = $scope.addPer;
                $scope.uppflag = 0;
               	$scope.calculateTotal(); 
            }
        });
    };

    $scope.calculateTotal = function () {
        $scope.quote.quoteSubTotal = '0';
        $scope.quote.quoteOffer = 0;
        $scope.quote.quoteNetTotal = 0;
        $scope.quote.quoteTax = 0;
        $scope.quote.quoteTotal = 0;
        $scope.quote.quoteNet = 0;
        $scope.quote.quoteTx = 0;
        $scope.quote.quoteAdd = 0;
        $scope.addST = '0';
        if ($scope.items.length > 0) {
            angular.forEach($scope.items, function (d) {
                if (d.totalCost) {

                	//Product Sub total
                		if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
		            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
		            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
		            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
		            		$scope.locale != 'fr-FR') {
		            		$scope.addI = d.totalCost;
		            		$scope.addComma = $scope.addI.replace(/,/g,'');
		            		$scope.addFI = parseFloat($scope.addComma).toFixed(2);
			             } else {
			              	$scope.addI = d.totalCost;
			              	$scope.addDot = $scope.addI.replace(/(\.|,)/g, function(x) {
							    return x == ',' ? '.' : ',';
							});
							$scope.addComma = $scope.addDot.replace(/,/g,'');
							$scope.addFI = parseFloat($scope.addComma).toFixed(2);
			             }
			            $scope.addPer = $scope.addFI;
		                $scope.ProductTotal = $scope.addPer;
                	//End Product Sub total

                	//Sub total
                		if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
		            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
		            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
		            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
		            		$scope.locale != 'fr-FR') {
		            		$scope.addST = $scope.quote.quoteSubTotal;
		            		$scope.addCom = $scope.addST.replace(/,/g,'');
		            		$scope.addSTo = parseFloat($scope.addCom).toFixed(2);
			             } else {
			              	$scope.addST = $scope.quote.quoteSubTotal;
			              	$scope.addDots = $scope.addST.replace(/(\.|,)/g, function(x) {
							    return x == ',' ? '.' : ',';
							});
							$scope.addCom = $scope.addDots.replace(/,/g,'');
							$scope.addSTo = parseFloat($scope.addCom).toFixed(2);
			             }
			            $scope.addSubT = $scope.addSTo;
		                $scope.quote.quoteSubTotal = $scope.addSubT;
                	//Sub total

                	$scope.quote.quoteSubTo = parseFloat($scope.quote.quoteSubTotal);
                    $scope.quote.quoteSub = parseFloat($scope.quote.quoteSubTo) + parseFloat($scope.ProductTotal);
                    $scope.quote.quoteSubTotal = $scope.quote.quoteSub.toLocaleString($scope.locale, {minimumFractionDigits: 2});
                    if ($scope.qiConfig.discountLabel || $scope.quote.discountLabel) {
                        $scope.quote.quoteOff = (($scope.quote.quoteSub * $scope.quote.dP) / 100);
                        $scope.quote.quoteOffer =parseFloat( $scope.quote.quoteOff.toFixed(2)).toLocaleString($scope.locale, {minimumFractionDigits: 2});
                        $scope.quote.quoteNetT = $scope.quote.quoteSub - $scope.quote.quoteOff;
                        $scope.quote.quoteNetTotal = parseFloat($scope.quote.quoteNetT.toFixed(2)).toLocaleString($scope.locale, {minimumFractionDigits: 2});
						$scope.quote.discountPer = parseFloat($scope.quote.dP).toFixed(2);
						$scope.quote.discountPercent = parseFloat($scope.quote.discountPer).toLocaleString($scope.locale, {minimumFractionDigits: 2});
                    } else {
                        $scope.quote.quoteNetT = $scope.quote.quoteSub;
                        $scope.quote.quoteNetTotal = parseFloat($scope.quote.quoteNetT.toFixed(2)).toLocaleString($scope.locale, {minimumFractionDigits: 2});
                    }
                    if ($scope.qiConfig.taxlabel || $scope.quote.taxlabel) {
                        $scope.quote.quoteTx = (($scope.quote.quoteNetT * $scope.quote.tP) / 100);
                        $scope.quote.quoteTax = parseFloat($scope.quote.quoteTx.toFixed(2)).toLocaleString($scope.locale, {minimumFractionDigits: 2});
                        $scope.quote.quoteTo = parseFloat($scope.quote.quoteNetT) + parseFloat($scope.quote.quoteTx);
                        $scope.quote.quoteTotal = $scope.quote.quoteTo.toLocaleString($scope.locale, {minimumFractionDigits: 2});
						$scope.quote.taxPer = parseFloat($scope.quote.tP).toFixed(2);
						$scope.quote.taxPercent = parseFloat($scope.quote.taxPer).toLocaleString($scope.locale, {minimumFractionDigits: 2});
                    } 
                    // else {
                    //     $scope.quote.quoteTot = $scope.quote.quoteNetT + $scope.quote.quoteTx;
                    //     $scope.quote.quoteTotal = $scope.quote.quoteTot.toLocaleString($scope.locale);
                    // }
                     if ($scope.qiConfig.additionalTaxLabel || $scope.quote.additionalTaxLabel) {
                        $scope.quote.quoteAdd = parseFloat($scope.quote.qA);
                        $scope.quote.quoteAdditional = $scope.quote.quoteAdd.toLocaleString($scope.locale, {minimumFractionDigits: 2});
                        $scope.quote.quoteFinal = parseFloat($scope.quote.quoteNetT) + parseFloat($scope.quote.quoteTx) + parseFloat($scope.quote.quoteAdd);
                        $scope.quote.quoteTotal = parseFloat($scope.quote.quoteFinal.toFixed(2)).toLocaleString($scope.locale, {minimumFractionDigits: 2});
						// $scope.quote.additionalTP = parseFloat($scope.quote.quoteAdd).toFixed(2);
						// $scope.quote.quoteAdditional = $scope.quote.additionalTP.toLocaleString($scope.locale, {minimumFractionDigits: 2});
                    } else {
                    	$scope.quote.quoteTota =  parseFloat($scope.quote.quoteNetT) + parseFloat($scope.quote.quoteTx) + parseFloat($scope.quote.quoteAdd);
                    	$scope.quote.quoteTotal = parseFloat($scope.quote.quoteTota.toFixed(2)).toLocaleString($scope.locale, {minimumFractionDigits: 2});
                    }
                }
            });
        }
    };
    $scope.openQiConfig = function () {
        window.open(Data.webAppBase() + "account/quotesinvoice#settings", '_blank');
    };

    $scope.updateQuote = function (quote) {
        quote.organizationId = $scope.organizationId;
        quote.quoteUserId = sessionStorage.getItem('userId');
        quote.items = JSON.stringify($scope.items);
        if (quote.id === undefined) {
            Data.post('quote/quote', quote).then(function (result) {
                if (result.status !== 'error') {

                    toaster.pop("success", "", result.message, 10000, 'trustedHtml');
                    window.location.href = Data.webAppBase() + 'account/quotesinvoice';
                    $uibModalInstance.close('1');

                } else {
                    toaster.pop("error", "", result.message, 10000, 'trustedHtml');
                }
            });
        } else {
            Data.put('quote/quote', quote).then(function (result) {
                if (result.status !== 'error') {

                    toaster.pop("success", "", result.message, 10000, 'trustedHtml');
                    window.location.href = Data.webAppBase() + 'account/quotesinvoice';
                    $uibModalInstance.close('1');

                } else {
                    toaster.pop("error", "", result.message, 10000, 'trustedHtml');
                }
            });
        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('deleteQuoteCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, $uibModalInstance, quote, $http, toaster)
{
    $scope.quote = angular.copy(quote);

    $scope.deleteQuote = function (quote) {
        Data.delete('quote/deleteQuote/' + quote.id).then(function (result) {
            if (result.status !== 'error') {

                toaster.pop("success", "", result.message, 10000, 'trustedHtml');
                $uibModalInstance.close('1');

            } else {
                $uibModalInstance.close('1');
                toaster.pop("error", "", result.message, 10000, 'trustedHtml');
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});
evtApp.controller('modifyInvoiceCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, $uibModalInstance, qiConfig, invoice, $http, toaster)
{
    $scope.invoice = angular.copy(invoice);
    $scope.qiConfig = angular.copy(qiConfig);
    //$scope.istart = iStarting;
    $scope.orgCurrency = sessionStorage.getItem('orgCurrency');
    $scope.organizationId = sessionStorage.getItem('orgId');
    $scope.comlogo = '';
    $scope.pitem = {};
    $scope.items = [];
    $scope.orgDet = {};
    $scope.leadDet = {};

    if ($scope.qiConfig) {
        $scope.comlogo = $scope.qiConfig.companyLogo;
        $scope.signpict = $scope.qiConfig.signatureImg;
        console.log($scope.istart);
        if ($scope.invoice.id === undefined) {
            //$scope.invoice.invoiceNo = $scope.istart !== 0 ? $scope.istart : $scope.qiConfig.iStarting;
            $scope.invoice.invoiceDate = new Date();
            //$scope.invoice.invoiceTitle = $scope.qiConfig.qTitle;
			$scope.invoice.discountLabel = $scope.qiConfig.discountLabel;
			$scope.invoice.discountPercent = $scope.qiConfig.discountPercent;
			$scope.invoice.taxLabel = $scope.qiConfig.taxlabel;
			$scope.invoice.taxPercent = $scope.qiConfig.taxPercent;
			$scope.invoice.additionalTaxLabel = $scope.qiConfig.additionalTaxLabel;
			$scope.invoice.invoiceAdditional = $scope.qiConfig.additionalTaxPercent;
            $scope.invoice.invoiceCreatedBy = sessionStorage.getItem('firstName') + " " + sessionStorage.getItem('lastName');
        }

    }

    $scope.rearrangeItem = function () {
        var index = 1;
        var tempitems = $scope.items;
        $scope.items = [];
        angular.forEach(tempitems, function (d) {
            if (d.totalCost) {
                d.slno = index;
                index = index + 1;
                $scope.items.push(d);
            }
        });
    };

    $scope.getInvoiceProduct = function () {
        var resprom = $http.get(Data.serviceBase() + '/invoice/invoiceProductsWithLocaleModify', {params: {"organizationId": $scope.organizationId, "invoiceId": $scope.invoice.id}});

        resprom.then(function (response) {
            if (response.data) {

                $scope.items = response.data;
                $scope.rearrangeItem();
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
    if ($scope.invoice.id === undefined) {
        $scope.title = 'Create Invoice';
        $scope.btntxt = 'Save';
        console.log($scope.orgCurrency);
        $scope.invoice.invoiceCurrency = $scope.qiConfig.iCurrency;
    } else {
        $scope.title = 'Update Invoice';
        $scope.btntxt = 'Save';
        $scope.leadDet.name = $scope.invoice.leadName;
        $scope.leadDet.visitorOrganizationName = $scope.invoice.leadOrgName;
        $scope.leadDet.address1 = $scope.invoice.leadAddress1;
        $scope.leadDet.address2 = $scope.invoice.leadAddress2;
        $scope.leadDet.email = $scope.invoice.leadEmail;
        $scope.leadDet.mobile = $scope.invoice.leadMobile;
        $scope.leadDet.others = $scope.invoice.leadTaxCode;
        $scope.leadDet.city = $scope.invoice.leadCity;
        $scope.leadDet.state = $scope.invoice.leadState;
        $scope.leadDet.country = $scope.invoice.leadCountry;
        $scope.leadDet.zip = $scope.invoice.leadZip;
        $scope.getInvoiceProduct();
    }
    $scope.getOrgDetail = function () {
        var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

        resprom.then(function (response) {
            if (response.data) {
                $scope.orgDet = response.data[0];
                $scope.locale = response.data[0].countryCode;

            }

            //Additional Amount
            if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
            		if ($scope.invoice.invoiceAdditional == null || $scope.invoice.invoiceAdditional == '') {
            			$scope.invoice.invoiceAdditional = '0';
            		}
            		$scope.addI = $scope.invoice.invoiceAdditional;
            		$scope.addComma = $scope.addI.replace(/,/g,'');
            		$scope.addFI = parseFloat($scope.addComma).toFixed(2);
	             } else {
	             	if ($scope.invoice.invoiceAdditional == null || $scope.invoice.invoiceAdditional == '') {
            			$scope.invoice.invoiceAdditional = '0';
            		}
	              	$scope.addI = $scope.invoice.invoiceAdditional;
	              	$scope.addDot = $scope.addI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.addComma = $scope.addDot.replace(/,/g,'');
					$scope.addFI = parseFloat($scope.addComma).toFixed(2);
	             }
	            $scope.addPer = $scope.addFI;
                $scope.invoice.iA = $scope.addPer;
                //End Additional Amount

                //Tax Percentage
                if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
                	if ($scope.invoice.taxPercent == null || $scope.invoice.taxPercent == '') {
            			$scope.invoice.taxPercent = '0';
            		}
            		$scope.taxI = $scope.invoice.taxPercent;
            		$scope.taxComma = $scope.taxI.replace(/,/g,'');
            		$scope.taxFI = parseFloat($scope.taxComma).toFixed(2);
	             } else {
	             	if ($scope.invoice.taxPercent == null || $scope.invoice.taxPercent == '') {
            			$scope.invoice.taxPercent = '0';
            		}
	              	$scope.taxI = $scope.invoice.taxPercent;
	              	$scope.taxDot = $scope.taxI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.taxComma = $scope.taxDot.replace(/,/g,'');
					$scope.taxFI = parseFloat($scope.taxComma).toFixed(2);
	             }
	            $scope.taxPer = $scope.taxFI;
                $scope.invoice.tP = $scope.taxPer;
                //End Tax Percentage

                //Discount Percentage
            	if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
            		if ($scope.invoice.discountPercent == null || $scope.invoice.discountPercent == '') {
            			$scope.invoice.discountPercent = '0';
            		}
            		$scope.disI = $scope.invoice.discountPercent;
            		$scope.disComma = $scope.disI.replace(/,/g,'');
            		$scope.disFI = parseFloat($scope.disComma).toFixed(2);
	             } else {
	             	if ($scope.invoice.discountPercent == null || $scope.invoice.discountPercent == '') {
            			$scope.invoice.discountPercent = '0';
            		}
	              	$scope.disxI = $scope.invoice.discountPercent;
	              	$scope.disDot = $scope.disxI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.disComma = $scope.disDot.replace(/,/g,'');
					$scope.disFI = parseFloat($scope.disComma).toFixed(2);
	             }
	            $scope.disPer = $scope.disFI;
                $scope.invoice.dP = $scope.disPer;
                //End Discount Percentage


        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };

    $scope.openOrgDetail = function () {
        window.open(Data.webAppBase() + "account/company", '_blank');
    };

    $scope.setVisitor = function (selected) {
        $scope.leadDet = selected.originalObject;
        $scope.invoice.leadName = $scope.leadDet.name;
        $scope.invoice.leadOrgName = $scope.leadDet.visitorOrganizationName;
        $scope.invoice.visitorId = $scope.leadDet.id;
        $scope.invoice.leadTaxCode = $scope.leadDet.others;
        $scope.invoice.leadAddress1 = $scope.leadDet.address1;
        $scope.invoice.leadAddress2 = $scope.leadDet.address2;
        $scope.invoice.leadCity = $scope.leadDet.city;
        $scope.invoice.leadState = $scope.leadDet.state;
        $scope.invoice.leadCountry = $scope.leadDet.country;
        $scope.invoice.leadZip = $scope.leadDet.zip;
        $scope.invoice.leadEmail = $scope.leadDet.email;
        $scope.invoice.leadMobile = $scope.leadDet.mobile;


    };



    $scope.getInvoiceItem = function (p, size) {

        var modalInstance = $uibModal.open({
            templateUrl: './addItem',
            controller: 'addInvoiceItemCtrl',
            size: 'md',
            backdrop: 'static',
            resolve: {
                item: function () {
                    return p;
                },
                invoice: function () {
                    return $scope.invoice;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

                if (i.slno !== undefined) {
                    angular.forEach($scope.items, function (d) {
                        if (d.slno === i.slno) {
                            $scope.items[i.slno - 1] = i;
                        }
                    });

                } else {
                    var index = 1;
                    angular.forEach($scope.items, function (d) {
                        if (d.totalCost) {
                            index = index + 1;
                        }
                    });
                    i.slno = index;
                    $scope.items.push(i);
                }

                $scope.calculateInvoiceTotal();
            }
        });


    };

    $scope.deleteInvoiceItem = function (item) {

        $scope.items.splice(parseInt(item.slno) - 1, 1);

        $scope.rearrangeItem();
        $scope.calculateInvoiceTotal();

    };

    $scope.updateCompanylogoPic = function (e) {
        $scope.uppflag = 1;
        $scope.$apply(function () {


            //$scope.pfiles = e.files[0];
            if (parseInt(e.files[0]['size']) > 200000) {

                console.log("File size should not exceed 2MB");
                document.getElementById("cmlogo").value = "";
                toaster.pop("error", "", "File size should not exceed 2MB", 10000, 'trustedHtml');
                $scope.uppflag = 0;
                e.files.length = 0;

            } else if (parseInt(e.files[0]['size']) < 200000) {
                fileReader.readAsDataUrl(e.files[0], $scope)
                        .then(function (result) {
                            //console.log($scope.filecsv);
                            $scope.qiConfig.companyLogopic = result;
                            $scope.comlogo = result;
                            Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                                if (result.status !== 'error') {
                                    $scope.uppflag = 0;
                                    toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');
                                    document.getElementById("cmlogo").value = "";
                                } else {
                                    document.getElementById("cmlogo").value = "";
                                }
                            });

                        });


            }
        });
    };

    $scope.updateSignaturePict = function (e) {
        $scope.uppflag = 1;
        $scope.$apply(function () {


            //$scope.pfiles = e.files[0];
            if (parseInt(e.files[0]['size']) > 200000) {

                console.log("File size should not exceed 2MB");
                document.getElementById("signaturePic").value = "";
                toaster.pop("error", "", "File size should not exceed 2MB", 10000, 'trustedHtml');
                $scope.uppflag = 0;
                e.files.length = 0;

            } else if (parseInt(e.files[0]['size']) < 200000) {
                fileReader.readAsDataUrl(e.files[0], $scope)
                        .then(function (result) {
                            //console.log($scope.filecsv);
                            $scope.qiConfig.signatureImgpic = result;
                            $scope.signpict = result;
                            Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                                if (result.status !== 'error') {
                                    $scope.uppflag = 0;
                                    toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');
                                    document.getElementById("signaturePic").value = "";
                                } else {
                                    document.getElementById("signaturePic").value = "";
                                }
                            });

                        });


            }
        });
    };
    $scope.changeQiCurrency = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './changeCurrency',
            controller: 'changeCurrencyCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                currency: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.invoice.invoiceCurrency = i;
                $scope.qiConfig.iCurrency = i
                Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                    if (result.status !== 'error') {
                        $scope.uppflag = 0;
                        // toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');
                        $scope.calculateInvoiceTotal();

                    } else {

                    }
                });
            }
        });


    };
    //Change Discount Percentage Value
    $scope.changeQiDiscount = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './changeDiscount',
            controller: 'changeDiscountCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                discount: function () {
                    return p;
                },
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
            	if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
            		$scope.disI = i;
            		$scope.disComma = $scope.disI.replace(/,/g,'');
            		$scope.disFI = parseFloat($scope.disComma).toFixed(2);
	             } else {
	              	$scope.disxI = i;
	              	$scope.disDot = $scope.disxI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.disComma = $scope.disDot.replace(/,/g,'');
					$scope.disFI = parseFloat($scope.disComma).toFixed(2);
	             }
	            $scope.disPer = $scope.disFI;
                $scope.invoice.dP = $scope.disPer;
                //$scope.invoice.invoiceOffer = i;
                //$scope.qiConfig.discountPercent = i
                //Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                    //if (result.status !== 'error') {
                        $scope.uppflag = 0;
                        // toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');
                        $scope.calculateInvoiceTotal();

                    //} else {

                    //}
                //});
            }
        });
    };

    //Change Tax Percentage Value
    $scope.changeQiTax = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './changeTax',
            controller: 'changeTaxCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                tax: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
            	if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
            		$scope.taxI = i;
            		$scope.taxComma = $scope.taxI.replace(/,/g,'');
            		$scope.taxFI = parseFloat($scope.taxComma).toFixed(2);
	             } else {
	              	$scope.taxI = i;
	              	$scope.taxDot = $scope.taxI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.taxComma = $scope.taxDot.replace(/,/g,'');
					$scope.taxFI = parseFloat($scope.taxComma).toFixed(2);
	             }
	            $scope.taxPer = $scope.taxFI;
                $scope.invoice.tP = $scope.taxPer;
                //$scope.qiConfig.taxPercent = i
                //Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                   // if (result.status !== 'error') {
                        $scope.uppflag = 0;
                        $scope.calculateInvoiceTotal();
                    //} else {

                    //}
                //});
            }
        });
    };

    // $scope.changeQiTaxAmount = function (p, size) {
    //     var modalInstance = $uibModal.open({
    //         templateUrl: './changeTaxAmount',
    //         controller: 'changeTaxAmountCtrl',
    //         size: 'sm',
    //         backdrop: 'static',
    //         resolve: {
    //             taxAmount: function () {
    //                 return p;
    //             },
    //         }
    //     });
    //     modalInstance.result.then(function (i) {
    //         if (i) {
    //             //$scope.invoice.invoiceTax = i;
    //             $scope.qiConfig.taxPercent = i
    //             Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
    //                 if (result.status !== 'error') {
    //                     $scope.uppflag = 0;
    //                     // toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');
    //                     $scope.calculateInvoiceTotal();
    //                 } else {

    //                 }
    //             });
    //         }
    //     });


    // };

    //Change Discount Amount Value
    $scope.changeQiDiscountAmount = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './changeTaxAmount',
            controller: 'changeTaxAmountCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                taxAmount: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
   				$scope.percent = 0;
            	$scope.conI = 0;
            	$scope.convertSym = '';
            	$scope.setI = 0;
            	$scope.getI = 0;
            	$scope.Ival = 0;
            	if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
            		$scope.Ival = i;
            		$scope.convI = $scope.Ival.replace(/,/g,'');
            		$scope.conI = parseFloat($scope.convI).toFixed(2);
            		$scope.setST = $scope.invoice.invoiceSubTotal;
					$scope.getST = $scope.setST.replace(/,/g,'');
					$scope.getSuTotal = parseFloat($scope.getST).toFixed(2);
            	if ($scope.items.length > 0) {
	            angular.forEach($scope.items, function (d) {
	                if($scope.qiConfig.discountLabel || $scope.invoice.discountLabel) {
	                	$scope.per = (($scope.conI * 100) / $scope.getSuTotal);
	                }
	            });
               }
              } else {
              	$scope.conI = i;
              	$scope.convertSym = $scope.conI.replace(/(\.|,)/g, function(x) {
				    return x == ',' ? '.' : ',';
				});
				$scope.setI = $scope.convertSym.replace(/,/g,'');
				$scope.getI = parseFloat($scope.setI).toFixed(2);
				$scope.SubT = $scope.invoice.invoiceSubTotal;
				$scope.convertST = $scope.SubT.replace(/(\.|,)/g, function(x) {
				    return x == ',' ? '.' : ',';
				});
				$scope.SubTo = $scope.convertST.replace(/,/g,'');
				$scope.SubTot = parseFloat($scope.SubTo).toFixed(2);
              	if ($scope.items.length > 0) {
	            angular.forEach($scope.items, function (d) {
	                if($scope.qiConfig.discountLabel || $scope.invoice.discountLabel) {
	                	$scope.per = (($scope.getI * 100) / $scope.SubTot);
	                }
	            });
               }
             }
                $scope.discountPer = $scope.per;
                $scope.invoice.dP = $scope.discountPer;
            	//$scope.percent = 0;
            	//console.log(i);
            	//if ($scope.items.length > 0) {
	            //angular.forEach($scope.items, function (d) {
	                //if($scope.qiConfig.discountLabel) {
	                	//$scope.percent = ((i * 100) / $scope.quote.quoteSubTotal);
	                //}
	            //});
        //}
                $//scope.qiConfig.discountPercent = $scope.percent.toFixed(2);
                //Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                    //if (result.status !== 'error') {
                        $scope.uppflag = 0;
                        $scope.calculateInvoiceTotal();
                    //} else {

                    //}
                //});
            }
        });
    };

    //Change Additional Amount Value
    $scope.changeQiAdditionalAmount = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './changeAdditionalAmount',
            controller: 'changeAdditionalAmountCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                additionalAmount: function () {
                    return p;
                }
            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
            	if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
            		$scope.locale != 'fr-FR') {
            		$scope.addI = i;
            		$scope.addComma = $scope.addI.replace(/,/g,'');
            		$scope.addFI = parseFloat($scope.addComma).toFixed(2);
	             } else {
	              	$scope.addI = i;
	              	$scope.addDot = $scope.addI.replace(/(\.|,)/g, function(x) {
					    return x == ',' ? '.' : ',';
					});
					$scope.addComma = $scope.addDot.replace(/,/g,'');
					$scope.addFI = parseFloat($scope.addComma).toFixed(2);
	             }
	            $scope.addPer = $scope.addFI;
                $scope.invoice.iA = $scope.addPer;
                //$scope.quote.quoteTax = i;
                //$scope.qiConfig.additionalTaxPercent = i
                //Data.put('organization/qiConfig', $scope.qiConfig).then(function (result) {
                    //if (result.status !== 'error') {
                        $scope.uppflag = 0;
                        // toaster.pop("success", "", "Company logo updated successfully", 10000, 'trustedHtml');
                        $scope.calculateInvoiceTotal();
                    //} else {

                    //}
                //});
            }
        });
    };

    $scope.calculateInvoiceTotal = function () {
        $scope.invoice.invoiceSubTotal = '0';
        $scope.invoice.invoiceOffer = 0;
        $scope.invoice.invoiceNetTotal = 0;
        $scope.invoice.invoiceTax = 0;
        $scope.invoice.invoiceTotal = 0;
        $scope.invoice.invoiceNet = 0;
        $scope.invoice.invoiceTx = 0;
        $scope.invoice.invoiceAdd = 0;
        if ($scope.items.length > 0) {
            angular.forEach($scope.items, function (d) {
                if (d.totalCost) {
                	//Product Sub total
                		if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
		            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
		            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
		            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
		            		$scope.locale != 'fr-FR') {
		            		$scope.addI = d.totalCost;
		            		$scope.addComma = $scope.addI.replace(/,/g,'');
		            		$scope.addFI = parseFloat($scope.addComma).toFixed(2);
			             } else {
			              	$scope.addI = d.totalCost;
			              	$scope.addDot = $scope.addI.replace(/(\.|,)/g, function(x) {
							    return x == ',' ? '.' : ',';
							});
							$scope.addComma = $scope.addDot.replace(/,/g,'');
							$scope.addFI = parseFloat($scope.addComma).toFixed(2);
			             }
			            $scope.addPer = $scope.addFI;
		                $scope.ProductTotal = $scope.addPer;
                	//End Product Sub total

                	//Sub total
                		if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
		            		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
		            		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
		            		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
		            		$scope.locale != 'fr-FR') {
		            		$scope.addST = $scope.invoice.invoiceSubTotal;
		            		$scope.addCom = $scope.addST.replace(/,/g,'');
		            		$scope.addSTo = parseFloat($scope.addCom).toFixed(2);
			             } else {
			              	$scope.addST = $scope.invoice.invoiceSubTotal;
			              	$scope.addDots = $scope.addST.replace(/(\.|,)/g, function(x) {
							    return x == ',' ? '.' : ',';
							});
							$scope.addCom = $scope.addDots.replace(/,/g,'');
							$scope.addSTo = parseFloat($scope.addCom).toFixed(2);
			             }
			            $scope.addSubT = $scope.addSTo;
		                $scope.invoice.invoiceSubTotal = $scope.addSubT;
                	//Sub total
                    //console.log($scope.qiConfig);
                    $scope.invoice.invoiceSubTo = parseFloat($scope.invoice.invoiceSubTotal);
                    $scope.invoice.invoiceSub = parseFloat($scope.invoice.invoiceSubTo) + parseFloat($scope.ProductTotal);
                    $scope.invoice.invoiceSubTotal = $scope.invoice.invoiceSub.toLocaleString($scope.locale, {minimumFractionDigits: 2});
                    if ($scope.qiConfig.discountLabel || $scope.invoice.discountLabel) {
                    	$scope.invoice.invoiceOff = (($scope.invoice.invoiceSub * $scope.invoice.dP) / 100);
                        $scope.invoice.invoiceOffer =parseFloat( $scope.invoice.invoiceOff.toFixed(2)).toLocaleString($scope.locale, {minimumFractionDigits: 2});
                        $scope.invoice.invoiceNetT = $scope.invoice.invoiceSub - $scope.invoice.invoiceOff;
                        $scope.invoice.invoiceNetTotal = parseFloat($scope.invoice.invoiceNetT.toFixed(2)).toLocaleString($scope.locale, {minimumFractionDigits: 2});
						$scope.invoice.discountPer = parseFloat($scope.invoice.dP).toFixed(2);
						$scope.invoice.discountPercent = parseFloat($scope.invoice.discountPer).toLocaleString($scope.locale, {minimumFractionDigits: 2});
                        //$scope.invoice.invoiceOffer = (($scope.invoice.invoiceSubTotal * $scope.qiConfig.discountPercent) / 100);
      //                   console.log($scope.qiConfig.discountPercent);
      //                   $scope.invoice.invoiceNetTotal = $scope.invoice.invoiceSubTotal - $scope.invoice.invoiceOffer;
						// $scope.invoice.discountPercent = $scope.qiConfig.discountPercent;
                    } else {
                    	$scope.invoice.invoiceNetT = $scope.invoice.invoiceSub;
                        $scope.invoice.invoiceNetTotal = $scope.invoice.invoiceNetT.toLocaleString($scope.locale, {minimumFractionDigits: 2});
                        //$scope.invoice.invoiceNetTotal = $scope.invoice.invoiceSubTotal;
                    }
                    if ($scope.qiConfig.taxlabel || $scope.invoice.taxlabel) {
                    	$scope.invoice.invoiceTx = (($scope.invoice.invoiceNetT * $scope.invoice.tP) / 100);
                        $scope.invoice.invoiceTax = parseFloat($scope.invoice.invoiceTx.toFixed(2)).toLocaleString($scope.locale, {minimumFractionDigits: 2});
                        $scope.invoice.invoiceTo = parseFloat($scope.invoice.invoiceNetT) + parseFloat($scope.invoice.invoiceTx);
                        $scope.invoice.invoiceTotal = $scope.invoice.invoiceTo.toLocaleString($scope.locale, {minimumFractionDigits: 2});
						$scope.invoice.taxPer = parseFloat($scope.invoice.tP).toFixed(2);
						$scope.invoice.taxPercent = parseFloat($scope.invoice.taxPer).toLocaleString($scope.locale, {minimumFractionDigits: 2});
      //                   $scope.invoice.invoiceTax = (($scope.invoice.invoiceNetTotal * $scope.qiConfig.taxPercent) / 100);
      //                   $scope.invoice.invoiceTotal = $scope.invoice.invoiceNetTotal + $scope.invoice.invoiceTax;
						// $scope.invoice.taxPercent = $scope.qiConfig.taxPercent;
                    } 
                    // else {
                    //     $scope.invoice.invoiceTotal = $scope.invoice.invoiceNetTotal;
                    // }
                    if ($scope.qiConfig.additionalTaxLabel || $scope.invoice.additionalTaxLabel) {
                    	$scope.invoice.invoiceAdd = parseFloat($scope.invoice.iA);
                        $scope.invoice.invoiceAdditional = $scope.invoice.invoiceAdd.toLocaleString($scope.locale, {minimumFractionDigits: 2});
                        $scope.invoice.invoiceFinal = parseFloat($scope.invoice.invoiceNetT) + parseFloat($scope.invoice.invoiceTx) + parseFloat($scope.invoice.invoiceAdd);
                        $scope.invoice.invoiceTotal = parseFloat($scope.invoice.invoiceFinal.toFixed(2)).toLocaleString($scope.locale, {minimumFractionDigits: 2});
						// $scope.invoice.additionalTP = parseFloat($scope.invoice.invoiceAddi).toFixed(2);
						// $scope.invoice.invoiceAdditional = $scope.invoice.additionalTP.toLocaleString($scope.locale, {minimumFractionDigits: 2});
      //                   $scope.invoice.invoiceAdditional =  parseFloat($scope.qiConfig.additionalTaxPercent);           
      //                   $scope.invoice.invoiceTotal = $scope.invoice.invoiceNetTotal + $scope.invoice.invoiceTax + $scope.invoice.invoiceAdditional;
						// $scope.invoice.additionalTaxPercent = $scope.qiConfig.additionalTaxPercent;
                    } else {
                    	$scope.invoice.invoiceTota =  parseFloat($scope.invoice.invoiceNetT) + parseFloat($scope.invoice.invoiceTx) + parseFloat($scope.invoice.invoiceAdd);
                    	$scope.invoice.invoiceTotal = $scope.invoice.invoiceTota.toLocaleString($scope.locale, {minimumFractionDigits: 2});
                        //$scope.invoice.invoiceTotal = $scope.invoice.invoiceNetTotal + $scope.invoice.invoiceTax;
                    }

                }
            });
        }


    };
    $scope.openQiConfig = function () {
        window.open(Data.webAppBase() + "account/quotesinvoice#settings", '_blank');
    };

    $scope.updateInvoice = function (invoice) {
        invoice.organizationId = $scope.organizationId;
        invoice.invoiceUserId = sessionStorage.getItem('userId');
        invoice.items = JSON.stringify($scope.items);
        if (invoice.id === undefined) {
            Data.post('invoice/invoice', invoice).then(function (result) {
                if (result.status !== 'error') {

                    toaster.pop("success", "", result.message, 10000, 'trustedHtml');
                    window.location.href = Data.webAppBase() + 'account/quotesinvoice';
                    $uibModalInstance.close('1');

                } else {
                    toaster.pop("error", "", result.message, 10000, 'trustedHtml');
                }
            });
        } else {
            Data.put('invoice/invoice', invoice).then(function (result) {
                if (result.status !== 'error') {

                    toaster.pop("success", "", result.message, 10000, 'trustedHtml');
                    window.location.href = Data.webAppBase() + 'account/quotesinvoice';
                    $uibModalInstance.close('1');

                } else {
                    toaster.pop("error", "", result.message, 10000, 'trustedHtml');
                }
            });
        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('deleteInvoiceCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, $uibModalInstance, invoice, $http, toaster)
{
    $scope.invoice = angular.copy(invoice);

    $scope.deleteInvoice = function (invoice) {
        Data.delete('invoice/deleteInvoice/' + invoice.id).then(function (result) {
            if (result.status !== 'error') {

                toaster.pop("success", "", result.message, 10000, 'trustedHtml');
                $uibModalInstance.close('1');

            } else {
                $uibModalInstance.close('1');
                toaster.pop("error", "", result.message, 10000, 'trustedHtml');
            }
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});


evtApp.controller('addItemCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, $uibModalInstance, item, quote, $http, toaster)
{
    $scope.pOrgId = sessionStorage.getItem('orgId');
    $scope.item = angular.copy(item);
    $scope.quote = angular.copy(quote);
    $scope.product = {};
    $scope.currency = $scope.quote.quoteCurrency;
    var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.pOrgId}});

        resprom.then(function (response) {
            if (response.data) {
                $scope.locale = response.data[0].countryCode;
            }
        });
    $scope.setProduct = function (selected) {
        $scope.productDet = selected.originalObject;
        $scope.item = selected.originalObject;
    };
    $scope.addProductItem = function (item) {
    	if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
    		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
    		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
    		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
    		$scope.locale != 'fr-FR') {
    		$scope.disI = $scope.item.price;
    		$scope.disComma = $scope.disI.replace(/,/g,'');
    		$scope.item.price = parseFloat($scope.disComma).toFixed(2);
         } else {
          	$scope.disxI = $scope.item.price;
          	$scope.disDot = $scope.disxI.replace(/(\.|,)/g, function(x) {
			    return x == ',' ? '.' : ',';
			});
			$scope.disComma = $scope.disDot.replace(/,/g,'');
			$scope.item.price = parseFloat($scope.disComma).toFixed(2);
         }
    	$scope.total = parseFloat($scope.item.price) * parseInt(item.quantity);
    	item.price = parseFloat($scope.item.price).toLocaleString($scope.locale, {minimumFractionDigits: 2});
    	//item.quantity = parseFloat($scope.item.quantity).toLocaleString($scope.locale, {minimumFractionDigits: 2});
        item.totalCost = parseFloat($scope.total).toLocaleString($scope.locale, {minimumFractionDigits: 2});
        $uibModalInstance.close(item);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('addInvoiceItemCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, $uibModalInstance, item, invoice, $http, toaster)
{
    $scope.pOrgId = sessionStorage.getItem('orgId');
    $scope.item = angular.copy(item);
    $scope.invoice = angular.copy(invoice);
    $scope.product = {};
    $scope.currency = $scope.invoice.invoiceCurrency;
    var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.pOrgId}});
        resprom.then(function (response) {
            if (response.data) {
                $scope.locale = response.data[0].countryCode;
            }
    });
    $scope.setProduct = function (selected) {
        $scope.productDet = selected.originalObject;
        $scope.item = selected.originalObject;
    };
    $scope.addProductItem = function (item) {
    	if ($scope.locale != 'pt-BR' && $scope.locale != 'zu-ZA' && $scope.locale != 'id-ID' && 
    		$scope.locale != 'it-IT' && $scope.locale != 'de-DE' &&	$scope.locale != 'ru-RU' &&
    		$scope.locale != 'tr-TR' && $scope.locale != 'es-ES' && $scope.locale != 'es-CO' && 
    		$scope.locale != 'pt-PT' &&	$scope.locale != 'vi-VN' && $scope.locale != 'fr-FR' &&
    		$scope.locale != 'fr-FR') {
    		$scope.disI = $scope.item.price;
    		$scope.disComma = $scope.disI.replace(/,/g,'');
    		$scope.item.price = parseFloat($scope.disComma).toFixed(2);
         } else {
          	$scope.disxI = $scope.item.price;
          	$scope.disDot = $scope.disxI.replace(/(\.|,)/g, function(x) {
			    return x == ',' ? '.' : ',';
			});
			$scope.disComma = $scope.disDot.replace(/,/g,'');
			$scope.item.price = parseFloat($scope.disComma).toFixed(2);
         }
    	$scope.total = parseFloat($scope.item.price) * parseInt(item.quantity);
    	item.price = parseFloat($scope.item.price).toLocaleString($scope.locale, {minimumFractionDigits: 2});
    	//item.quantity = parseFloat($scope.item.quantity).toLocaleString($scope.locale, {minimumFractionDigits: 2});
        item.totalCost = parseFloat($scope.total).toLocaleString($scope.locale, {minimumFractionDigits: 2});
        $uibModalInstance.close(item);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('changeCurrencyCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, currency, $uibModalInstance, $http, toaster)
{
    $scope.currency = angular.copy(currency);

    $scope.addCurrency = function (currency) {


        $uibModalInstance.close(currency);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});


evtApp.controller('changeDiscountCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, discount, $uibModalInstance, $http, toaster)
{
    $scope.discount = angular.copy(discount);

    $scope.addDiscount = function (discount) {


        $uibModalInstance.close(discount);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('changeTaxAmountCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, taxAmount, $uibModalInstance, $http, toaster)
{
    $scope.taxAmount = angular.copy(taxAmount);

    $scope.addTaxAmount = function (taxAmount) {


        $uibModalInstance.close(taxAmount);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('changeAdditionalAmountCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, additionalAmount, $uibModalInstance, $http, toaster)
{
    $scope.additionalAmount = angular.copy(additionalAmount);

    $scope.addAdditionalAmount = function (additionalAmount) {


        $uibModalInstance.close(additionalAmount);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
evtApp.controller('changeTaxCtrl', function ($scope, $rootScope, alertUser, checkPremium, trialPopUp, fileReader, Data, $uibModal, tax, $uibModalInstance, $http, toaster)
{
    $scope.tax = angular.copy(tax);

    $scope.addTaxPercent = function (tax) {


        $uibModalInstance.close(tax);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});
