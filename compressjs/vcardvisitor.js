//---VV--- Bcard Visitor detail update module ----- VV -----
evtApp.controller('vcardvisitorsCtrl', function ($scope, $timeout, Data, $uibModal, toaster, $http, fileReader, $rootScope, $window) {

    $scope.visitorvalue = {};
    $scope.visitor = {};
    $rootScope.totstats = '0';
    $rootScope.totnewstats = '0';
    $scope.valueflag = true;
    $scope.btntxt = "";
    $scope.imageSrc = '';
    $rootScope.firstName = sessionStorage.getItem("firstName");
    $rootScope.lastName = sessionStorage.getItem("lastName");
    $rootScope.type = sessionStorage.getItem('userType');
    $rootScope.level = sessionStorage.getItem('userLevel');
    var chaeckStatus = function () {
        if (sessionStorage.getItem('userType') === "regular") {
            // Based on five minutes once request will be sent and find the count
            console.log("Regular");
            Data.get('visitor/vcardqueuestatsdetNew').then(function (result) {
                $rootScope.totnewstats = result.total;
            });
        } else {
            console.log("QC");
            Data.get('visitor/vcardqueuestatsdetQc').then(function (result) {
                // console.log(result);
                $rootScope.totstats = result.total;
            });
        }
       // $timeout(chaeckStatus, 100000);
    };


    $rootScope.makeRefresh = function () {
        $window.location.reload();
    };
    chaeckStatus();
    $scope.switchTab = function (tabId) {
        if (tabId === 1) {
            $scope.tab = 1;
            angular.element(document.getElementById("tab-3")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
        } else if (tabId === 2) {
            $scope.tab = 2;
            angular.element(document.getElementById("tab-4")).addClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
        } else {
            $scope.tab = 3;
            angular.element(document.getElementById("tab-5")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
        }

    };

    $scope.verifyEmail = function (visitor) {

        $scope.verifydata = {
            "orgId": visitor.organizationId,
            "emailId": visitor.email,
            "visitorId": visitor.id
        };

        if (visitor.organizationId && visitor.email && visitor.id) {
            Data.post('visitor/verifyEmail', $scope.verifydata).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    //$window.location.reload();
                    //$scope.visitor = {};
                    $scope.visitorvalue = {};
                    $scope.visitorvalue.leadnotes = "";
                    $scope.visitorvalue.orgnotes = "";
                    $scope.visitorvalue.linkedin = "";
                    $scope.visitorvalue.url1 = "";
                    $scope.visitorvalue.url2 = "";
                    $scope.visitorvalue.url3 = "";
                    console.log(result.visitorValAdd['leadnotes']);
                    $scope.visitor.firstName = result.visitor.firstName;
                    $scope.visitor.lastName = result.visitor.lastName;
                    $scope.visitor.visitorOrganizationName = result.visitor.visitorOrganizationName;
                    $scope.visitor.designation = result.visitor.designation;
                    $scope.visitor.email = result.visitor.email;
                    $scope.visitor.alternateEmail = result.visitor.alternateEmail;
                    $scope.visitor.mobile = result.visitor.mobile;
                    $scope.visitor.website = result.visitor.website;
                    $scope.visitor.phone = result.visitor.phone;
                    $scope.visitor.alternatePhone = result.visitor.alternatePhone;
                    $scope.visitor.fax = result.visitor.fax;
                    $scope.visitor.address1 = result.visitor.address1;
                    $scope.visitor.address2 = result.visitor.address2;
                    $scope.visitor.photourl = result.visitor.photourl;
                    $scope.visitor.city = result.visitor.city;
                    $scope.visitor.state = result.visitor.state;
                    $scope.visitor.country = result.visitor.country;
                    $scope.visitor.zip = result.visitor.zip;
                    $scope.visitorvalue.leadnotes = result.visitorValAdd['leadnotes'];
                    $scope.visitorvalue.orgnotes = result.visitorValAdd['orgnotes'];
                    $scope.visitorvalue.linkedin = result.visitorValAdd['linkedin'];
                    $scope.visitorvalue.url1 = result.visitorValAdd['url1'];
                    $scope.visitorvalue.url2 = result.visitorValAdd['url2'];
                    $scope.visitorvalue.url3 = result.visitorValAdd['url3'];

                } else {
                    console.log(result);
                }
            });
        }
    };
    $scope.skipemail = function () {

        $scope.skipdata = {
            "userId": $scope.visitor.capturedUserId,
            "visitorId": $scope.visitor.id
        };

        Data.post('visitor/skipEmail', $scope.skipdata).then(function (result) {
            if (result.status !== 'error') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                $window.location.reload();
            } else {
                console.log(result);
            }
        });
    };

    $scope.rejectemail = function () {

        $scope.rejectdata = {
            "userId": $scope.visitor.capturedUserId,
            "eventName": $scope.visitor.eventName,
            "visitorId": $scope.visitor.id
        };

        Data.post('visitor/vcardrejectionmail', $scope.rejectdata).then(function (result) {
            if (result.status !== 'error') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                $window.location.reload();
            } else {
                console.log(result);
            }
        });
    };

    $scope.getFile = function () {
        $scope.progress = 0;
        fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    console.log($scope.file);

                    $scope.imageSrc = result;
                });
    };


    var userId = sessionStorage.getItem('userId');
    $scope.userType = sessionStorage.getItem('userType');
    var businesscardurl = "";


    if (sessionStorage.getItem('userType') === "regular") {
        businesscardurl = "visitor/bcardvisitordetail";
    } else {
        businesscardurl = "visitor/bcardVisitorQcDetail";
    }

    var resprom = $http.get(Data.serviceBase() + businesscardurl, {params: {'userId': userId}});
    resprom.then(function (response) {
        if (sessionStorage.getItem('userType') === "regular") {

            console.log(response);
            $scope.visitor = response.data;
            console.log('got visitor data to transcribe ');
            console.log($scope.visitor);
            $scope.btntxt = "Save";
            if ($scope.visitor.length <= 0) {
                toaster.pop("error", "", "No more business cards are available", 10000, 'trustedHtml');
            }
        } else {
            $scope.btntxt = "Verified";
            $scope.visitor = response.data.visitors;
            $scope.visitorvalue = response.data.visitorsvalue[0];
            if ($scope.visitor.length <= 0) {
                toaster.pop("error", "", "No more business cards are available", 10000, 'trustedHtml');
            }
            // if($rootScope.totstats > 0 && response.data.visitors.length <=0 ){
            //     toaster.pop("error", "", "Business card are still in DC Queue", 10000, 'trustedHtml');
            // }

        }
        if ($scope.visitor['valueAddInfo'] === '1') {
            $scope.valueflag = true;
        } else {
            $scope.valueflag = false;
        }

    }, function (response) {
        console.log('Error occured -- ');
        console.log(response);
    });
    $scope.angle = 0;

    $scope.rotate = function (angle) {
        $scope.angle = $scope.angle + angle;
        console.log($scope.angle);
    };


    $scope.tab = 1;
    angular.element(document.getElementById("tab-3")).addClass("active");
    angular.element(document.getElementById("tab-4")).removeClass("active");
    angular.element(document.getElementById("tab-5")).removeClass("active");
    $scope.Continuetonext = function () {
        $scope.tab = 2;
        angular.element(document.getElementById("tab-4")).addClass("active");
        angular.element(document.getElementById("tab-3")).removeClass("active");
        angular.element(document.getElementById("tab-5")).removeClass("active");
        console.log($scope.tab);

    };
    $scope.Continuetonext1 = function () {
        $scope.tab = 3;
        angular.element(document.getElementById("tab-5")).addClass("active");
        angular.element(document.getElementById("tab-3")).removeClass("active");
        angular.element(document.getElementById("tab-4")).removeClass("active");
        console.log($scope.tab);

    };



    $scope.confvisitor = function (p, q, size) {

        /*var modalInstance1 = $uibModal.open({
            templateUrl: './confvisitor',
            controller: 'confvisitorCtrl',
            size: size,
            resolve: {
                visitor: function () {
                    return p;
                },
                visitorvalue: function () {
                    return q;
                },
                imgsrc: function () {
                    return $scope.imageSrc;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            $window.location.reload();
        });*/
		$window.location.reload();
    };
    $rootScope.vcardqueuestats = function (size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './vcardqueuestats',
            controller: 'vcardqueuestatsCtrl',
            size: 'lg',
            resolve: {
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };
});

evtApp.controller('confvisitorCtrl', function ($scope, Data, $uibModalInstance, visitor, visitorvalue, imgsrc, toaster) {
    $scope.visitor = visitor;
    $scope.imageSrc = imgsrc;
    $scope.visitorvalue = visitorvalue;
    $scope.title = "Confirmation";
    $scope.userType = "";
    if (sessionStorage.getItem('userType') === "regular") {
        $scope.userType = "DC";

    } else {

        $scope.userType = "QC";
    }
    $scope.updatevisitorvalue = function (visitor, visitorvalue) {
        //console.log(visitorvalue);
        if (visitor.id !== undefined) {
            visitor.userType = $scope.userType;

            if ($scope.imageSrc) {
                visitor.profilephotourl = $scope.imageSrc;
            }
            if (visitorvalue !== undefined) {
                visitor.leadnotes = visitorvalue.leadnotes;
                visitor.orgnotes = visitorvalue.orgnotes;
                visitor.linkedin = visitorvalue.linkedin;
                visitor.url1 = visitorvalue.url1;
                visitor.url2 = visitorvalue.url2;
                visitor.url3 = visitorvalue.url3;
                visitor.bcardstatus = $scope.userType;
            }
            Data.put('visitor/leadzwalletvisitor', visitor).then(function (result) {
                if (result.status !== 'error') {

                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    $uibModalInstance.close(1);
                } else {
                    console.log(result);
                }
            });
        }
    }


    /*if (visitorvalue.id !== undefined) {
     visitorvalue.visitorId = $scope.visitor.id;
     visitorvalue.photourl = $scope.imageSrc;
     
     Data.put('visitor/visitorvalueadd', visitorvalue).then(function (result) {
     if (result.status !== 'error') {
     
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     if (sessionStorage.getItem('userType') === "regular") {
     $scope.updatbusinesscardsts("DC");
     
     } else {
     
     $scope.updatbusinesscardsts("QC");
     }
     $uibModalInstance.close(1);
     
     } else {
     console.log(result);
     }
     });
     } else {
     visitorvalue.visitorId = $scope.visitor.id;
     visitorvalue.photourl = $scope.imageSrc;
     
     Data.post('visitor/visitorvalueadd', visitorvalue).then(function (result) {
     if (result.status !== 'error') {
     
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     if (sessionStorage.getItem('userType') === "regular") {
     $scope.updatbusinesscardsts("DC");
     
     } else {
     
     $scope.updatbusinesscardsts("QC");
     }
     
     
     } else {
     console.log(result);
     }
     });
     }
     
     };
     $scope.updatbusinesscardsts = function (status) {
     $scope.bcard = {};
     $scope.bcard.cardId = $scope.visitor.cardId;
     $scope.bcard.status = status;
     console.log(status);
     Data.put('visitor/businesscard', $scope.bcard).then(function (result) {
     if (result.status !== 'error') {
     
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     $uibModalInstance.close(1);
     } else {
     console.log(result);
     }
     });
     };*/
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

evtApp.controller('vcardqueuestatsCtrl', function ($scope, Data, $uibModalInstance, toaster) {

    $scope.title = "Visitor Card Queue stats";
    $scope.stats = {};
    Data.get('visitor/vcardqueuestatsdet').then(function (result) {
        $scope.stats = result;
        console.log(result);
    });
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

evtApp.controller('vcardqueueorgstatsCtrl', function ($scope, Data, $uibModalInstance, toaster) {

    $scope.title = "Visitor Card Queue stats";
    $scope.orgst = {};
    Data.get('visitor/vcardqueueSstatsdetNew').then(function (result) {
        $scope.orgst = result.orgdet;
        console.log(result.orgdet);
    });
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

evtApp.controller('vcardselectCtrl', function ($scope, $timeout, Data, $uibModal, toaster, $http, fileReader, $rootScope, $window) {

    $scope.visitorvalue = {};
    $scope.visitor = {};
    $rootScope.totstats = '0';
    $rootScope.totnewstats = '0';
    $scope.valueflag = true;
    $scope.btntxt = "Verified";
    $scope.imageSrc = '';
    $rootScope.firstName = sessionStorage.getItem("firstName");
    $rootScope.lastName = sessionStorage.getItem("lastName");
    $rootScope.type = sessionStorage.getItem('userType');

    var chaeckStatus = function () {
        if (sessionStorage.getItem('userType') === "supervisor") {
            // Based on five minutes once request will be sent and find the count
            console.log("Regular");
            Data.get('visitor/vcardqueuestatsdetNew').then(function (result) {
                $rootScope.totnewstats = result.total;
            });
        } else {
            console.log("QC");
            Data.get('visitor/vcardqueuestatsdetQc').then(function (result) {
                // console.log(result);
                $rootScope.totstats = result.total;
            });
        }
       // $timeout(chaeckStatus, 9000);
    };
    $rootScope.makeRefresh = function () {
        $window.location.reload();
    };
    chaeckStatus();

    $scope.$on('onBeforeUnload', function (e, confirmation) {
        confirmation.message = "All data will be lost.";
        e.preventDefault();
    });
    $scope.$on('onUnload', function (e) {
        console.log('leaving page'); // Use 'Preserve Log' option in Console
        Data.get('account/lwlogout').then(function (results) {
            Data.toast(results);
            sessionStorage.setItem("userId", '');
            sessionStorage.setItem("firstName", '');
            sessionStorage.setItem("lastName", '');
            sessionStorage.setItem("token", '');
            sessionStorage.setItem("userEmail", "");
            sessionStorage.setItem("cFirstName", '');
            sessionStorage.setItem("cLastName", '');
            sessionStorage.setItem("userType", '');
            sessionStorage.setItem("menuact", '');
        });
    });
    $scope.switchTab = function (tabId) {
        if (tabId === 1) {
            $scope.tab = 1;
            angular.element(document.getElementById("tab-3")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
        } else if (tabId === 2) {
            $scope.tab = 2;
            angular.element(document.getElementById("tab-4")).addClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
        } else {
            $scope.tab = 3;
            angular.element(document.getElementById("tab-5")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
        }

    };

    $scope.verifyEmail = function (visitor) {

        $scope.verifydata = {
            "orgId": visitor.organizationId,
            "emailId": visitor.email,
            "visitorId": visitor.id
        };

        if (visitor.organizationId && visitor.email && visitor.id) {
            Data.post('visitor/verifyEmail', $scope.verifydata).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    //$window.location.reload();
                    //$scope.visitor = {};
                    $scope.visitorvalue = {};
                    $scope.visitorvalue.leadnotes = "";
                    $scope.visitorvalue.orgnotes = "";
                    $scope.visitorvalue.linkedin = "";
                    $scope.visitorvalue.url1 = "";
                    $scope.visitorvalue.url2 = "";
                    $scope.visitorvalue.url3 = "";
                    console.log(result.visitorValAdd['leadnotes']);
                    $scope.visitor.firstName = result.visitor.firstName;
                    $scope.visitor.lastName = result.visitor.lastName;
                    $scope.visitor.visitorOrganizationName = result.visitor.visitorOrganizationName;
                    $scope.visitor.designation = result.visitor.designation;
                    $scope.visitor.email = result.visitor.email;
                    $scope.visitor.alternateEmail = result.visitor.alternateEmail;
                    $scope.visitor.mobile = result.visitor.mobile;
                    $scope.visitor.website = result.visitor.website;
                    $scope.visitor.phone = result.visitor.phone;
                    $scope.visitor.alternatePhone = result.visitor.alternatePhone;
                    $scope.visitor.fax = result.visitor.fax;
                    $scope.visitor.address1 = result.visitor.address1;
                    $scope.visitor.address2 = result.visitor.address2;
                    $scope.visitor.photourl = result.visitor.photourl;
                    $scope.visitor.city = result.visitor.city;
                    $scope.visitor.state = result.visitor.state;
                    $scope.visitor.country = result.visitor.country;
                    $scope.visitor.zip = result.visitor.zip;
                    $scope.visitorvalue.leadnotes = result.visitorValAdd['leadnotes'];
                    $scope.visitorvalue.orgnotes = result.visitorValAdd['orgnotes'];
                    $scope.visitorvalue.linkedin = result.visitorValAdd['linkedin'];
                    $scope.visitorvalue.url1 = result.visitorValAdd['url1'];
                    $scope.visitorvalue.url2 = result.visitorValAdd['url2'];
                    $scope.visitorvalue.url3 = result.visitorValAdd['url3'];

                } else {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    console.log(result);
                }
            });
        }
    };
    $scope.skipemail = function () {

        $scope.skipdata = {
            "userId": $scope.visitor.capturedUserId,
            "visitorId": $scope.visitor.id
        };

        Data.post('visitor/skipEmail', $scope.skipdata).then(function (result) {
            if (result.status !== 'error') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                //$window.location.reload();
                $scope.gettingDetails();
            } else {
                console.log(result);
            }
        });
    };

    $scope.rejectemail = function () {

        $scope.rejectdata = {
            "userId": $scope.visitor.capturedUserId,
            "eventName": $scope.visitor.eventName,
            "visitorId": $scope.visitor.id
        };

        Data.post('visitor/vcardrejectionmail', $scope.rejectdata).then(function (result) {
            if (result.status !== 'error') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                //$window.location.reload();
                $scope.gettingDetails();
            } else {
                console.log(result);
            }
        });
    };

    $scope.getFile = function () {
        $scope.progress = 0;
        fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    console.log($scope.file);

                    $scope.imageSrc = result;
                });
    };

   // $scope.selectEvent();
    var userId = sessionStorage.getItem('userId');
    $scope.userType = sessionStorage.getItem('userType');
    var businesscardurl = "";
    $scope.organizationID = 0;
    $scope.eventID = 0;
    $scope.organizations = {};
    $scope.events = {};
    var resprom = $http.get(Data.serviceBase() + '/admin/allorgsactive');
    resprom.then(function (response) {
        $scope.organizations = response.data;
        if ($scope.organizations.length > 0) {
            $scope.organizationID = $scope.organizations[0]['id'];
           
        }
    }, function (response) {
        console.log('Error occured -- ');
        console.log(response);
    });

    $scope.selectEvent = function () {
        var resprom = $http.get(Data.serviceBase() + '/admin/eventGetDetails', {params: {'key': "organizationId", "value": $scope.organizationID}});
        resprom.then(function (response) {
            $scope.events = response.data;
            if ($scope.events.length > 0) {
                $scope.eventID = $scope.events[0]['id'];
            }
            //$scope.gettingDetails();
        }, function (response) {
            console.log('Error occured -- ');
            console.log(response);
        });
    };

    $scope.gettingDetails = function () {
        console.log("EventID " + $scope.eventID);
		var vid = '0';
		if($scope.visitor.length <= 0){
			vid = '0';
		}else{
			vid = $scope.visitor.id;
		}
        var resprom = $http.get(Data.serviceBase() + "visitor/bcardVisitorInd", {params: {'organizationId': $scope.organizationID, 'eventId': $scope.eventID,'visitorId': vid}});
        resprom.then(function (response) {

            $scope.btntxt = "Verified";
            $scope.visitor = response.data.visitors;
            $scope.visitorvalue = response.data.visitorsvalue[0];
            if ($scope.visitor.length <= 0) {
                toaster.pop("error", "", "No more business cards are available", 10000, 'trustedHtml');
            }
            // if($rootScope.totstats > 0 && response.data.visitors.length <=0 ){
            //     toaster.pop("error", "", "Business card are still in DC Queue", 10000, 'trustedHtml');
            // }

            if ($scope.visitor['valueAddInfo'] === '1') {
                $scope.valueflag = true;
            } else {
                $scope.valueflag = false;
            }


        }, function (response) {
            console.log('Error occured -- ');
            console.log(response);
        });
    };
    $scope.angle = 0;

    $scope.rotate = function (angle) {
        $scope.angle = $scope.angle + angle;
        console.log($scope.angle);
    };


    $scope.tab = 1;
    angular.element(document.getElementById("tab-3")).addClass("active");
    angular.element(document.getElementById("tab-4")).removeClass("active");
    angular.element(document.getElementById("tab-5")).removeClass("active");
    $scope.Continuetonext = function () {
        $scope.tab = 2;
        angular.element(document.getElementById("tab-4")).addClass("active");
        angular.element(document.getElementById("tab-3")).removeClass("active");
        angular.element(document.getElementById("tab-5")).removeClass("active");
        console.log($scope.tab);

    };
    $scope.Continuetonext1 = function () {
        $scope.tab = 3;
        angular.element(document.getElementById("tab-5")).addClass("active");
        angular.element(document.getElementById("tab-3")).removeClass("active");
        angular.element(document.getElementById("tab-4")).removeClass("active");
        console.log($scope.tab);

    };



    $scope.confvisitor = function (p, q, size) {

       /* var modalInstance1 = $uibModal.open({
            templateUrl: './confvisitor',
            controller: 'confsingleCtrl',
            size: size,
            resolve: {
                visitor: function () {
                    return p;
                },
                visitorvalue: function () {
                    return q;
                },
                imgsrc: function () {
                    return $scope.imageSrc;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            //$window.location.reload();
            $scope.imageSrc = "";
            $scope.tab = 1;
            angular.element(document.getElementById("tab-3")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
            $scope.gettingDetails();

        });*/
			$scope.imageSrc = "";
            $scope.tab = 1;
            angular.element(document.getElementById("tab-3")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
            $scope.gettingDetails();
    };
    $rootScope.vcardqueuestats = function (size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './vcardqueuestats',
            controller: 'vcardqueuestatsCtrl',
            size: 'lg',
            resolve: {
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };
    $rootScope.vcardqueueorgstats = function (size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './vcardqueueorgstats',
            controller: 'vcardqueueorgstatsCtrl',
            size: 'lg',
            resolve: {
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };
});
evtApp.controller('confsingleCtrl', function ($scope, Data, $uibModalInstance, visitor, visitorvalue, imgsrc, toaster) {
    $scope.visitor = visitor;
    $scope.imageSrc = imgsrc;
    $scope.visitorvalue = visitorvalue;
    $scope.title = "Confirmation";
    $scope.userType = "QC";

    $scope.updatevisitorvalue = function (visitor, visitorvalue) {
        //console.log(visitorvalue);
        if (visitor.id !== undefined) {
            visitor.userType = $scope.userType;

            if ($scope.imageSrc) {
                visitor.profilephotourl = $scope.imageSrc;

            }
            if (visitorvalue !== undefined) {
                visitor.leadnotes = visitorvalue.leadnotes;
                visitor.orgnotes = visitorvalue.orgnotes;
                visitor.linkedin = visitorvalue.linkedin;
                visitor.url1 = visitorvalue.url1;
                visitor.url2 = visitorvalue.url2;
                visitor.url3 = visitorvalue.url3;
                visitor.bcardstatus = $scope.userType;
            }
            Data.put('visitor/leadzwalletvisitor', visitor).then(function (result) {
                if (result.status !== 'error') {

                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    $uibModalInstance.close(1);
                } else {
                    console.log(result);
                }
            });
        }
    }


    /*if (visitorvalue.id !== undefined) {
     visitorvalue.visitorId = $scope.visitor.id;
     visitorvalue.photourl = $scope.imageSrc;
     
     Data.put('visitor/visitorvalueadd', visitorvalue).then(function (result) {
     if (result.status !== 'error') {
     
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     if (sessionStorage.getItem('userType') === "regular") {
     $scope.updatbusinesscardsts("DC");
     
     } else {
     
     $scope.updatbusinesscardsts("QC");
     }
     $uibModalInstance.close(1);
     
     } else {
     console.log(result);
     }
     });
     } else {
     visitorvalue.visitorId = $scope.visitor.id;
     visitorvalue.photourl = $scope.imageSrc;
     
     Data.post('visitor/visitorvalueadd', visitorvalue).then(function (result) {
     if (result.status !== 'error') {
     
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     if (sessionStorage.getItem('userType') === "regular") {
     $scope.updatbusinesscardsts("DC");
     
     } else {
     
     $scope.updatbusinesscardsts("QC");
     }
     
     
     } else {
     console.log(result);
     }
     });
     }
     
     };
     $scope.updatbusinesscardsts = function (status) {
     $scope.bcard = {};
     $scope.bcard.cardId = $scope.visitor.cardId;
     $scope.bcard.status = status;
     console.log(status);
     Data.put('visitor/businesscard', $scope.bcard).then(function (result) {
     if (result.status !== 'error') {
     
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     $uibModalInstance.close(1);
     } else {
     console.log(result);
     }
     });
     };*/
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});


// Leads Wallet for each user seperately

evtApp.controller('vcardqueueSepOrgstatsCtrl', function ($scope,$http, Data, $uibModalInstance, toaster) {

    $scope.title = "Visitor Card Queue stats";
    $scope.orgst = {};
    var orgId = sessionStorage.getItem('orgId');
    var resprom = $http.get(Data.serviceBase() + '/visitor/vcardqueueSOstatsdetNew', {params: {"orgId": orgId}});
    resprom.then(function (response) {
        $scope.orgst = response.data.orgdet;
        //$scope.gettingDetails();
    }, function (response) {
        console.log('Error occured -- ');
        console.log(response);
    });
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});

evtApp.controller('vcardSepOrgCtrl', function ($scope, $timeout, Data, $uibModal, toaster, $http, fileReader, $rootScope, $window) {

    $scope.visitorvalue = {};
    $scope.visitor = {};
    $rootScope.totstats = '0';
    $rootScope.totnewstats = '0';
    $scope.valueflag = true;
    $scope.btntxt = "Verified";
    $scope.imageSrc = '';
    $rootScope.firstName = sessionStorage.getItem("firstName");
    $rootScope.lastName = sessionStorage.getItem("lastName");
    //$rootScope.type = sessionStorage.getItem('userType');
    var userId = sessionStorage.getItem('userId');
    var orgId = sessionStorage.getItem('orgId');
    $scope.userType = sessionStorage.getItem('userType');
    $rootScope.level = sessionStorage.getItem('userLevel');
    var businesscardurl = "";
    $scope.organizationID = 0;
    $scope.eventID = 0;
    $scope.organizations = {};
    $scope.events = {};
    $scope.organizationID = orgId;
    $rootScope.totstats = '0';
    $rootScope.totnewstats = '0';

    var chaeckStatus = function () {

        var resprom = $http.get(Data.serviceBase() + '/visitor/vcardqueuestatsSepdetNew', {params: {"orgId": $scope.organizationID}});
        resprom.then(function (response) {
            //console.log(response);
           $rootScope.totnewstats = response.data.total;
            //$scope.gettingDetails();
        }, function (response) {
            console.log('Error occured -- ');
            console.log(response);
        });
        $timeout(chaeckStatus, 9000);
    };
    $rootScope.makeRefresh = function () {
        $window.location.reload();
    };
    chaeckStatus();

    $scope.$on('onBeforeUnload', function (e, confirmation) {
        confirmation.message = "All data willl be lost.";
        e.preventDefault();
    });
    $scope.$on('onUnload', function (e) {
        console.log('leaving page'); // Use 'Preserve Log' option in Console
        Data.get('account/logout').then(function (results) {
            Data.toast(results);
            sessionStorage.setItem("userId", '');
            sessionStorage.setItem("orgId", '');
            sessionStorage.setItem("firstName", '');
            sessionStorage.setItem("lastName", '');
            sessionStorage.setItem("token", '');
            sessionStorage.setItem("userEmail", "");
            sessionStorage.setItem("cFirstName", '');
            sessionStorage.setItem("cLastName", '');
            sessionStorage.setItem("userType", '');
            sessionStorage.setItem("menuact", '');
        });
    });
    $scope.switchTab = function (tabId) {
        if (tabId === 1) {
            $scope.tab = 1;
            angular.element(document.getElementById("tab-3")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
        } else if (tabId === 2) {
            $scope.tab = 2;
            angular.element(document.getElementById("tab-4")).addClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
        } else {
            $scope.tab = 3;
            angular.element(document.getElementById("tab-5")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
        }

    };
    
    $scope.makeUrl = function (vvisitor) {
        var name = "";
        if(vvisitor.firstName){
            name = vvisitor.firstName;
        }
        else{
            name ="";
        }
        if(vvisitor.lastName){
            name = name + " " + vvisitor.lastName;
        }
        else{
            name = name + "";
        }
         var desig = "";
         if(vvisitor.designation){
              desig = vvisitor.designation;
         }
         else{
             desig = "";
         }
       
        var orgnaiz = "";
        if(vvisitor.visitorOrganizationName){
           orgnaiz = vvisitor.visitorOrganizationName
        }
        else{
            orgnaiz = "";
        }
        
            return name + "+" + desig + "+" +orgnaiz;
        
    };

    $scope.verifyEmail = function (visitor) {

        $scope.verifydata = {
            "orgId": visitor.organizationId,
            "emailId": visitor.email,
            "visitorId": visitor.id
        };

        if (visitor.organizationId && visitor.email && visitor.id) {
            Data.post('visitor/verifySepEmail', $scope.verifydata).then(function (result) {
                if (result.status !== 'error') {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    //$window.location.reload();
                    //$scope.visitor = {};
                    $scope.visitorvalue = {};
                    $scope.visitorvalue.leadnotes = "";
                    $scope.visitorvalue.orgnotes = "";
                    $scope.visitorvalue.linkedin = "";
                    $scope.visitorvalue.url1 = "";
                    $scope.visitorvalue.url2 = "";
                    $scope.visitorvalue.url3 = "";
                    console.log(result.visitorValAdd['leadnotes']);
                    $scope.visitor.firstName = result.visitor.firstName;
                    $scope.visitor.lastName = result.visitor.lastName;
                    $scope.visitor.visitorOrganizationName = result.visitor.visitorOrganizationName;
                    $scope.visitor.designation = result.visitor.designation;
                    $scope.visitor.email = result.visitor.email;
                    $scope.visitor.alternateEmail = result.visitor.alternateEmail;
                    $scope.visitor.mobile = result.visitor.mobile;
                    $scope.visitor.website = result.visitor.website;
                    $scope.visitor.phone = result.visitor.phone;
                    $scope.visitor.alternatePhone = result.visitor.alternatePhone;
                    $scope.visitor.fax = result.visitor.fax;
                    $scope.visitor.address1 = result.visitor.address1;
                    $scope.visitor.address2 = result.visitor.address2;
                    $scope.visitor.photourl = result.visitor.photourl;
                    $scope.visitor.city = result.visitor.city;
                    $scope.visitor.state = result.visitor.state;
                    $scope.visitor.country = result.visitor.country;
                    $scope.visitor.zip = result.visitor.zip;
                    $scope.visitorvalue.leadnotes = result.visitorValAdd['leadnotes'];
                    $scope.visitorvalue.orgnotes = result.visitorValAdd['orgnotes'];
                    $scope.visitorvalue.linkedin = result.visitorValAdd['linkedin'];
                    $scope.visitorvalue.url1 = result.visitorValAdd['url1'];
                    $scope.visitorvalue.url2 = result.visitorValAdd['url2'];
                    $scope.visitorvalue.url3 = result.visitorValAdd['url3'];

                } else {
                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    console.log(result);
                }
            });
        }
    };
    $scope.skipemail = function () {

        $scope.skipdata = {
            "userId": $scope.visitor.capturedUserId,
            "visitorId": $scope.visitor.id
        };

        Data.post('visitor/skipSepEmail', $scope.skipdata).then(function (result) {
            if (result.status !== 'error') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                //$window.location.reload();
                $scope.gettingDetails();
            } else {
                console.log(result);
            }
        });
    };

    $scope.rejectemail = function () {

        $scope.rejectdata = {
            "userId": $scope.visitor.capturedUserId,
            "eventName": $scope.visitor.eventName,
            "visitorId": $scope.visitor.id
        };

        Data.post('visitor/vcardrejectionmail', $scope.rejectdata).then(function (result) {
            if (result.status !== 'error') {
                toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                //$window.location.reload();
                $scope.gettingDetails();
            } else {
                console.log(result);
            }
        });
    };

    $scope.getFile = function () {
        $scope.progress = 0;
        fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    console.log($scope.file);

                    $scope.imageSrc = result;
                });
    };


    $scope.selectEvent = function () {
        var resprom = $http.get(Data.serviceBase() + '/admin/eventGetSepDetails', {params: {'key': "organizationId", "value": $scope.organizationID}});
        resprom.then(function (response) {
            $scope.events = response.data;
            if ($scope.events.length > 0) {
                $scope.eventID = $scope.events[0]['id'];
            }
            //$scope.gettingDetails();
        }, function (response) {
            console.log('Error occured -- ');
            console.log(response);
        });
    };

    $scope.gettingDetails = function () {
        console.log("EventID " + $scope.eventID);
        var resprom = $http.get(Data.serviceBase() + "visitor/bcardOrgVisitorInd", {params: {'organizationId': $scope.organizationID, 'eventId': $scope.eventID}});
        resprom.then(function (response) {

            $scope.btntxt = "Verified";
            $scope.visitor = response.data.visitors;
            $scope.visitorvalue = response.data.visitorsvalue[0];
            if ($scope.visitor.length <= 0) {
                toaster.pop("error", "", "No more business cards are available", 10000, 'trustedHtml');
            }
            // if($rootScope.totstats > 0 && response.data.visitors.length <=0 ){
            //     toaster.pop("error", "", "Business card are still in DC Queue", 10000, 'trustedHtml');
            // }

            if ($scope.visitor['valueAddInfo'] === '1') {
                $scope.valueflag = true;
            } else {
                $scope.valueflag = false;
            }


        }, function (response) {
            console.log('Error occured -- ');
            console.log(response);
        });
    };
    $scope.angle = 0;

    $scope.rotate = function (angle) {
        $scope.angle = $scope.angle + angle;
        console.log($scope.angle);
    };


    $scope.tab = 1;
    angular.element(document.getElementById("tab-3")).addClass("active");
    angular.element(document.getElementById("tab-4")).removeClass("active");
    angular.element(document.getElementById("tab-5")).removeClass("active");
    $scope.Continuetonext = function () {
        $scope.tab = 2;
        angular.element(document.getElementById("tab-4")).addClass("active");
        angular.element(document.getElementById("tab-3")).removeClass("active");
        angular.element(document.getElementById("tab-5")).removeClass("active");
        console.log($scope.tab);

    };
    $scope.Continuetonext1 = function () {
        $scope.tab = 3;
        angular.element(document.getElementById("tab-5")).addClass("active");
        angular.element(document.getElementById("tab-3")).removeClass("active");
        angular.element(document.getElementById("tab-4")).removeClass("active");
        console.log($scope.tab);

    };



    $scope.confvisitor = function (p, q, size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './confvisitor',
            controller: 'confSepsingleCtrl',
            size: size,
            resolve: {
                visitor: function () {
                    return p;
                },
                visitorvalue: function () {
                    return q;
                },
                imgsrc: function () {
                    return $scope.imageSrc;
                }
            }
        });
        modalInstance1.result.then(function (i) {
            //$window.location.reload();
            $scope.imageSrc = "";
            $scope.tab = 1;
            angular.element(document.getElementById("tab-3")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
            $scope.gettingDetails();

        });
    };

    $rootScope.vcardqueueorgstats = function (size) {

        var modalInstance1 = $uibModal.open({
            templateUrl: './vcardqueueSeporgstats',
            controller: 'vcardqueueSepOrgstatsCtrl',
            size: 'lg',
            resolve: {
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };
});
evtApp.controller('confSepsingleCtrl', function ($scope, Data, $uibModalInstance, visitor, visitorvalue, imgsrc, toaster) {
    $scope.visitor = visitor;
    $scope.imageSrc = imgsrc;
    $scope.visitorvalue = visitorvalue;
    $scope.title = "Confirmation";
    $scope.userType = "QC";

    $scope.updatevisitorvalue = function (visitor, visitorvalue) {
        //console.log(visitorvalue);
        if (visitor.id !== undefined) {
            visitor.userType = $scope.userType;

            if ($scope.imageSrc) {
                visitor.profilephotourl = $scope.imageSrc;

            }
            if (visitorvalue !== undefined) {
                visitor.leadnotes = visitorvalue.leadnotes;
                visitor.orgnotes = visitorvalue.orgnotes;
                visitor.linkedin = visitorvalue.linkedin;
                visitor.url1 = visitorvalue.url1;
                visitor.url2 = visitorvalue.url2;
                visitor.url3 = visitorvalue.url3;
               
            }
			visitor.bcardstatus = $scope.userType;
            Data.put('visitor/leadzwalletSepvisitor', visitor).then(function (result) {
                if (result.status !== 'error') {

                    toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
                    $uibModalInstance.close(1);
                } else {
                    console.log(result);
                }
            });
        }
    }


    /*if (visitorvalue.id !== undefined) {
     visitorvalue.visitorId = $scope.visitor.id;
     visitorvalue.photourl = $scope.imageSrc;
     
     Data.put('visitor/visitorvalueadd', visitorvalue).then(function (result) {
     if (result.status !== 'error') {
     
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     if (sessionStorage.getItem('userType') === "regular") {
     $scope.updatbusinesscardsts("DC");
     
     } else {
     
     $scope.updatbusinesscardsts("QC");
     }
     $uibModalInstance.close(1);
     
     } else {
     console.log(result);
     }
     });
     } else {
     visitorvalue.visitorId = $scope.visitor.id;
     visitorvalue.photourl = $scope.imageSrc;
     
     Data.post('visitor/visitorvalueadd', visitorvalue).then(function (result) {
     if (result.status !== 'error') {
     
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     if (sessionStorage.getItem('userType') === "regular") {
     $scope.updatbusinesscardsts("DC");
     
     } else {
     
     $scope.updatbusinesscardsts("QC");
     }
     
     
     } else {
     console.log(result);
     }
     });
     }
     
     };
     $scope.updatbusinesscardsts = function (status) {
     $scope.bcard = {};
     $scope.bcard.cardId = $scope.visitor.cardId;
     $scope.bcard.status = status;
     console.log(status);
     Data.put('visitor/businesscard', $scope.bcard).then(function (result) {
     if (result.status !== 'error') {
     
     toaster.pop(result.status, "", result.message, 10000, 'trustedHtml');
     $uibModalInstance.close(1);
     } else {
     console.log(result);
     }
     });
     };*/
    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
});