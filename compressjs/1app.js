var evtApp = angular.module('evtApp', ['ngRoute','ngAnimate','toaster','ui.bootstrap','ui.bootstrap.datetimepicker', 
                            'datatables', 'datatables.buttons','ngSanitize','monospaced.elastic','ui.tinymce','ui.select','ui.sortable',
                            'angular-flot','angles','gridshore.c3js.chart','chart.js','graph.funnel','textAngular','angucomplete-alt','ng-clipboard','lazy-scroll','wu.masonry',
                            'angularUtils.directives.dirPagination']);

evtApp.config(function($provide){
    $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions){       
        taRegisterTool('visitorname', {
              buttontext: '#Visitor Name#',
            action: function (taRegisterTool, taOptions) {                
         this.$editor().wrapSelection('insertText', '#VisitorName#');             
            }
        });
        taRegisterTool('compname', {
              buttontext: '#Visitor Company Name#',
            action: function (taRegisterTool, taOptions) {                
         this.$editor().wrapSelection('insertText', '#VisitorCompanyName#');             
            }
        });
         taRegisterTool('eventname', {
              buttontext: '#Event Name#',
            action: function (taRegisterTool, taOptions) {                
         this.$editor().wrapSelection('insertText', '#EventName#');             
            }
        });
        taOptions.toolbar=['visitorname','compname','eventname'];
        return taOptions;
    }]);
});

evtApp.service('alertUser', function ($uibModal, $http) {

   
    this.alertpopmodel = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './alertPage',
            controller: 'alertsCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                msg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

            }
        });
    };
});

evtApp.controller('alertsCtrl', function ($scope, Data, $http, $uibModalInstance, $uibModal, msg) {

    $scope.altmessage = msg;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});
evtApp.service('checkPremium', function ($uibModal, $http) {

   
    this.premiumpopmodel = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './checkPremiumPopup',
            controller: 'premiumCheckCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                msg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

            }
        });
    };
});

evtApp.controller('premiumCheckCtrl', function ($scope, Data, $uibModalInstance, msg, toaster) {
    $scope.message = msg;
    $scope.redirecttoBack = function () {
        $uibModalInstance.close(1);
        //history.back();
		window.location.href = Data.webAppBase() + 'account/dashboard';

    };
    $scope.redirecttoPay = function () {
        $uibModalInstance.close(1);
        window.location.href = Data.webAppBase() + 'account/pricing';

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});
evtApp.service('trialPopUp', function ($uibModal, $rootScope, $http) {

    this.trialPopmodel = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './trialPopup',
            controller: 'trialPopupCtrl',
            size: 'md',
            backdrop: 'static',
            keyboard: false,
            resolve: {
                msg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {

            }
        });
    };


});
evtApp.controller('trialPopupCtrl', function ($scope, Data,$http, $uibModalInstance,alertUser, msg, toaster) {
    $scope.message = msg;
	
    if (window.location.href.indexOf("datamange") > -1) {
        $scope.bHome = true;
        $scope.cancelBtn = false;
    } else if (window.location.href.indexOf("integration") > -1) {
        $scope.bHome = true;
        $scope.cancelBtn = false;
    }
    else if (window.location.href.indexOf("helloEmail") > -1) {
        $scope.bHome = true;
        $scope.cancelBtn = false;
    }else if(window.location.href.indexOf("report#") > -1){
        $scope.bHome = true;
        $scope.cancelBtn = false;
    }else if(window.location.href.indexOf("leadStage") > -1){
        $scope.bHome = true;
        $scope.cancelBtn = false;
    }else if(window.location.href.indexOf("apisettings") > -1){
        $scope.bHome = true;
        $scope.cancelBtn = false;
    }else if(window.location.href.indexOf("quotesinvoice") > -1){
        $scope.bHome = true;
        $scope.cancelBtn = false;
    }else if(window.location.href.indexOf("emailTemplate") > -1){
        $scope.bHome = true;
        $scope.cancelBtn = false;
    }else if(window.location.href.indexOf("SMTPIntegration") > -1){
        $scope.bHome = true;
        $scope.cancelBtn = false;
    }else if(window.location.href.indexOf("facebookInteg") > -1){
        $scope.bHome = true;
        $scope.cancelBtn = false;
    }else if(window.location.href.indexOf("hellosms") > -1){
        $scope.bHome = true;
        $scope.cancelBtn = false;
    }
    else {
        $scope.bHome = false;
        $scope.cancelBtn = true;
    }
    $scope.redirectTo = function (link,label) {
       if (label === 'Request for extension') {
            var resprom = $http.get(Data.serviceBase() + '/organization/trialExtensionReq', {params: {"organizationId": $scope.organizationId}});
            $scope.emptyorg = {};
            $scope.orgs = {};
            resprom.then(function (response) {
                if (response.data.status === 'success') {
                    sessionStorage.setItem('accountType', 'P3');
                    //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
                    $scope.message = {"title": "Trail Extended", "message": response.data.message};
                    alertUser.alertpopmodel($scope.message);
                    $uibModalInstance.close(1);
                } else {
                    window.open(link, '_blank');
                }
                $scope.orgs = response.data;
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
        } else {
            window.open(link, '_blank');
        }
		$uibModalInstance.close(1);
            // window.open(link, '_blank');
    };
    $scope.redirecttoPay = function () {
        $uibModalInstance.close(1);
        window.location.href = Data.webAppBase() + 'account/pricing';

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };
    $scope.backToHome = function () {
        $uibModalInstance.dismiss('Close');
        history.back();
    }


});

evtApp.service('warningPopup', function ($uibModal,$rootScope, $http) {

   
    this.warningpopmodel = function (p, size) {
        var modalInstance = $uibModal.open({
            templateUrl: './warningPopup',
            controller: 'warningPopupCtrl',
            size: 'md',
            backdrop: 'static',
            resolve: {
                msg: function () {
                    return p;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i === '1') {
                $rootScope.$broadcast('warnresp', { status: "true" });
            }else{
                 $rootScope.$broadcast('warnresp', { status: "false" });
            }
        });
    };
});

evtApp.controller('warningPopupCtrl', function ($scope, Data, $http, $uibModalInstance, $uibModal, msg) {

    $scope.altmessage = msg;


    $scope.okbtn = function(){
        $uibModalInstance.close('1');
    };
    $scope.cancelbtn = function(){
        $uibModalInstance.close('0');
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('Close');
    };

});