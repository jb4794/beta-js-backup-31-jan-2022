evtApp.controller('Analysis', ['$scope', '$rootScope', '$http', '$location','Data', 'trialPopUp','demoService','$uibModal','alertUser', Formcontroller]);
function Formcontroller($scope, $rootScope, $http, $location,Data,trialPopUp, demoService,$uibModal,alertUser)
{
    $rootScope.title = "Reports and Analytics";
    $rootScope.addButtonhtml = '&nbsp;';
    sessionStorage.setItem('menuact', '8');
    $rootScope.menuact = '8';
    var baseUrl = "https://beta.helloleads.io/index.php/app/report";

    var orgId = sessionStorage.getItem('orgId');
    var userId = sessionStorage.getItem('userId');
	$scope.cstages = {};
    //console.log(orgId);
    $scope.GetContent = function (prm) {
        //alert('No leads Found under this categories');
        //window.location.href = baseUrl + "/filterIndex";
        //var FilterValues = new Array({Organization: orgId, UserId: userId, Categories: prm});
        //var Path = new Array({Path: 'UserFilter'});
        $scope.pageNumber = 1;
        $scope.totalVisitors = 0;
        $scope.totalPage = 1;
        $scope.usersPerPage = 20;
        $scope.Filter("Total", "", "", "", prm);
        //sessionStorage.setItem('Filter', JSON.stringify(FilterValues));
        // sessionStorage.setItem('Path', JSON.stringify(Path));

    };
    $scope.$on('chart-create', function (event, instance) {
        // used to obtain chart instance

        $scope.chart = instance.chart;
//         if (event.targetScope.chartOptions.title.text === "Over all Lead Analysis via Product with Stages For Organization")
//        {
//            var width = instance.width;
//            console.log(width);
//    $("#ChartInterestGroupVsLeads").width(width);
//
//        }

    });
    $scope.orgId = sessionStorage.getItem('orgId');
	$scope.roleId = sessionStorage.getItem('roleId');
	$scope.rolemsg = "";
    if ($scope.roleId === '2') {
        $scope.rolemsg = "Manager";
    } else if ($scope.roleId === '3') {
        $scope.rolemsg = "L1 User";
    } else {
        $scope.rolemsg = "L2 User";
    }
    $scope.init = function () {
        $scope.loader1 = true;
        //sessionStorage.clear();
        $location.url('');
        $scope.LeadTimeValue = "Week";
        $scope.LeadTimeValueUser = "Week";
        $scope.LeadTimeValueAccount = "Week";
        $scope.getChart();
        $scope.getChartUser();
        $scope.getChartAccount();
		$scope.getTargetStats();
        $scope.getUserTargetStats();
        $scope.CustomerGroupHide = false;
        $scope.InterestGroupHide = false;
        $scope.SourceHide = false;
        $scope.AgeStageHide = false;
        $scope.StageHide = false;
        $scope.PotentialHide = false;
        $scope.CustomerGroupHideError = false;
        $scope.InterestGroupHideError = false;
        $scope.SourceHideError = false;
        $scope.AgeStageHideError = false;
        $scope.StageHideError = false;
        $scope.PotentialHideError = false;
        $scope.UserHide = false;
        $scope.UserVsStagesHide = false;
        $scope.UserVsPotentialHide = false;
        $scope.UserVsSourcesHide = false;
        $scope.CustomerGroupVsLeadsHide = false;
        $scope.UserHideError = false;
        $scope.UserVsStagesHideError = false;
        $scope.UserVsPotentialHideError = false;
        $scope.UserVsSourcesHideError = false;
        $scope.CustomerGroupVsLeadsHideError = false;
        $scope.LeadAccountHide = false;
        $scope.LeadStageAccountHide = false;
        $scope.LeadPotentialAccountHide = false;
        $scope.LeadAccountHideError = false;
        $scope.LeadStageAccountHideError = false;
        $scope.LeadPotentialAccountHideError = false;
        $scope.loader1 = false;
        $scope.orgPayemnt();
    };

    //UBT
    $scope.usersAnalysis = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('user/usersAnalysis', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };

    $scope.leadAnalysis = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('visitor/leadAnalysis', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
    };


    $scope.orgPayemnt = function () { 
	var resprole = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"organizationId": orgId, "checkReq": "trialEnd"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {
        var resprole = $http.get(Data.serviceBase() + '/permission/checkPermission', {params: {"organizationId": orgId, "userId": userId, "permission": "customAnalytics"}});

        // $scope.users = {};
        resprole.then(function (response) {
            console.log(response);
            $scope.loader = false;
            if (response.data.status === 'success') {

                var orgResp = $http.get(Data.serviceBase() + '/organization/orgChecks', {params: {"checkReq": "customAnalytics", "organizationId": $scope.organizationId}});
                orgResp.then(function (response) {
                    console.log(response.data);
                    if (response.data.status != 'success') {
                        $scope.orgPlanCheck(response.data);

                    }

                }, function (response) {
                    console.log('Error happened -- ');
                    console.log(response);
                });

            } else {
                $scope.message = {"title": "Authorization failure", "message": "Your current role ( " + $scope.rolemsg + " ) does not allow you to use Custom Analytics feature. Only account owner or managers can view Custom Analytics. Please contact HelloLeads account owner or managers in your company for assistance with this task"};
                alertUser.alertpopmodel($scope.message);
                //toaster.pop(response.data.status, "", response.data.message, 10000, 'trustedHtml');
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });
		 } else {
               trialPopUp.trialPopmodel(response.data);
            }
        }, function (response) {

            console.log('Error happened -- ');
            console.log(response);
        });

    };
    $scope.orgPlanCheck = function (p, size) {
        var modalInstance1 = $uibModal.open({
            templateUrl: './checkPremiumPopup',
            controller: 'premiumCheckCtrl',
            size: size,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                msg: function () {
                    return p;
                }
            }
        });
        modalInstance1.result.then(function (i) {

        });
    };
	$scope.getTargetStats = function () {
        $scope.loader3 = true;
        $http({
            method: 'get',
            url: baseUrl + '/userTargetStats',
            params:
                    {
                        organizationId: orgId,
                        userId: userId,
                    },
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (result)
        {
            $scope.loader3 = false;
            $scope.userStats = result.data;
            $scope.currencyType = sessionStorage.getItem('orgCurrency');

        }, function (result) {

            console.log(result);
        });
    };

    $scope.getCustomers = function () {
        var userId = sessionStorage.getItem('userId');
        //$scope.Filter("Target", "1", '',userId ,'');
    };

    $scope.getUserTargetStats = function () {
        //$scope.loader3 = true;
        $scope.userTarHide = false;
        $scope.userTarHideError = true;
        $http({
            method: 'get',
            url: baseUrl + '/usersTargetStats',
            params:
                    {
                        organizationId: orgId,
                    },
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (result)
        {
            if (result.data.usersTarget.length > 0 && result.data.usersAchive.length > 0) {
                $scope.currencyType = sessionStorage.getItem('orgCurrency');
                // console.log(result.data.AddvsSources.Values)
                var userIds = result.data.userIds;
                $scope.UserTarget = {
                    labels: result.data.users,
                    series: ['Target', 'Achieved'],
                    data: [result.data.usersTarget,
                        result.data.usersAchive

                    ],
                    //colors:['#72C02C', '#3498DB', '#717984', '#F1C40F'],
                    colors: [{
                            backgroundColor: "#45afed"
                        }, {
                            backgroundColor: "#ed4d45"
                        }, {
                            backgroundColor: "#FFCC66"
                        }, {
                            backgroundColor: "#717984"
                        }, {
                            backgroundColor: "#8EDC9D"
                        }, {
                            backgroundColor: "#F9E559"
                        }, {
                            backgroundColor: "#6CCECB"
                        }],
                    options: {title: {
                            display: true,
                            position: 'top',
                            text: 'Annual Sales Target',
                        },
                        legend: {
                            display: true,
                            position: 'right',
                            labels: {
                                usePointStyle: true,
                                fontSize: 10
                            },
                            onHover: function (e) {
                                e.target.style.cursor = 'pointer';
                            }
                        },
                        onClick: function (e) {
                            var element = this.getElementAtEvent(e);
                            if (element.length) {
                                console.log(element);
                                if (element[0]['_model']['datasetLabel'] === 'Achieved') {
                                    $scope.pageNumber = 1;
                                    $scope.totalVisitors = 0;
                                    $scope.totalPage = 1;
                                    $scope.usersPerPage = 20;
                                    //alert(element[0]['_model']['label'] + element[0]['_model']['datasetLabel']);
                                    $scope.Filter("Target", "2", '', userIds[element[0]['_index']], element[0]['_model']['datasetLabel']);
                                } else {
                                    return false;
                                }
                            }
                        },
                        hover: {
                            onHover: function (e) {
                                var point = this.getElementAtEvent(e);
                                if (point.length)
                                    e.target.style.cursor = 'pointer';
                                else
                                    e.target.style.cursor = 'default';
                            }
                        },
                        scales: {
                            xAxes: [{
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'User name'
                                    },
                                    ticks: {
                                        fontSize: 15,
                                    },
                                    stacked: false
                                }],
                            yAxes: [{
                                    scaleLabel: {
                                        display: true,
                                        labelString: "Target Amount (" + $scope.currencyType + ")"
                                    },
                                    stacked: false
                                }]
                        }

                    }
                };
            } else
            {
                $scope.userTarHide = true;
                $scope.userTarHideError = false;
                $scope.UserTar_Error = "Annual Target yet to be set for  all users.";
            }

        }, function (result) {
            $scope.userTarHide = true;
            $scope.userTarHideError = false;
            $scope.UserTar_Error = result.message;
            console.log(result);
        });
    };

    $scope.repredirect = function (link) {
        window.open(Data.webAppBase() + 'account/users', '_blank');
    };
    $scope.Actions = function ()
    {
        $scope.loader2 = true;
        $http({
            method: 'get',
            url: baseUrl + '/LeadActions',
            params:
                    {
                        OrganizationId: orgId,
                        UserId: userId,
                    },
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (data)
        {

            $scope.TotalLeads = data.data.Total[0].Count;
            $scope.OpenFollowups = data.data.OpenFollowups[0].Count;
            $scope.ContactedFollowups = data.data.wonFollowups[0].Count;
            $scope.QualifiedFollowups = data.data.lostFollowups[0].Count;
            $scope.TodayFollowups = data.data.TodayFollowups[0].Count;
            $scope.PriorityFollowups = data.data.PriorityTodayFollowups[0].Count;
            $scope.LapsedFollowups = data.data.LapsedFollowups[0].Count;
            $scope.PriorityLapsedFollowups = data.data.PriorityLapsedFollowups[0].Count;
            $scope.openLabel = data.data.openLabel;
            $scope.wonLabel = data.data.wonLabel;
            $scope.lostLabel = data.data.lostLabel;
            $scope.PriorityLapsedFollowups = data.data.PriorityLapsedFollowups[0].Count;
            $scope.PriorityLapsedFollowups = data.data.PriorityLapsedFollowups[0].Count;
            $scope.loader2 = false;
        }, function (data) {

            console.log(data);
        });
    };
    $scope.getChart = function ()
    {
        $http({
            method: 'get',
            url: baseUrl + '/leadAnalysis',
            params:
                    {
                        OrganizationId: orgId,
                        UserId: userId,
                        TimeValue: $scope.LeadTimeValue
                    },
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (data)
        {
            //console.log(data);
            var yaxis = "";
            if ($scope.LeadTimeValue === "" || $scope.LeadTimeValue === "lastMonth")
            {
                yaxis = "Weeks";
            } else if ($scope.LeadTimeValue === "LastYear" || $scope.LeadTimeValue === "currentYear")
            {
                yaxis = "Months";
            } else if ($scope.LeadTimeValue === "YearWise")
            {
                yaxis = "Years";
            }

            if (data.data.Flag === "TRUE") {
                $scope.CustomerGroupHide = false;
                $scope.InterestGroupHide = false;
                $scope.SourceHide = false;
                $scope.AgeStageHide = false;
                $scope.StageHide = false;
                $scope.PotentialHide = false;
                $scope.CustomerGroupHideError = true;
                $scope.InterestGroupHideError = true;
                $scope.SourceHideError = true;
                $scope.AgeStageHideError = true;
                $scope.StageHideError = true;
                $scope.PotentialHideError = true;
                $scope.CustomerCount = data.data.CustomerCount[0].Count;
                $scope.CapturedCount = data.data.CapturedCount[0].Count;
                $scope.CommentCount = data.data.CommentCount[0].Count;
                if (data.data.AddvsSources.Flag === "TRUE")
                {
                    console.log(data.data.AddvsSources.Values)
                    $scope.SourceHide = false;
                    $scope.SourceHideError = true;
                    $scope.Source = {
                        labels: data.data.AddvsSources.Year,
                        series: $scope.getSourceSeries(data.data.AddvsSources.Sources),
                        data: [data.data.AddvsSources.Values[0],
                            data.data.AddvsSources.Values[1],
                            data.data.AddvsSources.Values[2],
                            data.data.AddvsSources.Values[3],
                            data.data.AddvsSources.Values[4],
                            data.data.AddvsSources.Values[5],
                            data.data.AddvsSources.Values[6],
                            data.data.AddvsSources.Values[7],
                            data.data.AddvsSources.Values[8],
                            data.data.AddvsSources.Values[9],
                            data.data.AddvsSources.Values[10],
                            data.data.AddvsSources.Values[11]
                        ],
                        //colors:['#72C02C', '#3498DB', '#717984', '#F1C40F'],
                        colors: [{
                                backgroundColor: "#218C8D"
                            }, {
                                backgroundColor: "#B9D7D3"
                            }, {
                                backgroundColor: "#FFCC66"
                            }, {
                                backgroundColor: "#717984"
                            }, {
                                backgroundColor: "#8EDC9D"
                            }, {
                                backgroundColor: "#F9E559"
                            }, {
                                backgroundColor: "#6CCECB"
                            }],
                        options: {title: {
                                display: true,
                                position: 'top',
                                text: 'Leads captured sources',
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    usePointStyle: true,
                                    fontSize: 10
                                },
                                onHover: function (e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            onClick: function (e) {
                                var element = this.getElementAtEvent(e);
                                if (element.length) {
                                    $scope.pageNumber = 1;
                                    $scope.totalVisitors = 0;
                                    $scope.totalPage = 1;
                                    $scope.usersPerPage = 20;
                                    //      alert( element[0]['_model']['label'] + element[0]['_model']['datasetLabel']);
                                    $scope.Filter("Leads", "6", $scope.LeadTimeValue, element[0]['_model']['label'], element[0]['_model']['datasetLabel']);
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length)
                                        e.target.style.cursor = 'pointer';
                                    else
                                        e.target.style.cursor = 'default';
                                }
                            },
                            scales: {
                                xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'No.of.Leads Captured'
                                        },
                                        ticks: {
                                            fontSize: 15,
                                        },
                                        stacked: true
                                    }],
                                yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: yaxis
                                        },
                                        stacked: true
                                    }]
                            }

                        }
                    };
                } else
                {
                    $scope.SourceHide = true;
                    $scope.SourceHideError = false;
                    $scope.Source_Error = data.data.AddvsSources.Error_Message;
                }
                if (data.data.AddvsStages.Flag === "TRUE")
                {
                     console.log(data.data.AddvsStages.Values[0]);
                    $scope.AgeStageHide = false;
                    $scope.AgeStageError = true;
                    $scope.AgeStagesValue = [];
                    for(var w=0;w<data.data.AddvsStages.Values.length;w++){
                        $scope.AgeStagesValue.push(data.data.AddvsStages.Values[w]);
                    }
                    $scope.AgeStage = {
                        labels: data.data.AddvsStages.Year,
                        series: data.data.AddvsStages.Sources,
                        data:  $scope.AgeStagesValue,
                        //colors:['#72C02C', '#3498DB', '#717984', '#F1C40F'],
                        colors: [{
                                backgroundColor: "#2077B4"
                            }, {
                                backgroundColor: "#FF7F0F"
                            }, {
                                backgroundColor: "#2DA02D"
                            }, {
                                backgroundColor: "#F1C40F"
                            }, {
                                backgroundColor: "#D62829"
                            }, {
                                backgroundColor: "#F3F3F4"
                            },{
                                backgroundColor: "#FA34E0"
                            },{
                                backgroundColor: "#F4ED80"
                            }],
                        options: {title: {
                                display: true,
                                position: 'top',
                                text: 'Leads age Vs Stages',
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    usePointStyle: true,
                                    fontSize: 10
                                },
                                onHover: function (e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            onClick: function (e) {
                                var element = this.getElementAtEvent(e);
                                if (element.length) {
                                    $scope.pageNumber = 1;
                                    $scope.totalVisitors = 0;
                                    $scope.totalPage = 1;
                                    $scope.usersPerPage = 20;
                                    $scope.Filter("Leads", "5", $scope.LeadTimeValue, element[0]['_model']['label'], element[0]['_model']['datasetLabel']);
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length)
                                        e.target.style.cursor = 'pointer';
                                    else
                                        e.target.style.cursor = 'default';
                                }
                            },
                            scales: {
                                xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'No.of.Leads '
                                        },
                                        ticks: {
                                            fontSize: 15,
                                        },
                                        stacked: true
                                    }],
                                yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: yaxis
                                        },
                                        stacked: true
                                    }]
                            }

                        }
                    };
                } else
                {
                    $scope.AgeStageHide = true;
                    $scope.AgeStageHideError = false;
                    $scope.AgeStage_Error = data.data.AddvsStages.Error_Message;
                }

                // ================FollVsStage ==========================//
                if (data.data.FollVsStage.Flag === "TRUE")
                {$scope.StageHide = false;
                    $scope.StageHideError = true;
                    $scope.followStages = [];
                    for(var e=0;e<data.data.FollVsStage.Values.length;e++){
                        if(data.data.FollVsStage.Values[e] != false){
                            $scope.followStages.push(data.data.FollVsStage.Values[e]);
                        }
                    }
                    $scope.Stage = {
                        labels: data.data.FollVsStage.Year,
                        series: data.data.FollVsStage.Stage,
                        data: $scope.followStages
                        ,  colors: [{
                                backgroundColor: "#2077B4"
                            }, {
                                backgroundColor: "#FF7F0F"
                            }, {
                                backgroundColor: "#2DA02D"
                            }, {
                                backgroundColor: "#F1C40F"
                            }, {
                                backgroundColor: "#D62829"
                            }, {
                                backgroundColor: "#F3F3F4"
                            },{
                                backgroundColor: "#EF9980"
                            },{
                                backgroundColor: "#FE78A0"
                            }],
                        options: {title: {
                                display: true,
                                position: 'top',
                                text: 'Leads follow-up Vs Stage',
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    usePointStyle: true,
                                    fontSize: 10
                                },
                                onHover: function (e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            onClick: function (e) {
                                var element = this.getElementAtEvent(e);
                                if (element.length) {
                                    $scope.pageNumber = 1;
                                    $scope.totalVisitors = 0;
                                    $scope.totalPage = 1;
                                    $scope.usersPerPage = 20;
                                    $scope.Filter("Leads", "4", $scope.LeadTimeValue, element[0]['_model']['label'], element[0]['_model']['datasetLabel']);
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length)
                                        e.target.style.cursor = 'pointer';
                                    else
                                        e.target.style.cursor = 'default';
                                }
                            },
                            scales: {
                                xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'No.of.Leads Follow Ups'
                                        },
                                        ticks: {
                                            fontSize: 15,
                                        },
                                        stacked: true
                                    }],
                                yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: yaxis
                                        },
                                        stacked: true
                                    }]
                            }

                        }
                    };
                } else
                {
                    $scope.StageHide = true;
                    $scope.StageHideError = false;
                    $scope.Stage_Error = data.data.FollVsStage.Error_Message;
                }
                //=================Followup vs potential ===================
                if (data.data.FollVsPotential.Flag === "TRUE")
                {
                    console.log(data.data.FollVsPotential.Potential)
                    $scope.PotentialHide = false;
                    $scope.PotentialHideError = true;
                    $scope.Potential = {
                        labels: data.data.FollVsPotential.Year,
                        series: $scope.getPotentialSeries(data.data.FollVsPotential.Potential),
                        data: [data.data.FollVsPotential.Values[0],
                            data.data.FollVsPotential.Values[1],
                            data.data.FollVsPotential.Values[2],
                            data.data.FollVsPotential.Values[3]
                        ],
                        colors: [{
                                backgroundColor: "#57BC8A"
                            }, {
                                backgroundColor: "#539ECC"
                            }, {
                                backgroundColor: "#F2992F"
                            }, {
                                backgroundColor: "#F3F3F4"
                            }],
                        options: {title: {
                                display: true,
                                position: 'top',
                                text: 'Leads follow-up Vs Potential'
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    usePointStyle: true,
                                    fontSize: 10
                                },
                                onHover: function (e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            onClick: function (e) {
                                var element = this.getElementAtEvent(e);
                                if (element.length) {
                                    $scope.pageNumber = 1;
                                    $scope.totalVisitors = 0;
                                    $scope.totalPage = 1;
                                    $scope.usersPerPage = 20;
                                    $scope.Filter("Leads", "3", $scope.LeadTimeValue, element[0]['_model']['label'], element[0]['_model']['datasetLabel']);
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length)
                                        e.target.style.cursor = 'pointer';
                                    else
                                        e.target.style.cursor = 'default';
                                }
                            },
                            scales: {
                                xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'No.of.Leads Follow ups'
                                        },
                                        stacked: true
                                    }],
                                yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: yaxis
                                        }, ticks: {
                                            fontSize: 15,
                                        },
                                        stacked: true
                                    }]
                            }

                        }
                    };
                } else
                {
                    $scope.PotentialHide = true;
                    $scope.PotentialHideError = false;
                    $scope.Potential_Error = data.data.FollVsPotential.Error_Message;
                }
                //===============Follow ups vs Customer Group ========================

                //var dataPer = new Array();

                if (data.data.FollVSCustomerGroup.Flag === "TRUE")
                {
                    $scope.CustomerGroupHide = false;
                    $scope.CustomerGroupHideError = true;
					$scope.CustFollowStages = [];
                    for(var o=0;o<data.data.FollVSCustomerGroup.Values.length;o++){
                        if(data.data.FollVSCustomerGroup.Values != false){
                            $scope.CustFollowStages.push(data.data.FollVSCustomerGroup.Values[o]);
                        }
                    }
                    $scope.CustomerGroup = {
                        labels: data.data.FollVSCustomerGroup.Customer,
                        series: data.data.FollVSCustomerGroup.Stages,
                        data: $scope.CustFollowStages,
                        colors: [{
                                backgroundColor: "#2077B4"
                            }, {
                                backgroundColor: "#FF7F0F"
                            }, {
                                backgroundColor: "#2DA02D"
                            }, {
                                backgroundColor: "#F1C40F"
                            }, {
                                backgroundColor: "#D62829"
                            }, {
                                backgroundColor: "#F3F3F4"
                            },{
                                backgroundColor: "#EFA899"
                            },{
                                backgroundColor: "#45afed"
                            }
                        ],
                        options: {title: {
                                display: true,
                                position: 'top',
                                text: 'Customer group based Follow-up analysis with Lead stage',
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    usePointStyle: true,
                                    fontSize: 10
                                },
                                onHover: function (e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            onClick: function (e) {
                                var element = this.getElementAtEvent(e);
                                if (element.length) {
                                    $scope.pageNumber = 1;
                                    $scope.totalVisitors = 0;
                                    $scope.totalPage = 1;
                                    $scope.usersPerPage = 20;
                                    $scope.Filter("Leads", "1", $scope.LeadTimeValue, element[0]['_model']['label'], element[0]['_model']['datasetLabel']);
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length)
                                        e.target.style.cursor = 'pointer';
                                    else
                                        e.target.style.cursor = 'default';
                                }
                            },
                            tooltips: {
                                callbacks: {
                                    title: function (tooltipItems, data) {
                                        var labelIndex = tooltipItems[0].index;
                                        var realLabel = data.labels[labelIndex];
                                        return realLabel;
                                    }
                                }
                            },
                            scales: {
                                xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'No.of.Leads Follow ups'
                                        },
                                        ticks: {
                                            fontSize: 15,
                                        },
                                        stacked: true

                                    }],
                                yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Customer Groups'
                                        },
                                        ticks: {
                                            fontSize: 12,
                                            callback: function (label) {

                                                if (label.length <= 10)
                                                {
                                                    return label;
                                                } else
                                                {
                                                    return label.substring(0, 10) + "..";
                                                }


                                            }
                                        },
                                        stacked: true

                                    }]
                            }

                        }
                    };
                } else
                {
                    $scope.CustomerGroupHide = true;
                    $scope.CustomerGroupHideError = false;
                    $scope.CustomerGroup_Error = data.data.FollVSCustomerGroup.Error_Message;
                }
                if (data.data.FollVSInterestGroup.Flag === "TRUE")
                {

                    $scope.InterestGroupHide = false;
                    $scope.InterestGroupHideError = true;
					$scope.IntFollowStages = [];
                    for(var u=0 ;u < data.data.FollVSInterestGroup.Values.length ; u++){
                        if(data.data.FollVSInterestGroup.Values[u] != false){
                            $scope.IntFollowStages.push(data.data.FollVSInterestGroup.Values[u]);
                        }
                    }
                    $scope.InterestGroup = {
                        labels: data.data.FollVSInterestGroup.Product,
                        series: data.data.FollVSInterestGroup.Stages,
                         data: $scope.IntFollowStages,
                        colors: [{
                                backgroundColor: "#2077B4"
                            }, {
                                backgroundColor: "#FF7F0F"
                            }, {
                                backgroundColor: "#2DA02D"
                            }, {
                                backgroundColor: "#F1C40F"
                            }, {
                                backgroundColor: "#D62829"
                            }, {
                                backgroundColor: "#F3F3F4"
                            },
                            {
                                backgroundColor: "#EF98F6"
                                },
                            {
                                backgroundColor: "#45afed"
                            }],
                        options: {title: {
                                display: true,
                                position: 'top',
                                text: 'Product of Interest based Follow-up analysis with Lead stage',
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    usePointStyle: true,
                                    fontSize: 10
                                },
                                onHover: function (e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            onClick: function (e) {
                                var element = this.getElementAtEvent(e);
                                if (element.length) {
                                    $scope.pageNumber = 1;
                                    $scope.totalVisitors = 0;
                                    $scope.totalPage = 1;
                                    $scope.usersPerPage = 20;
                                    $scope.Filter("Leads", "2", $scope.LeadTimeValue, element[0]['_model']['label'], element[0]['_model']['datasetLabel']);
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length)
                                        e.target.style.cursor = 'pointer';
                                    else
                                        e.target.style.cursor = 'default';
                                }
                            },
                            tooltips: {
                                callbacks: {
                                    title: function (tooltipItems, data) {
                                        var labelIndex = tooltipItems[0].index;
                                        var realLabel = data.labels[labelIndex];
                                        return realLabel;
                                    }
                                }
                            },
                            scales: {
                                xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'No.of.Leads Follow ups'
                                        },
                                        ticks: {
                                            fontSize: 15,
                                        },
                                        stacked: true

                                    }],
                                yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Interest Product Groups'
                                        },
                                        ticks: {
                                            fontSize: 12,
                                            callback: function (label) {

                                                if (label.length <= 10)
                                                {
                                                    return label;
                                                } else
                                                {
                                                    return label.substring(0, 10) + "..";
                                                }


                                            }
                                        },
                                        stacked: true

                                    }]
                            }

                        }
                    };
                } else
                {
                    $scope.InterestGroupHide = true;
                    $scope.InterestGroupHideError = false;
                    $scope.InterestGroup_Error = data.data.FollVSInterestGroup.Error_Message;
                }
            } else
            {
                $scope.CustomerGroupHide = true;
                $scope.SourceHide = true;
                $scope.AgeStageHide = true;
                $scope.StageHide = true;
                $scope.PotentialHide = true;
                $scope.CustomerGroupHideError = false;
                $scope.InterestGroupHideError = false;
                $scope.SourceHideError = false;
                $scope.AgeStageHideError = false;
                $scope.StageHideError = false;
                $scope.PotentialHideError = false;
                $scope.Source_Error = data.data.Error_Message;
                $scope.AgeStage_Error = data.data.Error_Message;
                $scope.Stage_Error = data.data.Error_Message;
                $scope.Potential_Error = data.data.Error_Message;
                $scope.CustomerGroup_Error = data.data.Error_Message;
                $scope.InterestGroup_Error = data.data.Error_Message;
            }



        }, function (data) {

            console.log(data);
        });
    };


    //========================== User Analysis====================================
    $scope.getChartUser = function ()
    {
        $http({
            method: 'get',
            url: baseUrl + '/UserAnalysis',
            params:
                    {
                        OrganizationId: orgId,
                        TimeValue: $scope.LeadTimeValueUser
                    },
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function (data)
        {
        	$scope.accessRights = data.data.accessRights;

            //==========================================User Vs Leads Added==============

            if (data.data.Flag === "TRUE")
            {
                $scope.UserHide = false;
                $scope.UserVsStagesHide = false;
                $scope.UserVsPotentialHide = false;
                $scope.UserVsSourcesHide = false;
                $scope.UserHideError = true;
                $scope.UserVsStagesHideError = true;
                $scope.UserVsPotentialHideError = true;
                $scope.UserVsSourcesHideError = true;
                if (data.data.TopLeadGenerator.Flag === "False")
                {
                    $scope.TopLeadGenerator = [];
                } else
                {
                    $scope.TopLeadGenerator = data.data.TopLeadGenerator;
                    //$scope.TopCustomerGenerator = data.data.TopCustomeGenerator;
                }
                if (data.data.TopCustomeGenerator.Flag === "False")
                {
                    $scope.TopCustomerGenerator = [];
                } else
                {
                    //$scope.TopLeadGenerator = data.data.TopLeadGenerator;
                    $scope.TopCustomerGenerator = data.data.TopCustomeGenerator;
                }
                if (data.data.UserVSAdded.Flag === "TRUE")
                {
                    $scope.UserHide = false;
                    $scope.UserHideError = true;
                    $scope.User = {
                        labels: data.data.UserVSAdded.UserName,
                        ref: data.data.UserVSAdded.RefId,
                        series: data.data.UserVSAdded.UserName,
                        data: data.data.UserVSAdded.Values,
                        options: {title: {
                                display: true,
                                position: 'top',
                                text: 'User Capture Analysis',
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    usePointStyle: true,
                                    fontSize: 10
                                },
                                onHover: function (e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length)
                                        e.target.style.cursor = 'pointer';
                                    else
                                        e.target.style.cursor = 'default';
                                }
                            },
//                            scales: {
//                                xAxes: [{
//                                        scaleLabel: {
//                                            display: true,
//                                            labelString: 'No.of.Leads Captured'
//                                        },
//                                        //  stacked: true
//
//                                    }],
//                                yAxes: [{
//                                        scaleLabel: {
//                                            display: true,
//                                            labelString: 'User Name'
//                                        },
//                                        ticks: {
//                                            fontSize: 15
//                                        }
//                                        //    stacked: true
//
//                                    }]
//                            }

                        }

                    };
                } else
                {
                    $scope.UserHide = true;
                    $scope.UserHideError = false;
                    $scope.User_Error = data.data.UserVSAdded.Error_Message;
                }
                if (data.data.UserVSStages.Flag === "TRUE")
                {
                    $scope.UserVsStagesHide = false;
                    $scope.UserVsStagesHideError = true;
                    $scope.userStageValues = [];
                    for (var y = 0; y < data.data.UserVSStages.Values.length; y++) {
                        if (data.data.UserVSStages.Values[y] != false) {
                            $scope.userStageValues.push(data.data.UserVSStages.Values[y].No_of_leads);
                        }
                    }

                    $scope.UserVsStages = {
                        labels: data.data.UserVSStages.Values[0].UserNames,
                        ref: data.data.UserVSStages.Values[0].RefId,
                        series: data.data.UserVSStages.Stages,
                        data:$scope.userStageValues,
                        colors: [{
                                backgroundColor: "#2077B4"
                            }, {
                                backgroundColor: "#FF7F0F"
                            }, {
                                backgroundColor: "#2DA02D"
                            }, {
                                backgroundColor: "#F1C40F"
                            }, {
                                backgroundColor: "#D62829"
                            }, {
                                backgroundColor: "#F3F3F4"
                            }, {
                                backgroundColor: "#F8F389"
                            },
                            {
                                backgroundColor: "#EF87FA"
                            }
                        ],
                        options: {
                            title: {
                                display: true,
                                position: 'top',
                                text: 'Active Follow-Ups per user with Lead Stages'
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    usePointStyle: true,
                                    fontSize: 10
                                },
                                onHover: function (e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length)
                                        e.target.style.cursor = 'pointer';
                                    else
                                        e.target.style.cursor = 'default';
                                }
                            },
                            scales: {
                                xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'No.of.Leads Follow ups'
                                        },
                                        ticks: {
                                            fontSize: 15,
                                        },
                                        stacked: true
                                    }],
                                yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'User Name'
                                        },
                                        stacked: true
                                    }]
                            }

                        }
                    };
                } else
                {
                    $scope.UserVsStagesHide = true;
                    $scope.UserVsStagesHideError = false;
                    $scope.UserVsStages_Error = data.data.UserVSStages.Error_Message;
                }
                //========================Users Vs Potential ====================================
                if (data.data.UserVsPotential.Flag === "TRUE")

                {
                    $scope.UserVsPotentialHide = false;
                    $scope.UserVsPotentialHideError = true;
                    $scope.UserVsPotential = {
                        labels: data.data.UserVsPotential.Values[0].UserNames,
                        ref: data.data.UserVsPotential.Values[0].RefId,
                        series: data.data.UserVsPotential.Potential,
                        data: [
                            data.data.UserVsPotential.Values[0].No_of_leads,
                            data.data.UserVsPotential.Values[1].No_of_leads,
                            data.data.UserVsPotential.Values[2].No_of_leads,
                            data.data.UserVsPotential.Values[3].No_of_leads

                        ],
                        colors: [{
                                backgroundColor: "#57BC8A"
                            }, {
                                backgroundColor: "#539ECC"
                            }, {
                                backgroundColor: "#F2992F"
                            }, {
                                backgroundColor: "#F3F3F4"
                            }],
                        options: {title: {
                                display: true,
                                position: 'top',
                                text: 'Active Follow-Ups per user with Lead Potentials'
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    usePointStyle: true,
                                    fontSize: 10
                                },
                                onHover: function (e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length)
                                        e.target.style.cursor = 'pointer';
                                    else
                                        e.target.style.cursor = 'default';
                                }
                            },
                            scales: {
                                xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'No.of.Leads Follow ups'
                                        },
                                        ticks: {
                                            fontSize: 15,
                                        },
                                        stacked: true
                                    }],
                                yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'User Name'
                                        },
                                        stacked: true
                                    }]
                            }

                        }
                    };
                } else

                {
                    $scope.UserVsPotentialHide = true;
                    $scope.UserVsPotentialHideError = false;
                    $scope.UserVsPotential_Error = data.data.UserVsPotential.Error_Message;
                }
                if (data.data.UserVsSources.Flag === "TRUE")

                {
                    $scope.UserVsSourcesHide = false;
                    $scope.UserVsSourcesHideError = true;
                    if (data.data.UserVsSources.Name.length <= 8)
                    {
                        $("#ChartUserVsSources").width(700);
                        $("#ChartUserVsStages").width(700);
                        $("#ChartUserVsPotential").width(700);
                    } else if (data.data.UserVsSources.Name.length >= 8 && data.data.UserVsSources.Name.length <= 40)
                    {
                        $("#ChartUserVsSources").width(1000);
                        $("#ChartUserVsStages").width(1000);
                        $("#ChartUserVsPotential").width(1000);
                    } else if (data.data.UserVsSources.Name.length >= 40)
                    {
                        $("#ChartUserVsSources").width(1200);
                        $("#ChartUserVsStages").width(1200);
                        $("#ChartUserVsPotential").width(1200);
                        $("#ChartUserVsStages").width(1200);
                    }
                    $scope.UserVsSources = {
                        labels: data.data.UserVsSources.Name,
                        ref: data.data.UserVsSources.RefId,
                        series: $scope.getSourceSeries(data.data.UserVsSources.Sources),
                        data: [
                            data.data.UserVsSources.Values[0],
                            data.data.UserVsSources.Values[1],
                            data.data.UserVsSources.Values[2],
                            data.data.UserVsSources.Values[3],
                            data.data.UserVsSources.Values[4],
                            data.data.UserVsSources.Values[5],
                            data.data.UserVsSources.Values[6],
                            data.data.UserVsSources.Values[7],
                            data.data.UserVsSources.Values[8],
                            data.data.UserVsSources.Values[9],
                            data.data.UserVsSources.Values[10],
                            data.data.UserVsSources.Values[11]
                        ],
                        colors: [{
                                backgroundColor: "#218C8D"
                            }, {
                                backgroundColor: "#B9D7D3"
                            }, {
                                backgroundColor: "#FFCC66"
                            }, {
                                backgroundColor: "#717984"
                            }, {
                                backgroundColor: "#8EDC9D"
                            }, {
                                backgroundColor: "#F9E559"
                            }, {
                                backgroundColor: "#6CCECB"
                            }],
                        options: {title: {
                                display: true,
                                position: 'top',
                                text: 'Leads captured by users and the sources'
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    usePointStyle: true,
                                    fontSize: 10
                                },
                                onHover: function (e) {
                                    e.target.style.cursor = 'pointer';
                                }
                            },
                            hover: {
                                onHover: function (e) {
                                    var point = this.getElementAtEvent(e);
                                    if (point.length)
                                        e.target.style.cursor = 'pointer';
                                    else
                                        e.target.style.cursor = 'default';
                                }
                            },
                            scales: {
                                xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'No.of.Leads Captured'
                                        },
                                        ticks: {
                                            fontSize: 15,
                                        },
                                        stacked: true
                                    }],
                                yAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'User Name'
                                        },
                                        stacked: true
                                    }]
                            }

                        }
                    };
                } else
                {
                    $scope.UserVsSourcesHide = true;
                    $scope.UserVsSourcesHideError = false;
                    $scope.UserVsSources_Error = data.data.UserVsSources.Error_Message;
                }


            } else
            {
                $scope.UserHide = true;
                $scope.UserVsStagesHide = true;
                $scope.UserVsPotentialHide = true;
                $scope.UserVsSourcesHide = true;
                $scope.UserHideError = false;
                $scope.UserVsStagesHideError = false;
                $scope.UserVsPotentialHideError = false;
                $scope.UserVsSourcesHideError = false;
                $scope.User_Error = data.data.Error_Message;
                $scope.UserVsStages_Error = data.data.Error_Message;
                $scope.UserVsPotential_Error = data.data.Error_Message;
                $scope.UserVsSources_Error = data.data.Error_Message;
            }
        }, function (data) {

            console.log(data);
        });
    };
    //=========================== Account Analysis===================================
    $scope.getChartAccount = function ()
    {
        {
            $http({
                method: 'get',
                url: baseUrl + '/AccountAnalysis',
                params:
                        {
                            OrganizationId: orgId,
                            TimeValue: $scope.LeadTimeValueAccount
                        },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (data)
            {
                $scope.TopInterestdata = data.data.TopInterest;
                $scope.BottomInterestdata = data.data.BottomInterest;
                $scope.TopCustomerdata = data.data.TopCustomer;
                $scope.BottomCustomerdata = data.data.BottomCustomer;

                if (data.data.Flag === "TRUE")
                {
                    $scope.LeadAccountHide = false;
                    $scope.LeadAccountHideError = true;
                    $scope.LeadStageAccountHide = false;
                    $scope.LeadStageAccountHideError = true;
                    $scope.LeadPotentialAccountHide = false;
                    $scope.LeadPotentialAccountHideError = true;
                    $scope.CustomerGroupVsLeadsHide = false;
                    $scope.CustomerGroupVsLeadsHideError = true;
                    $scope.InterestGroupVsLeadsHide = false;
                    $scope.InterestGroupVsLeadsHideError = true;
                    if (data.data.LeadsCaputure.Flag === "TRUE")
                    {
                        $scope.LeadAccountHide = false;
                        $scope.LeadAccountHideError = true;
                        console.log(data.data.LeadsCaputure.Source);
                        $scope.LeadAccount = {
//                            labels: data.data.LeadsCaputure.Source,
//                            series: data.data.LeadsCaputure.Source,
                            labels: $scope.getSourceSeries(data.data.LeadsCaputure.Source),
                            series: $scope.getSourceSeries(data.data.LeadsCaputure.Source),
                            data: data.data.LeadsCaputure.No_of_leads,
                            colors: ["#218C8D", "#B9D7D3", "#FFCC66", "#717984", "#8EDC9D", "#F9E559", "#6CCECB"],
                            options: {title: {
                                    display: true,
                                    position: 'top',
                                    text: 'Leads captured based on Sources'
                                },
                                legend: {
                                    display: true,
                                    position: 'right',
                                    labels: {
                                        usePointStyle: true,
                                        fontSize: 10
                                    },
                                    onHover: function (e) {
                                        e.target.style.cursor = 'pointer';
                                    }
                                },
                                hover: {
                                    onHover: function (e) {
                                        var point = this.getElementAtEvent(e);
                                        if (point.length)
                                            e.target.style.cursor = 'pointer';
                                        else
                                            e.target.style.cursor = 'default';
                                    }
                                },
//                                    scales: {
//                                        xAxes: [{
//                                                scaleLabel: {
//                                                    display: true,
//                                                    labelString: 'No.of.Leads Captured'
//                                                },
//                                                //  stacked: true
//
//                                            }],
//                                        yAxes: [{
//                                                scaleLabel: {
//                                                    display: true,
//                                                    labelString: 'User Name'
//                                                },
//                                                //    stacked: true
//
//                                            }]
//                                    }

                            }

                        };
                    } else
                    {
                        $scope.LeadAccountHide = true;
                        $scope.LeadAccountHideError = false;
                        $scope.LeadAccount_Error = data.data.LeadsCaputure.Error_Message;
                    }
                    if (data.data.LeadsStages.Flag === "TRUE")
                    {
                        $scope.LeadStageAccountHide = false;
                        $scope.LeadStageAccountHideError = true;
                        $scope.LeadStageAccount =
                                {
                                    labels: data.data.LeadsStages.Stage,
                                    series: data.data.LeadsStages.Stage,
                                    data: data.data.LeadsStages.No_of_leads,
                                    colors: ["#2077B4", "#FF7F0F", "#2DA02D", "#F1C40F", "#D62829", "#F3F3F4","#F3FDFA","#E8F4A2"],
                                    options: {title: {
                                            display: true,
                                            position: 'top',
                                            text: 'Active follow-ups based on Lead Stages',
                                        },
                                        legend: {
                                            display: true,
                                            position: 'right',
                                            labels: {
                                                usePointStyle: true,
                                                fontSize: 10
                                            },
                                            onHover: function (e) {
                                                e.target.style.cursor = 'pointer';
                                            }
                                        },
                                        hover: {
                                            onHover: function (e) {
                                                var point = this.getElementAtEvent(e);
                                                if (point.length)
                                                    e.target.style.cursor = 'pointer';
                                                else
                                                    e.target.style.cursor = 'default';
                                            }
                                        },
//                                    scales: {
//                                        xAxes: [{
//                                                scaleLabel: {
//                                                    display: true,
//                                                    labelString: 'No.of.Leads Captured'
//                                                },
//                                                //  stacked: true
//
//                                            }],
//                                        yAxes: [{
//                                                scaleLabel: {
//                                                    display: true,
//                                                    labelString: 'User Name'
//                                                },
//                                                //    stacked: true
//
//                                            }]
//                                    }

                                    }

                                };
                    } else
                    {
                        $scope.LeadStageAccountHide = true;
                        $scope.LeadStageAccountHideError = false;
                        $scope.LeadStageAccount_Error = data.data.LeadsStages.Error_Message;
                    }
                    if (data.data.LeadsPotential.Flag === "TRUE")
                    {
                        $scope.LeadPotentialAccountHide = false;
                        $scope.LeadPotentialAccountHideError = true;
                        $scope.LeadPotentialAccount =
                                {
                                    labels: data.data.LeadsPotential.Potential,
                                    series: data.data.LeadsPotential.Potential,
                                    data: data.data.LeadsPotential.No_of_leads,
                                    colors: ["#57BC8A", "#539ECC", "#F2992F", "#F3F3F4"],
                                    options: {
                                        title: {
                                            display: true,
                                            position: 'top',
                                            text: 'Active follow-ups based on Lead Potential',
                                        },
                                        legend: {
                                            display: true,
                                            position: 'right',
                                            labels: {
                                                usePointStyle: true,
                                                fontSize: 10
                                            },
                                            onHover: function (e) {
                                                e.target.style.cursor = 'pointer';
                                            }
                                        },
                                        hover: {
                                            onHover: function (e) {
                                                var point = this.getElementAtEvent(e);
                                                if (point.length)
                                                    e.target.style.cursor = 'pointer';
                                                else
                                                    e.target.style.cursor = 'default';
                                            }
                                        },
//                                        scales: {
//                                            xAxes: [{
//                                                    scaleLabel: {
//                                                        display: true,
//                                                        labelString: 'Potential'
//                                                    },
//                                                    //  stacked: true
//
//                                                }],
//                                            yAxes: [{
//                                                    scaleLabel: {
//                                                        display: true,
//                                                        labelString: 'No.of.Leads'
//                                                    },
//                                                    //    stacked: true
//
//                                                }]
//                                        }

                                    }

                                };
                    } else
                    {
                        $scope.LeadPotentialAccountHide = true;
                        $scope.LeadPotentialAccountHideError = false;
                        $scope.LeadPotentialAccount_Error = data.data.LeadsPotential.Error_Message;
                    }
                    if (data.data.CustomerGroupVsLeads.Flag === "TRUE")

                    {
                        $scope.CustomerGroupVsLeadsHide = false;
                        $scope.CustomerGroupVsLeadsHideError = true;
                        if (data.data.CustomerGroupVsLeads.Customer.length <= 8)
                        {
                            $("#ChartCustomerGroupVsLeads").width(800);
                        } else if (data.data.CustomerGroupVsLeads.Customer.length >= 8 && data.data.CustomerGroupVsLeads.Customer.length <= 40)
                        {
                            $("#ChartCustomerGroupVsLeads").width(1000);
                        } else if (data.data.CustomerGroupVsLeads.Customer.length >= 40)
                        {
                            $("#ChartCustomerGroupVsLeads").width(2500);
                        }
                        $scope.CustomerGroupVsLeads = {
                            labels: data.data.CustomerGroupVsLeads.Customer,
                            series: data.data.CustomerGroupVsLeads.Stages,
                            data: [
                                data.data.CustomerGroupVsLeads.Values[0],
                                data.data.CustomerGroupVsLeads.Values[1],
                                data.data.CustomerGroupVsLeads.Values[2],
                                data.data.CustomerGroupVsLeads.Values[3],
                                data.data.CustomerGroupVsLeads.Values[4],
                                data.data.CustomerGroupVsLeads.Values[5],
								data.data.CustomerGroupVsLeads.Values[6],
								data.data.CustomerGroupVsLeads.Values[7]

                            ],
                            colors: [{
                                    backgroundColor: "#2077B4"
                                }, {
                                    backgroundColor: "#4585ed"
                                }, {
                                    backgroundColor: "#2DA02D"
                                }, {
                                    backgroundColor: "#45affd"
                                }, {
                                    backgroundColor: "#D62829"
                                }, {
                                    backgroundColor: "#45ed59"
                                },{
                                    backgroundColor: "#45edaa"
                                },{
                                    backgroundColor: "#45afed"
                                }],
                            options: {
                                title: {
                                    display: true,
                                    position: 'top',
                                    text: ['Customer Group wise Leads analysis alongside Lead Stages'],
                                    fontSize: 23,
                                },
                                legend: {
                                    display: true,
                                    position: 'right',
                                    labels: {
                                        usePointStyle: true,
                                        fontSize: 10
                                    },
                                    onHover: function (e) {
                                        e.target.style.cursor = 'pointer';
                                    }
                                },
                                hover: {
                                    onHover: function (e) {
                                        var point = this.getElementAtEvent(e);
                                        if (point.length)
                                            e.target.style.cursor = 'pointer';
                                        else
                                            e.target.style.cursor = 'default';
                                    }
                                },
                                tooltips: {
                                    callbacks: {
                                        title: function (tooltipItems, data) {
                                            var labelIndex = tooltipItems[0].index;
                                            var realLabel = data.labels[labelIndex];
                                            return realLabel;
                                        }
                                    }
                                },
                                scales: {
                                    xAxes: [{
                                            ticks: {
                                                fontSize: 15,
                                            },
                                            scaleLabel: {
                                                display: true,
                                                labelString: 'No.of.Leads Follow ups',
                                                fontSize: 20,
                                                //  fontColor:'#ff0008'
                                            },
                                            stacked: true
                                        }],
                                    yAxes: [{
                                            ticks: {
                                                callback: function (label) {


                                                    if (label.length <= 10)
                                                    {
                                                        return label;
                                                    } else
                                                    {
                                                        return label.substring(0, 10) + "..";
                                                    }

                                                }
                                            },
                                            scaleLabel: {
                                                display: true,
                                                labelString: 'Customer Groups',
                                                fontSize: 20,
                                            },
                                            stacked: true
                                        }]


                                }

                            }
                        };
                    } else

                    {
                        $scope.CustomerGroupVsLeadsHide = true;
                        $scope.CustomerGroupVsLeadsHideError = false;
                        $scope.CustomerGroupVsLeads_Error = data.data.CustomerGroupVsLeads.Error_Message;
                    }
                    if (data.data.InterestGroupVsLeads.Flag === "TRUE")
                    {
                        $scope.InterestGroupVsLeadsHide = false;
                        $scope.InterestGroupVsLeadsHideError = true;
                        //console.log(data.data.InterestGroupVsLeads.Values[0].Product.length + "dsfd");
                        if (data.data.InterestGroupVsLeads.Product.length <= 8)
                        {
                            $("#ChartInterestGroupVsLeads").width(800);
                        } else if (data.data.InterestGroupVsLeads.Product.length >= 8 && data.data.InterestGroupVsLeads.Product.length <= 40)
                        {
                            $("#ChartInterestGroupVsLeads").width(1000);
                        } else if (data.data.InterestGroupVsLeads.Product.length > 40)
                        {
                            // console.log("sdfsdfdsfsdf");
                            $("#ChartInterestGroupVsLeads").width(2000);
                        }
                        $scope.InterestGroupVsLeads = {
                            labels: data.data.InterestGroupVsLeads.Product,
                            series: data.data.InterestGroupVsLeads.Stages,
                            data: [
                                data.data.InterestGroupVsLeads.Values[0],
                                data.data.InterestGroupVsLeads.Values[1],
                                data.data.InterestGroupVsLeads.Values[2],
                                data.data.InterestGroupVsLeads.Values[3],
                                data.data.InterestGroupVsLeads.Values[4],
                                data.data.InterestGroupVsLeads.Values[5],
								 data.data.InterestGroupVsLeads.Values[6],
								  data.data.InterestGroupVsLeads.Values[7]


                            ],
                            colors: [{
                                    backgroundColor: "#2077B4"
                                }, {
                                    backgroundColor: "#4585ed"
                                }, {
                                    backgroundColor: "#2DA02D"
                                }, {
                                    backgroundColor: "#45affd"
                                }, {
                                    backgroundColor: "#D62829"
                                }, {
                                    backgroundColor: "#45ed59"
                                },{
                                    backgroundColor: "#45edaa"
                                },{
                                    backgroundColor: "#45afed"
                                }],
                            options: {
                                title: {
                                    display: true,
                                    position: 'top',
                                    text: 'Product Group wise Leads analysis alongside Lead Stages',
                                    fontSize: 23,
                                },
                                legend: {
                                    display: true,
                                    position: 'right',
                                    labels: {
                                        usePointStyle: true,
                                        fontSize: 10
                                    },
                                    onHover: function (e) {
                                        e.target.style.cursor = 'pointer';
                                    }
                                },
                                hover: {
                                    onHover: function (e) {
                                        var point = this.getElementAtEvent(e);
                                        if (point.length)
                                            e.target.style.cursor = 'pointer';
                                        else
                                            e.target.style.cursor = 'default';
                                    }
                                },
                                tooltips: {
                                    callbacks: {
                                        title: function (tooltipItems, data) {
                                            var labelIndex = tooltipItems[0].index;
                                            var realLabel = data.labels[labelIndex];
                                            return realLabel;
                                        }
                                    }
                                },
                                scales: {
                                    xAxes: [{
                                            ticks: {
                                                fontSize: 15,
                                            },
                                            scaleLabel: {
                                                display: true,
                                                labelString: 'No.of.Leads Follow ups',
                                                fontSize: 20,
                                            },
                                            stacked: true

                                        }],
                                    yAxes: [{
                                            scaleLabel: {
                                                display: true,
                                                labelString: 'Product Groups',
                                                fontSize: 20,
                                            },
                                            ticks: {
                                                fontSize: 12,
                                                callback: function (label) {

                                                    if (label.length <= 10)
                                                    {
                                                        return label;
                                                    } else
                                                    {
                                                        return label.substring(0, 10) + "..";
                                                    }


                                                }
                                            },
                                            stacked: true,
                                        }]
                                }

                            }
                        };
                    } else

                    {
                        $scope.InterestGroupVsLeadsHide = true;
                        $scope.InterestGroupVsLeadsHideError = false;
                        $scope.InterestGroupVsLeads_Error = data.data.CustomerGroupVsLeads.Error_Message;
                    }
                } else
                {
                    $scope.LeadAccountHide = true;
                    $scope.LeadAccountHideError = false;
                    $scope.LeadAccount_Error = data.data.Error_Message;
                    $scope.LeadStageAccountHide = true;
                    $scope.LeadStageAccountHideError = false;
                    $scope.LeadStageAccount_Error = data.data.Error_Message;
                    $scope.LeadPotentialAccountHide = true;
                    $scope.LeadPotentialAccountHideError = false;
                    $scope.LeadPotentialAccount_Error = data.data.Error_Message;
                    $scope.CustomerGroupVsLeadsHide = true;
                    $scope.CustomerGroupVsLeadsHideError = false;
                    $scope.CustomerGroupVsLeads_Error = data.data.Error_Message;
                    $scope.InterestGroupVsLeadsHide = true;
                    $scope.InterestGroupVsLeadsHideError = false;
                    $scope.InterestGroupVsLeads_Error = data.data.Error_Message;
                }



                $scope.onclickCustomerGroupVsLeads = function (elements, e)
                {
                    // helper function that translates an event to a position in canvas coordinates
                    var pos = Chart.helpers.getRelativePosition(e, $scope.chart);
                    // inRange is the function on the chart element that is used for hit testing
                    var intersect = elements.find(function (element) {
                        return element.inRange(pos.x, pos.y);
                    });
                    if (intersect) {
                        $scope.pageNumber = 1;
                        $scope.totalVisitors = 0;
                        $scope.totalPage = 1;
                        $scope.usersPerPage = 20;
                        //alert('You clicked ' + $scope.CustomerGroupVsLeads.labels[intersect._index] + '--' + $scope.CustomerGroupVsLeads.series[intersect._datasetIndex]);
                        $scope.Filter("Account", "1", $scope.LeadTimeValueAccount, $scope.CustomerGroupVsLeads.labels[intersect._index], $scope.CustomerGroupVsLeads.series[intersect._datasetIndex]);
                    }
                };
                $scope.onclickInterestGroupVsLeads = function (elements, e)
                {
                    // helper function that translates an event to a position in canvas coordinates
                    var pos = Chart.helpers.getRelativePosition(e, $scope.chart);
                    // inRange is the function on the chart element that is used for hit testing
                    var intersect = elements.find(function (element) {
                        return element.inRange(pos.x, pos.y);
                    });
                    if (intersect) {
                        $scope.pageNumber = 1;
                        $scope.totalVisitors = 0;
                        $scope.totalPage = 1;
                        $scope.usersPerPage = 20;
                        // alert('You clicked ' + $scope.InterestGroupVsLeads.labels[intersect._index] + '--' + $scope.InterestGroupVsLeads.series[intersect._datasetIndex]);
                        $scope.Filter("Account", "2", $scope.LeadTimeValueAccount, $scope.InterestGroupVsLeads.labels[intersect._index], $scope.InterestGroupVsLeads.series[intersect._datasetIndex]);
                    }
                };
                $scope.onclickLeadPotentialAccount = function (elements, e)
                {
                    // helper function that translates an event to a position in canvas coordinates
                    var pos = Chart.helpers.getRelativePosition(e, $scope.chart);
                    // inRange is the function on the chart element that is used for hit testing
                    var intersect = elements.find(function (element) {
                        return element.inRange(pos.x, pos.y);
                    });
                    if (intersect) {
                        $scope.pageNumber = 1;
                        $scope.totalVisitors = 0;
                        $scope.totalPage = 1;
                        $scope.usersPerPage = 20;
                        // alert('You clicked ' + $scope.LeadPotentialAccount.labels[intersect._index] + '--' + $scope.LeadPotentialAccount.series[intersect._datasetIndex]);
                        $scope.Filter("Account", "3", $scope.LeadTimeValueAccount, $scope.LeadPotentialAccount.labels[intersect._index], "");
                    }

                };
                $scope.onclickLeadStageAccount = function (elements, e)
                {
                    // helper function that translates an event to a position in canvas coordinates
                    var pos = Chart.helpers.getRelativePosition(e, $scope.chart);
                    // inRange is the function on the chart element that is used for hit testing
                    var intersect = elements.find(function (element) {
                        return element.inRange(pos.x, pos.y);
                    });
                    if (intersect) {
                        $scope.pageNumber = 1;
                        $scope.totalVisitors = 0;
                        $scope.totalPage = 1;
                        $scope.usersPerPage = 20;
                        // alert('You clicked ' + $scope.LeadStageAccount.labels[intersect._index] + '--' + $scope.LeadStageAccount.series[intersect._datasetIndex]);
                        $scope.Filter("Account", "4", $scope.LeadTimeValueAccount, $scope.LeadStageAccount.labels[intersect._index], "");
                    }

                };
                $scope.onclickLeadAccount = function (elements, e)
                {
                    // helper function that translates an event to a position in canvas coordinates
                    var pos = Chart.helpers.getRelativePosition(e, $scope.chart);
                    // inRange is the function on the chart element that is used for hit testing
                    var intersect = elements.find(function (element) {
                        return element.inRange(pos.x, pos.y);
                    });
                    if (intersect) {
                        $scope.pageNumber = 1;
                        $scope.totalVisitors = 0;
                        $scope.totalPage = 1;
                        $scope.usersPerPage = 20;
                        // alert('You clicked ' + $scope.LeadAccount.labels[intersect._index] + '--' + $scope.LeadAccount.series[intersect._datasetIndex]);
                        $scope.Filter("Account", "5", $scope.LeadTimeValueAccount, $scope.LeadAccount.labels[intersect._index], "");
                    }

                };
                $scope.onclickUser = function (elements, e)
                {
                    // helper function that translates an event to a position in canvas coordinates
                    var pos = Chart.helpers.getRelativePosition(e, $scope.chart);
                    // inRange is the function on the chart element that is used for hit testing
                    var intersect = elements.find(function (element) {
                        return element.inRange(pos.x, pos.y);
                    });
                    if (intersect) {
                        $scope.pageNumber = 1;
                        $scope.totalVisitors = 0;
                        $scope.totalPage = 1;
                        $scope.usersPerPage = 20;
                        // alert('You clicked ' + $scope.User.labels[intersect._index] + '--' + $scope.User.series[intersect._datasetIndex]);
                        $scope.Filter("User", "1", $scope.LeadTimeValueUser, $scope.User.ref[intersect._index], "");
                    }

                };
                $scope.onclickUserVsStages = function (elements, e)
                {
                    // helper function that translates an event to a position in canvas coordinates
                    var pos = Chart.helpers.getRelativePosition(e, $scope.chart);
                    // inRange is the function on the chart element that is used for hit testing
                    var intersect = elements.find(function (element) {
                        return element.inRange(pos.x, pos.y);
                    });
                    if (intersect) {
                        $scope.pageNumber = 1;
                        $scope.totalVisitors = 0;
                        $scope.totalPage = 1;
                        $scope.usersPerPage = 20;
                        // alert('You clicked ' + $scope.UserVsStages.labels[intersect._index] + '--' + $scope.UserVsStages.series[intersect._datasetIndex]);
                        $scope.Filter("User", "2", $scope.LeadTimeValueUser, $scope.UserVsStages.ref[intersect._index], $scope.UserVsStages.series[intersect._datasetIndex]);
                    }

                };
                $scope.onclickUserVsPotential = function (elements, e)
                {

                    // helper function that translates an event to a position in canvas coordinates
                    var pos = Chart.helpers.getRelativePosition(e, $scope.chart);
                    // inRange is the function on the chart element that is used for hit testing
                    var intersect = elements.find(function (element) {
                        return element.inRange(pos.x, pos.y);
                    });
                    if (intersect) {
                        $scope.pageNumber = 1;
                        $scope.totalVisitors = 0;
                        $scope.totalPage = 1;
                        $scope.usersPerPage = 20;
                        // alert('You clicked ' + $scope.UserVsPotential.labels[intersect._index] + '--' + $scope.UserVsPotential.series[intersect._datasetIndex]);
                        $scope.Filter("User", "3", $scope.LeadTimeValueUser, $scope.UserVsPotential.ref[intersect._index], $scope.UserVsPotential.series[intersect._datasetIndex]);
                    }

                };
                $scope.onclickUserVsSources = function (elements, e)
                {

                    // helper function that translates an event to a position in canvas coordinates
                    var pos = Chart.helpers.getRelativePosition(e, $scope.chart);
                    // inRange is the function on the chart element that is used for hit testing
                    var intersect = elements.find(function (element) {
                        return element.inRange(pos.x, pos.y);
                    });
                    if (intersect) {
                        $scope.pageNumber = 1;
                        $scope.totalVisitors = 0;
                        $scope.totalPage = 1;
                        $scope.usersPerPage = 20;
                        //alert('You clicked ' + $scope.UserVsSources.labels[intersect._index] + '--' + $scope.UserVsSources.series[intersect._datasetIndex]);
                        $scope.Filter("User", "4", $scope.LeadTimeValueUser, $scope.UserVsSources.ref[intersect._index], $scope.UserVsSources.series[intersect._datasetIndex]);
                    }

                };
            

            }, function (data) {

                console.log(data);
            });
        }
    };
	    
    // Get Squence
    $scope.getSourceSeries = function (sources) {
        var resultSource = [];
        var t = 0;
        var lsource = ['M_FO', 'M_QFO', 'M_BC', 'M_QR', 'W_FO', 'R_FO', 'W_EX', 'W_Enq', 'FB_L', 'iOS_FO', 'iOS_BC', 'iOS_QR'];
        var lpsource = ['Mobile Form', 'Mobile Quick Form', 'Mobile Biz', 'Mobile QR', 'Web Form', 'Digital Form', 'Import Excel', 'Website Enquiry', 'Facebook Leads', 'iOS Form', 'iOS Business Card', 'iOS QR'];
        for (var i = 0; i < sources.length; i++) {

            resultSource[t] = lpsource[lsource.indexOf(sources[i])];
            t = t + 1;
        }
        for (var i = 0; i < lpsource.length; i++) {

            if (resultSource.indexOf(lpsource[i]) <= -1) {
                resultSource[t] = lpsource[i];
                t = t + 1;
            }

        }
        return resultSource;

    };
    $scope.getStageSeries = function (sources) {
        var resultSource = [];
        var t = 0;
        var lpsource = [];
        var resprom = $http.get(Data.serviceBase() + 'organization/getCurrentStages', {params: {"organizationId": orgId}});
        resprom.then(function (response) {
            console.log(response.data);
            lpsource = response.data;

            //var lsource = ['M_FO', 'M_QFO', 'M_BC', 'M_QR', 'W_FO', 'R_FO', 'W_EX'];
            //var lpsource = $scope.cstages;
            console.log(lpsource);
            for (var i = 0; i < sources.length; i++) {

                resultSource[t] = lpsource[lpsource.indexOf(sources[i])];
                t = t + 1;
            }
            for (var i = 0; i < lpsource.length; i++) {

                if (resultSource.indexOf(lpsource[i]) <= -1) {
                    resultSource[t] = lpsource[i];
                    t = t + 1;
                }

            }
            return resultSource;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });


    };
    $scope.getPotentialSeries = function (sources) {
        var resultSource = [];
        var t = 0;
        //var lsource = ['M_FO', 'M_QFO', 'M_BC', 'M_QR', 'W_FO', 'R_FO', 'W_EX'];
        var lpsource = ['High', 'Medium', 'Low', 'Not Relevant'];
        for (var i = 0; i < sources.length; i++) {

            resultSource[t] = lpsource[lpsource.indexOf(sources[i])];
            t = t + 1;
        }
        for (var i = 0; i < lpsource.length; i++) {

            if (resultSource.indexOf(lpsource[i]) <= -1) {
                resultSource[t] = lpsource[i];
                t = t + 1;
            }

        }
        return resultSource;

    };
    $scope.backToAnanlyse = function () {
        $scope.AnalysisSection = false;
    };
    $scope.pageChanged = function (newPage) {
        // $scope.loader = true;
        // $scope.cardView = false;
        $scope.pageNumber = newPage;
        //$scope.pagination.current = newPage;
        //paginationService.setCurrentPage('paginationId', newPage);
        if ($scope.totalVisitors <= 20) {
            //console.log("Visitor Count" + $scope.totalVisitors);
            $scope.totalPage = 1;
        }
        if ($scope.totalVisitors > 20) {
            //console.log("Visitor Count 2 " + $scope.totalVisitors);

            if (($scope.totalVisitors / 20) > 1) {
                $scope.totalPage = Math.floor($scope.totalVisitors / 20);
                $scope.totalPage = $scope.totalPage + 1;
            }

        }
        /* if ($scope.totalVisitors > (Math.round($scope.totalVisitors / 20) * 20) && !$scope.fixlength) {
         $scope.totalPage = $scope.totalPage + 1;
         $scope.fixlength = true;
         }*/

        ////console.log("Current"+newPage);
        if ($scope.prevpage != newPage) {
            var FilterValues = JSON.parse(sessionStorage.getItem('Filter'));
            //var path = JSON.parse(sessionStorage.getItem('Path'));
            $scope.Filter(FilterValues[0]['Analysis'], FilterValues[0]['SubAnalysis'], FilterValues[0]['Period'], FilterValues[0]['Yaxis'], FilterValues[0]['Categories']);
            $scope.prevpage = newPage;
        }
        //$scope.getResultsPage(newPage);
    };
	$scope.Filter = function (Analysis, SubAnalysis, Period, Yaxis, Categories) {
                    //window.location.href = baseUrl + '/filterIndex';
                    var FilterValues = new Array({Organization: orgId, UserId: userId, Analysis: Analysis, SubAnalysis: SubAnalysis, Period: Period, Yaxis: Yaxis, Categories: Categories});
                    var Path = new Array({Path: 'AnalysisFilter'});

                    sessionStorage.setItem('Filter', JSON.stringify(FilterValues));
                    sessionStorage.setItem('Path', JSON.stringify(Path));

                    $http({
                        method: 'get',
                        url: baseUrl + '/AnalysisFilter',
                        params: {
                            Organization: orgId,
                            Analysis: Analysis,
                            Period: Period,
                            Yaxis: Yaxis,
                            Categories: Categories,
                            SubAnalysis: SubAnalysis,
                            UserId: userId,
                            page: $scope.pageNumber

                        },
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function (data)
                    {
                        console.log(data);
                        /* if (data.data.Flag === "False")
                         {
                         alert("Leads are Not Found Under this categories");
                         $scope.AnalysisSection = false;
                         } else
                         {
                         $scope.visitors = data.data.visitors;
                         $scope.totalVisitors = parseInt(data.data.count);
                         $scope.AnalysisSection = true;
                         
                         var d = data.data.Table;
                         demoService.SetData(d);
                         // $location.url('/AnalysisFilter')
                         }*/
                        console.log(data.data.count);
                        $scope.visitors = data.data.visitors;
                        $scope.totalVisitors = parseInt(data.data.count);
                        $scope.AnalysisSection = true;
                        if ($scope.totalVisitors <= 20) {
                            $scope.totalPage = 1;
                        } else {
                            $scope.totalPage = Math.floor($scope.totalVisitors / 20) + 1;
                        }
                    }
                    , function (data) {
                        console.log(data);
                    });
                };
    $scope.viewSepPageVisitor = function (v) {
        window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + v.id, '_blank');
    };
}
evtApp.controller('AnalysisFilter', ['$scope', '$rootScope', '$http', 'demoService', FormFilter]);

function FormFilter($scope, $rootScope, $http, demoService)
{
    var baseUrl = "https://beta.helloleads.io/index.php/app/report";

    var orgId = sessionStorage.getItem('orgId');
    var userId = sessionStorage.getItem('userId');
    var FilterValues = JSON.parse(sessionStorage.getItem('Filter'));
    var path = JSON.parse(sessionStorage.getItem('Path'));
    console.log(FilterValues);
    $http({
        method: 'get',
        url: baseUrl + "/" + path[0].Path,
        params: FilterValues[0],
        headers: {'Content-Type': 'application/json'}
    }).then(function (data)
    {
        if (data.data.Flag === "False")
        {
            alert("Leads are Not Found Under this categories");
        } else
        {
            // $scope.AnalysisSection = true;
            $scope.FilterTable = data.data.Table;
//                var d = data.data.Table;
//                demoService.SetData(d);
            // $location.path('/AnalysisFilter');
        }

    }
    , function (data) {
    });
//    $rootScope.$on("dummyevent", function () {
//        $scope.FilterSection = false;
//        var t = demoService.GetData();
//        $scope.FilterTable = t;
//    });

}

evtApp.service("demoService", function ($rootScope) {
    this.TempData = "";
    this.SetData = function (d) {
        this.TempData = d;
        $rootScope.$emit("dummyevent");
    };
    this.GetData = function () {
        return this.TempData;
    };
});