//inject angular file upload directives and services.

evtApp.controller('FileUploadCtrl',  function ($scope, Upload, $timeout) {
    $scope.uploadFiles = function(file, errFiles) {
        $scope.f = file;
        $scope.uploadedFileName = '';
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            file.upload = Upload.upload({
                url: 'http://localhost/eventHello/index.php/api/files/upload',
                data: {userfile: file} //-- CI expects the param to be called as userfile
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    console.log(response);
                    $scope.uploadedFileName = response.data.file_name;
                    
                    console.log('Saved filename is ' + $scope.uploadedFileName);
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * 
                                         evt.loaded / evt.total));
            });
        }   
    }
});