evtApp.filter('parseDate', function () {
    return function (input) {
        return new Date(input);
    };
});

evtApp.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    //console.log(text);
                    var transformedInput = text.replace(/[^0-9]/g, '');
                    //console.log(transformedInput);
                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

evtApp.filter('utctolocal', function () {

    var LeadingZero = function (val) {
        return (val < 10) ? "0" + val : val;
    },
            ToDateString = function (dateObj, dateFormat) {
                var curr_year = dateObj.getFullYear(),
                        curr_month = LeadingZero(dateObj.getMonth() + 1),
                        curr_date = LeadingZero(dateObj.getDate()),
                        curr_hour = LeadingZero(dateObj.getHours()),
                        curr_min = LeadingZero(dateObj.getMinutes()),
                        curr_sec = LeadingZero(dateObj.getSeconds()),
                        curr_ampm = "AM";
                if (curr_hour > 11) {
                    curr_ampm = "PM";
                    curr_hour = (curr_hour == 12) ? 12 : curr_hour - 12;
                }
                var timestamp = curr_year + "-" + curr_month + "-" + curr_date + " " + curr_hour + ":" + curr_min + ":" + curr_sec + " " + curr_ampm;
                return timestamp;
            },
            LocalTimeZone = function () {
                // From http://stackoverflow.com/questions/2897478/get-client-timezone-not-gmt-offset-amount-in-js
                var now = new Date().toString(),
                        timezone = now.indexOf('(') > -1 ?
                        //now.match(/\([^\)]+\)/)[0] :  // Uncomment this line to return the full time zone text
                        now.match(/\([^\)]+\)/)[0].match(/[A-Z]/g).join('') : // Uncomment this line to return the full time zone abbreviation
                        now.match(/[A-Z]{3,4}/)[0];
                if (timezone == "GMT" && /(GMT\W*\d{4})/.test(now))
                    timezone = RegExp.$1;
                return timezone;
            };

    return function (input) {
        var inputDate = new Date(input);
        var dateString = ToDateString(inputDate);
        return dateString;
    };
});
evtApp.directive('compile', ['$compile', function ($compile) {
        return function (scope, element, attrs) {
            scope.$watch(
                    function (scope) {
                        // watch the 'compile' expression for changes
                        return scope.$eval(attrs.compile);
                    },
                    function (value) {
                        // when the 'compile' expression changes
                        // assign it into the current DOM
                        element.html(value);

                        // compile the new DOM and link it to the current
                        // scope.
                        // NOTE: we only compile .childNodes so that
                        // we don't get into infinite loop compiling ourselves
                        $compile(element.contents())(scope);
                    }
            );
        };
    }]);

evtApp.directive('icompile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
      scope.$watch(
        function(scope) {
          // watch the 'compile' expression for changes
          return scope.$eval(attrs.icompile);
        },
        function(value) {
          // when the 'compile' expression changes
          // assign it into the current DOM
          element.html(value);

          // compile the new DOM and link it to the current
          // scope.
          // NOTE: we only compile .childNodes so that
          // we don't get into infinite loop compiling ourselves
          $compile(element.contents())(scope);
        }
    );
  };
}]);
evtApp.filter('ireplace', function ($filter) {

    return function (text, type, username, para1, para2,para3,para4) {
        var orginalTxt = "";
        var paramEmpty = " ";
        if (type !== 'LFUD' && type !== 'LCOM' && type !== 'RFUD') {
            orginalTxt = text.toString();
            // console.log(username);
            orginalTxt = text.replace('#User', "<b>" + username + "</b>");
           	 if (type !== 'LMAIL' && type !== 'LCALL' && type !== 'LMEET' && type !== 'LMSG' && type !== 'LWAP' && type !== 'LSMS' && type !== 'LEML' && type !== 'LCIN' && type !== 'LCOUT') {

                orginalTxt = orginalTxt.replace('#Parameter1', "<b>" + para1 + "</b>");
                orginalTxt = orginalTxt.replace('#Parameter2', "<b>" + para2 + "</b>");
            } else if (type === 'LMEET' || type === 'LMSG' || type === 'LWAP' || type === 'LSMS' || type === 'LEML' || type === 'LCIN' || type == 'LCOUT') {
                orginalTxt = orginalTxt.replace('#Parameter1', "<b>" + para1 + "</b>");
                orginalTxt = orginalTxt.replace('#Parameter2', "<b>" + para2 + "</b>");
                orginalTxt = orginalTxt.replace('#Parameter3', "<br><br>" + para3);
            }
            else if (type === 'LCALL') {
                if (para4 === "00:00 mins" || para4 === "00:00:00") {
                    if (para1 === "IN") {
                        orginalTxt = orginalTxt.replace('#Parameter1', "had an <b>" + "Incoming" + "</b> call");
                    } else if (para1 === "OUT") {
                        orginalTxt = orginalTxt.replace('#Parameter1', "had an <b>" + "Outgoing" + "</b> call");
                    } else if (para1 === "MISSED") {
                        orginalTxt = orginalTxt.replace('#Parameter1', "had a <b>" + "missed" + "</b> call");
                    } else {
                        orginalTxt = orginalTxt.replace('#Parameter1', "had a <b>" + para1 + "</b> call");
                    }
                    orginalTxt = orginalTxt.replace('#Parameter2', "<b>" + para2 + "</b>");
                    orginalTxt = orginalTxt.replace('#Parameter4', "");
                    if (para3 !== null && para3 !== '') {
                        if(para3.startsWith("https://")){
                           orginalTxt = orginalTxt.replace('#Parameter3', "<br><br>" + "<audio controls><source src='"+para3+"'</source></audio>");
                        }else{
                            orginalTxt = orginalTxt.replace('#Parameter3', "<br><br>" + para3);
                        }
                    } else {
                        orginalTxt = orginalTxt.replace('#Parameter3', "");
                    }
                } else {
                    if (para1 === "IN") {
                        orginalTxt = orginalTxt.replace('#Parameter1', "had an <b>" + "Incoming" + "</b> call for ");
                    } else if (para1 === "OUT") {
                        orginalTxt = orginalTxt.replace('#Parameter1', "had an <b>" + "Outgoing" + "</b> call for ");
                    } else if (para1 === "MISSED") {
                        orginalTxt = orginalTxt.replace('#Parameter1', "had a <b>" + "missed" + "</b> call for ");
                    } else {
                        orginalTxt = orginalTxt.replace('#Parameter1', "had a <b>" + para1 + "</b> call for ");
                    }
                    orginalTxt = orginalTxt.replace('#Parameter2', "<b>" + para2 + "</b>");
                    orginalTxt = orginalTxt.replace('#Parameter4', "<b>" + para4 + "</b>");
                    if (para3 !== null && para3 !== '') {
                        if(para3.startsWith("https://")){
                           orginalTxt = orginalTxt.replace('#Parameter3', "<br><br>" + "<audio controls><source src='"+para3+"'</source></audio>");
                        }else{
                            orginalTxt = orginalTxt.replace('#Parameter3', "<br><br>" + para3);
                        }
                    } else {
                        orginalTxt = orginalTxt.replace('#Parameter3', "");
                    }
                }
            }
			else if(type === 'QDS'){
                console.log(para3);
                orginalTxt = orginalTxt.replace('#Parameter1', "<b>" + para1 + "</b>");
                var funqtxt = 'viewQuote("'+para3+'");';
                console.log(funqtxt);
                orginalTxt = orginalTxt.replace('#Parameter2', "<span ng-click='" + funqtxt + "' style='color:#4298CF;cursor:pointer;'><b>" + para2 + "</b></span>");
            }
            else if(type === 'IDS'){
                orginalTxt = orginalTxt.replace('#Parameter1', "<b>" + para1 + "</b>");
                var funitxt = 'viewInvoice("'+para3+'");';
                orginalTxt = orginalTxt.replace('#Parameter2', "<span ng-click='" + funitxt + "' style='color:#4298CF;cursor:pointer;'><b>" + para2 + "</b></span>");
            }
			else {
                 var strtext = "";
                 var strfun = "";
                if(para1 === 'IN'){
                    strtext = "received";
                    strfun = "viewMailReceive("+para3+")";
                }else{
                    strtext = "sent";
                    strfun = "viewMailSent("+para3+")";
                }
                orginalTxt = orginalTxt.replace('#Parameter1', "<b>" + strtext + "</b>");
                
				if(para3 == null || para3 === ''){
					orginalTxt = orginalTxt.replace('#Parameter2', para2);
				}else{
					orginalTxt = orginalTxt.replace('#Parameter2', "<span ng-click='"+strfun+"' style='color:#4298CF;cursor:pointer;'><b>" + para2 + "</b></span>");
				}
            }
        } else if (type === 'LFUD' || type === 'RFUD' && type !== 'LCOM') {
            orginalTxt = text.toString();
            //console.log(para1);
            orginalTxt = text.replace('#User', "<b>" + username + "</b>");
            if (para1) {
                var datech = para1;
                datech = datech.replace(' ', 'T');
                datech = datech + "Z";
                var valdate = new Date(datech);
                var valdtstr = $filter('date')(valdate, 'dd-MMM-yyyy H:mm:ss');
            }
            orginalTxt = orginalTxt.replace('#Parameter1', "<b>" + valdtstr + "</b>");
            orginalTxt = orginalTxt.replace('#Parameter2', "<b>" + para2 + "</b>");

        }
        //console.log(orginalTxt);
        return orginalTxt;
    };
});

/*evtApp.directive('myText', ['$rootScope', function ($rootScope) {
        return {
            link: function (scope, element, attrs) {
                $rootScope.$on('addTags', function (e, val) {
                    var domElement = element;

                    if (document.selection) {

                        domElement.focus();
                        var sel = document.selection.createRange();
                        sel.text = val;
                        domElement.focus();
                    } else if (domElement.find("input").selectionStart || domElement.find("input").selectionStart === 0) {
                        var startPos = domElement.find("input").selectionStart;
                        var endPos = domElement.find("input").selectionEnd;
                        var scrollTop = domElement.find("input").scrollTop;
                        domElement.find("input").val(domElement.find("input").val.substring(0, startPos) + val + domElement.find("input").val.substring(endPos, domElement.find("input").val().length));
                        domElement.focus();
                        domElement.selectionStart = startPos + val.length;
                        domElement.selectionEnd = startPos + val.length;
                        domElement.scrollTop = scrollTop;
                    } else {
                        var tempVal = domElement.find("input").val();
                        domElement.find("input").val(tempVal + val);

                        domElement.focus();
                    }
                    //console.log(scope.eventemailtemplate.userBodyText);
                    //console.log(domElement.value);
                    scope.eventemailtemplate.userBodyText = domElement.find("input").val();
                });
            }
        }
    }]);
Old one (17-7-2018)
*/
evtApp.directive('myText', ['$rootScope', function ($rootScope) {
        return {
            link: function (scope, element, attrs) {
                $rootScope.$on('addTags', function (e, val) {
                    var domElement = element;
                    console.log(domElement[0].selectionStart)
                    if (document.selection) {

                        domElement[0].focus();
                        var sel = document.selection.createRange();
                        
                        sel.text = val;
                        domElement.focus();
                    } else if (domElement[0].selectionStart || domElement[0].selectionStart === 0) {
                        var startPos = domElement[0].selectionStart;
                        var endPos = domElement[0].selectionEnd;
                        var scrollTop = domElement[0].scrollTop;
                        element[0].value =element[0].value.substring(0, startPos) + val + element[0].value.substring(endPos, element[0].value.length);
                        domElement.focus();
                        domElement[0].selectionStart = startPos + val.length;
                        domElement[0].selectionEnd = startPos + val.length;
                        domElement[0].scrollTop = scrollTop;
                        
                    } else {
                        
                        var tempVal = element[0].value;
                        element[0].value = tempVal + val;
                        
                        element.focus();
                    }
                    //console.log(scope.eevent.smsContent);
                    //console.log(domElement.value);
                    //scope.eventemailtemplate.userBodyText = domElement.find("input").val();
                    scope.eevent.smsContent = element[0].value;
                });
            }
        }
    }]);
	evtApp.directive('myText1', ['$rootScope', function ($rootScope) {
        return {
            link: function (scope, element, attrs) {
                $rootScope.$on('addcTags', function (e, val) {
                    var domElement = element;
                    console.log(domElement[0].selectionStart)
                    if (document.selection) {
                        domElement[0].focus();
                        var sel = document.selection.createRange();

                        sel.text = val;
                        domElement.focus();
                    } else if (domElement[0].selectionStart || domElement[0].selectionStart === 0) {
                        var startPos = domElement[0].selectionStart;
                        var endPos = domElement[0].selectionEnd;
                        var scrollTop = domElement[0].scrollTop;
                        element[0].value = element[0].value.substring(0, startPos) + val + element[0].value.substring(endPos, element[0].value.length);
                        domElement.focus();
                        domElement[0].selectionStart = startPos + val.length;
                        domElement[0].selectionEnd = startPos + val.length;
                        domElement[0].scrollTop = scrollTop;

                    } else {
                        var tempVal = element[0].value;
                        element[0].value = tempVal + val;

                        element.focus();
                    }
                    //console.log(scope.eevent.smsContent);
                    //console.log(domElement.value);
                    //scope.eventemailtemplate.userBodyText = domElement.find("input").val();
                    scope.smsFilter.body = element[0].value;
                });
            }
        }
    }]);

    evtApp.directive('myText2', ['$rootScope', function ($rootScope) {
        return {
            link: function (scope, element, attrs) {
                $rootScope.$on('addTaggs', function (e, val) {
                    var domElement = element;
                    console.log(domElement[0].selectionStart)
                    if (document.selection) {

                        domElement[0].focus();
                        var sel = document.selection.createRange();
                        
                        sel.text = val;
                        domElement.focus();
                    } else if (domElement[0].selectionStart || domElement[0].selectionStart === 0) {
                        var startPos = domElement[0].selectionStart;
                        var endPos = domElement[0].selectionEnd;
                        var scrollTop = domElement[0].scrollTop;
                        element[0].value =element[0].value.substring(0, startPos) + val + element[0].value.substring(endPos, element[0].value.length);
                        domElement.focus();
                        domElement[0].selectionStart = startPos + val.length;
                        domElement[0].selectionEnd = startPos + val.length;
                        domElement[0].scrollTop = scrollTop;
                        
                    } else {
                        
                        var tempVal = element[0].value;
                        element[0].value = tempVal + val;
                        
                        element.focus();
                    }
                    //console.log(scope.eevent.smsContent);
                    //console.log(domElement.value);
                    //scope.eventemailtemplate.userBodyText = domElement.find("input").val();
                    scope.sms.hlsmessage = element[0].value;
                });
            }
        }
    }]);
    evtApp.directive('myText3', ['$rootScope', function ($rootScope) {
        return {
            link: function (scope, element, attrs) {
                $rootScope.$on('addTagggs', function (e, val) {
                    var domElement = element;
                    console.log(domElement[0].selectionStart)
                    if (document.selection) {

                        domElement[0].focus();
                        var sel = document.selection.createRange();
                        
                        sel.text = val;
                        domElement.focus();
                    } else if (domElement[0].selectionStart || domElement[0].selectionStart === 0) {
                        var startPos = domElement[0].selectionStart;
                        var endPos = domElement[0].selectionEnd;
                        var scrollTop = domElement[0].scrollTop;
                        element[0].value =element[0].value.substring(0, startPos) + val + element[0].value.substring(endPos, element[0].value.length);
                        domElement.focus();
                        domElement[0].selectionStart = startPos + val.length;
                        domElement[0].selectionEnd = startPos + val.length;
                        domElement[0].scrollTop = scrollTop;
                        
                    } else {
                        
                        var tempVal = element[0].value;
                        element[0].value = tempVal + val;
                        
                        element.focus();
                    }
                    //console.log(scope.eevent.smsContent);
                    //console.log(domElement.value);
                    //scope.eventemailtemplate.userBodyText = domElement.find("input").val();
                    scope.sms.hlsmessage = element[0].value;
                });
            }
        }
    }]);

	
evtApp.directive('addHtml', function ($compile) {
    return {
        restrict: 'AE',
        link: function (scope, element, attrs, val) {
            var html = val,
                    compiledElement = $compile(html)(scope);


            var pageElement = angular.element(document.getElementById("taTextElement6282527989875428"));
            pageElement.empty()
            pageElement.append(compiledElement);

        }
    }
});
evtApp.directive('starRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
                '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
                '\u2605' +
                '</li>' +
                '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];

                for (var i = 0; i < scope.max; i++) {
                    if (scope.ratingValue != 0) {
                        scope.stars.push({
                            filled: i < scope.ratingValue
                        });
                    }else{
                        scope.stars.push({
                            filled: false
                        });
                    }
                }

            };

            scope.toggle = function (index) {
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };

            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
        }
    }
});

evtApp.directive('ngAutocomplete', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            ngModel: '=',
            options: '=?',
            details: '=?'
        },
        link: function (scope, element, attrs, controller) {

            //options for autocomplete
            var opts
            var watchEnter = false
            //convert options provided to opts
            var initOpts = function () {

                opts = {}
                if (scope.options) {

                    if (scope.options.watchEnter !== true) {
                        watchEnter = false
                    } else {
                        watchEnter = true
                    }

                    if (scope.options.types) {
                        opts.types = []
                        opts.types.push(scope.options.types)
                        scope.gPlace.setTypes(opts.types)
                    } else {
                        scope.gPlace.setTypes([])
                    }

                    if (scope.options.bounds) {
                        opts.bounds = scope.options.bounds
                        scope.gPlace.setBounds(opts.bounds)
                    } else {
                        scope.gPlace.setBounds(null)
                    }

                    if (scope.options.country) {
                        opts.componentRestrictions = {
                            country: scope.options.country
                        };
                        scope.gPlace.setComponentRestrictions(opts.componentRestrictions)
                    } else {
                        scope.gPlace.setComponentRestrictions(null)
                    }
                }
            };

            if (scope.gPlace == undefined) {
                scope.gPlace = new google.maps.places.Autocomplete(element[0], {});
            }
            google.maps.event.addListener(scope.gPlace, 'place_changed', function () {
                var result = scope.gPlace.getPlace();
                if (result !== undefined) {
                    if (result.address_components !== undefined) {

                        scope.$apply(function () {
                            //console.log(result);
                            scope.details = result.name;

                            scope.$parent.evisitor.address1 = "";
                            scope.$parent.evisitor.website ="";
                            scope.$parent.evisitor.phone = "";
                            scope.$parent.evisitor.city = "";
                            scope.$parent.evisitor.state = "";
                            scope.$parent.evisitor.country = "";
                            scope.$parent.evisitor.zip = "";
                            
                            scope.$parent.evisitor.website = result.website;
                            scope.$parent.evisitor.phone = result.formatted_phone_number;
							
							 //Setting lat long for the given address
                             scope.$parent.evisitor.leadAddr_lat = result.geometry.location.lat();
                             scope.$parent.evisitor.leadAddr_long = result.geometry.location.lng();
							 
                            //scope.ngModel = result.name;
                            // console.log(controller);
                            controller.$setViewValue(result.name);
                            controller.$render();
                            for (var i = 0; i < result.address_components.length; i++) {
                                var addressType = result.address_components[i].types[0];
                                if (addressType === 'street_number') {
                                    scope.$parent.evisitor.address1 = result.address_components[i].long_name;
                                }
                                if (addressType === 'route') {
                                    scope.$parent.evisitor.address1 = scope.$parent.evisitor.address1 + "," + result.address_components[i].long_name;
                                }
                                if (addressType === 'locality') {
                                    scope.$parent.evisitor.city = result.address_components[i].long_name;
                                }
                                if (addressType === 'country') {
                                    scope.$parent.evisitor.country = result.address_components[i].long_name;
                                    for(var k=0;k< scope.$parent.countrymobDet.length ;k++){
                                        if(scope.$parent.countrymobDet[k]['country'] === scope.$parent.evisitor.country){
                                            //console.log(scope.$parent.countrymobDet[k]);
                                            scope.$parent.evisitor.mobileCode = scope.$parent.countrymobDet[k]['calling_code'];
                                        }
                                    }
                                }
                                if (addressType === 'administrative_area_level_1') {
                                    scope.$parent.evisitor.state = result.address_components[i].long_name;
                                }
                                if (addressType === 'postal_code') {
                                    scope.$parent.evisitor.zip = result.address_components[i].long_name;
                                }

                            }
                            if (!scope.$parent.evisitor.address1) {
                                if (result.vicinity) {
                                    scope.$parent.evisitor.address1 = result.vicinity;
                                } else {
                                    scope.$parent.evisitor.address1 = "";
                                }

                            }

                        });
                    } else {
                        if (watchEnter) {
                            console.log(result.name);

                            getPlace(result.name);
                        }
                    }
                }
            });

            /* //function to get retrieve the autocompletes first result using the AutocompleteService 
             var getPlace = function (result) {
             var autocompleteService = new google.maps.places.AutocompleteService();
             if (result.name.length > 0) {
             autocompleteService.getPlacePredictions(
             {
             input: result.name,
             offset: result.name.length
             },
             function listentoresult(list, status) {
             if (list == null || list.length == 0) {
             
             scope.$apply(function () {
             scope.details = null;
             });
             
             } else {
             var placesService = new google.maps.places.PlacesService(element[0]);
             placesService.getDetails(
             {'reference': list[0].reference},
             function detailsresult(detailsResult, placesServiceStatus) {
             
             if (placesServiceStatus == google.maps.GeocoderStatus.OK) {
             scope.$apply(function () {
             
             controller.$setViewValue(detailsResult.name);
             element.val(detailsResult.name);
             scope.details = detailsResult;
             scope.$parent.evisitor.visitorOrganizationName = detailsResult.name;
             //on focusout the value reverts, need to set it again.
             var watchFocusOut = element.on('focusout', function (event) {
             element.val(detailsResult.name);
             element.unbind('focusout')
             })
             
             });
             }
             }
             );
             }
             });
             }
             };
             
             controller.$render = function () {
             var location = controller.$viewValue;
             element.val(location);
             };
             
             //watch options provided to directive
             scope.watchOptions = function () {
             return scope.options
             };
             scope.$watch(scope.watchOptions, function () {
             initOpts();
             }, true);*/

        }
    };
});

evtApp.directive('fileDropzone', function (toaster) {
    return {
        restrict: 'A',
        
        link: function (scope, element, attrs) {
            var processDragOverOrEnter;
            processDragOverOrEnter = function (event) {
                if (event != null) {
                    event.preventDefault();
                }
                return false;
            };

            element.bind('dragover', processDragOverOrEnter);
            element.bind('dragenter', processDragOverOrEnter);

            return element.bind('drop', function (event) {
                try {

                    var files = [];
                    //scope.files = [];

                    if (event != null) {
                        event.preventDefault();
                    }

                   /* var fileCount = 0;
                    angular.forEach(event.originalEvent.dataTransfer.files, function (item) {
                        if (fileCount < 10) { //Can add a variety of file validations                              files.push(item);                         }                         fileCount++;                     });                     if (fileCount > 10) alert("You can only select up to 10 files. Please note only the first 10 will be processed.");

                            files.forEach(function (item) {
                                var reader = new FileReader();

                                reader.readAsDataURL(item);

                                reader.onload = function (evt) {
                                    //For each file gather the attributes required
                                    var newFile = {
                                        extension: "." + item.name.split('.').pop(),
                                        name: item.name,
                                        rawFile: item,
                                        size: item.size
                                    }

                                    scope.files.push(newFile);

                                };
                            });

                        }
                    });*/
                    if(scope.commentdet.comment){
                        scope.getAttachFileDetails(event.originalEvent.dataTransfer);
                    }else{
                        toaster.pop("error", "", "Please add some comments before adding files", 10000, 'trustedHtml');
                    }
                    
                    //scope.files = event.originalEvent.dataTransfer.files;
                    console.log(event.originalEvent.dataTransfer.files);
                } catch (err) {
                    console.log(err);
                }



            });

        }
    };
});
evtApp.directive('autoProtocol', function() {
        return {
            require: 'ngModel',
            link: function(scope, el, attrs, ngModel) {
                ngModel.$parsers.push(function(v) {
                    if(!/^[a-z]+:/.test(v)) {
                        v = 'http://'+v;
                    }
                    return v;
                });
            }
        }
    });

evtApp.filter('range', function () {
    return function (input, total) {
        total = parseInt(total);
        for (var i = 1; i <= total; i++)
            input.push(i);
        return input;
    };
});

evtApp.directive('myDraggable', ['$document', '$timeout', function ($document, $timeout) {
        return function (scope, element, attr) {
            var startX = 0, startY = 0, x = 0, y = 0;
            scope.updateX = 0;
            scope.updatesize = 100;
            // scope.topsize = 1;

            function zoomController(zoomtype, updatesize) {
                var default_scale = 120;
                var zoomtype = zoomtype;
                var updatesize = updatesize;
                if (zoomtype == 1 && updatesize > 275) {
                    return updatesize;
                } else if (zoomtype == 1 && updatesize < 275) {
                    return updatesize = updatesize * 1.02;
                } else if (zoomtype == 0 && updatesize > 20) {
                    return updatesize = updatesize / 1.02;
                } else {
                    return updatesize;
                }

            }

            /* mouse wheel */
            var doScroll = function (e) {
                e = window.event || e;
                var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
                $timeout(function () {
                    if (delta == 1) {
                        scope.updatesize = zoomController(1, scope.updatesize);
                    } else {
                        scope.updatesize = zoomController(0, scope.updatesize);

                    }
                }, 30);
                e.preventDefault();
            };

            if (element[0].addEventListener) {
                element[0].addEventListener("mousewheel", doScroll, false);
                element[0].addEventListener("DOMMouseScroll", doScroll, false);
            } else {
                element[0].attachEvent("onmousewheel", doScroll);
            }
            /* mouse wheel */
            scope.zoomInImage = function () {
                scope.updatesize = zoomController(1, scope.updatesize);
            }
            scope.zoomOutImage = function () {
                scope.updatesize = zoomController(0, scope.updatesize);
            }

            element[0].on('mousewheel', function (event) {
                // Prevent default dragging of selected content
                event.preventDefault();
                startX = event.pageX - x;
                startY = event.pageY - y;
                $document.on('mousemove', mousemove);
                $document.on('click', mouseup);
            });

            function mousemove(event) {
                y = event.pageY - startY;
                x = event.pageX - startX;

                scope.updateX = x;
                scope.updateY = y;

                scope.$apply();

                element.css({
                    top: y + 'px',
                    left: x + 'px'
                });
            }

            function mouseup() {
                $document.off('mousemove', mousemove);
                $document.off('click', mouseup);
            }
        };
    }]);

evtApp.filter('customSplit', function () {
    return function (input) {
        //console.log(input);
        var ar = input.split(','); // this will make string an array 
        return ar;
    };
});
evtApp.directive('showFocus', function ($timeout) {
    return function (scope, element, attrs) {
        scope.$watch(attrs.showFocus,
                function (newValue) {
                    $timeout(function () {
                        newValue && element.focus();
                    });
                }, true);
    };
});
evtApp.filter('timeago', function () {
    return function (input, p_allowFuture) {
        var substitute = function (stringOrFunction, number, strings) {
            var string = $.isFunction(stringOrFunction) ? stringOrFunction(number, dateDifference) : stringOrFunction;
            var value = (strings.numbers && strings.numbers[number]) || number;
            return string.replace(/%d/i, value);
        },
                nowTime = (new Date()).getTime(),
                date = (new Date(input)).getTime(),
                //refreshMillis= 6e4, //A minute
                allowFuture = p_allowFuture || false,
                strings = {
                    prefixAgo: null,
                    prefixFromNow: null,
                    suffixAgo: "ago",
                    suffixFromNow: "from now",
                    seconds: "less than a minute",
                    minute: "about a minute",
                    minutes: "%d minutes",
                    hour: "about an hour",
                    hours: "about %d hours",
                    day: "a day",
                    days: "%d days",
                    month: "about a month",
                    months: "%d months",
                    year: "about a year",
                    years: "%d years"
                },
        dateDifference = nowTime - date,
                words,
                seconds = Math.abs(dateDifference) / 1000,
                minutes = seconds / 60,
                hours = minutes / 60,
                days = hours / 24,
                years = days / 365,
                separator = strings.wordSeparator === undefined ? " " : strings.wordSeparator,
                // var strings = this.settings.strings;
                prefix = strings.prefixAgo,
                suffix = strings.suffixAgo;

        if (allowFuture) {
            if (dateDifference < 0) {
                prefix = strings.prefixFromNow;
                suffix = strings.suffixFromNow;
            }
        }

        words = seconds < 45 && substitute(strings.seconds, Math.round(seconds), strings) ||
                seconds < 90 && substitute(strings.minute, 1, strings) ||
                minutes < 45 && substitute(strings.minutes, Math.round(minutes), strings) ||
                minutes < 90 && substitute(strings.hour, 1, strings) ||
                hours < 24 && substitute(strings.hours, Math.round(hours), strings) ||
                hours < 42 && substitute(strings.day, 1, strings) ||
                days < 30 && substitute(strings.days, Math.round(days), strings) ||
                days < 45 && substitute(strings.month, 1, strings) ||
                days < 365 && substitute(strings.months, Math.round(days / 30), strings) ||
                years < 1.5 && substitute(strings.year, 1, strings) ||
                substitute(strings.years, Math.round(years), strings);

        return $.trim([prefix, words, suffix].join(separator));
        // conditional based on optional argument
        // if (somethingElse) {
        //     out = out.toUpperCase();
        // }
        // return out;
    }
});


evtApp.filter('truncate', function () {
    return function (text, length, end) {
        if (isNaN(length))
            length = 10;

        if (end === undefined)
            end = "...";
        if (text !== null) {
            if (text.length <= length || text.length - end.length <= length) {
                return text;
            } else {
                return String(text).substring(0, length - end.length) + end;
            }
        }
    };
});

evtApp.directive('rotate', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(attrs.degrees, function (rotateDegrees) {
                console.log(rotateDegrees);
                console.log(element);
                var r = 'rotate(' + rotateDegrees + 'deg)';
                var trans = "all 0.3s ease-out";
                element.css({
                    "-webkit-transform": r,
                    "-moz-transform": r,
                    "-o-transform": r,
                    "msTransform": r,
                    "transform": r,
                    "-webkit-transition": trans,
                    "-moz-transition": trans,
                    "-o-transition": trans,
                    "transition": trans
                });
            });
        }
    }
});

function parseDate(input) {
    if (input) {
        var parts = input.split('-');
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
}

evtApp.filter("dateFilter", function () {
    return function (items, from, to) {
        if (from != 0) {
            var df = parseDate(from.toString());
            var dt = parseDate(to.toString());
            var result = [];
            for (var i = 0; i < items.length; i++) {
                var tf = new Date(parseDate(items[i].nextFollow) * 1000),
                        tt = new Date(parseDate(items[i].nextFollow) * 1000);
                if (tf >= df && tt <= dt) {
                    result.push(items[i]);
                }
            }
            return result;
        } else {
            return items;
        }
    };
});

//Date conversion filter
evtApp.filter('localDate', function() {

  // In the return function, we must pass in a single parameter which will be the data we will work on.
  // We have the ability to support multiple other parameters that can be passed into the filter optionally
  return function(input) {
      //console.log(input);

    //var output="";
     var datorg = input;
     if (input) {
           
            datorg = datorg.replace(' ', 'T');
            datorg = datorg + "Z";
            var valdate = new Date(datorg);
            
           
        }

    // Do filter work here

    return valdate;

  };

});





evtApp.directive('capitalizeFirst', function ($parse) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var capitalize = function (inputValue) {
                if (inputValue === undefined) {
                    inputValue = '';
                }
                var capitalized = inputValue.charAt(0).toUpperCase() +
                        inputValue.substring(1);
                if (capitalized !== inputValue) {
                    modelCtrl.$setViewValue(capitalized);
                    modelCtrl.$render();
                }
                return capitalized;
            }
            modelCtrl.$parsers.push(capitalize);
            capitalize($parse(attrs.ngModel)(scope)); // capitalize initial value
        }
    };
});
evtApp.service('visitorShareData', function ($window) {
    var KEY = 'visitors';

    var addVisitorData = function (newObj) {
        var mydata = [];

        mydata.push(newObj);
        localStorage.setItem(KEY, JSON.stringify(mydata));
    };

    var getVisitorData = function () {
        var mydata = localStorage.getItem(KEY);
        if (mydata) {
            mydata = JSON.parse(mydata);
        }
        return mydata || [];
    };

    return {
        addVisitorData: addVisitorData,
        getVisitorData: getVisitorData
    };
});
evtApp.filter('addtargetblank', function () {
    return function (x) {
        var tree = angular.element('<span><br>' + x + '</span>');//defensively wrap in a div to avoid 'invalid html' exception
        tree.find('a').attr('target', '_blank'); //manipulate the parse tree
        return angular.element('<span>').append(tree).html(); //trick to have a string representation
    }
});

evtApp.directive("ngFileSelect", function () {

    return {
        link: function ($scope, el) {

            el.bind("change", function (e) {

                $scope.file = (e.srcElement || e.target).files[0];
                $scope.getFile();
            })

        }

    };
});

evtApp.directive("ngFileCsvSelect", function () {

    return {
        link: function ($scope, el) {

            el.bind("change", function (e) {


                $scope.filecsv = (e.srcElement || e.target).files[0];
                $scope.getcsvFile();
            })

        }

    };
});

evtApp.factory('beforeUnload', function ($rootScope, $window) {
    // Events are broadcast outside the Scope Lifecycle

    $window.onbeforeunload = function (e) {
        var confirmation = {};
        var event = $rootScope.$broadcast('onBeforeUnload', confirmation);
        if (event.defaultPrevented) {
            return confirmation.message;
        }
    };

    $window.onunload = function () {
        $rootScope.$broadcast('onUnload');
    };
    return {};
})
evtApp.run(function (beforeUnload) {
    // Must invoke the service at least once
});


