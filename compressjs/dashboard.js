evtApp.controller('dashboardCtrl', function ($scope, $rootScope, Data, $filter, $uibModal, $sce, $http) {
    $scope.$on('$locationChangeStart', function (event) {
        var myHistory = [];

        window.history.pushState(myHistory, "<name>", "dashboard");
        event.preventDefault();

    });
	$scope.orgDetails = {};

    $scope.setCategory = function (categ) {
        $scope.selectedCategories = categ;
        $scope.selCategname = categ.name;
    };

    $scope.getCurrenyDetails = function () {
        // --------------------------------------------------------------- Getting Currency details ----------------------------------------------------
        var resprom = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

        resprom.then(function (response) {
            if (response.data) {
                $scope.orgDetails = response.data[0];
            }

        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    }

    $rootScope.title = "Dashboard";
    $rootScope.addButtonhtml = '';
	$scope.roleId = sessionStorage.getItem('roleId');
    $scope.organizationId = sessionStorage.getItem('orgId');
	$scope.orgCurrency = sessionStorage.getItem('orgCurrency');
	$scope.firstName = sessionStorage.getItem('firstName');
	$scope.firstTimeFlag = false;
	$scope.emptyuser = {"id":undefined};
    $scope.emptyvisitor = {"id":undefined};
    $scope.emptyevent = {"id":undefined};
    if (sessionStorage.getItem("xtime") === "1") {
        $rootScope.firstProfile = true;
    } else {
        $rootScope.firstProfile = false;
    }

    $rootScope.dashBoardQualifier = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('dashboard/setQualifierCount', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
     }
	
	$scope.redirectToQualifier = function () {
        window.open(Data.webAppBase() + 'account/qualifier', "_self");
    };

	
	$scope.StageCustom = function () {
        var stageCustom = $http.get(Data.serviceBase() + '/organization/leadStagesCount', {params: {"orgId": $scope.organizationId}});
        stageCustom.then(function (response) {
            console.log(response);
            if (response.data > 0) {
                // Checking the stage customization
                var modalInstance = $uibModal.open({
                    templateUrl: './stageCustomLoad',
                    controller: 'stageCustomCtrl',
                    size: 'md',
                    backdrop: 'static',
                    keyboard: false,
                });
                modalInstance.result.then(function (i) {
                    if (i) {

                    }
                });
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    };
	//feedback.feedbackCheck($scope.organizationId);
    /**
     * Options for Doughnut chart
     */

    this.doughnutOptions = {
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 45, // This is 0 for Pie charts
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false
    };
    $scope.checkDate = function (dateString1, dateString) {
        var daysAgo = new Date();
        daysAgo.setDate(daysAgo.getDate());
        //console.log((new Date(dateString) >= daysAgo) && (new Date(dateString1) <= daysAgo));
        return (new Date(dateString) > daysAgo && new Date(dateString1) < daysAgo);
    };
	$scope.firstTimeShow = true;
    $scope.loader = true;
    sessionStorage.setItem('menuact', '1');
    $rootScope.menuact = '1';
    //Setting the label to filter on vissitor
    sessionStorage.setItem('filterlabel', "");
	
	// Walk through popups and conditions
    $scope.lcount = sessionStorage.getItem('lcount');
    $scope.wcount = sessionStorage.getItem('wcount');
    $scope.chkCount = sessionStorage.getItem('chkcount');
    $scope.pchkcount = sessionStorage.getItem('pchkcount') === '1'?sessionStorage.getItem('pchkcount'):'0';
	$scope.aType = sessionStorage.getItem('accountType');
    $scope.PEDate = sessionStorage.getItem('PEDate');
         
    //console.log($scope.lcount); 
    if ($scope.lcount === '0' && ($scope.wcount === '0' || $scope.wcount === null)) {
        var modalInstance = $uibModal.open({
            templateUrl: './firstTimeBoarding',
            controller: 'firstTimeBoardingCtrl',
            size: 'lg',
            backdrop: 'static',
			keyboard: false,
        });
        modalInstance.result.then(function (i) {
            if (i) {
                sessionStorage.setItem('wcount', '1');
                $scope.firstTimeFlag = true;
            }
        });

    }
    // Comapre the date difference
   
    var wlcount = ['2','5','9','12','16','19'];
    console.log(wlcount.indexOf($scope.lcount));
    if (wlcount.indexOf($scope.lcount) >= 0 && ($scope.chkCount === '0' || $scope.chkCount === null)) {
        var walkthroughresp = $http.get(Data.serviceBase() + '/organization/walkThroughContent', {params: {"lcount":$scope.lcount,"firstName":$scope.firstName}});
     walkthroughresp.then(function (response) {
         console.log(response);
        $scope.wcontent = response;
        var modalInstance = $uibModal.open({
            templateUrl: './walkThroughPopup',
            controller: 'walkThroughPopupCtrl',
            size: 'lg',
            backdrop: 'static',
			keyboard: false,
            resolve: {
                        popupContent: function () {
                            console.log($scope.wcontent);
                            return  $scope.wcontent;
                        }
                  }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                sessionStorage.setItem('chkcount', '1');
                $scope.firstTimeFlag = true;
            }
        });
        }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
    }
	if ($scope.pchkcount === '0') {
        var pwalkthroughresp = $http.get(Data.serviceBase() + '/organization/trialPaymentPopup', {params: {"orgId": $scope.organizationId}});
        pwalkthroughresp.then(function (response) {
            console.log(response);
            if (response.data.status === 'success') {
                $scope.pwcontent = response.data;
                var modalInstance = $uibModal.open({
                    templateUrl: './paymentPROPopup',
                    controller: 'PaymentPopupCtrl',
                    size: 'md',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        popupContent: function () {
                            console.log($scope.pwcontent);
                            return  $scope.pwcontent;
                        }
                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        sessionStorage.setItem('pchkcount', '1');
                        $scope.firstTimeFlag = true;
                        $scope.orgVal = {"orgId": $scope.organizationId, "ftime": $scope.pwcontent.ftime};
                        /*  Data.put('organization/payProResp', $scope.orgVal).then(function (result) {
                         if (result.status !== 'error') {
                         
                         
                         } else {
                         console.log(result);
                         }
                         });*/
                    }
                });
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    }
	 // For premium members popup
    var premiumType = sessionStorage.getItem('accountType');
    if ($scope.pchkcount === '0' && premiumType !== 'FR') {
        var pwalkthroughresp = $http.get(Data.serviceBase() + '/organization/premiumPopup', {params: {"orgId": $scope.organizationId}});
        pwalkthroughresp.then(function (response) {
            console.log(response);
            if (response.data.status === 'success') {

                var modalInstance = $uibModal.open({
                    templateUrl: './premiumEngPopUp',
                    controller: 'premiumEngPopUpCtrl',
                    size: 'md',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        msg: function () {
                            return response.data;
                        }

                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        sessionStorage.setItem('pchkcount', '1');
                    }
                });
            }
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    }
	/*var dateone = new Date($scope.PEDate); 
    var datetwo = new Date();
    var dateDiff = Math.round((datetwo - dateone)  / 1000 / 60 / 60 / 24);
	if((dateDiff === 8 || dateDiff === 13) && $scope.pchkcount === '0'){
        var pwalkthroughresp = $http.get(Data.serviceBase() + '/organization/trialPaymentPopup', {params: {"dcount": dateDiff,"plan":$scope.aType,"firstName": $scope.firstName}});
            pwalkthroughresp.then(function (response) {
                console.log(response);
                $scope.pwcontent = response;
                var modalInstance = $uibModal.open({
                    templateUrl: './paymentPROPopup',
                    controller: 'PaymentPopupCtrl',
                    size: 'lg',
                    backdrop: 'static',
					keyboard: false,
                    resolve: {
                        popupContent: function () {
                            console.log($scope.pwcontent);
                            return  $scope.pwcontent;
                        }
                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        sessionStorage.setItem('pchkcount', '1');
                        $scope.firstTimeFlag = true;
                    }
                });
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
    }*/
   /* if ($scope.aType === 'FR' && $scope.pchkcount === '0') {
        if ($scope.lcount % 4 === 0) {
            var fwalkthroughresp = $http.get(Data.serviceBase() + '/organization/trialPaymentPopup', {params: {"dcount":$scope.lcount,"plan":$scope.aType,"firstName": $scope.firstName}});
            fwalkthroughresp.then(function (response) {
                console.log(response);
                $scope.fwcontent = response;
                var modalInstance = $uibModal.open({
                    templateUrl: './paymentPROPopup',
                    controller: 'PaymentPopupCtrl',
                    size: 'lg',
                    backdrop: 'static',
					keyboard: false,
                    resolve: {
                        popupContent: function () {
                            console.log($scope.fwcontent);
                            return  $scope.fwcontent;
                        }
                    }
                });
                modalInstance.result.then(function (i) {
                    if (i) {
                        sessionStorage.setItem('pchkcount', '1');
                        $scope.firstTimeFlag = true;
                    }
                });
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });


        }


    }*/
    /*  var serviceresp = $http.get(Data.serviceBase() + '/service/service', {params: {"key": "organizationId", "value": $scope.organizationId}});
     serviceresp.then(function (response) {
     console.log(response.data);
     $scope.service = response.data;
     if (response.data.length > 0) {
     sessionStorage.setItem("service", "1");
     } else {
     sessionStorage.setItem("service", "");
     }
     
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
     $scope.formatDate = function (date) {
     var dateOut = new Date(date);
     return dateOut;
     };
     var categoryresp = $http.get(Data.serviceBase() + '/category/category', {params: {"key": "organizationId", "value": $scope.organizationId}});
     categoryresp.then(function (response) {
     console.log(response.data);
     $scope.categorie = response.data;
     if (response.data.length > 0) {
     sessionStorage.setItem("category", "1");
     } else {
     sessionStorage.setItem("category", "");
     }
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });
     
     var userresp = $http.get(Data.serviceBase() + '/user/user', {params: {key: "organizationId", "value": $scope.organizationId}});
     
     $scope.countusers = {};
     userresp.then(function (response) {
     $scope.countusers = response.data;
     if (response.data.length > 1) {
     sessionStorage.setItem("cUsers", "1");
     } else {
     sessionStorage.setItem("cUsers", "");
     }
     // $scope.loader = false;
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });*/
    //Follow up deatils for current user
    $scope.curuserId = sessionStorage.getItem("userId");

   /* $scope.curr = new Date; // get current date
    $scope.first = 0; // First day is the day of the month - the day of the week
    $scope.last = 0; // last day is the first day + 6
    $scope.firstday = 0;
    $scope.lastday = 0;
    $scope.currDate = '';
    $scope.daterange = function (dt) {

        if (dt === 1) {
            $scope.first = $scope.curr.getDate(); // First day is the day of the month - the day of the week
            $scope.last = $scope.curr.getDate(); // last day is the first day + 6
            $scope.firstday = new Date($scope.curr.setDate($scope.first));
            $scope.lastday = new Date($scope.curr.setDate($scope.last));
            console.log($scope.firstday);
            $scope.currDate = $scope.curr.getDate();
        }
        if (dt === 7) {
            $scope.first = $scope.curr.getDate(); // First day is the day of the month - the day of the week
            $scope.last = $scope.first + 6; // last day is the first day + 6
            $scope.firstday = new Date($scope.curr.setDate($scope.first));
            $scope.lastday = new Date($scope.curr.setDate($scope.last));
            console.log($scope.firstday);
        }
        if (dt === 15) {
            $scope.first = $scope.curr.getDate(); // First day is the day of the month - the day of the week
            $scope.last = $scope.first + 14; // last day is the first day + 6
            $scope.firstday = new Date($scope.curr.setDate($scope.first));
            $scope.lastday = new Date($scope.curr.setDate($scope.last));
            console.log($scope.firstday);

        }



    };

    $scope.dateR = function () {
        return function (fv) {
            t = fv.nextFollow;
            var a = t.split(" ");
            var date = a[0];
            var time = a[1];
            var d = new Date().toString;
           
            $scope.formattedDate =   $filter('date')(d,"yyyy-mm-dd");
           
            var tdate =  $scope.formattedDate;
            console.log(fv.nextFollow.split(" ")[0] + "---" + d);
            return fv.nextFollow.split(" ")[0] ===  d;
        }
    }*/

$scope.followUpVisitor = function () {
    var followupresp = $http.get(Data.serviceBase() + '/visitor/followUpDet', {params: {"userId": $scope.curuserId, "organizationId": $scope.organizationId}});
    $scope.followupVisitors = {};
    followupresp.then(function (response) {
        $scope.followupVisitors = response.data;
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
}

   /* var followupassignresp = $http.get(Data.serviceBase() + '/visitor/followUpAssignDet', {params: {"userId": $scope.curuserId, "organizationId": $scope.organizationId}});
    $scope.followupAssigned = {};
    followupassignresp.then(function (response) {
        $scope.followupAssigned = response.data;
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });*/
	$scope.countvisitor = 0;
    $scope.countusers = 0;
    $scope.countevents = 0;
    $scope.countfollow = 0;
    var orgresp = $http.get(Data.serviceBase() + '/organization/orgFirstTimeCount', {params: {"organizationId": $scope.organizationId}});
    $scope.loader = true;

    orgresp.then(function (response) {
        $scope.countvisitor = response.data.visitorCount;
        $scope.countusers = response.data.userCount;
        $scope.countevents = response.data.eventCount;
        $scope.countfollow = response.data.followCount;
        if ($scope.countvisitor > 0 && $scope.countfollow > 0) {
            $scope.firstTimeShow = false;
        }else{
             sessionStorage.setItem('vcheck','');
             $scope.firstTimeShow = true;
        }
        $scope.loader = false;
        // $scope.loader = false;
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });

    // UBT Transaction
    $rootScope.dashBoardEvent = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('dashboard/setEventCount', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
     }
    // End UBT Transaction
	
	 $rootScope.addDashboardevent = function (p, size) {



        var modalInstance = $uibModal.open({
            templateUrl: './modifyEvent',
            controller: 'editeventCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                eevent: function () {
                    return p;
                },
                fields: function () {
                    return $rootScope.fields;
                },
                services: function () {
                    return $rootScope.services;
                },
                categories: function () {
                    return $rootScope.categories;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.countevents = 1;
            }
        });

    };

    $rootScope.dashBoardVisitor = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('dashboard/setVisitorCount', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
     }


    $rootScope.adddashboardvisitor = function (p, size) {

        var modalInstance = $uibModal.open({
            templateUrl: './modifyVisitor',
            controller: 'editvisitorCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                evisitor: function () {
                    return p;
                },
                tabsel: function () {
                    return "NA";
                },
                currententId: function () {

                    return 0;

                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.countvisitor = 1;

            }
        });

    };


    $rootScope.dashBoardUser = function () {   
     $scope.cEmail = sessionStorage.getItem('userEmail');
     $scope.email = {"cEmail":$scope.cEmail};
         Data.post('dashboard/setUserCount', $scope.email).then(function (result) {
         console.log('Email '+$scope.cEmail);
         });
     }
     

    $rootScope.adddashboarduser = function (user) {


        var modalInstance = $uibModal.open({
            templateUrl: './modifyUser',
            controller: 'modifyUserCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                user: function () {
                    return user;
                }

            }
        });
        modalInstance.result.then(function (i) {
            if (i) {
                $scope.countusers = 1;
            }
        });

    };
	

    $scope.viewvisitor = function (p, label, size) {
        console.log(p.id);
        console.log(label);
        if (!p.firstName && (p.email || p.mobile)) {
            return false;
        }
        if (label === 'assigne') {
            window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + p.id + '&assignee=' + p.gAssigne, "_self");
        } else {
            window.open(Data.webAppBase() + 'account/viewLead?visitorId=' + p.id + '&assignTo=' + p.gAssignTo, "_self");
        }
        /*  var valueAdd = $http.get(Data.serviceBase() + 'visitor/getvisitorValueAdds', {params: {"visitorId": p.id}});
         valueAdd.then(function (response) {
         $scope.valueAdd = response.data[0];
         console.log(response.data[0]);
         var visitorComment = $http.get(Data.serviceBase() + 'visitor/getvisitorComments', {params: {"visitorId": p.id}});
         visitorComment.then(function (responseC) {
         $scope.vComments = responseC.data;
         
         
         var modalInstance = $uibModal.open({
         templateUrl: './viewVisitor',
         controller: 'viewvisitorCtrl',
         size: "lg",
         resolve: {
         vvisitor: function () {
         if (p.nextFollow){
         p.nextFollow = $scope.formatDate(p.nextFollow);
         }
         return p;
         },
         vvisitors: function () {
         return $scope.followupVisitors;
         },
         valueAdd: function () {
         return $scope.valueAdd;
         },
         vComments: function () {
         return $scope.vComments;
         }
         }
         });
         modalInstance.result.then(function (i) {
         if (i) {
         $scope.refresh();
         }
         });
         }, function (response) {
         console.log('Error occured -- ');
         console.log(response);
         });
         }, function (response) {
         console.log('Error occured -- ');
         console.log(response);
         });*/

    };
    if (!sessionStorage.getItem("cFirstName")) {
        var orgsresp = $http.get(Data.serviceBase() + '/organization/organization', {params: {key: "id", "value": $scope.organizationId}});

        $scope.orgs = {};
        orgsresp.then(function (response) {

            if (response.data.length > 0) {
                $scope.orgs = response.data[0];
                if ($scope.orgs['contactEmail'] !== '') {
                    sessionStorage.setItem("cEmail", $scope.orgs['contactEmail']);
                } else {
                    sessionStorage.setItem("cEmail", '');
                }
                if ($scope.orgs['contactFirstName'] !== '') {
                    sessionStorage.setItem("cFirstName", $scope.orgs['contactFirstName']);
                } else {
                    sessionStorage.setItem("cFirstName", '');
                }
                if ($scope.orgs['contactLastName'] !== '') {
                    sessionStorage.setItem("cLastName", $scope.orgs['contactLastName']);
                } else {
                    sessionStorage.setItem("cLastName", '');
                }
                if ($scope.orgs['displayName'] !== 'My Organization name') {
                    sessionStorage.setItem("cOrgName", $scope.orgs['displayName']);
                } else {
                    sessionStorage.setItem("cOrgName", '');
                }
            }
            //$scope.loader = false;
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });
    }

    $scope.switchTab = function (tabId) {
        $scope.cEmail = sessionStorage.getItem('userEmail');
        $scope.email = {"cEmail":$scope.cEmail};
        if (tabId === 1) {
            $scope.tab = 1;
            angular.element(document.getElementById("tab-1")).addClass("active");
            angular.element(document.getElementById("tab-2")).removeClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
			angular.element(document.getElementById("tab-5")).removeClass("active");
			angular.element(document.getElementById("tab-6")).removeClass("active");
            Data.post('dashboard/setTabOne', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else if (tabId === 2) {
            $scope.tab = 2;
            angular.element(document.getElementById("tab-2")).addClass("active");
            angular.element(document.getElementById("tab-1")).removeClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
			angular.element(document.getElementById("tab-5")).removeClass("active");
			angular.element(document.getElementById("tab-6")).removeClass("active");
            Data.post('dashboard/setTabTwo', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        } else if (tabId === 4) {
            $scope.tab = 4;
            angular.element(document.getElementById("tab-2")).removeClass("active");
            angular.element(document.getElementById("tab-1")).removeClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
            angular.element(document.getElementById("tab-4")).addClass("active");
			angular.element(document.getElementById("tab-5")).removeClass("active");
			angular.element(document.getElementById("tab-6")).removeClass("active");
             Data.post('dashboard/setTabFour', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        }
		else if (tabId === 5) {
            $scope.tab = 5;
            angular.element(document.getElementById("tab-2")).removeClass("active");
            angular.element(document.getElementById("tab-1")).removeClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
            angular.element(document.getElementById("tab-5")).addClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
			angular.element(document.getElementById("tab-6")).removeClass("active");
             Data.post('dashboard/setTabFive', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        }else if (tabId === 6) {
            $scope.tab = 6;
            angular.element(document.getElementById("tab-2")).removeClass("active");
            angular.element(document.getElementById("tab-1")).removeClass("active");
            angular.element(document.getElementById("tab-3")).removeClass("active");
            angular.element(document.getElementById("tab-5")).removeClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
            angular.element(document.getElementById("tab-6")).addClass("active");
            Data.post('dashboard/setTabSix', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        }		
		else {
            $scope.tab = 3;
            angular.element(document.getElementById("tab-3")).addClass("active");
            angular.element(document.getElementById("tab-1")).removeClass("active");
            angular.element(document.getElementById("tab-2")).removeClass("active");
            angular.element(document.getElementById("tab-4")).removeClass("active");
			angular.element(document.getElementById("tab-5")).removeClass("active");
			angular.element(document.getElementById("tab-6")).removeClass("active");
            Data.post('dashboard/setTabThree', $scope.email).then(function (result) {
                console.log('Email '+$scope.cEmail);
            });
        }

    };
	$scope.eventVisitor = {};
    $scope.eventSelected = {};
    $scope.events = [];
    var page = 0;
    $scope.pageindex = 0;

    console.log("Organization Id is " + $scope.organizationId);
    var resprom = $http.get(Data.serviceBase() + '/dashboard/eventHistory', {params: {"organizationId": $scope.organizationId}});
    resprom.then(function (response) {
        $scope.events = response.data;
        $scope.eventHistory = angular.copy($scope.events);
        console.log(response.data[0]);
        //$scope.eventSelected = response.data[0];
        /*if (sessionStorage.getItem("selpage") && $scope.events.length >= sessionStorage.getItem("selpage")) {

            page = sessionStorage.getItem("selpage");
            if (page > 0) {
                $scope.pageindex = sessionStorage.getItem("selpage");
            }
            //console.log("Original page " + page);
            $scope.eventChange($scope.events[page]);
        } else {
            sessionStorage.removeItem("selpage")
            $scope.eventChange(response.data[0]);
        }*/
		//console.log(localStorage.getItem("curEventId"));
        if (localStorage.getItem("curEventId") && localStorage.getItem("curEventId") != '0') {
            //console.log("Inside if");
            var tempEventId = localStorage.getItem("curEventId");
            angular.forEach($scope.events, function (d) {
                if (tempEventId === d.id) {
                    $scope.eventChange(d);
                }
            });
        } else {
            //console.log("Inside else");
            sessionStorage.removeItem("selpage");
            $scope.eventChange(response.data[0]);
        }

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
    $scope.eventNext = function () {

        if (page < $scope.events.length - 1) {

            page = parseInt(page) + 1;
            console.log("Next");
            console.log(page);
            $scope.pageindex = page;
            console.log($scope.events.length);
            $scope.eventChange($scope.events[page]);
        }

    }
    $scope.eventPrevious = function () {

        if (page >= 1 && page !== 0) {
            page = parseInt(page) - 1;
            console.log("Previous");
            console.log(page);
            $scope.pageindex = page;
            $scope.eventChange($scope.events[page]);
        }
    }
    /* var resprom1 = $http.get(Data.serviceBase() + '/dashboard/eventHistory', {params: {"organizationId": $scope.organizationId}});
     $scope.eventHistory = {};
     resprom1.then(function (response) {
     $scope.eventHistory = response.data;
     
     console.log($scope.eventHistory);
     }, function (response) {
     console.log('Error happened -- ');
     console.log(response);
     });*/
    /*var resprom2 = $http.get(Data.serviceBase() + '/dashboard/eventUpcoming', {params: {"organizationId": $scope.organizationId}});
    $scope.eventUpcoming = {};
    resprom2.then(function (response) {
        $scope.eventUpcoming = response.data;
        console.log($scope.eventUpcoming);
    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });*/
    $scope.showvisitor = function (eventId, eventName, eventsend) {
        localStorage.setItem('curEventId', eventId);
        localStorage.setItem('curEventName', eventName);
        localStorage.setItem('curEventsend', eventsend);
        sessionStorage.setItem('menuact', '2');
        localStorage.setItem('filters', "");
        sessionStorage.setItem("selpage", page);
        window.open(Data.webAppBase() + 'account/manageVisitor', "_self");
    };

    $scope.filterVisitor = function (label, index) {
        localStorage.setItem('curEventId', $scope.eventSelected.id);
        localStorage.setItem('curEventName', $scope.eventSelected.name);
        localStorage.setItem('curEventsend', $scope.eventSelected.sendGreet);
		if (index !== 'interest' && index !== 'category' && index !== 'potential') {
            //console.log('Inside Stringify');
            sessionStorage.setItem('filterindex', index);
            sessionStorage.setItem('filterlabel', JSON.stringify(label));
        } else {
            sessionStorage.setItem('filterindex', index);
            sessionStorage.setItem('filterlabel', label);
        }
        
        localStorage.setItem('filters', "");
        sessionStorage.setItem('menuact', '2');
        sessionStorage.setItem("selpage", page);
        //window.open(Data.webAppBase() + 'account/manageVisitor', "_blank");
        window.location.href = Data.webAppBase() + 'account/manageVisitor';
    };
	// Load the map plugin 
    var map, infoWindow;
    var markers = [];
    var addr = {};
    // map config
    var mapOptions = {
        center: new google.maps.LatLng(0, 0),
        zoom: 2.4,
        scrollwheel: false
    };

    // init the map
    $scope.initMap = function () {
        if (map === void 0) {
            map = new google.maps.Map(document.getElementById("devMap"), mapOptions);
        }
    };
	
	
    var potentialVcount = {};
    var potential = [];
    var stageVcount = {};
    var stage = [];
    $scope.stagearr = [];
    var serviceVcount = {};
    var services = [];
    var categoryVcount = {};
    var categories = [];
	var deals = [];
    var dealsCount = {};
    $scope.potetialhigh = 0;
    $scope.potetialmedium = 0;
    $scope.potetiallow = 0;
    $scope.potetialnr = 0;
    $scope.allcategories = {};
    $scope.allservices = {};
    $scope.eventChange = function (event) {
        angular.forEach($scope.events, function (e) {
            if (e.id === event.id) {
                $scope.eventSelected = e;

            }
        });
        //$scope.eventSelected = event;
        console.log($scope.eventSelected);
        //Getting Visitor Potential Count
        console.log($scope.eventSelected.id);
        if ($scope.eventSelected.id !== undefined) {
            var resprom5 = $http.get(Data.serviceBase() + '/dashboard/dashVisitorPotential', {params: {"eventId": $scope.eventSelected.id}});
            resprom5.then(function (response) {
                console.log(response.data);
                $scope.potetialhigh = 0;
                $scope.potetialmedium = 0;
                $scope.potetiallow = 0;
                $scope.potetialnr = 0;
                potentialVcount.length = 0;
                potential.length = 0;
                if (response.data.highlabel !== null && response.data.high !== "0") {
                    potential.push(response.data.highlabel);
                    potentialVcount[response.data.highlabel] = response.data.high;
                    $scope.potetialhigh = response.data.high;
                }
                if (response.data.mediumlabel !== null && response.data.medium !== "0") {
                    potential.push(response.data.mediumlabel);
                    potentialVcount[response.data.mediumlabel] = response.data.medium;
                    $scope.potetialmedium = response.data.medium;
                }
                if (response.data.lowlabel !== null && response.data.low !== "0") {
                    potential.push(response.data.lowlabel);
                    potentialVcount[response.data.lowlabel] = response.data.low;
                    console.log("Low Potential");
                    console.log(potentialVcount[response.data.lowlabel]);
                    $scope.potetiallow = response.data.low;
                }
                if (response.data.nrlabel !== null && response.data.nr !== "0") {
                    potential.push(response.data.nrlabel);
                    potentialVcount[response.data.nrlabel] = response.data.nr;
                    $scope.potetialnr = response.data.nr;
                }
                /* if(response.data.mediumlabel === null && response.data.highlabel === null && response.data.lowlabel === null && response.data.nrlabel === null){
                 potential.push("Nil");
                 potentialVcount["Nil"] = "0";
                 }*/

                $scope.showPotentialGraph();

            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });

            var resprom6 = $http.get(Data.serviceBase() + '/dashboard/dashVisitorStage', {params: {"eventId": $scope.eventSelected.id}});
            resprom6.then(function (response) {
                //console.log(response.data);
                $scope.openstage = 0;
                $scope.contacted = 0;
                $scope.qualified = 0;
                $scope.unqualified = 0;
                $scope.customer = 0;
                $scope.inactivecust = 0;
                $scope.stagearr = [];
                var myEl = angular.element(document.querySelector('#funnelContainer'));
                myEl.empty();
                stageVcount = {};
                $scope.stageDet = {};
                stage.length = 0;
                $scope.stageData = response.data.stageStats;
                $scope.stagesList = response.data.stages;
                var lstages = response.data.stages;
                for (var t = 0; t < lstages.length; t++) {
                    //if ($scope.stageData[lstages[t]+'label'] !== null && $scope.stageData[lstages[t]] !== "0") {
                    stage.push($scope.stageData[lstages[t].stage + 'label']);
                    $scope.stageDet[$scope.stageData[lstages[t].stage + 'label']] = $scope.stageData[lstages[t].stage];
                    if ($scope.stageData[lstages[t].stage + 'label'] !== null && $scope.stageData[lstages[t].stage] !== "0") {
                        var stagetemp = {};
                        stagetemp[$scope.stageData[lstages[t].stage + 'label']] = parseInt($scope.stageData[lstages[t].stage]);
                        $scope.stagearr.push(stagetemp);
                    }
                    // console.log("Data fecthing");
                    // console.log($scope.stagearr);

                    //$scope.openstage = $scope.stageData[lstages[t]];
                    //}
                }
                /*if (response.data.openlabel !== null && response.data.open !== "0") {
                    stage.push(response.data.openlabel);
                    stageVcount[response.data.openlabel] = response.data.open;
                    var stagetemp = {};
                    stagetemp[response.data.openlabel] = parseInt(response.data.open);
                    $scope.stagearr.push(stagetemp);
                    // console.log("Data fecthing");
                    // console.log($scope.stagearr);

                    $scope.openstage = response.data.open;
                }
                if (response.data.contactedlabel !== null && response.data.contacted !== "0") {
                    stage.push(response.data.contactedlabel);
                    stageVcount[response.data.contactedlabel] = response.data.contacted;
                    var stagetemp1 = {};
                    stagetemp1[response.data.contactedlabel] = parseInt(response.data.contacted);
                    $scope.stagearr.push(stagetemp1);
                    $scope.contacted = response.data.contacted;
                }
                if (response.data.qualifiedlabel !== null && response.data.qualified !== "0") {
                    stage.push(response.data.qualifiedlabel);
                    stageVcount[response.data.qualifiedlabel] = response.data.qualified;
                    var stagetemp2 = {};
                    stagetemp2[response.data.qualifiedlabel] = parseInt(response.data.qualified);
                    $scope.stagearr.push(stagetemp2);

                    $scope.qualified = response.data.qualified;
                }
                if (response.data.unqualifiedlabel !== null && response.data.unqualified !== "0") {
                    stage.push(response.data.unqualifiedlabel);
                    stageVcount[response.data.unqualifiedlabel] = response.data.unqualified;
                    // var stagetemp3 = {};
                    // stagetemp3[response.data.unqualifiedlabel] = parseInt(response.data.unqualified);
                    // $scope.stagearr.push(stagetemp3);
                    $scope.unqualified = response.data.unqualified;
                }
                if (response.data.customerlabel !== null && response.data.customer !== "0") {
                    stage.push(response.data.customerlabel);
                    stageVcount[response.data.customerlabel] = response.data.customer;
                    var stagetemp4 = {};
                    stagetemp4[response.data.customerlabel] = parseInt(response.data.customer);
                    $scope.stagearr.push(stagetemp4);
                    $scope.customer = response.data.customer;
                }
                if (response.data.inactivelabel !== null && response.data.inactive !== "0") {
                    stage.push(response.data.inactivelabel);
                    stageVcount[response.data.inactivelabel] = response.data.inactive;
                    // var stagetemp5 = {};
                    // stagetemp5[response.data.inactivelabel] = parseInt(response.data.inactive);
                    // $scope.stagearr.push(stagetemp5);
                    $scope.inactivecust = response.data.inactive;
                }*/
                //console.log("Data for Stage");
                //console.log($scope.stagearr);
                // $rootScope.$broadcast('funneldata', $scope.stagearr);
                //$scope.showStageGraph($scope.stagearr);
                var formattedData = [];
				$scope.showStageGraph($scope.stagearr);
                if ($scope.stagearr.length > 0) {

                    for (var i = 0; i < $scope.stagearr.length; i++) {
                        var unitData = [];
                        unitData.push(Object.keys($scope.stagearr[i])[0]);
                        unitData.push($scope.stagearr[i][Object.keys($scope.stagearr[i])[0]]);
                        formattedData.push(unitData);

                    }

                    //console.log("Funnel Data");
                    //console.log(formattedData);

                    new FunnelChart({
                        data: formattedData,
                        width: 325,
                        height: 275,
                        bottomPinch: 4,
                        bottomPct: 1 / 3
                    }).draw('#funnelContainer', 6);
                } else {
                    formattedData.length = 0;
                    $scope.stagearr.length = 0;

                }


            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });

            $scope.categoryA = 0;
            //Getting Service Visitor Count
            var resprom3 = $http.get(Data.serviceBase() + '/dashboard/dashEventservice', {params: {"eventId": $scope.eventSelected.id}});
            resprom3.then(function (response) {
                serviceVcount.length = 0;
                services.length = 0;
                $scope.allservices = {};
                $scope.allservices = response.data;
                angular.forEach(response.data, function (d) {
                    services.push(d.label);
                    serviceVcount[d.label] = d.data;
                });
                console.log(serviceVcount);
                console.log(services);
                $scope.showServicesGraph();
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
            //Getting Category Visitor Count

            var resprom4 = $http.get(Data.serviceBase() + '/dashboard/dashEventcategory', {params: {"eventId": $scope.eventSelected.id}});
            resprom4.then(function (response) {
                categoryVcount.length = 0;
                $scope.allcategories = {};
                $scope.allcategories = response.data;
                categories.length = 0;
                angular.forEach(response.data, function (d) {
                    categories.push(d.label);
                    categoryVcount[d.label] = d.data;
                    $scope.categoryA = $scope.categoryA + parseInt(d.data);
                });
                $scope.categoryA = parseInt($scope.eventSelected.Vcount) - $scope.categoryA;
                $scope.loader = false;
                console.log("Category");
                console.log(categoryVcount);
                console.log(categories);
                $scope.showCategoriesGraph();
            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
			 //Getting deal Visitor Count
           var resprom7 = $http.get(Data.serviceBase() + '/dashboard/dashVisitorStageDeals', {params: {"eventId": $scope.eventSelected.id}});
            resprom7.then(function (response) {
                console.log(response.data);
                $scope.dopenstage = 0;
                $scope.dcontacted = 0;
                $scope.dqualified = 0;
                $scope.dunqualified = 0;
                $scope.dcustomer = 0;
                $scope.dinactive = 0;
                $scope.vcopenstage = 0;
                $scope.vccontacted = 0;
                $scope.vcqualified = 0;
                $scope.vcunqualified = 0;
                $scope.vccustomer = 0;
                $scope.vcinactive = 0;
                dealsCount.length = 0;
                deals.length = 0;
				$scope.dstageDet = [];
                $scope.dstageData = response.data.stageStats;
                $scope.dstagesList = response.data.stages;
                var dstages = response.data.stages;
                for (var t = 0; t < dstages.length; t++) {
                    //

                    var temparray = {};
                    temparray['label'] = $scope.dstageData[dstages[t].stage + 'label'];
                    temparray['vcount'] = $scope.dstageData[dstages[t].stage];
                    temparray['dcount'] = $scope.dstageData[dstages[t].stage + 'DealSum'];
                    temparray['stageDet'] = dstages[t];
                    $scope.dstageDet.push(temparray);
                    if ($scope.dstageData[dstages[t].stage + 'label'] !== null && $scope.dstageData[dstages[t].stage] !== "0") {
                        deals.push($scope.dstageData[dstages[t].stage + 'label']);
                        dealsCount[$scope.dstageData[dstages[t].stage + 'label']] = $scope.dstageData[dstages[t].stage + 'DealSum'];
                    }

                    // console.log("Data fecthing");
                    // console.log($scope.stagearr);

                    //$scope.openstage = $scope.stageData[lstages[t]];
                    //}
                }
                /*if (response.data.openlabel !== null && response.data.open !== "0") {
                    deals.push(response.data.openlabel);
                    dealsCount[response.data.openlabel] = response.data.openDealSum;
                    $scope.vcopenstage = response.data.open;
                    $scope.dopenstage = response.data.openDealSum;
                }
                if (response.data.contactedlabel !== null && response.data.contacted !== "0") {
                    deals.push(response.data.contactedlabel);
                    dealsCount[response.data.contactedlabel] = response.data.contactedDealSum;
                    $scope.vccontacted = response.data.contacted;
                    $scope.dcontacted = response.data.contactedDealSum;
                }
                if (response.data.qualifiedlabel !== null && response.data.qualified !== "0") {
                    deals.push(response.data.qualifiedlabel);
                    dealsCount[response.data.qualifiedlabel] = response.data.qualifiedDealSum;
                    $scope.vcqualified = response.data.qualified;
                    $scope.dqualified = response.data.qualifiedDealSum;
                }
                if (response.data.unqualifiedlabel !== null && response.data.unqualified !== "0") {
                    deals.push(response.data.unqualifiedlabel);
                    dealsCount[response.data.unqualifiedlabel] = response.data.unqualifiedDealSum;
                    $scope.vcunqualified = response.data.unqualified;
                    $scope.dunqualified = response.data.unqualifiedDealSum;
                }
                if (response.data.customerlabel !== null && response.data.customer !== "0") {
                    deals.push(response.data.customerlabel);
                    dealsCount[response.data.customerlabel] = response.data.customerDealSum;
                    $scope.vccustomer = response.data.customer;
                    $scope.dcustomer = response.data.customerDealSum;
                }
                if (response.data.inactivelabel !== null && response.data.inactive !== "0") {
                    deals.push(response.data.inactivelabel);
                    dealsCount[response.data.inactivelabel] = response.data.inactiveDealSum;
                    $scope.vcinactive = response.data.inactive;
                    $scope.dinactive = response.data.inactiveDealSum;
                }*/
                
                /* if(response.data.mediumlabel === null && response.data.highlabel === null && response.data.lowlabel === null && response.data.nrlabel === null){
                 potential.push("Nil");
                 potentialVcount["Nil"] = "0";
                 }*/

                $scope.showDelasGraph();

            }, function (response) {
                console.log('Error happened -- ');
                console.log(response);
            });
        } else {
            $scope.loader = false;
        }
		
		// $scope.initMap();
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        $scope.eventVisitor = {};
        $scope.getEventVisitor();
        markers.length = 0;

    };
    $scope.getData = function (val) {
        console.log("DAta content");
        console.log(val);
    };
    $scope.serviceChart = null;
    $scope.showServicesGraph = function () {

        $scope.serviceChart = c3.generate({
            bindto: '#serviceChart',
            data: {
                json: [serviceVcount],
                keys: {
                    value: services,
                },
                type: 'donut',
                onclick: function (label) {
                    localStorage.setItem('curEventId', $scope.eventSelected.id);
                    localStorage.setItem('curEventName', $scope.eventSelected.name);
                    sessionStorage.setItem('menuact', '2');
                    sessionStorage.setItem('filterindex', "interest");
                    sessionStorage.setItem('filterlabel', label.id);
                    sessionStorage.setItem('selpage', page);
                    window.location.href = Data.webAppBase() + 'account/manageVisitor';
                },
                empty: {
                    label: {
                        text: "No Data Available"
                    }
                }

            },
            tooltip: {
                show: false
            },
            color: {
                pattern: ['#D95E40', '#F2992E', '#56BC8A', '#529ECC', '#A77DC2', '#B2912F', '#B276B2', '#DECF3F', '#5DA5DA', '#FAA43A', '#60BD68', '#fcd828', '#80c99b', '#ea2127', '#F17CB0', '#F15854']
            },
            size: {
                width: 600
                        //height: 300
            },
            padding: {
                top: 10,
                left: 40
            },
            legend: {
                position: 'bottom'
            }
        });
    };
    $scope.categoryChart = null;
    $scope.showCategoriesGraph = function () {
        $scope.categoryChart = c3.generate({
            bindto: '#categoryChart',
            data: {
                json: [categoryVcount],
                keys: {
                    value: categories,
                },
                type: 'pie',
                onclick: function (label) {
                    localStorage.setItem('curEventId', $scope.eventSelected.id);
                    localStorage.setItem('curEventName', $scope.eventSelected.name);
                    sessionStorage.setItem('filterindex', "category");
                    sessionStorage.setItem('filterlabel', label.id);
                    sessionStorage.setItem('menuact', '2');
                    sessionStorage.setItem('selpage', page);
                    window.location.href = Data.webAppBase() + 'account/manageVisitor';
                },
                empty: {
                    label: {
                        text: "No Data Available",
                    }
                }
            },
            tooltip: {
                show: false
            },
            color: {
                pattern: ['#D95E40', '#F2992E', '#56BC8A', '#529ECC', '#A77DC2', '#B2912F', '#B276B2', '#DECF3F', '#F17CB0', '#F15854', '#5DA5DA', '#FAA43A', '#60BD68', '#fcd828', '#80c99b', '#ea2127']
            },
            size: {
                width: 500
                        //height: 300
            },
            padding: {
                top: 10,
                left: 300
            },
            legend: {
                position: 'bottom'
            }
        });
    };
    $scope.potentialChart = null;
    $scope.showPotentialGraph = function () {
        $scope.potentialChart = null;
        $scope.potentialChart = c3.generate({
            bindto: '#potentialChart',
            data: {
                json: [potentialVcount],
                keys: {
                    value: potential,
                },
                type: 'donut',
                onclick: function (label) {
                    localStorage.setItem('curEventId', $scope.eventSelected.id);
                    localStorage.setItem('curEventName', $scope.eventSelected.name);
                    sessionStorage.setItem('filterindex', "potential");
                    sessionStorage.setItem('filterlabel', label.id);
                    sessionStorage.setItem('menuact', '2');
                    sessionStorage.setItem('selpage', page);
                    window.location.href = Data.webAppBase() + 'account/manageVisitor';
                },
                empty: {
                    label: {
                        text: "No Data Available"
                    }
                }


            },
            color: {
                pattern: ['#56BC8A', '#529ECC', '#F2992E', '#e8e8e8', '#A77DC2']
            },
            size: {
                width: 500
                        //height: 300
            },
            padding: {
                top: 10,
                left: 300
            },
            legend: {
                position: 'bottom'
            },
            tooltip: {
                format: {
                    value: function (value, ratio, id, index) {
                        return "";
                    }
                },
                show: false
            },
        });

    };
    $scope.stageChart = null;
    $scope.showStageGraph = function (dat) {
        $scope.stageChart = null;
        $scope.stageChart = c3.generate({
            bindto: '#stageChart',
            data: {
                json: [stageVcount],
                keys: {
                    value: stage,
                },
                type: 'gauge',
                onclick: function (label) {
                    localStorage.setItem('curEventId', $scope.eventSelected.id);
                    localStorage.setItem('curEventName', $scope.eventSelected.name);
                    sessionStorage.setItem('filterindex', "type");
                    sessionStorage.setItem('filterlabel', JSON.stringify(label));
                    sessionStorage.setItem('menuact', '2');
                    sessionStorage.setItem('selpage', page);
                    window.location.href = Data.webAppBase() + 'account/manageVisitor';
                },
                empty: {
                    label: {
                        text: "No Data Available"
                    }
                }


            },
            color: {
                pattern: ['#56BC8A', '#529ECC', '#F2992E', '#e8e8e8', '#A77DC2', '#B276B2', '#DECF3F', '#F17CB0', '#F15854']
            },
            size: {
                width: 500
                        //height: 300
            },
            padding: {
                top: 10,
                left: 300
            },
            legend: {
                position: 'bottom'
            },
            tooltip: {
                format: {
                    value: function (value, ratio, id, index) {
                        return "";
                    }
                },
                show: false
            },
        });
		

    };
	$scope.dealsChart = null;
    $scope.showDelasGraph = function () {

        $scope.dealsChart = c3.generate({
            bindto: '#dealsChart',
            data: {
                json: [dealsCount],
                keys: {
                    value: deals,
                },
                type: 'donut',
                onclick: function (label) {
					var stDet = {};
                    console.log(label);
                    angular.forEach($scope.stagesList, function (d) {
                        if (d.stage === label.id) {
                            console.log(d);
                            stDet = d;
                        }
                    });
                    localStorage.setItem('curEventId', $scope.eventSelected.id);
                    localStorage.setItem('curEventName', $scope.eventSelected.name);
                    sessionStorage.setItem('menuact', '2');
                    sessionStorage.setItem('filterindex', "dtype");
                    sessionStorage.setItem('filterlabel', JSON.stringify(stDet));
                    sessionStorage.setItem('selpage', page);
                    window.location.href = Data.webAppBase() + 'account/manageVisitor';
                },
                empty: {
                    label: {
                        text: "No Data Available"
                    }
                }

            },
            tooltip: {
                show: false
            },
            color: {
                pattern: ['#D95E40', '#F2992E', '#56BC8A', '#529ECC', '#A77DC2', '#B2912F', '#B276B2', '#DECF3F', '#5DA5DA', '#FAA43A', '#60BD68', '#fcd828', '#80c99b', '#ea2127', '#F17CB0', '#F15854']
            },
            size: {
                width: 600
                        //height: 300
            },
            padding: {
                top: 10,
                left: 40
            },
            legend: {
                position: 'bottom'
            }
        });
    };

	
    $scope.getEventVisitor = function () {
        var resprom = $http.get(Data.serviceBase() + '/visitor/mapvisitor', {params: {"key": "eventId", "value": $scope.eventSelected.id}});
        resprom.then(function (response) {

            $scope.eventVisitor = response.data;
            var tempVisitor = {};
            console.log($scope.eventVisitor.length);
			$scope.initMap();
            for (var i = 0; i < $scope.eventVisitor.length; i++) {
                tempVisitor = $scope.eventVisitor[i];

                var contentHtml = '';
                contentHtml = contentHtml + '<a href="https://beta.helloleads.io/index.php/app/account/emailverify?nextURL?=/viewLead?visitorId=' + tempVisitor.id + '" target="_blank">' + tempVisitor.firstName;
                if (tempVisitor.lastName !== null && tempVisitor.lastName) {
                    contentHtml = contentHtml +" "+tempVisitor.lastName + '</a><br>';
                } else {
                    contentHtml = contentHtml + '</a><br>';
                }
                if (tempVisitor.designation && tempVisitor.visitorOrganizationName) {
                    contentHtml = contentHtml + tempVisitor.designation + '@' + tempVisitor.visitorOrganizationName;
                } else if (tempVisitor.designation && tempVisitor.designation !== null) {
                    contentHtml = contentHtml + tempVisitor.designation;
                } else if (tempVisitor.visitorOrganizationName !== null) {
                    contentHtml = contentHtml + tempVisitor.visitorOrganizationName;
                }
                contentHtml = contentHtml + '<br> Potential : ' + tempVisitor.potential;
                contentHtml = contentHtml + '<br> Stage : ' + tempVisitor.type;
                /*if ($scope.eventVisitor[i].leadAddr_lat === '' && $scope.eventVisitor[i].leadAddr_long === '') {
                    $scope.getLatitudeLongitude($scope.setLatlang, i, $scope.eventVisitor[i].visitorOrganizationName+","+$scope.eventVisitor[i].address1+","+$scope.eventVisitor[i].address2+"," +$scope.eventVisitor[i].city + "," + $scope.eventVisitor[i].state + "," + $scope.eventVisitor[i].country, tempVisitor.firstName, contentHtml);
                } else {
                    $scope.setMarker(map,new google.maps.LatLng($scope.eventVisitor[i].leadAddr_lat, $scope.eventVisitor[i].leadAddr_long),tempVisitor.firstName, contentHtml);
                }*/
                if ( ($scope.eventVisitor[i].leadAddr_lat === '' && $scope.eventVisitor[i].leadAddr_long === '') || ($scope.eventVisitor[i].leadAddr_lat === null && $scope.eventVisitor[i].leadAddr_long === null))  {
                    $scope.getLatitudeLongitude($scope.setLatlang, i, $scope.eventVisitor[i].visitorOrganizationName+","+$scope.eventVisitor[i].address1+","+$scope.eventVisitor[i].address2+"," +$scope.eventVisitor[i].city + "," + $scope.eventVisitor[i].state + "," + $scope.eventVisitor[i].country, tempVisitor.firstName, contentHtml);
                } else {
                    $scope.setMarker(map,new google.maps.LatLng($scope.eventVisitor[i].leadAddr_lat, $scope.eventVisitor[i].leadAddr_long),tempVisitor.firstName, contentHtml);
                }
            }

            
            /*for (var gi = 0; gi < $scope.eventVisitor.length; gi++) {
             tempVisitor = $scope.eventVisitor[gi];
             
             var contentHtml = '';
             contentHtml = contentHtml + '<a href="http://localhost/eventHello/index.php/app/account/viewLead?visitorId=' + tempVisitor.id + '" target="_blank">' + tempVisitor.firstName;
             if (tempVisitor.lastName !== null && tempVisitor.lastName) {
             contentHtml = contentHtml + tempVisitor.lastName + '</a><br>';
             } else {
             contentHtml = contentHtml + '</a><br>';
             }
             if (tempVisitor.designation && tempVisitor.visitorOrganizationName) {
             contentHtml = contentHtml + tempVisitor.designation + '@' + tempVisitor.visitorOrganizationName;
             } else if (tempVisitor.designation && tempVisitor.designation !== null) {
             contentHtml = contentHtml + tempVisitor.designation;
             } else if (tempVisitor.visitorOrganizationName !== null) {
             contentHtml = contentHtml + tempVisitor.visitorOrganizationName;
             }
             contentHtml = contentHtml + '<br> Potential : ' + tempVisitor.potential;
             contentHtml = contentHtml + '<br> Stage : ' + tempVisitor.type;
             console.log(tempVisitor)   
             
             $scope.setMarker(map,$scope.eventVisitor[gi].llresult , tempVisitor.firstName, contentHtml);
             
             }*/
            //$scope.initialise();
        }, function (response) {
            console.log('Error happened -- ');
            console.log(response);
        });

    };

	$scope.getLatitudeLongitude = function (callback, index, address, title, contentHtml) {
        // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
        //address = address || 'Ferrol, Galicia, Spain';
        var geocoder = new google.maps.Geocoder();
        // Initialize the Geocoder
        geocoder = new google.maps.Geocoder();
        if (geocoder) {
            geocoder.geocode({
                'address': address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    callback(index, results[0], title, contentHtml);
                }
            });
        }
    };
    $scope.setLatlang = function (index, result, title, contentHtml) {
        console.log(result.geometry.location);
		if($scope.eventVisitor[index] != undefined && result.geometry.location != undefined){ 
        $scope.eventVisitor[index].lat = result.geometry.location.lat().toString();
        $scope.eventVisitor[index].long = result.geometry.location.lng().toString();
        $scope.eventVisitor[index].llresult = result.geometry.location;
        $scope.setMarker(map, $scope.eventVisitor[index].llresult, title, contentHtml);
		}
    };

    // place a marker
    $scope.setMarker = function (map, position, title, content) {
        var marker;

        //console.log(map);
       // console.log(position);
        //console.log(content);
        var markerOptions = {
            position: position,
            map: map,
            title: title
        };


        marker = new google.maps.Marker(markerOptions);
        markers.push(marker); // add marker to array
        console.log(marker);
        google.maps.event.addListener(marker, 'click', function () {
            // close window if not undefined
            if (infoWindow !== void 0) {
                infoWindow.close();
            }
            // create new window
            var infoWindowOptions = {
                content: content
            };
            infoWindow = new google.maps.InfoWindow(infoWindowOptions);
            infoWindow.open(map, marker);
        });

    };


});

evtApp.controller('firstTimeBoardingCtrl', function ($scope, Data, $uibModalInstance, toaster) {

    $scope.hifirstName = sessionStorage.getItem('firstName');
    $scope.title = "Welcome";
    console.log($scope.hifirstName);
    $scope.cancel = function () {
        sessionStorage.getItem('wcount', '1');
        $uibModalInstance.close(1);
    };
});
// Walkthrough content
evtApp.controller('walkThroughPopupCtrl', function ($scope, Data,popupContent, $uibModalInstance, toaster) {

    $scope.wcontent = popupContent.data;
    console.log($scope.wcontent);
    $scope.cancel = function () {
        sessionStorage.getItem('chkcount', '1');
        $uibModalInstance.close(1);
    };
    $scope.learnContent = function(link){
		//$uibModalInstance.close(1);
        window.open(link, "_blank");
    };
    $scope.tryItlink = function(link){
		$uibModalInstance.close(1);
        window.open(Data.webAppBase()+link,"_self");
    };
});
// Premium Popup content
evtApp.controller('PaymentPopupCtrl', function ($scope, Data, popupContent, $uibModalInstance, toaster) {
	$scope.wcontent = popupContent;
    console.log($scope.wcontent);
    $scope.cancel = function () {
        sessionStorage.setItem('pchkcount', '1');
        $uibModalInstance.close(1);
    };
    $scope.learnContent = function (link) {
        $uibModalInstance.close(1);
        window.open(link, "_blank");
    };
    $scope.tryItlink = function (link) {
        $uibModalInstance.close(1);
        window.open(Data.webAppBase() + link, "_self");
    };
});

evtApp.controller('premiumEngPopUpCtrl', function ($scope, Data, $uibModalInstance, msg, toaster) {
    $scope.message = msg;

    $scope.redirectTo = function (link) {
        sessionStorage.setItem('pchkcount', '1');
        $uibModalInstance.close(1);
        //window.location.href = link
        window.open(link, '_blank');
    };
    $scope.cancel = function () {
        sessionStorage.setItem('pchkcount', '1');
        $uibModalInstance.dismiss('Close');
    };

});

evtApp.controller('stageCustomCtrl', function ($scope, Data, $uibModalInstance, $http, toaster) {
    $scope.organizationId = sessionStorage.getItem('orgId');

    $scope.loader = true;
    var customizeStageresp = $http.get(Data.serviceBase() + '/organization/customizeStage', {params: {"orgId": $scope.organizationId}});
    customizeStageresp.then(function (response) {
        console.log(response);
        if (response.data.status === 'success') {
            $scope.loader = false;
            $uibModalInstance.dismiss('Close');
        }

    }, function (response) {
        console.log('Error happened -- ');
        console.log(response);
    });
});
