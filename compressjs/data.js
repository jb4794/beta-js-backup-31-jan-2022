evtApp.factory("Data", ['$http', 'toaster',
    function ($http, toaster) {

        //var serviceBase = 'http://localhost/eventHello/index.php/api/';
        //var webappBase = 'http://localhost/eventHello/index.php/app/';
        var serviceBase = 'https://beta.helloleads.io/index.php/api/';
        var webappBase = 'https://beta.helloleads.io/index.php/app/';

        var obj = {};
        obj.toast = function (data) {
            toaster.pop(data.status, "", data.message, 10000, 'trustedHtml');
        }
        obj.get = function (q) {
            return $http.get(serviceBase + q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(serviceBase + q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(serviceBase + q).then(function (results) {
                return results.data;
            });
        };

        obj.serviceBase = function() {
            return serviceBase;
        };

        obj.webAppBase = function() {
            return webappBase;
        };


        return obj;
}]);
