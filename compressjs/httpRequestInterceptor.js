evtApp.factory('httpRequestInterceptor', ['$q', '$location',
    function ($q, $location) {
        //var serviceBase = 'http://localhost/eventHello/index.php/api/';
        //var webappBase = 'http://localhost/eventHello/index.php/app/';
		var serviceBase = 'http://beta.helloleads.io/index.php/api/';
		var webappBase = 'http://beta.helloleads.io/index.php/app/';
        return {
            'request': function (config) {
                if (sessionStorage.getItem('token')) {

                    //console.log("token[" + sessionStorage.getItem('token') + "], config.headers: ", config.headers);
                    config.headers.Auth = sessionStorage.getItem('token');
                    config.headers.Xemail = sessionStorage.getItem('userId');
                    config.headers.ApiClient = "Web";
                } else {
                	config.headers.ApiClient = "Web";
                }
                return config || $q.when(config);
            },
            responseError: function (rejection) {
                console.log("Found responseError: ", rejection);
                console.log("Access denied (error 401), please login again");
                //window.location.href = webappBase + 'account/login'
                //$location.path('/login');
                return $q.reject(rejection);
            }
        };
    }]);

evtApp.config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpRequestInterceptor');
});
