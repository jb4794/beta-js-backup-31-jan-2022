<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./application/libraries/Dex_Rest_Controller.php');
require (APPPATH . 'controllers/PassHash.php');
require (APPPATH . 'controllers/AuthManager.php');
require (APPPATH . 'controllers/CommonMethods.php');

class Organization extends Dex_Rest_Controller {

    private $commonMethods = null;

    public function __construct() {

        parent::__construct();
        $this->load->model('organizationmodel');
        $this->load->model('emailqueuemodel');
        $this->load->model('usermodel');
        $this->load->model('apimodel');
        $this->load->model('smsqueuemodel');
        $this->load->model('eventmodel');
        $this->load->model('visitormodel');
        $this->load->model('emailqueuemodel');
        $this->load->model('fieldmodel');
        $this->load->database();
        $this->load->library('zip');
        $this->lang->load('messages', 'english');
        $this->load->library('session');
        $this->commonMethods = new CommonMethods();
    }

    public function organization_get() {

        AuthManager::authenticate();

        $orgs = array();
        if ($this->get('key') && $this->get('value')) {
            $orgs = $this->organizationmodel->getBy($this->get('key'), $this->get('value'));
        }

        $this->response($orgs, 200);
    }

    // Sending premium expiry and usage alert emails from mobile 
    public function sendEmailMob_post() {
        AuthManager::authenticate();
        if (!$this->post('subject')) {
            $this->response(array('status' => 'error', 'message' => 'subject is required for sending message'), 200);
        }
        $to = 'sales@helloleads.io';
        $from = 'alert@helloleads.io';
        $subject = $this->post('subject');
        $message = $this->post('message');

        $headers = apache_request_headers();
        $users = $this->usermodel->getBy("id", $headers['Xemail']);

        //$username = $this->post('userName');
        //$orgname = $this->post('orgName');
        //$orgdate = $this->post('orgDate');
        //$userEmail = $this->post('userEmail');
        $maildata = array(
            "fromid" => $from,
            "toId" => $to,
            "subject" => $subject,
            "message" => $message,
            "status" => 'pending',
            "date" => gmdate('Y-m-d H:i:s')
        );
        $mailqueueId = $this->emailqueuemodel->LWcreate($maildata);
        if (!empty($users)) {

            $orgDet = $this->organizationmodel->getBy("id", $users[0]->organizationId);
            // Sending mail to customer for post trial usage
            //$cussubject = $orgDet[0]->displayName.' - Request for another 14 days trial extensions';
            $message = '';
            $subject = '';
            $tempDet = $this->emailtemplatesmodel->getByName('Request for extension');
            if (!empty($tempDet)) {
                if (!empty($tempDet[0]->body) && !empty($tempDet[0]->subject)) {
                    $message = $tempDet[0]->body;
                    $message = str_replace("#VisitorName#", !empty($orgDet[0]->contactFirstName) ? $orgDet[0]->contactFirstName : '', $message);
                    $message = str_replace("#VisitorCompanyName#", $orgDet[0]->displayName, $message);
                    $message = str_replace("#Email#", $orgDet[0]->contactEmail, $message);
                    $message = str_replace("#pdate#", date("d-M-Y", strtotime($orgDet[0]->createdAt)), $message);

                    $subject = $tempDet[0]->subject;
                }
            }

            /* $cmessage = 'Dear '.$users[0]->firstName.',<br/><br/>

              Thanks for registering the trial version on '.date("d-M-Y", strtotime($orgDet[0]->createdAt)).'.<br/><br/>

              While it was valid for 14 days, I understand  that you may be interested in using the application for a further period of 14 days to check out the great new features that we have introduced recently like FaceBook integration, Visiting card scanner, WhatsApp connectivity, invoice generation and so on.   <br/><br/>

              Please click on <a href="mailto:support@helloleads.io?subject= '.$cussubject.'">TRIAL</a> to help us renew your usage by another 14 days. Thanks.<br/><br/>

              We look forward to hearing from you.<br/><br/>

              Regards,<br/>
              Linda
              '; */
            $cmessage = $message;

            $csubject = $subject;
            $cnotifyInternal = array(
                "fromId" => "alert@helloleads.io",
                "fromName" => "HelloLeads",
                "toId" => $users[0]->email,
                "cc" => 'sales@helloleads.io',
                "subject" => $csubject,
                "message" => $cmessage,
                "date" => gmdate('Y-m-d H:i:s')
            );

            $cnotifyInternalqueueId = $this->emailqueuemodel->create($cnotifyInternal);
        }


        $this->response(array("status" => "success", "message" => "Email added to queue"), 200);
    }
    
    public function sendCustomEmailMob_post() {
        AuthManager::authenticate();
        if (!$this->post('subject') || !$this->post('to')) {
            $this->response(array('status' => 'error', 'message' => 'subject is required for sending message'), 200);
        }
        $to = $this->post('to');
        $username = $this->post('username');
        $from = 'linda@helloleads.io';
       
        //$username = $this->post('userName');
        //$orgname = $this->post('orgName');
        //$orgdate = $this->post('orgDate');
        //$userEmail = $this->post('userEmail');
            $message = '';
            $subject = '';
            $tempDet = $this->emailtemplatesmodel->getByName('Unsupported QR code');
            if (!empty($tempDet)) {
                if (!empty($tempDet[0]->body) && !empty($tempDet[0]->subject)) {
                    $message = $tempDet[0]->body;
                    $message = str_replace("#VisitorName#", $username, $message);
                    

                    $subject = $tempDet[0]->subject;
                }
            }
        $maildata = array(
            "fromName" => "Linda - HelloLeads",
            "fromId" => $from,
            "toId" => $to,
            "cc" => "support@helloleads.io",
            "subject" => $subject,
            "message" => $message,
            "status" => 'pending',
            "date" => gmdate('Y-m-d H:i:s')
        );
        $mailqueueId = $this->emailqueuemodel->LWcreate($maildata);
    
        if($mailqueueId){
            $this->response(array("status" => "success", "message" => "Email added to queue"), 200);
        }
    }
    public function emailMetrics_get() {
        AuthManager::authenticate();
        $orgId = $this->get('organizationId');
        $metrics = $this->organizationmodel->emailMetrics($orgId);
        $this->response($metrics, 200);
    }

    public function getEmailContent_get() {
        AuthManager::authenticate();
        $mailId = $this->get('emailId');
        if ($mailId) {
            $this->db->where('id', $mailId);
            $mail = $this->db->get('emailCampQueue')->result();
            if ($mail) {

                $mail[0]->message = html_entity_decode($mail[0]->message);
                $mail[0]->message = preg_replace("#<a id=\'usublink\\' href=(.*?)>(.*?)</a>#", ' ', $mail[0]->message);
                $mail[0]->message = preg_replace("#<span id=\'hlpro\\'>(.*?)</span>#", ' ', $mail[0]->message);
                $mail[0]->message = preg_replace("#&lt;(.*?)&gt;#", ' ', $mail[0]->message);
                $mail[0]->message = preg_replace("/<img id=\'trackDet\\' [^>]+\>/i", "", $mail[0]->message);
                // $mail[0]->message = $this->strip_word_html($mail[0]->message);
                $this->response($mail[0], 200);
            } else {
                $this->response(array(), 200);
            }
        }
    }
    
    public function getSMSContent_get() {
        AuthManager::authenticate();
        $mailId = $this->get('smsId');
        if ($mailId) {
            $this->db->where('id', $mailId);
            $mail = $this->db->get('smsCampQueue')->result();
            if ($mail) {

                // $mail[0]->message = $this->strip_word_html($mail[0]->message);
                $this->response($mail[0], 200);
            } else {
                $this->response(array(), 200);
            }
        }
    }

    public function getOrgEmailRevContent_get() {
        AuthManager::authenticate();
        $mailId = $this->get('emailId');
        if ($mailId) {
            $this->db->where('id', $mailId);
            $mail = $this->db->get('orgemailparse')->result();
            if ($mail) {

                $mail[0]->body = $mail[0]->body;
                $mail[0]->body = preg_replace("#<span id=\'hlpro\\'>(.*?)</span>#", ' ', $mail[0]->body);
                $mail[0]->body = preg_replace("#<a id=\'usublink\\' href=(.*?)>(.*?)</a>#", ' ', $mail[0]->body);
                $mail[0]->body = preg_replace("#&lt;(.*?)&gt;#", ' ', $mail[0]->body);
                // log_message("error", $mail[0]->body);
                //$mail[0]->body = preg_replace("#<a id=\'usublink\\' href=(.*?)>(.*?)</a>#", ' ', $mail[0]->body);
                // $mail[0]->body = preg_replace("#<span id=\'hlpro\\'>(.*?)</span>#", ' ', $mail[0]->body);
                //$mail[0]->body = preg_replace("/<img[^>]+\>/i", "", $mail[0]->body);
                //log_message("error", $mail[0]->body);
                //$mail[0]->body = $this->strip_word_html($mail[0]->body);
                $this->response($mail[0], 200);
            } else {
                $this->response(array(), 200);
            }
        }
    }

    public function getEmailRevContent_get() {
        AuthManager::authenticate();
        $mailId = $this->get('emailId');
        if ($mailId) {
            $this->db->where('id', $mailId);
            $mail = $this->db->get('orgemailparse')->result();
            if ($mail) {

                $mail[0]->body = html_entity_decode($mail[0]->body);
                // log_message("error", $mail[0]->body);
                $mail[0]->body = preg_replace("#<a id=\'usublink\\' href=(.*?)>(.*?)</a>#", ' ', $mail[0]->body);
                $mail[0]->body = preg_replace("#<span id=\'hlpro\\'>(.*?)</span>#", ' ', $mail[0]->body);
                $mail[0]->body = preg_replace("/<img[^>]+\>/i", "", $mail[0]->body);

                //log_message("error", $mail[0]->body);
                //$mail[0]->body = $this->strip_word_html($mail[0]->body);
                $this->response($mail[0], 200);
            } else {
                $this->response(array(), 200);
            }
        }
    }

    public function getlParseCount_get() {
        AuthManager::authenticate();
        $orgId = $this->get('organizationId');
        $userId = $this->get('userId');
        if ($userId && $orgId) {
            $this->db->where('userId', $userId);
            $this->db->where('organizationId', $orgId);
            $this->db->where('openFlag', '0');
            $mailCount = $this->db->get('orgemailparse')->num_rows();
            $this->response(array("status" => "success", "mailCount" => $mailCount), 200);
        } else {
            $this->response(array("status" => "success", "mailCount" => '0'), 200);
        }
    }

    public function mparseList_get() {
        $orgId = $this->get('organizationId');
        $userId = $this->get('userId');
        $mailDets = array();
        if ($userId && $orgId) {
            $mailDets = $this->db->query('select orep.*,concat(v.firstName," ",ifnull(v.lastName,"")) as name from visitor v left outer join orgemailparse orep on orep.visitorId = v.id where orep.userId = ? and v.firstName is not null  and v.email is not null and orep.organizationId = ?', array($userId, $orgId))->result();
            $this->db->where('userId', $userId);
            $this->db->where('organizationId', $orgId);
            $this->db->where('openFlag', '0');
            $this->db->update('orgemailparse', array('openFlag' => '1', 'modifiedAt' => gmdate("Y-m-d H:i:s")));
            $this->response($mailDets, 200);
        } else {
            $this->response($mailDets, 200);
        }
    }

    public function orgFirstTimeCount_get() {

        AuthManager::authenticate();
        $eventCount = 0;
        $visitorCount = 0;
        $userCount = 0;
        $followupCount = 0;
        if (!empty($this->get('organizationId'))) {
            $eventCount = $this->organizationmodel->getEventCount($this->get('organizationId'));
            $visitorCount = $this->organizationmodel->getVisitorCount($this->get('organizationId'));
            $userCount = $this->organizationmodel->getUsersCount($this->get('organizationId'));
            $followCount = $this->organizationmodel->getFollowupCount($this->get('organizationId'));
        }

        $this->response(array('eventCount' => $eventCount, 'visitorCount' => $visitorCount, 'userCount' => $userCount, 'followCount' => $followCount), 200);
    }

    // SMTP Configuration -----------------------------------------------------------------

    public function updateSMTPConfig_post() {

        AuthManager::authenticate();

        if (!$this->post('organizationId')) {
            $this->response(array('status' => 'error', 'message' => 'organizationId is required for update'), 200);
        }
        $orgId = $this->post('organizationId');
        $userId = $this->post('userId');
        $sHost = $this->post('mailHost');
        $s2Host = $this->post('mailHost2');
        $sPort = $this->post('mailPort');
        $s2Port = $this->post('mailPort2');
        $sUser = $this->post('mailUser');
        $sPass = $this->post('mailPassword');
        $sSSL = $this->post('mailSSL');
        $sHId = $this->nullIfEmpty($this->post('id'));
        $smtpResult = $this->organizationmodel->updateSmtp($orgId, $userId, $sHId, $sHost, $s2Host, $sPort, $s2Port, $sUser, $sPass, $sSSL);
        if ($smtpResult) {
            $this->response(array('status' => 'success', 'message' => 'SMTP configuration updated successfully'), 200);
        }
    }

    public function orgSMTP_get() {
        AuthManager::authenticate();

        if (!$this->get('organizationId')) {
            $this->response(array('status' => 'error', 'message' => 'organizationId is required '), 200);
        }
        $orgId = $this->get('organizationId');
        $userId = $this->get('userId');
        $smtpResult = $this->organizationmodel->getSmtp($orgId, $userId);

        $this->response($smtpResult, 200);
    }

    // Lead stage customization -------------------------------------------------------------------------------------------
    public function leadStages_get() {
        AuthManager::authenticate();

        if (!$this->get('organizationId')) {
            $this->response(array('status' => 'error', 'message' => 'organizationId is required '), 200);
        }
        $orgId = $this->get('organizationId');
        $stageResult = $this->organizationmodel->getLeadStages($orgId);

        $this->response($stageResult, 200);
    }

    public function leadStages_post() {
        AuthManager::authenticate();
        if (!$this->post('organizationId') || !$this->post('delStage') || !$this->post('stages') || !$this->post('default') || !$this->post('won') || !$this->post('lost')) {
            $this->response(array('status' => 'error', 'message' => 'organizationId ,Stages , final stages nad remaining details are required '), 200);
        }
        $orgId = $this->post('organizationId');
        $stage = $this->post('stages');
        $default = $this->post('default');
        $won = $this->post('won');
        $lost = $this->post('lost');
        $delstage = $this->post('delStage');

        if (!empty($stage)) {
            $stageData = array(
                "stages" => $stage,
                "default" => $default,
                "won" => $won,
                "lost" => $lost,
                "finalStage" => $delstage,
            );
            if ($this->organizationmodel->updateLeadStage($orgId, $stageData)) {
                $this->response(array('status' => 'success', 'message' => 'Lead stage configuration updated successfully'), 200);
            }
        }
    }

    // Email Templates  ------------------------------------------------------------------------------

    public function emailTemplate_get() {
        AuthManager::authenticate();
        if (!$this->get('organizationId')) {
            $this->response(array('status' => 'error', 'message' => 'organizationId are required '), 200);
        }
        $orgId = $this->get('organizationId');
        $emailTemplates = $this->organizationmodel->getEmailTemlates($orgId);
        $this->response($emailTemplates, 200);
    }

    public function emailTemplate_post() {
        AuthManager::authenticate();
        if (!$this->post('organizationId') || !$this->post('body')) {
            $this->response(array('status' => 'error', 'message' => 'organizationId, Body of email are required '), 200);
        }
        $emailData = array(
            "organizationId" => $this->post('organizationId'),
            "userId" => $this->post('userId'),
            "name" => $this->post('name'),
            "subject" => $this->post('subject'),
            "body" => preg_replace("#&lt;(.*?)&gt;#", ' ', $this->post('body', FALSE)),
            "createdAt" => gmdate("Y-m-d H:i:s"),
            "modifiedAt" => gmdate("Y-m-d H:i:s")
        );
        $emailTemplates = $this->organizationmodel->createEmailTemplate($emailData);
        $this->response(array("status" => "success", "message" => $emailData['name'] . " template created successfully"), 200);
    }

    public function emailTemplate_put() {
        AuthManager::authenticate();
        if (!$this->put('organizationId') || !$this->put('body') || !$this->put('id')) {
            $this->response(array('status' => 'error', 'message' => 'organizationId,id, Body of email are required '), 200);
        }
        $id = $this->put('id');
        $emailData = array(
            "organizationId" => $this->put('organizationId'),
            "userId" => $this->put('userId'),
            "name" => $this->put('name'),
            "subject" => $this->put('subject'),
            "body" => $this->put('body'),
            "modifiedAt" => gmdate("Y-m-d H:i:s")
        );
        $emailTemplates = $this->organizationmodel->updateEmailTemplate($id, $emailData);
        $this->response(array("status" => "success", "message" => $emailData['name'] . " template updated successfully"), 200);
    }

    public function deleteEmailTemplate_get() {
        AuthManager::authenticate();
        if (!$this->get('id')) {
            $this->response(array('status' => 'error', 'message' => 'organizationId are required '), 200);
        }
        $id = $this->get('id');
        $this->organizationmodel->deleteEmailTemplate($id);
        $this->response(array('status' => 'success', 'message' => 'Email Template deleted successfully'), 200);
    }

    // --------------------------------------- End Email Templates -----------------------------------------


    public function getCurrentStages_get() {
        $orgId = $this->get('organizationId');
        if (!$orgId) {
            $this->response(array('status' => 'error', 'message' => 'OrgnaizationId is rquired to get the current lead stages'), 200);
        }
        $leadStages = $this->organizationmodel->getCustomLeadStages($orgId);
        $this->response($leadStages, 200);
    }

    // -----------------------------------------------------------------------------------------------------------------------
    // Delete account by deleting all account details
    public function deleteMyAccount_get() {
        if (!$this->get('organizationId')) {
            $this->response(array('status' => 'error', 'message' => 'organizationId is required '), 200);
        }
        $orgId = $this->get('organizationId');
        $orgEmail = $this->get('orgEmail');
        if ($this->organizationmodel->deleteOrgDetails($orgId, $orgEmail)) {
            // Email Contnet to send after deleting the account
            $delSubject = "Your account is being closed.";
            $delMessage = $this->accountCloseMSg();
            $maildata = array(
                "fromid" => "no-reply@helloleads.io",
                "toid" => $orgEmail,
                "subject" => $delSubject,
                "message" => $delMessage,
                "status" => 'pending',
                "date" => gmdate('Y-m-d H:i:s')
            );
            $mailqueueId = $this->emailqueuemodel->create($maildata);



            $this->response(array('status' => 'success', 'message' => 'Thank you for using helloleads. All your account details are permenantly deleted'), 200);
        }
    }

    // ---------------------------- End SMTP Configurations -------------------------------
    // -------------------------------------------------Need extension for trial ---------------------------------------
    public function trialExtensionReq_get() {
        AuthManager::authenticate();
        $orgId = $this->get('organizationId');
        if (!empty($orgId)) {
            // Check visitor count 
            $this->db->where('organizationId', $orgId);
            $vquery = $this->db->get('visitor');
            $vcount = $vquery->num_rows();
            $this->db->where('id', $orgId);
            $orgDet = $this->db->get('organization')->result();
            if ($vcount > 50 && $orgDet[0]->spflag3 < 4) {
                $today = gmdate("Y-m-d H:i:s");
                $ts1 = strtotime($today);
                $spflag3 = $orgDet[0]->spflag3 + 1;
                $premDate = gmdate('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $ts1) + 14, date('d', $ts1) - 1, date('y', $ts1)));
                $trialExtend = array(
                    "accountType" => 'P3',
                    "trialAccount" => '1',
                    "spflag3" => $spflag3,
                    "premiumExpiry" => $premDate,
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->where('id', $orgId);
                $this->db->update('organization', $trialExtend);
                $message = 'Dear ' . $orgDet[0]->contactFirstName . ' ' . $orgDet[0]->contactLastName . ' ,<br><br>

                    Thank you for showing interest in exploring HelloLeads. <br><br>

                    Based on your request, we have extended your trail period for 14 more days, to understand more and to evaluate HelloLeads further for your business.<br><br>

                    In case you have any questions on the usage of HelloLeads, please feel free to get in touch with us at support@helloleads.io. <a href="https://www.helloleads.io/pricing" >Click here</a> to get details about our Plans and Pricing for upgrading the free account to premium. <br>

                    We look forward to know your business needs and assisting you with HelloLeads. <br><br>
                    Thank you and Best Regards,<br><br>
                    Linda<br>
                    HelloLeads<br>
                    W: www.helloleads.io <br>
                    E: linda@helloleads.io<br>
                    T: +1 650 561 6681<br>
                    ';
                $subject = 'HelloLeads - Extension of Trial period';
                $notifyInternal = array(
                    "fromId" => "alert@helloleads.io",
                    "fromName" => "HelloLeads",
                    "toId" => $orgDet[0]->contactEmail,
                    "subject" => $subject,
                    "message" => $message,
                    "date" => gmdate('Y-m-d H:i:s')
                );

                $notifyInternalqueueId = $this->emailqueuemodel->LWcreate($notifyInternal);
                $this->response(array('status' => "success", "message" => "Thank you for your interest. As an exclusive offer, your trial has been extended by another 14 days."), 200);
            } else {
                $this->response(array('status' => "error", "message" => "Account exceeds restriction"), 200);
            }
        }
    }

    // Feedback popup -------------------------

    public function feedbackCheck_get() {
        $headers = apache_request_headers();
        $actionUserId = isset($headers["Xemail"]) ? $headers["Xemail"] : "";
        $orgId = $this->get('organizationId');
        $trialAlert = $this->trailCheck($orgId, $actionUserId);


        if ($trialAlert) {
            $this->response($trialAlert, 200);
        } else if ($paidAlert = $this->paidCheck($orgId, $actionUserId)) {
            $this->response($paidAlert, 200);
        } else if ($featureAlert = $this->featureCheck($orgId, $actionUserId)) {
            $this->response($featureAlert, 200);
        } else {
            $this->response(array(), 200);
        }
    }

    protected function trailCheck($orgId, $userId) {
        $this->db->where('id', $orgId);
        $this->db->where('trialAccount', '1');
        $trialAccount = $this->db->get('organization')->result();
        if (!empty($trialAccount)) {
            $today = gmdate("Y-m-d");
            $another = $trialAccount[0]->createdAt;
            $dateDiff = strtotime($today) - strtotime(date("Y-m-d", strtotime($another)));
            $compDay = floor($dateDiff / (60 * 60 * 24));
            $fdb = $this->getFeedback($orgId, $userId, 'T1');
            if ($compDay >= 3 && $compDay <= 5 && sizeof($fdb) === 0) {
                $feedback = array(
                    "organizationId" => $orgId,
                    "userId" => $userId,
                    "title" => "How has been your experience with HelloLeads so far?",
                    "accessStage" => "T1",
                    "times" => '1',
                    "createdAt" => gmdate("Y-m-d H:i:s"),
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->insert('feedback', $feedback);
                if ($this->db->insert_id()) {
                    return array('choice' => '1', 'times' => '1');
                }
            }
            if ($compDay >= 6 && $compDay <= 8 && sizeof($fdb) > 0 && $fdb[0]->times ==='1' && empty($fdb[0]->value)) {
                $feedback = array(
                    "times" => '2',
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->where('organizationId', $orgId);
                $this->db->where('userId', $userId);
                $this->db->where('accessStage', 'T1');
                if ($this->db->update('feedback', $feedback)) {
                    return array('choice' => '1', 'times' => '2');
                }
            }
            $fdb2 = $this->getFeedback($orgId, $userId, 'T2');
            if ($compDay >= 21 && $compDay <= 24 && sizeof($fdb2) === 0) {
                $feedback = array(
                    "organizationId" => $orgId,
                    "userId" => $userId,
                    "title" => "Are you considering for a paid plan upgradation?",
                    "accessStage" => "T2",
                    "times" => '1',
                    "createdAt" => gmdate("Y-m-d H:i:s"),
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->insert('feedback', $feedback);
                if ($this->db->insert_id()) {
                    return array('choice' => '2', 'times' => '1');
                }
            }
        }
    }

    protected function paidCheck($orgId, $userId) {
        $this->db->where('id', $orgId);
        $this->db->where('accountType !=', 'FR');
        $this->db->where('trialAccount', '0');
        $paidAccount = $this->db->get('organization')->result();
        if (!empty($paidAccount)) {
            $today = gmdate("Y-m-d");
            $another = $paidAccount[0]->premiumExpiry;
            $dateDiff = strtotime(date("Y-m-d", strtotime($another))) - strtotime($today);
            $compDay = floor($dateDiff / (60 * 60 * 24));
            $fdb = $this->getFeedback($orgId, $userId, 'P1');
            if ($compDay > 305 && $compDay < 335 && sizeof($fdb) === 0) {
                $feedback = array(
                    "organizationId" => $orgId,
                    "userId" => $userId,
                    "title" => "Need help in using HelloLeads?",
                    "accessStage" => "P1",
                    "times" => '1',
                    "createdAt" => gmdate("Y-m-d H:i:s"),
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->insert('feedback', $feedback);
                if ($this->db->insert_id()) {
                    return array('choice' => '3', 'times' => '1');
                }
            }
            $fp2 = $this->getFeedback($orgId, $userId, 'P2');
            if ($compDay > 286 && $compDay < 304 && sizeof($fp2) === 0) {
                $feedback = array(
                    "organizationId" => $orgId,
                    "userId" => $userId,
                    "title" => "How helpful is HelloLeads to your business?",
                    "accessStage" => "P2",
                    "times" => '1',
                    "createdAt" => gmdate("Y-m-d H:i:s"),
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->insert('feedback', $feedback);
                if ($this->db->insert_id()) {
                    return array('choice' => '4', 'times' => '1');
                }
            }

            if ($compDay > 256 && $compDay < 276 && sizeof($fp2) > 0 && $fp2[0]->times === '1' && empty($fp2[0]->value)) {
                $feedback = array(
                    "times" => '2',
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->where('organizationId', $orgId);
                $this->db->where('userId', $userId);
                $this->db->where('accessStage', 'P2');
                if ($this->db->update('feedback', $feedback)) {
                    return array('choice' => '4', 'times' => '2');
                }
            }
            $fp3 = $this->getFeedback($orgId, $userId, 'P3');
            if ($compDay > 216 && $compDay < 246 && sizeof($fp3) == 0) {
                $feedback = array(
                    "organizationId" => $orgId,
                    "userId" => $userId,
                    "title" => "How likely you are to recommend HelloLeads to your network?",
                    "accessStage" => "P3",
                    "times" => '1',
                    "createdAt" => gmdate("Y-m-d H:i:s"),
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->insert('feedback', $feedback);
                if ($this->db->insert_id()) {
                    return array('choice' => '5', 'times' => '1');
                }
            }
            if ($compDay > 176 && $compDay < 206 && $fp3[0]->times === '1' && empty($fp3[0]->value)) {
                $feedback = array(
                    "times" => '2',
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->where('organizationId', $orgId);
                $this->db->where('userId', $userId);
                $this->db->where('accessStage', 'P2');
                if ($this->db->update('feedback', $feedback)) {
                    return array('choice' => '5', 'times' => '2');
                }
            }
            $fp4 = $this->getFeedback($orgId, $userId, 'P4');
            if ($compDay > 126 && $compDay < 156 && sizeof($fp4) == 0) {
                $feedback = array(
                    "organizationId" => $orgId,
                    "userId" => $userId,
                    "title" => "What is the most one you like in HelloLeads?",
                    "accessStage" => "P3",
                    "times" => '1',
                    "createdAt" => gmdate("Y-m-d H:i:s"),
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->insert('feedback', $feedback);
                if ($this->db->insert_id()) {
                    return array('choice' => '6', 'times' => '1');
                }
            }
        }
    }

    protected function featureCheck($orgId, $userId) {

        $todayFed = $this->getFeedback($orgId, $userId, NULL, gmdate('Y-m-d'));
        // Hello Email
        $this->db->where('accessStage', 'F1');
        $this->db->where('organizationId', $orgId);
        $f1 = $this->db->get('feedback')->result();
        $this->db->where('organizationId', $orgId);
        $helloCount = $this->db->get('emailCampQueue')->num_rows();
        if (sizeof($f1) == 0 && $helloCount > 4 && empty($todayFed)) {
            $feedback = array(
                "organizationId" => $orgId,
                "userId" => $userId,
                "title" => "Hello Email",
                "accessStage" => "F1",
                "times" => '1',
                "createdAt" => gmdate("Y-m-d H:i:s"),
                "modifiedAt" => gmdate("Y-m-d H:i:s")
            );
            $this->db->insert('feedback', $feedback);
            if ($this->db->insert_id()) {
                return array('choice' => '7', 'times' => '1');
            }
        } else if (sizeof($f1) > 0 && $f1[0]->times === '1' && empty($f1[0]->value) && $helloCount > 4 && empty($todayFed)) {
            $today = gmdate("Y-m-d");
            $another = $f1[0]->createdAt;
            $dateDiff = strtotime($today) - strtotime(date("Y-m-d", strtotime($another)));
            $compDay = floor($dateDiff / (60 * 60 * 24));
            if ((int) $f1[0]->times < 2 && $compDay > 7) {
                $feedback = array(
                    "times" => '2',
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->where('organizationId', $orgId);
                $this->db->where('userId', $userId);
                $this->db->where('accessStage', 'F1');
                if ($this->db->update('feedback', $feedback)) {
                    return array('choice' => '7', 'times' => '2');
                }
            }
        }
        // Import leads
        $this->db->where('accessStage', 'F2');
        $this->db->where('organizationId', $orgId);
        $f2 = $this->db->get('feedback')->result();
        $this->db->where('organizationId', $orgId);
        $this->db->where('lead_Source', 'W_EX');
        $leadiCount = $this->db->get('visitor')->num_rows();
        if (sizeof($f2) == 0 && $leadiCount > 4 && empty($todayFed)) {
            $feedback = array(
                "organizationId" => $orgId,
                "userId" => $userId,
                "title" => "Import Leads",
                "accessStage" => "F2",
                "times" => '1',
                "createdAt" => gmdate("Y-m-d H:i:s"),
                "modifiedAt" => gmdate("Y-m-d H:i:s")
            );
            $this->db->insert('feedback', $feedback);
            if ($this->db->insert_id()) {
                return array('choice' => '8', 'times' => '1');
            }
        } else if (sizeof($f2) > 0 && $f2[0]->times === '1' && empty($f2[0]->value) && $leadiCount > 4 && empty($todayFed)) {
            $today = gmdate("Y-m-d");
            $another = $f2[0]->createdAt;
            $dateDiff = strtotime($today) - strtotime(date("Y-m-d", strtotime($another)));
            $compDay = floor($dateDiff / (60 * 60 * 24));
            if ((int) $f2[0]->times < 2 && $compaDay > 7) {
                $feedback = array(
                    "times" => '2',
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->where('organizationId', $orgId);
                $this->db->where('userId', $userId);
                $this->db->where('accessStage', 'F2');
                if ($this->db->update('feedback', $feedback)) {
                    return array('choice' => '8', 'times' => '2');
                }
            }
        }
        //Facebook leads
        $this->db->where('accessStage', 'F3');
        $this->db->where('organizationId', $orgId);
        $f3 = $this->db->get('feedback')->result();
        $this->db->where('organizationId', $orgId);
        $this->db->where('lead_Source', 'FB_L');
        $leadfCount = $this->db->get('visitor')->num_rows();
        if (sizeof($f3) == 0 && $leadfCount > 4 && empty($todayFed)) {
            $feedback = array(
                "organizationId" => $orgId,
                "userId" => $userId,
                "title" => "Facebook Integration",
                "accessStage" => "F3",
                "times" => '1',
                "createdAt" => gmdate("Y-m-d H:i:s"),
                "modifiedAt" => gmdate("Y-m-d H:i:s")
            );
            $this->db->insert('feedback', $feedback);
            if ($this->db->insert_id()) {
                return array('choice' => '9', 'times' => '1');
            }
        } else if (sizeof($f3) > 0 && $f3[0]->times === '1' && empty($f3[0]->value) && leadfCount > 4 && empty($todayFed)) {
            $today = gmdate("Y-m-d");
            $another = $f3[0]->createdAt;
            $dateDiff = strtotime($today) - strtotime(date("Y-m-d", strtotime($another)));
            $compDay = floor($dateDiff / (60 * 60 * 24));
            if ((int) $f3[0]->times < 2 && $compDay > 7) {
                $feedback = array(
                    "times" => '2',
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->where('organizationId', $orgId);
                $this->db->where('userId', $userId);
                $this->db->where('accessStage', 'F3');
                if ($this->db->update('feedback', $feedback)) {
                    return array('choice' => '9', 'times' => '2');
                }
            }
        }
        //DEF leads
        $this->db->where('accessStage', 'F4');
        $this->db->where('organizationId', $orgId);
        $f4 = $this->db->get('feedback')->result();
        $this->db->where('organizationId', $orgId);
        $this->db->where('lead_Source', 'R_FO');
        $leaddCount = $this->db->get('visitor')->num_rows();
        if (sizeof($f4) == 0 && $leaddCount > 4 && empty($todayFed)) {
            $feedback = array(
                "organizationId" => $orgId,
                "userId" => $userId,
                "title" => "DEF Integration",
                "accessStage" => "F4",
                "times" => '1',
                "createdAt" => gmdate("Y-m-d H:i:s"),
                "modifiedAt" => gmdate("Y-m-d H:i:s")
            );
            $this->db->insert('feedback', $feedback);
            if ($this->db->insert_id()) {
                return array('choice' => '10', 'times' => '1');
            }
        } else if (sizeof($f4) > 0 && $f4[0]->times === '1' && empty($f4[0]->value) && $leaddCount > 4 && empty($todayFed)) {
            $today = gmdate("Y-m-d");
            $another = $f4[0]->createdAt;
            $dateDiff = strtotime($today) - strtotime(date("Y-m-d", strtotime($another)));
            $compDay = floor($dateDiff / (60 * 60 * 24));
            if ((int) $f4[0]->times < 2 && $compDay > 7) {
                $feedback = array(
                    "times" => '2',
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->where('organizationId', $orgId);
                $this->db->where('userId', $userId);
                $this->db->where('accessStage', 'F4');
                if ($this->db->update('feedback', $feedback)) {
                    return array('choice' => '10', 'times' => '2');
                }
            }
        }
        //Website leads
        $this->db->where('accessStage', 'F5');
        $this->db->where('organizationId', $orgId);
        $f5 = $this->db->get('feedback')->result();
        $this->db->where('organizationId', $orgId);
        $this->db->where('lead_Source', 'W_Enq');
        $leadwCount = $this->db->get('visitor')->num_rows();

        if (sizeof($f5) == 0 && $leadwCount > 4 && empty($todayFed)) {
            $feedback = array(
                "organizationId" => $orgId,
                "userId" => $userId,
                "title" => "Website Integration",
                "accessStage" => "F5",
                "times" => '1',
                "createdAt" => gmdate("Y-m-d H:i:s"),
                "modifiedAt" => gmdate("Y-m-d H:i:s")
            );
            $this->db->insert('feedback', $feedback);
            if ($this->db->insert_id()) {
                return array('choice' => '11', 'times' => '1');
            }
        } else if (sizeof($f5) > 0 && $f5[0]->times === '1' && empty($f5[0]->value) && $leadwCount > 4 && empty($todayFed)) {
            $today = gmdate("Y-m-d");
            $another = $f5[0]->createdAt;
            $dateDiff = strtotime($today) - strtotime(date("Y-m-d", strtotime($another)));
            $compDay = floor($dateDiff / (60 * 60 * 24));
            if ((int) $f5[0]->times < 2 && $compDay > 7) {
                $feedback = array(
                    "times" => '2',
                    "modifiedAt" => gmdate("Y-m-d H:i:s")
                );
                $this->db->where('organizationId', $orgId);
                $this->db->where('userId', $userId);
                $this->db->where('accessStage', 'F5');
                if ($this->db->update('feedback', $feedback)) {
                    return array('choice' => '11', 'times' => '2');
                }
            }
        }
    }

    protected function getFeedback($orgId, $userId, $access = NULL, $date = NULL) {
        $this->db->where('organizationId', $orgId);
        $this->db->where('userId', $userId);
        if ($access) {
            $this->db->where('accessStage', $access);
        }
        if ($date) {
            $this->db->where('DATE(modifiedAt)', $date);
        }
        return $this->db->get('feedback')->result();
    }

    public function submitFeedback_post() {
        $orgId = $this->post('organizationId');
        $access = $this->post('access');
        $userId = $this->post('userId');
        $value = $this->post('value');
        $description = $this->nullIfEmpty($this->post('description'));
        $orgDet = $this->organizationmodel->getBy('id', $orgId);
        $userDet = $this->usermodel->getBy('id', $userId);
        $this->db->where('organizationId', $orgId);
        $this->db->where('accessStage', $access);
        $feedbackDet = $this->db->get('feedback')->result();
        if (!empty($orgDet) && !empty($userDet) && !empty($feedbackDet)) {
            $feedData = array(
                "value" => $value,
                "description" => $description,
                "modifiedAt" => gmdate('Y-m-d H:i:s')
            );
            $this->db->where('id', $feedbackDet[0]->id);
            $this->db->update('feedback', $feedData);
            $tempmessage = $this->getFeedbackMessage($orgDet, $userDet, $feedbackDet,$value,$description);
            $maildata = array(
                "fromId" => "alert@helloleads.io",
                "toId" => "support@helloleads.io",
                "subject" => "HelloLeads Rating and Feedback alert -" . $userDet[0]->firstName . "," . $orgDet[0]->displayName,
                "message" => $tempmessage,
                "date" => gmdate('Y-m-d H:i:s')
            );
            $mailqueueId = $this->emailqueuemodel->LWcreate($maildata);


            $this->response(array("status" => "success", "message" => "Feedback submitted sucessfully"), 200);
        }
    }

    // --------------------------------------------- End of trial extension -----------------------------------------------
    // Generate / Get API key fro this account
    public function apiRequest_get() {
        $orgId = $this->get('orgId');
        if (empty($orgId)) {
            $this->response(array('status' => 'error', 'message' => "OrganizationId is required to provide API key"), 200);
        }
        $apiDet = $this->apimodel->apiKeyByOrg($orgId);
        if (!empty($apiDet)) {
            $this->response($apiDet, 200);
        } else {
            $this->response(NULL, 200);
        }
    }

    public function createApiKey_post() {
        $orgId = $this->post('orgId');
        $userId = $this->post('userId');
        if (empty($orgId)) {
            $this->response(array('status' => 'error', 'message' => "OrganizationId is required to generate API key"), 200);
        }
        $orgDet = $this->organizationmodel->getBy('id', $orgId);
        $apiDet = $this->apimodel->apiKeyByOrg($orgId);
        if (!empty($orgDet)) {


            if (empty($apiDet)) {
                $orgEmail = $orgDet[0]->contactEmail;

                $generateDet = $orgId . $orgEmail . strtotime(gmdate('Y-m-d H:i:s'));
                // Generating api key
                $apiKey = md5(sha1($generateDet));

                $apiData = array(
                    "organizationId" => $orgId,
                    "apiKey" => $apiKey,
                    "xEmail" => $orgEmail,
                    "requestUserId" => $userId,
                    "createdAt" => gmdate('Y-m-d H:i:s'),
                    "modifiedAt" => gmdate('Y-m-d H:i:s')
                );

                $apiId = $this->apimodel->apiCreate($apiData);
                $apiData['id'] = $apiId;

                $this->response($apiData, 200);
            } else {
                $this->response($apiDet[0], 200);
            }
        }
    }

    // ----------------------------------------- End Generate API key ------------------------------------ 
    // Walkthrough contnet 
    public function walkThroughContent_get() {

        if (!empty($this->get('lcount'))) {
            $lcount = $this->get('lcount');
            $firstName = $this->get('firstName');
            $response = array();
            if ($lcount === '2') {

                $response = array("title" => "Digi - Business Card Sharing",
                    "content" => "Hello " . $firstName . "<br/><br/>

Would you like to share your contact details with your leads and prospects ?<br/><br/>

Leads having your contact data readily are more likely to come back and transact business with you.<br/><br/>Digital Business Card Sharing option in HelloLeads will help you in this.<br/><br/>",
                    "imgSrc" => "http://www.localhost/eventHello/images/DigiSteps.png",
                    "identity1" => "DigitryImport",
                    "identity2" => "DigilearnId",
                    "llink" => "https://www.helloleads.io/blog/recent_updates/digi-business-card-sharing.php",
                    "tlink" => "account/userProfile"
                );
            }
            if ($lcount === '5') {

                $response = array("title" => "Import Leads",
                    "content" => "Hello " . $firstName . "<br/><br/>

How about importing your existing leads into your HelloLeads account<br/> and managing all leads in one place ?<br/><br/>

It is simple and quick.<br/><br/>",
                    "imgSrc" => "http://www.localhost/eventHello/images/importSteps.png",
                    "identity1" => "tryImport",
                    "identity2" => "LImport",
                    "llink" => NULL,
                    "tlink" => "account/datamange"
                );
            }

            if ($lcount === '9') {

                $response = array("title" => "Export Leads",
                    "content" => "Hello " . $firstName . "<br/><br/>

Exporting your leads from from your HelloLeads account as a CSV file is easy and quick.<br/><br/>

",
                    "imgSrc" => "http://www.localhost/eventHello/images/exportSteps.png",
                    "identity1" => "tryExport",
                    "identity2" => "LExport",
                    "llink" => NULL,
                    "tlink" => "account/datamange"
                );
            }
            if ($lcount === '12') {

                $response = array("title" => "Hello Email",
                    "content" => "Hello " . $firstName . "<br/><br/>

Sending emails, in one go, to all your leads with a customized email content is easier than you think.<br/><br/>

Hello Email, a premium feature of HelloLeads helps you to send emails to your leads without any additional programs or integrations.<br/><br/>",
                    "imgSrc" => NULL,
                    "identity1" => "tryHEmail",
                    "identity2" => "tryHEmail",
                    "llink" => "https://www.helloleads.io/blog/recent_updates/bulk_email.php",
                    "tlink" => "account/helloEmail"
                );
            }
            if ($lcount === '16') {

                $response = array("title" => "Website Enquiry Integration",
                    "content" => "Hello " . $firstName . "<br/><br/>

Would you like to integrate your website enquiries with your HelloLeads account ?<br/><br/>

It is quick and easy.<br/><br/>",
                    "imgSrc" => "http://www.localhost/eventHello/images/IWebSteps.png",
                    "identity1" => "tryInWeb",
                    "identity2" => "lInWeb",
                    "llink" => NULL,
                    "tlink" => "account/integration"
                );
            }
            if ($lcount === '19') {

                $response = array("title" => "Digital Enquiry Form",
                    "content" => "Hello " . $firstName . "<br/><br/>

In addition to adding leads using Mobile and Web form, HelloLeads gives you an additional option<br/> to capture your lead details using a Digital Enquiry Form.
<br/><br/>

It helps you best when you are exhibiting in a trade show/ event.<br/><br/>",
                    "imgSrc" => "http://www.localhost/eventHello/images/DEFGuide.png",
                    "identity1" => "tryDEF",
                    "identity2" => "lDEF",
                    "llink" => "https://www.helloleads.io/blog/recent_updates/digital-enquiry-form.php",
                    "tlink" => "account/integration"
                );
            }

            $this->response($response, 200);
        }
    }

    // ------------------------------------------------ Payment promotions start ------------------------------------------

    public function trialPaymentPopup_get() {
        if (!empty($this->get('orgId'))) {
            $response = array();
            $userName = $this->session->userdata('username');
            $userEmail = $this->session->userdata('userEmail');
            $organizationId = $this->session->userdata('organizationId');
            $trialAccount = $this->session->userdata('trialAccount');
            $orgDet = $this->organizationmodel->getBy('id', $organizationId);
            if (!empty($orgDet)) {
                $now = strtotime(gmdate("Y-m-d")); // or your date as well
                $pre_date = strtotime(date("Y-m-d", strtotime($orgDet[0]->createdAt)));
                $datediff = $now - $pre_date;
                $compDay = floor($datediff / (60 * 60 * 24));
                log_message("error", $compDay);
                if ($compDay >= 10 and $compDay <= 14 && $trialAccount === '1') {

                    if ((int) $orgDet[0]->spflag1 < 2) {
                        //log_message("error", "Inside if" . $compDay);
                        $response = array("status" => "success",
                            "title" => "",
                            "content" => "<h2><b>Congratulations!! <br/><br/> You have got 2 months free subscription.</b></h2><br/><p style='font-size:13px;'><i> Get 2 months extra by upgrading before your trail ending on " . date("d-M-Y", strtotime($orgDet[0]->premiumExpiry)) . "</i></p>",
                            "imgSrc" => "",
                            "identity1" => "",
                            "identity2" => "tryFirst",
                            "llink" => "",
                            "tlink" => "account/pricing",
                            "tname" => "Upgrade Today",
                            "ftime" => $compDay
                        );
                        $flagC = (int) $orgDet[0]->spflag1 + 1;
                        $orgData = array("spflag1" => $flagC);
                        $this->db->where('id', $organizationId);
                        $this->db->update('organization', $orgData);
                    }
                    $this->response($response, 200);
                }
                if ($compDay >= 16 and $compDay <= 20 && $trialAccount === '1') {

                    if ((int) $orgDet[0]->spflag2 < 2) {

                        $nowTime = strtotime(gmdate("Y-m-d")); // or your date as well
                        $premium_date = strtotime(date("Y-m-d", strtotime($orgDet[0]->premiumExpiry)));
                        $datepremiumdiff = $nowTime - $premium_date;
                        $numDays = floor($datepremiumdiff / (60 * 60 * 24));

                        // log_message("error", "Inside if" . $compDay);
                        $response = array("status" => "success",
                            "title" => "",
                            "content" => "<h2><b>Exclusive offer for you:<br/><br/>We have extended your trail by another 14 days.<b/></h2><p style='font-size:13px;'><i> Also get 1 month additional by upgrading before your trail ends (before " . date("d-M-Y", strtotime($orgDet[0]->premiumExpiry)) . ", " . $numDays . " days to go)</i></p>",
                            "imgSrc" => "",
                            "identity1" => "",
                            "identity2" => "trySencond",
                            "llink" => "",
                            "tlink" => "account/pricing",
                            "tname" => "Upgrade Today",
                            "ftime" => $compDay
                        );
                        $flagC = (int) $orgDet[0]->spflag2 + 1;
                        $orgData = array("spflag2" => $flagC);
                        $this->db->where('id', $organizationId);
                        $this->db->update('organization', $orgData);
                    }
                    $this->response($response, 200);
                }
                if ($compDay >= 25 and $compDay <= 28 && $trialAccount === '1') {

                    if ((int) $orgDet[0]->spflag3 < 2) {
                        //log_message("error", "Inside if" . $compDay);
                        $response = array("status" => "success",
                            "title" => "",
                            "content" => "<h2><b>Upgrade today to get 1 month free.<b/></h2><br/><br/><p style='font-size:13px;'><i> Get 1 month extra by upgrading before your trial ends on " . date("d-M-Y", strtotime($orgDet[0]->premiumExpiry)) . "</i></p>",
                            "imgSrc" => "",
                            "identity1" => "",
                            "identity2" => "tryThird",
                            "llink" => "",
                            "tlink" => "account/pricing",
                            "tname" => "Upgrade",
                            "ftime" => $compDay
                        );
                        $flagC = (int) $orgDet[0]->spflag3 + 1;
                        $orgData = array("spflag3" => $flagC);
                        $this->db->where('id', $organizationId);
                        $this->db->update('organization', $orgData);
                    }
                    $this->response($response, 200);
                }

                if ($compDay >= 29 && $orgDet[0]->accountType === 'FR') {

                    if ((int) $orgDet[0]->spflag4 < 2) {
                        log_message("error", "Inside if" . $compDay);
                        $response = array("status" => "success",
                            "title" => "",
                            "content" => "<h2><b>You use HelloLeads a lot. We like that.<b/></h2><br/><br/><p style='font-size:13px;'><i>You have reached the end of your free trial. Upgrade to a premium plan of your choice to continue and enjoy the full power of HelloLeads for your business.</i></p>",
                            "imgSrc" => "",
                            "identity1" => "",
                            "identity2" => "tryFourth",
                            "llink" => "",
                            "tlink" => "account/pricing",
                            "tname" => "Upgrade Today",
                            "ftime" => $compDay
                        );
                        $flagC = (int) $orgDet[0]->spflag4 + 1;
                        $orgData = array("spflag4" => $flagC);
                        $this->db->where('id', $organizationId);
                        $this->db->update('organization', $orgData);

                        // Send alert email -------------------------------------------------
                        //log_message("error",$orgDet[0]->spflag4);
                        if ((int) $orgDet[0]->spflag4 == 1) {
                            $message = '<h3><u>HelloLeads post trial usage alert - Web </u></h3><br><br>
                                        Below are details of a customer who is attempting to use HelloLeads after trial period has ended. The customer may be interested in using HelloLeads but not upgraded to a premium plan.<br><br>
                                        <u>Account Details</u><br>
                                        Account Name : ' . $orgDet[0]->displayName . ' <br><br>
                                        AccountId : HLS0000' . $orgDet[0]->id . ' <br><br>   
                                        Name : ' . $orgDet[0]->contactFirstName . ' ' . $orgDet[0]->contactLastName . '<br><br>    
                                        Email :  ' . $orgDet[0]->contactEmail . '<br><br>
                                        Mobile : ' . $orgDet[0]->contactPhoneCode . $orgDet[0]->contactPhone . ' <br><br>
                                        Registration Date : ' . date("d-M-Y", strtotime($orgDet[0]->createdAt)) . '<br><br>
                                        Country:' . $orgDet[0]->country . '<br><br>     
                                        <br><br>
                                        <u>User Details</u><br>
                                        Name : ' . $userName . '<br><br>    
                                        Email :  ' . $useremail . '<br><br>
                                        Talking to above customer today will improve the possibility of helping him or her to upgrade to a premium plan.<br><br>
                                        Please review and take it forward.<br><br>
                                        -------------------------------------------------<br>
                                        This is a system generated email from HelloLeads 
                                       ';
                            $subject = 'Free user has acknowledged the pricing alert twice - Web';
                            $notifyInternal = array(
                                "fromId" => "alert@helloleads.io",
                                "fromName" => "HelloLeads",
                                "toId" => "support@helloleads.io",
                                "subject" => $subject,
                                "message" => $message,
                                "date" => gmdate('Y-m-d H:i:s')
                            );

                            $notifyInternalqueueId = $this->emailqueuemodel->LWcreate($notifyInternal);

                            // Sending mail to customer for post trial usage
                            /* $cussubject = $orgDet[0]->displayName.' - Request for another 14 days trial extensions';
                              $cmessage = 'Dear '.$userName.',<br/><br/>

                              Thanks for registering the trial version on '.date("d-M-Y", strtotime($orgDet[0]->createdAt)).'.<br/><br/>

                              While it was valid for 14 days, I understand  that you may be interested in using the application for a further period of 14 days to check out the great new features that we have introduced recently like FaceBook integration, Visiting card scanner, WhatsApp connectivity, invoice generation and so on.   <br/><br/>

                              Please click on <a href="mailto:support@helloleads.io?subject= '.$cussubject.'">TRIAL</a> to help us renew your usage by another 14 days. Thanks.<br/><br/>

                              We look forward to hearing from you.<br/><br/>

                              Regards,<br/>
                              Linda
                              ';
                              $csubject = 'HelloLeads - Would you like to extend your trial period?'; */
                            $cmessage = '';
                            $csubject = '';
                            $tempDet = $this->emailtemplatesmodel->getByName('Request for extension');
                            if (!empty($tempDet)) {
                                if (!empty($tempDet[0]->body) && !empty($tempDet[0]->subject)) {
                                    $cmessage = $tempDet[0]->body;
                                    $cmessage = str_replace("#VisitorName#", !empty($orgDet[0]->contactFirstName) ? $orgDet[0]->contactFirstName : '', $cmessage);
                                    $cmessage = str_replace("#VisitorCompanyName#", $orgDet[0]->displayName, $cmessage);
                                    $cmessage = str_replace("#Email#", $orgDet[0]->contactEmail, $cmessage);
                                    $cmessage = str_replace("#pdate#", date("d-M-Y", strtotime($orgDet[0]->createdAt)), $cmessage);

                                    $csubject = $tempDet[0]->subject;
                                }
                            }
                            $cnotifyInternal = array(
                                "fromId" => "alert@helloleads.io",
                                "fromName" => "HelloLeads",
                                "toId" => $userEmail,
                                "subject" => $csubject,
                                "message" => $cmessage,
                                "date" => gmdate('Y-m-d H:i:s')
                            );

                            $cnotifyInternalqueueId = $this->emailqueuemodel->create($cnotifyInternal);
                        }
                    }
                    $this->response($response, 200);
                }
                if (empty($response)) {
                    $this->response(array("status" => "error", "message" => ""), 200);
                }
            }
        }
    }

    // ------------------------------------------------- Payment Promotion End --------------------------------------------
// ------------------------------------------------ Premium Pop up for subscribed users -------------------------------------
    public function premiumPopup_get() {
        if (!empty($this->get('orgId'))) {
            $response = array();
            $userName = $this->session->userdata('username');
            $userEmail = $this->session->userdata('userEmail');
            $organizationId = $this->session->userdata('organizationId');
            $trialAccount = $this->session->userdata('trialAccount');
            $orgDet = $this->organizationmodel->getBy('id', $organizationId);
            if (!empty($orgDet)) {
                $now = strtotime(gmdate("Y-m-d")); // or your date as well
                $pre_date = strtotime(date("Y-m-d", strtotime($orgDet[0]->premiumExpiry)));
                $datediff = $pre_date - $now;
                $compDay = floor($datediff / (60 * 60 * 24));
                $lastLogin = $this->session->userdata('lastLogin');
                // 30-60 days (1-2 months)

                if (($compDay == 330 || $compDay == 320) && $orgDet[0]->trialAccount == '0' && $orgDet[0]->accountType != 'FR' && date("Y-m-d", strtotime($lastLogin)) !== date('Y-m-d')) {
                    $response = array("status" => "success",
                        "title" => "Need Help?",
                        "content" => "If you need a demo or assistance in leveraging HelloLeads for your business, please feel free to contact us.",
                        "identity1" => "l1link",
                        "identity2" => "l2link",
                        "identity3" => "plink",
                        "l1link" => "https://calendly.com/helloleads/demo/",
                        "l1name" => "Request a demo",
                        "l2link" => "https://www.helloleads.io/support.php",
                        "l2name" => "Request a call back",
                        "plink" => "https://www.helloleads.io/support.php",
                        "pname" => "Contact Support"
                    );
                    $this->response($response, 200);
                }

                // 120-150 days (3-4 months)


                if (($compDay == 240 || $compDay == 220) && $orgDet[0]->trialAccount == '0' && $orgDet[0]->accountType != 'FR' && date("Y-m-d", strtotime($lastLogin)) !== date('Y-m-d')) {
                    $response = array("status" => "success",
                        "title" => "Add 3 months free by sharing your experience",
                        "content" => "Share your experience on how HelloLeads is useful for your business (and get 3 months free subscription).",
                        "identity1" => "l1link",
                        "identity2" => "l2link",
                        "identity3" => "plink",
                        "l1link" => "",
                        "l1name" => "Request a demo",
                        "l2link" => "",
                        "l2name" => "Request a call back",
                        "plink" => "https://www.helloleads.io/support.php",
                        "pname" => "Here is how"
                    );
                    $this->response($response, 200);
                }

                // 180 – 210  days (6-7 months)

                if (($compDay == 185 || $compDay == 160) && $orgDet[0]->trialAccount == '0' && $orgDet[0]->accountType != 'FR' && date("Y-m-d", strtotime($lastLogin)) !== date('Y-m-d')) {
                    $response = array("status" => "success",
                        "title" => "Help other businesses grow and get 2 months free",
                        "content" => "Recommend HelloLeads to your business contacts and get 2 months free subscription when they sign up for a premium plan (they also get a month free).",
                        "identity1" => "l1link",
                        "identity2" => "l2link",
                        "identity3" => "plink",
                        "l1link" => "",
                        "l1name" => "Request a demo",
                        "l2link" => "",
                        "l2name" => "Request a call back",
                        "plink" => "https://www.helloleads.io/refer-today-and-help-others-grow/",
                        "pname" => "Refer now"
                    );
                    $this->response($response, 200);
                }

                // For Re-conversion
                // 30-15 days before expiry
                if (($compDay == 20 || $compDay == 25) && $orgDet[0]->trialAccount == '0' && $orgDet[0]->accountType != 'FR' && date("Y-m-d", strtotime($lastLogin)) !== date('Y-m-d')) {
                    $response = array("status" => "success",
                        "title" => "Special Offer : Get 1 month additional by upgrading before your subscription ends.",
                        "content" => "Your subscription expires on " . date('d-M-Y', strtotime($orgDet[0]->premiumExpiry)) . " (i.e in " . $compDay . " days). Re-subscribe before your plan expires and get one month added to account. Let good times continue without any interruptions.",
                        "identity1" => "l1link",
                        "identity2" => "l2link",
                        "identity3" => "plink",
                        "l1link" => "",
                        "l1name" => "Request a demo",
                        "l2link" => "",
                        "l2name" => "Request a call back",
                        "plink" => base_url() . "app/account/pricing",
                        "pname" => "Renew today"
                    );
                    $this->response($response, 200);
                }

                // In the last 5 days before expiry 
                if (($compDay == 2 || $compDay == 5) && $orgDet[0]->trialAccount == '0' && $orgDet[0]->accountType != 'FR' && date("Y-m-d", strtotime($lastLogin)) !== date('Y-m-d')) {
                    $response = array("status" => "success",
                        "title" => "Special Offer : Get 1 month additional by upgrading before your subscription ends.",
                        "content" => "Your subscription expires on " . date('d-M-Y', strtotime($orgDet[0]->premiumExpiry)) . " (i.e in " . $compDay . " days). Re-subscribe before your plan expires and get one month added to account. Let good times continue without any interruptions.",
                        "identity1" => "l1link",
                        "identity2" => "l2link",
                        "identity3" => "plink",
                        "l1link" => "",
                        "l1name" => "Request a demo",
                        "l2link" => "",
                        "l2name" => "Request a call back",
                        "plink" => base_url() . "app/account/pricing",
                        "pname" => "Renew today"
                    );
                    $this->response($response, 200);
                }
            }
        }
    }

    //--------------------------------------------------- End Premium pop up for subscribed users ------------------------------    

    public function breakbysource_get() {

        AuthManager::authenticatelw();

        $orgs = array();
        if ($this->get('orgid')) {
            //$orgs = $this->organizationmodel->getBy($this->get('key'), $this->get('value'));
            $mfo = $this->visitormodel->getVisitorBymfo1($this->get('orgid'));
            $mbc = $this->visitormodel->getVisitorBymbc1($this->get('orgid'));
            $mqr = $this->visitormodel->getVisitorBymqr1($this->get('orgid'));
            $mqfo = $this->visitormodel->getVisitorBymqfo1($this->get('orgid'));
            $wfo = $this->visitormodel->getVisitorBywfo1($this->get('orgid'));
            $wexcel = $this->visitormodel->getVisitorBywexcel1($this->get('orgid'));
            $rwfo = $this->visitormodel->getVisitorByrwfo1($this->get('orgid'));
            $wenq = $this->visitormodel->getVisitorBywenq1($this->get('orgid'));
            $iOsfo = $this->visitormodel->getVisitorByifo1($this->get('orgid'));
            $iOsqr = $this->visitormodel->getVisitorByiqr1($this->get('orgid'));
            $iOsbc = $this->visitormodel->getVisitorByibc1($this->get('orgid'));
            $fbl = $this->visitormodel->getVisitorByfbl1($this->get('orgid'));
            $apir = $this->visitormodel->getVisitorByapi1($this->get('orgid'));
            $total = $mfo + $mbc + $mqr + $mqfo + $wfo + $rwfo + $wenq + $iOsfo + $iOsqr + $wexcel + $iOsbc + $apir + $fbl;
            $orgs['mfo'] = $mfo;
            $orgs['mbc'] = $mbc;
            $orgs['mqr'] = $mqr;
            $orgs['mqfo'] = $mqfo;
            $orgs['wfo'] = $wfo;
            $orgs['rwfo'] = $rwfo;
            $orgs['wexcel'] = $wexcel;
            $orgs['wenq'] = $wenq;
            $orgs['iosfo'] = $iOsfo;
            $orgs['iosqr'] = $iOsqr;
            $orgs['iosbc'] = $iOsbc;
            $orgs['fbl'] = $fbl;
            $orgs['apir'] = $apir;

            $orgs['total'] = $total;
            $this->response($orgs, 200);
        }
    }

    public function planDet_get() {
        AuthManager::authenticate();
        $planQuery = $this->db->get('priceplan');
        $planResult = $planQuery->result();
        $this->response($planResult, 200);
    }

    public function getOrgCarddetails_get() {
        AuthManager::authenticate();
        if (!empty($this->get('cardId')) && !empty($this->get('customerId'))) {
            require "./application/libraries/stripe/init.php";
            \Stripe\Stripe::setApiKey('sk_test_UmWtrUk5f2EK9IDPjR4ijfZF');
            $cardId = $this->get('cardId');
            $customerId = $this->get('customerId');
            $customer = \Stripe\Customer::retrieve($customerId);
            //log_message("error",json_encode($customer));
            $card = $customer->sources->retrieve($cardId);
            $this->response($card, 200);
        }
    }
    
    public function getSmsConfig_get(){
         AuthManager::authenticate();
         if (!empty($this->get('organizationId'))) {
             $orgId = $this->get('organizationId');
             $orgsms = $this->smsqueuemodel->getSmsConfig($orgId);
             $this->response($orgsms,200);
         }else{
              $this->response(array("status"=>"error","message"=>"OrganizationID is required"), 200);
         }
    }

    public function orgChecks_get() {
        AuthManager::authenticate();
        $orgId = $this->get('organizationId');
        $req = $this->get('checkReq');
        $orgdet = array();
        $orgdettemp = array();
        //log_message("error", $orgId);
        if (!empty($orgId)) {

            $orgdettemp = $this->organizationmodel->getById($orgId);
            if (!empty($orgdettemp)) {
                //Getting Plan details
                $this->db->where('planNameId', $orgdettemp[0]->accountType);
                $planDetQuery = $this->db->get('pricePlan');
                $planDetResult = $planDetQuery->result();
                if ($req === 'user') {
                    $this->db->where('organizationId', $orgId);
                    $this->db->where('status', '0');
                    $userQuery = $this->db->get('users');
                    $this->db->where('organizationId', $orgId);
                    $this->db->where('status', '0');
                    $creditQuery = $this->db->get('credit');
                    $creditResult = $creditQuery->result();
                    if (!empty($creditResult)) {
                        $noUsers = $userQuery->num_rows();
                        if ($noUsers == $creditResult[0]->multi_user) {
                            $message = array(
                                'status' => 'error',
                                'message' => 'The maximum number of users allowed for your account is ' . $creditResult[0]->multi_user . '.  You already have ' . $noUsers . ' users. Please upgrade your plan or subscribe for additional users.');
                            $this->response($message, 200);
                        } else {
                            $message = array(
                                'status' => 'success',
                                'message' => '');
                            $this->response($message, 200);
                        }
                    }
                }
                if ($req === 'visitor') {
                    if (!empty($planDetResult)) {
                        if ($planDetResult[0]->credits !== '0') {
                            $this->db->where('organizationId', $orgId);
                            $creditQuery = $this->db->get('credit');
                            $creditDet = $creditQuery->result();
                            if (!empty($creditDet)) {
                                if ((int) $creditDet[0]->remainingCredit == 0) {

                                    $message = array(
                                        'status' => 'error',
                                        'message' => 'The maximum number of leads allowed to be managed for this ' . $planDetResult[0]->planName . ' plan is ' . $planDetResult[0]->credits . '. You have reached this limit. To capture more leads, upgrade your plan.');
                                    $this->response($message, 200);
                                } else {
                                    $message = array(
                                        'status' => 'success',
                                        'message' => '');
                                    $this->response($message, 200);
                                }
                            }
                        } else {
                            $message = array(
                                'status' => 'success',
                                'message' => '');
                            $this->response($message, 200);
                        }
                    }
                }
                if ($req === 'trialEnd') {
                    $this->db->where("id", $orgId);
                    $orgQuery = $this->db->get('organization');
                    $orgDetails = $orgQuery->result();
                    if (!empty($orgDetails)) {
                        if ($orgDetails[0]->accountType === 'FR') {
                            $response = array("status" => "error",
                                "title" => "Upgrade & enjoy full power of HelloLeads",
                                "content" => "We appreciate your use of HelloLeads for your business during the trial period. Your trial has ended. Please upgrade to one of the premium plans for better lead management and improved sales conversions",
                                "identity1" => "l1link",
                                "identity2" => "l2link",
                                "identity3" => "plink",
                                "l1link" => "https://www.helloleads.io/support.php",
                                "l1name" => "Request for extension",
                                "l2link" => "https://www.helloleads.io/support.php",
                                "l2name" => "Contact Us",
                                "pname" => "Upgrade Today",
                            );
                            $this->response($response, 200);
                        } else {
                            $message = array(
                                'status' => 'success',
                                'message' => '');
                            $this->response($message, 200);
                        }
                    }
                }
                if (!empty($planDetResult) && $req !== 'user' && $req !== 'visitor' && $req !== 'trialEnd') {
                    log_message("error", $planDetResult[0]->$req);
                    if ($planDetResult[0]->$req === '0') {

                        $message = array(
                            'status' => 'success',
                            'message' => '');
                        $this->response($message, 200);
                    } else {
                        $feature = $this->getFeatureDet($req);
                        $message = array(
                            'status' => 'error',
                            'title' => $feature,
                            'message' => $feature . " feature is not available in your current <b>" . $planDetResult[0]->planName . "</b> plan. (<a href='https://www.helloleads.io/pricing.php' target='_blank'> View pricing plan details </a>)");
                        $this->response($message, 200);
                    }
                }
            }
        }
    }

    public function getFeatureDet($reqf) {
        if ($reqf === 'bulkEmail') {
            return "Hello Email";
        }
        if ($reqf === 'integration') {
            return "Website integration";
        }
        if ($reqf === 'DEF') {
            return "Digital Enquiry Form";
        }
        if ($reqf === 'customAnalytics') {
            return "Custom analytics";
        }
        if ($reqf === 'exportLead') {
            return "Export Lead";
        }
    }

    // Old logic for user and other plan based checking
    /*
     *       log_message("error", json_encode($orgdettemp));
      $orgdet = $orgdettemp[0];
      if ($orgdet->accountType === 'FR') {
      if ($req === 'user') {
      $this->db->where('organizationId', $orgId);
      $this->db->where('status', '0');
      $userQuery = $this->db->get('users');
      $this->db->where('organizationId', $orgId);
      $this->db->where('status', '0');
      $creditQuery = $this->db->get('credit');
      $creditResult = $creditQuery->result();
      if (!empty($creditResult)) {
      $noUsers = $userQuery->num_rows();
      if ($noUsers == $creditResult[0]->multi_user) {
      $message = array(
      'status' => 'error',
      'message' => '', 'allowedUser' => $creditResult[0]->multi_user, "currentUser" => $noUsers);
      $this->response($message, 200);
      } else {
      $message = array(
      'status' => 'success',
      'message' => '');
      $this->response($message, 200);
      }
      }
      }
      if ($req === 'list') {
      $this->db->where('organizationId', $orgId);
      $this->db->where('status', '0');
      $this->db->where('demoStatus', '1');
      $eventQuery = $this->db->get('events');
      $this->db->where('organizationId', $orgId);
      $this->db->where('status', '0');
      $creditQuery = $this->db->get('credit');
      $creditResult = $creditQuery->result();
      log_message("error", $eventQuery->num_rows());
      if ($eventQuery->num_rows() == $creditResult[0]->multi_lists) {
      $message = array(
      'status' => 'error',
      'message' => 'Upgrade to premium to add more then 2 lists');
      $this->response($message, 200);
      } else {
      $message = array(
      'status' => 'success',
      'message' => '');
      $this->response($message, 200);
      }
      }
      if ($req === 'visitor') {
      $this->db->where('organizationId', $orgId);
      $creditQuery = $this->db->get('credit');
      $creditDet = $creditQuery->result();
      if (!empty($creditDet)) {
      if ((int) $creditDet[0]->remainingCredit == 0) {
      $message = array(
      'status' => 'error',
      'message' => 'Upgrade to premium to add more leads');
      $this->response($message, 200);
      } else {
      $message = array(
      'status' => 'success',
      'message' => '');
      $this->response($message, 200);
      }
      }
      }
      } elseif ($orgdet->accountType != 'FR') {
      if ($req === 'user') {
      $this->db->where('organizationId', $orgId);
      $this->db->where('status', '0');
      $userQuery = $this->db->get('users');
      $this->db->where('organizationId', $orgId);
      $this->db->where('status', '0');
      $creditQuery = $this->db->get('credit');
      $creditResult = $creditQuery->result();
      if (!empty($creditResult)) {
      $noUsers = $userQuery->num_rows();
      if ($noUsers == $creditResult[0]->multi_user) {
      $message = array(
      'status' => 'error',
      'message' => '', 'allowedUser' => $creditResult[0]->multi_user, "currentUser" => $noUsers);
      $this->response($message, 200);
      } else {
      $message = array(
      'status' => 'success',
      'message' => '');
      $this->response($message, 200);
      }
      }
      }
      if ($req === 'list') {
      $message = array(
      'status' => 'success',
      'message' => '');
      $this->response($message, 200);
      }
      /* if($req === 'visitor'){
      $message = array(
      'status' => 'success',
      'message' => '');
      $this->response($message, 200);
      }
      if ($req === 'visitor') {
      $this->db->where('organizationId', $orgId);
      $creditQuery = $this->db->get('credit');
      $creditDet = $creditQuery->result();
      if (!empty($creditDet)) {
      if ((int) $creditDet[0]->remainingCredit == 0 && $orgdet->accountType != 'P4') {
      $message = array(
      'status' => 'error',
      'message' => 'Upgrade to premium to add more leads');
      $this->response($message, 200);
      } else {
      $message = array(
      'status' => 'success',
      'message' => '');
      $this->response($message, 200);
      }
      }
      }
      }


     */

    /*
      public function organizationCheck_get() {

      AuthManager::authenticate();

      $name = $this->get('name');
      $city = $this->get('city');
      $country = $this->get('country');

      log_message("debug", 'Checking if org exists  name ' . $name . ' city ' . $city . ' country ' . $country);

      $org = $this->organizationmodel->getOrganizationByNameCityAndCountry($name, $city, $country);

      if ($org) {
      log_message("debug", "we got the org as follows....");
      $this->response($org, 200);
      } else {
      log_message("debug", "sorry we do not have an org here yet....");
      $this->response([], 404);
      }
      }
     */

    /*
      public function organization_post() {

      AuthManager::authenticate();

      if (!$this->post('name')) {
      $this->response(array('error' => 'Missing post data: name'), 400);
      } else {

      $name = $this->post('name');
      $city = $this->post('city');
      $country = $this->post('country');

      $org = $this->organizationmodel->getOrganizationByNameCityAndCountry($name, $city, $country);


      if ($org) {
      log_message("debug", "Fetched the existing org details...");
      log_message("debug", "Org id is " . $org->id);

      $message = array(
      'status' => 'success',
      'message' => 'Organization with same name already exists.. Returning');
      $this->response($org, 200);
      }

      log_message("debug", "Going to create a new organization.....");
      $data = array(
      'name' => $this->post('name'),
      'description' => $this->Check_empty($this->post('description')),
      'contactFirstName' => $this->Check_empty($this->post('contactFirstName')),
      'contactLastName' => $this->Check_empty($this->post('contactLastName')),
      'contactDesignation' => $this->Check_empty($this->post('contactDesignation')),
      'contactEmail' => $this->Check_empty($this->post('contactEmail')),
      'contactPhone' => $this->Check_empty($this->post('contactPhone')),
      'address1' => $this->Check_empty($this->post('address1')),
      'address2' => $this->Check_empty($this->post('address2')),
      'city' => $this->Check_empty($this->post('city')),
      'state' => $this->Check_empty($this->post('state')),
      'zip' => $this->Check_empty($this->post('zip')),
      'country' => $this->Check_empty($this->post('country')),
      'noOfEmployees' => $this->post('noOfEmployees'),
      'status' => $this->post('status')
      );
      }
      $name = $this->post('name');
      $orgId = $this->organizationmodel->create($data);
      log_message("debug", "Sucessfully created organization....." . $orgId);

      $data['id'] = $orgId;



      if ($orgId > 0) {
      $message = array(
      'status' => 'success',
      'message' => sprintf($this->lang->line('organization.add'), $orgId, $name));
      $this->response($data, 200);
      } else {
      $message = array(
      'status' => 'error',
      'message' => 'Organization could not be created');
      $this->response($message, 404);
      }
      }
     */

    //----------------------------------------------- Show User Activity Log -------------------------------------------------- 

    public function dataAcitivites_get() {
        AuthManager::authenticate();
        if (!$this->get('organizationId')) {
            $this->response(array('error' => 'Organization details is required'), 400);
        }
        $organizationId = $this->get('organizationId');
        $req = $this->get('request');
        $resultActivities = $this->organizationmodel->getActivitiesByOrg($organizationId, $req);

        $this->response($resultActivities, 200);
    }

    //----------------------------------------------- End Show User Activity Log -------------------------------------------------- 
    // ---------------------------------------------- Updating the currency value for organization --------------------------------
    public function currencyUpdate_put() {
        AuthManager::authenticate();
        if (!$this->put('organizationId')) {
            $this->response(array('status' => 'error', 'message' => 'OrganizationId is required for update'), 400);
        }
        $organizationId = $this->put('organizationId');
        $orgUpdate = array(
            "orgCurrency" => $this->put('orgCurrency'),
            "modifiedAt" => gmdate('Y-m-d H:i:s')
        );
        $ucurrency = $this->organizationmodel->updateCurrency($organizationId, $orgUpdate);
        $message = array(
            'status' => 'success',
            'message' => 'Organization Currency type updated successfully');

        $this->response($message, 200);
    }

    //----------------------------------------------------- End Currency Update ------------------------------------------------------ 
    public function organization_put() {

        AuthManager::authenticate();

        if (!$this->put('id')) {
            $this->response(array('error' => 'id is required for update'), 400);
        }

        $data = array(
            'id' => $this->put('id'),
            'displayName' => $this->put('displayName'),
            'name' => $this->put('displayName'),
            'description' => $this->nullIfEmpty($this->put('description')),
            'contactFirstName' => $this->nullIfEmpty($this->put('contactFirstName')),
            'contactLastName' => $this->nullIfEmpty($this->put('contactLastName')),
            'contactDesignation' => $this->nullIfEmpty($this->put('contactDesignation')),
            'contactEmail' => $this->nullIfEmpty($this->put('contactEmail')),
            'contactPhone' => $this->nullIfEmpty($this->put('contactPhone')),
            'contactPhoneCode' => $this->nullIfEmpty($this->put('contactPhoneCode')),
            'address1' => $this->nullIfEmpty($this->put('address1')),
            'address2' => $this->nullIfEmpty($this->put('address2')),
            'city' => $this->nullIfEmpty($this->put('city')),
            'state' => $this->nullIfEmpty($this->put('state')),
            'zip' => $this->nullIfEmpty($this->put('zip')),
            'country' => $this->nullIfEmpty($this->put('country')),
            'noOfEmployees' => $this->put('noOfEmployees'),
            'orgCurrency' => $this->put('orgCurrency'),
            'orgTimeZone' => $this->put('orgTimeZone'),
            'status' => $this->put('status'),
            'modifiedAt' => gmdate('Y-m-d H:i:s')
        );
        if ($this->organizationmodel->update($data)) {
            // Update all users organization name with this
            $this->db->where('organizationId', $this->put('id'));
            $this->db->update('users', array('organizationName' => $this->put('displayName')));
            //-----------------------------------------------------
            $users = $this->usermodel->getUsersByOrganization($this->put('id'));
            if ($users) {
                $user = $users[0];
                if ($user->firstName == $user->email && $this->put('contactFirstName')) {
                    if ($this->put('newpassword')) {
                        $password = $this->put('newpassword');
                        $password_hash = PassHash::hash($password);
                    }
                    $userData = array(
                        'firstName' => $this->put('contactFirstName'),
                        'lastName' => $this->put('contactLastName'),
                        'password_hash' => $password_hash
                    );
                    $this->db->where('id', $user->id);
                    $this->db->update('users', $userData);
                    $newVisitor = array(
                        "firstName" => $this->put('contactFirstName'),
                        "lastName" => $this->put('contactLastName'),
                        'visitorOrganizationName' => $this->put('displayName'),
                        'designation' => $this->put('contactDesignation'),
                        'email' => $this->put('contactEmail'),
                        'phone' => $this->put('contactPhone'),
                        'eventId' => '2095',
                        'country' => $this->put('country'),
                        'organizationId' => "13",
                        'capturedUserId' => "29",
                        'capturedUserEmailId' => "info@helloleadz.io",
                        'createdAt' => gmdate('Y-m-d H:i:s'),
                        'modifiedAt' => gmdate('Y-m-d H:i:s')
                    );

                    $visitorId = $this->visitormodel->create($newVisitor);
                    $subject = "HelloLeads - New Registration Alert";
                    $alertMsg = $this->getNewRegAlertMsg($data, $user->createdAt);
                    $maildata = array(
                        "fromid" => "no-reply@helloleads.io",
                        "toid" => "info@helloleads.io",
                        "subject" => $subject,
                        "message" => $alertMsg,
                        "status" => 'pending',
                        "date" => gmdate('Y-m-d H:i:s')
                    );
                    $mailqueueId = $this->emailqueuemodel->create($maildata);
                }
            }
            $message = array(
                'status' => 'success',
                'message' => sprintf($this->lang->line('organization.edit'), $this->put('displayName')));
        } else {
            $message = array(
                'status' => 'success',
                'message' => $this->lang->line('organization.edit.error'));
        }
        $this->response($message, 200);
    }

    // Update map api values 
    public function makeLeadApi_get() {

        $page = $this->get('page');

        $start = ($page - 1) * 100;
        $end = 100;
        $visitorQuery = $this->db->query("select v.id as visitorId,v.city,v.state,v.country,v.zip,v.address1,v.address2 FROM visitor v LEFT OUTER JOIN EVENTS es ON es.id  = v.eventId LEFT OUTER JOIN organization org ON org.id = v.organizationId WHERE org.status  = '0' AND es.status  = '0' AND v.status  = '0' AND (v.address1 IS NOT NULL OR v.address1 != '' OR v.address2 IS NOT NULL ) AND (v.city IS NOT NULL OR v.city != '' OR v.state != '' OR v.state IS NOT NULL) AND v.country != '' AND v.country IS NOT NULL AND v.zip IS NOT NULL and v.leadAddr_lat is null and v.leadAddr_long is null limit " . $start . "," . $end);
        $visitorCount = $this->db->query("select v.id FROM visitor v LEFT OUTER JOIN EVENTS es ON es.id  = v.eventId LEFT OUTER JOIN organization org ON org.id = v.organizationId WHERE org.status  = '0' AND es.status  = '0' AND v.status  = '0' AND (v.address1 IS NOT NULL OR v.address1 != '' OR v.address2 IS NOT NULL ) AND (v.city IS NOT NULL OR v.city != '' OR v.state != '' OR v.state IS NOT NULL) AND v.country != '' AND v.country IS NOT NULL AND v.zip IS NOT NULL and v.leadAddr_lat IS NULL and v.leadAddr_long IS NULL");
        $this->response(array("visitors" => $visitorQuery->result(), "totalPages" => round($visitorCount->num_rows() / 100, 0), "page" => $page), 200);
    }

    public function makeLeadUpdate_put() {

        $visitorData = array(
            "leadAddr_lat" => $this->put('leadAddr_lat'),
            "leadAddr_long" => $this->put('leadAddr_long'),
            "modifiedAt" => gmdate('Y-m-d H:i:s')
        );

        $visitorId = $this->put('visitorId');
        $this->db->where('id', $visitorId);
        $this->db->update('visitor', $visitorData);
        $this->response(array("status" => "success", "message" => "Lead data updated"), 200);
    }

    public function organization_delete($id = NULL) {

        AuthManager::authenticate();

        if ($id == NULL) {
            $message = array('error' => 'Missing delete data: id');
            $this->response($message, 400);
        } else {
            $this->organizationmodel->delete($id);
            $message = array('status' => 'success', 'message' => sprintf($this->lang->line('organization.delete'), $id));
            $this->response($message, 200);
        }
    }

  public function getFeedbackMessage($orgDet,$userDet,$feedback,$value,$description) {
        $plan= '';
        if(stripos("T",$feedback[0]->accessStage) ){
            $plan = 'Trial';
        }else if(stripos("P",$feedback[0]->accessStage) ){
            $plan = 'Paid';
        }else{
            $plan = 'Feature';
        }
        $message = '<html>
                <head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <style>
                .header {
                background-color : #18A689;
                color:white;
                }
                </style>
                </head>
                <body style="font-size:18px;">
                Hello Team,<br><br>
                The below rating and feedback have been given by '.$userDet[0]->firstName." ".$userDet[0]->lastName.', '.$orgDet[0]->displayName.'.<br><br>
                <table border="1" style="font-size:18px;">
                <tr><td class="header">Title</td><td>Rating and Feedback</td></tr>
                <tr><td class="header">Account Number</td><td>HL0000'.$orgDet[0]->id.'</td></tr>
                <tr><td class="header">User Name</td><td></td>'.$userDet[0]->firstName." ".$userDet[0]->lastName.'</tr>
                <tr><td class="header">Organization</td><td>'.$orgDet[0]->displayName.'</td></tr>
                <tr><td class="header">Email</td><td>'.$userDet[0]->email.'</td></tr>
                <tr><td class="header">Mobile</td><td>'.$userDet[0]->phoneCode.$userDet[0]->phone.'</td></tr>
                <tr><td class="header">Plan Type</td><td>'.$plan.'</td></tr>
                <tr><td class="header">Question/ Topic</td>'.$feedback[0]->title.'<td></td></tr>
                <tr><td class="header">Answer/ Rating</td>'.$value.'<td></td></tr>
                <tr><td class="header">Feedback</td><td>'.$description.'</td></tr>
                <tr><td class="header">Date and Time</td><td>'.date('Y-m-d H:i:s').'GMT + 5:30</td></tr>
                </table>
                <br><br>
                Regards,<br>HelloLeads Team
                </body>
                </html>';
        
        return $message;
    }

    public function accountCloseMsg() {
        $message = "<html xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns:m='http://schemas.microsoft.com/office/2004/12/omml' xmlns='http://www.w3.org/TR/REC-html40'>

<head>
    <meta http-equiv=Content-Type content='text/html; charset=utf-8'>
    <meta name=Generator content='Microsoft Word 14 (filtered medium)'>
	
    <!--[if !mso]><style>v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style><![endif]-->
   
    <style>
        < !--
        /* Font Definitions */
        @font-face {
            font-family: Helvetica;
            panose-1: 2 11 6 4 2 2 2 2 2 4;
        }

        @font-face {
            font-family: Helvetica;
            panose-1: 2 11 6 4 2 2 2 2 2 4;
        }

        @font-face {
            font-family: Cambria;
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }

        @font-face {
            font-family: Tahoma;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
        }

        /* Style Definitions */
        p.MsoNormal,
        li.MsoNormal,
        div.MsoNormal {
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            font-family: 'Times New Roman', 'serif';
        }

        h1 {
            mso-style-priority: 9;
            mso-style-link: 'Heading 1 Char';
            mso-margin-top-alt: auto;
            margin-right: 0in;
            mso-margin-bottom-alt: auto;
            margin-left: 0in;
            font-size: 24.0pt;
            font-family: 'Times New Roman', 'serif';
            font-weight: bold;
        }

        h2 {
            mso-style-priority: 9;
            mso-style-link: 'Heading 2 Char';
            mso-margin-top-alt: auto;
            margin-right: 0in;
            mso-margin-bottom-alt: auto;
            margin-left: 0in;
            font-size: 18.0pt;
            font-family: 'Times New Roman', 'serif';
            font-weight: bold;
        }

        a:link,
        span.MsoHyperlink {
            mso-style-priority: 99;
            color: blue;
            text-decoration: underline;
        }

        a:visited,
        span.MsoHyperlinkFollowed {
            mso-style-priority: 99;
            color: purple;
            text-decoration: underline;
        }

        p {
            mso-style-priority: 99;
            mso-margin-top-alt: auto;
            margin-right: 0in;
            mso-margin-bottom-alt: auto;
            margin-left: 0in;
            font-size: 12.0pt;
            font-family: 'Times New Roman', 'serif';
        }

        p.MsoAcetate,
        li.MsoAcetate,
        div.MsoAcetate {
            mso-style-priority: 99;
            mso-style-link: 'Balloon Text Char';
            margin: 0in;
            margin-bottom: .0001pt;
            font-size: 8.0pt;
            font-family: 'Tahoma', 'sans-serif';
        }

        span.Heading1Char {
            mso-style-name: 'Heading 1 Char';
            mso-style-priority: 9;
            mso-style-link: 'Heading 1';
            font-family: 'Cambria', 'serif';
            color: #365F91;
            font-weight: bold;
        }

        span.Heading2Char {
            mso-style-name: 'Heading 2 Char';
            mso-style-priority: 9;
            mso-style-link: 'Heading 2';
            font-family: 'Cambria', 'serif';
            color: #4F81BD;
            font-weight: bold;
        }

        span.EmailStyle20 {
            mso-style-type: personal;
            font-family: 'Calibri', 'sans-serif';
            color: #1F497D;
        }

        span.BalloonTextChar {
            mso-style-name: 'Balloon Text Char';
            mso-style-priority: 99;
            mso-style-link: 'Balloon Text';
            font-family: 'Tahoma', 'sans-serif';
        }

        span.EmailStyle23 {
            mso-style-type: personal-reply;
            font-family: 'Calibri', 'sans-serif';
            color: #1F497D;
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            font-size: 10.0pt;
        }

        @page WordSection1 {
            size: 8.5in 11.0in;
            margin: 1.0in 1.0in 1.0in 1.0in;
        }

        div.WordSection1 {
            page: WordSection1;
        }

        -->
    </style>
    <!--[if gte mso 9]><xml>
<o:shapedefaults v:ext='edit' spidmax='1026' />
</xml><![endif]-->
    <!--[if gte mso 9]><xml>
<o:shapelayout v:ext='edit'>
<o:idmap v:ext='edit' data='1' />
</o:shapelayout></xml><![endif]-->
</head>
<div align=center>
                        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=650 style='width:487.5pt;background:white'>
                            <tr style='height:30.0pt'>
                                <td style='padding:7.5pt 30.0pt 7.5pt 30.0pt;height:30.0pt'>
                                    <p class=MsoNormal align=right style='margin-right:-31.9pt;text-align:right'><b><span style='font-size:11.0pt;font-family:'Calibri','sans-serif';color:#1F497D'><img border=0 width=175 height=52 id='Picture_x0020_3' src='https://www.helloleads.io/images/helloleads_logo.png' alt='New HLS for sign drive1'></span></b><span style='color:#333333'><o:p></o:p></span></p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan=2 style='background:#13B696;padding:0in 30.0pt 0in 30.0pt'>
                                    <div align=center>
                                        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 style='border-collapse:collapse'>
                                            <tr style='height:24.6pt'>
                                                <td style='padding:37.5pt 0in 37.5pt 0in;height:24.6pt'>
                                                    <h1 align=center style='margin:0in;margin-bottom:.0001pt;text-align:center;font-weight:inherit'><span style='font-size:18.0pt;color:white'>Your account is being closed.</span><span style='font-size:18.0pt;color:#1F497D'><o:p></o:p></span></h1>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style='padding:0in 30.0pt 0in 30.0pt'>
                                    <div align=center>
                                        <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0>
                                            <tr>
                                                <td style='padding:22.5pt 0in 0in 0in'>
                                                    <h2 style='margin:0in;margin-bottom:.0001pt;font-weight:inherit'><span style='font-size:13.5pt'>Hello,<o:p></o:p></span></h2>
                                                    <p style='margin:0in;margin-bottom:.0001pt'><b><span style='font-size:11.0pt;font-family:'Calibri','sans-serif''><o:p>&nbsp;</o:p></span></b></p>
                                                    <p>It was wonderful having you on board and we are a sad to see you go.
                                                        <o:p></o:p>
                                                    </p>
                                                   
                                                    
                                                    </p>
                                                    <p>For your information - Since we take the security and safety of your data seriously, we will be deleting your account with all its data.
                                                        <o:p></o:p>
                                                    </p>
                                                </td>
                                            </tr>
											<tr>
                                                                    <td style='padding:15.0pt 0in 0in 0in'>
                                                                        <h2 style='margin:0in;margin-bottom:.0001pt'><span style='font-size:15.0pt'>Here's what will happen to your data<o:p></o:p></span></h2>
                                                                        <h2 style='margin:0in;margin-bottom:.0001pt'><span style='font-size:11.0pt;font-family:'Calibri','sans-serif''><o:p>&nbsp;</o:p></span></h2>
                                                                        <p style='margin:0in;margin-bottom:.0001pt'>Your data will be securely deleted from our servers. Once deleted you will not be able to retrieve your data.
                                                                            <o:p></o:p>
                                                                        </p>
                                                                        <p style='margin:0in;margin-bottom:.0001pt'>
                                                                            <o:p>&nbsp;</o:p>
                                                                        </p>
                                                                        
                                                                    </td>
                                                                </tr>
																<tr>
                                                                    <td style='padding:15.0pt 0in 0in 0in'>
                                                                        <h2 style='margin:0in;margin-bottom:.0001pt'><span style='font-size:15.0pt'>We want your feedback<o:p></o:p></span></h2>
                                                                        <h2 style='margin:0in;margin-bottom:.0001pt'><span style='font-size:11.0pt;font-family:'Calibri','sans-serif''><o:p>&nbsp;</o:p></span></h2>
                                                                        <p style='margin:0in;margin-bottom:.0001pt'>Did you stop using HelloLeads because something specific you wanted was missing? If that’s the case, respond to this email to let us know. Also we might have added the feature you are looking for already.<span style='font-size:11.0pt;font-family:'Calibri','sans-serif''><o:p></o:p></span></p>
                                                                        <p style='margin:0in;margin-bottom:.0001pt'>
                                                                            <o:p>&nbsp;</o:p>
                                                                        </p>
                                                                        
                                                                    </td>
                                                                </tr>
                                            <tr>
                                                <td style='padding:22.5pt 0in 26.25pt 0in'>
                                                    <p style='margin:0in;margin-bottom:.0001pt'>Cheers,
                                                        <o:p></o:p>
                                                    </p>
                                                    <p style='margin:0in;margin-bottom:.0001pt'><span style='font-size:11.0pt;font-family:'Calibri','sans-serif''><o:p>&nbsp;</o:p></span></p>
                                                    <p style='margin:0in;margin-bottom:.0001pt'><b><span style='font-size:11.0pt;font-family:'Calibri','sans-serif''>HelloLeads - Support Team <o:p></o:p></span></b></p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td style='padding:0in 0in 0in 0in'></td>
                            </tr>
                        </table>
                    </div>
					</body>
					</html>
					";

        return $message;
    }

    public function getNewRegAlertMsg($orgDetail, $cdate) {
        $message = "<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid #273038;
    border-collapse: collapse;
}
th, td {
    padding: 10px;
    text-align: left;font-family:Sans-serif;
}
th {width:200px; background-color:#EF5E4A;color:white;}
p {font-size:120%;}
</style>
</head>
<!---Sub:HelloLeads - New Lead Registration Alert-->
<body style='margin:10px;'>
<h2 style='color:#273038'>HelloLeads - New Registration Alert</h2>
<table style='width:600px;'>
  <tr>
    <th>Account Number</th>
    <td>HL00" . $orgDetail['id'] . "</td>
  </tr>
  <tr>
    <th>Organization Name</th>
    <td>" . $orgDetail['name'] . "</td>
  </tr>
<tr>
    <th>Name</th>
    <td>" . $orgDetail['contactFirstName'] . " " . $orgDetail['contactLastName'] . "</td>
  </tr>
  <tr>
    <th>Designation</th>
    <td>" . $orgDetail['contactDesignation'] . "</td>
  </tr>

<tr>
    <th>Email Address</th>
    <td>" . $orgDetail['contactEmail'] . "</td>
  </tr>
<tr>
    <th>Phone</th>
    <td>" . $orgDetail['contactPhone'] . "</td>
  </tr><tr>
    <th>City</th>
    <td>" . $orgDetail['city'] . "</td>
  </tr>
<tr>
    <th>State</th>
    <td>" . $orgDetail['state'] . "</td>
  </tr>
<tr>
    <th>Country</th>
    <td>" . $orgDetail['country'] . "</td>
  </tr>
<tr>
    <th>ZIP</th>
    <td>" . $orgDetail['zip'] . "</td>
  </tr>
  <tr>
    <th>Source of Registration</th>
    <td>Web</td>
  </tr>
<tr>
    <th>Date of Registration</th>
    <td>" . date("Y-M-d", strtotime($cdate)) . "</td>
  </tr>
</table>
<p>Please take it forward.</p>
<p>
Best Regards,</p>
<p>Team-HelloLeads</p>

</body>
</html>


";


        return $message;
    }

}
