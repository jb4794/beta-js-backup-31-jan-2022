<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./application/libraries/Dex_Rest_Controller.php');
require (APPPATH . 'controllers/AuthManager.php');

class Permission extends Dex_Rest_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('permissionmodel');
        $this->load->model('organizationmodel');
		$this->load->model('usermodel');
        $this->load->model('emailqueuemodel');
        $this->load->database();
        $this->lang->load('messages', 'english');
        $this->load->library('session');
    }

    public function permission_get($id = NULL) {

        AuthManager::authenticate();

        if (!$this->get('id')) {
            $permission = $this->permissionmodel->getAll();
        } else {
            $permission = $this->permissionmodel->getById($id);
        }

        if ($permission) {
            $this->response($permission, 200);
        } else {
            $this->response([], 404);
        }
    }

    public function permission_post() {

        AuthManager::authenticate();

        if (!$this->post('name')) {
            $this->response(array('error' => 'Missing post data: name'), 400);
        }
        $data = array(
            'name' => $this->nullIfEmpty($this->post('name')),
            'description' => $this->nullIfEmpty($this->post('description'))
        );

        $name = $this->post('name');
        $permissionId = $this->permissionmodel->create($data);
        if ($permissionId > 0) {
            $message = array(
                'status' => 'success',
                'message' => sprintf($this->lang->line('permission.add'), $permissionId, $name));
            $this->response($message, 200);
        }
    }

    public function permission_put() {

        AuthManager::authenticate();

        if (!$this->put('id')) {
            $this->response(array('error' => 'id is required for update'), 400);
        }

        $data = array(
            'id' => $this->put('id'),
            'name' => $this->nullIfEmpty($this->put('name')),
            'description' => $this->nullIfEmpty($this->put('description'))
        );
        if ($this->permissionmodel->update($data)) {
            $message = array(
                'status' => 'success',
                'message' => sprintf($this->lang->line('permission.edit'), $this->put('id')));
        } else {
            $message = array(
                'status' => 'success',
                'message' => $this->lang->line('permission.edit.error'));
        }
        $this->response($message, 200);
    }
	
	// API to get Role permission config
    public function rolePermissionConfig_get(){
        AuthManager::authenticate();
        $rpresult = $this->permissionmodel->getRolePermission();
        $this->response($rpresult, 200);
    }
    

    public function checkPermission_get() {

        if (empty($this->get('userId')) && empty($this->get('permission'))) {
            $message = array('error' => 'Missing input details(role and permission)');
            $this->response($message, 400);
        }
        $chkResult = array();
        //$role = $this->get('role');
        $permission = $this->get('permission');
        $userId = $this->get('userId');
        $userName = $this->session->userdata('username');
		$roleId = $this->session->userdata('roleId');
        $orgId = $this->get('organizationId');
        $orgDet = array();
        $userDet = array();
        $orgFname = "";
		$orgEmail = "";
        $orgLname = "";
        $orgCname = "";
        $orgno = "";
        $roletxt = "";
        $transMsg = $this->getTransMsg($permission);
        $userDet = $this->usermodel->getBy("id",$userId);
        $orgDet = $this->organizationmodel->getById($orgId);
        if ($orgDet) {
            $orgFname = $orgDet[0]->contactFirstName;
            $orgLname = $orgDet[0]->contactLastName;
            $orgCname = $orgDet[0]->displayName;
            $orgno = $orgDet[0]->id;
			$orgEmail = $orgDet[0]->contactEmail;
        }
        if ($userDet) {
             $userName = $userDet[0]->firstName;
             $roleId = $userDet[0]->roleId;
        }
		if($roleId == '2'){
			$roletxt ="Manager"; 
			
		}
		if($roleId == '3'){
			$roletxt ="L1 User"; 
			
		}
		if($roleId == '4'){
			$roletxt ="L2 User"; 
			
		}

        $chkResult = $this->permissionmodel->checkRolePermission($userId, $permission);
        if ($chkResult > 0) {					//$chkResult > 0
            $message = array(
                'status' => 'success',
                'message' => "Permission granted you can access");
        } else {

            $alertMsg = "
						Dear ".$orgFname." ".$orgLname.",<br><br>
						
						Following are details of recent authorization failure. Please review.<br><br><br>
						Account No: HL0000" . $orgId . "<br>
                        Account Name: " . $orgCname . "<br>
                        User Name: ".$userName."<br>
						Role: ".$roletxt."<br>
                        Transaction : ".$transMsg."<br>
                        Date and Time: ". gmdate('d-M-y H:i:s')." GMT <br><br>

                        His / her current role does not have permissions to carry out the above transaction.
                        To understand roles and permissions, please review the <a href='https://www.helloleads.io/files/HelloLeads-Roles_and_Permission_Help_Image.jpg'>permission matrix</a>. 
                        To modify roles (and hence permission) please visit user management in HelloLeads Web or Mobile. (<a href='https://beta1.helloleads.io'>Login Now</a>)<br><br>


                        This is an automatically generated email from HelloLeads.<br>

                        For support, please feel free to <a href='https://www.helloleads.io/support.php'>contact</a> the support team.<br><br>

                        <a href='https://www.HelloLeads.io'>www.HelloLeads.io</a> 
                        ";
            $alertMaildata = array(
                "fromId" => "alert@helloleads.io",
                "toId" => $orgEmail,
                "subject" => "HelloLeads – Authorization Failure Alert – " . $userName.', '.$roletxt,
                "message" => $alertMsg,
                "date" => gmdate('Y-m-d H:i:s')
            );

            $alertMailId = $this->emailqueuemodel->create($alertMaildata);
            $message = array(
                'status' => 'error',
                'message' => "Your are not authorized to do this action. Please contact your Account Owner, " . $orgFname . " " . $orgLname . " for assistance. 
");
        }
        $this->response($message, 200);
    }

    public function permission_delete($id = NULL) {

        AuthManager::authenticate();

        if ($id == NULL) {
            $message = array('error' => 'Missing delete data: id');
            $this->response($message, 400);
        } else {
            $this->permissionmodel->delete($id);
            $message = array('status' => 'success', 'message' => sprintf($this->lang->line('permission.delete'), $id));
            $this->response($message, 200);
        }
    }
    
    public function getTransMsg($permission){
        if($permission === 'addUser'){
            return "Add User Details";
        }
        if($permission === 'editUser'){
            return "Edit User Details";
        }
        if($permission === 'deleteUser'){
            return "Delete User Details";
        }
        if($permission === 'addList'){
            return "Add New List";
        }
        if($permission === 'editList'){
            return "Edit List";
        }
        if($permission === 'deleteList'){
            return "Delete List";
        }
        if($permission === 'addLead'){
            return "Add Lead Details";
        }
        if($permission === 'editLead'){
            return "Edit Lead Details";
        }
        if($permission === 'deleteLead'){
            return "Delete Lead Details";
        }
        if($permission === 'importLead'){
            return "Importing Leads";
        }
        if($permission === 'exportLeads'){
            return "Exporting Lead";
        }
        if($permission === 'openAssignments'){
            return "Assigning Open Assignments";
        }
        if($permission === 'shareInfo'){
            return "Share Lead Information";
        }
        if($permission === 'addInterest'){
            return "Add New Interest";
        }
        if($permission === 'editInterest'){
            return "Edit Interest";
        }
        if($permission === 'deleteInterest'){
            return "Delete Interest";
        }
        if($permission === 'addCategory'){
            return "Add New C-Group";
        }
        if($permission === 'editCategory'){
            return "Edit C-Group";
        }
        if($permission === 'deleteCategory'){
            return "Delete C-Group";
        }
        if($permission === 'addCustom'){
            return "Add Custom Fields";
        }
        if($permission === 'editCustom'){
            return "Edit Custom Fields";
        }
         if($permission === 'deleteCustom'){
            return "Delete Custom Fields";
        }
         if($permission === 'companyProfile'){
            return "Company Profile Updation";
        }
         if($permission === 'payment'){
            return "Payment";
        }
		if ($permission === 'bulkEmail') {
            return "Hello Email";
        }
		if ($permission === 'changeCurrency') {
            return "Change Currency";
        }
		if ($permission === 'social') {
            return "Facebook Integration";
        }
		if ($permission === 'quotesInvoice') {
            return "Quotes and Invoices";
        }
        if ($permission === 'productCatalogue') {
            return "Accessing Product Catalog";
        }
		if ($permission === 'quotesInvoiceSettings') {
            return " Accessing Quotes and Invoices Settings";
        } 
    }

}
